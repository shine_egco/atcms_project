<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>
<%@ page import = "java.text.DateFormat" %>



<% 

	System.out.println("Start get_purchase_bill_main_by_purchase_bill_id_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

		
	//List<Order> order_list = new ArrayList<Order>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	//String purchase_id = request.getParameter("purchase_id");
	//System.out.println("purchase_id:"+purchase_id);
	 String purchase_bill_id = request.getParameter("purchase_bill_id");

	 System.out.println("purchase_bill_id:"+purchase_bill_id);



      try{
    	  
    
    		System.out.println("Keyword is Text"); 
       		String	sql_query =  " SELECT * "+
    						"  FROM purchase_main "+
       						" JOIN company "+
    						" ON  purchase_main.vendor_id = company.company_id "+
    						"  WHERE BINARY purchase_main.purchase_id ='"+purchase_bill_id+"'";
  
   			 
   			 

    
    	  System.out.println("sql_query:"+sql_query);
    	  
    	  
    	  ResultSet rs_pb = connect.createStatement().executeQuery(sql_query);
          
    	  
    	  Purchase pb = new Purchase();
    	  
    	  while(rs_pb.next())
          {
             	pb.setPurchaseId(rs_pb.getString("purchase_id"));
             	pb.setCreateDate(rs_pb.getString("create_date"));
             	pb.setInvNo(rs_pb.getString("inv_no"));
             	pb.setInvoiceDate(rs_pb.getString("invoice_date"));
             	pb.setPurchaseStatus(rs_pb.getString("status"));
             	pb.setTotalIncVat(rs_pb.getString("total_inc_vat"));
             	pb.setTotalValue(rs_pb.getString("total_value"));
             	pb.setTotalVat(rs_pb.getString("total_vat"));
             	pb.setVendorId(rs_pb.getString("vendor_id"));
             
             	
             	if((!("-").equals(rs_pb.getString("name_en")))&&(!("").equals(rs_pb.getString("name_en"))))
             	{
             
              		pb.setVendorName(rs_pb.getString("name_en"));
            		pb.setVendorAddress(rs_pb.getString("address_en"));
             		
             	}else{
            		pb.setVendorName(rs_pb.getString("name_th"));
             		pb.setVendorAddress(rs_pb.getString("address_th"));
             	}
             	pb.setVendorTaxID(rs_pb.getString("tax_id"));
             	
             	pb.setPatmentRef(rs_pb.getString("payment_ref"));
             	pb.setNote(rs_pb.getString("note"));
             	
       				
         //     credit_inv_list.add(ord);
            } 
              
          
             
         	try {
				Json = mapper.writeValueAsString(pb); 
				System.out.println("Json:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	
         	
         	out.print(Json);
	   
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  connect.close();
    	  out.print("fail");
      }
     
  
	connect.close();
%>
