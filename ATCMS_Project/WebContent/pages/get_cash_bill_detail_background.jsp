<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>



<% 

	System.out.println("Start get_cash_detail_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

	List<CashBill> cash_bill_list = new ArrayList<CashBill>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	
	String cash_bill_id = request.getParameter("cash_bill_id");
	System.out.println("cash_bill_id:"+cash_bill_id);

      try{
    	  
    	  String sql_query = " SELECT cash_bill_main.bill_date "+
    	  							 " ,cash_bill_main.inv_generated_datetime "+
    	    	  					 " ,cash_bill_main.inv_file_path "+
    	  					         " ,cash_bill_main.inv_no "+
    	  							 " ,cash_bill_main.create_date "+
    	  					         " ,cash_bill_main.cash_bill_id "+
    	  							 " ,cash_bill_main.status "+
    	  					         " ,cash_bill_main.ponum "+
    	  							 " ,cash_bill_main.total_inc_vat "+
    	  					         " ,cash_bill_main.total_value "+
    	  							 " ,cash_bill_main.total_vat "+
    	  					         " ,cash_bill_main.type "+
    	  							 " ,company.name_th "+
    	  					         " ,company.name_en "+
    	  							 " ,company.address_th "+
    	  					         " ,company.address_en "+
							" FROM cash_bill_main"+
		    			    " JOIN company "+
							" ON  cash_bill_main.customer_id = company.company_id "+
							" WHERE BINARY cash_bill_main.cash_bill_id='"+cash_bill_id+"'";
    	  System.out.println("sql_query:"+sql_query);
    	  ResultSet rs_cash_bill = connect.createStatement().executeQuery(sql_query);
          
    	  while(rs_cash_bill.next())
          {
              CashBill cas = new CashBill();
              		cas.setCreateDate("create_date");
              		cas.setCustomerId("customer_id");
              		
              		if(("-").equals(rs_cash_bill.getString("name_th")))
              		{
              			cas.setCustomerName(rs_cash_bill.getString("name_en"));
              			cas.setCustomerAddress(rs_cash_bill.getString("address_en"));
              			
              		}else{
              			cas.setCustomerName(rs_cash_bill.getString("name_th"));
              			cas.setCustomerAddress(rs_cash_bill.getString("address_th"));
              		}

              		cas.setIncGeneratedDate(rs_cash_bill.getString("inv_generated_datetime"));
              		cas.setInvNo(rs_cash_bill.getString("inv_no"));
              		cas.setCreateDate(rs_cash_bill.getString("create_date"));
              		cas.setCashBillId(rs_cash_bill.getString("cash_bill_id"));
              		cas.setStatus(rs_cash_bill.getString("status"));
              		cas.setPoNum(rs_cash_bill.getString("ponum"));
					cas.setTotalIncVat(rs_cash_bill.getString("total_inc_vat"));
					cas.setTotalValue(rs_cash_bill.getString("total_value"));
					cas.setTotalVat(rs_cash_bill.getString("total_vat"));
					cas.setType(rs_cash_bill.getString("type"));
					if(rs_cash_bill.getString("inv_file_path")!=null){	
						cas.setInvFilePath(rs_cash_bill.getString("inv_file_path"));
						String pattern = Pattern.quote(System.getProperty("file.separator"));
						String[] parts = rs_cash_bill.getString("inv_file_path").split(pattern);
						int temp = parts.length;
						String inv_file_name = parts[temp-1];
							   
						cas.setInvFileName(inv_file_name);
					}else{
						cas.setInvFilePath("-");
					}
				//	System.out.println("Q:path:"+rs_ord.getString("inv_file_path"));
				//	System.out.println("Q:name:"+inv_file_name);
					
					
              cash_bill_list.add(cas);
              
          }
             
         	try {
				Json = mapper.writeValueAsString(cash_bill_list); 
				System.out.println("Json:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	out.print(Json);
	   
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  connect.close();
    	  out.print("fail");
      }
     
  
	connect.close();
%>
