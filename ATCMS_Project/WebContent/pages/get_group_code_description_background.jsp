<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "com.atcms.*" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "java.util.Properties" %>
<%@ page import = "javax.servlet.ServletContext" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>


<% 

	System.out.println("Start get_group_code_description");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

	 ///////////////////////////////////////////////////
	List<Description> desc_list = new ArrayList<Description>();
	String Json="";
	ObjectMapper mapper = new ObjectMapper();
	try {		
		
		
		String sql_query =" select * "+
					      " from description "+
						  " where description.type = 'group_code'";

	
		ResultSet rs =  connect.createStatement().executeQuery(sql_query);
		System.out.println(sql_query);	
		
			
		
			while(rs.next()) {
				Description temp = new Description();
							temp.setCode(rs.getString("code"));
							temp.setType(rs.getString("type"));
							temp.setShortDescription(rs.getString("short_description"));
							temp.setLongDescription(rs.getString("long_description"));
				

				desc_list.add(temp);
				
				//if(product_list.size()==10) break;
			}
			

		
	 	System.out.println("desc_list size :"+desc_list.size());
		try {
			Json = mapper.writeValueAsString(desc_list); 
			
		} catch (JsonGenerationException ex) {
			ex.printStackTrace();
		} catch (JsonMappingException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		System.out.println("Json_description:"+Json);
		out.print(Json);
		
	} catch (SQLException e) {
		e.printStackTrace();
		  connect.close();
		out.print("fail");
	}
	 
	connect.close();
%>
