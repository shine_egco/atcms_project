<%@page import="org.apache.poi.xwpf.usermodel.TextAlignment"%>
<%@page import="org.apache.poi.xwpf.usermodel.Borders"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "com.atcms.*" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "java.util.Properties" %>
<%@ page import = "javax.servlet.ServletContext" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.util.Date" %>
<%@ page import="java.math.BigInteger" %>
<%@ page import="java.io.FileOutputStream" %>
<%@ page import="org.apache.poi.xssf.usermodel.*" %>
<%@ page import="org.apache.poi.hssf.usermodel.HSSFCellStyle" %>
<%@ page import="org.apache.poi.hssf.util.*" %>
<%@ page import="org.apache.poi.ss.util.CellUtil" %>
<%@ page import="org.apache.poi.ss.usermodel.*" %>

<!DOCTYPE html>
<html lang="en">

<head>




    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style type="text/css" style="display: none">
	
		.table-sortable tbody tr {
	    cursor: move;
		}
	
    
	</style>
	

</head>

<body>
	<%!
		public static final short EXCEL_COLUMN_WIDTH_FACTOR = 256; 
	    public static final short EXCEL_ROW_HEIGHT_FACTOR = 20; 
	    public static final int UNIT_OFFSET_LENGTH = 7; 
	    public static final int[] UNIT_OFFSET_MAP = new int[] { 0, 36, 73, 109, 146, 182, 219 };
		public static XSSFCellStyle CreateStyle(XSSFWorkbook workbook_in
							 				   ,String font_name_in 
							                   , int font_size_in)
		{
				XSSFFont font = workbook_in.createFont();
				font.setFontName(font_name_in);
				font.setFontHeightInPoints((short)font_size_in);
				
				XSSFCellStyle style = workbook_in.createCellStyle();
				style.setFont(font);
				return style ;
		}
				
		public static XSSFCellStyle CreateCellBorder(XSSFWorkbook workbook_in 
										, boolean top 
										, boolean bot 
										, boolean left 
										, boolean right)
		{
				
				XSSFCellStyle style = workbook_in.createCellStyle();
				if(top){  style.setBorderTop(HSSFCellStyle.BORDER_THIN); }
				if(bot){ style.setBorderBottom(HSSFCellStyle.BORDER_THIN); }
				if(left){ style.setBorderLeft(HSSFCellStyle.BORDER_THIN); }
				if(right){ style.setBorderRight(HSSFCellStyle.BORDER_THIN); }
				
				return style ;
		}
		public static void DrawListCellBorder (XSSFWorkbook workbook , XSSFRow row_in){
			
			XSSFCellStyle style_border_top_left = CreateCellBorder(workbook,true,false,true,false);
			XSSFCellStyle style_border_top_right = CreateCellBorder(workbook,true,false,false,true);
			XSSFCellStyle style_border_top_left_right = CreateCellBorder(workbook,true,false,true,true);
			XSSFCellStyle style_border_bot_left = CreateCellBorder(workbook,false,true,true,false);
			XSSFCellStyle style_border_bot_right = CreateCellBorder(workbook,false,true,false,true);
			XSSFCellStyle style_border_bot_left_right = CreateCellBorder(workbook,false,true,true,true);
			XSSFCellStyle style_border_top = CreateCellBorder(workbook,true,false,false,false);
			XSSFCellStyle style_border_bot = CreateCellBorder(workbook,false,true,false,false);
			XSSFCellStyle style_border_left = CreateCellBorder(workbook,false,false,true,false);
			XSSFCellStyle style_border_right = CreateCellBorder(workbook,false,false,false,true);
			XSSFCellStyle style_border_left_right = CreateCellBorder(workbook,false,false,true,true);
			XSSFCellStyle style_border_full = CreateCellBorder(workbook,true,true,true,true);
			
			row_in.createCell(0);
			row_in.createCell(1);
			row_in.createCell(11);
			row_in.createCell(12);
			row_in.getCell(0).setCellStyle(style_border_left_right);
			row_in.getCell(1).setCellStyle(style_border_left_right);
			row_in.getCell(11).setCellStyle(style_border_left_right);
			row_in.getCell(12).setCellStyle(style_border_left_right);

		}
	    public static int widthUnits2Pixel(short widthUnits) {
	    	
	        int pixels = (widthUnits / EXCEL_COLUMN_WIDTH_FACTOR) * UNIT_OFFSET_LENGTH; 
	        int offsetWidthUnits = widthUnits % EXCEL_COLUMN_WIDTH_FACTOR; 
	        pixels += Math.floor((float) offsetWidthUnits / ((float) EXCEL_COLUMN_WIDTH_FACTOR / UNIT_OFFSET_LENGTH));   
	        return pixels; 
	    }
	    public static short pixel2WidthUnits(int pxs) {
	        short widthUnits = (short) (EXCEL_COLUMN_WIDTH_FACTOR * (pxs / UNIT_OFFSET_LENGTH)); 
	        widthUnits += UNIT_OFFSET_MAP[(pxs % UNIT_OFFSET_LENGTH)];  
	        return widthUnits; 
	    } 
	    


	%>

	<%


	try{
		

	 	
		String fileName = application.getRealPath("/report/myexcel.xlsx");


		XSSFWorkbook workbook = new XSSFWorkbook(); 
		
		
	    XSSFSheet page1 = workbook.createSheet("Page1");
	    XSSFSheet page2 = workbook.createSheet("Page2");
	    page1.setColumnWidth(0,pixel2WidthUnits((short)43));//A
	    page1.setColumnWidth(1,pixel2WidthUnits((short)47));//B
	    page1.setColumnWidth(2,pixel2WidthUnits((short)36));//C
	    page1.setColumnWidth(3,pixel2WidthUnits((short)36));//D
	    page1.setColumnWidth(4,pixel2WidthUnits((short)40));//E
	    page1.setColumnWidth(5,pixel2WidthUnits((short)34));//F
	    page1.setColumnWidth(6,pixel2WidthUnits((short)31));//G
	    page1.setColumnWidth(7,pixel2WidthUnits((short)32));//H
	    page1.setColumnWidth(8,pixel2WidthUnits((short)44));//I
	    page1.setColumnWidth(9,pixel2WidthUnits((short)31));//J
	    page1.setColumnWidth(10,pixel2WidthUnits((short)38));//K
	    page1.setColumnWidth(11,pixel2WidthUnits((short)82));//L
	    page1.setColumnWidth(12,pixel2WidthUnits((short)101));//M
	   
	    
		XSSFRow row_1 = page1.createRow((short)0);
		XSSFRow row_2 = page1.createRow((short)1);
		XSSFRow row_3 = page1.createRow((short)2);
		XSSFRow row_4 = page1.createRow((short)3);	
		XSSFRow row_5 = page1.createRow((short)4);
		XSSFRow row_6 = page1.createRow((short)5);		
		XSSFRow row_7 = page1.createRow((short)6);		
		XSSFRow row_8 = page1.createRow((short)7);	
		XSSFRow row_9 = page1.createRow((short)8);		
		XSSFRow row_10 = page1.createRow((short)9);	
		XSSFRow row_11 = page1.createRow((short)10);	
		XSSFRow row_12 = page1.createRow((short)11);	
		XSSFRow row_13 = page1.createRow((short)12);	
		XSSFRow row_14 = page1.createRow((short)13);
		XSSFRow row_15 = page1.createRow((short)14);	
		XSSFRow row_16 = page1.createRow((short)15);	
		XSSFRow row_17 = page1.createRow((short)16);
		XSSFRow row_18 = page1.createRow((short)17);
		XSSFRow row_19 = page1.createRow((short)18);
		XSSFRow row_20 = page1.createRow((short)19);
		XSSFRow row_21 = page1.createRow((short)20);
		XSSFRow row_22 = page1.createRow((short)21);
		XSSFRow row_23 = page1.createRow((short)22);
		XSSFRow row_24 = page1.createRow((short)23);
		XSSFRow row_25 = page1.createRow((short)24);
		XSSFRow row_26 = page1.createRow((short)25);
		XSSFRow row_27 = page1.createRow((short)26);
		XSSFRow row_28 = page1.createRow((short)27);
		XSSFRow row_29 = page1.createRow((short)28);
		XSSFRow row_30 = page1.createRow((short)29);
		XSSFRow row_31 = page1.createRow((short)30);
		XSSFRow row_32 = page1.createRow((short)31);
		XSSFRow row_33 = page1.createRow((short)32);
		XSSFRow row_34 = page1.createRow((short)33);
		XSSFRow row_35 = page1.createRow((short)34);
		XSSFRow row_36 = page1.createRow((short)35);
		XSSFRow row_37 = page1.createRow((short)36);
		XSSFRow row_38 = page1.createRow((short)37);
		XSSFRow row_39 = page1.createRow((short)38);
		XSSFRow row_40 = page1.createRow((short)39);
		XSSFRow row_41 = page1.createRow((short)40);
		XSSFRow row_42 = page1.createRow((short)41);
		XSSFRow row_43 = page1.createRow((short)42);
		XSSFRow row_44 = page1.createRow((short)43);
		XSSFRow row_45 = page1.createRow((short)44);
		XSSFRow row_46 = page1.createRow((short)45);
		XSSFRow row_47 = page1.createRow((short)46);
		XSSFRow row_48 = page1.createRow((short)47);
		XSSFRow row_49 = page1.createRow((short)48);
		XSSFRow row_50 = page1.createRow((short)49);
		XSSFRow row_51 = page1.createRow((short)50);
		XSSFRow row_52 = page1.createRow((short)51);
		XSSFRow row_53 = page1.createRow((short)52);
		XSSFRow row_54 = page1.createRow((short)53);
		
		row_1.setHeightInPoints(23.25f);
		row_2.setHeightInPoints(13.50f);
		row_3.setHeightInPoints(13.50f);
		row_4.setHeightInPoints(13.50f);
		row_5.setHeightInPoints(13.50f);
		row_6.setHeightInPoints(13.50f);
		row_7.setHeightInPoints(14.25f);
		row_8.setHeightInPoints(14.25f);
		row_9.setHeightInPoints(14.25f);
		row_10.setHeightInPoints(14.25f);
		row_11.setHeightInPoints(14.25f);
		row_12.setHeightInPoints(21f);
		row_13.setHeightInPoints(11.25f);
		row_14.setHeightInPoints(21f);
		row_15.setHeightInPoints(21f);
		row_16.setHeightInPoints(21f);
		row_17.setHeightInPoints(11.25f);
		row_18.setHeightInPoints(15.75f);
		row_19.setHeightInPoints(15.75f);
		row_20.setHeightInPoints(12.75f);
		row_21.setHeightInPoints(12.75f);
		row_22.setHeightInPoints(12.75f);
		row_23.setHeightInPoints(12.75f);
		row_24.setHeightInPoints(12.75f);
		row_25.setHeightInPoints(12.75f);
		row_26.setHeightInPoints(12.75f);
		row_27.setHeightInPoints(12.75f);
		row_28.setHeightInPoints(12.75f);
		row_29.setHeightInPoints(12.75f);
		row_30.setHeightInPoints(12.75f);
		row_31.setHeightInPoints(12.75f);
		row_32.setHeightInPoints(12.75f);
		row_33.setHeightInPoints(12.75f);
		row_34.setHeightInPoints(12.75f);
		row_35.setHeightInPoints(12.75f);
		row_36.setHeightInPoints(12.75f);
		row_37.setHeightInPoints(12.75f);
		row_38.setHeightInPoints(12.75f);
		row_39.setHeightInPoints(12.75f);
		row_40.setHeightInPoints(12.75f);
		row_41.setHeightInPoints(12.75f);
		row_42.setHeightInPoints(12.75f);
		row_43.setHeightInPoints(12.75f);
		row_44.setHeightInPoints(14.25f);
		row_45.setHeightInPoints(14.25f);
		row_46.setHeightInPoints(14.25f);
		row_47.setHeightInPoints(14.25f);
		row_48.setHeightInPoints(14.25f);
		row_49.setHeightInPoints(14.25f);
		row_50.setHeightInPoints(14.25f);
		row_51.setHeightInPoints(14.25f);
		row_52.setHeightInPoints(14.25f);
		row_53.setHeightInPoints(12.75f);
		row_54.setHeightInPoints(12.75f);
		
		
		XSSFCellStyle style_arial_14 = CreateStyle(workbook,"Arial",14);
		XSSFCellStyle style_arial_13 = CreateStyle(workbook,"Arial",13);
		XSSFCellStyle style_arial_11 = CreateStyle(workbook,"Arial",11);
		XSSFCellStyle style_arial_10 = CreateStyle(workbook,"Arial",10);
		XSSFCellStyle style_arial_8 = CreateStyle(workbook,"Arial",8);
		
		XSSFCellStyle style_border_top_left = CreateCellBorder(workbook,true,false,true,false);
		XSSFCellStyle style_border_top_right = CreateCellBorder(workbook,true,false,false,true);
		XSSFCellStyle style_border_top_left_right = CreateCellBorder(workbook,true,false,true,true);
		XSSFCellStyle style_border_bot_left = CreateCellBorder(workbook,false,true,true,false);
		XSSFCellStyle style_border_bot_right = CreateCellBorder(workbook,false,true,false,true);
		XSSFCellStyle style_border_bot_left_right = CreateCellBorder(workbook,false,true,true,true);
		XSSFCellStyle style_border_top = CreateCellBorder(workbook,true,false,false,false);
		XSSFCellStyle style_border_bot = CreateCellBorder(workbook,false,true,false,false);
		XSSFCellStyle style_border_left = CreateCellBorder(workbook,false,false,true,false);
		XSSFCellStyle style_border_right = CreateCellBorder(workbook,false,false,false,true);
		XSSFCellStyle style_border_left_right = CreateCellBorder(workbook,false,false,true,true);
		XSSFCellStyle style_border_full = CreateCellBorder(workbook,true,true,true,true);
		
		page1.addMergedRegion(new CellRangeAddress(17,17,0,1));
		page1.addMergedRegion(new CellRangeAddress(17,17,2,10));
		page1.addMergedRegion(new CellRangeAddress(18,18,0,1));
		page1.addMergedRegion(new CellRangeAddress(18,18,2,10));
		page1.addMergedRegion(new CellRangeAddress(43,43,0,9));
		page1.addMergedRegion(new CellRangeAddress(43,43,10,11));
		page1.addMergedRegion(new CellRangeAddress(44,44,0,9));
		page1.addMergedRegion(new CellRangeAddress(44,44,10,11));
		page1.addMergedRegion(new CellRangeAddress(45,45,0,9));
		page1.addMergedRegion(new CellRangeAddress(45,45,10,11));
		page1.addMergedRegion(new CellRangeAddress(46,46,11,12));
		page1.addMergedRegion(new CellRangeAddress(47,47,11,12));
		page1.addMergedRegion(new CellRangeAddress(48,48,11,12));
		page1.addMergedRegion(new CellRangeAddress(49,49,11,12));
		page1.addMergedRegion(new CellRangeAddress(50,50,11,12));
		page1.addMergedRegion(new CellRangeAddress(51,51,11,12));
		
		
		row_1.createCell(0).setCellValue("บริษัท อาทิตย์ เคมีภัณฑ์ จำกัด (สำนักงานใหญ่)");	      
		row_1.getCell(0).setCellStyle(style_arial_14);
		row_1.createCell(9).setCellValue("( ต้นฉบับ ) ใบกำกับภาษี  / ใบส่งสินค้า");	      
		row_1.getCell(9).setCellStyle(style_arial_11);
		
		
		
		row_2.createCell(0).setCellValue("ARTHITAYA CHEMICAL CO., LTD.");	      
		row_2.getCell(0).setCellStyle(style_arial_13);	 
		row_2.createCell(9).setCellValue("(เอกสารออกเป็นชุด)");	      
		row_2.getCell(9).setCellStyle(style_arial_10);	
		
		row_4.createCell(0).setCellValue("123 / 3 - 4 - 5   หมู่ 5 ถ.สุขุมวิท ต.บ้านฉาง อ.บ้านฉาง จ.ระยอง 21130 ");	      
		row_4.getCell(0).setCellStyle(style_arial_10);	    
		row_4.createCell(11).setCellValue("บัญชีลูกค้าเลขที");
		row_4.getCell(11).setCellStyle(style_border_top_left);
		row_4.createCell(12);
		row_4.getCell(12).setCellStyle(style_border_top_right);
		
		row_5.createCell(0).setCellValue("โทร (038) 602 068 , 605 039 , 081-647 3802 , 082-716 0156 , แฟ็กซ์ (038) 605 040");	      
		row_5.getCell(0).setCellStyle(style_arial_10);	 
		row_5.createCell(11).setCellValue("ใบสั่งซื้อ");
		row_5.getCell(11).setCellStyle(style_border_top_left);
		row_5.createCell(12).setCellValue("xxxxxxx");
		row_5.getCell(12).setCellStyle(style_border_top_right);
		
		
		row_6.createCell(0).setCellValue("E-Mail address : arthitsho@gmail.com");	      
		row_6.getCell(0).setCellStyle(style_arial_10);	  
		row_6.createCell(11).setCellValue("P/O No.");
		row_6.getCell(11).setCellStyle(style_border_left);
		row_6.createCell(12).setCellValue("");
		row_6.getCell(12).setCellStyle(style_border_right);
		
		row_7.createCell(11).setCellValue("วันส่งของ");
		row_7.getCell(11).setCellStyle(style_border_top_left);
		row_7.createCell(12).setCellValue("XX/XX/XX");
		row_7.getCell(12).setCellStyle(style_border_top_right);
		
		row_8.createCell(11).setCellValue("Date of Delivery");
		row_8.getCell(11).setCellStyle(style_border_left);
		row_8.createCell(12).setCellValue("");
		row_8.getCell(12).setCellStyle(style_border_right);
		
		
		
		row_9.createCell(0).setCellValue("เลขประจำตัวผู้เสียภาษีอากร 0 2 1 5 5 4 5 0 0 0 3 2 0");	      
		row_9.getCell(0).setCellStyle(style_arial_10);	 
		
		row_9.createCell(11).setCellValue("การชำระเงิน");
		row_9.getCell(11).setCellStyle(style_border_top_left);
		row_9.createCell(12).setCellValue("XX วัน");
		row_9.getCell(12).setCellStyle(style_border_top_right);
		
		row_10.createCell(11).setCellValue("Term of Payment");
		row_10.getCell(11).setCellStyle(style_border_left);
		row_10.createCell(12).setCellValue("");
		row_10.getCell(12).setCellStyle(style_border_right);
		
		row_11.createCell(6).setCellValue("เลขที่");	      
		row_11.getCell(6).setCellStyle(style_arial_10);	  
		row_11.createCell(7).setCellValue("59/XXXX");	      
		row_11.getCell(7).setCellStyle(style_arial_14);	  
		
		row_11.createCell(11).setCellValue("ครบกำหนด");
		row_11.getCell(11).setCellStyle(style_border_top_left);
		row_11.createCell(12).setCellValue("XX/XX/XX");
		row_11.getCell(12).setCellStyle(style_border_top_right);
		
		row_12.createCell(11).setCellValue("Due Date");
		row_12.getCell(11).setCellStyle(style_border_bot_left);
		row_12.createCell(12).setCellValue("");
		row_12.getCell(12).setCellStyle(style_border_bot_right);
		
		row_13.createCell(0).setCellValue("ผู้ซื้อ / BUYER");	      
		row_13.getCell(0).setCellStyle(style_arial_8);	 
		
		row_14.createCell(0).setCellValue("ที่อยู่ / ADDRESS");	 
		row_14.getCell(0).setCellStyle(style_arial_8);	
		
		row_18.createCell(0);
		row_18.createCell(1);
		row_18.getCell(0).setCellStyle(style_border_top_left);
		row_18.getCell(1).setCellStyle(style_border_top_right);
		row_18.getCell(0).setCellValue("จำนวน");
		
		
		for(int i = 2 ;i<=10 ;i++ )
		{
			row_18.createCell(i);
			row_18.getCell(i).setCellStyle(style_border_top);
			
		}
		row_18.createCell(11);
		row_18.createCell(12);
		row_18.getCell(11).setCellStyle(style_border_top_left);
		row_18.getCell(12).setCellStyle(style_border_top_left_right);
		row_18.getCell(2).setCellValue("รายการ");
		row_18.getCell(11).setCellValue("หน่วยละ");
		row_18.getCell(12).setCellValue("จำนวนเงิน");
		
		
		
		CellUtil.setAlignment(row_18.getCell(0), workbook, CellStyle.ALIGN_CENTER);
		CellUtil.setAlignment(row_18.getCell(2), workbook, CellStyle.ALIGN_CENTER);
		CellUtil.setAlignment(row_18.getCell(11), workbook, CellStyle.ALIGN_CENTER);
		CellUtil.setAlignment(row_18.getCell(12), workbook, CellStyle.ALIGN_CENTER);
		
		row_19.createCell(0);
		row_19.createCell(1);
		row_19.createCell(2);
		row_19.getCell(0).setCellStyle(style_border_bot_left);
		row_19.getCell(1).setCellStyle(style_border_bot_right);
		for(int j = 2 ;j<=10 ;j++ )
		{
			row_19.createCell(j);
			row_19.getCell(j).setCellStyle(style_border_bot);
			
		}
		row_19.createCell(11);
		row_19.createCell(12);
		row_19.getCell(11).setCellStyle(style_border_bot_left);
		row_19.getCell(12).setCellStyle(style_border_bot_left_right);
		
		row_19.getCell(0).setCellValue("QUANTITY");
		row_19.getCell(2).setCellValue("DESCRIPTION");
		row_19.getCell(11).setCellValue("UNIT PRICE");
		row_19.getCell(12).setCellValue("AMOUNT");

		CellUtil.setAlignment(row_19.getCell(0), workbook, CellStyle.ALIGN_CENTER);
		CellUtil.setAlignment(row_19.getCell(2), workbook, CellStyle.ALIGN_CENTER);
		CellUtil.setAlignment(row_19.getCell(11), workbook, CellStyle.ALIGN_CENTER);
		CellUtil.setAlignment(row_19.getCell(12), workbook, CellStyle.ALIGN_CENTER);

		DrawListCellBorder(workbook,row_20);
		DrawListCellBorder(workbook,row_21);
		DrawListCellBorder(workbook,row_22);
		DrawListCellBorder(workbook,row_23);
		DrawListCellBorder(workbook,row_24);
		DrawListCellBorder(workbook,row_25);
		DrawListCellBorder(workbook,row_26);
		DrawListCellBorder(workbook,row_27);
		DrawListCellBorder(workbook,row_28);
		DrawListCellBorder(workbook,row_29);
		DrawListCellBorder(workbook,row_30);
		DrawListCellBorder(workbook,row_31);
		DrawListCellBorder(workbook,row_32);
		DrawListCellBorder(workbook,row_33);
		DrawListCellBorder(workbook,row_34);
		DrawListCellBorder(workbook,row_35);
		DrawListCellBorder(workbook,row_36);
		DrawListCellBorder(workbook,row_37);
		DrawListCellBorder(workbook,row_38);
		DrawListCellBorder(workbook,row_39);
		DrawListCellBorder(workbook,row_40);
		DrawListCellBorder(workbook,row_41);
		DrawListCellBorder(workbook,row_42);
		DrawListCellBorder(workbook,row_43);
		
		
		
		
		row_44.createCell(0);	

		row_44.getCell(0).setCellStyle(style_border_top_left);
		for(int l=1;l<=12;l++)
		{
			row_44.createCell(l);
			if(l!=10){
				row_44.getCell(l).setCellStyle(style_border_top);
			}
		}
		
		row_44.getCell(10).setCellStyle(style_border_top_left);
		row_44.getCell(12).setCellStyle(style_border_full);
		row_44.getCell(0).setCellValue("จำนวนเงินรวมทั้งสิ้น ( ตัวอักษร )");
		row_44.getCell(10).setCellValue("รวมราคาทั้งสิ้น");
		
		CellUtil.setAlignment(row_44.getCell(0), workbook, CellStyle.ALIGN_CENTER);
		
		row_45.createCell(0);
		row_45.createCell(10);
		row_45.createCell(11);
		row_45.createCell(12);
		row_45.getCell(0).setCellStyle(style_border_left);
		row_45.getCell(10).setCellStyle(style_border_left);
		row_45.getCell(11).setCellStyle(style_border_right);
		row_45.getCell(12).setCellStyle(style_border_full);
		
		row_45.getCell(10).setCellValue("จำนวนภาษีมูลค่าเพิ่ม");		
		
		row_46.createCell(0);
		row_46.createCell(10);
		row_46.createCell(11);
		row_46.createCell(12);
		for(int a=1;a<=9;a++)
		{
			row_46.createCell(a);
			row_46.getCell(a).setCellStyle(style_border_bot);
		}
		row_46.getCell(0).setCellStyle(style_border_bot_left);
		row_46.getCell(10).setCellStyle(style_border_bot_left);
		row_46.getCell(11).setCellStyle(style_border_bot_right);
		row_46.getCell(12).setCellStyle(style_border_full);
		row_46.getCell(10).setCellValue("จำนวนเงินรวมทั้งสิ้น");
		
		row_47.createCell(0);
		row_47.createCell(1);
		row_47.createCell(11);
		row_47.createCell(12);
		row_47.getCell(0).setCellStyle(style_border_left);
		row_47.getCell(11).setCellStyle(style_border_left);
		row_47.getCell(12).setCellStyle(style_border_right);
		row_47.getCell(1).setCellValue("เงินสด");		
		row_47.getCell(11).setCellValue("บริษัท อาทิตย์ เคมีภัณฑ์ จำกัด");
		CellUtil.setAlignment(row_47.getCell(11), workbook, CellStyle.ALIGN_CENTER);
		
		row_48.createCell(0);
		row_48.createCell(1);
		row_48.createCell(11);
		row_48.createCell(12);
		row_48.getCell(0).setCellStyle(style_border_left);
		row_48.getCell(11).setCellStyle(style_border_left);
		row_48.getCell(12).setCellStyle(style_border_right);
		row_48.getCell(1).setCellValue("เช็คธนาคาร / สาขา");
		row_48.getCell(11).setCellValue("ARTHITAYA CHEMICAL CO., LTD.");
		CellUtil.setAlignment(row_48.getCell(11), workbook, CellStyle.ALIGN_CENTER);
		
		row_49.createCell(0);
		row_49.createCell(4);
		row_49.createCell(8);
		row_49.createCell(11);
		row_49.createCell(12);
		row_49.getCell(0).setCellStyle(style_border_left);
		row_49.getCell(11).setCellStyle(style_border_left);
		row_49.getCell(12).setCellStyle(style_border_right);
		row_49.getCell(0).setCellValue("เลขที่เช็ค");
		row_49.getCell(4).setCellValue("ผู้รับสินค้า");
		row_49.getCell(8).setCellValue("วันที่ ");
		
		row_50.createCell(0);
		row_50.createCell(11);
		row_50.createCell(12);
		row_50.getCell(0).setCellStyle(style_border_left);
		row_50.getCell(11).setCellStyle(style_border_left);
		row_50.getCell(12).setCellStyle(style_border_right);
		row_50.getCell(0).setCellValue("วันที่ ");
		
		
		row_51.createCell(0);
		row_51.createCell(4);
		row_51.createCell(8);
		row_51.createCell(11);
		row_51.createCell(12);
		row_51.getCell(0).setCellStyle(style_border_left);
		row_51.getCell(11).setCellStyle(style_border_left);
		row_51.getCell(12).setCellStyle(style_border_right);
		row_51.getCell(0).setCellValue("ผู้รับเงิน");
		row_51.getCell(4).setCellValue("ผู้ส่งสินค้า");
		row_51.getCell(8).setCellValue("วันที่ ");
		row_51.getCell(11).setCellValue("ผู้มีอำนาจลงนาม");
		CellUtil.setAlignment(row_51.getCell(11), workbook, CellStyle.ALIGN_CENTER);
		
		
		row_52.createCell(0);
		row_52.createCell(11);
		row_52.createCell(12);
		for(int b=1;b<=10;b++)
		{
			row_52.createCell(b);
			row_52.getCell(b).setCellStyle(style_border_bot);
			
		}
		row_52.getCell(0).setCellStyle(style_border_bot_left);
		row_52.getCell(11).setCellStyle(style_border_bot_left);
		row_52.getCell(12).setCellStyle(style_border_bot_right);
		row_52.getCell(11).setCellValue("Authorized Signature");
		CellUtil.setAlignment(row_52.getCell(11), workbook, CellStyle.ALIGN_CENTER);
		
		row_53.createCell(0);
		row_53.getCell(0).setCellValue("ใบเสร็จรับเงินฉบับนี้จะสมบูรณ์ต่อเมื่อ ธนาคารได้เรียกเก็บเงินตามเช็คเข้าบัญชีของบริษัทฯเรียบร้อยแล้ว");
		
		row_54.createCell(0);
		row_54.getCell(0).setCellValue("If payment is made by cheque, this receipt will be valid, Until the cheque is honoured by the bank,");
		
		
		workbook.write(new FileOutputStream(fileName));
		
	    out.println("Word document created to : " + application.getRealPath("/report/"));

		
	}
	catch (Exception e) {
		e.printStackTrace();
	}

	%>



    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.jsp">ATCMS Admin</a>
            </div>
            <!-- /.navbar-header -->

   
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="index.jsp"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">Flot Charts</a>
                                </li>
                                <li>
                                    <a href="morris.html">Morris.js Charts</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="tables.html"><i class="fa fa-table fa-fw"></i> Tables</a>
                        </li>
                        <li>
                            <a href="forms.html"><i class="fa fa-edit fa-fw"></i> Forms</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> UI Elements<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="panels-wells.html">Panels and Wells</a>
                                </li>
                                <li>
                                    <a href="buttons.html">Buttons</a>
                                </li>
                                <li>
                                    <a href="notifications.html">Notifications</a>
                                </li>
                                <li>
                                    <a href="typography.html">Typography</a>
                                </li>
                                <li>
                                    <a href="icons.html"> Icons</a>
                                </li>
                                <li>
                                    <a href="grid.html">Grid</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">Second Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Second Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Third Level <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="blank.html">Blank Page</a>
                                </li>
                                <li>
                                    <a href="login.html">Login Page</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
           
		<br><br>

				<div class="container">
				    <div class="row clearfix">
				    	<div class="col-md-12 table-responsive">
							<table class="table table-bordered table-hover table-sortable" id="tab_logic">
								<thead>
									<tr >
										<th class="text-center">
											Product Name
										</th>
										<th class="text-center">
											Addition 
										</th>
										<th class="text-center">
											Price
										</th>
				    					<th class="text-center">
											Quantity
										</th>
				        				<th class="text-center" style="border-top: 1px solid #ffffff; border-right: 1px solid #ffffff;">
										</th>
									</tr>
								</thead>
								<tbody>
				    				<tr id='addr0' data-id="0" class="hidden">
										<td data-name="name">
										    <input type="text" name='pd_name' id = "pd_name" class="form-control"/>
										</td>
										<td data-name="pd_addition">
										    <input type="text" name='pd_addition' id="pd_addition" placeholder='Color , Perfume Code , etc' class="form-control"/>
										</td>
										<td data-name="price">
										    <input type ="text" name = 'price' id="price" class="form-control" >
										</td>
				    					<td data-name="quantity">
										    <input type ="number" name = 'quantity' id="price" class="form-control" >
										</td>
										</td>
				                        <td data-name="del">
				                            <button nam"del0" class='btn btn-danger glyphicon glyphicon-remove row-remove'></button>
				                        </td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<a id="add_row" class="btn btn-default pull-right">Add Row</a>
				</div>
				           
           
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
        
        
        

        $("#add_row").on("click", function() {
            // Dynamic Rows Code
            
            // Get max row id and set new id
            var newid = 0;
            $.each($("#tab_logic tr"), function() {
                if (parseInt($(this).data("id")) > newid) {
                    newid = parseInt($(this).data("id"));
                }
            });
            newid++;
            
            var tr = $("<tr></tr>", {
                id: "addr"+newid,
                "data-id": newid
            });
            
            // loop through each td and create new elements with name of newid
            $.each($("#tab_logic tbody tr:nth(0) td"), function() {
                var cur_td = $(this);
                
                var children = cur_td.children();
                
                // add new td and element if it has a nane
                if ($(this).data("name") != undefined) {
                    var td = $("<td></td>", {
                        "data-name": $(cur_td).data("name")
                    });
                    
                    var c = $(cur_td).find($(children[0]).prop('tagName')).clone().val("");
                    c.attr("name", $(cur_td).data("name") + newid);
                    c.appendTo($(td));
                    td.appendTo($(tr));
                } else {
                    var td = $("<td></td>", {
                        'text': $('#tab_logic tr').length
                    }).appendTo($(tr));
                }
            });
            
            // add delete button and td
            /*
            $("<td></td>").append(
                $("<button class='btn btn-danger glyphicon glyphicon-remove row-remove'></button>")
                    .click(function() {
                        $(this).closest("tr").remove();
                    })
            ).appendTo($(tr));
            */
            
            // add the new row
            $(tr).appendTo($('#tab_logic'));
            
            $(tr).find("td button.row-remove").on("click", function() {
                 $(this).closest("tr").remove();
            });
    });




        // Sortable Code
        var fixHelperModified = function(e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
        
            $helper.children().each(function(index) {
                $(this).width($originals.eq(index).width())
            });
            
            return $helper;
        };
      
        $(".table-sortable tbody").sortable({
            helper: fixHelperModified      
        }).disableSelection();

        $(".table-sortable thead").disableSelection();



        $("#add_row").trigger("click"); 	
    });
    
    </script>
 
</body>

</html>
