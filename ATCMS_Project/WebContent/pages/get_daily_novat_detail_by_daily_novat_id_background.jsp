<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.Locale" %>



<% 

	System.out.println("Start get_daily_novat_detail_by_daily_novat_id_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

	List<DailyNovatDetail> result_list = new ArrayList<DailyNovatDetail>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	
	String daily_novat_id = request.getParameter("daily_novat_id");
	
      try{
    	  
    	  String sql_query  = " SELECT * "+
    			 			  " FROM daily_novat_detail "+
    						  " JOIN product "+
							  " ON  daily_novat_detail.product_id = product.product_id"+
	    			          " WHERE BINARY daily_novat_detail.daily_novat_id = '"+daily_novat_id+"'";
    	  
    	  System.out.println("sql_query_dnd:"+sql_query);
    	  ResultSet rs_dnd = connect.createStatement().executeQuery(sql_query);
          
    	
    	  
    	  while(rs_dnd.next())
          {
    		  DailyNovatDetail dnd = new DailyNovatDetail();
 			  

    	//	  String bill_date = rs_dnd.getString("bill_date");
    	//	  Date date = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH).parse(bill_date_old_str);	  
    	//	  String bill_date_new_str =  new SimpleDateFormat("dd MMMM yyyy",Locale.ENGLISH).format(date);
    		  
    		
    		  dnd.setDailyNovatId(rs_dnd.getString("daily_novat_id"));
    		  dnd.setPrice(rs_dnd.getString("price"));
    		  dnd.setQuantity(rs_dnd.getString("quantity"));
    		  dnd.setSum(rs_dnd.getString("sum"));
    		  dnd.setIndex(rs_dnd.getString("index"));
    		  dnd.setProductId(rs_dnd.getString("product_id"));
    		  dnd.setCostPerUnit(rs_dnd.getString("cost_per_unit"));
    		  dnd.setProfitPerUnitI(rs_dnd.getString("profit_per_unit"));
    		  dnd.setSumCost(rs_dnd.getString("sum_cost"));
    		  dnd.setSumProfit(rs_dnd.getString("sum_profit"));
    		  if(("-").equals(rs_dnd.getString("name_th")))
    		  {
    				dnd.setProductName(rs_dnd.getString("name_en"));
    			  
    		  }else{
    			  dnd.setProductName(rs_dnd.getString("name_th"));
    		  }
    		  
    		  
    		  if(("-").equals(rs_dnd.getString("unit_th")))
    		  {
    				dnd.setUnit(rs_dnd.getString("unit_en"));
    			  
    		  }else{
    			 	dnd.setUnit(rs_dnd.getString("unit_th"));
    		  }
    		  
    		  
    		  
    		  result_list.add(dnd);
              
          }
             
         	try {
				Json = mapper.writeValueAsString(result_list); 
				System.out.println("Json_dnd"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	out.print(Json);
	   
	 
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  connect.close();
    	  out.print("fail");
      }
     
    System.out.println("SUCCESS");
	connect.close();
%>
