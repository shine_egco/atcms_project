<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "com.atcms.*" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "java.util.Properties" %>
<%@ page import = "javax.servlet.ServletContext" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>


<% 

	System.out.println("Start fetch_product_data_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
	///////////////////file_path parameter/////////////
	String product_id = request.getParameter("product_id");
	System.out.println("product_id:"+product_id);
	 ///////////////////////////////////////////////////
	List<Product> product_list = new ArrayList<Product>();
	String Json="";
	ObjectMapper mapper = new ObjectMapper();
	try {		
		/*
		String sql_query = " SELECT * "+
				   " FROM `company` "+
				   " WHERE type = 'customer' "+
				   " AND company_id = '"+company_id+"'";
		*/
		
		String sql_query =" select product.product_id "+
						  "       , product.name_th "+
						  "       , product.name_en "+
						  " 	  , product.name_pool "+
						  "		  , product.unit_th "+
						  "		  , product.unit_en "+
						  " 	  , order_detail.price "+
					      " from product "+
				          " left join order_detail "+
			 			  "	on  product.product_id = order_detail.product_id "+
						  " where BINARY product.product_id = '"+product_id+"'";

	
		ResultSet rs =  connect.createStatement().executeQuery(sql_query);
		System.out.println(sql_query);	
		
			
		
			while(rs.next()) {
				Product pro = new Product();
					//	pro.setNameTH(rs.getString("name_th"));
					//	pro.setNameEN(rs.getString("name_en"));
					//	pro.setUnitTH(rs.getString("unit_th"));
					//	pro.setUnitEN(rs.getString("unit_en"));
					
					
					  // need many condition test 
					  if(rs.getString("name_th").equals("-")||rs.getString("name_th").equals(""))
		              {
		            	  pro.setDisplayName(rs.getString("name_en"));
		            	  pro.setDisplayUnit(rs.getString("unit_en"));
		            	  pro.setProductLanFlag("EN");
		            	  
		              }else{
		            	  pro.setDisplayName(rs.getString("name_th"));
		            	  pro.setDisplayUnit(rs.getString("unit_th"));
		            	  pro.setProductLanFlag("TH");
		            	  
		              }

						pro.setProductID(rs.getString("product_id"));
						// ****** waiting for implement many price ******** //
						pro.setPricePool(rs.getString("price"));
				

				product_list.add(pro);
				
				//if(product_list.size()==10) break;
			}
			

		
	 	System.out.println("product_list size :"+product_list.size());
		try {
			Json = mapper.writeValueAsString(product_list); 
			
		} catch (JsonGenerationException ex) {
			ex.printStackTrace();
		} catch (JsonMappingException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		System.out.println("Json_product:"+Json);
		out.print(Json);
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	 
	connect.close();
%>
