<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "com.atcms.*" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "java.util.Properties" %>
<%@ page import = "javax.servlet.ServletContext" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.util.Date" %>



<% 

	System.out.println("Start create_work_order_full_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
	///////////////////file_path parameter/////////////
	String manufac_date = request.getParameter("manufac_date");
	String batch_number = request.getParameter("batch_number");
	String total_sum_cost = request.getParameter("total_sum_cost");
	
	int index = Integer.parseInt(request.getParameter("index"));
		
	System.out.println("manufac_date:"+manufac_date);
	System.out.println("batch_number:"+batch_number);
	System.out.println("total_sum_cost:"+total_sum_cost);

	
	 ///////////////////////////////////////////////////
//	List<Product> product_list = new ArrayList<Product>();

	ObjectMapper mapper = new ObjectMapper();

	
		
		try{

			String workorder_id ="WO"+ KeyGen.generateWorkOrderID();


			String sql_query =" INSERT INTO `workorder_main`(`workorder_id`"+
													   ", `manu_date`"+
													   ", `batch_number`"+
													   ", `total_cost`) " +
							   " VALUES ('"+workorder_id+"'"+
										",'"+manufac_date+"'" +
									    ",'"+batch_number+"'"+
										",'"+total_sum_cost+"')";
	
							   
			System.out.println("sql_query:"+sql_query);
			try{
				connect.createStatement().executeUpdate(sql_query);
				String 	pd_id ;
				String  pd_cost ;
				String  pd_quantity ;
			    String  pd_sum ;
			    String  pd_formula_id;
			    

				for(int i=0;i<index;i++)
				{
					
					pd_id = request.getParameter("pd_id"+i);
					pd_cost = request.getParameter("pd_cost"+i);
					pd_quantity = request.getParameter("pd_quantity"+i);
				    pd_sum = request.getParameter("pd_sum"+i);
				    pd_formula_id = request.getParameter("pd_formula_id"+i);
				    
				    
				    
				    String sql_order_detail = " INSERT INTO `workorder_detail`(`workorder_id`"+
												", `product_id`"+
												", `formula_id`"+
												", `quantity`"+
												", `unit_cost`"+
												", `sum_cost`)"+
						" VALUES ('"+workorder_id+"'"+
								  ",'"+pd_id+"'"+
							      ",'"+pd_formula_id+"'"+
								  ",'"+pd_quantity+"'"+
								  ",'"+pd_cost+"'"+
								  ",'"+pd_sum+"')";
				    
				    connect.createStatement().executeUpdate(sql_order_detail);
				    System.out.println("Success:"+i+":"+sql_order_detail);
	
				    
				};
				
				out.print(workorder_id);
			
			}catch(Exception x){
			
				x.printStackTrace();
				connect.close();
				out.print("error");
			}
			

			
			
		}catch(Exception o){
			o.printStackTrace();
			connect.close();
			out.print("error");
		}
	
		

	connect.close();
%>
