<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../bower_components/sidebar/sidebar_style_3.css" rel="stylesheet" type="text/css"> 


 <script>
	
	 function formatMoney(number, places, symbol, thousand, decimal) {
			number = number || 0;
			places = !isNaN(places = Math.abs(places)) ? places : 0;
			symbol = symbol !== undefined ? symbol : "";
			thousand = thousand || ",";
			decimal = decimal || ".";
			var negative = number < 0 ? "-" : "",
			    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
			    j = (j = i.length) > 3 ? j % 3 : 0;
			return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
	}
	
	

	
	 function numberWithCommas(x) {
		    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	 
	 function fetch_vendor_detail(){
		 
		 var vendor_id = sessionStorage.getItem("vendor_id_for_get_detail");

		  var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				

				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
					
					var jsonObj = JSON.parse(xmlhttp.responseText);
					//alert(xmlhttp.responseText);
					var vendor_id =  document.getElementById("vendor_id");
					var name_th = document.getElementById("name_th");
					var address_th_line_1 = document.getElementById("address_th_line1");
					var address_th_line_2 = document.getElementById("address_th_line2");
					var name_en = document.getElementById("name_en");
					var address_en_line_1 = document.getElementById("address_en_line1");
					var address_en_line_2 = document.getElementById("address_en_line2");
					var tax_id = document.getElementById("tax_id");
					var credit =  document.getElementById("credit");
					
					
					customer_id.value = jsonObj[0].companyId;
					name_th.value = jsonObj[0].nameTH;
					address_th_line_1.value = "-";
					address_th_line_2.value = "-";
					name_en.value = jsonObj[0].nameEN;
					address_en_line_1.value = "-";
					address_en_line_2.value = "-";
					tax_id.value = jsonObj[0].taxID;
					credit.value = jsonObj[0].credit;
					
				
				}// end if check state
				
			}// end function
			
			
			xmlhttp.open("POST", "get_vendor_detail_by_vendor_id_backdround.jsp?vendor_id="+vendor_id, true);
			xmlhttp.send();
		 
	 }
	 
	 function fetch_purchase_history(){
		 
			console.log("Start get_purchase_main");
			 var vendor_id = sessionStorage.getItem("vendor_id_for_get_detail");
			 console.log("Start vendor_id:"+vendor_id);
			  var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
			 	xmlhttp.onreadystatechange = function() {

					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
						
							console.log("Back with :"+xmlhttp.responseTex);
		
							var jsonObj = JSON.parse(xmlhttp.responseText);
		
							console.log("Inv Num return:"+jsonObj.length);
		
							if(jsonObj.length == 0)
							{
		
								
							}else{
								
								for(i in jsonObj) {
									
									$('#dataTable_purchase_main_history').DataTable().row.add([
			                           '<tr><td><center>'+jsonObj[i].invNo+'</center></td>'
			                           ,'<td><center>'+jsonObj[i].invoiceDate+'</center></td>'
			                           ,'<td><center>'+jsonObj[i].totalValue+'</center></td>'
			                           ,'<td><center>'+jsonObj[i].totalVat+'</center></td>'
			                           ,'<td><center>'+jsonObj[i].totalIncVat+'</center></td>'
			                           ,'<td><center><button id = "'+jsonObj[i].purchaseId+'" type="button" class="btn btn-info btn-circle btn-md" onclick = "redirect_purchase_bill_detail(this.id)" data-toggle="modal" data-target="#myModal">'
			                           +'<i class="glyphicon glyphicon-search"></i></button></center></td></tr>'	                               
			                         ]).draw();
										
													
								}
								
											
							}// end if check state
						
						}// end function
				
			 	}
				xmlhttp.open("POST", "get_purchase_main_by_vendor_id_background.jsp?vendor_id="+vendor_id, true);
				xmlhttp.send();
			 
			 
		 }
	 
	 function fetch_product_relate_vendor_history(){
		 
			console.log("Start fetch_product_relate_vendor_history");
			 var vendor_id = sessionStorage.getItem("vendor_id_for_get_detail");
			 console.log("Start vendor_id:"+vendor_id);
			  var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
			 	xmlhttp.onreadystatechange = function() {

					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
						
							console.log("Back with :"+xmlhttp.responseTex);
		
							var jsonObj = JSON.parse(xmlhttp.responseText);
		
							console.log("Inv Num return:"+jsonObj.length);
		
							if(jsonObj.length == 0)
							{
		
								
							}else{
								
								for(i in jsonObj) {
									
									$('#dataTables_product_his').DataTable().row.add([
			                           '<tr><td><center>'+jsonObj[i].productId+'</center></td>'
			                           ,'<td><center>'+jsonObj[i].invNo+'</center></td>'
			                           ,'<td><center>'+jsonObj[i].invDate+'</center></td>'
			                           ,'<td><center>'+jsonObj[i].nameTH+'</center></td>'
			                           ,'<td><center>'+jsonObj[i].unitTH+'</center></td>'
			                           ,'<td><center>'+jsonObj[i].nameEN+'</center></td>'
			                           ,'<td><center>'+jsonObj[i].unitEN+'</center></td>'
			                           ,'<td><center>'+jsonObj[i].price+'</center></td>'
			                           ,'<td><center>'+jsonObj[i].quantity+'</center></td>'
			                           ,'<td><center><button id = "'+jsonObj[i].purchaseId+'" type="button" class="btn btn-info btn-circle btn-md" onclick = "redirect_cash_bill_detail(this.id)" data-toggle="modal" data-target="#myModal">'
			                           +'<i class="glyphicon glyphicon-search"></i></button></center></td></tr>'	                               
			                         ]).draw();
										
													
								}
								
											
							}// end if check state
						
						}// end function
				
			 	}
				xmlhttp.open("POST", "get_product_relate_vendor_history_background.jsp?vendor_id="+vendor_id, true);
				xmlhttp.send();
			 
			 
		 }
	 
		function redirect_purchase_bill_detail(purchase_id){
			
			sessionStorage.setItem("purchase_id_for_get_detail", purchase_id); 
			
			window.open("report_purchase_bill_detail.jsp");
			
		}
	
</script>
<style>
	.text-right {
 		 text-align:right;
	}
	/*    --------------------------------------------------
	:: General
	-------------------------------------------------- */

	.content h1 {
		text-align: center;
	}
	.content .content-footer p {
		color: #6d6d6d;
	    font-size: 12px;
	    text-align: center;
	}
	.content .content-footer p a {
		color: inherit;
		font-weight: bold;
	}
	
	/*	--------------------------------------------------
		:: Table Filter
		-------------------------------------------------- */
	.panel {
		border: 1px solid #ddd;
		background-color: #fcfcfc;
	}

	.panel .btn-group .btn {
		transition: background-color .3s ease;
	}
	.table-filter {
		background-color: #fff;
		border-bottom: 1px solid #eee;
	}
	.table-filter tbody tr:hover {
		cursor: pointer;
		background-color: #eee;
	}
	.table-filter tbody tr td {
		padding: 10px;
		vertical-align: middle;
		border-top-color: #eee;
	}
	.table-filter tbody tr.selected td {
		background-color: #eee;
	}
	.table-filter tr td:first-child {
		width: 3px;
	}
	.table-filter tr td:nth-child(2) {
		width: 3px;
	}

	.table-filter .star {
		color: #ccc;
		text-align: center;
		display: block;
	}
	.table-filter .star.star-checked {
		color: #F0AD4E;
	}
	.table-filter .star:hover {
		color: #ccc;
	}
	.table-filter .star.star-checked:hover {
		color: #F0AD4E;
	}

	.table-filter .media-meta {
		font-size: 13px;
		color: #999;
	}
	.table-filter .media .title {
		color: #2BBCDE;
		font-size: 16px;
		font-weight: bold;
		line-height: normal;
		margin: 1px;
	}
	.table-filter .media .title span {
		font-size: .8em;
		margin-right: px;
	}

	.table-filter .media .summary {
		font-size: 14px;
	}	
	.scrollit {
    	overflow-x: hidden;
    	overflow-y: auto;
   	 	height:300px;
   	 	
   	  
	}

</style>
    

</head>

<body>

    <div id="wrapper">
    		<nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>
                <div class="sidebar-header">
                    <h3>Bored Sidebar</h3>
                </div>
                
    
        		<div id="includedSidebar"></div>
        		
        	</nav>



        <!-- Navigation -->
       

        <div id="content" class="form-control">
        
        	   <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-justify"></i>
                                <span></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Page</a></li>
                       
                            </ul>
                        </div>
                    </div>
                </nav>
        
             <div class="row">
             <br>
	           <div class="col-lg-4">
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                          Customer Detail
	                        </div>
	                        
	                        <!-- /.panel-heading -->
	                        <div class="panel-body">
	                        	<input name="customer_id" id="customer_id" type="text" class="form-control" placeholder="Customer ID" readonly>
                                <br>	
	                        	<label>Name TH (ชื่อภาษาไทย)</label>
	                           	<input id="name_th" name = "name_th" class="form-control" value=""   >
	                           	<br>
	         
	         					<label>Address TH Line1</label>
	                           	<input id="address_th_line1" name = "address_th_line1" class="form-control" value=""   >
	                           	<br>
	                           	<input id="address_th_line2" name = "address_th_line2" class="form-control" value=""   >
	                           	<br>
	                           		
	                           	
	                           	<br>
	                           	<label>Name EN (ชื่อภาษาอังกฤษ)</label>
	                           	<input id="name_en" name = "name_en" class="form-control" value=""   >
	                           	<br>
                         	  	<label>Address EN Line1</label>
	                           	<input id="address_en_line1" name = "address_en_line1" class="form-control" value=""   >
	                           	<br>
	                           	<input id="address_en_line2" name = "address_en_line2" class="form-control" value=""   >
	                           	<br>
	                           		
	                        	<br>
	                        	<table>
	                        		  	<tr class="pagination-centered">
                      			 		
                      			 			<td style="padding-left:2em;">
                        						<label>Credit  </label>	                        						
                        					</td>
                        					<td style="padding-left:1em;">
                        					 	<input type="text" id="credit" name ="credit" class="form-control">  					                        					
                        					</td>	
                        					
               

                        	   			</tr>
	                        	</table>
	                        	<br>
	                        	<label>Tax ID</label>
	                        	 <input id="tax_id" name = "tax_id" class="form-control" value=""   >
	                           	<br>
	                           		
	                      
	                
	                        		<hr>

	                        		<br>
	                            	
												
													                            
	                        </div>
	                        <!-- .panel-body -->
	                    </div>
                    <!-- /.panel -->
                </div>
                 <div class="col-lg-8">
								<div class="row">
											<div class="panel panel-default">
												<div class="panel-body">
												
												
															<table class="table table-striped table-bordered table-hover" id="dataTable_purchase_main_history">
									                                    <thead>
									                                        <tr>	
									                                        
									                                       		 <th style="width: 90px;" >Inv No.</th> 
									                                             <th>Invoice Date</th>
									                                             <th>Total Value</th>
									                                             <th>Total Vat  </th>
									                                             <th> Total Inc Vat </th>
									                                            <th style="width:20px;"></th>
									                                            
									                                        </tr>
									                                    </thead>
									                                    <tbody>
									                                    
									                                    </tbody>
									                                </table>
									                                
							                               
														
													
												</div>
											</div>
									
								
									
								</div>
						
                    <!-- /.panel -->
               		 </div>
                </div>
                
            <div class="row">
              		<a class="btn btn-primary  js-scroll-trigger form-control" href="#section_product_his">Scoll Down</a>
           </div>
           
           <section class="container-fluid" id="#section_product_his">
				  <div class="col-lg-12 center">
				  	<br>
				  	<br>
						<div class="row">
									<table class="table table-striped table-bordered table-hover" id="dataTables_product_his">
	                                    <thead>
	                                        <tr>	
	                                        
	                                       		 <th style="width: 90px;" >Product ID.</th> 
	                                       		 <th style="width: 40px;"> Inv No. </th>
	                                       		 <th style="width: 80px;"> Inv Date. </th>
	           
	                                             <th>Name TH</th>
	                                             <th style="width: 80px;">Unit TH</th>
												 <th>Name EN</th>
	                                             <th style="width: 80px;">Unit EN</th>
	                                             <th style="width: 120px;">Price</th>
	                                             <th style="width: 120px;">Quantity</th>
	                                             <th style="width: 20px;"></th>
	                                            
	                                        </tr>
	                                    </thead>
	                                    <tbody>
	                                    
	                                    </tbody>
	                                </table>
						</div>
						
				  </div>
			</section>
           
           
                
                
 
                

                <!-- /.col-lg-12 -->
             </div>
      

        
        </div>
        <!-- /#page-wrapper -->

  
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
     <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    
    
    <script>
    $(document).ready(function() {

    	
    	$("#includedSidebar").load("sidebar.jsp"); 
    	
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').fadeOut();
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        
        
    	
    	
    	fetch_vendor_detail();
    	fetch_purchase_history();
    	fetch_product_relate_vendor_history();

    	
    	  $('#dataTable_purchase_main_history').DataTable({
              responsive: true,
              "aLengthMenu": [[25, 50, 75, -1], [1, 3, 5]],
              "iDisplayLength": 10 ,
              lengthChange: false 
              ,
              "sDom": 'T<"clear">lfrtip' ,
              "oTableTools": {
                      "aButtons": [
  	                                   {
  	                                       "sExtends":    "text",
  	                                       "fnClick": function ( nButton, oConfig, oFlash ) {
  	                                          	show_modal_create_summary_cash_report();
  	                                       },
  	                                       "sButtonText": "<i class='glyphicon glyphicon-list-alt'></i>",
  	                                        "sExtraData": [ 
  	                                                            { "name":"operation", "value":"downloadcsv" }       
  	                                                      ]
  	                                    
  	                                   }
                     			    ]
                  }

     	    });
    	  
    	  
    	  
 
 
    	
    });
    </script>

</body>

</html>
