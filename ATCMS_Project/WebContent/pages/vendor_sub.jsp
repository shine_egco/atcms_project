<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="../bower_components/sidebar/sidebar_style_3.css" rel="stylesheet" type="text/css">
  
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <% 
    	System.out.println("Start vendor_sub !!!!");
  		//set Database Connection
  		String hostProps = "";
  		String usernameProps  = "";
  		String passwordProps  = "";
  		String databaseProps = "";
  		
  		try {
  			ServletContext servletContext = request.getSession().getServletContext();
  			
  			InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
  			Properties props = new Properties();
  			
  			props.load(input);

  			hostProps  = props.getProperty("host");
  			usernameProps  = props.getProperty("username");
  			passwordProps  = props.getProperty("password");
  			databaseProps = props.getProperty("database");
  		} catch (Exception e) { 
  			out.println(e);  
  		}
  	
  		// connect database
  		Statement statement = null;
		Connection connect = null;	
		
		List<Vendor> vendor_list = new ArrayList<Vendor>();
  		try {
				Class.forName("com.mysql.jdbc.Driver");
	
				connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
						"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");//////"&characterEncoding=tis620" Encoding Thai);
	
				if(connect != null){
				   System.out.println("Database Connect Sucesses.");
				   
				   
			 	   ResultSet rs = connect.createStatement().executeQuery(" SELECT * FROM `company` WHERE type = 'vendor' ");
			 	   
				 	  while(rs.next())
					  {
				 		   Vendor ven = new Vendor();
				 		  ven.setCompanyId(rs.getString("company_id"));
				 		  ven.setCodeName(rs.getString("code_name"));
				 		  ven.setAddress(rs.getString("address"));
				 		  ven.setAddressTH(rs.getString("address_th"));
				 		  ven.setAddressEN(rs.getString("address_en"));
				 		  ven.setNameTH(rs.getString("name_th"));
				 		  ven.setNameEN(rs.getString("name_en"));
				 		  ven.setCodeName(rs.getString("code_name"));
				 		  ven.setTaxID(rs.getString("tax_id"));
				 		  ven.setCompanyId(rs.getString("company_id"));
				 		   
				 		   vendor_list.add(ven);
					  }
	
				  } else {
					  
			       System.out.println("Database Connect Failed.");	
			       
			      }

		} catch (Exception e) {
			out.println(e.getMessage());
			e.printStackTrace();
		}
    	
    %>
    
 <script>
	function edit_data(company_id)
	{
		//alert(company_id);
		if(company_id != "") {
				
				/* AJAX */
				var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
						var jsonObj = JSON.parse(xmlhttp.responseText);
						
						if(jsonObj.length == 0) {
	
						//	alert("no  Data");
						}
						else{
						//	alert("get Data");
								var cus_company_id = document.getElementById("cus_company_id");
								var cus_name_th = document.getElementById("cus_name_th");
								var cus_add_th = document.getElementById("cus_add_th");
								var cus_name_en = document.getElementById("cus_name_en");
								var cus_add_en = document.getElementById("cus_add_en");
								var cus_tax_id = document.getElementById("cus_tax_id");
								var cus_credit = document.getElementById("cus_credit");
								var cus_email = document.getElementById("cus_email");	
								
								cus_company_id.value = jsonObj[0].companyId;
								cus_name_th.value = jsonObj[0].nameTH;
								cus_add_th.value = jsonObj[0].addressTH; 		
								cus_name_en.value = jsonObj[0].nameEN;
								cus_add_en.value = jsonObj[0].addressEN; 	
								cus_tax_id.value = jsonObj[0].taxID;
								cus_credit.value = parseInt(jsonObj[0].credit);
								cus_email.value = jsonObj[0].email;
							
						}
					}
					
				}
				
				xmlhttp.open("POST", "fetch_cus_data_background.jsp?company_id="+company_id, true);
				xmlhttp.send();
			}
		
		
	
			
		
	}
	function submit_change_customer()
	{
		
		var cus_company_id = document.getElementById("cus_company_id").value;
		var cus_name_th = document.getElementById("cus_name_th").value;
		var cus_add_th = document.getElementById("cus_add_th").value;
		var cus_name_en = document.getElementById("cus_name_en").value;
		var cus_add_en = document.getElementById("cus_add_en").value;
		var cus_tax_id = document.getElementById("cus_tax_id").value;
		var cus_credit = document.getElementById("cus_credit").value;
		var cus_email = document.getElementById("cus_email").value;	
		
		var parameter = "company_id="+cus_company_id+
		    "&name_th="+cus_name_th+
		    "&name_en="+cus_name_en+
		    "&address_th="+cus_add_th+
		    "&address_en="+cus_add_en+
		    "&tax_id="+cus_tax_id+
		    "&credit="+cus_credit+
		    "&email="+cus_email;

		
		alert("Company ID :"+cus_company_id+"\n"
			 +"Name TH : "+cus_name_th+"\n"
			 +"Address TH : "+cus_add_th+"\n"
			 +"Name EN : "+cus_name_en+"\n"
			 +"Address EN : "+cus_add_en+"\n"
			 +"Tax ID : "+cus_tax_id+"\n"
			 +"Credit : "+cus_credit+"\n"
			 +"Email : "+cus_email);
		
		
	
			/* AJAX */
			var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
					var result = xmlhttp.responseText;
					
					if(result=="success")
					{
						$('#myModal').modal('hide');
						location.reload();
						
					}else{
						alert("Error Pls Contact Admin")
					}
				}
				
			}
			
			xmlhttp.open("POST", "edit_single_customer_background.jsp?"+parameter, true);
			xmlhttp.send();
		
		
	
	}
	
	
	function redirect_vendor_detail(id){
		
		sessionStorage.setItem("vendor_id_for_get_detail", id); 
		
		window.open("report_vendor_detail.jsp");
	}
	
	
</script>
    
    

</head>

<body>

    <div id="wrapper">

        <!-- Navigation --><nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>
                <div class="sidebar-header">
                    <h3>Bored Sidebar</h3>
                </div>
                
    
        		<div id="includedSidebar"></div>
        		
        	</nav>



        <!-- Navigation -->
       

        <div id="content" class="form-control">
        
        	   <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-justify"></i>
                                <span></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Page</a></li>
                       
                            </ul>
                        </div>
                    </div>
                </nav>
                
             <div class="row">
	                <div class="col-lg-12">
	                    <h1 class="page-header">Customers</h1>
	                </div>
                <!-- /.col-lg-12 -->
             </div>
			
             <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		                     <div class="modal-dialog">
		                         <div class="modal-content">
		                             <div class="modal-header">
		                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                                 <h4 class="modal-title" id="myModalLabel">Modal title</h4>
		                             </div>
		                             <div class="modal-body">
		                               	<input id="cus_company_id" name="cus_company_id" class="form-control" disabled >
		                               	<br>
		                               	<input id="cus_name_th" name = "cus_name_th" class="form-control" value=""   >
		                               	<br>
		                               	<textarea name="cus_add_th" id="cus_add_th" class="form-control" rows="2" placeholder="ที่อยู่(ภาษาไทย)" ></textarea>
		                                <br>
		                                <input id="cus_name_en" name = "cus_name_en" class="form-control" value=""   >
		                             	<br>
		                             	 <textarea name="cus_add_en" id="cus_add_en" class="form-control" rows="2" placeholder="ที่อยู่(ภาษาอังกฤษ)" ></textarea>
		                            	<br>
		                            	<input name="cus_tax_id" id="cus_tax_id" type="text" class="form-control" placeholder="เลขประจำตัวผู้เสียภาษี">
		                            	<br>
		                            	<input type="number" name="cus_credit" id="cus_credit" class="form-control" placeholder="Credit (Day)" >
		                            	<br>
		                            	<input type="email" name="cus_email" id="cus_email" class="form-control" placeholder="info@atc.co.th">
		                             </div>
		                             <div class="modal-footer">
		                                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                                 <button type="button" id="submit_change_cus_btn" onclick="submit_change_customer()" name="submit_change_customer" class="btn btn-primary">Save changes</button>
		                             </div>
		                         </div>
		                         <!-- /.modal-content -->
		                     </div>
		                     <!-- /.modal-dialog -->
		     </div>
                                        	
			
             <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            DataTables
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                        	<th>Company ID</th>
                                            <th>Name TH</th>
                                            <th>Name EN</th>
                                            <th>Code Name</th>
                                            <th style="width:1%"></th>
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        
                                        <%
                                        for(int i=0 ; i<vendor_list.size();i++)
                                        {
                                        %>
                                        <tr>	
                                        	<td> <%= vendor_list.get(i).getCompanyId()%> </td>
                                        	<td> <%= vendor_list.get(i).getNameTH()%> </td>
                                        	<td> <%= vendor_list.get(i).getNameEN()%> </td>
                                        	<td> <%= vendor_list.get(i).getCodeName()%> </td>
                                        	<td> 
                                        
	                                        	<button id = "<%=vendor_list.get(i).getCompanyId() %>" type="button" class="btn btn-info btn-circle btn-md" onclick = "redirect_vendor_detail(this.id)">
	                                        		<i class="glyphicon glyphicon-search"></i>            				
	                            				</button>
	  
                                        	</td>
                                        </tr>
                                     	<% 
                                        }
                                        
                                        %>
                                       
                                        
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        
        

        
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
                <!-- jQuery Custom Scroller CDN -->
     <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
    	
 	$("#includedSidebar").load("sidebar.jsp"); 
    	
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').fadeOut();
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        
        
        
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
	<script>
		
  	    // tooltip demo
	    $('.tooltip-demo').tooltip({
	        selector: "[data-toggle=tooltip]",
	        container: "body"
	    })
	
	    // popover demo
	    $("[data-toggle=popover]")
	        .popover()
	        
    </script>
</body>

</html>
