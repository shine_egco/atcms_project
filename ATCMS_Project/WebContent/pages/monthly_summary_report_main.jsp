<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@ page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
     <link href="../dist/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
     <link href="../dist/css/morris.css" rel="stylesheet" type="text/css">

    <link href="../bower_components/sidebar/sidebar_style_3.css" rel="stylesheet" type="text/css"> 

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

 <script>

	
	function set_monthly_summary_report_to_table(){
		
//		  var  table = $('#dataTables-example-product').DataTable();
		//	       table.clear();
		
			  var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
					//	alert("I'm Back");//////////////////
						
				
						var jsonObj = JSON.parse(xmlhttp.responseText);
					
						//alert("I'm Back");
						
						if(jsonObj.length == 0) {
						//	massage();
							alert("Error Occer Can't get all customer list");
						}
						else{
						//	alert(jsonObj.length);
								
							for(i in jsonObj) {
							
								$('#dataTables-example-month').DataTable().row.add([
		                           '<tr><td><center>'+jsonObj[i].index+'</center></td>'
		                           ,'<td><center>'+getMonthString(jsonObj[i].month)+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].year+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].status+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].dateOfValidation+'</center></td>'
		                           ,'<td><center><button id = "'+jsonObj[i].reportID+'" type="button" class="btn btn-info btn-circle btn-md" onclick = "redirect_monthly_summary_report_detail(this.id)" data-toggle="modal" data-target="#myModal">'
		                           +'<i class="glyphicon glyphicon-search"></i></button></center></td></tr>'	                               
		                         ]).draw();
							}
							
						}
						
						
						//System.out.println("temp_x:"+temp_x);	
					
					
					}// end if check state
				}// end function
				
		
				xmlhttp.open("POST", "get_all_monthly_summary_report_background.jsp", true);
				xmlhttp.send();
		
	}
	
	function getMonthString(temp){
			
		var month = "";
				switch (temp) {
				
			    case "1":
			    	month = "January";
			        break;
			    case "2":
			    	month = "February";
			        break;
			    case "3":
			    	month = "March";
			        break;
			    case "4":
			    	month = "April";
			        break;
			    case "5":
			    	month = "May";
			        break;
			    case "6":
			    	month = "June";
			    	break;
			    case "7":
			    	month = "July";
			    	break;
			    case "8":
			    	month = "August";
			    	break;
			    case "9":
			    	month = "September";
			    	break;
			    case "10":
			    	month = "October";
			    	break;
			    case "11":
			    	month = "November";
			    	break;
			    case "12":
			    	month = "December";
			    	break;
			}
				
			return month;
		
	}
	
	function show_modal_create_report(){
		
		$('#create_new_month_report_modal').modal('show');

	}
	
	function create_monthly_summary_report(){
		
			var month = 	$('#select_month option:selected').attr('value');
			var year = 	$('#select_year option:selected').attr('value');
		
			alert("month:"+month+" , year:"+year);
	
	
			  var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					
					$('#create_new_month_report_modal').modal('hide');
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
						
						alert(xmlhttp.responseText);
					
					}// end if check state
					
				}// end function
				
		
				xmlhttp.open("POST", "create_monthly_summary_report.jsp?month="+month+"&year="+year, true);
				xmlhttp.send();
		
		
	}
	
	function redirect_monthly_summary_report_detail(report_id){
		
		//	alert(cash_bill_id);
			sessionStorage.setItem("report_id_for_get_detail", report_id); 
			
			window.open("monthly_summary_report_detail.jsp");
		}
		
	
</script>
    
    

</head>

<body>

    <div id="wrapper">

    		<nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>
                <div class="sidebar-header">
                    <h3>Bored Sidebar</h3>
                </div>
                
    
        		<div id="includedSidebar"></div>
        		
        	</nav>



        <!-- Navigation -->
       

        <div id="content" class="form-control">
        
        	   <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-justify"></i>
                                <span></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Page</a></li>
                       
                            </ul>
                        </div>
                    </div>
                </nav>
             <div class="row">
	                <div class="col-lg-12">
	                    <h2 class="page-header">Monthly Summary Report List</h2>
	                </div>
                <!-- /.col-lg-12 -->
             </div>
	
                                        	
			
             <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           	Month List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        	<div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example-month">
                                    <thead>
                                        <tr>	
                                        
                                        <th>Index</th> 
                                             <th>Month</th>
                                            <th>Year</th>
                                             <th>Status</th>
                                             <th>Date Of Validation</th>
                                       		<th></th>
                                       		
                                          
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        
                           
                                       
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        
        

        
        </div>
        <!-- /#page-wrapper -->
           <div class="modal fade" id="create_new_month_report_modal" tabindex="1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style=" overflow-y: auto;" >
          		<div class="modal-dialog" style="width:15%;">
          			 <div class="modal-content">

                            <div class="modal-body">
                            
									<div class="col-center-block">
		                           			<br>
		                            		<div class="form-group">
													  <label for="select_month">Month:</label>
													  <select class="form-control" id="select_month">
													    <option value="1">January</option>
													    <option value="2">February</option>
													    <option value="3">March</option>
													    <option value="4">April</option>
													    <option value="5">May</option>
													    <option value="6">June</option>
													    <option value="7">July</option>
													    <option value="8">August</option>
													    <option value="9">September</option>
													    <option value="10">October</option>
													    <option value="11">November</option>
													    <option value="12">December</option>
													  </select>
											</div>     
											<br>                       
		                  					<div class="form-group">
													  <label for="select_year">Year:</label>
													  <select class="form-control" id="select_year">
													    <option value="2016">2016</option>
													    <option value="2017">2017</option>
													    <option value="2018">2018</option>
													    <option value="2019">2019</option>
													    <option value="2020">2020</option>
													    <option value="2021">2021</option>
													    <option value="2022">2022</option>
													    <option value="2023">2023</option>
													    <option value="2024">2024</option>
													    <option value="2025">2025</option>
													    <option value="2026">2026</option>
													    
													  </select>
											</div>              
		                            
		                            </div>
			                         <div style="margin: auto;">
			                            <br>
		                            
	          			 			 	<button type="button" name="bot_init_credit_inv_no"  id="bot_init_credit_inv_no" class="btn btn-primary pull-right" onclick="create_monthly_summary_report()">Submit</button>
		                            	<br>
		                            	 <br>
		                              </div>
                            </div>
                     
                          

          			</div>
          		
        	  </div>  
          </div>         
        

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
   
	
    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script src="../dist/js/dataTables.tableTools.js"></script>
     <script src="../dist/js/raphael-min.js"></script>
     <script src="../dist/js/morris.min.js"></script>
     <script src="../dist/js/chart.js"></script>
   <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
    	

    	
    	$("#includedSidebar").load("sidebar.jsp"); 
    	
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').fadeOut();
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        
    	
    	
    	var table_cus = $('#dataTables-example-month').DataTable({
    	      responsive: true,
              "aLengthMenu": [[25, 50, 75, -1], [1, 3, 5]],
              "iDisplayLength": 10 ,
              lengthChange: false 
              ,
              "sDom": 'T<"clear">lfrtip' ,
              "oTableTools": {
                      "aButtons": [
  	                                   {
  	                                       "sExtends":    "text",
  	                                       "fnClick": function ( nButton, oConfig, oFlash ) {
  	                                    	 show_modal_create_report();
  	                                       },
  	                                       "sButtonText": "<i class='fa glyphicon-plus'></i>",
  	                                        "sExtraData": [ 
  	                                                            { "name":"operation", "value":"downloadcsv" }       
  	                                                      ]
  	                                    
  	                                   }
                     			 ]
                  }
             
        });
        set_monthly_summary_report_to_table();
        test();
    });
    </script>

</body>

</html>
