<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

 <script>
	 
	 
	 function show_confirm_delete(id){
		
 		$('#confirm-delete-order').modal('show');
 		//alert(id);
		  document.getElementById("delete_bill_id").value = id;

		 
	 }
	
	 function formatMoney(number, places, symbol, thousand, decimal) {
			number = number || 0;
			places = !isNaN(places = Math.abs(places)) ? places : 0;
			symbol = symbol !== undefined ? symbol : "";
			thousand = thousand || ",";
			decimal = decimal || ".";
			var negative = number < 0 ? "-" : "",
			    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
			    j = (j = i.length) > 3 ? j % 3 : 0;
			return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
	}

	 function create_material(){
		 		 
		 var material_id =  document.getElementById("material_id");
		 var name_en =  document.getElementById("name_en");
		 var name_th =  document.getElementById("name_th");
		 var material_code = document.getElementById("material_code");
		 var description =  document.getElementById("description");
		 var remark =  document.getElementById("material_remark");
	     var density = document.getElementById("density");
	     var concentration = document.getElementById("concentration");
	     
	     
	     var capacity_but = document.getElementById("capacity_but");
	     var capacity_unit_value = capacity_but.name;
	 	 var packaging_but =   document.getElementById("packaging_but");
	 	 var packaging_unit_value = packaging_but.name;
		 var total_cost =  document.getElementById("total_cost");
	
			
		 
		 var parameter_main = "material_id="+material_id.value
		 						  +"&name_th="+name_th.value
		 						  +"&name_en="+name_en.value
		 						  +"&material_code="+material_code.value
		 						  +"&description="+description.value
		 						 +"&remark="+remark.value
		 						  +"&capacity_unit="+capacity_unit_value.toLowerCase()
		 						  +"&capacity_value="+capacity_input.value
		 						  +"&packaging_unit="+packaging_unit_value.toLowerCase()
		 						  +"&total_cost="+total_cost.value
		 						  +"&density="+density.value
		 						  +"&concentration="+concentration.value;
		 					

	 alert("parameter_main:"+parameter_main);
		
		
		 var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
					if(xmlhttp.responseText=="fail")
					{
						alert("Create Error");
					}else{
						
						alert("Success");
						
						
						location.reload();
					}
					
				}// end if check state
			}// end function
			
		
			xmlhttp.open("POST", "create_material_full_background.jsp?"+parameter_main, true);
			xmlhttp.send();
			
		 
		 
	 }
	 
	 function onchange_capacity_unit_ul(temp){
			
			var click_li_value = temp.innerHTML;
			var capacity_but =   document.getElementById("capacity_but");
				  capacity_but.innerHTML = click_li_value+"<span class='caret'></span>";
				  capacity_but.name = click_li_value.toLowerCase();
				 // alert(capacity_but.name);
				  
		  
	 }
	 
	 function onchange_packaging_unit_ul(temp)
	 {
			var click_li_value = temp.innerHTML;
			var packaging_but =   document.getElementById("packaging_but");
			   	packaging_but.innerHTML = click_li_value+"<span class='caret'></span>";
			   	packaging_but.name = click_li_value.toLowerCase();
		 
	 }
	 
 function generateMaterial_ID(){
		 
		 
		 var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
									
							if(xmlhttp.responseText=="fail")
							{
								
							}else{
								
								var jsonObj = JSON.parse(xmlhttp.responseText);
								
								var batch_number =   document.getElementById("material_id");
									   batch_number.value = jsonObj.temp_code;
						
					
								
								
								
							}
				
				}// end if check state
			}// end function
			
	
			xmlhttp.open("POST", "generate_long_encoding_background.jsp", true);
			xmlhttp.send();
		 
		 
	 }
	 
	 
</script>
<style>
	.text-right {
 		 text-align:right;
	}
	

</style>
    

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
     		   <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.jsp">ATCMS Admin</a>
            </div>
            <!-- /.navbar-header -->

   
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="dashboard_main.jsp"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-group fa-fw"></i>Customer&Vendor<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="customer_sub.jsp">Customers</a>
                                </li>
                                <li>
                                    <a href="vendor_sub.jsp">Vendors</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                         <li>
                            <a href="#"><i class="fa fa-leaf"></i>  Product<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="atc_product_sub.jsp">ATC Product</a>
                                </li>
                                <li>
                                    <a href="other_product_sub.jsp">Other Product</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o"></i>  Bill Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="direct_bill_sub.jsp">Direct Bill</a>
                                </li>
                                <li>
                                    <a href="indirect_bill_sub.jsp">Indirect Bill</a>
                                </li>
                                <li>
                                    <a href="credit_inv_sub.jsp">Credit Invoice</a>
                                </li>
                                <li>
                                    <a href="cash_inv_sub.jsp">Cash Invoice</a>
                                </li>
                                <li>
                                    <a href="credit_note_sub.jsp">Credit Note (ใบลดหนี้)</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                        	<a href="#"><i class="glyphicon glyphicon-fire"></i> Production (การผลิต)<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">                         
                               <li>
                                    <a href="production_material_main.jsp">Material</a>
                                </li>
                                <li>
                                    <a href="production_work_order_main.jsp">Work Order</a>
                                </li>
                                <li>
                                    <a href="production_formula_main.jsp">Formula</a>
                                </li>
                                 <li>
                                    <a href="production_product_relate_formula.jsp">Product Relate Formula</a>
                                </li>                
                            </ul>
                        </li>
                        <li>
                        	<a href="#"><i class="glyphicon glyphicon-stats"></i> Reporting (รายงาน)<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="monthly_summary_report_main.jsp">Monthly Summary Report</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                        	<a href="#"><i class="glyphicon glyphicon-road"></i> (DEVEL)0pinG Z()Ne <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="dev_show_profit_each_bill_detail.jsp">Each  Bill Value Detail</a>
                                </li>
                                <li>
                                    <a href="dev_balancing_work_order.jsp">Balancing Work Order</a>
                                </li>
                
                            </ul>
                        </li>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
             <div class="row">
	                <div class="col-lg-12">
	                    <h1 class="page-header"></h1>
	                </div>
                <!-- /.col-lg-12 -->
             </div>
                    

         
         	 <div class="row">
         	
	                   
	                  <div class="col-lg-6" >
		                    <div class="panel panel-default">
		                        <div class="panel-heading">
		                            New Material
		                        </div>
		                        <div class="panel-body">
		                        	<label>Material ID : </label>
	                           		 <input id="material_id"  name = "material_id" class="form-control" value=""  readonly>
	                           		 <br>
	                           		 <div class="row">
		                           		 <div class ="col-lg-4">
		                           		 	 <label>Name TH : </label>
		                           			 <input id="name_th" name = "name_th" class="form-control"  >
		                           		 </div>
		                           		 <div class ="col-lg-4">
		                           		 	 <label>Name EN : </label>
		                           			 <input id="name_en" name = "name_en" class="form-control"  >
		                           		 </div>
		                           		 <div class ="col-lg-4">
		                           		 		 <label>Material Code : </label>
		                           				  <input id="material_code" name = "material_code" class="form-control" value=""  >
		                           		 </div>
		                           		 <br>
		                           		 <br>
		                           		 <br>
		                           	</div>
		                           	
	                           		 <label>Remark : </label>
	                           		 <input id="material_remark" name = "material_remark" class="form-control" value="" >
	                           		 <br>
	                           		
	                        	<label>Description : </label>
	                        		 <textarea id="description" name ="description" class="form-control" rows="3" placeholder=""></textarea>
	                        	 	 <br>	
	                        	<div  class="row">
		                           		 <div class ="col-lg-4">
		                           		 	 <label>Density : </label>
		                           			 <input id="density" name = "density" class="form-control"  >
		                           		 </div>
		                           		 	 <div class ="col-lg-4">
		                           		 	 <label>Concentration : </label>
		                           			 <input id="concentration" name = "concentration" class="form-control"  >
		                           		 </div>
		                           		 <br>
		                           		 <br>
		                           	</div>
	                        	 	 
	                        	 	 
	                      			 <hr>

	                      			 
	                      			<div class="row">
												  <div class="col-lg-3">
													    <div class="input-group">
													       <input type="text" class="form-control"  id="capacity_input">
															      <div class="input-group-btn">
															        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"  id="capacity_but" name="lite" aria-haspopup="true" aria-expanded="false">Lite<span class="caret"></span></button>
															        <ul id="capacity_ul" class="dropdown-menu">
															            <li data-value="CC"><a onclick="onchange_capacity_unit_ul(this)" href="#">CC</a></li>
						                                                <li data-value="Lite"><a onclick="onchange_capacity_unit_ul(this)"  href="#">Lite</a></li>
						                                                <li data-value="Kg"><a onclick="onchange_capacity_unit_ul(this)" href="#">Kg</a></li>
						                                                 <li data-value="g"><a onclick="onchange_capacity_unit_ul(this)" href="#">g</a></li>
															        </ul>
															      </div><!-- /btn-group -->
													    </div><!-- /input-group -->
												  </div><!-- /.col-lg-6 -->
	
						                          <div class="col-lg-2">
									                            <div class="input-group">
									                     	     <span class="input-group-addon">/</span>
											                              <div class="input-group-btn">
											                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"  id="packaging_but" name="pail" aria-haspopup="true" aria-expanded="false">Pail<span class="caret"></span></button>
											                                <ul id="packaging_ul" class="dropdown-menu">
											                                 <li data-value="Pail"><a  onclick="onchange_packaging_unit_ul(this)" href="#">Pail</a></li>
						                                                      <li data-value="Gallon"><a onclick="onchange_packaging_unit_ul(this)"  href="#">Gallon</a></li>
						                                                      <li data-value="Bag"><a  onclick="onchange_packaging_unit_ul(this)"  href="#">Bag</a></li>
						                                                      <li data-value="Bot"><a  onclick="onchange_packaging_unit_ul(this)"  href="#">Bot</a></li>
											                                </ul>
											                              </div><!-- /btn-group -->
									                            </div><!-- /input-group -->					                            
						                          </div><!-- /.col-lg-6 -->																							  
												  <br>
												  <br>
												  <br>
										</div><!-- /.row -->
	                      			
	                      			<table>
	                      			 		<tr class="pagination-centered">
	                      			 
	                        					<td>
	                        					</td>
	                      			 		
	                      			 			<td>
	                        						<label style="padding-left:5em;">Total Cost/Unit : </label>                      						
	                        					</td>
	                        					<td style="padding-left:1em;">
	                        					 	<input type="text" name='total_cost' id = "total_cost" class="form-control text-right"/>
	                        					</td>
	                        					
	                        	   			</tr>
	                        	   
	                        
	                        		</table>
	                      		
		                        
		                        		                    
		          				<div class="row" >
		          					
										<br>
										<div class="pull-right"  style="padding-right:2em ;">
												<button type="button" class="form-control btn btn-primary" style="width:200px;" onclick="create_material()">Save</button>
										</div>
								</div>
								
	                       	    </div>
	                       	    
	                       	    
		                    </div>
		                  
         	 		  </div>
         	 
         	 </div>
        

        
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    
    
    <script>
    $(document).ready(function() {
    	
        $('#dataTables-example').DataTable({
                responsive: true
        });
        

        generateMaterial_ID();

    });
    </script>
	<script>
    // tooltip demo
	    $('.tooltip-demo').tooltip({
	        selector: "[data-toggle=tooltip]",
	        container: "body"
	    })
	
	    // popover demo
	    $("[data-toggle=popover]")
	        .popover()
    </script>
</body>

</html>
