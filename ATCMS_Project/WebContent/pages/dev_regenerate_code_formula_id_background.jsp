<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.util.concurrent.TimeUnit" %>
<%@ page import = "java.sql.PreparedStatement" %>


<% 

	System.out.println("Start dev_regenerate_code_formula_id_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
  

      try{
    	   KeyGen key = new KeyGen();
    	
    	//   System.out.println("key_str:"+key_str);
    	   
    	   
    	   String sql_formula_main = " SELECT * "+
    	   											" FROM formula_main "+
    			   									" WHERE 1";
    	   ResultSet rs_main = connect.createStatement().executeQuery(sql_formula_main);
    	   
    	   int count = 0;
    	   
    	   while(rs_main.next())
           {
    		   
	    		   String old_formula_id = rs_main.getString("formula_id");
	    		   System.out.println(count+":"+old_formula_id);
	    		   
	    		   String new_formula_id = "FORM"+key.generateFormulaID();
	    		   TimeUnit.MILLISECONDS.sleep(100);
	    		   System.out.println("new-> :"+new_formula_id);
	    		   

    		       int count_2 = 0;
    		   
    	
    			//g   System.out.println("    "+count_2+":"+rs_main.getString("formula_id"));
    			   
    			   String regen_product = " UPDATE product "+
								" SET product.formula_id = '"+new_formula_id+"' "+
								" WHERE BINARY product.formula_id = '"+old_formula_id+"' ";
    			   // Active
    			   connect.createStatement().executeUpdate(regen_product);
    			   
    			   String regen_formula_main = "  UPDATE formula_main "+
							" SET formula_main.formula_id = '"+new_formula_id+"' "+
							" WHERE BINARY formula_main.formula_id =  '"+old_formula_id+"' ";
    			   // Active
    			   connect.createStatement().executeUpdate(regen_formula_main); 
    			   
    		   count++;
    		   
           }
    	  
    	
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  connect.close();
    	  out.print("fail");
      }
     
    System.out.println("REGENERATE ORDER_ID SUCCESS");
    out.print("success");
	connect.close();
%>
