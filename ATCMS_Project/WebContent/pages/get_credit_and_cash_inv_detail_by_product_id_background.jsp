<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.Locale" %>



<% 

	System.out.println("Start get_credit_and_cash_inv_detail_by_product_id_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

	List<CreditAndCashDetail> result_list = new ArrayList<CreditAndCashDetail>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	
	String product_id = request.getParameter("product_id");
	
      try{
    	  
    	  String sql_query  = " SELECT  RE.SELL_ID , RE.PRICE , RE.product_id "+
    	  							 ", RE.INV_DATE , RE.COM_TH , RE.COM_EN "+
    	  							 ", RE.UNIT_TH , RE.UNIT_EN , RE.INV_NO "+
    	  							 ", RE.QUANTITY , RE.SUM "+
   				 			  " FROM ( "+
  				  				  "    SELECT order_detail.order_id  AS SELL_ID "+
    				    		       "    , order_detail.price       AS PRICE "+
	    				    		   "    , order_detail.product_id AS product_id "+
    				    			   "    , order_main.delivery_date AS INV_DATE "+
	    				    		   "    , order_main.inv_no AS INV_NO "+
    				    			   "    , company.name_th AS COM_TH "+
    				    			   "    , company.name_en AS COM_EN "+
    				    			   "    , product.unit_th AS UNIT_TH "+
    				    			   "    , product.unit_en AS UNIT_EN "+
    				    			   "    , order_detail.quantity AS QUANTITY "+
    				    			   "    , order_detail.sum AS SUM "+
    				      		 	 " FROM order_detail "+
    				    	         " JOIN order_main "+
    						         " ON  order_detail.order_id = order_main.order_id "+
    				    	         " JOIN company "+
									 " ON  order_main.customer_id = company.company_id "+
    				    	         " JOIN product "+
									 " ON  product.product_id = order_detail.product_id "+
    				                 " UNION "+
    				                 " SELECT cash_bill_detail.cash_bill_id  AS SELL_ID "+
    				     			      " , cash_bill_detail.price  AS PRICE "+
    				    			      " , cash_bill_detail.product_id AS product_id "+
    				    			      " , cash_bill_main.bill_date AS INV_DATE "+
    				    			      " , cash_bill_main.inv_no AS INV_NO "+
    				    			      " , company.name_th AS COM_TH "+
										  " , company.name_en AS COM_EN "+ 
										  " , product.unit_th AS UNIT_TH "+
			    				          " , product.unit_en AS UNIT_EN "+
			    				          " , cash_bill_detail.quantity AS QUANTITY "+
			    				    	  " , cash_bill_detail.sum AS SUM "+
    				        		 " FROM cash_bill_detail  "+
    				       		     " JOIN cash_bill_main  "+
    				    			 " ON  cash_bill_detail.cash_bill_id = cash_bill_main.cash_bill_id  "+
    				    			 " JOIN company "+
    								 " ON  cash_bill_main.customer_id = company.company_id "+
    							     " JOIN product "+
    								 " ON  product.product_id = cash_bill_detail.product_id "+
    				        		" ) AS RE "+
    						 " WHERE BINARY  RE.product_id = '"+product_id+"'" +
    					     " AND RE.SELL_ID != 'INIT_ORDER_ID' "+
    						 " ORDER BY RE.INV_DATE ";
    	  
    	  System.out.println("sql_query_ccd:"+sql_query);
    	  ResultSet rs_ccd = connect.createStatement().executeQuery(sql_query);
          
    	
    	  
    	  while(rs_ccd.next())
          {
    		  CreditAndCashDetail ccd = new CreditAndCashDetail();
 			  
    		  ccd.setSellId(rs_ccd.getString("SELL_ID"));
    		  ccd.setPrice(rs_ccd.getString("PRICE"));

    		  String inv_date_old_str = rs_ccd.getString("INV_DATE");
    		  Date date = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH).parse(inv_date_old_str);	  
    		  String inv_date_new_str =  new SimpleDateFormat("dd MMMM yyyy",Locale.ENGLISH).format(date);
    		  
    		  ccd.setInvDate(inv_date_new_str);
    		  ccd.setInvNo(rs_ccd.getString("INV_NO"));
    		  ccd.setQuantity(rs_ccd.getString("QUANTITY"));
    		  ccd.setSum(rs_ccd.getString("SUM"));
    		  
    		  if(("-").equals(rs_ccd.getString("COM_EN")))
    		  {
    				ccd.setCustomerName(rs_ccd.getString("COM_TH"));	
    			  
    		  }else{
    				ccd.setCustomerName(rs_ccd.getString("COM_EN"));	
    		  }
    		  
    		  if(("-").equals(rs_ccd.getString("UNIT_EN")))
    		  {
    				ccd.setProductUnit(rs_ccd.getString("UNIT_TH"));	
    			  
    		  }else{
    				ccd.setProductUnit(rs_ccd.getString("UNIT_EN"));	
    		  }
    		  
    		  String sell_id_code = ccd.getSellId().substring(0,2);
    		  if(sell_id_code.equals("CB"))
    		  {
    			  ccd.setType("CASH");
    		  }else if(sell_id_code.equals("OR"))
    		  {
    			  ccd.setType("CREDIT");
    		  }else{
    			  ccd.setType("ERROR");  
    		  }
    		  
    		  System.out.println("sell_id_code:"+sell_id_code);
    		  
    		//  ccd.setType(")
    		  
   			  
    		  
    		  
    		  result_list.add(ccd);
              
          }
             
         	try {
				Json = mapper.writeValueAsString(result_list); 
				System.out.println("Json_ccd:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	out.print(Json);
	   
	 
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  connect.close();
    	  out.print("fail");
      }
     
    System.out.println("SUCCESS");
	connect.close();
%>
