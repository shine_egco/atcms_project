<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>


<% 

	System.out.println("Start temp_migration_other_product_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
	///////////////////file_path parameter/////////////
	String file_pathx = request.getParameter("file_path");
	System.out.println("File_path background:"+file_pathx);
	 ///////////////////////////////////////////////////
	 
	 ///////////////////init parameter ///////////////
	 int i = 0;
     int num_row = 2;
     int num_col  = 0;
     int count = 0;
     int count_product_added = 0;
     int count_order_detail_added = 0;
     List<String> fail_list = new ArrayList<String>();
     String[] header_array = new  String[]{"name_th","name_en","name_pool","unit_en","unit_th","code_name","category","group_code","init_price"};

	
     System.out.println("Header_array size : "+ header_array.length);
     ////////////////////////////////////////////////
     /////////////////Data Parameter////////////////
     String pcode_name = "";
     String name_th = "" ;
     String name_en = "" ; 
     String name_pool = "";
     String unit_en = "";
     String unit_th = "";
     String category = "";
     String group_code = "";
     String init_price = "";
     ///////////////////////////////////////////////
     
     
     
     
     try{
    	 
   		FileInputStream file = new FileInputStream(new File(file_pathx));
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		 
         //Get first/desired sheet from the workbook
         XSSFSheet sheet = workbook.getSheetAt(0);
         //Iterate through each rows one by one
         Iterator<Row> rowIterator = sheet.iterator();
         
         Iterator<Row> headerIt_row_check = sheet.iterator();
         Row row_check = headerIt_row_check.next();
         Iterator<Cell> headerIt_cell_check = row_check.cellIterator();
         while (rowIterator.hasNext())
         {
        	 
        	 if(count==0)
             {
         		while(headerIt_cell_check.hasNext()){
         			Cell header_cell = headerIt_cell_check.next();
         			   switch (header_cell.getCellType())
                        {
                            case Cell.CELL_TYPE_NUMERIC:
                                break;
                            case Cell.CELL_TYPE_STRING:
                               // System.out.print(header_cell.getStringCellValue()+ " ");
                               	 System.out.println(header_array[i]+":"+header_cell.getStringCellValue());
                                	 if(header_array[i].equals(header_cell.getStringCellValue()))
                                	 {
                                		 System.out.println("Pass:" + i);
                                	 }
                                	 else{
                           
                                		throw new Exception();           		
                                	 }
                                	 
                                break;
                        }// end switch
         				i++;
         		}// end headerIt_cell_check.hasNext()
         		System.out.println("");
         		count = count +1;
         		rowIterator.next();
         		System.out.println("////////// All Header Matched ////////////");        		        		
         	 }// end if(count==0)
        	 
         	try{
         		System.out.println("Row:"+num_row);
         		Row row = rowIterator.next();
         		Iterator<Cell> cellIterator = row.cellIterator();
         	    while (cellIterator.hasNext())
                {
                    Cell cell = cellIterator.next();
                    switch(num_col){
	                	case 0 :
	                		name_th = cell.getStringCellValue();
	      					name_th.replaceAll("\\s+","");
	      					System.out.println("name_th:"+name_th);
	           			break;
						
	                	case 1 :
	                		name_en = cell.getStringCellValue();
	      					name_en.replaceAll("\\s+","");
	      					System.out.println("name_en:"+name_en);
	                	break;
	                	
	                	case 2 :
	                		name_pool = cell.getStringCellValue();
	      					name_pool.replaceAll("\\s+","");
	      					System.out.println("name_pool:"+name_pool);

	                	break;
	                	
	                	case 3 :
	                		unit_en = cell.getStringCellValue();
	      					unit_en.replaceAll("\\s+","");
	      					System.out.println("unit_en:"+unit_en);
	                	
	                	break;
	                	case 4 :
	                		unit_th = cell.getStringCellValue();
	      					unit_th.replaceAll("\\s+","");
	      					System.out.println("unit_th:"+unit_th);
	                	
	                	break;
	                	case 5 :
	                		pcode_name = cell.getStringCellValue();
	      					pcode_name.replaceAll("\\s+","");
	      					System.out.println("pcode_name:"+pcode_name);
	                	break;
	                	case 6 :
	                		category = cell.getStringCellValue();
	      					category.replaceAll("\\s+","");
	      					System.out.println("category:"+category);
	                	break;
	                	case 7:
	                		group_code = cell.getStringCellValue();
	      					group_code.replaceAll("\\s+","");
	      					System.out.println("group_code:"+group_code);
	                	break;
	                	case 8 :
	                		try{
	                			init_price = cell.getStringCellValue();
		      					init_price.replaceAll("\\s+","");
		      					System.out.println("init_price case1:"+init_price);
		      					
		                		
		                	}
	                		catch(Exception x){
	                			
		      					init_price = String.valueOf(cell.getNumericCellValue());
		      					init_price.replaceAll("\\s+","");
		      					System.out.println("init_price case2:"+init_price);
	                		}
	                	break;
                    }// end switch case 
                    num_col++;
         	    	
                }// end while(cellIterator.hasNext())
         	    num_col=0;
                num_row++;
                
                ///////////////Process Add to Database each row /////////////////
                String product_id ="OTH"+ KeyGen.genTimeStamp();
                String description = "-";
     
             	String sql = " INSERT INTO `product`(`product_id` "+
             									  ", `name_th` "+
             									  ", `name_en` "+
             									  ", `description`"+
             									  ", `name_pool`"+
             									  ", `unit_en`"+
             									  ", `unit_th`"+
             									  ", `pcode_name`"+
             									  ", `category`"+
             									  ", `group_code`)"+
             				" VALUES ('"+product_id+"'"+
             						 ",'"+name_th+"'"+
             						 ",'"+name_en+"'"+
             						 ",'"+description +"'"+
             						 ",'"+name_pool+"'"+
             					     ",'"+unit_en+"'"+
             						 ",'"+unit_th+"'"+
             					     ",'"+pcode_name+"'"+
             						 ",'"+category+"'"+
             					     ",'"+group_code+"')"; 
                
				System.out.println("sql:"+sql);
				
	
							
				PreparedStatement pstmt = connect.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);  
				pstmt.executeUpdate();  
				ResultSet keys = pstmt.getGeneratedKeys();    
				keys.next();  
				int key = 0;
				key = keys.getInt(1);
				System.out.println("Adding Success with product inddex:"+key);
				count_product_added ++ ;
				if(key!=0)
				{
					////////////////////Initial order data//////////////////////////////
					/*
					String order_id = "INIT_ORDER_ID";
					String order_type = "VAT";
					String order_po = "INIT_ORDER_PO";
					Date order_date = new Date();
					
					String sql_order = " INSERT INTO `order`("+
															 " `order_id`"+
															 ", `type`"+
															 ", `order_date`"+
															 ", `ponum`)"+
									   " VALUES ('"+order_id+"'"+
										 		 ",'"+order_type+"'"+
									   			 ",'"+order_date+"'"+
										 		 ",'"+order_po+"')";

					System.out.println("sql_order:"+sql_order);
					connect.createStatement().executeUpdate(sql_order);
					*/
					///////////////////////////////////////////////////////////////////////
					//Inital order_detail data
					String order_id = "INIT_ORDER_ID";
					String order_type = "VAT";
					int quantity = 1;
					
					String sql_order_detail = "INSERT INTO `order_detail`(" +
																		  "  `order_id`"+
																		  ", `product_id`"+
																		  ", `price`"+
																		  ", `type` "+
																		  ", `quantity`"+
																		  ", `sum`)"+
											  " VALUES ('"+order_id+"'"+
													   ",'"+product_id+"'"+
													   ",'"+init_price+"'"+
													   ",'"+order_type+"'"+
													   ", "+quantity+" "+
													   ",'"+init_price+"')";
																		  	
					System.out.println("sql_order_detail:"+sql_order_detail);
					connect.createStatement().executeUpdate(sql_order_detail);
					count_order_detail_added ++;
					

				}
				else
				{
					
					System.out.println("Error when initial order date ");
				}
				
				
				
				
				
				
				
                /////////////////////////////////////////////////////////////////
               
         	}catch(Exception getData){
         		System.out.println("Error cant't read Excel:"+getData);
                
        		num_col = 0;
        		fail_list.add(num_row+1+"");
        		num_row++;
         		
         	}
            
         }// end while (rowIterator.hasNext())
     }//end try fileinputstream
		catch(Exception x){
			System.out.println(x);
			connect.close();
			out.print("fail");
			
     }
     System.out.println("//////////RESULT/////////");
     System.out.println("Add to Product:"+count_product_added);
     System.out.println("Add to Order Detail:"+count_order_detail_added);
    out.println("success");
	connect.close();
%>
