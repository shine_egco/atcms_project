<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>
<%@ page import = "java.text.DateFormat" %>



<% 

	System.out.println("Start get_cash_inv_history_by_customer_id_background");
			//set Database Connection
			String hostProps = "";
			String usernameProps  = "";
			String passwordProps  = "";
			String databaseProps = "";
			
			try {
				//get current path
				ServletContext servletContext = request.getSession().getServletContext();
				
				InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
				Properties props = new Properties();
				
				props.load(input);
			 
				hostProps  = props.getProperty("host"); 
				usernameProps  = props.getProperty("username");
				passwordProps  = props.getProperty("password");
				databaseProps = props.getProperty("database");
				
				System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
						"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
				
			} catch (Exception e) { 
				out.println(e);  
			}
			
			// connect database
			Connection connect = null;		
			try {
				Class.forName("com.mysql.jdbc.Driver");
			
				connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
						"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
			
				if(connect != null){
					System.out.println("Database Connect Sucesses."); 
				} else {
					System.out.println("Database Connect Failed.");	
				}
			
			} catch (Exception e) {
				out.println(e.getMessage());
				e.printStackTrace();
			}

		
	List<CashBill> cash_inv_list = new ArrayList<CashBill>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	//String purchase_id = request.getParameter("purchase_id");
	//System.out.println("purchase_id:"+purchase_id);
	 String customer_id = request.getParameter("customer_id");

	 System.out.println("customer_id:"+customer_id);



      try{
    	  
    
    	
       		String	sql_query =  " SELECT * "+
    						"  FROM cash_bill_main "+
    						"  JOIN company "+
    						"  ON  cash_bill_main.customer_id = company.company_id "+
    						"  WHERE BINARY company.company_id ='"+customer_id+"'" +
    						"  ORDER BY cash_bill_main.inv_no ASC ";
  
   			 
   			 

    
    	  System.out.println("sql_query:"+sql_query);
    	  
    	  
    	  ResultSet rs_cash = connect.createStatement().executeQuery(sql_query);
          
    	  
    	 
    	  
    	  while(rs_cash.next())
          {
    		  CashBill cash_bill = new CashBill();
              		cash_bill.setInvNo(rs_cash.getString("inv_no"));
         	  if(rs_cash.getString("name_th").equals("-"))
              {
         			cash_bill.setCustomerName(rs_cash.getString("name_en"));
         			cash_bill.setCustomerAddress(rs_cash.getString("address_en"));
         	  }else{
         		    cash_bill.setCustomerName(rs_cash.getString("name_th"));
         		    cash_bill.setCustomerAddress(rs_cash.getString("address_th"));
         	  }
              		cash_bill.setTotalIncVat(rs_cash.getString("total_inc_vat"));
              		cash_bill.setCompletedDate(rs_cash.getString("completed_date"));
              		cash_bill.setCreateDate(rs_cash.getString("create_date"));      
         	  		cash_bill.setBillDate(rs_cash.getString("bill_date"));
       				cash_bill.setIncGeneratedDate(rs_cash.getString("inv_generated_datetime"));
       				cash_bill.setInvFileName(rs_cash.getString("inv_file_path"));
       				cash_bill.setInvNo(rs_cash.getString("inv_no"));
       				cash_bill.setCashBillId(rs_cash.getString("cash_bill_id"));
       				cash_bill.setPoNum(rs_cash.getString("ponum"));
       				cash_bill.setTotalIncVat(rs_cash.getString("total_inc_vat"));
       				cash_bill.setTotalValue(rs_cash.getString("total_value"));
       				cash_bill.setTotalVat(rs_cash.getString("total_vat"));
       				cash_bill.setType(rs_cash.getString("type"));
       				cash_bill.setTaxId(rs_cash.getString("tax_id"));
       				cash_bill.setInvFilePath(rs_cash.getString("inv_file_path"));
       				cash_bill.setInvFileName(rs_cash.getString("inv_file_name"));
       				cash_bill.setNote(rs_cash.getString("note"));
       				cash_bill.setPaymentReference(rs_cash.getString("payment_reference"));
       				cash_bill.setTotalCost(rs_cash.getString("total_cost"));
       				cash_bill.setTotalProfit(rs_cash.getString("total_profit"));
       				
       				
              cash_inv_list.add(cash_bill);
            } 
              
          
             
         	try {
				Json = mapper.writeValueAsString(cash_inv_list); 
				System.out.println("Json:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	
         	
         	out.print(Json);
	   
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  out.print("fail");
      }
     
  
	connect.close();
%>
