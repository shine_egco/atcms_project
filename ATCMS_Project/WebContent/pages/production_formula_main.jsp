<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../dist/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<script type="text/javascript" src="../js/jquery-2.1.1.min.js"></script>
 <script>
	 

	
	 function formatMoney(number, places, symbol, thousand, decimal) {
			number = number || 0;
			places = !isNaN(places = Math.abs(places)) ? places : 0;
			symbol = symbol !== undefined ? symbol : "";
			thousand = thousand || ",";
			decimal = decimal || ".";
			var negative = number < 0 ? "-" : "",
			    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
			    j = (j = i.length) > 3 ? j % 3 : 0;
			return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
	}
	
	 function set_formula_to_table(){
			
			//	  var  table = $('#dataTables-example-product').DataTable();
			//	       table.clear();
			
				  var xmlhttp;
					
					if(window.XMLHttpRequest) {
						// code for IE7+, Firefox, Chrome, Opera, Safari
						xmlhttp = new XMLHttpRequest();
					}
					else {
						// code for IE6, IE5
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					
					xmlhttp.onreadystatechange = function() {
						if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
							
					
							
							var jsonObj = JSON.parse(xmlhttp.responseText);
						
					
							if(jsonObj.length == 0) {
							//	massage();
							//	alert("0 Result");
							}
							else{
							
								
								for(i in jsonObj) {
									$('#dataTables-example-formula').DataTable().row.add([
		                               '<tr><td>'+jsonObj[i].formulaID+'</td>'
			                           ,'<td>'+jsonObj[i].formulaCode+'</td>'
			                           ,'<td>'+jsonObj[i].nameTH+'</td>'
			                           ,'<td>'+jsonObj[i].nameEN+'</td>'
			                           ,'<td><center>'+jsonObj[i].capacity+' '+jsonObj[i].capacityUnit+'/'+jsonObj[i].packagingUnit+'</center></td>'
			                           ,'<td>'+jsonObj[i].description+'</td>'
			                           ,'<td><center>'+jsonObj[i].totalCost+'</center></td>'
			                           ,'<td><center><button id = "'+jsonObj[i].formulaID+'" type="button" class="btn btn-info btn-circle btn-md" onclick = "redirect_formula_detail(this.id)" data-target="#myModal">'
			                           +'<i class="glyphicon glyphicon-search"></i></button></center></td>'
			                           ,'<td><center><button id = "'+jsonObj[i].formulaID+'" type="button" class="btn btn-danger btn-circle btn-md" onclick = "show_delete_modal(this.id)" data-target="#myModal">'
			                           +'<i class="glyphicon glyphicon-trash"></i></button> </center></td></tr>'	                               
			                         ]).draw();
								}
								
							}
							
							
							//System.out.println("temp_x:"+temp_x);	
						
						
						}// end if check state
					}// end function
					
			
					xmlhttp.open("POST", "get_all_formula_main_background.jsp", true);
					xmlhttp.send();

			}
	 
	   function redirect_formula_detail(form_id){
		   
			sessionStorage.setItem("formula_for_get_detail", form_id); 
		 
			
			window.open("production_formula_detail.jsp");
			
			
	   }
	   function show_delete_modal(id){
		   
	 		$('#confirm-delete-order').modal('show');
	 		//alert(id);
			  document.getElementById("delete_form_id").value = id;

	   }
	   
	   function delete_formula(){
		   
			 var delete_form_id =  document.getElementById("delete_form_id");
			 
			 alert(delete_form_id.value);
			  var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
						
						if(xmlhttp.responseText=="fail")
						{
							alert("Create Error");
							
						}else{
							
							alert("Success");
							$('#confirm-delete-order').modal('hide');
							
							location.reload();
						}
					
					}// end if check state
				}// end function
				
		
				xmlhttp.open("POST", "delete_single_formula_by_formula_id_background.jsp?formula_id="+delete_form_id.value, true);
				xmlhttp.send();

	   }

	   
</script>
<style>
				body.modal-open #wrap{
				    -webkit-filter: blur(7px);
				    -moz-filter: blur(15px);
				    -o-filter: blur(15px);
				    -ms-filter: blur(15px);
				    filter: blur(15px);
				}
				  
				.modal-backdrop {background: #f7f7f7;}
				
				.close {
				    font-size: 50px;
				    display:block;
				}
				
				.modal {
				    position: fixed;
				    top: 40%;
				    left: 10%;
				    right: 10%;
				    bottom: 15%;
				}			
	 </style>
	 <style type="text/css">
		 #loading {
			    background: #f4f4f2 url("img/page-bg.png") repeat scroll 0 0;
			    height: 100%;
			    left: 0;
			    margin: auto;
			    position: fixed;
			    top: 0;
			    width: 100%;
			}
			.bokeh {
			    border: 0.01em solid rgba(150, 150, 150, 0.1);
			    border-radius: 50%;
			    font-size: 100px;
			    height: 1em;
			    list-style: outside none none;
			    margin: 0 auto;
			    position: relative;
			    top: 35%;
			    width: 1em;
			    z-index: 2147483647;
			}
			.bokeh li {
			    border-radius: 50%;
			    height: 0.2em;
			    position: absolute;
			    width: 0.2em;
			}
			.bokeh li:nth-child(1) {
			    animation: 1.13s linear 0s normal none infinite running rota, 3.67s ease-in-out 0s alternate none infinite running opa;
			    background: #00c176 none repeat scroll 0 0;
			    left: 50%;
			    margin: 0 0 0 -0.1em;
			    top: 0;
			    transform-origin: 50% 250% 0;
			}
			.bokeh li:nth-child(2) {
			    animation: 1.86s linear 0s normal none infinite running rota, 4.29s ease-in-out 0s alternate none infinite running opa;
			    background: #ff003c none repeat scroll 0 0;
			    margin: -0.1em 0 0;
			    right: 0;
			    top: 50%;
			    transform-origin: -150% 50% 0;
			}
			.bokeh li:nth-child(3) {
			    animation: 1.45s linear 0s normal none infinite running rota, 5.12s ease-in-out 0s alternate none infinite running opa;
			    background: #fabe28 none repeat scroll 0 0;
			    bottom: 0;
			    left: 50%;
			    margin: 0 0 0 -0.1em;
			    transform-origin: 50% -150% 0;
			}
			.bokeh li:nth-child(4) {
			    animation: 1.72s linear 0s normal none infinite running rota, 5.25s ease-in-out 0s alternate none infinite running opa;
			    background: #88c100 none repeat scroll 0 0;
			    margin: -0.1em 0 0;
			    top: 50%;
			    transform-origin: 250% 50% 0;
			}
			@keyframes opa {
			12% {
			    opacity: 0.8;
			}
			19.5% {
			    opacity: 0.88;
			}
			37.2% {
			    opacity: 0.64;
			}
			40.5% {
			    opacity: 0.52;
			}
			52.7% {
			    opacity: 0.69;
			}
			60.2% {
			    opacity: 0.6;
			}
			66.6% {
			    opacity: 0.52;
			}
			70% {
			    opacity: 0.63;
			}
			79.9% {
			    opacity: 0.6;
			}
			84.2% {
			    opacity: 0.75;
			}
			91% {
			    opacity: 0.87;
			}
			}
			
			@keyframes rota {
			100% {
			    transform: rotate(360deg);
			}
			}
			
			
			.table-fixed thead {
			  width: 97%;
			}
			.table-fixed tbody {
			  height: 230px;
			  overflow-y: auto;
			  width: 100%;
			}
			.table-fixed thead, .table-fixed tbody, .table-fixed tr, .table-fixed td, .table-fixed th {
			  display: block;
			}
			.table-fixed tbody td, .table-fixed thead > tr> th {
			  float: left;
			  border-bottom-width: 0;
			}
			
    </style>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
      		   <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.jsp">ATCMS Admin</a>
            </div>
            <!-- /.navbar-header -->

   
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="dashboard_main.jsp"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-group fa-fw"></i>Customer&Vendor<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="customer_sub.jsp">Customers</a>
                                </li>
                                <li>
                                    <a href="vendor_sub.jsp">Vendors</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                         <li>
                            <a href="#"><i class="fa fa-leaf"></i>  Product<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="atc_product_sub.jsp">ATC Product</a>
                                </li>
                                <li>
                                    <a href="other_product_sub.jsp">Other Product</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o"></i>  Bill Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="direct_bill_sub.jsp">Direct Bill</a>
                                </li>
                                <li>
                                    <a href="indirect_bill_sub.jsp">Indirect Bill</a>
                                </li>
                                <li>
                                    <a href="credit_inv_sub.jsp">Credit Invoice</a>
                                </li>
                                <li>
                                    <a href="cash_inv_sub.jsp">Cash Invoice</a>
                                </li>
                                <li>
                                    <a href="credit_note_sub.jsp">Credit Note (ใบลดหนี้)</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                        	<a href="#"><i class="glyphicon glyphicon-fire"></i> Production (การผลิต)<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">                         
                               <li>
                                    <a href="production_material_main.jsp">Material</a>
                                </li>
                                <li>
                                    <a href="production_work_order_main.jsp">Work Order</a>
                                </li>
                                <li>
                                    <a href="production_formula_main.jsp">Formula</a>
                                </li>
                                 <li>
                                    <a href="production_product_relate_formula.jsp">Product Relate Formula</a>
                                </li>                
                            </ul>
                        </li>
                        <li>
                        	<a href="#"><i class="glyphicon glyphicon-stats"></i> Reporting (รายงาน)<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="monthly_summary_report_main.jsp">Monthly Summary Report</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                        	<a href="#"><i class="glyphicon glyphicon-road"></i> (DEVEL)0pinG Z()Ne <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="dev_show_profit_each_bill_detail.jsp">Each  Bill Value Detail</a>
                                </li>
                                <li>
                                    <a href="dev_balancing_work_order.jsp">Balancing Work Order</a>
                                </li>
                
                            </ul>
                        </li>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
             <div class="row">
	                <div class="col-lg-12">
	                    <h1 class="page-header"></h1>
	                </div>
                <!-- /.col-lg-12 -->
             </div>
             <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           	Formula List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        	<div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example-formula">
                                    <thead>
                                        <tr>
                                      <!--   	<th>ID</th>   -->
                                            <th>Formula ID</th>
                                             <th>Formula Code</th>
                                            <th>Name TH</th>
                                            <th>Name EN</th>
                                            <th>Capacity</th>
                                            <th>Description</th>
                                       		<th>Total Cost</th>
                                       		<th style="width: 20px;"></th>
                                       		<th style="width: 20px;"></th>
                                       		
                                          
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        
                           
                                       
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        
         <div class="modal fade" id="confirm-delete-order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			    <div class="modal-dialog">
			        <div class="modal-content">
			        	<form>
					          <div class="modal-body">
								    <textarea id="reason_delete" class="form-control" rows="2" placeholder="Reason for deleting this order"></textarea>
								    
								    <input type="hidden" id="delete_form_id">
							  </div>
							  <div class="modal-footer">
								    <button type="button" data-dismiss="modal" class="btn btn-danger" id="but_delete_order" onclick="delete_formula(this.form)">Delete</button>
								    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
							  </div>
						 </form>
			        </div>
			    </div>
		</div>             

        
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
      <script src="../dist/js/dataTables.tableTools.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    	function create_new_formula(){
    		
    		window.location ="production_create_formula.jsp";
    	}
    
    </script>
    
    <script>
    $(document).ready(function() {
    	var table_cus = $('#dataTables-example-formula').DataTable({
  	      responsive: true,
            "aLengthMenu": [[25, 50, 75, -1], [1, 3, 5]],
            "iDisplayLength": 10 ,
            lengthChange: false 
            ,
            "sDom": 'T<"clear">lfrtip' ,
            "oTableTools": {
                    "aButtons": [
	                                   {
	                                       "sExtends":    "text",
	                                       "fnClick": function ( nButton, oConfig, oFlash ) {
	                                    	   create_new_formula();
	                                       },
	                                       "sButtonText": "<i class='fa glyphicon-plus'></i>",
	                                        "sExtraData": [ 
	                                                            { "name":"operation", "value":"downloadcsv" }       
	                                                      ]
	                                    
	                                   }
                   			 ]
                }
           
      });
    	set_formula_to_table();
    	
        var fixHelperModified = function(e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
        
            $helper.children().each(function(index) {
                $(this).width($originals.eq(index).width())
            });
            
            return $helper;
        };
                                           
        $(".table-sortable tbody").sortable({
            helper: fixHelperModified      
        }).disableSelection();

        $(".table-sortable thead").disableSelection();
		// DEAD ZONE all code below here not working
        
        
    });
    </script>

</body>

</html>
