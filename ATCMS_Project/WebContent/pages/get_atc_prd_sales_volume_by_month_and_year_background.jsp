<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>


<% 

	System.out.println("Start get_atc_prd_sales_volume_by_month_and_year_background/");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

	List<ProductSalesVolume> rs_list = new ArrayList<ProductSalesVolume>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	String 	month = request.getParameter("month");
	String year = request.getParameter("year");
	

      try{
    	  
    	  
    	  String sql_query  = " SELECT  PRODUCT_ID , NAME_TH , NAME_EN , SUM(QUANTITY) AS QUANTITY  ,FORMULA_ID "+
    	  												  " , capacity , net_capacity , capacity_unit ,  packaging_unit "+
					    				 "   FROM ( "+
					    			     "     (   "+
					    			     "                  SELECT  order_detail.product_id AS PRODUCT_ID "+
					    			    		 						" , product.name_th AS NAME_TH "+
					    			     								" , product.name_en AS NAME_EN  "+
					    			    		 						" , SUM(order_detail.quantity)  AS QUANTITY "+
					    			     								" , product.formula_id AS FORMULA_ID "+
					    			    		 						" , formula_main.capacity "+
					    			    		 						" , formula_main.net_capacity "+
					    			    		 						" , formula_main.capacity_unit "+
					    			    		 						" , formula_main.packaging_unit "+
					    			     "                  FROM order_detail "+
					    			     "                  JOIN product "+
					    			     "                  ON  order_detail.product_id = product.product_id "+
					    			     "                  JOIN formula_main "+
					    			     "                  ON  product.formula_id = formula_main.formula_id "+
					    			     "					JOIN order_main "+
					    			     "                  ON  order_detail.order_id = order_main.order_id   "+
					    			     "                  WHERE product.category='ATC_PRD' "+
					    			     "                  AND product.formula_id IS NOT NULL "+
					    			     "					AND order_detail.order_id !='INIT_ORDER_ID' "+
					                     "                  AND order_main.order_id !='INIT_ORDER_ID' "+
					                     "                  AND MONTH(order_main.delivery_date) =  '"+month+"' "+
							             "                  AND YEAR(order_main.delivery_date) =  '"+year+"' "+
					    			     "                  GROUP BY  order_detail.product_id "+
					    			     "      )   "+
					    			     "   UNION "+
					    			     "     (  "+
					    			     "                   SELECT  cash_bill_detail.product_id AS PRODUCT_ID "+
					    			    		 			            "  , product.name_th AS NAME_TH "+
					    			   									"  , product.name_en AS NAME_EN "+
					    			    		 			            "  , SUM(cash_bill_detail.quantity) AS QUANTITY "+
					    			   									"  , product.formula_id AS FORMULA_ID  "+
					    			    		 						" , formula_main.capacity "+
					    			    		 						" , formula_main.net_capacity "+
					    			    		 						" , formula_main.capacity_unit "+
					    			    		 						" , formula_main.packaging_unit "+
					    			     "                    FROM cash_bill_detail "+
					    			     "                   JOIN product "+
					    			     "                   ON  cash_bill_detail.product_id = product.product_id "+
					    			     "                   JOIN formula_main "+
					    			     "                   ON  product.formula_id = formula_main.formula_id "+
							    	     "					 JOIN cash_bill_main "+
							    		 "                   ON  cash_bill_detail.cash_bill_id = cash_bill_main.cash_bill_id   "+
					    			     "                   WHERE product.category='ATC_PRD' "+
					    			     "                   AND product.formula_id IS NOT NULL "+
					    			     "					 AND cash_bill_detail.cash_bill_id !='INIT_ORDER_ID' "+
					                     "                   AND cash_bill_main.cash_bill_id !='INIT_ORDER_ID' "+
							              "                  AND MONTH(cash_bill_main.bill_date) =  '"+month+"' "+
									      "                  AND YEAR(cash_bill_main.bill_date) =  '"+year+"' "+
					    			     "                   GROUP BY  cash_bill_detail.product_id "+
					    			     "     )  "+
					    			     "   UNION "+
					    			     "     (  "+
					    			     "                   SELECT daily_novat_detail.product_id AS PRODUCT_ID "+
					    			    		 			            "  , product.name_th AS NAME_TH "+
					    			   									"  , product.name_en AS NAME_EN "+
					    			    		 			            "  , SUM(daily_novat_detail.quantity) AS QUANTITY "+
					    			   									"  , product.formula_id AS FORMULA_ID  "+
					    			    		 						" , formula_main.capacity "+
					    			    		 						" , formula_main.net_capacity "+
					    			    		 						" , formula_main.capacity_unit "+
					    			    		 						" , formula_main.packaging_unit "+
					    			     "                   FROM daily_novat_detail "+
					    			     "                   JOIN product "+
					    			     "                   ON  daily_novat_detail.product_id = product.product_id "+
					    			     "                   JOIN formula_main "+
					    			     "                   ON  product.formula_id = formula_main.formula_id "+
							    	     "					 JOIN daily_novat_main "+
							    		 "                   ON  daily_novat_detail.daily_novat_id = daily_novat_main.daily_novat_id   "+
					    			     "                   WHERE product.category='ATC_PRD' "+
					    			     "                   AND product.formula_id IS NOT NULL "+
					    			     "					 AND daily_novat_detail.daily_novat_id !='INIT_ORDER_ID' "+
					                     "                   AND daily_novat_main.daily_novat_id !='INIT_ORDER_ID' "+
							              "                  AND MONTH(daily_novat_main.bill_date) =  '"+month+"' "+
									      "                  AND YEAR(daily_novat_main.bill_date) =  '"+year+"' "+
					    			     "                   GROUP BY  daily_novat_detail.product_id "+
					    			     "     )  "+
					    			     "   ) AS T_UNION "+
					    		"	GROUP BY PRODUCT_ID";
    	  
    	  
    	  System.out.println("sql_query:"+sql_query);
    	  ResultSet rs = connect.createStatement().executeQuery(sql_query);
          
    	  while(rs.next())
          {
    		  ProductSalesVolume temp = new ProductSalesVolume();
      			
    		  temp.setFormulaID(rs.getString("FORMULA_ID"));
    		  temp.setNameEN(rs.getString("NAME_EN"));
    		  temp.setNameTH(rs.getString("NAME_TH"));
    		  temp.setProductID(rs.getString("PRODUCT_ID"));
    		  temp.setTotalVolume(rs.getString("QUANTITY"));
    		  temp.setFullUnit(rs.getString("capacity")+" "+rs.getString("capacity_unit") +"/"+rs.getString("packaging_unit"));
    		  
    		  rs_list.add(temp);
              
          }
             
         	try {
				Json = mapper.writeValueAsString(rs_list); 
				System.out.println("Json:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	out.print(Json);
	   
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  connect.close();
    	  out.print("fail");
      }
     
    System.out.println("SUCCESS");
	connect.close();
%>
