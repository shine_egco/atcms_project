<%@page import="org.apache.poi.xwpf.usermodel.TextAlignment"%>
<%@page import="org.apache.poi.xwpf.usermodel.Borders"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "com.atcms.*" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "java.util.Properties" %>
<%@ page import = "javax.servlet.ServletContext" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.util.Date" %>
<%@ page import="java.math.BigInteger" %>
<%@ page import="java.io.FileOutputStream" %>
<%@ page import="org.apache.poi.xssf.usermodel.*" %>
<%@ page import="org.apache.poi.hssf.usermodel.HSSFCellStyle" %>
<%@ page import="org.apache.poi.hssf.util.*" %>
<%@ page import="org.apache.poi.ss.util.CellUtil" %>
<%@ page import="org.apache.poi.ss.usermodel.*" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>
<%@ page import = "java.text.DecimalFormat" %>




<%!
		public static final short EXCEL_COLUMN_WIDTH_FACTOR = 256; 
	    public static final short EXCEL_ROW_HEIGHT_FACTOR = 20; 
	    public static final int UNIT_OFFSET_LENGTH = 7; 
	    public static final int[] UNIT_OFFSET_MAP = new int[] { 0, 36, 73, 109, 146, 182, 219 };
	    
	    public static RichTextString createRichTextString (XSSFWorkbook workbook_in,String text,String font_name ,int font_size)
	    {
	    	 RichTextString richString = new XSSFRichTextString(text);
	    	 XSSFFont font = workbook_in.createFont();
	    	 font.setFontName(font_name);
			 font.setFontHeightInPoints((short)font_size);
			 richString.applyFont(font);
			 return richString;
	    }
	    
	    
		public static XSSFCellStyle CreateStyle(XSSFWorkbook workbook_in
							 				   ,String font_name_in 
							                   , int font_size_in
							                   , boolean isBold)
		{
				XSSFFont font = workbook_in.createFont();
				font.setFontName(font_name_in);
				font.setFontHeightInPoints((short)font_size_in);
				font.setBold(isBold);
				XSSFCellStyle style = workbook_in.createCellStyle();
				style.setFont(font);
				return style ;
		}
				
		public static XSSFCellStyle CreateCellBorder(XSSFWorkbook workbook_in 
										, boolean top 
										, boolean bot 
										, boolean left 
										, boolean right)
		{
				
				XSSFCellStyle style = workbook_in.createCellStyle();
				if(top){  style.setBorderTop(HSSFCellStyle.BORDER_THIN); }
				if(bot){ style.setBorderBottom(HSSFCellStyle.BORDER_THIN); }
				if(left){ style.setBorderLeft(HSSFCellStyle.BORDER_THIN); }
				if(right){ style.setBorderRight(HSSFCellStyle.BORDER_THIN); }
				
				return style ;
		}
		public static void DrawListCellBorder (XSSFWorkbook workbook , XSSFRow row_in){
			
			XSSFCellStyle style_border_top_left = CreateCellBorder(workbook,true,false,true,false);
			XSSFCellStyle style_border_top_right = CreateCellBorder(workbook,true,false,false,true);
			XSSFCellStyle style_border_top_left_right = CreateCellBorder(workbook,true,false,true,true);
			
			XSSFCellStyle style_border_top_bot_right = CreateCellBorder(workbook,true,true,false,true);
			
			XSSFCellStyle style_border_bot_left = CreateCellBorder(workbook,false,true,true,false);
			XSSFCellStyle style_border_bot_right = CreateCellBorder(workbook,false,true,false,true);
			XSSFCellStyle style_border_bot_left_right = CreateCellBorder(workbook,false,true,true,true);
			XSSFCellStyle style_border_top = CreateCellBorder(workbook,true,false,false,false);
			XSSFCellStyle style_border_bot = CreateCellBorder(workbook,false,true,false,false);
			XSSFCellStyle style_border_left = CreateCellBorder(workbook,false,false,true,false);
			XSSFCellStyle style_border_right = CreateCellBorder(workbook,false,false,false,true);
			XSSFCellStyle style_border_left_right = CreateCellBorder(workbook,false,false,true,true);
			XSSFCellStyle style_border_full = CreateCellBorder(workbook,true,true,true,true);
			
			row_in.createCell(0);
			row_in.createCell(1);
			row_in.createCell(11);
			row_in.createCell(12);
			row_in.createCell(13);
			row_in.createCell(15);
			row_in.getCell(0).setCellStyle(style_border_left_right);
			row_in.getCell(1).setCellStyle(style_border_left_right);
			row_in.getCell(11).setCellStyle(style_border_left);
			row_in.getCell(13).setCellStyle(style_border_left);
			row_in.getCell(15).setCellStyle(style_border_right);

		}
	    public static int widthUnits2Pixel(short widthUnits) {
	    	
	        int pixels = (widthUnits / EXCEL_COLUMN_WIDTH_FACTOR) * UNIT_OFFSET_LENGTH; 
	        int offsetWidthUnits = widthUnits % EXCEL_COLUMN_WIDTH_FACTOR; 
	        pixels += Math.floor((float) offsetWidthUnits / ((float) EXCEL_COLUMN_WIDTH_FACTOR / UNIT_OFFSET_LENGTH));   
	        return pixels; 
	    }
	    public static short pixel2WidthUnits(int pxs) {
	        short widthUnits = (short) (EXCEL_COLUMN_WIDTH_FACTOR * (pxs / UNIT_OFFSET_LENGTH)); 
	        widthUnits += UNIT_OFFSET_MAP[(pxs % UNIT_OFFSET_LENGTH)];  
	        return widthUnits; 
	    } 
	    


%>

<% 

	System.out.println("Start generate_credit_inv_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
	
	/////////////////////////Start Get Data Process /////////////////////////////
	String cash_bill_id = request.getParameter("cash_bill_id");
	String type ="";
	String ponum ="";
	String customer_name="";
	String customer_address_full="";  
	String total_value="";
	String total_vat="";
	String total_inc_vat = "";
	String inv_no="";
	String due_date="";
	String delivery_date="";
	String credit="";
	String tax_id = "";
	String address_line0 = "";
	String address_line1 = "";
	String status = "";
	String inv_no_temp ="";
	String bill_date_full = "";
	String bill_date = "";
	
	
	List<CashBillDetail> cash_bill_detail_list = new ArrayList<CashBillDetail>();
	
	/////////////////////////////////////////////////////////////////////////////
	
	String cash_bill_main_query = " SELECT * "+
							  " FROM cash_bill_main "+
					          "	JOIN company "+
							  " ON BINARY cash_bill_main.customer_id = company.company_id "+
							  " WHERE BINARY cash_bill_main.cash_bill_id='"+cash_bill_id+"'";

	System.out.println("order_main_query:"+cash_bill_main_query);
	ResultSet rs_cash_bill_main = connect.createStatement().executeQuery(cash_bill_main_query);
	 while(rs_cash_bill_main.next())
     {
		 type = rs_cash_bill_main.getString("type");
		 ponum = rs_cash_bill_main.getString("ponum");
		 if(("-").equals(rs_cash_bill_main.getString("name_th")))
		 {
			 customer_name = rs_cash_bill_main.getString("name_en");
			 customer_address_full = rs_cash_bill_main.getString("address_en");
			 address_line0 = rs_cash_bill_main.getString("address_en_line1");
			 
			 if(("-").equals(rs_cash_bill_main.getString("address_en_line2")))
			 {
				 
			 }else{
				 address_line1 = rs_cash_bill_main.getString("address_en_line2"); 
			 }
				 
			
			 
		 }else{
			 customer_name = rs_cash_bill_main.getString("name_th");
			 customer_address_full = rs_cash_bill_main.getString("address_th");
			 address_line0 = rs_cash_bill_main.getString("address_th_line1");
			 if(("-").equals(rs_cash_bill_main.getString("address_th_line2")))
			 {
				 
			 }else{
				 address_line1 = rs_cash_bill_main.getString("address_th_line2"); 
			 }
				 
		 }
		 
		 total_value = rs_cash_bill_main.getString("total_value");
		 total_vat = rs_cash_bill_main.getString("total_vat");
		 total_inc_vat =  rs_cash_bill_main.getString("total_inc_vat");
		 inv_no = rs_cash_bill_main.getString("inv_no");
		 credit = rs_cash_bill_main.getString("credit");
		 tax_id = rs_cash_bill_main.getString("tax_id");
		 status = rs_cash_bill_main.getString("status");
		 bill_date_full = rs_cash_bill_main.getString("bill_date");
     }
	/*
	String [] due_date_parts = due_date.split("-");
	int due_date_year = Integer.parseInt(due_date_parts[0]);
		due_date_year = due_date_year + 543 - 2500 ;  
	due_date =  due_date_parts[2] + "/"+ due_date_parts[1]+"/"+due_date_year;
	*/
	String [] bill_date_parts = bill_date_full.split("-");
	int bill_date_year = Integer.parseInt(bill_date_parts[0]);
	bill_date_year = bill_date_year + 543 - 2500 ;  
	bill_date =  bill_date_parts[2] + "/"+ bill_date_parts[1]+"/"+bill_date_year;
	
	System.out.println("bill_date:"+bill_date);
	System.out.println("bill_date_full:"+bill_date_full);

	
	System.out.println("type:"+type);
	System.out.println("ponum:"+ponum);
	System.out.println("customer_name:"+customer_name);
	System.out.println("customer_address:"+customer_address_full);	
	System.out.println("total_value:"+total_value);
	System.out.println("total_vat:"+total_vat);
	System.out.println("total_inc_vat:"+total_inc_vat);
	System.out.println("inv_no:"+inv_no);

	
	////////////////////////////////////////////////////////////////////////////
	
	String order_detail_query = " select cash_bill_detail.index , product.name_th , product.unit_th , product.name_en , product.unit_en "+
			  				            " ,cash_bill_detail.price , cash_bill_detail.adt_description ,cash_bill_detail.quantity "+
										",cash_bill_detail.price , cash_bill_detail.sum "+
			   					" from cash_bill_detail "+
			   					" join product "+
							    " on cash_bill_detail.product_id = product.product_id "+
							    " WHERE BINARY cash_bill_detail.cash_bill_id='"+cash_bill_id+"'"+
							    " ORDER BY cash_bill_detail.index ";

	ResultSet rs_cash_bill_detail = connect.createStatement().executeQuery(order_detail_query);
	
	while(rs_cash_bill_detail.next())
    {
			CashBillDetail temp_cash_bill_detail = new CashBillDetail();
			temp_cash_bill_detail.setAdtDescription(rs_cash_bill_detail.getString("adt_description"));
			if(("-").equals(rs_cash_bill_detail.getString("name_en")))
			{
				temp_cash_bill_detail.setProductName(rs_cash_bill_detail.getString("name_th"));
				
			}else{
				temp_cash_bill_detail.setProductName(rs_cash_bill_detail.getString("name_en"));
			}
			
			if(("-").equals(rs_cash_bill_detail.getString("unit_en")))
			{
				temp_cash_bill_detail.setProductUnit(rs_cash_bill_detail.getString("unit_th"));
			}else{
				temp_cash_bill_detail.setProductUnit(rs_cash_bill_detail.getString("unit_en"));
			}
			temp_cash_bill_detail.setPrice(rs_cash_bill_detail.getString("price"));
			temp_cash_bill_detail.setQuantity(rs_cash_bill_detail.getString("quantity"));
			temp_cash_bill_detail.setSum(rs_cash_bill_detail.getString("sum"));
			
			cash_bill_detail_list.add(temp_cash_bill_detail);
    }
	System.out.println("cash_bill_detail_list.size:"+cash_bill_detail_list.size());
	
	//////////////////////////End Get Data Process /////////////////////////////
	
	
	/////////////////////////Start Writing Excel Process//////////////////////////

	try {		

		inv_no_temp = inv_no.replace("/","_");
	//	Date curr_date = new Date();
		String[] delivery_part = bill_date.split("/");
		String year = (Integer.parseInt(delivery_part[2])+2500)+"";
		String month = delivery_part[1] ;
		    if(month.length()==1)
		    {
		    	month = "0" + month;
		    }
	
		File file_month_year = new File(application.getRealPath("/report/"+month+"_"+year));
		    if (!file_month_year.exists()) {
	            if (file_month_year.mkdir()) {
	                System.out.println("file_month_year Created!");
	            } else {
	                System.out.println("Failed to create file file_month_year");
	            }
	        }
		    File file_cash_report = new File(application.getRealPath("/report/"+month+"_"+year+"/cash_report/"));
			if (!file_cash_report.exists()) {
		            if (file_cash_report.mkdir()) {
		                System.out.println("file_cash_report created");
		            } else {
		                System.out.println("Failed to create file created");
		            }
		    }

		
		   
		String fileName = application.getRealPath("/report/"+month+"_"+year+"/cash_report/"+inv_no_temp+".xlsx");


	
			XSSFWorkbook workbook = new XSSFWorkbook(); 

		    XSSFSheet page1 = workbook.createSheet("Page1");
		    XSSFSheet page2 = workbook.createSheet("Page2");

		    				  
		    for(int w=0;w<2;w++)
		    {
		    	//workbook.getSheetAt(w).setColumnWidth(0,pixel2WidthUnits((short)54));//A
		    	workbook.getSheetAt(w).setColumnWidth(0,pixel2WidthUnits((short)(54-8)));//A
		    	workbook.getSheetAt(w).setColumnWidth(1,pixel2WidthUnits((short)(38-5)));//B
		    	workbook.getSheetAt(w).setColumnWidth(2,pixel2WidthUnits((short)(32-5)));//C
		    	workbook.getSheetAt(w).setColumnWidth(3,pixel2WidthUnits((short)(32-5)));//D
		    	workbook.getSheetAt(w).setColumnWidth(4,pixel2WidthUnits((short)(32-5)));//E
		    	workbook.getSheetAt(w).setColumnWidth(5,pixel2WidthUnits((short)(32-5)));//F
		    	workbook.getSheetAt(w).setColumnWidth(6,pixel2WidthUnits((short)(32-5)));//G
		    	workbook.getSheetAt(w).setColumnWidth(7,pixel2WidthUnits((short)(32-5)));//H
		    	workbook.getSheetAt(w).setColumnWidth(8,pixel2WidthUnits((short)(32-5)));//I
		    	workbook.getSheetAt(w).setColumnWidth(9,pixel2WidthUnits((short)(32-5)));//J
		    	workbook.getSheetAt(w).setColumnWidth(10,pixel2WidthUnits((short)(24-3)));//K
		    	workbook.getSheetAt(w).setColumnWidth(11,pixel2WidthUnits((short)(79-11)));//L
		    	workbook.getSheetAt(w).setColumnWidth(12,pixel2WidthUnits((short)(52-9)));//M
		    	workbook.getSheetAt(w).setColumnWidth(13,pixel2WidthUnits((short)(62-9)));//N
		    	workbook.getSheetAt(w).setColumnWidth(14,pixel2WidthUnits((short)(75-11)));//O
		    	workbook.getSheetAt(w).setColumnWidth(15,pixel2WidthUnits((short)(32-5)));//P
		    }
		    
			
			XSSFRow row_1 = page1.createRow((short)0);
			XSSFRow row_2 = page1.createRow((short)1);
			XSSFRow row_3 = page1.createRow((short)2);
			XSSFRow row_4 = page1.createRow((short)3);	
			XSSFRow row_5 = page1.createRow((short)4);
			XSSFRow row_6 = page1.createRow((short)5);		
			XSSFRow row_7 = page1.createRow((short)6);		
			XSSFRow row_8 = page1.createRow((short)7);	
			XSSFRow row_9 = page1.createRow((short)8);		
			XSSFRow row_10 = page1.createRow((short)9);	
			XSSFRow row_11 = page1.createRow((short)10);	
			XSSFRow row_12 = page1.createRow((short)11);	
			XSSFRow row_13 = page1.createRow((short)12);	
			XSSFRow row_14 = page1.createRow((short)13);
			XSSFRow row_15 = page1.createRow((short)14);	
			XSSFRow row_16 = page1.createRow((short)15);	
			XSSFRow row_17 = page1.createRow((short)16);
			XSSFRow row_18 = page1.createRow((short)17);
			XSSFRow row_19 = page1.createRow((short)18);
			XSSFRow row_20 = page1.createRow((short)19);
			XSSFRow row_21 = page1.createRow((short)20);
			XSSFRow row_22 = page1.createRow((short)21);
			XSSFRow row_23 = page1.createRow((short)22);
			XSSFRow row_24 = page1.createRow((short)23);
			XSSFRow row_25 = page1.createRow((short)24);
			XSSFRow row_26 = page1.createRow((short)25);
			XSSFRow row_27 = page1.createRow((short)26);
			XSSFRow row_28 = page1.createRow((short)27);
			XSSFRow row_29 = page1.createRow((short)28);
			XSSFRow row_30 = page1.createRow((short)29);
			XSSFRow row_31 = page1.createRow((short)30);
			XSSFRow row_32 = page1.createRow((short)31);
			XSSFRow row_33 = page1.createRow((short)32);
			XSSFRow row_34 = page1.createRow((short)33);
			XSSFRow row_35 = page1.createRow((short)34);
			XSSFRow row_36 = page1.createRow((short)35);
			XSSFRow row_37 = page1.createRow((short)36);
			XSSFRow row_38 = page1.createRow((short)37);
			XSSFRow row_39 = page1.createRow((short)38);
			XSSFRow row_40 = page1.createRow((short)39);

			

			XSSFRow row_p2_1 = page2.createRow((short)0);
			XSSFRow row_p2_2 = page2.createRow((short)1);
			XSSFRow row_p2_3 = page2.createRow((short)2);
			XSSFRow row_p2_4 = page2.createRow((short)3);	
			XSSFRow row_p2_5 = page2.createRow((short)4);
			XSSFRow row_p2_6 = page2.createRow((short)5);		
			XSSFRow row_p2_7 = page2.createRow((short)6);		
			XSSFRow row_p2_8 = page2.createRow((short)7);	
			XSSFRow row_p2_9 = page2.createRow((short)8);		
			XSSFRow row_p2_10 = page2.createRow((short)9);	
			XSSFRow row_p2_11 = page2.createRow((short)10);	
			XSSFRow row_p2_12 = page2.createRow((short)11);	
			XSSFRow row_p2_13 = page2.createRow((short)12);	
			XSSFRow row_p2_14 = page2.createRow((short)13);
			XSSFRow row_p2_15 = page2.createRow((short)14);	
			XSSFRow row_p2_16 = page2.createRow((short)15);	
			XSSFRow row_p2_17 = page2.createRow((short)16);
			XSSFRow row_p2_18 = page2.createRow((short)17);
			XSSFRow row_p2_19 = page2.createRow((short)18);
			XSSFRow row_p2_20 = page2.createRow((short)19);
			XSSFRow row_p2_21 = page2.createRow((short)20);
			XSSFRow row_p2_22 = page2.createRow((short)21);
			XSSFRow row_p2_23 = page2.createRow((short)22);
			XSSFRow row_p2_24 = page2.createRow((short)23);
			XSSFRow row_p2_25 = page2.createRow((short)24);
			XSSFRow row_p2_26 = page2.createRow((short)25);
			XSSFRow row_p2_27 = page2.createRow((short)26);
			XSSFRow row_p2_28 = page2.createRow((short)27);
			XSSFRow row_p2_29 = page2.createRow((short)28);
			XSSFRow row_p2_30 = page2.createRow((short)29);
			XSSFRow row_p2_31 = page2.createRow((short)30);
			XSSFRow row_p2_32 = page2.createRow((short)31);
			XSSFRow row_p2_33 = page2.createRow((short)32);
			XSSFRow row_p2_34 = page2.createRow((short)33);
			XSSFRow row_p2_35 = page2.createRow((short)34);
			XSSFRow row_p2_36 = page2.createRow((short)35);
			XSSFRow row_p2_37 = page2.createRow((short)36);
			XSSFRow row_p2_38 = page2.createRow((short)37);
			XSSFRow row_p2_39 = page2.createRow((short)38);
			XSSFRow row_p2_40 = page2.createRow((short)39);
	
			
			row_1.setHeightInPoints(30f);
			row_2.setHeightInPoints(30f);
			row_3.setHeightInPoints(18f);
			row_4.setHeightInPoints(23.25f);
			row_5.setHeightInPoints(18f);
			row_6.setHeightInPoints(9f);
			row_7.setHeightInPoints(18f);
			row_8.setHeightInPoints(1f);
			row_9.setHeightInPoints(15f);
			row_10.setHeightInPoints(15f);
			row_11.setHeightInPoints(18.75f);
			row_12.setHeightInPoints(17.25f);
			row_13.setHeightInPoints(17.25f);
			row_14.setHeightInPoints(17.25f);
			row_15.setHeightInPoints(8.25f);
			row_16.setHeightInPoints(15f);
			row_17.setHeightInPoints(15f);
			row_18.setHeightInPoints(21f);
			row_19.setHeightInPoints(21f);
			row_20.setHeightInPoints(21f);
			row_21.setHeightInPoints(21f);
			row_22.setHeightInPoints(21f);
			row_23.setHeightInPoints(21f);
			row_24.setHeightInPoints(21f);
			row_25.setHeightInPoints(21f);
			row_26.setHeightInPoints(21f);
			row_27.setHeightInPoints(21f);
			row_28.setHeightInPoints(21f);
			row_29.setHeightInPoints(21f);
			row_30.setHeightInPoints(24f);
			row_31.setHeightInPoints(24f);
			row_32.setHeightInPoints(24f);
			row_33.setHeightInPoints(16.5f);
			row_34.setHeightInPoints(16.5f);
			row_35.setHeightInPoints(16.5f);
			row_36.setHeightInPoints(16.5f);
			row_37.setHeightInPoints(16.5f);
			row_38.setHeightInPoints(12.75f);
			row_39.setHeightInPoints(12.75f);
			row_40.setHeightInPoints(12.75f);
		
			
			
			row_p2_1.setHeightInPoints(30f);
			row_p2_2.setHeightInPoints(30f);
			row_p2_3.setHeightInPoints(18f);
			row_p2_4.setHeightInPoints(23.25f);
			row_p2_5.setHeightInPoints(18f);
			row_p2_6.setHeightInPoints(9f);
			row_p2_7.setHeightInPoints(18f);
			row_p2_8.setHeightInPoints(1f);
			row_p2_9.setHeightInPoints(15f);
			row_p2_10.setHeightInPoints(15f);
			row_p2_11.setHeightInPoints(18.75f);
			row_p2_12.setHeightInPoints(17.25f);
			row_p2_13.setHeightInPoints(17.25f);
			row_p2_14.setHeightInPoints(17.25f);
			row_p2_15.setHeightInPoints(8.25f);
			row_p2_16.setHeightInPoints(15f);
			row_p2_17.setHeightInPoints(15f);
			row_p2_18.setHeightInPoints(21f);
			row_p2_19.setHeightInPoints(21f);
			row_p2_20.setHeightInPoints(21f);
			row_p2_21.setHeightInPoints(21f);
			row_p2_22.setHeightInPoints(21f);
			row_p2_23.setHeightInPoints(21f);
			row_p2_24.setHeightInPoints(21f);
			row_p2_25.setHeightInPoints(21f);
			row_p2_26.setHeightInPoints(21f);
			row_p2_27.setHeightInPoints(21f);
			row_p2_28.setHeightInPoints(21f);
			row_p2_29.setHeightInPoints(21f);
			row_p2_30.setHeightInPoints(24f);
			row_p2_31.setHeightInPoints(24f);
			row_p2_32.setHeightInPoints(24f);
			row_p2_33.setHeightInPoints(16.5f);
			row_p2_34.setHeightInPoints(16.5f);
			row_p2_35.setHeightInPoints(16.5f);
			row_p2_36.setHeightInPoints(16.5f);
			row_p2_37.setHeightInPoints(16.5f);
			row_p2_38.setHeightInPoints(12.75f);
			row_p2_39.setHeightInPoints(12.75f);
			row_p2_40.setHeightInPoints(12.75f);
		
			
			XSSFCellStyle style_arial_18_bold = CreateStyle(workbook,"Arial",18,true);		
			XSSFCellStyle style_arial_14 = CreateStyle(workbook,"Arial",14,false);
			XSSFCellStyle style_arial_14_bold = CreateStyle(workbook,"Arial",14,true);
			XSSFCellStyle style_arial_13 = CreateStyle(workbook,"Arial",13,false);
			XSSFCellStyle style_arial_12_bold = CreateStyle(workbook,"Arial",12,true);
			XSSFCellStyle style_arial_11 = CreateStyle(workbook,"Arial",11,false);
			XSSFCellStyle style_arial_11_bold = CreateStyle(workbook,"Arial",11,true);
			XSSFCellStyle style_arial_10 = CreateStyle(workbook,"Arial",10,false);
			XSSFCellStyle style_arial_9 = CreateStyle(workbook,"Arial",9,false);	
			XSSFCellStyle style_arial_8 = CreateStyle(workbook,"Arial",8,false);
			
	
			XSSFCellStyle style_border_top_left = CreateCellBorder(workbook,true,false,true,false);
			XSSFCellStyle style_border_top_right = CreateCellBorder(workbook,true,false,false,true);
			XSSFCellStyle style_border_top_left_right = CreateCellBorder(workbook,true,false,true,true);
			XSSFCellStyle style_border_top_bot_right = CreateCellBorder(workbook,true,true,false,true);
			XSSFCellStyle style_border_bot_left = CreateCellBorder(workbook,false,true,true,false);
			XSSFCellStyle style_border_bot_right = CreateCellBorder(workbook,false,true,false,true);
			XSSFCellStyle style_border_bot_left_right = CreateCellBorder(workbook,false,true,true,true);
			XSSFCellStyle style_border_top = CreateCellBorder(workbook,true,false,false,false);
			XSSFCellStyle style_border_bot = CreateCellBorder(workbook,false,true,false,false);
			XSSFCellStyle style_border_top_bot = CreateCellBorder(workbook,true,true,false,false);
			XSSFCellStyle style_border_left = CreateCellBorder(workbook,false,false,true,false);
			XSSFCellStyle style_border_right = CreateCellBorder(workbook,false,false,false,true);
			XSSFCellStyle style_border_left_right = CreateCellBorder(workbook,false,false,true,true);
			XSSFCellStyle style_border_full = CreateCellBorder(workbook,true,true,true,true);
			
			
			XSSFCellStyle com_tl_border_arial_10 =  workbook.createCellStyle();
			  com_tl_border_arial_10.cloneStyleFrom(style_arial_10);
			  com_tl_border_arial_10.setDataFormat(style_border_top_left.getDataFormat());
			
			page1.addMergedRegion(new CellRangeAddress(0,0,0,15));
			page1.addMergedRegion(new CellRangeAddress(1,1,0,15));
			page1.addMergedRegion(new CellRangeAddress(2,2,0,15));
			page1.addMergedRegion(new CellRangeAddress(3,3,0,15));
			page1.addMergedRegion(new CellRangeAddress(4,4,0,15));
			page1.addMergedRegion(new CellRangeAddress(15,15,0,1));
			page1.addMergedRegion(new CellRangeAddress(15,15,2,10));
			page1.addMergedRegion(new CellRangeAddress(15,15,11,12));
			page1.addMergedRegion(new CellRangeAddress(15,15,13,15));
			page1.addMergedRegion(new CellRangeAddress(16,16,0,1));
			page1.addMergedRegion(new CellRangeAddress(16,16,2,10));
			page1.addMergedRegion(new CellRangeAddress(16,16,11,12));
			page1.addMergedRegion(new CellRangeAddress(16,16,13,15));
			for(int i=17;i<=28;i++)
			{
				page1.addMergedRegion(new CellRangeAddress(i,i,11,12));
				page1.addMergedRegion(new CellRangeAddress(i,i,13,15));
			}
			page1.addMergedRegion(new CellRangeAddress(29,29,0,9));
			page1.addMergedRegion(new CellRangeAddress(29,29,13,15));
			page1.addMergedRegion(new CellRangeAddress(30,30,0,9));
			page1.addMergedRegion(new CellRangeAddress(30,30,13,15));
			page1.addMergedRegion(new CellRangeAddress(31,31,13,15));
			page1.addMergedRegion(new CellRangeAddress(32,32,12,15));
			page1.addMergedRegion(new CellRangeAddress(33,33,12,15));
			page1.addMergedRegion(new CellRangeAddress(36,36,12,15));
			page1.addMergedRegion(new CellRangeAddress(37,37,12,15));
			////////////////////////////////////////////////////////////////////////
			
			page2.addMergedRegion(new CellRangeAddress(0,0,0,15));
			page2.addMergedRegion(new CellRangeAddress(1,1,0,15));
			page2.addMergedRegion(new CellRangeAddress(2,2,0,15));
			page2.addMergedRegion(new CellRangeAddress(3,3,0,15));
			page2.addMergedRegion(new CellRangeAddress(4,4,0,15));
			page2.addMergedRegion(new CellRangeAddress(15,15,0,1));
			page2.addMergedRegion(new CellRangeAddress(15,15,2,10));
			page2.addMergedRegion(new CellRangeAddress(15,15,11,12));
			page2.addMergedRegion(new CellRangeAddress(15,15,13,15));
			page2.addMergedRegion(new CellRangeAddress(16,16,0,1));
			page2.addMergedRegion(new CellRangeAddress(16,16,2,10));
			page2.addMergedRegion(new CellRangeAddress(16,16,11,12));
			page2.addMergedRegion(new CellRangeAddress(16,16,13,15));
			for(int i=17;i<=28;i++)
			{
				page2.addMergedRegion(new CellRangeAddress(i,i,11,12));
				page2.addMergedRegion(new CellRangeAddress(i,i,13,15));
			}
			page2.addMergedRegion(new CellRangeAddress(29,29,0,9));
			page2.addMergedRegion(new CellRangeAddress(29,29,13,15));
			page2.addMergedRegion(new CellRangeAddress(30,30,0,9));
			page2.addMergedRegion(new CellRangeAddress(30,30,13,15));
			page2.addMergedRegion(new CellRangeAddress(31,31,13,15));
			page2.addMergedRegion(new CellRangeAddress(32,32,12,15));
			page2.addMergedRegion(new CellRangeAddress(33,33,12,15));
			page2.addMergedRegion(new CellRangeAddress(36,36,12,15));
			page2.addMergedRegion(new CellRangeAddress(37,37,12,15));
			

			
	

			
			
			row_1.createCell(0).setCellValue("บริษัท อาทิตย์ เคมีภัณฑ์ จำกัด (สำนักงานใหญ่)");	      
			row_1.getCell(0).setCellStyle(style_arial_18_bold);
			CellUtil.setAlignment(row_1.getCell(0), workbook, CellStyle.ALIGN_CENTER);

			row_p2_1.createCell(0).setCellValue("บริษัท อาทิตย์ เคมีภัณฑ์ จำกัด (สำนักงานใหญ่)");	      
			row_p2_1.getCell(0).setCellStyle(style_arial_18_bold);
			CellUtil.setAlignment(row_p2_1.getCell(0), workbook, CellStyle.ALIGN_CENTER);
					
			row_2.createCell(0).setCellValue("ARTHITAYA CHEMICAL CO., LTD.");	      
			row_2.getCell(0).setCellStyle(style_arial_18_bold);	 
			CellUtil.setAlignment(row_2.getCell(0), workbook, CellStyle.ALIGN_CENTER);

			row_p2_2.createCell(0).setCellValue("ARTHITAYA CHEMICAL CO., LTD.");	      
			row_p2_2.getCell(0).setCellStyle(style_arial_18_bold);	 
			CellUtil.setAlignment(row_p2_2.getCell(0), workbook, CellStyle.ALIGN_CENTER);

			row_3.createCell(0).setCellValue("123/3-4-5 หมู่ 5 ถ.สุขุมวิท ต.บ้านฉาง อ.บ้านฉาง จ.ระยอง 21130 ");	      
			row_3.getCell(0).setCellStyle(style_arial_10);	 
			CellUtil.setAlignment(row_3.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_3.createCell(0).setCellValue("123/3-4-5 หมู่ 5 ถ.สุขุมวิท ต.บ้านฉาง อ.บ้านฉาง จ.ระยอง 21130 ");	      
			row_p2_3.getCell(0).setCellStyle(style_arial_10);	 
			CellUtil.setAlignment(row_p2_3.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			
			row_4.createCell(0).setCellValue("โทร (038) 602 068 , 082-716 0166 , 081-647 3802");	      
			row_4.getCell(0).setCellStyle(style_arial_11_bold);	 	
			CellUtil.setAlignment(row_4.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_4.createCell(0).setCellValue("โทร (038) 602 068 , 082-716 0166 , 081-647 3802");	      
			row_p2_4.getCell(0).setCellStyle(style_arial_11_bold);	 
			CellUtil.setAlignment(row_p2_4.getCell(0), workbook, CellStyle.ALIGN_CENTER);

			row_5.createCell(0).setCellValue("E-Mail address : arthitsho@gmail.com");	      
			row_5.getCell(0).setCellStyle(style_arial_11);	  
			CellUtil.setAlignment(row_5.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_5.createCell(0).setCellValue("E-Mail address : arthitsho@gmail.com");	      
			row_p2_5.getCell(0).setCellStyle(style_arial_11);	  
			CellUtil.setAlignment(row_p2_5.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			
			
			
			row_7.createCell(0).setCellValue("(ต้นฉบับ)ใบเสร็จรับเงิน/ใบกำกับภาษี");	    
			row_7.getCell(0).setCellStyle(style_arial_11_bold);	 
			row_7.createCell(12).setCellValue(createRichTextString(workbook,"เลขที่","Arial",10));
			row_7.getCell(12).setCellStyle(style_arial_10);	  
			row_7.createCell(13).setCellValue(createRichTextString(workbook,inv_no,"Arial",12));
			
			row_p2_7.createCell(0).setCellValue("(สำเนา)ใบเสร็จรับเงิน/ใบกำกับภาษี");	      
			row_p2_7.createCell(12).setCellValue(createRichTextString(workbook,"เลขที่","Arial",10));
			row_p2_7.getCell(12).setCellStyle(style_arial_10);	  
			row_p2_7.createCell(13).setCellValue(createRichTextString(workbook,inv_no,"Arial",12));
			
			row_9.createCell(0).setCellValue(createRichTextString(workbook,"เลขประจำตัวผู้เสียภาษีอากร 0 2 1 5 5 4 5 0 0 0 3 2 0","Arial",10));
			row_9.getCell(0).setCellStyle(style_arial_10);	  
			row_9.createCell(12).setCellValue(createRichTextString(workbook,"วันที่","Arial",10));
			row_9.getCell(12).setCellStyle(style_arial_10);	  
			

			row_p2_9.createCell(0).setCellValue(createRichTextString(workbook,"เลขประจำตัวผู้เสียภาษีอากร 0 2 1 5 5 4 5 0 0 0 3 2 0","Arial",10));
			row_p2_9.getCell(0).setCellStyle(style_arial_10);	  
			row_p2_9.createCell(12).setCellValue(createRichTextString(workbook,"วันที่","Arial",10));
			row_p2_9.getCell(12).setCellStyle(style_arial_10);	  
			
			row_10.createCell(12).setCellValue(createRichTextString(workbook,"Date","Arial",10));
			row_10.getCell(12).setCellStyle(style_arial_10);	
			row_10.createCell(13).setCellValue(createRichTextString(workbook,bill_date,"Arial",10));
			
			row_p2_10.createCell(12).setCellValue(createRichTextString(workbook,"Date","Arial",10));
			row_p2_10.getCell(12).setCellStyle(style_arial_10);	
			row_p2_10.createCell(13).setCellValue(createRichTextString(workbook,bill_date,"Arial",10));
		    
			row_11.createCell(0).setCellValue(createRichTextString(workbook,"ผู้ซื้อ / BUYER","Arial",10));
			row_11.getCell(0).setCellStyle(style_arial_10);	
			row_11.createCell(3).setCellValue(createRichTextString(workbook,customer_name,"Arial",10));

			
			row_p2_11.createCell(0).setCellValue(createRichTextString(workbook,"ผู้ซื้อ / BUYER","Arial",10));
			row_p2_11.getCell(0).setCellStyle(style_arial_10);	
			row_p2_11.createCell(3).setCellValue(createRichTextString(workbook,customer_name,"Arial",10));

			
			row_12.createCell(0).setCellValue(createRichTextString(workbook,"ที่อยู่ / ADDRESS","Arial",10));
			row_12.getCell(0).setCellStyle(style_arial_10);	
			row_12.createCell(3).setCellValue(createRichTextString(workbook,address_line0,"Arial",10));

			row_p2_12.createCell(0).setCellValue(createRichTextString(workbook,"ที่อยู่ / ADDRESS","Arial",10));
			row_p2_12.getCell(0).setCellStyle(style_arial_10);	
			row_p2_12.createCell(3).setCellValue(createRichTextString(workbook,address_line0,"Arial",10));
			
			row_13.createCell(3).setCellValue(createRichTextString(workbook,address_line1,"Arial",10));
			row_p2_13.createCell(3).setCellValue(createRichTextString(workbook,address_line1,"Arial",10));
			
			row_14.createCell(3).setCellValue(createRichTextString(workbook,"Tax ID:"+tax_id,"Arial",10));
			row_p2_14.createCell(3).setCellValue(createRichTextString(workbook,"Tax ID:"+tax_id,"Arial",10));
			
			row_16.createCell(0).setCellValue(createRichTextString(workbook,"จำนวน","Arial",10));
			row_16.getCell(0).setCellStyle(style_border_top_left);
			
			row_16.createCell(1);
			row_16.getCell(1).setCellStyle(style_border_top_right);
			for(int t=3;t<=10;t++)
			{
				row_16.createCell(t);
				row_16.getCell(t).setCellStyle(style_border_top);
			}
			
			row_16.createCell(2).setCellValue(createRichTextString(workbook,"รายการ","Arial",10));
			row_16.getCell(2).setCellStyle(style_border_top_right);
			row_16.createCell(11).setCellValue(createRichTextString(workbook,"หน่วยละ","Arial",10));
			row_16.getCell(11).setCellStyle(style_border_top_left);	
			row_16.createCell(12);
			row_16.getCell(12).setCellStyle(style_border_top);
			row_16.createCell(13).setCellValue(createRichTextString(workbook,"จำนวนเงิน","Arial",10));
			row_16.getCell(13).setCellStyle(style_border_top_left);
			row_16.createCell(14);
			row_16.getCell(14).setCellStyle(style_border_top);
			row_16.createCell(15);
			row_16.getCell(15).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_16.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_16.getCell(2), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_16.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_16.getCell(13), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_16.createCell(0).setCellValue(createRichTextString(workbook,"จำนวน","Arial",10));
			row_p2_16.getCell(0).setCellStyle(style_border_top_left);
			
			row_p2_16.createCell(1);
			row_p2_16.getCell(1).setCellStyle(style_border_top_right);
			for(int t=3;t<=10;t++)
			{
				row_p2_16.createCell(t);
				row_p2_16.getCell(t).setCellStyle(style_border_top);
			}
			row_p2_16.createCell(2).setCellValue(createRichTextString(workbook,"รายการ","Arial",10));
			row_p2_16.getCell(2).setCellStyle(style_border_top_right);
			row_p2_16.createCell(11).setCellValue(createRichTextString(workbook,"หน่วยละ","Arial",10));
			row_p2_16.getCell(11).setCellStyle(style_border_top_left);	
			row_p2_16.createCell(12);
			row_p2_16.getCell(12).setCellStyle(style_border_top);
			row_p2_16.createCell(13).setCellValue(createRichTextString(workbook,"จำนวนเงิน","Arial",10));
			row_p2_16.getCell(13).setCellStyle(style_border_top_left);
			row_p2_16.createCell(14);
			row_p2_16.getCell(14).setCellStyle(style_border_top);
			row_p2_16.createCell(15);
			row_p2_16.getCell(15).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_p2_16.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p2_16.getCell(2), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p2_16.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p2_16.getCell(13), workbook, CellStyle.ALIGN_CENTER);
			
			
		
			row_17.createCell(0).setCellValue(createRichTextString(workbook,"QUANTITY","Arial",10));
			row_17.getCell(0).setCellStyle(style_border_bot_left);	
			row_17.createCell(1);
			row_17.getCell(1).setCellStyle(style_border_bot_right);	
			for(int t=3;t<=10;t++)
			{
				row_17.createCell(t);
				row_17.getCell(t).setCellStyle(style_border_bot);
			}
			row_17.createCell(2).setCellValue(createRichTextString(workbook,"DESCRIPTION","Arial",10));
			row_17.getCell(2).setCellStyle(style_border_bot_right);
			row_17.createCell(11).setCellValue(createRichTextString(workbook,"UNIT PRICE","Arial",10));
			row_17.getCell(11).setCellStyle(style_border_bot_left);	
			row_17.createCell(12);
			row_17.getCell(12).setCellStyle(style_border_bot);
			row_17.createCell(13).setCellValue(createRichTextString(workbook,"AMOUNT","Arial",10));
			row_17.getCell(13).setCellStyle(style_border_bot_left);
			row_17.createCell(14);
			row_17.getCell(14).setCellStyle(style_border_bot);
			row_17.createCell(15);
			row_17.getCell(15).setCellStyle(style_border_bot_right);
			CellUtil.setAlignment(row_17.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_17.getCell(2), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_17.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_17.getCell(13), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_17.createCell(0).setCellValue(createRichTextString(workbook,"QUANTITY","Arial",10));
			row_p2_17.getCell(0).setCellStyle(style_border_bot_left);	
			row_p2_17.createCell(1);
			row_p2_17.getCell(1).setCellStyle(style_border_bot_right);	
			for(int t=3;t<=10;t++)
			{
				row_p2_17.createCell(t);
				row_p2_17.getCell(t).setCellStyle(style_border_bot);
			}
			row_p2_17.createCell(2).setCellValue(createRichTextString(workbook,"DESCRIPTION","Arial",10));
			row_p2_17.getCell(2).setCellStyle(style_border_bot_right);
			row_p2_17.createCell(11).setCellValue(createRichTextString(workbook,"UNIT PRICE","Arial",10));
			row_p2_17.getCell(11).setCellStyle(style_border_bot_left);	
			row_p2_17.createCell(12);
			row_p2_17.getCell(12).setCellStyle(style_border_bot);
			row_p2_17.createCell(13).setCellValue(createRichTextString(workbook,"AMOUNT","Arial",10));
			row_p2_17.getCell(13).setCellStyle(style_border_bot_left);
			row_p2_17.createCell(14);
			row_p2_17.getCell(14).setCellStyle(style_border_bot);
			row_p2_17.createCell(15);
			row_p2_17.getCell(15).setCellStyle(style_border_bot_right);
			CellUtil.setAlignment(row_p2_17.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p2_17.getCell(2), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p2_17.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p2_17.getCell(13), workbook, CellStyle.ALIGN_CENTER);
		
			
			double total_value_fixed =  Double.parseDouble(total_value);
			DecimalFormat formatter = new DecimalFormat("#,###.00");
			
			row_30.createCell(0).setCellValue(createRichTextString(workbook,"จำนวนเงินรวมทั้งสิ้น ( ตัวอักษร )","Arial",10));
			row_30.getCell(0).setCellStyle(style_border_top_left);
			for(int t=1;t<=9;t++)
			{
				row_30.createCell(t);
				row_30.getCell(t).setCellStyle(style_border_top);
			}
			row_30.createCell(10).setCellValue(createRichTextString(workbook,"รวมราคาทั้งสิ้น","Arial",10));
			row_30.getCell(10).setCellStyle(style_border_top_left);	
			row_30.createCell(11);
			row_30.getCell(11).setCellStyle(style_border_top);
			row_30.createCell(12);
			row_30.getCell(12).setCellStyle(style_border_top);
			row_30.createCell(13);
			row_30.getCell(13).setCellStyle(style_border_top_left);
			row_30.getCell(13).setCellValue(createRichTextString(workbook,formatter.format(total_value_fixed),"Arial",10));
			row_30.createCell(14);
			row_30.getCell(14).setCellStyle(style_border_top);
			row_30.createCell(15);
			row_30.getCell(15).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_30.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_30.getCell(13), workbook, CellStyle.ALIGN_RIGHT);
			
			
			
			row_p2_30.createCell(0).setCellValue(createRichTextString(workbook,"จำนวนเงินรวมทั้งสิ้น ( ตัวอักษร )","Arial",10));
			row_p2_30.getCell(0).setCellStyle(style_border_top_left);
			for(int t=1;t<=9;t++)
			{
				row_p2_30.createCell(t);
				row_p2_30.getCell(t).setCellStyle(style_border_top);
			}
			row_p2_30.createCell(10).setCellValue(createRichTextString(workbook,"รวมราคาทั้งสิ้น","Arial",10));
			row_p2_30.getCell(10).setCellStyle(style_border_top_left);	
			row_p2_30.createCell(11);
			row_p2_30.getCell(11).setCellStyle(style_border_top);
			row_p2_30.createCell(12);
			row_p2_30.getCell(12).setCellStyle(style_border_top);
			row_p2_30.createCell(13);
			row_p2_30.getCell(13).setCellStyle(style_border_top_left);
			row_p2_30.getCell(13).setCellValue(createRichTextString(workbook,formatter.format(total_value_fixed),"Arial",10));
			row_p2_30.createCell(14);
			row_p2_30.getCell(14).setCellStyle(style_border_top);
			row_p2_30.createCell(15);
			row_p2_30.getCell(15).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_p2_30.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p2_30.getCell(13), workbook, CellStyle.ALIGN_RIGHT);
			
			
			double total_vat_fixed =  Double.parseDouble(total_vat);
			
			row_31.createCell(0);
			row_31.getCell(0).setCellStyle(style_border_left);
			row_31.createCell(10).setCellValue(createRichTextString(workbook,"จำนวนภาษีมูลค่าเพิ่ม","Arial",10));
			row_31.getCell(10).setCellStyle(style_border_left);
			row_31.createCell(13);
			row_31.getCell(13).setCellStyle(style_border_top_left);
			row_31.getCell(13).setCellValue(createRichTextString(workbook,formatter.format(total_vat_fixed),"Arial",10));
			row_31.createCell(14);
			row_31.getCell(14).setCellStyle(style_border_top);
			row_31.createCell(15);
			row_31.getCell(15).setCellStyle(style_border_top_right);
			String str_formula_32 = "BAHTTEXT(N32)";
			row_31.getCell(0).setCellType(Cell.CELL_TYPE_FORMULA);
			row_31.getCell(0).setCellFormula(str_formula_32);
			CellUtil.setAlignment(row_31.getCell(13), workbook, CellStyle.ALIGN_RIGHT);
			CellUtil.setAlignment(row_31.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			
			
			
			row_p2_31.createCell(0);
			row_p2_31.getCell(0).setCellStyle(style_border_left);
			row_p2_31.createCell(10).setCellValue(createRichTextString(workbook,"จำนวนภาษีมูลค่าเพิ่ม","Arial",10));
			row_p2_31.getCell(10).setCellStyle(style_border_left);
			row_p2_31.createCell(13);
			row_p2_31.getCell(13).setCellStyle(style_border_top_left);
			row_p2_31.getCell(13).setCellValue(createRichTextString(workbook,formatter.format(total_vat_fixed),"Arial",10));
			row_p2_31.createCell(14);
			row_p2_31.getCell(14).setCellStyle(style_border_top);
			row_p2_31.createCell(15);
			row_p2_31.getCell(15).setCellStyle(style_border_top_right);
			String str_formula_32_2 = "BAHTTEXT(N32)";
			row_p2_31.getCell(0).setCellType(Cell.CELL_TYPE_FORMULA);
			row_p2_31.getCell(0).setCellFormula(str_formula_32_2);
			CellUtil.setAlignment(row_p2_31.getCell(13), workbook, CellStyle.ALIGN_RIGHT);
			CellUtil.setAlignment(row_p2_31.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			

			double total_inc_vat_fixed = Double.parseDouble(total_inc_vat);
			row_32.createCell(0);
			row_32.getCell(0).setCellStyle(style_border_bot_left);
			for(int t=1;t<=9;t++)
			{
				row_32.createCell(t);
				row_32.getCell(t).setCellStyle(style_border_bot);
			}
			row_32.createCell(10).setCellValue(createRichTextString(workbook,"จำนวนเงินรวมทั้งสิ้น","Arial",10));
			row_32.getCell(10).setCellStyle(style_border_bot_left);	
			row_32.createCell(11);
			row_32.getCell(11).setCellStyle(style_border_bot);	
			row_32.createCell(12);
			row_32.getCell(12).setCellStyle(style_border_bot_right);
			row_32.createCell(13);
			row_32.getCell(13).setCellValue(createRichTextString(workbook,formatter.format(total_inc_vat_fixed),"Arial",10));
			row_32.getCell(13).setCellStyle(style_border_top_bot);
			row_32.createCell(14);
			row_32.getCell(14).setCellStyle(style_border_top_bot);
			row_32.createCell(15);
			row_32.getCell(15).setCellStyle(style_border_top_bot_right);
			CellUtil.setAlignment(row_32.getCell(13), workbook, CellStyle.ALIGN_RIGHT);
			
			row_p2_32.createCell(0);
			row_p2_32.getCell(0).setCellStyle(style_border_bot_left);
			for(int t=1;t<=9;t++)
			{
				row_p2_32.createCell(t);
				row_p2_32.getCell(t).setCellStyle(style_border_bot);
			}
			row_p2_32.createCell(10).setCellValue(createRichTextString(workbook,"จำนวนเงินรวมทั้งสิ้น","Arial",10));
			row_p2_32.getCell(10).setCellStyle(style_border_bot_left);	
			row_p2_32.createCell(11);
			row_p2_32.getCell(11).setCellStyle(style_border_bot);	
			row_p2_32.createCell(12);
			row_p2_32.getCell(12).setCellStyle(style_border_bot_right);
			row_p2_32.createCell(13);
			row_p2_32.getCell(13).setCellStyle(style_border_top_bot);
			row_p2_32.getCell(13).setCellValue(createRichTextString(workbook,formatter.format(total_inc_vat_fixed),"Arial",10));
			row_p2_32.createCell(14);
			row_p2_32.getCell(14).setCellStyle(style_border_top_bot);
			row_p2_32.createCell(15);
			row_p2_32.getCell(15).setCellStyle(style_border_top_bot_right);
			CellUtil.setAlignment(row_p2_32.getCell(13), workbook, CellStyle.ALIGN_RIGHT);
			
			
			row_33.createCell(0).setCellStyle(style_border_left);
			row_33.createCell(1).setCellValue("เงินสด");		
			row_33.createCell(12).setCellValue(createRichTextString(workbook,"บริษัท อาทิตย์ เคมีภัณฑ์ จำกัด","Arial",10));
			row_33.getCell(12).setCellStyle(style_border_left);
			row_33.createCell(15);
			row_33.getCell(15).setCellStyle(style_border_right);
			CellUtil.setAlignment(row_33.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			
			row_p2_33.createCell(0).setCellStyle(style_border_left);
			row_p2_33.createCell(1).setCellValue("เงินสด");		
			row_p2_33.createCell(12).setCellValue(createRichTextString(workbook,"บริษัท อาทิตย์ เคมีภัณฑ์ จำกัด","Arial",10));
			row_p2_33.getCell(12).setCellStyle(style_border_left);
			row_p2_33.createCell(15);
			row_p2_33.getCell(15).setCellStyle(style_border_right);
			CellUtil.setAlignment(row_p2_33.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			
			row_34.createCell(0).setCellStyle(style_border_left);
			row_34.createCell(1).setCellValue("เช็คธนาคาร / สาขา");
			row_34.createCell(12).setCellValue(createRichTextString(workbook,"ARTHITAYA CHEMICAL CO., LTD.","Arial",9));
			row_34.getCell(12).setCellStyle(style_border_left);
			row_34.createCell(15);
			row_34.getCell(15).setCellStyle(style_border_right);
			CellUtil.setAlignment(row_34.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_34.createCell(0).setCellStyle(style_border_left);
			row_p2_34.createCell(1).setCellValue("เช็คธนาคาร / สาขา");
			row_p2_34.createCell(12).setCellValue(createRichTextString(workbook,"ARTHITAYA CHEMICAL CO., LTD.","Arial",9));
			row_p2_34.getCell(12).setCellStyle(style_border_left);
			row_p2_34.createCell(15);
			row_p2_34.getCell(15).setCellStyle(style_border_right);
			CellUtil.setAlignment(row_p2_34.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			
			row_35.createCell(0);
			row_35.getCell(0).setCellStyle(style_border_left);
			row_35.createCell(12);
			row_35.getCell(12).setCellStyle(style_border_left);
			row_35.createCell(15);
			row_35.getCell(15).setCellStyle(style_border_right);
			row_35.getCell(0).setCellValue("เลขที่เช็ค");
			row_35.createCell(4).setCellValue("ผู้รับสินค้า");
			row_35.createCell(8).setCellValue("วันที่ ");
			CellUtil.setAlignment(row_35.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			

			row_p2_35.createCell(0);
			row_p2_35.getCell(0).setCellStyle(style_border_left);
			row_p2_35.createCell(12);
			row_p2_35.getCell(12).setCellStyle(style_border_left);
			row_p2_35.createCell(15);
			row_p2_35.getCell(15).setCellStyle(style_border_right);
			row_p2_35.getCell(0).setCellValue("เลขที่เช็ค");
			row_p2_35.createCell(4).setCellValue("ผู้รับสินค้า");
			row_p2_35.createCell(8).setCellValue("วันที่ ");
			CellUtil.setAlignment(row_p2_35.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_36.createCell(0);
			row_36.getCell(0).setCellStyle(style_border_left);
			row_36.getCell(0).setCellValue(createRichTextString(workbook,"วันที่","Arial",10));	
			row_36.createCell(12);
			row_36.getCell(12).setCellStyle(style_border_left);
			row_36.createCell(15);
			row_36.getCell(15).setCellStyle(style_border_right);
			CellUtil.setAlignment(row_36.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_36.createCell(0);
			row_p2_36.getCell(0).setCellStyle(style_border_left);
			row_p2_36.getCell(0).setCellValue(createRichTextString(workbook,"วันที่","Arial",10));	
			row_p2_36.createCell(12);
			row_p2_36.getCell(12).setCellStyle(style_border_left);
			row_p2_36.createCell(15);
			row_p2_36.getCell(15).setCellStyle(style_border_right);
			CellUtil.setAlignment(row_p2_36.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_37.createCell(0);
			row_37.createCell(4);
			row_37.createCell(8);
			row_37.createCell(11);
			row_37.getCell(0).setCellStyle(style_border_left);
			row_37.createCell(12).setCellValue(createRichTextString(workbook,"ผู้มีอำนาจลงนาม","Arial",10));
			row_37.getCell(12).setCellStyle(style_border_left);
			row_37.createCell(15);
			row_37.getCell(15).setCellStyle(style_border_right);
			row_37.getCell(0).setCellValue(createRichTextString(workbook,"ผู้รับเงิน","Arial",10));
			row_37.getCell(4).setCellValue("ผู้ส่งสินค้า");
			row_37.getCell(8).setCellValue("วันที่ ");
			CellUtil.setAlignment(row_37.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_37.createCell(0);
			row_p2_37.createCell(4);
			row_p2_37.createCell(8);
			row_p2_37.createCell(11);
			row_p2_37.getCell(0).setCellStyle(style_border_left);
			row_p2_37.createCell(12).setCellValue(createRichTextString(workbook,"ผู้มีอำนาจลงนาม","Arial",10));
			row_p2_37.getCell(12).setCellStyle(style_border_left);
			row_p2_37.createCell(15);
			row_p2_37.getCell(15).setCellStyle(style_border_right);
			row_p2_37.getCell(0).setCellValue(createRichTextString(workbook,"ผู้รับเงิน","Arial",10));
			row_p2_37.getCell(4).setCellValue("ผู้ส่งสินค้า");
			row_p2_37.getCell(8).setCellValue("วันที่ ");
			CellUtil.setAlignment(row_p2_37.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			

			row_38.createCell(0);
			row_38.getCell(0).setCellStyle(style_border_bot_left);
			for(int t=1;t<=11;t++)
			{
				row_38.createCell(t);
				row_38.getCell(t).setCellStyle(style_border_bot);
			}
			row_38.createCell(12).setCellValue(createRichTextString(workbook,"Authorized Signature","Arial",10));
			row_38.getCell(12).setCellStyle(style_border_bot_left);	
			row_38.createCell(13);
			row_38.getCell(13).setCellStyle(style_border_bot);
			row_38.createCell(14);
			row_38.getCell(14).setCellStyle(style_border_bot);
			row_38.createCell(15);
			row_38.getCell(15).setCellStyle(style_border_bot_right);
			CellUtil.setAlignment(row_38.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_38.createCell(0);
			row_p2_38.getCell(0).setCellStyle(style_border_bot_left);
			for(int t=1;t<=11;t++)
			{
				row_p2_38.createCell(t);
				row_p2_38.getCell(t).setCellStyle(style_border_bot);
			}
			row_p2_38.createCell(12).setCellValue(createRichTextString(workbook,"Authorized Signature","Arial",10));
			row_p2_38.getCell(12).setCellStyle(style_border_bot_left);	
			row_p2_38.createCell(13);
			row_p2_38.getCell(13).setCellStyle(style_border_bot);
			row_p2_38.createCell(14);
			row_p2_38.getCell(14).setCellStyle(style_border_bot);
			row_p2_38.createCell(15);
			row_p2_38.getCell(15).setCellStyle(style_border_bot_right);
			CellUtil.setAlignment(row_p2_38.getCell(12), workbook, CellStyle.ALIGN_CENTER);
		
			row_39.createCell(0).setCellValue(createRichTextString(workbook,"ในกรณีชำระเป็นเช็ค ใบเสร็จรับเงินนี้จะสมบูรณ์ต่อเมื่อ ธนาคารได้เรียกเก็บเงินตามเช็คเข้าบัญชีของบริษัทฯเรียบร้อยแล้ว","Arial",9));
			row_39.getCell(0).setCellStyle(style_arial_9);	
			
			row_p2_39.createCell(0).setCellValue(createRichTextString(workbook,"ในกรณีชำระเป็นเช็ค ใบเสร็จรับเงินนี้จะสมบูรณ์ต่อเมื่อ ธนาคารได้เรียกเก็บเงินตามเช็คเข้าบัญชีของบริษัทฯเรียบร้อยแล้ว","Arial",9));
			row_p2_39.getCell(0).setCellStyle(style_arial_9);	
			
			row_40.createCell(0).setCellValue(createRichTextString(workbook,"If payment is made by cheque, this receipt will be valid, Until the cheque is honoured by the bank","Arial",9));
			row_40.getCell(0).setCellStyle(style_arial_9);	
			
			row_p2_40.createCell(0).setCellValue(createRichTextString(workbook,"If payment is made by cheque, this receipt will be valid, Until the cheque is honoured by the bank","Arial",9));
			row_p2_40.getCell(0).setCellStyle(style_arial_9);	
			
			DrawListCellBorder(workbook,row_18);
			DrawListCellBorder(workbook,row_19);
			DrawListCellBorder(workbook,row_20);
			DrawListCellBorder(workbook,row_21);
			DrawListCellBorder(workbook,row_22);
			DrawListCellBorder(workbook,row_23);
			DrawListCellBorder(workbook,row_24);
			DrawListCellBorder(workbook,row_25);
			DrawListCellBorder(workbook,row_26);
			DrawListCellBorder(workbook,row_27);
			DrawListCellBorder(workbook,row_28);
			DrawListCellBorder(workbook,row_29);
			
			DrawListCellBorder(workbook,row_p2_18);
			DrawListCellBorder(workbook,row_p2_19);
			DrawListCellBorder(workbook,row_p2_20);
			DrawListCellBorder(workbook,row_p2_21);
			DrawListCellBorder(workbook,row_p2_22);
			DrawListCellBorder(workbook,row_p2_23);
			DrawListCellBorder(workbook,row_p2_24);
			DrawListCellBorder(workbook,row_p2_25);
			DrawListCellBorder(workbook,row_p2_26);
			DrawListCellBorder(workbook,row_p2_27);
			DrawListCellBorder(workbook,row_p2_28);
			DrawListCellBorder(workbook,row_p2_29);
		
	
			
	
			int count_detail = 0;
			for(int q=17;count_detail<cash_bill_detail_list.size();count_detail++,q++)
			{
				String temp_price = cash_bill_detail_list.get(count_detail).getPrice();
				String temp_sum = cash_bill_detail_list.get(count_detail).getSum();
				
				double fix_price = Double.parseDouble(temp_price);
				double fix_sum =  Double.parseDouble(temp_sum);
				
			
				
				if(temp_price.indexOf('.')<0)
				{
					temp_price = temp_price + ".00";
					
				}else{
					
				}
				if(temp_sum.indexOf('.')<0)
				{
					temp_sum = temp_sum + ".00";
					
				}else{
					
				}
				
				
				page1.getRow(q).createCell(2);
				page1.getRow(q).getCell(0).setCellValue(createRichTextString(workbook,cash_bill_detail_list.get(count_detail).getQuantity(),"Arial",10));
				page1.getRow(q).getCell(1).setCellValue(createRichTextString(workbook,cash_bill_detail_list.get(count_detail).getProductUnit(),"Arial",10));
				page1.getRow(q).getCell(2).setCellValue(createRichTextString(workbook,cash_bill_detail_list.get(count_detail).getAdtDescription(),"Arial",10));
				page1.getRow(q).getCell(11).setCellValue(createRichTextString(workbook,formatter.format(fix_price),"Arial",10));
				page1.getRow(q).getCell(13).setCellValue(createRichTextString(workbook,formatter.format(fix_sum),"Arial",10));
				
				CellUtil.setAlignment(page1.getRow(q).getCell(0), workbook, CellStyle.ALIGN_CENTER);
				CellUtil.setAlignment(page1.getRow(q).getCell(1), workbook, CellStyle.ALIGN_CENTER);
				CellUtil.setAlignment(page1.getRow(q).getCell(11), workbook, CellStyle.ALIGN_RIGHT);
				CellUtil.setAlignment(page1.getRow(q).getCell(13), workbook, CellStyle.ALIGN_RIGHT);
				
				
				page2.getRow(q).createCell(2);
				page2.getRow(q).getCell(0).setCellValue(createRichTextString(workbook,cash_bill_detail_list.get(count_detail).getQuantity(),"Arial",10));
				page2.getRow(q).getCell(1).setCellValue(createRichTextString(workbook,cash_bill_detail_list.get(count_detail).getProductUnit(),"Arial",10));
				page2.getRow(q).getCell(2).setCellValue(createRichTextString(workbook,cash_bill_detail_list.get(count_detail).getAdtDescription(),"Arial",10));
				page2.getRow(q).getCell(11).setCellValue(createRichTextString(workbook,formatter.format(fix_price),"Arial",10));
				page2.getRow(q).getCell(13).setCellValue(createRichTextString(workbook,formatter.format(fix_sum),"Arial",10));
				
				CellUtil.setAlignment(page2.getRow(q).getCell(0), workbook, CellStyle.ALIGN_CENTER);
				CellUtil.setAlignment(page2.getRow(q).getCell(1), workbook, CellStyle.ALIGN_CENTER);
				CellUtil.setAlignment(page2.getRow(q).getCell(11), workbook, CellStyle.ALIGN_RIGHT);
				CellUtil.setAlignment(page2.getRow(q).getCell(13), workbook, CellStyle.ALIGN_RIGHT);
				
			
			}
			
			
			
			workbook.write(new FileOutputStream(fileName));
			
			
			String file_path = fileName;
			String new_status ="";
			
		
			
				String update_new_status =  " UPDATE `cash_bill_main` SET `status`='inv_generated'"
										   +" WHERE cash_bill_main.cash_bill_id ='"+cash_bill_id+"'";
				connect.createStatement().executeUpdate(update_new_status);
				

		    System.out.println("Word document created to : " + application.getRealPath("/report/"));
			
		    
			String sql_update_file_path = " UPDATE `cash_bill_main` SET `inv_file_path`= ? "
											 +" , `inv_file_name`= ? "
										     +" , `inv_generated_datetime`= NOW() "
											 +" WHERE cash_bill_main.cash_bill_id = ? ";
				
			PreparedStatement preparedStatement = null;
			preparedStatement = connect.prepareStatement(sql_update_file_path);
					    
			System.out.println("sql_update_file_path:"+sql_update_file_path);
		    
		   
			String pattern = Pattern.quote(System.getProperty("file.separator"));
			String[] parts = file_path.split(pattern);
			int temp = parts.length;
			String inv_file_name = parts[temp-1];
			
			String output = inv_file_name+"&"+file_path;
		    System.out.println("output:"+output);
		    
		    
			preparedStatement.setString(1, file_path);
			preparedStatement.setString(2, inv_file_name);
			preparedStatement.setString(3, cash_bill_id);
			
			preparedStatement.executeUpdate();
			
			out.print(output);
			
	} catch (Exception e) {
		e.printStackTrace();
		  connect.close();
		out.print("fail");
	}
	///////////////////////////////////////////////////////End Writing Excel Process/////////////////////////////////////////////////////////
	 
	connect.close();
%>
