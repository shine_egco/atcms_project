<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    
    
    
   
  

    <title> ATCMS Prototype</title>

    <!-- 
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <!--  Bootstrap Core CSS -->
   <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
   

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
	
    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../bower_components/sidebar/sidebar_style_3.css" rel="stylesheet" type="text/css">
   

 <!--  
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
 	 -->


      <% 
    	System.out.println("Start dashboard_main ");
  		//set Database Connection
  		String hostProps = "";
  		String usernameProps  = "";
  		String passwordProps  = "";
  		String databaseProps = "";
  		
  		try {
  			ServletContext servletContext = request.getSession().getServletContext();
  			
  			InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
  			Properties props = new Properties();
  			
  			props.load(input);

  			hostProps  = props.getProperty("host");
  			usernameProps  = props.getProperty("username");
  			passwordProps  = props.getProperty("password");
  			databaseProps = props.getProperty("database");
  		} catch (Exception e) { 
  			out.println(e);  
  		}
  	
  		// connect database
  		Statement statement = null;
		Connection connect = null;	
		
		List<Order> order_list = new ArrayList<Order>();
		List<CashBill> cash_bill_list = new ArrayList<CashBill>();
		List<ShortBill> short_bill_list = new ArrayList<ShortBill>();
		List<Quotation> quotation_list = new ArrayList<Quotation>();
		
  		try {
				Class.forName("com.mysql.jdbc.Driver");
				
				Date current_date = new Date();
				
				String current_year = current_date.getYear()+1900+"";
				String current_month = current_date.getMonth()+1+"";
	
				connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
						"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");//////"&characterEncoding=tis620" Encoding Thai);
	
				if(connect != null){
				   System.out.println("Database Connect Sucesses.");
				   
				   String sql_query_credit_bill = " SELECT order_main.order_id , order_main.status "+
				   					  " ,company.company_id ,company.name_th , company.name_en ,order_main.index "  +
				   					  " ,order_main.inv_no "+
					         " FROM order_main "+
							 " LEFT JOIN company "+
							 " ON order_main.customer_id = company.company_id "+
							 " WHERE company.type = 'customer'"+
							 " AND YEAR(order_main.delivery_date) LIKE '%"+current_year+"%'"+
							 " ORDER BY order_main.index DESC "+
							 " LIMIT 50 ";
				   
				   System.out.println("sql_query_credit_bill:"+sql_query_credit_bill);
				   System.out.println("order_main.delivery_date:"+current_year);
			 	   ResultSet rs_credit_bill = connect.createStatement().executeQuery(sql_query_credit_bill);
			 	   
			 	   
			 	   	
				 	  while(rs_credit_bill.next())
					  {
					 		if(!("00/0000").equals(rs_credit_bill.getString("inv_no")))  
							{
					 			 Order ord = new Order();
									ord.setCustomerId(rs_credit_bill.getString("company_id"));
									ord.setOrderId(rs_credit_bill.getString("order_id"));
									ord.setOrderStatus(rs_credit_bill.getString("status"));
									if(("-").equals(rs_credit_bill.getString("name_th")))
									{
										ord.setCustomerName(rs_credit_bill.getString("name_en"));
									}else{
										ord.setCustomerName(rs_credit_bill.getString("name_th"));									
									}
									ord.setInvNo(rs_credit_bill.getString("inv_no"));
					 		   
					 		   order_list.add(ord);
							}
					 		  
					  }
				 	  /////////////////////////////////////////////////////////////////////////////////////////////////
				 	 String sql_query_cash_bill = " SELECT cash_bill_main.cash_bill_id , cash_bill_main.status "+
		   					  " ,company.company_id ,company.name_th , company.name_en ,cash_bill_main.index "  +
		   					  " ,cash_bill_main.inv_no "+
			         " FROM cash_bill_main "+
					 " LEFT JOIN company "+
					 " ON cash_bill_main.customer_id = company.company_id "+
					 " WHERE company.type = 'customer'"+
					 " AND YEAR(cash_bill_main.bill_date)='"+current_year+"'"+
					 " ORDER BY cash_bill_main.index DESC "+
					 " LIMIT 50 ";
				 	  
				 	 System.out.println("sql_query_cash_bill:"+sql_query_cash_bill);
				 	 ResultSet rs_cash_bill = connect.createStatement().executeQuery(sql_query_cash_bill);
				  	
				 	  while(rs_cash_bill.next())
					  {
					 		if(!("c00/0000").equals(rs_cash_bill.getString("inv_no")))  
							{
					 			CashBill cash = new CashBill();
					 			cash.setCustomerId(rs_cash_bill.getString("company_id"));
					 			cash.setCashBillId(rs_cash_bill.getString("cash_bill_id"));
					 			cash.setStatus(rs_cash_bill.getString("status"));
									if(("-").equals(rs_cash_bill.getString("name_th"))||(("").equals(rs_cash_bill.getString("name_th"))))
									{
										cash.setCustomerName(rs_cash_bill.getString("name_en"));
									}else{
										cash.setCustomerName(rs_cash_bill.getString("name_th"));									
									}
									cash.setInvNo(rs_cash_bill.getString("inv_no"));
					 		   
									cash_bill_list.add(cash);
							}
					 		  
					  }
				 	  
				 	  
				 	  
				 	  ///////////////////////////////////////////////////////////////////////////////////////////////
				 	  
				 	  
				 	  
				 	   String sql_query_short_bill = " SELECT * "+
			         " FROM short_bill_main "+
					 " ORDER BY short_bill_main.inv_no DESC "+
					 " LIMIT 50 ";
				 	  
				 	 System.out.println("sql_query_short_bill:"+sql_query_short_bill);
				 	 ResultSet rs_short_bill = connect.createStatement().executeQuery(sql_query_short_bill);
				  	
				 	  while(rs_short_bill.next())
					  {
				 		 //System.out.println("Active 1 ");
					 		if(!("SB000000").equals(rs_short_bill.getString("inv_no")))  
							{
					 			// System.out.println("Active 2 ");
					 			ShortBill shortbill = new ShortBill();
					 		//	System.out.println("Active 3 ");
					 			shortbill.setshortBillId(rs_short_bill.getString("short_bill_id"));
					 		//	System.out.println("Active  4");
					 			shortbill.setCustomerName(rs_short_bill.getString("customer_name"));
					 		//	System.out.println("Active 5 ");
					 			shortbill.setCustomerTel(rs_short_bill.getString("customer_tel"));
					 		//	System.out.println("Active 6 ");
								shortbill.setInvNo(rs_short_bill.getString("inv_no"));
							//	System.out.println("Active 7 ");
								short_bill_list.add(shortbill);
							//	 System.out.println("Active 8 ");
							}
					 		  
					  }
				 	  
				 	 /////////////////////////////////////////////////////////////////////////////////////////////////
				 	 
					 	 String sql_query_quotation = " SELECT *"+
				         " FROM quotation_main "+
						 " LEFT JOIN company "+
						 " ON quotation_main.customer_id = company.company_id "+
						 " WHERE  YEAR(quotation_main.quotation_date)='"+current_year+"'"+
						 " ORDER BY quotation_main.quotation_no DESC "+
						 " LIMIT 50 ";
					 	  
					 	 System.out.println("sql_query_quotation:"+sql_query_quotation);
					 	 ResultSet rs_quotation = connect.createStatement().executeQuery(sql_query_quotation);
					  	
					 	  while(rs_quotation.next())
						  {
					 		 Quotation quo_unit = new Quotation();
					 		 
					 		 if((rs_quotation.getString("name_th").equals("-"))||(rs_quotation.getString("name_th").equals("")))
					 		 {
					 			 quo_unit.setCustomerName(rs_quotation.getString("name_en"));
					 		 }else{
					 			quo_unit.setCustomerName(rs_quotation.getString("name_th"));
					 		 }
					 			 		 
					 		 
					 		 quo_unit.setQuotationNo(rs_quotation.getString("quotation_no"));
					 		 quo_unit.setQuotationDate(rs_quotation.getString("quotation_date"));
					 		 quo_unit.setTotalValue(rs_quotation.getString("total_value"));
					 		quo_unit.setQuotationId(rs_quotation.getString("quotation_id"));
					 		
					 		 quotation_list.add(quo_unit);
						 		  
						  }
				 	  
				 	  
				  
	
				  } else {
					  
			       System.out.println("Database Connect Failed.");	
			       
			      }

		} catch (Exception e) {
			out.println(e.getMessage());
			e.printStackTrace();
		}
    	
    %>
    <script>
    	function show_order_detail(order_id){
    		
    		
    		/*
    		
    		 var generate_but =document.getElementById('activate-step-2');
    		 var set_delivered_but = document.getElementById('activate-step-3');
    	     var delivery_date = document.getElementById('delivery_date');
    		 
    		$('#order_detail_modal').modal('show');
    		
    		  var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
						//alert("I'm Back");
						var jsonObj = JSON.parse(xmlhttp.responseText);
						var order_id = document.getElementById('order_view_id');
						var customer_name = document.getElementById('customer_view_name');
						var customer_address = document.getElementById('customer_address');
						var total_value = document.getElementById('total_value');
						var total_vat = document.getElementById('total_vat');
						var total_inc_vat = document.getElementById('total_inc_vat');
						var inv_file_name = document.getElementById('inv_file_name');
						var inv_file_path = document.getElementById('inv_file_path');
						
						
						var order_status = jsonObj[0].orderStatus;
					
							order_id.value = jsonObj[0].orderId;
							customer_name.value =  jsonObj[0].customerName;
							customer_address.value = jsonObj[0].customerAddress;
							total_value.value = jsonObj[0].totalValue;
							total_vat.value = jsonObj[0].totalVat;
							total_inc_vat.value = jsonObj[0].totalIncVat;
							
							inv_file_name.value = jsonObj[0].invFileName;
							inv_file_path.value = jsonObj[0].invFilePath;

							
						switch(order_status) {
						    case "order_created":
						    	
						       
						        break;
						    case "inv_generated":
						    	 $('ul.setup-panel li:eq(1)').removeClass('disabled');
						    	 $('ul.setup-panel li a[href="#step-2"]').trigger('click');
						    //	 generate_but.remove(); 
						    		
						        break;
						    case "delivered":
						    	
						    	$('ul.setup-panel li:eq(1)').removeClass('disabled');
						    	$('ul.setup-panel li:eq(2)').removeClass('disabled');
						    	$('ul.setup-panel li a[href="#step-3"]').trigger('click');
						    	
						    	delivery_date.value = jsonObj[0].deliveryDate;
						    	
						    	generate_but.remove(); 
						    	set_delivered_but.remove();
						    	break;
						}
							
							
					}// end if check state
				}// end function
				

				xmlhttp.open("POST", "get_order_detail_background.jsp?order_id="+order_id, true);
				xmlhttp.send();
				
				*/
				sessionStorage.setItem("order_id_for_get_detail", order_id); 
				
				window.open("report_credit_inv_detail.jsp");

    	}
    	
    	
    	function show_cash_bill_detail(cash_bill_id){
		
    		sessionStorage.setItem("cash_bill_id_for_get_detail", cash_bill_id); 
			
			window.open("report_cash_bill_detail.jsp");

   		} 	function show_short_bill_detail(short_bill_id){
		
    		sessionStorage.setItem("short_bill_id_for_get_detail", short_bill_id); 
			
			window.open("report_short_bill_detail.jsp");

   		}
    	
    	
    	function show_quotation_detail(quotation_id){
    		
    		sessionStorage.setItem("quotation_id_for_get_detail", quotation_id); 
			//alert("P1:"+quotation_id);
			window.open("report_quotation_detail2.jsp");

   		}
    	function GenerateCashInv(button){
    		var cash_bill_id = document.getElementById('cash_bill_view_id').value;
    		
    		alert(cash_bill_id);
    		
    		 var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
						
						if(xmlhttp.responseText=="fail")
						{
							alert("Error Occer to Generate inv , pls contact Admin");
						}else{
							
							
							
							var values = xmlhttp.responseText.split("&");
							var cash_bill_file_name = values[0];
							var cash_bill_file_path = values[1];
							alert("Your file "+cash_bill_file_name+" are ready.");
							
						
							var cash_bill_inv_file_name = document.getElementById('cash_bill_inv_file_name');
							var cash_bill_inv_file_path = document.getElementById('cash_bill_inv_file_path');
							
							cash_bill_inv_file_name.value = cash_bill_file_name;
							cash_bill_inv_file_path.value = cash_bill_file_path;
							
						}
						
						
							
					}// end if check state
				}// end function
				
			//	alert("going to send POST");
				xmlhttp.open("POST", "generate_cash_bill_inv_background.jsp?cash_bill_id="+cash_bill_id, true);
				xmlhttp.send();

    	}
    	
    	function GenerateInvoice(button){
    		
    		  var order_id = document.getElementById('order_view_id').value;
    		  var inv_file_name = document.getElementById('inv_file_name');
			  var inv_file_path = document.getElementById('inv_file_path');
				
    		 // alert("gen inv ");
    		//  $('ul.setup-panel li:eq(1)').removeClass('disabled');
    		  $('ul.setup-panel li a[href="#step-2"]').trigger('click');
    		//  button.remove();
    		  
    		  var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
						if(xmlhttp.responseText=="fail")
						{
							alert("Error Occer to Generate inv , pls contact Admin");
						}else{
							
							//alert(xmlhttp.responseText);
							var values = xmlhttp.responseText.split("&");
							var file_name = values[0];
							var file_path = values[1];
							
							
							alert("Your file "+file_name+" are ready.");
						
							inv_file_name.value = file_name;
							inv_file_path.value = file_path;
						}
						
						
							
					}// end if check state
				}// end function
				
				alert("going to send POST");
				xmlhttp.open("POST", "generate_credit_inv_background.jsp?order_id="+order_id, true);
				xmlhttp.send();

    		  
    	}
    	
    	function submit_init_credit_inv_no()
    	{
    		var part_two_credit_inv_no = document.getElementById('part_two_credit_inv_no');
    		var credit_inv_no =  parseInt(part_two_credit_inv_no.value);
    		
    		alert("credit_inv_no:"+credit_inv_no);
    	
    		
    		 var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	

						alert("I'm Back:"+xmlhttp.responseText);

						if(xmlhttp.responseText=='success')
						{
							$('#setting_credit_inv_no_modal').modal('hide');
							location.reload();
							
						}else{
							
						}


					}// end if check state
				}// end function
				

				xmlhttp.open("POST", "set_init_credit_inv_no.jsp?inv_no="+credit_inv_no, true);
				xmlhttp.send();
				
    	}
    	function show_confirm_delete(id){
    		$('#confirm-delete-order').modal('show');
    		  document.getElementById("delete_order_id").value = id;

    	}
    	function DeleteOrder(form_id)
    	{
    		var frm_element = form_id;
    		var order_id = frm_element.elements["delete_order_id"].value;
    		var reason = document.getElementById("reason_delete").value;
    		var inv_prefix = order_id[0]+order_id[1];
    		if(inv_prefix=="CB")
    		{
    			var cash_bill_id = order_id;
    			var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
							alert(xmlhttp.responseText);
							if(xmlhttp.responseText=="success")
							{
								location.reload();
								
							}else{
								
							}

					}// end if check state
				}// end function
				

				xmlhttp.open("POST", "delete_single_cash_bill_background.jsp?cash_bill_id="+cash_bill_id+"&reason="+reason, true);
				xmlhttp.send();
    			
    			
    			
    		}else{
	    		
	    		var xmlhttp;
					
					if(window.XMLHttpRequest) {
						// code for IE7+, Firefox, Chrome, Opera, Safari
						xmlhttp = new XMLHttpRequest();
					}
					else {
						// code for IE6, IE5
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					
					xmlhttp.onreadystatechange = function() {
						if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
								alert(xmlhttp.responseText);
								if(xmlhttp.responseText=="success")
								{
									location.reload();
									
								}else{
									
								}
	
						}// end if check state
					}// end function
					
	
					xmlhttp.open("POST", "delete_single_order_background.jsp?order_id="+order_id+"&reason="+reason, true);
					xmlhttp.send();
    		}

    		
    	}
    	function ActiveDelivered()
    	{
    		var delivery_date = document.getElementById("delivery_date");
    		var order_id = document.getElementById('order_view_id');
    		
    		if(delivery_date.value=="")
    		{
    			alert("Pls input Delivery Date");
    			
    		}else{
    			
				var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
							alert(xmlhttp.responseText);
						

					}// end if check state
				}// end function
				

				xmlhttp.open("POST", "set_delivery_date_to_order_main.jsp?order_id="+order_id.value+"&delivery_date="+delivery_date.value, true);
				xmlhttp.send();
    			
    			
    		}

    	}
    	
    	
    	function setDailyNovatToTable(){
    		
    		
    		var parameter = "";
    		var current_date = new Date();
    		
    		var current_year = current_date.getYear()+1900;
    		var current_month = current_date.getMonth()+1;
    			parameter = "year="+current_year+"&month="+current_month;
    			
    			//alert(parameter);
    				
    		var ul = document.getElementById("ul_daily_novat");
    		
    		var xmlhttp;
    			
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
					//	alert("I'm Back");//////////////////
						
				
						var jsonObj = JSON.parse(xmlhttp.responseText);
					
						//alert("I'm Back");
						
						if(jsonObj.length == 0) {
						//	massage();
						//	alert("");
						}
						else{
							
							//alert(jsonObj.length);
						//	var li = document.createElement("li");
						 	var temp_id = "";
							    
							for(i in jsonObj) {
									  
							  	var div = document.createElement("div");
								    div.className = "chat-body clearfix";
								    
								  //  div.appendChild(li_0);
								  
								    var id = jsonObj[i].dailyNovatId;
								    
								    if(temp_id==id)
								    {
								    //	alert("FOUND");
								    }else{
								    	
									    var id_bold = document.createElement("B");
									    	id_bold.appendChild(document.createTextNode(id));
									    	
							    	    var total_bold = document.createElement("B");
								   	   	    total_bold.appendChild(document.createTextNode("Total Value:"+jsonObj[i].totalValue));
									    	
									    	div.appendChild(id_bold);
									    	div.appendChild(document.createElement("br"));
									    	div.appendChild(total_bold);
									    	div.appendChild(document.createElement("br"));
									    	ul.appendChild(document.createElement("li"));
									    	temp_id = id;
								    }
								    
								   
								    div.appendChild(document.createTextNode("- " +jsonObj[i].adtDescription));
								    div.appendChild(document.createTextNode(" : "));
								    var detail_bold = document.createElement("B");
								  	    detail_bold.appendChild(document.createTextNode("("+jsonObj[i].price+")("+jsonObj[i].quantity+") = "+jsonObj[i].sum));
								  	div.appendChild(detail_bold);
								    ul.appendChild(div);

							}
							
							//   ul.appendChild(total_bold);
							   ul.appendChild(document.createElement("li"));
				
						}
						

					}// end if check state
				}// end function
				

				xmlhttp.open("POST", "get_daily_novat_background.jsp?"+parameter, true);
				xmlhttp.send();

    	}
    	
    	function display_c(){
    	
    		var refresh=1000; // Refresh rate in milli seconds
    		mytime=setTimeout('display_ct()',refresh)
    	
    	}

    	function display_ct() {
    	
    		var strcount;
    		var x = new Date();
    		
    		document.getElementById('live_time').innerHTML = x.toString("dd MMMM yyyy ,HH:mm:ss ");
    		tt=display_c();
    	}

    	
    	
    	function display_total_value_bill_this_month(){
    		
    		var xmlhttp;
    		var year = 0;
    		var month = 0; 
    		var current_date = new Date();
    			month = current_date.getMonth()+1;
    			year = current_date.getFullYear();
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
						//alert(xmlhttp.responseText);
					var jsonObj = JSON.parse(xmlhttp.responseText);
					
					var x = new Date();

					document.getElementById("credit_total_inc_vat_current_month").innerHTML = jsonObj.creditTotalIncVat;
					document.getElementById("cash_total_inc_vat_current_month").innerHTML = jsonObj.cashBillTotalIncVat;
					document.getElementById("daily_novat_total_value_current_month").innerHTML = jsonObj.dailyNovatTotalValue;
					
					document.getElementById("credit_sub_detail").innerHTML = "Since "+"1 "+x.toString("MMMM yyyy");
					document.getElementById("cash_sub_detail").innerHTML = "Since "+"1 "+x.toString("MMMM yyyy");
					document.getElementById("daily_novat_sub_detail").innerHTML = "Since "+"1 "+x.toString("MMMM yyyy");
					

				}// end if check state
			}// end function
			

			xmlhttp.open("POST", "get_all_inv_value_by_month_year_background.jsp?month="+month+"&year="+year, true);
			xmlhttp.send();

    	}
	
 
    </script>
   


</head>

<body>

    <div class="wrapper">
    
    		<nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>
                <div class="sidebar-header">
                    <h3>Bored Sidebar</h3>
                </div>
                
    
        		<div id="includedSidebar"></div>
        		
        	</nav>



        <!-- Navigation -->
       

        <div id="content" class="form-control">
        
        	   <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-justify"></i>
                                <span></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Page-Check31-07-2023</a></li>
                       
                            </ul>
                        </div>
                    </div>
                </nav>
                
                

            <!-- /.row -->
            
            <div class="row">

                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-credit-card fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" id=credit_total_inc_vat_current_month>Credit Only !!</div>
                                    <div id="credit_sub_detail"> Create</div>
                                </div>
                            </div>
                        </div>
                        <a href="dashboard_create_ord.jsp">
                            <div class="panel-footer">
                                <span class="pull-left">Create New Credit Invoice</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				<div class="col-lg-6 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-dollar fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                     <div class="huge" id=cash_total_inc_vat_current_month>Cash Only!!l</div>
                                     <div id="cash_sub_detail"> Create</div>
                                </div>
                            </div>
                        </div>
                        <a href="dashboard_create_cash_bill.jsp">
                            <div class="panel-footer">
                                <span class="pull-left">Create New Cash Invoice</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
               
            </div>
            
        	<div class="row">	
                <div class="col-lg-6">
                	 <div class="panel panel-default">
                	    <div class="panel-heading">
                           	Credit Invoice List
                        </div>
                       <div class="panel-body">
                        
                   				<!-- Left Conner  -->
                   				
                   			<div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example-order">
                                    <thead>
                                        <tr>
                                            <th>Invoice No.</th>
                                            <th>Customer Name</th>
                                            <th>Status</th>
                                            
                                            <th></th>
                                          	<th></th>
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        
                                      
                                           <%
                                        for(int i=0 ; i<order_list.size();i++)
                                        {
                                        %>
                                        <tr>	
                                        	<td><%= order_list.get(i).getInvNo()%></td>
                                        	 <td><%= order_list.get(i).getCustomerName()%></td>
                                        
                                        	<td> <div class="text-center">  <%= order_list.get(i).getOrderStatus()%> </div></td>
                                        	<td> 
                                        		
	                                        	<div class="text-center">  	
	                                        	<button id = "<%=order_list.get(i).getOrderId() %>" type="button" class="btn btn-info btn-circle btn-md" onclick = "show_order_detail(this.id)">
	                                        			<i class="glyphicon glyphicon-search"></i>            	
	                                        						
	                            					</button> 
	                            					
	                            				</div>
                                        		
                                        	</td>
                                        	<td> 
                                        		
	                                        	<div class="text-center">  	
	                                        	<button id = "<%=order_list.get(i).getOrderId() %>" type="button" class="btn btn-danger btn-circle btn-md" onclick="show_confirm_delete(this.id)">
	                                        			<i class="glyphicon glyphicon-trash"></i>            	
	                                        						
	                            					</button> 
	                            					
	                            				</div>
                                        		
                                        	</td>
                                        	
                                        	
                                        </tr>
                                     	<% 
                                        }
                                        
                                        %>
                                       
                                    
                                     
                                        
                                    </tbody>
                                </table>
                            </div>
                                 
                                  
                           </div>
                       </div> 
                </div>
                <div class="col-lg-6">
                	 <div class="panel panel-default">
                	    <div class="panel-heading">
                           	Cash Invoice List
                        </div>
                       <div class="panel-body">
                        
                   				<!-- Left Conner  -->
                   				
                   			<div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example-cash-bill">
                                    <thead>
                                        <tr>
                                            <th>Invoice No.</th>
                                            <th>Customer Name</th>
                                            <th>Status</th>
                                            
                                            <th></th>
                                          	<th></th>
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        
                                      
                                        <%
                                        for(int i=0 ; i<cash_bill_list.size();i++)
                                        {
                                        %>
                                        <tr>	
                                        	<td><%= cash_bill_list.get(i).getInvNo()%></td>
                                        	 <td><%= cash_bill_list.get(i).getCustomerName()%></td>
                                        
                                        	<td> <div class="text-center">  <%= cash_bill_list.get(i).getStatus()%> </div></td>
                                        	<td> 
                                        		
	                                        	<div class="text-center">  	
	                                        	<button id = "<%=cash_bill_list.get(i).getCashBillId() %>" type="button" class="btn btn-info btn-circle btn-md" onclick = "show_cash_bill_detail(this.id)">
	                                        			<i class="glyphicon glyphicon-search"></i>            	
	                                        						
	                            					</button> 
	                            					
	                            				</div>
                                        		
                                        	</td>
                                        	<td> 
                                        		
	                                        	<div class="text-center">  	
	                                        	<button id = "<%=cash_bill_list.get(i).getCashBillId()%>" name="delete_cash_inv" type="button" class="btn btn-danger btn-circle btn-md" onclick="show_confirm_delete(this.id)">
	                                        			<i class="glyphicon glyphicon-trash"></i>            	
	                                        						
	                            					</button> 
	                            					
	                            				</div>
                                        		
                                        	</td>
                                        	
                                        	
                                        </tr>
                                     	<% 
                                        }
                                        
                                        %>
                                       
                                    
                                     
                                        
                                    </tbody>
                                </table>
                            </div>
                                 
                                  
                           </div>
                       </div> 
                </div>

                
              
            </div>
            
            
           <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" id=>Create Quotation</div>
                                    <div id="credit_quotation_sub_detail"> Create</div>
                                </div>
                            </div>
                        </div>
                        <a href="dashboard_create_quotation.jsp">
                            <div class="panel-footer">
                                <span class="pull-left">Create New Quotation</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-money fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" id=>Create Short Invoice</div>
                                    <div id="credit_quotation_sub_detail"> Create</div>
                                </div>
                            </div>
                        </div>
                        <a href="dashboard_create_short_bill.jsp">
                            <div class="panel-footer">
                                <span class="pull-left">Create New Short Invoice</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            
            
            
            <div class="row">	
            	 <div class="col-lg-6">
                	 <div class="panel panel-default">
                	    <div class="panel-heading">
                           	Quotation List
                        </div>
                       <div class="panel-body">
                        
                   				<!-- Left Conner  -->
                   				
                   			<div class="dataTable_wrapper">
                   			
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example-abb-inv">
                                    <thead>
                                        <tr>
                                            <th>Quotation No.</th>
                                            <th>Date</th>
                                            <th>Customer</th>
                                            <th></th>
                                          
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        	               
                                      

                                        
                                    </tbody>
                                </table>
                            </div>
                                 
                                  
                           </div>
                       </div> 
                </div>
                <div class="col-lg-6">
                	 <div class="panel panel-default">
                	    <div class="panel-heading">
                           	Short Invoice List
                        </div>
                       <div class="panel-body">
                        
                   				<!-- Left Conner  -->
                   				
                   			<div class="dataTable_wrapper">
                   			
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example-short-inv">
                                    <thead>
                                        <tr>
                                            <th>Short inv No.</th>
                                            <th>Customer Name</th>
                                            <th>Customer Tel</th>
                                        
                                            <th></th>
                                            <th></th>
                                            
                                          
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        
                                      
                                        <%
                                        for(int i=0 ; i<short_bill_list.size();i++)
                                        {
                                        	//System.out.println("Test Pivot");
                                        %>
                                        <tr>	
                                        	<td><%= short_bill_list.get(i).getInvNo()%></td>
                                        	 <td><%= short_bill_list.get(i).getCustomerName()%></td>
                                        <td><%= short_bill_list.get(i).getCustomerTel()%></td>
                                        	
                                        	<td> 
                                        		
	                                        	<div class="text-center">  	
	                                        	<button id = "<%=short_bill_list.get(i).getshortBillId() %>" type="button" class="btn btn-info btn-circle btn-md" onclick = "show_short_bill_detail(this.id)">
	                                        			<i class="glyphicon glyphicon-search"></i>            	
	                                        						
	                            					</button> 
	                            					
	                            				</div>
                                        		
                                        	</td>
                                        	<td> 
                                        		
	                                        	<div class="text-center">  	
	                                        	<button id = "<%=short_bill_list.get(i).getshortBillId()%>" name="delete_short_inv" type="button" class="btn btn-danger btn-circle btn-md" onclick="show_confirm_delete(this.id)">
	                                        			<i class="glyphicon glyphicon-trash"></i>            	
	                                        						
	                            					</button> 
	                            					
	                            				</div>
                                        		
                                        	</td>
                                        	
                                        	
                                        </tr>
                                     	<% 
                                        }
                                        
                                        %>
                                       
                                        	               			
                                      

                                        
                                    </tbody>
                                </table>
                            </div>
                                 
                                  
                           </div>
                       </div> 
                </div>

            </div>
            
             
        
        
        </div>
	        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	                  
	                         <div class="modal-dialog">
	                          	 <div class="col-sm-6 col-sm-offset-3 text-center">
										 <div class="container">
														<div class="row">
															
													            <div id="loading">
													                <ul class="bokeh">
													                    <li></li>
													                    <li></li>
													                    <li></li>
													                </ul>
														            </div>
														</div>
										</div>
										
									</div>
	 
	                          </div>
	                     
	                          <!-- /.modal-content -->
	             
	                      <!-- /.modal-dialog -->
	      </div>
         <div class="modal fade" id="order_detail_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                                <div class="modal-dialog" style="width:1000px; ">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                          	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Order Detail</h4>
                                        </div>
                                        <div class="modal-body">
                                       		<div class="row">
                                       				<div class="col-md-12 col-centered">	
                                       						<input name="order_view_id" id="order_view_id" type="text" class="form-control" placeholder="Order ID" readonly>
                                       						<br>
                                       						<input name="customer_view_name" id="customer_view_name" type="text" class="form-control" placeholder="Customer Name" readonly>
                                       						<br>
                                       						<textarea id="customer_address" name ="customer_address" class="form-control" rows="2" disabled></textarea>
                                       						
                                       				</div>                           
                                       		</div>
                                       		<br>
                                       		<div class="container">
												<div class="row form-group">
											        <div class="col-xs-10">
											            <ul class="nav nav-pills nav-justified thumbnail setup-panel">
											                <li class="active"><a href="#step-1">
											                    <h4 class="list-group-item-heading">Step 1</h4>
											                    <p class="list-group-item-text">Order Created</p>
											                </a></li>
											                <li class="disabled"><a href="#step-2">
											                    <h4 class="list-group-item-heading">Step 2</h4>
											                    <p class="list-group-item-text">Invoice Generated</p>
											                </a></li>
											                <li class="disabled"><a href="#step-3">
											                    <h4 class="list-group-item-heading">Step 3</h4>
											                    <p class="list-group-item-text">Delivered</p>
											                </a></li>
											                  <li class="disabled"><a href="#step-4">
											                    <h4 class="list-group-item-heading">Step 4</h4>
											                    <p class="list-group-item-text">Billing</p>
											                </a></li>
											                </a></li>
											                  <li class="disabled"><a href="#step-5">
											                    <h4 class="list-group-item-heading">Step 5</h4>
											                    <p class="list-group-item-text">Completed</p>
											                </a></li>
											            </ul>
											        </div>
												</div>
											    <div class="row setup-content" id="step-1">
											        <div class="col-xs-10">
											            <div class="col-xs-12 well text-center">
											            	<div class="row">
											            		<div class="col-lg-2">
											            			<h5><strong>Total(no-Vat) :</strong></h5>
											            		</div>
											            		<div class="col-lg-2">
											            				<input type="text" id="total_value" name="total_value" class="form-control text-right" readonly>
											            		</div>
											            	</div>
											            	<br>
											            	<div class="row">
											            		<div class="col-lg-2">
											            			<h5><strong>Vat :</strong></h5>
											            		</div>
											            		<div class="col-lg-2">
											            				<input type="text" id="total_vat" name="total_vat" class="form-control text-right" readonly>
											            		</div>
											            	</div>
											            	<br>
											            	<div class="row">
											            		<div class="col-lg-2">
											            			<h5><strong>Total:</strong></h5>
											            		</div>
											            		<div class="col-lg-2">
											            				<input type="text" id="total_inc_vat" name="total_inc_vat" class="form-control text-right" readonly>
											            		</div>
											            	</div>
															<br>
											            	</div>
											            	<div class="row pull-right">
											            		 <button id="activate-step-2" class="btn btn-primary btn-lg" onclick="GenerateInvoice(this)">Generate Invoice</button>
											            	</div>
											            </div>
											        </div>
											    </div>
											    <div class="row setup-content" id="step-2">
											        <div class="col-xs-12">
											            <div class="col-xs-12 well text-center">
											            
											               <div class="row">	
											               				
																	  <div class="col-lg-6" style="float: none; margin: 0 auto;">
																	  		<form  method="get" action="../DownloadServlet">
																				    <div class="input-group">
																						      <input id="inv_file_name" name="inv_file_name" type="text" class="form-control" readonly>
																						      <input id="inv_file_path" name="inv_file_path" type="hidden">
																						      <span class="input-group-btn">
																						        <input type="submit" class="btn btn-primary" value="Download">
																						      </span>
																					    
																				    </div><!-- /input-group -->
																	    	</form>
																	    	<br>
																	    	<div class="col-lg-6" style="float: none; margin: 0 auto;">
																	    		<input type="date" id="delivery_date" class="form-control">
																	    	</div>
																	    	<br>
																	    	 <button id="activate-step-3" class="btn btn-primary btn-lg" onclick="ActiveDelivered()">Delivered</button>
																	   
																	  </div>
																	  
																
															 </div>
											            </div>
											        </div>
											    </div>
											    <div class="row setup-content" id="step-3">
											        <div class="col-xs-12">
											            <div class="col-xs-12 well">
											              
											            </div>
											        </div>
											    </div>
											      <div class="row setup-content" id="step-4">
											        <div class="col-xs-10">
											            <div class="col-xs-12 well">
											                <h1 class="text-center"> STEP 4</h1>
											            </div>
											        </div>
											    </div>
											    <div class="row setup-content" id="step-5">
											        <div class="col-xs-10">
											            <div class="col-xs-12 well">
											                <h1 class="text-center"> STEP 5</h1>
											            </div>
											        </div>
											    </div>
											</div>
											                    
                                        </div>
                                
                                    </div>
                                    <!-- /.modal-content -->
         </div>
          <div class="modal fade" id="cash_bill_detail_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                                <div class="modal-dialog" style="width:1000px; ">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                          	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Order Detail</h4>
                                        </div>
                                        <div class="modal-body">
                                       		<div class="row">
                                       				<div class="col-md-12 col-centered">	
                                       						<input name="cash_bill_view_id" id="cash_bill_view_id" type="text" class="form-control" placeholder="Order ID" readonly>
                                       						<br>
                                       						<input name="cash_bill_customer_view_name" id="cash_bill_customer_view_name" type="text" class="form-control" placeholder="Customer Name" readonly>
                                       						<br>
                                       						<textarea id="cash_bill_customer_address" name ="cash_bill_customer_address" class="form-control" rows="2" disabled></textarea>
                                       						
                                       				</div>                           
                                       		</div>
                                       		<br>
                                       		<div class="container">
												<div class="row form-group">
											        <div class="col-xs-10">
											       
											       
											        </div>
												</div>
											   
											        <div class="col-xs-10">
											            <div class="col-xs-12 well text-center">
											            	<div class="row">
											            		<div class="col-lg-2">
											            			<h5><strong>Total(no-Vat) :</strong></h5>
											            		</div>
											            		<div class="col-lg-2">
											            				<input type="text" id="cash_bill_total_value" name="cash_bill_total_value" class="form-control text-right" readonly>
											            		</div>
											            	</div>
											            	<br>
											            	<div class="row">
											            		<div class="col-lg-2">
											            			<h5><strong>Vat :</strong></h5>
											            		</div>
											            		<div class="col-lg-2">
											            				<input type="text" id="cash_bill_total_vat" name="cash_bill_total_vat" class="form-control text-right" readonly>
											            		</div>
											            	</div>
											            	<br>
											            	<div class="row">
											            		<div class="col-lg-2">
											            			<h5><strong>Total:</strong></h5>
											            		</div>
											            		<div class="col-lg-2">
											            				<input type="text" id="cash_bill_total_inc_vat" name="cash_bill_total_inc_vat" class="form-control text-right" readonly>
											            		</div>
											            	</div>
															<br>
											            	</div>
											            	
											            	  <div class="row">
											            	  		<div class="text-center">
											         	
											         					<button type="button" class="btn btn-primary btn-lg" onclick="GenerateCashInv(this)"> Generate Invoice</button>
											         				</div>
											         				<br>
											         				 <div class="col-lg-6" style="float: none; margin: 0 auto;">
											         					<form  method="get" action="../DownloadCashBillServlet">
																				    <div class="input-group">
																						      <input id="cash_bill_inv_file_name" name="cash_bill_inv_file_name" type="text" class="form-control" readonly>
																						      <input id="cash_bill_inv_file_path" name="cash_bill_inv_file_path" type="hidden">
																						      <span class="input-group-btn">
																						        <input type="submit" class="btn btn-primary" value="Download">
																						      </span>
																					    
																				    </div><!-- /input-group -->
																	    	</form>
											         				</div>
											         				
											       			  </div>

											         </div>
											         <br>
											       
											       
											    </div>
											   
											</div>
											                    
                                        </div>
                                
                                    </div>
                                    <!-- /.modal-content -->
         </div>
         
                                <!-- /.modal-dialog -->
          <div class="modal fade" id="setting_credit_inv_no_modal" tabindex="1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style=" overflow-y: auto;" >
          		<div class="modal-dialog" style="width:30%;">
          			 <div class="modal-content">
                            <div class="modal-header">
                            	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                 <h4 class="modal-title" id="myModalLabel">Set Start Credit Invoice No.</h4>
                            </div>
                            <div class="modal-body">
                            
									<div class="col-center-block">
		                           
		                            		<input type="text" id="part_one_credit_inv_no" class="form-control text-right " value="" readonly>                            
		                  					<br>
		                            		<input type="text" id="part_two_credit_inv_no" class="form-control text-right "  maxlength="4" placeholder="0000" >
		                            
		                            </div>
			                         <div style="margin: auto;">
			                            <br>
		                            
	          			 			 	<button type="button" name="bot_init_credit_inv_no"  id="bot_init_credit_inv_no" class="btn btn-primary pull-right" onclick="submit_init_credit_inv_no()">Submit</button>
		                            	<br>
		                            	 <br>
		                              </div>
                            </div>
                     
                          

          			</div>
          		
        	  </div>  
          </div>         
          
          <div class="modal fade" id="confirm-delete-order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			    <div class="modal-dialog">
			        <div class="modal-content">
			        	<form>
					          <div class="modal-body">
								    <textarea id="reason_delete" class="form-control" rows="2" placeholder="Reason for deleting this order"></textarea>
								    
								    <input type="hidden" id="delete_order_id">
							  </div>
							  <div class="modal-footer">
								    <button type="button" data-dismiss="modal" class="btn btn-danger" id="but_delete_order" onclick="DeleteOrder(this.form)">Delete</button>
								    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
							  </div>
						 </form>
			        </div>
			    </div>
		  </div>            
		  
    
      </div>
        <!-- /#page-wrapper -->

    
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script src="../js/date.js"></script>
    
    
            <!-- jQuery Custom Scroller CDN -->
     <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>


    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
    	
    	
    	$("#includedSidebar").load("sidebar.jsp"); 
    	
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').fadeOut();
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        
        
        
        
        $('#dataTables-example-order').DataTable({
                responsive: true
        });
        $('#dataTables-example-cash-bill').DataTable({
           		responsive: true
  	    });
        $('#dataTables-example-abb-inv').DataTable({
       		responsive: true
	    });
        $('#dataTables-example-short-inv').DataTable({
       		responsive: true
	    });
       
        

        
        var current_date = new Date();
        var c_year =  current_date.getFullYear();
        var int_year = parseInt(c_year,10) + parseInt(543,10) - parseInt(2500,10);
        
        var part_one_credit_inv_no = document.getElementById('part_one_credit_inv_no');
        part_one_credit_inv_no.value = int_year+"/";
        	
        //SetShowCurrentData();
        setDailyNovatToTable();
        display_ct();
        display_total_value_bill_this_month();
        
        var navListItems = $('ul.setup-panel li a'),
            allWells = $('.setup-content');

        allWells.hide();

        navListItems.click(function(e)
        {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this).closest('li');
            
            if (!$item.hasClass('disabled')) {
                navListItems.closest('li').removeClass('active');
                $item.addClass('active');
                allWells.hide();
                $target.show();
            }
        });
        
        $('ul.setup-panel li.active a').trigger('click');
        
  
        
    });
    //////////////
    
    </script>

</body>

</html>
