<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "com.atcms.*" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "java.util.Properties" %>
<%@ page import = "javax.servlet.ServletContext" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.util.Date" %>



<% 

	System.out.println("Start create_quotation_full_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
	///////////////////file_path parameter/////////////
	String quotation_no = request.getParameter("quotation_main_id");	
	String contact_name = request.getParameter("contact_name");
	String contact_tel = request.getParameter("contact_tel");
	String contact_email = request.getParameter("contact_email");
	String customer_id = request.getParameter("company_id");
	// pJFIAshtn = Initial company_id for non-company Quotation
	if(customer_id.equals(""))
	{
		customer_id = "pJFIAshtn";
	}
	String customer_address = request.getParameter("customer_address");
	String customer_tax_id = request.getParameter("customer_tax_id");
	String total_vat = request.getParameter("total_vat");
	String total_value = request.getParameter("total_value");
	String total_inc_vat = request.getParameter("total_inc_vat");
	String quotation_date = request.getParameter("quotation_date");
	String expire_date = request.getParameter("expire_date");

	
	if(customer_id=="")
	{
		customer_id = null;
	}
	
	int index = Integer.parseInt(request.getParameter("index"));
	
	String temporary_inv_no = request.getParameter("temp_inv_no_value");
	
    String quotation_id ="QO"+ KeyGen.generateQuatationNo() ;

	
	String current_inv_no="";
	String inv_no_part_two = "";
	String full_inv_no ="";
	
	int temp_inv_no = 0 ;
	String type = "VAT";
	
	System.out.println("quotation_no:"+quotation_no);
	System.out.println("contact_name:"+contact_name);
	System.out.println("contact_tel:"+contact_tel);
	System.out.println("contact_email:"+contact_email);
	System.out.println("customer_id:"+customer_id);
	System.out.println("customer_address:"+customer_address);
	System.out.println("customer_tax_id:"+customer_tax_id);
	System.out.println("total_vat:"+total_vat);
	System.out.println("total_value:"+total_value);
	System.out.println("total_inc_vat:"+total_inc_vat);
	System.out.println("quotation_date:"+quotation_date);
	System.out.println("expire_date:"+expire_date);
	
	
	
	 ///////////////////////////////////////////////////
//	List<Product> product_list = new ArrayList<Product>();

	ObjectMapper mapper = new ObjectMapper();
	try {		
		

	
		
		try{

			Date create_date = new Date();
			String year = ""+(create_date.getYear()+1900); 
			String month = "" +(create_date.getMonth()+1);
			String date = ""+create_date.getDate();
			
			if(date.length()==1)
			{
				date = "0"+date;
			}
			if(month.length()==1)
			{
				month = "0"+month;
			}
			String create_date_str = year+"-"+month+"-"+date;
			
			full_inv_no = ""+(create_date.getYear()+1900+543-2500)+"/"+current_inv_no;
			

			String sql_query =" INSERT INTO `quotation_main`(`quotation_id`"+
													   ", `quotation_no`"+	
													   ", `create_date`"+	
													   ", `customer_id`"+
													   ", `quotation_date`"+
													   ", `expire_date`"+
													   ", `contact_name`"+
													   ", `contact_tel`"+
													   ", `contact_email`"+
													   ", `total_value`"+
													   ", `total_vat`"+
													   ", `total_inc_vat` )"+
							   " VALUES ('"+quotation_id+"'"+
									    ",'"+quotation_no+"'"+		   
									    ",'"+create_date_str+"'"+
									    ",'"+customer_id+"'"+
									    ",'"+quotation_date+"'"+
									    ",'"+expire_date+"'"+
									    ",'"+contact_name+"'"+
									    ",'"+contact_tel+"'"+
									    ",'"+contact_email+"'"+
									    ",'"+total_value+"'"+
										",'"+total_vat+"'"+
									    ",'"+total_inc_vat+"')";
			
	
							   
			System.out.println("sql_query:"+sql_query);
			
			try{
				connect.createStatement().executeUpdate(sql_query);
				String 	pd_id ;
				String  pd_addition;
				String  pd_price ;
				String  pd_quantity ;
			    String  pd_sum ;
			    String  pd_unit ;
			    
			    

				for(int i=0;i<index;i++)
				{
					
					pd_id = request.getParameter("pd_id"+i);
					
					pd_addition = 	request.getParameter("pd_name"+i);	
					pd_price = request.getParameter("pd_price"+i);
					pd_quantity = request.getParameter("pd_quantity"+i);
				    pd_sum = request.getParameter("pd_sum"+i);
				    pd_unit = request.getParameter("pd_unit"+i);
				    
				    
				    String sql_quotation_detail = " INSERT INTO `quotation_detail`(`quotation_id`"+
												", `product_id`"+
												", `price`"+
												", `adt_description`"+
												", `quantity`"+
												", `sum`"+
												", `unit`)"+
						" VALUES ('"+quotation_id+"'"+
								  ",'"+pd_id+"'"+
							      ",'"+pd_price+"'"+
							      ",'"+pd_addition+"'"+
								  ",'"+pd_quantity+"'"+
							      ",'"+pd_sum+"'"+
								  ",'"+pd_unit+"')";
				    
				     System.out.println("sql_order_detail:"+ i + sql_quotation_detail);
				     connect.createStatement().executeUpdate(sql_quotation_detail);
				
				    System.out.println("Success:"+i+":"+sql_quotation_detail);
	
				    
				};

			
			}catch(Exception x){
			
				x.printStackTrace();
				connect.close();
				out.print("error");
			}
			

			
			
		}catch(Exception o){
			o.printStackTrace();
			connect.close();
			out.print("error");
		}
	
		
	} catch (Exception e) {
		
		e.printStackTrace();
		connect.close();
		out.print("error");
	}
	 
	connect.close();
%>
