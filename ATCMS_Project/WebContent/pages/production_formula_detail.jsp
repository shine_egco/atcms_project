<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

 <script>
	 
 
 
	 function fetch_formula_detail(){
		 
		 
		 var form_id =   sessionStorage.getItem("formula_for_get_detail");
		 
		  var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
				
								
							if(xmlhttp.responseText=="fail")
							{
								alert("get formula detail error");
							}else{
								
								var jsonObj = JSON.parse(xmlhttp.responseText);
								
								var formula_id  =   document.getElementById("formula_id");
								var formula_name_en =   document.getElementById("formula_name_en");
								var formula_name_th =   document.getElementById("formula_name_th");
								var formula_code =   document.getElementById("formula_code");
								var formula_remark =   document.getElementById("formula_remark");
								var description =   document.getElementById("description");
								var capacity_input =   document.getElementById("capacity_input");
								var total_cost =   document.getElementById("total_cost");

								
								var product_unit =   document.getElementById("product_unit");
								
								
									formula_id.value = jsonObj.formulaID;
									formula_name_en.value = jsonObj.nameEN;
									formula_name_th.value = jsonObj.nameTH;
									formula_code.value =  jsonObj.formulaCode;
									formula_remark.value = jsonObj.remark;
									description.value = jsonObj.description;
									total_cost.value = jsonObj.totalCost;
									
									product_unit.value = jsonObj.capacity + " "+ jsonObj.capacityUnit + "/ "+jsonObj.packagingUnit;
									
								

							}
					
					//System.out.println("temp_x:"+temp_x);	
						
				
				}// end if check state
			}// end function
			
	
			xmlhttp.open("POST", "get_formula_detail_by_formula_id_background.jsp?form_id="+form_id, true);
			xmlhttp.send();
		 
	 }
		 
	 function formatMoney(number, places, symbol, thousand, decimal) {
			number = number || 0;
			places = !isNaN(places = Math.abs(places)) ? places : 0;
			symbol = symbol !== undefined ? symbol : "";
			thousand = thousand || ",";
			decimal = decimal || ".";
			var negative = number < 0 ? "-" : "",
			    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
			    j = (j = i.length) > 3 ? j % 3 : 0;
			return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
	}

	 function create_formula(){
		 		 
		 var form_name_en =  document.getElementById("formula_name_en");
		 var form_name_th =  document.getElementById("formula_name_th");
		 var form_code = document.getElementById("formula_code");
		 var description =  document.getElementById("description");
		 
	     var capacity_but = document.getElementById("capacity_but");
	     var capacity_unit_value = capacity_but.name;
	 	 var packaging_but =   document.getElementById("packaging_but");
	 	 var packaging_unit_value = packaging_but.name;
	 	 
		 var total_cost =  document.getElementById("total_cost");
		
		 var capacity_input = document.getElementById("capacity_input");
			
		 
		 var parameter_main = "form_name_en="+form_name_en.value
		 						  +"&form_name_th="+form_name_th.value
		 						  +"&form_code="+form_code.value
		 						  +"&description="+description.value
		 						  +"&capacity_unit="+capacity_unit_value.toLowerCase()
		 						  +"&capacity_value="+capacity_input.value
		 						  +"&packing_unit="+packaging_unit_value.toLowerCase()
		 						  +"&total_cost="+total_cost.value;
		 						  

		 alert("parameter_main:"+parameter_main);
		
		
		 var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
					if(xmlhttp.responseText=="fail")
					{
						alert("Create Error");
					}else{
						
						alert("Success");
						
						
						location.reload();
					}
					
				}// end if check state
			}// end function
			
		
			xmlhttp.open("POST", "create_formula_full_background.jsp?"+parameter_main, true);
			xmlhttp.send();
			
		 
		 
	 }
	 function onchange_capacity_unit_ul(temp){
	
			var click_li_value = temp.innerHTML;
			var capacity_but =   document.getElementById("capacity_but");
				  capacity_but.innerHTML = click_li_value+"<span class='caret'></span>";
				  capacity_but.name = click_li_value.toLowerCase();
				 // alert(capacity_but.name);
				  
		  
	 }
	 
	 function onchange_packaging_unit_ul(temp)
	 {
			var click_li_value = temp.innerHTML;
			var packaging_but =   document.getElementById("packaging_but");
			   	packaging_but.innerHTML = click_li_value+"<span class='caret'></span>";
			   	packaging_but.name = click_li_value.toLowerCase();
		 
	 }
	 
	 
	 function show_matching_modal(){
		
		 $('#matching_modal').modal('show');
		 
		 
		 
	 }
	 function onchange_match_option(temp){
		 
	
		 var option_value = $(temp).find(":selected").val();
		
	 	 if(option_value=="existing_product")
	 	{
	
				 		var xmlhttp;
						
						if(window.XMLHttpRequest) {
							// code for IE7+, Firefox, Chrome, Opera, Safari
							xmlhttp = new XMLHttpRequest();
						}
						else {
							// code for IE6, IE5
							xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
						}
						
						xmlhttp.onreadystatechange = function() {
							if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
								var jsonObj = JSON.parse(xmlhttp.responseText);
								
								if(jsonObj.length == 0) {
			
								alert("Can't get ATC Product ");
								}
								else{
									var exist_select = document.getElementById("select_existing_prd");
									
									for(i in jsonObj) {
										
										var option = document.createElement("option");
										
											   option.value = jsonObj[i].productID;
											   
											   if((jsonObj[i].nameTH=="-"))
												{
												     option.text = jsonObj[i].nameEN;
												     console.log("nameTH is -");
												 }else{
													 option.text = jsonObj[i].nameTH;
													 console.log("nameTH is not null");
												 }
											// console.log(option.text);
											   exist_select.appendChild(option);
									}
									
								}
							}
							
						}
						
						xmlhttp.open("POST", "get_product_by_category_background.jsp?category="+"ATC_PRD", true);
						xmlhttp.send();

	 	}else{

	 		var  existing_prd = document.getElementById("select_existing_prd");
	 		        existing_prd.value ="-";
	 		
	 	}
	 	 
	 	 
	 }
	 
	function  onchange_get_single_product(product_id)
	{
		
		var xmlhttp;
		
		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange = function() {
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				
				var jsonObj = JSON.parse(xmlhttp.responseText);
				
				if(jsonObj.length == 0) {

					alert("Can't get Select Product Detail ");
				}
				else{
				//	alert("Get it !!");
				var pro_name_th = document.getElementById("pro_name_th");
				var pro_unit_th = document.getElementById("pro_unit_th");
				var pro_name_en = document.getElementById("pro_name_en");
				var pro_unit_en = document.getElementById("pro_unit_en");
				
					   pro_name_th.value = jsonObj[0].nameTH;
					   pro_unit_th.value = jsonObj[0].unitTH;
					   pro_name_en.value = jsonObj[0].nameEN;
					   pro_unit_en.value =  jsonObj[0].unitEN;
					
				}
			}
			
		}
		
		xmlhttp.open("POST", "get_product_detail_by_product_id_background.jsp?product_id="+product_id, true);
		xmlhttp.send();
		
		
	}
	 
	 
	 function matching_formula_to_product(){
		 
			var matching_method = document.getElementById("select_method");
			var existing_prd = document.getElementById("select_existing_prd");
			
			var pro_name_th = document.getElementById("pro_name_th");
			var pro_unit_th = document.getElementById("pro_unit_th");
			var pro_name_en = document.getElementById("pro_name_en");
			var pro_unit_en = document.getElementById("pro_unit_en");
	/*
		   var parameter = "matching_method="+matching_method.value +
		   							  "&existing_prd="+existing_prd.value +
		   							  "&pro_name_th="+pro_name_th.value +
		   							  "&pro_unit_th="+pro_unit_th.value+
		   							  "&pro_name_en="+pro_name_en.value+
		   							  "&pro_unit_en="+pro_unit_en.value;
			   */
			if(matching_method.value=="create_as_new_product")
			{
				var total_cost = document.getElementById("total_cost");
				
				var parameter = "new_pro_name_th="+pro_name_th.value+
											"&new_pro_unit_th="+pro_unit_th.value+
											"&new_pro_name_en="+pro_name_en.value+
											"&new_pro_unit_en="+pro_unit_en.value+
											"&new_pro_category="+"ATC_PRD"+
											"&new_pro_group_code="+"-"+
											"&init_price="+total_cost.value;
				
				
				// waiting for implement
				
				
				
			}else{
				
				var formula_id = document.getElementById("formula_id").value;
				var product_id =  existing_prd.value;
				
				var init_price = document.getElementById("total_cost").value;
					
				var parameter_temp  =  "product_id="+product_id+
													   "&formula_id="+formula_id+
													   "&init_price="+init_price;
					
				
						var xmlhttp;
						
						if(window.XMLHttpRequest) {
							// code for IE7+, Firefox, Chrome, Opera, Safari
							xmlhttp = new XMLHttpRequest();
						}
						else {
							// code for IE6, IE5
							xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
						}
						
						xmlhttp.onreadystatechange = function() {
							if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
								
								alert(xmlhttp.responseText);
								 $('#matching_modal').modal('hide');
								 
								 location.reload();
		
							}
							
						}
						
						xmlhttp.open("POST", "update_formula_id_to_product_background.jsp?"+parameter_temp, true);
						xmlhttp.send();
				
			}
		
		
		
	 }
	 
	 function fetch_formula_relate_product(){
		 
		 var formula_id =   sessionStorage.getItem("formula_for_get_detail");

		 var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	

						var jsonObj = JSON.parse(xmlhttp.responseText);
					
						//alert("I'm Back");
						
										if(jsonObj.length == 0) {
										//	massage();
											alert("No Relate with Product");
										}
										else{
											//alert(jsonObj.length);
											
											for(i in jsonObj) {
											
												$('#dataTables-example-relate-product').DataTable().row.add([
						                           '<tr><td><center>'+jsonObj[i].productID+'</center></td>'
						                           ,'<td><center>'+jsonObj[i].nameTH+'</center></td>'
						                           ,'<td><center>'+jsonObj[i].nameEN+'</center></td></tr>'  
						                         ]).draw();
											}
											
										
										}// end function
					}
			}
			
			xmlhttp.open("POST", "get_formula_relate_product_by_formula_id_background.jsp?formula_id="+formula_id, true);
			xmlhttp.send();
			
}
	 
	 
</script>
<style>
	.text-right {
 		 text-align:right;
	}
	

</style>
    

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
     		   <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.jsp">ATCMS Admin</a>
            </div>
            <!-- /.navbar-header -->

   
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="dashboard_main.jsp"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-group fa-fw"></i>Customer&Vendor<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="customer_sub.jsp">Customers</a>
                                </li>
                                <li>
                                    <a href="vendor_sub.jsp">Vendors</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                         <li>
                            <a href="#"><i class="fa fa-leaf"></i>  Product<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="atc_product_sub.jsp">ATC Product</a>
                                </li>
                                <li>
                                    <a href="other_product_sub.jsp">Other Product</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o"></i>  Bill Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="direct_bill_sub.jsp">Direct Bill</a>
                                </li>
                                <li>
                                    <a href="indirect_bill_sub.jsp">Indirect Bill</a>
                                </li>
                                <li>
                                    <a href="credit_inv_sub.jsp">Credit Invoice</a>
                                </li>
                                <li>
                                    <a href="cash_inv_sub.jsp">Cash Invoice</a>
                                </li>
                                <li>
                                    <a href="credit_note_sub.jsp">Credit Note (ใบลดหนี้)</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                        	<a href="#"><i class="glyphicon glyphicon-fire"></i> Production (การผลิต)<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">                         
                               <li>
                                    <a href="production_material_main.jsp">Material</a>
                                </li>
                                <li>
                                    <a href="production_work_order_main.jsp">Work Order</a>
                                </li>
                                <li>
                                    <a href="production_formula_main.jsp">Formula</a>
                                </li>
                                 <li>
                                    <a href="production_product_relate_formula.jsp">Product Relate Formula</a>
                                </li>                
                            </ul>
                        </li>
                        <li>
                        	<a href="#"><i class="glyphicon glyphicon-stats"></i> Reporting (รายงาน)<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="monthly_summary_report_main.jsp">Monthly Summary Report</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                        	<a href="#"><i class="glyphicon glyphicon-road"></i> (DEVEL)0pinG Z()Ne <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="dev_show_profit_each_bill_detail.jsp">Each  Bill Value Detail</a>
                                </li>
                                <li>
                                    <a href="dev_balancing_work_order.jsp">Balancing Work Order</a>
                                </li>
                
                            </ul>
                        </li>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
             <div class="row">
	                <div class="col-lg-12">
	                    <h1 class="page-header"></h1>
	                </div>
                <!-- /.col-lg-12 -->
             </div>
                    
         
         	 <div class="row">
         	 		  <div class="col-lg-6">
		                    <div class="panel panel-default">
		                        <div class="panel-heading">
		                            Material List
		                        </div>
		                        <div class="panel-body">
		                        <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example-material">
                                    <thead>
                                        <tr>

                                            <th>Name TH</th>
                                            <th>Name EN</th>
                                       		<th>Unit</th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
		                        
		                        
		                        
		                        </div>
		                   </div>
	                  </div>
	                   
	                  <div class="col-lg-6" >
		                    <div class="panel panel-default">
		                        <div class="panel-heading">
		                            New Formula
		                        </div>
		                        <div class="panel-body">
		                     		   <label>Formula ID : </label>
	                           		 <input id="formula_id" name = "formula_id" class="form-control" readonly>
	                           		 <br>
		                        	<label>Formula Name EN : </label>
	                           		 <input id="formula_name_en" name = "formula_name_en" class="form-control" value=""  placeholder="Ex Rush Remover.....">
	                           		 <br>
	                           		 <label>Formula Name TH : </label>
	                           		 <input id="formula_name_th" name = "formula_name_th" class="form-control" placeholder="Ex น้ำยาล้าง......"  >
	                           		 <br>
	                           		  <label>Formula Code : </label>
	                           		 <input id="formula_code" name = "formula_code" class="form-control" value=""  >
	                           		 <br>
	                           		   <label>Remark : </label>
	                           		 <input id="formula_remark" name = "formula_remark" class="form-control" value=""  placeholder="">
	                           		 <br>
	                           		
	                        	<label>Description : </label>
	                        		 <textarea id="description" name ="description" class="form-control" rows="3" placeholder="รายเอียดสูตร"></textarea>
	                        	 	 <br>	
	                      			 <hr>
	                      			
	                      			<table>
	                      			 		<tr class="pagination-centered">
	                      			 
	                        					<td>
	                        						<label style="padding-left:2em;">Product Unit : </label>                      				
	                        					</td>
	                        					<td style="padding-left:1em;">
	                        							<input type="text" name='product_unit' id = "product_unit"   class="form-control text-right" readonly />
	                        					</td>
	                      			 		
	                      			 			<td>
	                        						<label style="padding-left:5em;">Total Cost/Unit : </label>                      						
	                        					</td>
	                        					<td style="padding-left:1em;">
	                        					 	<input type="text" name='total_cost' id = "total_cost" class="form-control text-right"/>
	                        					</td>
	                        					
	                        	   			</tr>
	                        	   
	                        
	                        		</table>
		                        
		                        		                    
		          				<div class="row" >
		          					
										<br>
										
										<div class="pull-right" style="padding-right:2em ;" >
												<button type="button" class="form-control btn btn-primary" style="width:200px;" onclick="create_formula()">Save</button>
										</div>
										<div class="pull-right"  style="padding-right:2em ;">
												<button type="button" class="form-control btn btn-success" style="width:200px;" onclick="show_matching_modal()">Match with Product</button>
										</div>
										
										
								</div>
								
	                       	    </div>
	                       	    
	                       	    
		                    </div>
		                  
         	 		  </div>
         	 
         	 </div>
         	 
         	 <div class="row">
  	 			    <div class="col-lg-6" >
  	 			    </div>
  	 			     <div class="col-lg-6" >
		                    <div class="panel panel-default">
			                        <div class="panel-heading">
			                            Match With Product 
			                        </div>
			                              <div class="panel-body">
                        						<div class="dataTable_wrapper">
									                      <table class="table table-striped table-bordered table-hover" id="dataTables-example-relate-product">
							                                    <thead>
							                                        <tr>
																		 <th>Product ID </th>
							                                            <th>Name TH</th>
							                                            <th>Name EN</th>
							                                       	
							
							                                        </tr>
							                                    </thead>
							                                    <tbody>
							
							                                    </tbody>
						                                </table>
						                           </div>
						                       </div>    
		                    </div>
  	 			    </div>
         	 </div>
        

        
        </div>
        <!-- /#page-wrapper -->
              <div class="modal fade" id="matching_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style=" position: fixed; top: 10%; left: 10%; right: 10%; bottom: 15% ">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Matching Product</h4>
                                        </div>
                                        <div class="modal-body">
                                        	 <div class="form-group">
												  <label for="select_method">Match Option</label>
												  <select class="form-control" id="select_method" onchange="onchange_match_option(this)">
												    <option value="create_as_new_product">Create as New Product</option>
												    <option value="existing_product">Existing Product</option>											    
												  </select>
											</div>
										    <div class="form-group">
												  <label for="sel1">Existing Product</label>
												  <select class="form-control"  onchange="onchange_get_single_product(this.value)"  id="select_existing_prd" >
												    <option value="-" >-</option>					    
												  </select>
											</div>
                                           <input name="pro_name_th" id="pro_name_th" type="text" class="form-control" placeholder="ชื่อภาษาไทย">
                                           <br>
                                           	<input name="pro_unit_th" id="pro_unit_th" type="text" class="form-control" placeholder="หน่วยภาษาไทย">
                                           	<br>
                                           	<input name="pro_name_en" id="pro_name_en" type="text" class="form-control" placeholder="Name Eng">
                                           	<br>
                                           	<input name="pro_unit_en" id="pro_unit_en" type="text" class="form-control" placeholder="Unit Eng">
                                           	<br>
                                          
                                              
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" name="submit_create_customer"  id="submit_create_customer" class="btn btn-primary" onclick="matching_formula_to_product()">Submit</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
      </div>

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    
    
    <script>
    $(document).ready(function() {
    	
        $('#dataTables-example').DataTable({
                responsive: true
        });
        
			fetch_formula_detail();
			fetch_formula_relate_product();
		

    });
    </script>
	<script>
    // tooltip demo
	    $('.tooltip-demo').tooltip({
	        selector: "[data-toggle=tooltip]",
	        container: "body"
	    })
	
	    // popover demo
	    $("[data-toggle=popover]")
	        .popover()
    </script>
</body>

</html>
