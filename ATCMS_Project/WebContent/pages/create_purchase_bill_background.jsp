<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "com.atcms.*" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "java.util.Properties" %>
<%@ page import = "javax.servlet.ServletContext" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.util.Date" %>



<% 

	System.out.println("Start create_purchase_bill_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
	///////////////////file_path parameter/////////////
	String vendor_id = request.getParameter("company_id");
	String  inv_no = request.getParameter("inv_no");
	String due_date_str = request.getParameter("due_date");
	String invoice_date_str = request.getParameter("invoice_date");
	String calculate_date = request.getParameter("calculate_date");
	String type_of_purchase = request.getParameter("type_of_purchase");
	
	String total_value = request.getParameter("total_value");
	String total_vat = request.getParameter("total_vat");
	String total_inc_vat = request.getParameter("total_inc_vat");
	
	String type = request.getParameter("type");
	String payment_method = request.getParameter("payment_method");
	
	System.out.println("payment_method:"+payment_method);
	
	int index = Integer.parseInt(request.getParameter("index"));
	



	
	System.out.println("vendor_id:"+vendor_id);
	System.out.println("inv_no:"+inv_no);
	System.out.println("due_date:"+due_date_str);
	System.out.println("calculate_date:"+calculate_date);
	System.out.println("invoice_date_str:"+invoice_date_str);
	System.out.println("total_vat:"+total_vat);
	System.out.println("total_inc_vat:"+total_inc_vat);
	
	 ///////////////////////////////////////////////////
//	List<Product> product_list = new ArrayList<Product>();

	ObjectMapper mapper = new ObjectMapper();
	
		
		try{

			String purchase_main_id ="PUR"+ KeyGen.generatePurchaseID();

			Date create_date = new Date();
			String year = ""+(create_date.getYear()+1900); 
			String month = "" +(create_date.getMonth()+1);
			String date = ""+create_date.getDate();
			
			if(date.length()==1)
			{
				date = "0"+date;
			}
			if(month.length()==1)
			{
				month = "0"+month;
			}
			String create_date_str = year+"-"+month+"-"+date;
			
		
			
					
			//	Date create_date ;
		//	Date delivery_date ;
		//	Date due_date ;
			String  init_status = "-";
			String sql_query =" INSERT INTO `purchase_main`(`purchase_id`"+
													   ", `type`"+
													   ", `type_of_purchase`"+
													   ", `create_date`"+
													   ", `inv_no`"+
													   ", `vendor_id`"+
													   ", `status`"+
													   ", `calculate_date`"+
													   ", `invoice_date`"+
														", `due_date`"+
													   ", `payment_method`"+
													   ", `total_value`"+
													   ", `total_vat`"+
													   ", `total_inc_vat`)"+
							   " VALUES ('"+purchase_main_id+"'"+
										",'"+type+"'" +
										",'"+type_of_purchase+"'" +
									    ",'"+create_date_str+"'"+
										",'"+inv_no+"'"+
									    ",'"+vendor_id+"'"+
										",'"+init_status+"'"+
									    ",'"+calculate_date+"'"+
										",'"+invoice_date_str+"'"+
										",'"+due_date_str+"'"+
										",'"+payment_method+"'"+
									    ",'"+total_value+"'"+
										",'"+total_vat+"'"+
									    ",'"+total_inc_vat+"')";
	
							   
			System.out.println("sql_query:"+sql_query);
			try{
				connect.createStatement().executeUpdate(sql_query);
				String 	pd_id ;
				String  pd_addition;
				String  pd_price ;
				String  pd_quantity ;
			    String  pd_sum ;
			    String  pd_unit ;
			    
			    
				
				
				for(int i=0;i<index;i++)
				{
					
					pd_id = request.getParameter("pd_id"+i);
					pd_addition = 	request.getParameter("pd_addition"+i);	
					pd_price = request.getParameter("pd_price"+i);
					pd_quantity = request.getParameter("pd_quantity"+i);
				    pd_sum = request.getParameter("pd_sum"+i);
				    pd_unit = request.getParameter("pd_unit"+i);
				    
				    
				    String sql_order_detail = " INSERT INTO `purchase_detail`(`purchase_id`"+
												", `product_id`"+
												", `price`"+
												", `type`"+
												", `adt_description`"+
												", `quantity`"+
												", `sum`"+
												", `unit`)"+
						" VALUES ('"+purchase_main_id+"'"+
								  ",'"+pd_id+"'"+
							      ",'"+pd_price+"'"+
								  ",'"+type+"'"+
							      ",'"+pd_addition+"'"+
								  ",'"+pd_quantity+"'"+
							      ",'"+pd_sum+"'"+
								  ",'"+pd_unit+"')";
				    connect.createStatement().executeUpdate(sql_order_detail);
				    System.out.println("Success:"+i+":"+sql_order_detail);
	
				    
				};
				System.out.println("Send Back");
				out.print(purchase_main_id);
	
			
			
			}catch(Exception x){
			
				x.printStackTrace();
				connect.close();
				out.print("error");
			}
			

			
			
		}catch(Exception o){
			o.printStackTrace();
			connect.close();
			out.print("error");
		}
	
		

	
	connect.close();
%>
