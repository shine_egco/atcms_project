<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>
<%@ page import = "java.text.DateFormat" %>



<% 

	System.out.println("Start get_cash_bill_main_by_cash_bill_id_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

		
	//List<Order> order_list = new ArrayList<Order>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	//String purchase_id = request.getParameter("purchase_id");
	//System.out.println("purchase_id:"+purchase_id);
	 String cash_bill_id = request.getParameter("cash_bill_id");

	 System.out.println("cash_bill_id:"+cash_bill_id);



      try{
    	  
    
    		System.out.println("Keyword is Text"); 
       		String	sql_query =  " SELECT * "+
    						"  FROM cash_bill_main "+
    						"  JOIN company "+
    						"  ON  cash_bill_main.customer_id = company.company_id "+
    						"  WHERE BINARY cash_bill_main.cash_bill_id ='"+cash_bill_id+"'";
  
   			 
   			 

    
    	  System.out.println("sql_query:"+sql_query);
    	  
    	  
    	  ResultSet rs_cd = connect.createStatement().executeQuery(sql_query);
          
    	  
    	  CashBill cb = new CashBill();
    	  
    	  while(rs_cd.next())
          {
             
              		cb.setInvNo(rs_cd.getString("inv_no"));
         	  if(rs_cd.getString("name_th").equals("-"))
              {
         			cb.setCustomerName(rs_cd.getString("name_en"));
         			cb.setCustomerAddress(rs_cd.getString("address_en"));
         	  }else{
         		    cb.setCustomerName(rs_cd.getString("name_th"));
         		    cb.setCustomerAddress(rs_cd.getString("address_th"));
         	  }
              		cb.setTotalIncVat(rs_cd.getString("total_inc_vat"));
              	//	ord.setCompletedDate(rs_cd.getString("completed_date"));
              		cb.setCreateDate(rs_cd.getString("create_date"));      
              	//	ord.setDeliveryDate(rs_cd.getString("delivery_date"));
         	  	//	ord.setDueDate(rs_cd.getString("due_date"));
       				cb.setIncGeneratedDate(rs_cd.getString("inv_generated_datetime"));
       				cb.setInvFileName(rs_cd.getString("inv_file_path"));
       		
       				cb.setCashBillId(rs_cd.getString("cash_bill_id"));
       				cb.setPoNum(rs_cd.getString("ponum"));
       				cb.setTotalIncVat(rs_cd.getString("total_inc_vat"));
       				cb.setTotalValue(rs_cd.getString("total_value"));
       				cb.setTotalVat(rs_cd.getString("total_vat"));
       				cb.setType(rs_cd.getString("type"));
       				cb.setTaxId(rs_cd.getString("tax_id"));
       				cb.setInvFilePath(rs_cd.getString("inv_file_path"));
       				cb.setInvFileName(rs_cd.getString("inv_file_name"));
       				cb.setStatus(rs_cd.getString("status"));
       				cb.setBillDate(rs_cd.getString("bill_date"));
       				cb.setNote(rs_cd.getString("note"));
       				cb.setPaymentReference(rs_cd.getString("payment_reference"));
       				cb.setCompletedDate(rs_cd.getString("completed_date"));
       				
       				
         //     credit_inv_list.add(ord);
            } 
              
          
             
         	try {
				Json = mapper.writeValueAsString(cb); 
				System.out.println("Json:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	
         	
         	out.print(Json);
	   
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  connect.close();
    	  out.print("fail");
      }
     
  
	connect.close();
%>
