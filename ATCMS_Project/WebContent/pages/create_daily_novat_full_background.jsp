<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "com.atcms.*" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "java.util.Properties" %>
<%@ page import = "javax.servlet.ServletContext" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.util.Date" %>



<% 

	System.out.println("Start create_daily_novat_full_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
	///////////////////file_path parameter/////////////

	
	String bill_date_str = request.getParameter("bill_date");
	String total_sum = request.getParameter("total_sum");
	int index = Integer.parseInt(request.getParameter("index"));
	
	String name_ref = request.getParameter("name_ref");
	String contact_ref = request.getParameter("contact_ref");
	
	String type ="NOVAT";
	System.out.println("bill_date_str:"+bill_date_str);
	System.out.println("total_sum:"+total_sum);
	System.out.println("index:"+index);
	

	 ///////////////////////////////////////////////////
//	List<Product> product_list = new ArrayList<Product>();

	ObjectMapper mapper = new ObjectMapper();
	try {		
		
		//get current credit_inv_no
		
		
		try{

			String daily_novat_id ="NV"+ KeyGen.generateDailyNovatID();
			Date create_date = new Date();
			String year = ""+(create_date.getYear()+1900); 
			String month = "" +(create_date.getMonth()+1);
			String date = ""+create_date.getDate();
			
			if(date.length()==1)
			{
				date = "0"+date;
			}
			if(month.length()==1)
			{
				month = "0"+month;
			}
		
	
					
			//	Date create_date ;
		//	Date delivery_date ;
		//	Date due_date ;
			//String  init_status = "order_created";
			String sql_query =" INSERT INTO `daily_novat_main`(`daily_novat_id`"+
															 ", `total_value`"+
															 ", `name_ref`"+
															 ", `contact_ref`"+
															 ", `bill_date`)"+
							   " VALUES ('"+daily_novat_id+"'"+
									    ",'"+total_sum+"'"+
									    ",'"+name_ref+"'"+
									   ",'"+contact_ref+"'"+
									    ",'"+bill_date_str+"')";


	
							   
			System.out.println("sql_query:"+sql_query);
			try{
				connect.createStatement().executeUpdate(sql_query);
				String 	pd_id ;
				String  pd_addition;
				String  pd_price ;
				String  pd_quantity ;
			    String  pd_sum ;
			    String  pd_unit ;
			    
			    

				for(int i=0;i<index;i++)
				{
					
					pd_id = request.getParameter("pd_id"+i);
					
					pd_addition = 	request.getParameter("pd_name"+i);	
					pd_price = request.getParameter("pd_price"+i);
					pd_quantity = request.getParameter("pd_quantity"+i);
				    pd_sum = request.getParameter("pd_sum"+i);
				    pd_unit = request.getParameter("pd_unit"+i);
				    
				    
				    String sql_order_detail = " INSERT INTO `daily_novat_detail`(`daily_novat_id`"+
												", `product_id`"+
												", `price`"+
												", `type`"+
												", `adt_description`"+
												", `quantity`"+
												", `sum`"+
												", `unit`)"+
						" VALUES ('"+daily_novat_id+"'"+
								  ",'"+pd_id+"'"+
							      ",'"+pd_price+"'"+
								  ",'"+type+"'"+
							      ",'"+pd_addition+"'"+
								  ",'"+pd_quantity+"'"+
							      ",'"+pd_sum+"'"+
								  ",'"+pd_unit+"')";
				    connect.createStatement().executeUpdate(sql_order_detail);
				    System.out.println("Success:"+i+":"+sql_order_detail);
	
				    
				};
				
				
			
			
			}catch(Exception x){
			
				x.printStackTrace();
				connect.close();
				out.print("error");
			}
			

			
			
		}catch(Exception o){
			o.printStackTrace();
			connect.close();
			out.print("error");
		}
	
		
	} catch (Exception e) {
		
		e.printStackTrace();
		connect.close();
		out.print("error");
	}
	 
	connect.close();
%>
