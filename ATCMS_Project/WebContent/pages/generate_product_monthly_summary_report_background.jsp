<%@page import="org.apache.poi.xwpf.usermodel.TextAlignment"%>
<%@page import="org.apache.poi.xwpf.usermodel.Borders"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "com.atcms.*" %>
<%@ page import = "java.util.List" %>
<%@ page import = "java.util.ArrayList"%>
<%@ page import = "java.util.Properties" %>
<%@ page import = "javax.servlet.ServletContext" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.util.Date" %>
<%@ page import="java.math.BigInteger" %>
<%@ page import="java.io.FileOutputStream" %>
<%@ page import="org.apache.poi.xssf.usermodel.*" %>
<%@ page import="org.apache.poi.hssf.usermodel.HSSFCellStyle" %>
<%@ page import="org.apache.poi.hssf.util.*" %>
<%@ page import="org.apache.poi.ss.util.CellUtil" %>
<%@ page import="org.apache.poi.ss.usermodel.*" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>
<%@ page import = "java.text.DecimalFormat" %>




<%!
		public static final short EXCEL_COLUMN_WIDTH_FACTOR = 256; 
	    public static final short EXCEL_ROW_HEIGHT_FACTOR = 20; 
	    public static final int UNIT_OFFSET_LENGTH = 7; 
	    public static final int[] UNIT_OFFSET_MAP = new int[] { 0, 36, 73, 109, 146, 182, 219 };
	    
	    public static RichTextString createRichTextString (XSSFWorkbook workbook_in,String text,String font_name ,int font_size)
	    {
	    	 RichTextString richString = new XSSFRichTextString(text);
	    	 XSSFFont font = workbook_in.createFont();
	    	 font.setFontName(font_name);
			 font.setFontHeightInPoints((short)font_size);
			 richString.applyFont(font);
			 return richString;
	    }
	    
	    
		public static XSSFCellStyle CreateStyle(XSSFWorkbook workbook_in
							 				   ,String font_name_in 
							                   , int font_size_in
							                   , boolean isBold)
		{
				XSSFFont font = workbook_in.createFont();
				font.setFontName(font_name_in);
				font.setFontHeightInPoints((short)font_size_in);
				font.setBold(isBold);
				XSSFCellStyle style = workbook_in.createCellStyle();
				style.setFont(font);
				return style ;
		}
				
		public static XSSFCellStyle CreateCellBorder(XSSFWorkbook workbook_in 
										, boolean top 
										, boolean bot 
										, boolean left 
										, boolean right)
		{
				
				XSSFCellStyle style = workbook_in.createCellStyle();
				if(top){  style.setBorderTop(HSSFCellStyle.BORDER_THIN); }
				if(bot){ style.setBorderBottom(HSSFCellStyle.BORDER_THIN); }
				if(left){ style.setBorderLeft(HSSFCellStyle.BORDER_THIN); }
				if(right){ style.setBorderRight(HSSFCellStyle.BORDER_THIN); }
				
				return style ;
		}
		public static void DrawListCellBorder (XSSFWorkbook workbook , XSSFRow row_in){
			
			XSSFCellStyle style_border_top_left = CreateCellBorder(workbook,true,false,true,false);
			XSSFCellStyle style_border_top_right = CreateCellBorder(workbook,true,false,false,true);
			XSSFCellStyle style_border_top_left_right = CreateCellBorder(workbook,true,false,true,true);
			XSSFCellStyle style_border_bot_left = CreateCellBorder(workbook,false,true,true,false);
			XSSFCellStyle style_border_bot_right = CreateCellBorder(workbook,false,true,false,true);
			XSSFCellStyle style_border_bot_left_right = CreateCellBorder(workbook,false,true,true,true);
			XSSFCellStyle style_border_top = CreateCellBorder(workbook,true,false,false,false);
			XSSFCellStyle style_border_bot = CreateCellBorder(workbook,false,true,false,false);
			XSSFCellStyle style_border_left = CreateCellBorder(workbook,false,false,true,false);
			XSSFCellStyle style_border_right = CreateCellBorder(workbook,false,false,false,true);
			XSSFCellStyle style_border_left_right = CreateCellBorder(workbook,false,false,true,true);
			XSSFCellStyle style_border_full = CreateCellBorder(workbook,true,true,true,true);
			
			row_in.createCell(0);
			row_in.createCell(1);
			row_in.createCell(11);
			row_in.createCell(12);
			row_in.getCell(0).setCellStyle(style_border_left_right);
			row_in.getCell(1).setCellStyle(style_border_left_right);
			row_in.getCell(11).setCellStyle(style_border_left_right);
			row_in.getCell(12).setCellStyle(style_border_left_right);

		}
	    public static int widthUnits2Pixel(short widthUnits) {
	    	
	        int pixels = (widthUnits / EXCEL_COLUMN_WIDTH_FACTOR) * UNIT_OFFSET_LENGTH; 
	        int offsetWidthUnits = widthUnits % EXCEL_COLUMN_WIDTH_FACTOR; 
	        pixels += Math.floor((float) offsetWidthUnits / ((float) EXCEL_COLUMN_WIDTH_FACTOR / UNIT_OFFSET_LENGTH));   
	        return pixels; 
	    }
	    public static short pixel2WidthUnits(int pxs) {
	        short widthUnits = (short) (EXCEL_COLUMN_WIDTH_FACTOR * (pxs / UNIT_OFFSET_LENGTH)); 
	        widthUnits += UNIT_OFFSET_MAP[(pxs % UNIT_OFFSET_LENGTH)];  
	        return widthUnits; 
	    } 
	    


%>

<% 

	System.out.println("Start generate_external_purchase_invoice_monthly_summary_report_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
	
	/////////////////////////Start Get Data Process /////////////////////////////
	
	String month = request.getParameter("month");
	String year = request.getParameter("year");
	String form_of_report = "product_monthly_report";
	
	
	String cash_bill_id ="";
	String type ="";
	String ponum ="";
	String customer_name="";
	String customer_address_full="";  

	String inv_no="";
	String due_date="";
//	String delivery_date="";
//	String credit="";
	String tax_id = "";
	String address_line0 = "";
	String address_line1 = "";
	String status = "";
	String inv_no_temp ="";
	
	
	List<PurchaseDetail> pd_detail_list = new ArrayList<PurchaseDetail>();

	

	/*
	
	String pd_detail_query =    " SELECT product.name_th AS product_name_th  , product.name_en  AS product_name_en "+
																" , purchase_detail.purchase_id , purchase_main.inv_no "+
																" , company.name_th AS vendor_name_th , company.name_en AS vendor_name_en "+
																" , purchase_main.total_inc_vat , purchase_detail.unit  "+
																" , purchase_main.invoice_date , purchase_detail.price "+
																" , purchase_detail.quantity , purchase_detail.sum "+
																" , purchase_main.total_value "+
																" , purchase_main.total_vat "+
																" , purchase_main.total_inc_vat "+
												"  FROM purchase_detail "+
												" JOIN purchase_main "+
												" ON  purchase_detail.purchase_id = purchase_main.purchase_id "+
												" JOIN company "+
												" ON  purchase_main.vendor_id = company.company_id "	+						
												" JOIN product "+
 												" ON product.product_id = purchase_detail.product_id" +
 												" WHERE ( MONTH(purchase_main.invoice_date) = '"+month+"' "+ " AND YEAR(purchase_main.invoice_date) ='"+year+"' ) "+
 		 										" OR ( MONTH(purchase_main.calculate_date) = '"+month+"' "+ " AND YEAR(purchase_main.calculate_date) ='"+year+"' ) "+
												" AND purchase_main.type = 'VAT' "+
											    " ORDER BY purchase_main.invoice_date ASC , purchase_detail.index ASC ";

	ResultSet rs_pd_detail = connect.createStatement().executeQuery(pd_detail_query);
	System.out.println("pd_detail_query:"+pd_detail_query);
	int i=0;
	while(rs_pd_detail.next())
    {
			PurchaseDetail temp_pd_detail = new PurchaseDetail();
			
			if(rs_pd_detail.getString("product_name_th").equals("-"))
			{
				temp_pd_detail.setProductName(rs_pd_detail.getString("product_name_en"));
			}else{
				temp_pd_detail.setProductName(rs_pd_detail.getString("product_name_th"));
			}
			
	
			temp_pd_detail.setPurchaseId(rs_pd_detail.getString("purchase_id"));
			
			temp_pd_detail.setInvNo(rs_pd_detail.getString("inv_no"));
			
			if(rs_pd_detail.getString("vendor_name_th").equals("-"))
			{
				temp_pd_detail.setVendorName(rs_pd_detail.getString("vendor_name_en"));
			}else{
				temp_pd_detail.setVendorName(rs_pd_detail.getString("vendor_name_th"));
			}
			
			temp_pd_detail.setPurchaseMainTotalIncVat(rs_pd_detail.getString("total_inc_vat"));
			
			temp_pd_detail.setProductUnit(rs_pd_detail.getString("unit"));
			temp_pd_detail.setInvoiceDate(rs_pd_detail.getString("invoice_date"));
			temp_pd_detail.setPrice(rs_pd_detail.getString("price"));
			temp_pd_detail.setQuantity(rs_pd_detail.getString("quantity"));
			temp_pd_detail.setSum(rs_pd_detail.getString("sum"));
			temp_pd_detail.setPurchaseMainTotalValue(rs_pd_detail.getString("total_value"));
			temp_pd_detail.setPurchaseMainTotalVat(rs_pd_detail.getString("total_vat"));
			temp_pd_detail.setPurchaseMainTotalIncVat(rs_pd_detail.getString("total_inc_vat"));
			
			pd_detail_list.add(temp_pd_detail);
			
		//	System.out.println(i+":"+temp_order_detail.getAdtDescription());
			i++;
    }
	
	*/
	//System.out.println("pd_detail_list.size:"+pd_detail_list.size());
	
	//////////////////////////End Get Data Process /////////////////////////////
	
	
	/////////////////////////Start Writing Excel Process//////////////////////////
	
	

	try {		
		
		
		
			inv_no_temp = inv_no.replace("/","_");
			Date curr_date = new Date();
			
	//		String curr_year = curr_date.getYear()+2443+"";
	//		String curr_month = curr_date.getMonth()+1+"";
	
			String year_temp = (Integer.parseInt(year)+543)+"";
			
			    if(month.length()==1)
			    {
				    month = "0" + month;
			    }
			
			 System.out.println(application.getRealPath("/report/"));
			    
			File file_month_year = new File(application.getRealPath("/report/"+month+"_"+year_temp));
		    if (!file_month_year.exists()) {
	            if (file_month_year.mkdir()) {
	                System.out.println("file_month_year Created!");
	            } else {
	                System.out.println("Failed to create file file_month_year");
	            }
	        }
			File file_msr_inv = new File(application.getRealPath("/report/"+month+"_"+year_temp+"/monthly_summary_report/"));
			    if (!file_msr_inv.exists()) {
		            if (file_msr_inv.mkdir()) {
		                System.out.println("Folder Summary Monthly Report   Created!");
		            } else {
		                System.out.println("Failed to create file monthly_summary_report");
		            }
		        }
			    

	
			
			   
			String fileName = application.getRealPath("/report/"+month+"_"+year_temp+"/monthly_summary_report/"+form_of_report+".xlsx");
	
	
			XSSFWorkbook workbook = new XSSFWorkbook(); 
			

		    XSSFSheet page1 = workbook.createSheet("Page1");
		    
			
		 
		


			int count_detail = 0;
			System.out.println("Starting Write Bill detail to xls file");
	
     
			
			workbook.write(new FileOutputStream(fileName));
			
			
			String file_path = fileName;
		
			String pattern = Pattern.quote(System.getProperty("file.separator"));
			String[] parts = file_path.split(pattern);
			int temp = parts.length;
			String inv_file_name = parts[temp-1];
			
			String output = inv_file_name+"&"+file_path;
			
			
		
		    
			out.print(output);
			
			
	} catch (Exception e) {
		e.printStackTrace();
		connect.close();
		out.print("fail");
	}
	
	///////////////////////////////////////////////////////End Writing Excel Process/////////////////////////////////////////////////////////
	 
	connect.close();
%>
