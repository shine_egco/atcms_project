<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>
<%@ page import = "java.text.NumberFormat" %>
<%@ page import = "java.util.Locale" %>




<% 

	System.out.println("Start get_all_inv_value_by_month_year_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

	
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	
	String month = request.getParameter("month");
	String year = request.getParameter("year");
	System.out.println("month:"+month);
	System.out.println("year:"+year);
	

	Report report = new Report();
	
	
	  try{
    	  
    	  String sql_credit = " SELECT SUM(order_main.total_inc_vat)AS result "+
			    			  " FROM order_main "+
			    			  " WHERE MONTH(order_main.delivery_date)='"+month+"'"+
			    			  " AND YEAR(order_main.delivery_date)='"+year+"'";
 
    	  System.out.println("sql_credit:"+sql_credit);
    	  ResultSet rs_credit = connect.createStatement().executeQuery(sql_credit);
          
    	  while(rs_credit.next())
          {
    		  double result = Double.parseDouble(rs_credit.getString("result"));
    		  String final_result = NumberFormat.getNumberInstance(Locale.US).format(result);
    		  
    		  report.setCreditTotalIncVat(final_result);
    		  
          }
    	  ////////////////////////////////////////////////////////////////////////////
    	  String sql_cash = " SELECT SUM(cash_bill_main.total_inc_vat)AS result "+
			    			" FROM cash_bill_main "+
			    			" WHERE MONTH(cash_bill_main.bill_date)='"+month+"'"+
			    			" AND YEAR(cash_bill_main.bill_date)='"+year+"'";
    	  
    	  System.out.println("sql_cash:"+sql_cash);
    	  ResultSet rs_cash = connect.createStatement().executeQuery(sql_cash);
          
    	  while(rs_cash.next())
          {
    		  
    		  double result = Double.parseDouble(rs_cash.getString("result"));
    		  String final_result = NumberFormat.getNumberInstance(Locale.US).format(result);
    		  
    		  report.setCashBillTotalIncVat(final_result);
    		  
          }
    	  ///////////////////////////////////////////////////////////////////////////
    	  String sql_daily_novat = " SELECT SUM(daily_novat_main.total_value)AS result "+
	    			" FROM daily_novat_main "+
	    			" WHERE MONTH(daily_novat_main.bill_date)='"+month+"'"+
	    			" AND YEAR(daily_novat_main.bill_date)='"+year+"'";
    	  System.out.println("sql_daily_novat:"+sql_daily_novat);
    	  ResultSet rs_daily_novat = connect.createStatement().executeQuery(sql_daily_novat);
    	  while(rs_daily_novat.next())
          {
    		  double result = Double.parseDouble(rs_daily_novat.getString("result"));
    		  String final_result = NumberFormat.getNumberInstance(Locale.US).format(result);
    		  
    		  report.setDailyNovatTotalValue(final_result);
    		
          }
    	  
    	  
    	  System.out.println("credit_total_inc_vat:"+report.getCreditTotalIncVat());
    	  System.out.println("cash_total_inc_vat:"+report.getCashBillTotalIncVat());
       	  System.out.println("daily_novat_total_value:"+report.getDailyNovatTotalValue());
       	  
      	try {
			Json = mapper.writeValueAsString(report); 
			
		} catch (JsonGenerationException ex) {
			ex.printStackTrace();
		} catch (JsonMappingException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		System.out.println("Json:"+Json);
		out.print(Json);

      }
      catch(Exception x){
    	  System.out.println(x);
    	  connect.close();
    	  out.print("fail");
      }
     
  	  
     
	connect.close();
%>
