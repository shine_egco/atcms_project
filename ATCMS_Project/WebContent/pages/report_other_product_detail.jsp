<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../bower_components/sidebar/sidebar_style_3.css" rel="stylesheet" type="text/css"> 

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

 <script>
	
	 function formatMoney(number, places, symbol, thousand, decimal) {
			number = number || 0;
			places = !isNaN(places = Math.abs(places)) ? places : 0;
			symbol = symbol !== undefined ? symbol : "";
			thousand = thousand || ",";
			decimal = decimal || ".";
			var negative = number < 0 ? "-" : "",
			    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
			    j = (j = i.length) > 3 ? j % 3 : 0;
			return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
	}
	
	
	
	 
	 function fetch_old_product_name_to_edit_modal(id){
 		
			var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {			
					
					var jsonObj = JSON.parse(xmlhttp.responseText);
					if(jsonObj.length == 0) {
						//	massage();
							alert("Product not found !!");
						}
						else{
							
							var name_th = document.getElementById("edited_pro_name_th");
							var unit_th = document.getElementById("edited_pro_unit_th");
							var name_en = document.getElementById("edited_pro_name_en");
							var unit_en = document.getElementById("edited_pro_unit_en");
							var std_price = document.getElementById("edited_std_price");
							
							var edited_product_id = document.getElementById("edited_product_id");
							
								name_th.value = jsonObj.nameTH;
								name_en.value = jsonObj.nameEN;
								unit_th.value = jsonObj.unitTH;
								unit_en.value = jsonObj.unitEN;
								std_price.value = jsonObj.stdPrice;
								edited_product_id.value = jsonObj.productID;
							
						}
		

				}
				
			}
			xmlhttp.open("POST", "fetch_product_name_by_product_id_background.jsp?product_id="+id, true);
			xmlhttp.send();
 		
 		
 	}
 	
	 
	 function  fetch_other_product_detail(){
		 alert("fetch_other_product_detail");
		 
		 var product_id = sessionStorage.getItem("other_product_id_for_get_detail");
         
	      var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					
					var jsonObj = JSON.parse(xmlhttp.responseText);
					
					if(jsonObj.length == 0) {

						alert("no  Data");
					}
					else{
						alert("get Data:"+xmlhttp.responseText);
						var product_id = document.getElementById("product_id");
						var product_id_model = document.getElementById("product_id_model");
						var name_th = document.getElementById("name_th");
						var unit_th = document.getElementById("unit_th");
						var name_en = document.getElementById("name_en");
						var unit_en = document.getElementById("unit_en");
						var std_price = document.getElementById("std_price");
						var updated_date = document.getElementById("updated_date");
						//var is_stock_unit ;
						var scu_product_id = document.getElementById("scu_product_id");
						
					//	console.log(scu_product_id.value);
					//	var group_code = document.getElementById("group_code");
					//	var category = document.getElementById("category");
						
						
							product_id.value = jsonObj[0].productID;
							product_id_model.value = jsonObj[0].productID;
							name_th.value = jsonObj[0].nameTH;
							unit_th.value = jsonObj[0].unitTH;
							name_en.value = jsonObj[0].nameEN;
							unit_en.value = jsonObj[0].unitEN;
							std_price.value  = jsonObj[0].stdPrice;
							updated_date.value = jsonObj[0].updatedDate; 
							scu_product_id.value = jsonObj[0].scuproductID; 
							
	
						
					}

				}// end if check state
			}// end function
			

			xmlhttp.open("POST", "get_other_product_detail_by_product_id_background.jsp?product_id="+product_id, true);
			xmlhttp.send();
	 
	 }
	 
	 function fetch_purchase_detail(){
		 
		 var product_id = sessionStorage.getItem("other_product_id_for_get_detail");
		 
	      var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					
					var jsonObj = JSON.parse(xmlhttp.responseText);
					
					if(jsonObj.length == 0) {

						//alert("no  Data");
					}
					else{
							for(i in jsonObj) {
								
							
							//alert(jsonObj[i].invoiceDate);
							
							$('#dataTables-example-purchase').DataTable().row.add([
                              '<tr><td><center>'+jsonObj[i].invoiceDate+'</center></td>'
	                           ,'<td>'+jsonObj[i].vendorName+'</td>'
	                           ,'<td><center>'+jsonObj[i].invNo+'</center></td>'
	                           ,'<td><center>'+jsonObj[i].quantity+'</center></td>'
	                           ,'<td><center>'+jsonObj[i].price+'</center></td></tr>'                              
	                         	]).draw();
						}
							

	
					}

				}// end if check state
			}// end function
			

			xmlhttp.open("POST", "get_purchase_detail_by_product_id_background.jsp?product_id="+product_id, true);
			xmlhttp.send();
		 
		 
	 }

	 function fetch_credit_detail(){
		 
		 var product_id = sessionStorage.getItem("other_product_id_for_get_detail");
		 
	      var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					
					var jsonObj = JSON.parse(xmlhttp.responseText);
					
					if(jsonObj.length == 0) {

						//alert("no  Data");
					}
					else{
						
						for(i in jsonObj) {
							
							
							
							$('#dataTables-example-credit').DataTable().row.add([
                              '<tr><td><center>'+jsonObj[i].invoiceDate+'</center></td>'
	                           ,'<td>'+jsonObj[i].customerName+'</td>'
	                           ,'<td><center>'+jsonObj[i].invoiceNo+'</center></td>'
	                           ,'<td><center>'+jsonObj[i].quantity+'</center></td>'
	                           ,'<td><center>'+jsonObj[i].price+'</center></td></tr>'                              
	                         	]).draw();
						}

					}

				}// end if check state
			}// end function
			

			xmlhttp.open("POST", "get_credit_inv_detail_by_product_id_background.jsp?product_id="+product_id, true);
			xmlhttp.send();
		 
		 
	 }
	 
	function fetch_cash_detail(){
		 
		 var product_id = sessionStorage.getItem("other_product_id_for_get_detail");
		 
	      var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					
					var jsonObj = JSON.parse(xmlhttp.responseText);
					
					if(jsonObj.length == 0) {

						//alert("no  Data");
					}
					else{
						
						for(i in jsonObj) {

							$('#dataTables-example-cash').DataTable().row.add([
                              '<tr><td><center>'+jsonObj[i].invoiceDate+'</center></td>'
	                           ,'<td>'+jsonObj[i].customerName+'</td>'
	                           ,'<td><center>'+jsonObj[i].invNo+'</center></td>'
	                           ,'<td><center>'+jsonObj[i].quantity+'</center></td>'
	                           ,'<td><center>'+jsonObj[i].price+'</center></td></tr>'                              
	                         	]).draw();
						}

					}

				}// end if check state
			}// end function
			

			xmlhttp.open("POST", "get_cash_inv_detail_by_product_id_background.jsp?product_id="+product_id, true);
			xmlhttp.send();
		 
		 
	 }
	 
	 
	
	 function numberWithCommas(x) {
		    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	 
	 
	 function submit_edited_product(){
 		
			var name_th = document.getElementById("edited_pro_name_th");
			var unit_th = document.getElementById("edited_pro_unit_th");
			var name_en = document.getElementById("edited_pro_name_en");
			var unit_en = document.getElementById("edited_pro_unit_en");
			var std_price = document.getElementById("edited_std_price");
			var product_id = document.getElementById("edited_product_id");
			var temp_scu_product_id = document.getElementById("pd_name-0");
			
			
			var temp =  (temp_scu_product_id.value).split(",");
			
			var scu_product_id = temp[0];
			
			alert("scu_product_id:"+scu_product_id);
			
			var today = new Date();
			var year = ""+(today.getYear()+1900); 
			var month = "" +(today.getMonth()+1);
			var date = ""+today.getDate();
			
			if(date.length==1)
			{
				date = "0"+date;
			}
			if(month.length==1)
			{
				month = "0"+month;
			}
			var updated_date_str = year+"-"+month+"-"+date;
			
			
			
			
			
			var parameter = "product_id="+product_id.value+
							"&name_th="+name_th.value+
							"&unit_th="+unit_th.value+
							"&name_en="+name_en.value+
							"&unit_en="+unit_en.value+
							"&std_price="+std_price.value+
							"&scu_product_id="+scu_product_id+
							"&updated_date="+updated_date_str;
			
			//alert(parameter);
			
			var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
				  //  $('#show_inv_history').modal('show');
					//var jsonObj = JSON.parse(xmlhttp.responseText);
					
					alert(xmlhttp.responseText);
					
					$('#editProModel').modal('hide');
					location.reload();

				}
				
			}
			xmlhttp.open("POST", "update_product_name_background.jsp?"+parameter, true);
			xmlhttp.send();

 	}
	 
 	function show_modal_edit_product_name(id){
		
 	
		fetch_old_product_name_to_edit_modal(id);
		$('#editProModel').modal('show');
		
	}
 	
	function search_pro_name(temp){
		
		 var input_box = temp;
		 var keyword = input_box.value;
		
		 //alert("input_box:"+input_box.id);
		 var temp2 = (input_box.id).split("-");  // get row from input value
		 
		 var row = temp2[1];
		 

		 if(keyword != "") {
				
			
				var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
						var jsonObj = JSON.parse(xmlhttp.responseText);
						
						if(jsonObj.length == 0) {
	
						//	alert("no  Data");
						}
						else{
							//alert("Have Data");

							
							$('#pro_lists-'+row).empty();
							
							
							for(i in jsonObj) {
							var text = jsonObj[i].productID+","+jsonObj[i].nameTH+","+jsonObj[i].nameEN;
							//	var text = jsonObj[i].nameTH+","+jsonObj[i].nameEN;
										
						//	text = text.replace("""","");	
							var id = jsonObj[i].productID ; 
								//alert(jsonObj[i].nameTH+" "+jsonObj[i].nameEN);
								var option = '<option id="'+id+'"  value="'+text+'" >' ;
								
								$('#pro_lists-'+row).append(option);
							}
							
						}
					}
					
				}
				
				xmlhttp.open("POST", "get_product_by_keyword_background.jsp?keyword="+keyword, true);
				xmlhttp.send();
			}
		

}
	
	
</script>
<style>
	.text-right {
 		 text-align:right;
	}
	/*    --------------------------------------------------
	:: General
	-------------------------------------------------- */

	.content h1 {
		text-align: center;
	}
	.content .content-footer p {
		color: #6d6d6d;
	    font-size: 12px;
	    text-align: center;
	}
	.content .content-footer p a {
		color: inherit;
		font-weight: bold;
	}
	
	/*	--------------------------------------------------
		:: Table Filter
		-------------------------------------------------- */
		
	.panel {
		border: 1px solid #ddd;
		background-color: #fcfcfc;
	}
	
	.panel .btn-group .btn {
		transition: background-color .3s ease;
	}
	.table-filter {
		background-color: #fff;
		border-bottom: 1px solid #eee;
	}
	.table-filter tbody tr:hover {
		cursor: pointer;
		background-color: #eee;
	}
	.table-filter tbody tr td {
		padding: 10px;
		vertical-align: middle;
		border-top-color: #eee;
	}
	.table-filter tbody tr.selected td {
		background-color: #eee;
	}
	.table-filter tr td:first-child {
		width: 3px;
	}
	.table-filter tr td:nth-child(2) {
		width: 3px;
	}

	.table-filter .star {
		color: #ccc;
		text-align: center;
		display: block;
	}
	.table-filter .star.star-checked {
		color: #F0AD4E;
	}
	.table-filter .star:hover {
		color: #ccc;
	}
	.table-filter .star.star-checked:hover {
		color: #F0AD4E;
	}

	.table-filter .media-meta {
		font-size: 13px;
		color: #999;
	}
	.table-filter .media .title {
		color: #2BBCDE;
		font-size: 16px;
		font-weight: bold;
		line-height: normal;
		margin: 1px;
	}
	.table-filter .media .title span {
		font-size: .8em;
		margin-right: px;
	}

	.table-filter .media .summary {
		font-size: 14px;
	}	
	.scrollit {
    	overflow-x: hidden;
    	overflow-y: auto;
   	 	height:380px;
   	 	
   	  
	}
	

</style>
    

</head>

<body>

    <div id="wrapper">

    		<nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>
                <div class="sidebar-header">
                    <h3>Bored Sidebar</h3>
                </div>
                
    
        		<div id="includedSidebar"></div>
        		
        	</nav>



        <!-- Navigation -->
       

        <div id="content" class="form-control">
        
        	   <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-justify"></i>
                                <span></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Page</a></li>
                       
                            </ul>
                        </div>
                    </div>
                </nav>
             <div class="row">
             <br>
	           <div class="col-lg-3">
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                          Product Detail
	                        </div>
	                        
	                        <!-- /.panel-heading -->
	                        <div class="panel-body">
	                        	<input name="product_id" id="product_id" type="text" class="form-control" placeholder="Product ID" readonly>
                                <br>	
	                        	<label>Name TH (ชื่อภาษาไทย)</label>
	                           	<input id="name_th" name = "name_th" class="form-control" value="" disabled  >
	                           	<br>
	  
	                           	<label>Name EN (ชื่อภาษาอังกฤษ)</label>
	                           	<input id="name_en" name = "name_en" class="form-control" value="" disabled  >
	                           	<br>
                         	  	<table>
                      			 		<tr class="pagination-centered">
                      			 		
                      			 			<td style="padding-left:2em;">
                        						<label>Unit EN </label>	                        						
                        					</td>
                        					<td style="padding-left:1em;">
                        					 	<input type="text" id="unit_en" name ="unit_en" class="form-control" disabled>  				                        					
                        					</td>	
                        					
                        					<td style="padding-left:2em;">
                        						<label>Unit TH</label>	                        						
                        					</td>
                        					<td style="padding-left:1em;">
                        					 	<input type="text" id="unit_th" name ="unit_th" class="form-control" disabled>  					                        					
                        					</td>	

                        	   			</tr>

	                        	</table>
	                        	
	                        	<br>
	                        	
	                        	<label>Standard Price</label>
	                           	<input id="std_price" name = "std_price" class="form-control"  disabled  >
	                           	<br>
	                           	
	                           	<label>Updated Date (YYYY/MM/DD)</label>
	                           	<input id="updated_date" name = "updated_date" class="form-control"  disabled  >
	                           	<br>
	                           	
	                           	<label>SCU Product ID </label>
	                           	<input id="scu_product_id" name = "scu_product_id" class="form-control"  disabled  >
								
	                        		<hr>
	                        		
	                        		<button type="button" class="form-control btn btn-warning" id="product_id_model" value="" onclick="show_modal_edit_product_name(this.value)">Edit</button>

	                        		<br>
	                          					
													                            
	                        </div>
	                        <!-- .panel-body -->
	                    </div>
                    <!-- /.panel -->
                </div>
                 <div class="col-lg-4">
                 		   <div class="panel panel-default">
			                      <div class="panel-heading">
			                           	Credit-Record
			                        </div>
			                        	  <div class="panel-body">
	                        					<div class="dataTable_wrapper">
													<table class="table table-striped table-bordered table-hover" id="dataTables-example-credit">
					                                    <thead>
					                                        <tr>
					                                        	<th style="width: 70px; text-align: center;">Date</th> 
					                                      		<th>Customer</th>   
					                                            <th style="width: 70px; text-align: center;">Inv No.</th>
					                                            <th style="width: 70px; text-align: center;">Quantity</th>
					                                            <th style="width: 70px; text-align: center;">Price</th>
					                                        </tr>
					                                    </thead>
					                                    <tbody>
			
			                                   			 </tbody>
			                                		</table>
			                                	</div>
											</div>
               			 </div>
               		  </div>
               		 
               	<div class="col-lg-4">
                 		   <div class="panel panel-default">
			                      <div class="panel-heading">
			                           	Cash-Record
			                        </div>
			                        	  <div class="panel-body">
	                        					<div class="dataTable_wrapper">
													<table class="table table-striped table-bordered table-hover" id="dataTables-example-cash">
					                                    <thead>
					                                        <tr>
					                                      		<th style="width: 70px; text-align: center;">Date</th> 
					                                      		<th>Customer</th>   
					                                            <th style="width: 70px; text-align: center;">Inv No.</th>
					                                            <th style="width: 70px; text-align: center;">Quantity</th>
					                                            <th style="width: 70px; text-align: center;">Price</th>

					                                        </tr>
					                                    </thead>
					                                    <tbody>
			
			                                   			 </tbody>
			                                		</table>
			                                	</div>
										</div>
               			 </div>
               	</div>
                </div>
                
                <div class="row">
                	<div class="col-lg-3">
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                          
	                        </div>
	                        
	                         <div class="panel-body">
	                         		<br>
	                         		<br>
	                         </div>
	            
	                    </div>
                    <!-- /.panel -->
                   </div>
                
                	  
       
               		
               		<div class="col-lg-4">
                 		   <div class="panel panel-default">
			                      <div class="panel-heading">
			                           	Purchase-Record
			                        </div>
			                        	  <div class="panel-body">
	                        					<div class="dataTable_wrapper">
													<table class="table table-striped table-bordered table-hover" id="dataTables-example-purchase">
					                                    <thead>
					                                        <tr>
					                                      		<th style="width: 70px; text-align: center;">Date</th> 
					                                      		<th>Customer</th>   
					                                            <th style="width: 70px; text-align: center;">Inv No.</th>
					                                            <th style="width: 70px; text-align: center;">Quantity</th>
					                                            <th style="width: 70px; text-align: center;">Price</th>

					                                        </tr>
					                                    </thead>
					                                    <tbody>
			
			                                   			 </tbody>
			                                		</table>
			                                	</div>
										</div>
               			 </div>
               		</div>
                </div>
                

                <!-- /.col-lg-12 -->
             </div>
             
              <div class="modal fade" id="editProModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style=" position: fixed; top: 10%; left: 10%; right: 10%; bottom: 15% ">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Edit Product Name</h4>
                                        </div>
                                        <div class="modal-body">
                                       		<div class="row">
                                       			.	<div class="col-md-6">
                                       					<input name="edited_pro_name_th" id="edited_pro_name_th" type="text" class="form-control" placeholder="ชื่อภาษาไทย">
                                       				</div>
                                       				<div class="col-md-4">
                                       					<input name="edited_pro_unit_th" id="edited_pro_unit_th" type="text" class="form-control" placeholder="หน่วยภาษาไทย">
                                       				</div>
                                       				
                                       		</div>
                                       		<div class="row">
                                       				<br>
                                       				<div class="col-md-6">
                                       					<input name="edited_pro_name_en" id="edited_pro_name_en" type="text" class="form-control" placeholder="Name Eng">
                                       				</div>
                                       				<div class="col-md-4">
                                       					<input name="edited_pro_unit_en" id="edited_pro_unit_en" type="text" class="form-control" placeholder="Unit Eng">
                                       					<input type="hidden" name="edited_product_id" id="edited_product_id">
                                       				</div>
                                       				<br>
                                       		
                                       		</div>        
                                       				<br> 
                                       			
                                       		<div class="row">
                                       				<br>
                                       				<div class="col-md-6">
                                       					<input name="text_std_price" id="text_std_price" type="text" class="form-control" placeholder="Set Standard Price to ->" disabled>
                                       				</div>
                                       				<div class="col-md-4">
                                       					<input name="edited_std_price" id="edited_std_price" type="text" class="form-control" placeholder="Baht">
                                       				</div>
                                       	

                                       		</div>     
                                       		
                                       		<div class="row">
                                       				<br>
                                       				<div class="col-md-6">
                                       					<input name="text_is_stock_unit" id="text_is_stock_unit" type="text" class="form-control" placeholder="Refer This Product to SCU Product:" disabled>
                                       				</div>
                                       			
                                       	

                                       		</div>     
                                       		
                                       		<div class="row">
                                       					<br>
                                       				<div class="col-md-8">
                                       					 <input list="pro_lists-0" type="text" name='pd_name-0' id = "pd_name-0" class="form-control"
																    		onkeyup="search_pro_name(this)"
																    		onchange="" />
																  <datalist id="pro_lists-0"></datalist>
                                       				</div>
                                       		</div>
                                       		
                                       		                                     
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" name="submit_edit_product_but"  id="submit_edit_product_but" class="btn btn-primary" onclick="submit_edited_product()">Submit Change</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
    				  </div>
      

        
        </div>
        <!-- /#page-wrapper -->


    
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script> 
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    
    
    <script>
    $(document).ready(function() {

    	
    	$("#includedSidebar").load("sidebar.jsp"); 
    	
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').fadeOut();
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        
        
    	
    	fetch_other_product_detail();
    	fetch_purchase_detail();   
    	fetch_credit_detail();
    	fetch_cash_detail();

    	
        $('#dataTables-example-credit').DataTable({
            responsive: true
    	});
        
        $('#dataTables-example-cash').DataTable({
            responsive: true
    	});
        $('#dataTables-example-purchase').DataTable({
            responsive: true
    	});


 
 
    	
    });
    </script>

</body>

</html>
