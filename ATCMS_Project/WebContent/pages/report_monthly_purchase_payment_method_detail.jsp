<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
 <link href="../bower_components/sidebar/sidebar_style_3.css" rel="stylesheet" type="text/css"> 

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

 <script>		
 
 function getMonthString(temp){
		
		var month = "";
				switch (temp) {
				
			    case "1":
			    	month = "January";
			        break;
			    case "2":
			    	month = "February";
			        break;
			    case "3":
			    	month = "March";
			        break;
			    case "4":
			    	month = "April";
			        break;
			    case "5":
			    	month = "May";
			        break;
			    case "6":
			    	month = "June";
			    case "7":
			    	month = "July";
			    case "8":
			    	month = "August";
			    case "9":
			    	month = "September";
			    case "10":
			    	month = "October";
			    case "11":
			    	month = "November";
			    case "12":
			    	month = "December";
			}
				
			return month;
		
	}
	
	
	function fetch_monthly_summary_report_detail(){
		
		 var report_id = sessionStorage.getItem("report_id_for_get_detail");
		 
		// alert("report_id:"+report_id);
		 
	
			var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
					
					var jsonObj = JSON.parse(xmlhttp.responseText);
					
					if(xmlhttp.responseText=="fail")
					{
						alert("Update Error");
					}else{
						
						//alert("Success");
						var month = document.getElementById("report_month");
						var year = document.getElementById("report_year");
						
						document.getElementById("report_header");
						
						
								month.value = jsonObj[0].month;
								year.value =  jsonObj[0].year;
								
								
								
								document.getElementById("report_header").innerHTML = "Payment Method Report : "+getMonthString(month.value)+" "+year.value;
							      fetch_monthly_payment_method();
								
							//	alert(month.value+" , " +year.value);
						
						//location.reload();
					}
					
				}// end if check state
			}// end function
			
		
			xmlhttp.open("POST", "get_monthly_summary_report_by_report_id_background.jsp?report_id="+report_id, true);
			xmlhttp.send();

		
		
	}
	
 
 	function fetch_monthly_payment_method(){
 		
 		var i;
 
		var month = document.getElementById("report_month");
		var year = document.getElementById("report_year");

 		var main_row = document.getElementById("main_row");
 		
 		for (i=0;i<4;i++)
 		{
 			var col_lg_6 = document.createElement("div");
 				col_lg_6.className = "col-lg-6";
 				
 			var panel_main = document.createElement("div");
 				panel_main.className = "panel panel-default";
 				
 			var panel_heading = document.createElement("div");
 				panel_heading.className = "panel-heading";
 				panel_heading.innerHTML ="Table Heading" ;
 				
 				switch (i) {
 					case 0 : panel_heading.innerHTML ="Private Acc0" ;
 						break;
 					case 1 : panel_heading.innerHTML = "Company Cash ";
 						break;
 					case 2 : panel_heading.innerHTML = "Company Cheque";
 						break;
 					default : panel_heading.innerHTML = "Personal Cash";
 					
 				}
 				
 				
 				panel_heading.id = "p_heading_"+i;
 					
 				
 	 		var panel_body = document.createElement("div");
 	 			panel_body.className = "panel-body";
 	 			
 	 		var wrapper = document.createElement("div");
 	 			wrapper.className = "dataTable_wrapper";
 	 		
 	 		var table = document.createElement("table");
 	 			table.className = "table table-striped table-bordered table-hover";
 	 			table.id = "dataTables_"+i;
 	 			
 	 			///  Assign dataTables_0  = private_acc0
 	 			///         dataTables_1 = company_cash
 	 			///			dataTables_2 = company_cheque
 	 			///			dataTables_3 = unassigned
 	 		
 	 		var thead = document.createElement("thead");
 	 		
 	 		var tr = document.createElement("tr");
 	 		
 	 		var th0 = document.createElement("th");
 	 			th0.innerHTML = "Inv Date" ;
 	 		var th1 = document.createElement("th");
 	 			th1.innerHTML = "Vendor Name";
 	 		var th2 = document.createElement("th");
 	 			th2.innerHTML = "Inv No."
 	 			/*
 	 		var th3 = document.createElement("th");
 	 			th3.innerHTML = "Value";
 	 		var th4 = document.createElement("th");
 	 			th4.innerHTML = "Vat";
 	 			*/
 	 		var th5 = document.createElement("th");
 	 			th5.innerHTML = "Value Inc Vat"
 	 		
 	 		var sum_value = document.createElement("input");
 	 			sum_value.setAttribute("type","text");
 	 			sum_value.readOnly = true;
 	 			sum_value.className = "form-control";
 	 			sum_value.style.textAlign = "right";
 	 			sum_value.id = "sumvalue_datatable_"+i;
 	 			
 	 			
 	 			tr.appendChild(th0);
 	 			tr.appendChild(th1);
 	 			tr.appendChild(th2);
 	 		//	tr.appendChild(th3);
 	 		//	tr.appendChild(th4);
 	 			tr.appendChild(th5);
 	 			
 	 			thead.appendChild(tr);
 	 			table.appendChild(thead);
 	 			wrapper.appendChild(table);
 	 			wrapper.appendChild(sum_value);
 	 			panel_body.appendChild(wrapper);
 	 			panel_heading.appendChild(panel_body);
 	 			panel_main.appendChild(panel_heading);
 	 			col_lg_6.appendChild(panel_main);

				main_row.appendChild(col_lg_6);
 			
 		}
 		
 		
 		
 		

		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
			xmlhttp.onreadystatechange = function() {
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
				
				var jsonObj = JSON.parse(xmlhttp.responseText);
				
				if(jsonObj.length == 0)
				{
	
					
				}else{
					//alert("back from purchase_main_background");
						var	sumvalue_dt0 = 0;
						var sumvalue_dt1 = 0;
						var sumvalue_dt2 = 0;
						var sumvalue_dt3 = 0;
									
	
								for(i in jsonObj) {
									
									switch((jsonObj[i].paymentMethod)){
										case "private_ACC" :
											
										
												$('#dataTables_0').DataTable().row.add([
												'<tr><td><center>'+jsonObj[i].invoiceDate+'</center></td>'
												   ,'<td><center>'+jsonObj[i].vendorName+'</center></td>'
						                           ,'<td><center>'+jsonObj[i].invNo+'</center></td>'
						                       //    ,'<td><center>'+jsonObj[i].totalValue+'</center></td>'
						                       //    ,'<td><center>'+jsonObj[i].totalVat+'</center></td>'
						                           ,'<td><center>'+jsonObj[i].totalIncVat+'</center></td></tr>'
												]).draw();			
												
												sumvalue_dt0 = sumvalue_dt0 + parseFloat(jsonObj[i].totalIncVat);
												
											  break;
										case "company_cash" :
												$('#dataTables_1').DataTable().row.add([
												'<tr><td><center>'+jsonObj[i].invoiceDate+'</center></td>'
												   ,'<td><center>'+jsonObj[i].vendorName+'</center></td>'
						                           ,'<td><center>'+jsonObj[i].invNo+'</center></td>'
						                    //       ,'<td><center>'+jsonObj[i].totalValue+'</center></td>'
						                    //       ,'<td><center>'+jsonObj[i].totalVat+'</center></td>'
						                           ,'<td><center>'+jsonObj[i].totalIncVat+'</center></td></tr>'
												]).draw();		
												
												sumvalue_dt1 = sumvalue_dt1 + parseFloat(jsonObj[i].totalIncVat);
												
											  break;
										case "company_cheque" :
												$('#dataTables_2').DataTable().row.add([
												'<tr><td><center>'+jsonObj[i].invoiceDate+'</center></td>'
												   ,'<td><center>'+jsonObj[i].vendorName+'</center></td>'
						                           ,'<td><center>'+jsonObj[i].invNo+'</center></td>'
						                //           ,'<td><center>'+jsonObj[i].totalValue+'</center></td>'
						                //           ,'<td><center>'+jsonObj[i].totalVat+'</center></td>'
						                           ,'<td><center>'+jsonObj[i].totalIncVat+'</center></td></tr>'
												]).draw();		
												
												sumvalue_dt2 = sumvalue_dt2 + parseFloat(jsonObj[i].totalIncVat);
												
											  break;
										default :
												$('#dataTables_3').DataTable().row.add([
												'<tr><td><center>'+jsonObj[i].invoiceDate+'</center></td>'
												   ,'<td><center>'+jsonObj[i].vendorName+'</center></td>'
						                           ,'<td><center>'+jsonObj[i].invNo+'</center></td>'
						                 //          ,'<td><center>'+jsonObj[i].totalValue+'</center></td>'
						                 //          ,'<td><center>'+jsonObj[i].totalVat+'</center></td>'
						                           ,'<td><center>'+jsonObj[i].totalIncVat+'</center></td></tr>'
												]).draw();		
										
												sumvalue_dt3 = sumvalue_dt3 + parseFloat(jsonObj[i].totalIncVat);
											
							
									} // end Switch
								
							} // end For Loop
					
							var dom_sum_value_dt0 = document.getElementById("sumvalue_datatable_0");
							var dom_sum_value_dt1 = document.getElementById("sumvalue_datatable_1");
							var dom_sum_value_dt2 = document.getElementById("sumvalue_datatable_2");
							var dom_sum_value_dt3 = document.getElementById("sumvalue_datatable_3");
							
								
							
							dom_sum_value_dt0.value = "Total Value  :  " + numberWithCommas(sumvalue_dt0);
							dom_sum_value_dt1.value = "Total Value  :  " + numberWithCommas(sumvalue_dt1);
							dom_sum_value_dt2.value = "Total Value  :  " + numberWithCommas(sumvalue_dt2);
							dom_sum_value_dt3.value = "Total Value  :  " + numberWithCommas(sumvalue_dt3);
							
						//	document.getElementById("p_heading_0").innerHTML = "Private Acc0"; 
							
							
					
					
		
				}
				

			}// end if check state
		}// end function
		
	

		
		xmlhttp.open("POST", "get_purchase_main_by_month_and_year_background.jsp?year="+year.value+"&month="+month.value, true);
		xmlhttp.send();

 		
 	}
 	
	 function numberWithCommas(x) {
		 
		    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		    
		}
	
</script>
<style>
	.text-right {
 		 text-align:right;
	}

</style>
    

</head>

<body>

    <div id="wrapper">

    		<nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>
                <div class="sidebar-header">
                    <h3>Bored Sidebar</h3>
                </div>
                
    
        		<div id="includedSidebar"></div>
        		
        	</nav>



        <!-- Navigation -->
       

        <div id="content" class="form-control">
        
        	   <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-justify"></i>
                                <span></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Page</a></li>
                       
                            </ul>
                        </div>
                    </div>
                </nav>
        
             <div class="row">
	                <div class="col-lg-12">
	                    <h2 class="page-header" id="report_header"></h2>
	                     <input id="report_month" type="hidden">
	                  	 <input id="report_year" type="hidden">
	                </div>
                <!-- /.col-lg-12 -->
             </div>
             
             <div id="main_row" class="row">
             
      

           	 </div>

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
 <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    
    
    <script>
    $(document).ready(function() {
    	
    	

    	
    	$("#includedSidebar").load("sidebar.jsp"); 
    	
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').fadeOut();
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        
        
        $('#dataTables-example').DataTable({
                responsive: true
        });
        fetch_monthly_summary_report_detail();
        
        

    });
    </script>
	<script>
    // tooltip demo
	    $('.tooltip-demo').tooltip({
	        selector: "[data-toggle=tooltip]",
	        container: "body"
	    })
	
	    // popover demo
	    $("[data-toggle=popover]")
	        .popover()
    </script>
</body>

</html>
