<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "com.atcms.*" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "java.util.Properties" %>
<%@ page import = "javax.servlet.ServletContext" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.util.Date" %>



<% 

	System.out.println("Start create_order_full_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
	///////////////////file_path parameter/////////////
	String customer_id = request.getParameter("company_id");
	String po_no = request.getParameter("po_no");
	String dalivery_date_str = request.getParameter("dalivery_date");
	String due_date_str = request.getParameter("due_date");
	
	String total_vat = request.getParameter("total_vat");
	String total_inc_vat = request.getParameter("total_inc_vat");
	int index = Integer.parseInt(request.getParameter("index"));
	
	String temporary_inv_no = request.getParameter("temp_inv_no_value");
	String compute_credit = request.getParameter("compute_credit");
	
	
//	double d_total_value = Double.parseDouble(total_inc_vat)-Double.parseDouble(total_vat);
	String total_value = request.getParameter("total_value");
	
	String current_inv_no="";
	String inv_no_part_two = "";
	//String full_inv_no ="";
	
	int temp_inv_no = 0 ;
	
	
	System.out.println("customer_id:"+customer_id);
	System.out.println("po_no:"+po_no);
	System.out.println("dalivery_date:"+dalivery_date_str);
	System.out.println("due_date:"+due_date_str);
	System.out.println("total_vat:"+total_vat);
	System.out.println("total_inc_vat:"+total_inc_vat);
	
	 ///////////////////////////////////////////////////
//	List<Product> product_list = new ArrayList<Product>();

	ObjectMapper mapper = new ObjectMapper();
	try {		
		
		//get current credit_inv_no
		
		
		String sql_query_inv_no = " SELECT * "+
								  " FROM init_parameter "+
							  	  " WHERE init_parameter.index='1'";
		
		ResultSet rs_inv_no =  connect.createStatement().executeQuery(sql_query_inv_no);
		System.out.println("sql_query_inv_no:"+sql_query_inv_no);	
		
		while(rs_inv_no.next())
		{
			current_inv_no = rs_inv_no.getString("start_credit_inv_no");
			temp_inv_no = Integer.parseInt(rs_inv_no.getString("start_credit_inv_no"));
		}
		
		int add = 3 -current_inv_no.length();
		
		for(int i=0;i<add;i++)
		{
			current_inv_no = "0"+ current_inv_no;
		}
	
		try{

			String order_main_id ="ORD"+ KeyGen.generateOrderID();
			String type ="VAT";
			Date create_date = new Date();
			String year = ""+(create_date.getYear()+1900); 
			String month = "" +(create_date.getMonth()+1);
			String date = ""+create_date.getDate();
			
			if(date.length()==1)
			{
				date = "0"+date;
			}
			if(month.length()==1)
			{
				month = "0"+month;
			}
			String create_date_str = year+"-"+month+"-"+date;
			
	//		full_inv_no = ""+(create_date.getYear()+1900+543-2500)+"/"+month+"/"+current_inv_no;
			
					
			//	Date create_date ;
		//	Date delivery_date ;
		//	Date due_date ;
			String  init_status = "order_created";
			String sql_query =" INSERT INTO `order_main`(`order_id`"+
													   ", `type`"+
													   ", `create_date`"+
													   ", `ponum`"+
													   ", `customer_id`"+
													   ", `status`"+
													   ", `delivery_date`"+
													   ", `due_date`"+
													   ", `total_value`"+
													   ", `total_vat`"+
													   ", `total_inc_vat`"+
													   ", `compute_credit`"+
													   ", `inv_no`) " +
							   " VALUES ('"+order_main_id+"'"+
										",'"+type+"'" +
									    ",'"+create_date_str+"'"+
										",'"+po_no+"'"+
									    ",'"+customer_id+"'"+
										",'"+init_status+"'"+
									    ",'"+dalivery_date_str+"'"+
										",'"+due_date_str+"'"+
									    ",'"+total_value+"'"+
										",'"+total_vat+"'"+
									    ",'"+total_inc_vat+"'"+
									    ",'"+compute_credit+"'"+
										",'"+temporary_inv_no+"')";
	
							   
			System.out.println("sql_query:"+sql_query);
			try{
				connect.createStatement().executeUpdate(sql_query);
				String 	pd_id ;
				String  pd_addition;
				String  pd_price ;
				String  pd_quantity ;
			    String  pd_sum ;
			    String  pd_unit ;
			    
			    
				
				
				for(int i=0;i<index;i++)
				{
					
					pd_id = request.getParameter("pd_id"+i);
					pd_addition = 	request.getParameter("pd_name"+i);	
					pd_price = request.getParameter("pd_price"+i);
					pd_quantity = request.getParameter("pd_quantity"+i);
				    pd_sum = request.getParameter("pd_sum"+i);
				    pd_unit = request.getParameter("pd_unit"+i);
				    
				    
				    String sql_order_detail = " INSERT INTO `order_detail`(`order_id`"+
												", `product_id`"+
												", `price`"+
												", `type`"+
												", `adt_description`"+
												", `quantity`"+
												", `sum`"+
												", `unit`)"+
						" VALUES ('"+order_main_id+"'"+
								  ",'"+pd_id+"'"+
							      ",'"+pd_price+"'"+
								  ",'"+type+"'"+
							      ",'"+pd_addition+"'"+
								  ",'"+pd_quantity+"'"+
							      ",'"+pd_sum+"'"+
								  ",'"+pd_unit+"')";
				    connect.createStatement().executeUpdate(sql_order_detail);
				    System.out.println("Success:"+i+":"+sql_order_detail);
	
				    
				};
				
				try{
					//update new credit_inv_no
					temp_inv_no ++;
					String sql_update_credit_inv_no = " UPDATE init_parameter "+
						 						      " SET start_credit_inv_no = "+temp_inv_no+
													  " WHERE init_parameter.index = 1 ";
					
					System.out.println();
					connect.createStatement().executeUpdate(sql_update_credit_inv_no);
					
					out.print(order_main_id);
					
				}catch(Exception q){

					q.printStackTrace();
					connect.close();
					out.print("error");
				}
				
	
			
			
			}catch(Exception x){
			
				x.printStackTrace();
				connect.close();
				out.print("error");
			}
			

			
			
		}catch(Exception o){
			o.printStackTrace();
			connect.close();
			out.print("error");
		}
	
		
	} catch (Exception e) {
		
		e.printStackTrace();
		connect.close();
		out.print("error");
	}
	 
	connect.close();
%>
