<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   <link href="../bower_components/sidebar/sidebar_style_3.css" rel="stylesheet" type="text/css"> 
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

 <script>
	
	 function formatMoney(number, places, symbol, thousand, decimal) {
			number = number || 0;
			places = !isNaN(places = Math.abs(places)) ? places : 0;
			symbol = symbol !== undefined ? symbol : "";
			thousand = thousand || ",";
			decimal = decimal || ".";
			var negative = number < 0 ? "-" : "",
			    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
			    j = (j = i.length) > 3 ? j % 3 : 0;
			return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
	}
	
	
	 
	 function  fetch_purchase_bill_main(){
		 
		 var purchase_bill_id = sessionStorage.getItem("purchase_id_for_get_detail");
         
	      var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					
					var jsonObj = JSON.parse(xmlhttp.responseText);
					
					if(jsonObj.length == 0) {

						//alert("no  Data");
					}
					else{
					//	alert("get Data");
						 
					//	alert(xmlhttp.responseText);
						var inv_name = document.getElementById("inv_name");
						var inv_address = document.getElementById("inv_address");
						var inv_tax_id = document.getElementById("inv_tax_id"); 
						var create_date = document.getElementById("create_date"); 
						
						var invoice_date = document.getElementById("invoice_date"); 
						var po_no = document.getElementById("po_no"); 
					//	var order_view_id = document.getElementById("order_view_id"); 

						var purchase_id = document.getElementById('purchase_bill_id');
						
						var status_but = document.getElementById('status_but');
						var modal_status_but = document.getElementById('modal_status_but');
						var modal_note = document.getElementById('modal_note');
						var payment_ref = document.getElementById('payment_ref');
						
						var inv_no = document.getElementById("inv_no"); 
						var total_vat = document.getElementById('total_vat');
						var total_sum = document.getElementById('total_sum');
						var total_inc_vat = document.getElementById('total_inc_vat');


							
						if(jsonObj.purchaseStatus=="unpaid")
						{
							status_but.innerHTML = "UnPaid"+"<span class='caret'></span>";
							modal_status_but.innerHTML = "UnPaid"+"<span class='caret'></span>";
							modal_status_but.name = "unpaid";
							
						}else if(jsonObj.purchaseStatus=="paid"){
							status_but.innerHTML = "Paid"+"<span class='caret'></span>";
							modal_status_but.innerHTML = "Paid"+"<span class='caret'></span>";
							modal_status_but.name = "paid";
						
						}
							
		
		   			    	payment_ref.value = jsonObj.paymentRef;
						
						
							purchase_id.value = jsonObj.purchaseId;
							inv_no.value = jsonObj.invNo;
							inv_name.value = jsonObj.vendorName;
							inv_address.value = jsonObj.vendorAddress; 				
							inv_tax_id.value = jsonObj.vendorTaxID; 
							invoice_date.value = jsonObj.invoiceDate;
							
							modal_note.value =  jsonObj.note;
							//create_date.value = jsonObj.createDate;
							//completed_date.value = jsonObj.completedDate;
							//po_no.value = jsonObj.poNum;
			
							total_vat.value = jsonObj.totalVat;
							total_sum.value = jsonObj.totalValue;
							total_inc_vat.value = jsonObj.totalIncVat;
							

					}

				}// end if check state
			}// end function
			

			//xmlhttp.open("POST", "get_credit_inv_main_by_order_id_background.jsp?order_id="+cash_inv_id, true);
			xmlhttp.open("POST", "get_purchase_main_by_purchase_id_background.jsp?purchase_bill_id="+purchase_bill_id, true);
			xmlhttp.send();
	    	
		 
	 } 
function  fetch_purchase_bill_detail(){
		 
	var purchase_id = sessionStorage.getItem("purchase_id_for_get_detail");
         
	      var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					
					var jsonObj = JSON.parse(xmlhttp.responseText);
					
					if(jsonObj.length == 0) {

						//alert("no  Data");
					}
					else{
						//alert("get Order Detail Data");
							
							for(i in jsonObj) {

							
								  var newid = 0;
							        $.each($("#purchase-detail-dataTables-example tr"), function() {
							            if (parseInt($(this).data("id")) > newid) {
							                newid = parseInt($(this).data("id"));
							            }
							        });
							        newid++;
							        
							        var tr = $("<tr></tr>", {
							            id: "addr"+newid,
							            "data-id": newid
							        });
							        
							     
							        // loop through each td and create new elements with name of newid
							            $.each($("#purchase-detail-dataTables-example tbody tr:nth(0) td"), function() {
							                var cur_td = $(this);
							                
							                var children = cur_td.children();
							                
							                // add new td and element if it has a name
							                if ($(this).data("name") != undefined) {
							                    var td = $("<td></td>", {
							                        "data-name": $(cur_td).data("name")
							                    });
							                    
							                    var c = $(cur_td).find($(children[0]).prop('tagName')).clone().val("");
							                    c.attr("name", $(cur_td).data("name") + newid);			
							                    c.attr("id", $(cur_td).data("name") + newid);		
							                    c.appendTo($(td));
							                    td.appendTo($(tr));
							                  

							                } else {
							                    var td = $("<td></td>", {
							                        'text': $('#purchase-detail-dataTables-example tr').length
							                    }).appendTo($(tr));
							                }
							            });
							        	
							            
							            // add the new row
							            $(tr).appendTo($('#purchase-detail-dataTables-example'));
							            
							            var  pd_name = document.getElementById("name"+newid);
							     //      var  pd_cost_per_unit = document.getElementById("cost_per_unit"+newid);
							            var  pd_price = document.getElementById("price"+newid);
							      //      var  pd_profit_per_unit = document.getElementById("profit_per_unit"+newid);
							            var  pd_quantity = document.getElementById("quantity"+newid);
							            var  pd_unit = document.getElementById("unit"+newid);
							    //        var  pd_sum_cost = document.getElementById("sum_cost"+newid);
							            var  pd_sum = document.getElementById("sum"+newid);
							 //           var  pd_sum_profit = document.getElementById("sum_profit"+newid);
							            var  pd_product_id =  document.getElementById("product_id"+newid);
							            
							            
							   		//		alert(pd_name);
							           		 pd_name.value = jsonObj[i].productName;
							   //       		 pd_cost_per_unit.value = jsonObj[i].costPerUnit;
							           		 pd_price.value = jsonObj[i].price;
							  //        		 pd_profit_per_unit.value = jsonObj[i].profitPerUnit;
							           	  	 pd_quantity.value = jsonObj[i].quantity;
							           	     pd_unit.value = jsonObj[i].productUnit;
							   //       	     pd_sum_cost.value = jsonObj[i].sumCost;
							           	     pd_sum.value = jsonObj[i].sum;
							   //       	 	 pd_sum_profit.value =  jsonObj[i].sumProfit;
							           	 	 pd_product_id.value = (parseInt(i)+1)+"_"+jsonObj[i].productId;
							           	 	 pd_product_id.name = jsonObj[i].index;
							        	
							}
							/*
							$('#order-detail-dataTables-example').DataTable().row.add([
 		                            '<tr id="summation_row"><td data-name="name" style=" width : 200px;" ><input type="text" name = "pd_name" class="form-control"  readonly /></td>'
 		                            ,'<td data-name="cost_per_unit" style="text-align:center; width : 100px;" ><input type="text" name="pd_cost_per_unit" class="form-control text-right" readonly /></td>'
 		                            ,'<td data-name="price" style=" width : 100px;"><input type="text" name = "pd_price" class="form-control text-right"   readonly /></td>'
 		                            ,'<td data-name="profit_per_unit" style="text-align:center; width : 100px;"><input type="text" name = "pd_profit_per_unit" class="form-control"  readonly /></td>'
 		                            ,'<td data-name="quantity" style="text-align:center; width : 100px;" ><input type ="number" name = "pd_quantity" class="form-control text-right"  readonly /></td>'
 		                            ,'<td data-name="unit" style="text-align:center; width : 100px;" ><input type="text" name="pd_unit"  class="form-control text-right"  readonly /></td>'
 		                            ,'<td data-name="sum_cost" style="width : 100px;"><input type="text" id="total_sum_cost" class="form-control text-right" readonly /></td>'
 		                            ,'<td data-name="sum" style="width : 100px;"><input type="text"  id = "total_sum"  class="form-control text-right" readolny /></td>'
 		                            ,'<td data-name="sum_profit" style=" width : 100px;"><input type="text" id="total_sum_profit" class="form-control text-right" readonly /></td>'
 		                            ,'<td data-name="product_id" style=" width : 100px;"></td></tr>'	                               
 		                          ]).draw();
							*/
						//  	calculate_profit_percentage();
						  //	calculate_summation_table();

					}

				}// end if check state
			}// end function
			

			xmlhttp.open("POST", "get_purchase_detail_by_purchase_id_background.jsp?purchase_id="+purchase_id, true);
			xmlhttp.send();
	 
	 }
	 
	 
	
	 
	 function numberWithCommas(x) {
		 
		    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		    
		}
	 
	 
	 
	 
	function show_modal_change_status(temp){

			$('#status_detail_modal').modal('show');
		
	}
	
	function change_status(temp)
	{
	
		var modal_status_but = document.getElementById('modal_status_but');	
			modal_status_but.innerHTML = temp.innerHTML+"<span class='caret'></span>";
			modal_status_but.name = temp.id;
	}
	
	function submit_change_status(){
		
		var modal_note = document.getElementById('modal_note');
		//var completed_date = document.getElementById('completed_date');
		var payment_ref = document.getElementById('payment_ref');
		var status_but = document.getElementById('status_but');
		var purchase_id = document.getElementById('purchase_bill_id');		
		var	modal_status_but = document.getElementById('modal_status_but');	
		var parameter ; 

	//	alert("status:"+modal_status_but.name+" , "+"modal_note:"+modal_note.value+" , complete_date:"+complete_date.value+" , payment_ref:"+payment_ref.value);
			parameter = "purchase_id="+purchase_id.value+
					//	"&complete_date="+completed_date.value+
						"&status="+modal_status_but.name+
						"&note="+modal_note.value+
						"&payment_ref="+payment_ref.value;
	
		var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
					if(xmlhttp.responseText=="fail")
					{
						alert("Change Status error");
					}else{
						
						alert("Success");
						location.reload();
					}
					
				}// end if check state
			}// end function
			
		
			xmlhttp.open("POST", "change_purchase_status_detail_background.jsp?"+parameter, true);
			xmlhttp.send();

	}
	
</script>
<style>
	.text-right {
 		 text-align:right;
	}
	hr {
	  -moz-border-bottom-colors: none;
	  -moz-border-image: none;
	  -moz-border-left-colors: none;
	  -moz-border-right-colors: none;
	  -moz-border-top-colors: none;
	  border-color: #DCDCDC;
	  border-style: solid none;
	  border-width: 1px 0;
	  margin: 18px 0;
	}

</style>
    

</head>

<body>

    <div id="wrapper">

    		<nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>
                <div class="sidebar-header">
                    <h3>Bored Sidebar</h3>
                </div>
                
    
        		<div id="includedSidebar"></div>
        		
        	</nav>



        <!-- Navigation -->
       

        <div id="content" class="form-control">
        
        	   <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-justify"></i>
                                <span></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Page</a></li>
                       
                            </ul>
                        </div>
                    </div>
                </nav>
             <div class="row">
             <br>
	           <div class="col-lg-4">
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                          Purchase Invoice
	                        </div>
	                        
	                        <!-- /.panel-heading -->
	                        <div class="panel-body">
	                        	<div class="dropdown">
								  <button class="btn btn-primary dropdown-toggle btn-block" id="status_but" onclick="show_modal_change_status(this)" type="button" data-toggle="dropdown"><span class="caret"></span></button>
								</div>
								<br>
								   	<label>Invoice No.</label>
								<input name="inv_no" id="inv_no" type="text" class="form-control" placeholder="Inv No." readonly>
	                        	<br>
	                        	<input name="purchase_bill_id" id="purchase_bill_id" type="text" class="form-control" placeholder="Purchase Bill ID" readonly>
                                <br>	
	                        	<label>Customer Name (ชื่อลูกค้า)</label>
	                           		 <input id="inv_name" name = "inv_name" class="form-control" value="" disabled  >
	                           		 <input id ="company_id_hidden" name="company_id_hidden" type="hidden" value="" >
	                           		 <br>
	                        	<label>Address (ที่อยู่)</label>
	                        		 <textarea id="inv_address" name ="inv_address" class="form-control" rows="3" disabled></textarea>
	                        	 	 <br>
	                        	<label>Tax ID (เลขประจำตัวผู้เสียภาษี)</label>
	                        		<input class="form-control" id="inv_tax_id" name = "inv_tax_id" value="" disabled>
	                        		<br>
	                      
	                      			<table>
	                  
	                        	   			
	                        	   			<tr>
	                        	   				<td>
	                        	   					
	                        	   				</td>
	                        	   			</tr>
                   	   			
	                        
	                        	   			<tr class="pagination-centered">
	                        	   				<td style="padding-left:2em;">
	                        						<label>Invoive Date   </label>	                        						
	                        					</td>
	                        					<td style="padding-left:1em;">
	                        					 	<input type="date" id="invoice_date" name ="invoice_date" class="form-control"  readonly >  					                        					
	                        					</td>	
	                        					
	                        					
										
										
	                        	   			</tr>
	                        	   			
	                        	   			
	                        	   			
	                        
	                        		</table>



	                     
									<div>
	                        			<hr>
	                        			<button type="button" onclick="show_confirm_delete()"  class="form-control btn btn-danger">Delete Invoice</button>
	                        		</div>
	                        	
	                     		
													  
												
											
	                        		
	                        		
	                        		<br>
	                            	
												
													                            
	                        </div>
	                        <!-- .panel-body -->
	                    </div>
                    <!-- /.panel -->
                </div>
                
                
                 <div class="col-lg-8">
                  
                  			 <div class="panel-body">
                  			 
                  			 	 <div class="alert alert-success fade in alert-dismissable" id="alert_saved_time" style="display: none">
	                                <button id="alert_saved_time_but" type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                                Save Change on : 14.10.2016 ,8.37 AM
                            	</div>
                  			 		
                  			 </div>
                  			
                  
	           
	                        <table class="table table-striped table-bordered table-hover" id="purchase-detail-dataTables-example">
									<thead>
										<tr>
											<th class="text-center" style="width: 200px;">
												Product Name
											</th>

											<th class="text-center" style="width: 90px;">
												Price/Unit
											</th>


					    					<th class="text-center" style="width: 50px;">
												Quantity
											</th>
											<th class="text-center" style="width: 90px;">
												Unit
											</th>

											<th class="text-center" style="width: 100px;">
												Sum
											</th>

					        				<th class="text-center" style="border-top: 1px solid #ffffff; border-right: 1px solid #ffffff; width: 20px;">
											</th>
											
										</tr>
									</thead>
									<tbody id = "pd_tbody">
									
					    				<tr id='addr0' data-id="0" class="hidden">
											<td data-name="name" style=" width : 200px;">
											    <input type="text" name='pd_name' id = "pd_name" class="form-control" />
											</td>

											<td data-name="price" style=" width : 100px;">
											    <input type ="text" name = 'pd_price' id="pd_price" class="form-control text-right"   onchange="calculate_each_row(this.id)"  />
											</td>
		
									
					    					<td data-name="quantity" style="text-align:center; width : 100px;" >
											    <input type ="number" name = 'pd_quantity' id="pd_quantity" class="form-control text-right" onchange="calculate_each_row(this.id)"  />
											</td>
											<td data-name="unit" style="text-align:center; width : 50px;" >
												 <input  type="text" name='pd_unit' id = "pd_unit" class="form-control text-center" />
												 
											</td>

											<td data-name="sum" style="width : 100px;">
												 <input  type="text" name='pd_sum' id = "pd_sum" class="form-control text-right"  readonly />
												 
											</td>

											<td data-name="product_id" style=" width : 100px;">
											    <button onclick="calculate_cost(this.value)"  class="btn btn-warning btn-circle btn-md" >
											   		 <i class="glyphicon glyphicon-transfer"></i>
											    </button>
												<input type="hidden" name="index" id="pd_product_id">
												
											</td>
											 
										
					             
										</tr>
									</tbody>
								</table>	
								
								<table class="table table-bordered table-hover table-sortable " id="table_summary" style="border:none;">
								
								
					
														  <tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Total Sum  : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input readonly type="text" name="total_sum" id="total_sum" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>
			
															

														 <tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Vat (7%) : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input readonly type="text" name="total_vat" id="total_vat" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>
												
															<tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Total Inc Vat : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input readonly type="text" name="total_inc_vat" id="total_inc_vat" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>
															
									
											</table>
								
								<div class="col-lg-2">
									
								</div>
								<div class="col-lg-2">  
									
								</div>
								<div class="col-lg-2">
									
								</div>
								<div class="col-lg-2">
									
								</div>
								<div class="col-lg-2">
									
								</div>
								<div class="col-lg-2">
									<br>
									<button type="button" class="form-control btn btn-primary" onclick="save_cash_bill_updated()">Save</button>
								</div>
	                        
	                        
	                       
	                        <!-- .panel-body -->
	               
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
             </div>
      

        
        </div>
        <!-- /#page-wrapper -->

    </div>
     <div class="modal fade" id="status_detail_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style=" position: fixed; top: 10%; left: 10%; right: 10%; bottom: 15% ">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Change Status</h4>
                                        </div>
                                        <div class="modal-body">
                                       		<div class="row">
                                       				 <div class="panel-body">
                                       				 	    <div class="dropdown">
																  <button class="btn btn-primary dropdown-toggle btn-block" id="modal_status_but" name="" type="button" data-toggle="dropdown"><span class="caret"></span></button>
																   <ul id="modal_status_ul" class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1" style="left: 50% !important;
																																			    right: auto !important;
																																			    text-align: center !important;
																																			    transform: translate(-50%, 0) !important;">
																        <li><a href="#" onclick="change_status(this)" id="unpaid">Unpaid</a></li>
																        <li><a href="#" onclick="change_status(this)" id="paid">Paid</a></li>
																   
	
																        
																  </ul>
															</div>
															<br>
														   <div class="form-group">
																  <label for="comment">Note :</label>
																  <textarea class="form-control" rows="3" id="modal_note"></textarea>
													  	  </div>
													  	  <div>
													  		  <div>
																		  	
				                        					
				                        					      <label>Payment Reference :</label>	                        						
				                        					      <input type="text" id="payment_ref" name ="payment_ref" class="form-control" >  
				                        					      <br>						                        					
											  				  </div>
					                                       					
                                       					</div>
                                       		</div>
                                       		                       
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" name="submit_change_status_but"  id="submit_change_status_but" class="btn btn-primary" onclick="submit_change_status()">Submit Change</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
     </div>
        <!-- /#page-wrapper -->

    </div>
    
      <div class="modal fade" id="confirm-delete-order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			    <div class="modal-dialog">
			        <div class="modal-content">
			        	<form>
					          <div class="modal-body">
								    <textarea id="reason_delete" class="form-control" rows="2" placeholder="Reason for deleting this order"></textarea>
								    
								    <input type="hidden" id="delete_order_id">
							  </div>
							  <div class="modal-footer">
								    <button type="button" data-dismiss="modal" class="btn btn-danger" id="but_delete_order" onclick="DeleteOrder()">Delete</button>
								    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
							  </div>
						 </form>
			        </div>
			    </div>
		  </div>            
		  
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
     <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    
    
    <script>
    
	function show_confirm_delete(){
		$('#confirm-delete-order').modal('show');
		
		//  document.getElementById("delete_order_id").value = id;

	}
	/*
	function DeleteOrder()
	{
		
		var cash_bill_id = document.getElementById("cash_bill_id").value;
		var reason = document.getElementById("reason_delete").value;
		var inv_prefix = cash_bill_id.charAt(0)+cash_bill_id.charAt(1);
		//alert(inv_prefix);

			var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
						alert(xmlhttp.responseText);
						if(xmlhttp.responseText=="success")
						{
						window.open("cash_inv_sub.jsp");
							
						}else{
							
						}

				}// end if check state
			}// end function
			

			xmlhttp.open("POST", "delete_single_cash_bill_background.jsp?cash_bill_id="+cash_bill_id+"&reason="+reason, true);
			xmlhttp.send();
			
		
	}*/
	
    $(document).ready(function() {
    	
    	
    	$("#includedSidebar").load("sidebar.jsp"); 
    	
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').fadeOut();
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        

    	
    fetch_purchase_bill_main();
    fetch_purchase_bill_detail();
      
    	
    	
    });
    </script>

</body>

</html>
