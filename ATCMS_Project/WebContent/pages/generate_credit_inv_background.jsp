<%@page import="org.apache.poi.xwpf.usermodel.TextAlignment"%>
<%@page import="org.apache.poi.xwpf.usermodel.Borders"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "com.atcms.*" %>
<%@ page import = "java.util.List" %>
<%@ page import = "java.util.ArrayList"%>
<%@ page import = "java.util.Properties" %>
<%@ page import = "javax.servlet.ServletContext" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.util.Date" %>
<%@ page import="java.math.BigInteger" %>
<%@ page import="java.io.FileOutputStream" %>
<%@ page import="org.apache.poi.xssf.usermodel.*" %>
<%@ page import="org.apache.poi.hssf.usermodel.HSSFCellStyle" %>
<%@ page import="org.apache.poi.hssf.util.*" %>
<%@ page import="org.apache.poi.ss.util.CellUtil" %>
<%@ page import="org.apache.poi.ss.usermodel.*" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>
<%@ page import = "java.text.DecimalFormat" %>


<%!
		public static final short EXCEL_COLUMN_WIDTH_FACTOR = 256; 
	    public static final short EXCEL_ROW_HEIGHT_FACTOR = 20; 
	    public static final int UNIT_OFFSET_LENGTH = 7; 
	    public static final int[] UNIT_OFFSET_MAP = new int[] { 0, 36, 73, 109, 146, 182, 219 };
	    
	    public static RichTextString createRichTextString (XSSFWorkbook workbook_in,String text,String font_name ,int font_size)
	    {
	    	 RichTextString richString = new XSSFRichTextString(text);
	    	 XSSFFont font = workbook_in.createFont();
	    	 font.setFontName(font_name);
			 font.setFontHeightInPoints((short)font_size);
			 richString.applyFont(font);
			 return richString;
	    }
	    
	    
		public static XSSFCellStyle CreateStyle(XSSFWorkbook workbook_in
							 				   ,String font_name_in 
							                   , int font_size_in
							                   , boolean isBold)
		{
				XSSFFont font = workbook_in.createFont();
				font.setFontName(font_name_in);
				font.setFontHeightInPoints((short)font_size_in);
				font.setBold(isBold);
				XSSFCellStyle style = workbook_in.createCellStyle();
				style.setFont(font);
				return style ;
		}
				
		public static XSSFCellStyle CreateCellBorder(XSSFWorkbook workbook_in 
										, boolean top 
										, boolean bot 
										, boolean left 
										, boolean right)
		{
				
				XSSFCellStyle style = workbook_in.createCellStyle();
				if(top){  style.setBorderTop(HSSFCellStyle.BORDER_THIN); }
				if(bot){ style.setBorderBottom(HSSFCellStyle.BORDER_THIN); }
				if(left){ style.setBorderLeft(HSSFCellStyle.BORDER_THIN); }
				if(right){ style.setBorderRight(HSSFCellStyle.BORDER_THIN); }
				
				return style ;
		}
		public static void DrawListCellBorder (XSSFWorkbook workbook , XSSFRow row_in){
			
			XSSFCellStyle style_border_top_left = CreateCellBorder(workbook,true,false,true,false);
			XSSFCellStyle style_border_top_right = CreateCellBorder(workbook,true,false,false,true);
			XSSFCellStyle style_border_top_left_right = CreateCellBorder(workbook,true,false,true,true);
			XSSFCellStyle style_border_bot_left = CreateCellBorder(workbook,false,true,true,false);
			XSSFCellStyle style_border_bot_right = CreateCellBorder(workbook,false,true,false,true);
			XSSFCellStyle style_border_bot_left_right = CreateCellBorder(workbook,false,true,true,true);
			XSSFCellStyle style_border_top = CreateCellBorder(workbook,true,false,false,false);
			XSSFCellStyle style_border_bot = CreateCellBorder(workbook,false,true,false,false);
			XSSFCellStyle style_border_left = CreateCellBorder(workbook,false,false,true,false);
			XSSFCellStyle style_border_right = CreateCellBorder(workbook,false,false,false,true);
			XSSFCellStyle style_border_left_right = CreateCellBorder(workbook,false,false,true,true);
			XSSFCellStyle style_border_full = CreateCellBorder(workbook,true,true,true,true);
			
			row_in.createCell(0);
			row_in.createCell(1);
			row_in.createCell(11);
			row_in.createCell(12);
			row_in.getCell(0).setCellStyle(style_border_left_right);
			row_in.getCell(1).setCellStyle(style_border_left_right);
			row_in.getCell(11).setCellStyle(style_border_left_right);
			row_in.getCell(12).setCellStyle(style_border_left_right);

		}
	    public static int widthUnits2Pixel(short widthUnits) {
	    	
	        int pixels = (widthUnits / EXCEL_COLUMN_WIDTH_FACTOR) * UNIT_OFFSET_LENGTH; 
	        int offsetWidthUnits = widthUnits % EXCEL_COLUMN_WIDTH_FACTOR; 
	        pixels += Math.floor((float) offsetWidthUnits / ((float) EXCEL_COLUMN_WIDTH_FACTOR / UNIT_OFFSET_LENGTH));   
	        return pixels; 
	    }
	    public static short pixel2WidthUnits(int pxs) {
	        short widthUnits = (short) (EXCEL_COLUMN_WIDTH_FACTOR * (pxs / UNIT_OFFSET_LENGTH)); 
	        widthUnits += UNIT_OFFSET_MAP[(pxs % UNIT_OFFSET_LENGTH)];  
	        return widthUnits; 
	    } 
	    


%>

<% 

	System.out.println("Start generate_credit_inv_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
	
	/////////////////////////Start Get Data Process /////////////////////////////
	String order_id = request.getParameter("order_id");
	String type ="";
	String ponum ="";
	String customer_name="";
	String customer_address_full="";  
	String total_value="";
	String total_vat="";
	String total_inc_vat = "";
	String inv_no="";
	String due_date="";
	String delivery_date="";
	String credit="";
	String tax_id = "";
	String address_line0 = "";
	String address_line1 = "";
	String status = "";
	String inv_no_temp ="";
	
	
	List<OrderDetail> order_detail_list = new ArrayList<OrderDetail>();
	
	/////////////////////////////////////////////////////////////////////////////
	String order_main_query = " SELECT * "+
							  " FROM order_main "+
					          "	JOIN company "+
							  " ON BINARY order_main.customer_id = company.company_id "+
							  " WHERE BINARY order_main.order_id='"+order_id+"'";

	System.out.println("order_main_query:"+order_main_query);
	ResultSet rs_order_main = connect.createStatement().executeQuery(order_main_query);
	 while(rs_order_main.next())
     {
		 type = rs_order_main.getString("type");
		 ponum = rs_order_main.getString("ponum");
		 if(("-").equals(rs_order_main.getString("name_th")))
		 {
			 customer_name = rs_order_main.getString("name_en");
			 customer_address_full = rs_order_main.getString("address_en");
			 address_line0 = rs_order_main.getString("address_en_line1");
			 
			 if(("-").equals(rs_order_main.getString("address_en_line2")))
			 {
				 
			 }else{
				 address_line1 = rs_order_main.getString("address_en_line2"); 
			 }
				 
			
			 
		 }else{
			 customer_name = rs_order_main.getString("name_th");
			 customer_address_full = rs_order_main.getString("address_th");
			 address_line0 = rs_order_main.getString("address_th_line1");
			 if(("-").equals(rs_order_main.getString("address_th_line2")))
			 {
				 
			 }else{
				 address_line1 = rs_order_main.getString("address_th_line2"); 
			 }
				 
		 }
		 
		 total_value = rs_order_main.getString("total_value");
		 total_vat = rs_order_main.getString("total_vat");
		 total_inc_vat =  rs_order_main.getString("total_inc_vat");
		 inv_no = rs_order_main.getString("inv_no");
		 due_date = rs_order_main.getString("due_date");
		 delivery_date = rs_order_main.getString("delivery_date");
		 credit = rs_order_main.getString("compute_credit");
		 tax_id = rs_order_main.getString("tax_id");
		 status = rs_order_main.getString("status");
     }
		
	String [] due_date_parts = due_date.split("-");
	int due_date_year = Integer.parseInt(due_date_parts[0]);
		due_date_year = due_date_year + 543 - 2500 ;  
	due_date =  due_date_parts[2] + "/"+ due_date_parts[1]+"/"+due_date_year;
	
	
	
	String [] delivery_date_parts = delivery_date.split("-");
	int delivery_date_year = Integer.parseInt(delivery_date_parts[0]);
	delivery_date_year = delivery_date_year + 543 - 2500 ;  
	delivery_date =  delivery_date_parts[2] + "/"+ delivery_date_parts[1]+"/"+delivery_date_year;
	
	

	
	System.out.println("type:"+type);
	System.out.println("ponum:"+ponum);
	System.out.println("customer_name:"+customer_name);
	System.out.println("customer_address:"+customer_address_full);	
	System.out.println("total_value:"+total_value);
	System.out.println("total_vat:"+total_vat);
	System.out.println("total_inc_vat:"+total_inc_vat);
	System.out.println("inv_no:"+inv_no);
	System.out.println("due_date:"+due_date);
	System.out.println("delivery_date:"+delivery_date);
	
	////////////////////////////////////////////////////////////////////////////
	
	String order_detail_query = " select order_detail.index , product.name_th , product.unit_th , product.name_en , product.unit_en "+
			  				            " ,order_detail.price , order_detail.adt_description ,order_detail.quantity "+
										",order_detail.price , order_detail.sum "+
			   					" from order_detail "+
			   					" join product "+
							    " on order_detail.product_id = product.product_id "+
							    " WHERE BINARY order_detail.order_id='"+order_id+"'"+
							    " ORDER BY order_detail.index ";

	ResultSet rs_order_detail = connect.createStatement().executeQuery(order_detail_query);
	System.out.println("order_detail_query:"+order_detail_query);
	int i=0;
	while(rs_order_detail.next())
    {
			OrderDetail temp_order_detail = new OrderDetail();
			temp_order_detail.setAdtDescription(rs_order_detail.getString("adt_description"));
			if(("-").equals(rs_order_detail.getString("name_en")))
			{
				temp_order_detail.setProductName(rs_order_detail.getString("name_th"));
				
			}else{
				temp_order_detail.setProductName(rs_order_detail.getString("name_en"));
			}
			
			if(("-").equals(rs_order_detail.getString("unit_en")))
			{
				temp_order_detail.setProductUnit(rs_order_detail.getString("unit_th"));
			}else{
				temp_order_detail.setProductUnit(rs_order_detail.getString("unit_en"));
			}
			temp_order_detail.setPrice(rs_order_detail.getString("price"));
			temp_order_detail.setQuantity(rs_order_detail.getString("quantity"));
			temp_order_detail.setSum(rs_order_detail.getString("sum"));
			
			order_detail_list.add(temp_order_detail);
			
			System.out.println(i+":"+temp_order_detail.getProductName());
			i++;
    }
	System.out.println("order_detail_list.size:"+order_detail_list.size());
	
	//////////////////////////End Get Data Process /////////////////////////////
	
	
	/////////////////////////Start Writing Excel Process//////////////////////////

	try {		
		
			inv_no_temp = inv_no.replace("/","_");
		//	Date curr_date = new Date();
			String[] delivery_part = delivery_date.split("/");
			String year = (Integer.parseInt(delivery_part[2])+2500)+"";
			String month = delivery_part[1] ;
			    if(month.length()==1)
			    {
			    	month = "0" + month;
			    }
		
			File file_month_year = new File(application.getRealPath("/report/"+month+"_"+year));
			    if (!file_month_year.exists()) {
		            if (file_month_year.mkdir()) {
		                System.out.println("file_month_year Created!");
		            } else {
		                System.out.println("Failed to create file file_month_year");
		            }
		        }
			    File file_credit_report = new File(application.getRealPath("/report/"+month+"_"+year+"/credit_report/"));
				if (!file_credit_report.exists()) {
			            if (file_credit_report.mkdir()) {
			                System.out.println("file_credit_report created");
			            } else {
			                System.out.println("Failed to create file file_credit_report");
			            }
			    }
	
			
			   
			String fileName = application.getRealPath("/report/"+month+"_"+year+"/credit_report/"+inv_no_temp+".xlsx");
	
	
			XSSFWorkbook workbook = new XSSFWorkbook(); 

		    XSSFSheet page1 = workbook.createSheet("Page1");
		    XSSFSheet page2 = workbook.createSheet("Page2");
		    XSSFSheet page3 = workbook.createSheet("Page3");
		    XSSFSheet page4 = workbook.createSheet("Page4");
		    XSSFSheet page5 = workbook.createSheet("Page5");
		    				  
		    for(int w=0;w<5;w++)
		    {

		    	workbook.getSheetAt(w).setColumnWidth(0,pixel2WidthUnits((short)(43)));//A
		    	workbook.getSheetAt(w).setColumnWidth(1,pixel2WidthUnits((short)(47)));//B
		    	workbook.getSheetAt(w).setColumnWidth(2,pixel2WidthUnits((short)(36)));//C
		    	workbook.getSheetAt(w).setColumnWidth(3,pixel2WidthUnits((short)(36)));//D
		    	workbook.getSheetAt(w).setColumnWidth(4,pixel2WidthUnits((short)(40)));//E
		    	workbook.getSheetAt(w).setColumnWidth(5,pixel2WidthUnits((short)(34)));//F
		    	workbook.getSheetAt(w).setColumnWidth(6,pixel2WidthUnits((short)(31)));//G
		    	workbook.getSheetAt(w).setColumnWidth(7,pixel2WidthUnits((short)(32)));//H
		    	workbook.getSheetAt(w).setColumnWidth(8,pixel2WidthUnits((short)(44)));//I
		    	workbook.getSheetAt(w).setColumnWidth(9,pixel2WidthUnits((short)(31)));//J
		    	workbook.getSheetAt(w).setColumnWidth(10,pixel2WidthUnits((short)(38)));//K
		    	workbook.getSheetAt(w).setColumnWidth(11,pixel2WidthUnits((short)(82)));//L
		    	workbook.getSheetAt(w).setColumnWidth(12,pixel2WidthUnits((short)(101)));//M
		    }
		    
		    
		    for(int y=0;y<5;y++)
		    {
		    	workbook.getSheetAt(y).setMargin(Sheet.TopMargin, 0.472);
		    	workbook.getSheetAt(y).setMargin(Sheet.HeaderMargin, 0.315);
		    	workbook.getSheetAt(y).setMargin(Sheet.LeftMargin, 0.472);
		    	workbook.getSheetAt(y).setMargin(Sheet.RightMargin, 0.078);
		    	workbook.getSheetAt(y).setMargin(Sheet.BottomMargin, 0.748);
		    	workbook.getSheetAt(y).setMargin(Sheet.FooterMargin, 0.04);
		    }
			
			XSSFRow row_1 = page1.createRow((short)0);
			XSSFRow row_2 = page1.createRow((short)1);
			XSSFRow row_3 = page1.createRow((short)2);
			XSSFRow row_4 = page1.createRow((short)3);	
			XSSFRow row_5 = page1.createRow((short)4);
			XSSFRow row_6 = page1.createRow((short)5);		
			XSSFRow row_7 = page1.createRow((short)6);		
			XSSFRow row_8 = page1.createRow((short)7);	
			XSSFRow row_9 = page1.createRow((short)8);		
			XSSFRow row_10 = page1.createRow((short)9);	
			XSSFRow row_11 = page1.createRow((short)10);	
			XSSFRow row_12 = page1.createRow((short)11);	
			XSSFRow row_13 = page1.createRow((short)12);	
			XSSFRow row_14 = page1.createRow((short)13);
			XSSFRow row_15 = page1.createRow((short)14);	
			XSSFRow row_16 = page1.createRow((short)15);	
			XSSFRow row_17 = page1.createRow((short)16);
			XSSFRow row_18 = page1.createRow((short)17);
			XSSFRow row_19 = page1.createRow((short)18);
			XSSFRow row_20 = page1.createRow((short)19);
			XSSFRow row_21 = page1.createRow((short)20);
			XSSFRow row_22 = page1.createRow((short)21);
			XSSFRow row_23 = page1.createRow((short)22);
			XSSFRow row_24 = page1.createRow((short)23);
			XSSFRow row_25 = page1.createRow((short)24);
			XSSFRow row_26 = page1.createRow((short)25);
			XSSFRow row_27 = page1.createRow((short)26);
			XSSFRow row_28 = page1.createRow((short)27);
			XSSFRow row_29 = page1.createRow((short)28);
			XSSFRow row_30 = page1.createRow((short)29);
			XSSFRow row_31 = page1.createRow((short)30);
			XSSFRow row_32 = page1.createRow((short)31);
			XSSFRow row_33 = page1.createRow((short)32);
			XSSFRow row_34 = page1.createRow((short)33);
			XSSFRow row_35 = page1.createRow((short)34);
			XSSFRow row_36 = page1.createRow((short)35);
			XSSFRow row_37 = page1.createRow((short)36);
			XSSFRow row_38 = page1.createRow((short)37);
			XSSFRow row_39 = page1.createRow((short)38);
			XSSFRow row_40 = page1.createRow((short)39);
			XSSFRow row_41 = page1.createRow((short)40);
			XSSFRow row_42 = page1.createRow((short)41);
			XSSFRow row_43 = page1.createRow((short)42);
			XSSFRow row_44 = page1.createRow((short)43);
			XSSFRow row_45 = page1.createRow((short)44);
			XSSFRow row_46 = page1.createRow((short)45);
			XSSFRow row_47 = page1.createRow((short)46);
			XSSFRow row_48 = page1.createRow((short)47);
			XSSFRow row_49 = page1.createRow((short)48);
			XSSFRow row_50 = page1.createRow((short)49);
			XSSFRow row_51 = page1.createRow((short)50);
			XSSFRow row_52 = page1.createRow((short)51);
			XSSFRow row_53 = page1.createRow((short)52);
			XSSFRow row_54 = page1.createRow((short)53);
			

			XSSFRow row_p2_1 = page2.createRow((short)0);
			XSSFRow row_p2_2 = page2.createRow((short)1);
			XSSFRow row_p2_3 = page2.createRow((short)2);
			XSSFRow row_p2_4 = page2.createRow((short)3);	
			XSSFRow row_p2_5 = page2.createRow((short)4);
			XSSFRow row_p2_6 = page2.createRow((short)5);		
			XSSFRow row_p2_7 = page2.createRow((short)6);		
			XSSFRow row_p2_8 = page2.createRow((short)7);	
			XSSFRow row_p2_9 = page2.createRow((short)8);		
			XSSFRow row_p2_10 = page2.createRow((short)9);	
			XSSFRow row_p2_11 = page2.createRow((short)10);	
			XSSFRow row_p2_12 = page2.createRow((short)11);	
			XSSFRow row_p2_13 = page2.createRow((short)12);	
			XSSFRow row_p2_14 = page2.createRow((short)13);
			XSSFRow row_p2_15 = page2.createRow((short)14);	
			XSSFRow row_p2_16 = page2.createRow((short)15);	
			XSSFRow row_p2_17 = page2.createRow((short)16);
			XSSFRow row_p2_18 = page2.createRow((short)17);
			XSSFRow row_p2_19 = page2.createRow((short)18);
			XSSFRow row_p2_20 = page2.createRow((short)19);
			XSSFRow row_p2_21 = page2.createRow((short)20);
			XSSFRow row_p2_22 = page2.createRow((short)21);
			XSSFRow row_p2_23 = page2.createRow((short)22);
			XSSFRow row_p2_24 = page2.createRow((short)23);
			XSSFRow row_p2_25 = page2.createRow((short)24);
			XSSFRow row_p2_26 = page2.createRow((short)25);
			XSSFRow row_p2_27 = page2.createRow((short)26);
			XSSFRow row_p2_28 = page2.createRow((short)27);
			XSSFRow row_p2_29 = page2.createRow((short)28);
			XSSFRow row_p2_30 = page2.createRow((short)29);
			XSSFRow row_p2_31 = page2.createRow((short)30);
			XSSFRow row_p2_32 = page2.createRow((short)31);
			XSSFRow row_p2_33 = page2.createRow((short)32);
			XSSFRow row_p2_34 = page2.createRow((short)33);
			XSSFRow row_p2_35 = page2.createRow((short)34);
			XSSFRow row_p2_36 = page2.createRow((short)35);
			XSSFRow row_p2_37 = page2.createRow((short)36);
			XSSFRow row_p2_38 = page2.createRow((short)37);
			XSSFRow row_p2_39 = page2.createRow((short)38);
			XSSFRow row_p2_40 = page2.createRow((short)39);
			XSSFRow row_p2_41 = page2.createRow((short)40);
			XSSFRow row_p2_42 = page2.createRow((short)41);
			XSSFRow row_p2_43 = page2.createRow((short)42);
			XSSFRow row_p2_44 = page2.createRow((short)43);
			XSSFRow row_p2_45 = page2.createRow((short)44);
			XSSFRow row_p2_46 = page2.createRow((short)45);
			XSSFRow row_p2_47 = page2.createRow((short)46);
			XSSFRow row_p2_48 = page2.createRow((short)47);
			XSSFRow row_p2_49 = page2.createRow((short)48);
			XSSFRow row_p2_50 = page2.createRow((short)49);
			XSSFRow row_p2_51 = page2.createRow((short)50);
			XSSFRow row_p2_52 = page2.createRow((short)51);
			XSSFRow row_p2_53 = page2.createRow((short)52);
			XSSFRow row_p2_54 = page2.createRow((short)53);
			
			XSSFRow row_p3_1 = page3.createRow((short)0);
			XSSFRow row_p3_2 = page3.createRow((short)1);
			XSSFRow row_p3_3 = page3.createRow((short)2);
			XSSFRow row_p3_4 = page3.createRow((short)3);	
			XSSFRow row_p3_5 = page3.createRow((short)4);
			XSSFRow row_p3_6 = page3.createRow((short)5);		
			XSSFRow row_p3_7 = page3.createRow((short)6);		
			XSSFRow row_p3_8 = page3.createRow((short)7);	
			XSSFRow row_p3_9 = page3.createRow((short)8);		
			XSSFRow row_p3_10 = page3.createRow((short)9);	
			XSSFRow row_p3_11 = page3.createRow((short)10);	
			XSSFRow row_p3_12 = page3.createRow((short)11);	
			XSSFRow row_p3_13 = page3.createRow((short)12);	
			XSSFRow row_p3_14 = page3.createRow((short)13);
			XSSFRow row_p3_15 = page3.createRow((short)14);	
			XSSFRow row_p3_16 = page3.createRow((short)15);	
			XSSFRow row_p3_17 = page3.createRow((short)16);
			XSSFRow row_p3_18 = page3.createRow((short)17);
			XSSFRow row_p3_19 = page3.createRow((short)18);
			XSSFRow row_p3_20 = page3.createRow((short)19);
			XSSFRow row_p3_21 = page3.createRow((short)20);
			XSSFRow row_p3_22 = page3.createRow((short)21);
			XSSFRow row_p3_23 = page3.createRow((short)22);
			XSSFRow row_p3_24 = page3.createRow((short)23);
			XSSFRow row_p3_25 = page3.createRow((short)24);
			XSSFRow row_p3_26 = page3.createRow((short)25);
			XSSFRow row_p3_27 = page3.createRow((short)26);
			XSSFRow row_p3_28 = page3.createRow((short)27);
			XSSFRow row_p3_29 = page3.createRow((short)28);
			XSSFRow row_p3_30 = page3.createRow((short)29);
			XSSFRow row_p3_31 = page3.createRow((short)30);
			XSSFRow row_p3_32 = page3.createRow((short)31);
			XSSFRow row_p3_33 = page3.createRow((short)32);
			XSSFRow row_p3_34 = page3.createRow((short)33);
			XSSFRow row_p3_35 = page3.createRow((short)34);
			XSSFRow row_p3_36 = page3.createRow((short)35);
			XSSFRow row_p3_37 = page3.createRow((short)36);
			XSSFRow row_p3_38 = page3.createRow((short)37);
			XSSFRow row_p3_39 = page3.createRow((short)38);
			XSSFRow row_p3_40 = page3.createRow((short)39);
			XSSFRow row_p3_41 = page3.createRow((short)40);
			XSSFRow row_p3_42 = page3.createRow((short)41);
			XSSFRow row_p3_43 = page3.createRow((short)42);
			XSSFRow row_p3_44 = page3.createRow((short)43);
			XSSFRow row_p3_45 = page3.createRow((short)44);
			XSSFRow row_p3_46 = page3.createRow((short)45);
			XSSFRow row_p3_47 = page3.createRow((short)46);
			XSSFRow row_p3_48 = page3.createRow((short)47);
			XSSFRow row_p3_49 = page3.createRow((short)48);
			XSSFRow row_p3_50 = page3.createRow((short)49);
			XSSFRow row_p3_51 = page3.createRow((short)50);
			XSSFRow row_p3_52 = page3.createRow((short)51);
			XSSFRow row_p3_53 = page3.createRow((short)52);
			XSSFRow row_p3_54 = page3.createRow((short)53);
			
			XSSFRow row_p4_1 = page4.createRow((short)0);
			XSSFRow row_p4_2 = page4.createRow((short)1);
			XSSFRow row_p4_3 = page4.createRow((short)2);
			XSSFRow row_p4_4 = page4.createRow((short)3);	
			XSSFRow row_p4_5 = page4.createRow((short)4);
			XSSFRow row_p4_6 = page4.createRow((short)5);		
			XSSFRow row_p4_7 = page4.createRow((short)6);		
			XSSFRow row_p4_8 = page4.createRow((short)7);	
			XSSFRow row_p4_9 = page4.createRow((short)8);		
			XSSFRow row_p4_10 = page4.createRow((short)9);	
			XSSFRow row_p4_11 = page4.createRow((short)10);	
			XSSFRow row_p4_12 = page4.createRow((short)11);	
			XSSFRow row_p4_13 = page4.createRow((short)12);	
			XSSFRow row_p4_14 = page4.createRow((short)13);
			XSSFRow row_p4_15 = page4.createRow((short)14);	
			XSSFRow row_p4_16 = page4.createRow((short)15);	
			XSSFRow row_p4_17 = page4.createRow((short)16);
			XSSFRow row_p4_18 = page4.createRow((short)17);
			XSSFRow row_p4_19 = page4.createRow((short)18);
			XSSFRow row_p4_20 = page4.createRow((short)19);
			XSSFRow row_p4_21 = page4.createRow((short)20);
			XSSFRow row_p4_22 = page4.createRow((short)21);
			XSSFRow row_p4_23 = page4.createRow((short)22);
			XSSFRow row_p4_24 = page4.createRow((short)23);
			XSSFRow row_p4_25 = page4.createRow((short)24);
			XSSFRow row_p4_26 = page4.createRow((short)25);
			XSSFRow row_p4_27 = page4.createRow((short)26);
			XSSFRow row_p4_28 = page4.createRow((short)27);
			XSSFRow row_p4_29 = page4.createRow((short)28);
			XSSFRow row_p4_30 = page4.createRow((short)29);
			XSSFRow row_p4_31 = page4.createRow((short)30);
			XSSFRow row_p4_32 = page4.createRow((short)31);
			XSSFRow row_p4_33 = page4.createRow((short)32);
			XSSFRow row_p4_34 = page4.createRow((short)33);
			XSSFRow row_p4_35 = page4.createRow((short)34);
			XSSFRow row_p4_36 = page4.createRow((short)35);
			XSSFRow row_p4_37 = page4.createRow((short)36);
			XSSFRow row_p4_38 = page4.createRow((short)37);
			XSSFRow row_p4_39 = page4.createRow((short)38);
			XSSFRow row_p4_40 = page4.createRow((short)39);
			XSSFRow row_p4_41 = page4.createRow((short)40);
			XSSFRow row_p4_42 = page4.createRow((short)41);
			XSSFRow row_p4_43 = page4.createRow((short)42);
			XSSFRow row_p4_44 = page4.createRow((short)43);
			XSSFRow row_p4_45 = page4.createRow((short)44);
			XSSFRow row_p4_46 = page4.createRow((short)45);
			XSSFRow row_p4_47 = page4.createRow((short)46);
			XSSFRow row_p4_48 = page4.createRow((short)47);
			XSSFRow row_p4_49 = page4.createRow((short)48);
			XSSFRow row_p4_50 = page4.createRow((short)49);
			XSSFRow row_p4_51 = page4.createRow((short)50);
			XSSFRow row_p4_52 = page4.createRow((short)51);
			XSSFRow row_p4_53 = page4.createRow((short)52);
			XSSFRow row_p4_54 = page4.createRow((short)53);
		
			XSSFRow row_p5_1 = page5.createRow((short)0);
			XSSFRow row_p5_2 = page5.createRow((short)1);
			XSSFRow row_p5_3 = page5.createRow((short)2);
			XSSFRow row_p5_4 = page5.createRow((short)3);	
			XSSFRow row_p5_5 = page5.createRow((short)4);
			XSSFRow row_p5_6 = page5.createRow((short)5);		
			XSSFRow row_p5_7 = page5.createRow((short)6);		
			XSSFRow row_p5_8 = page5.createRow((short)7);	
			XSSFRow row_p5_9 = page5.createRow((short)8);		
			XSSFRow row_p5_10 = page5.createRow((short)9);	
			XSSFRow row_p5_11 = page5.createRow((short)10);	
			XSSFRow row_p5_12 = page5.createRow((short)11);	
			XSSFRow row_p5_13 = page5.createRow((short)12);	
			XSSFRow row_p5_14 = page5.createRow((short)13);
			XSSFRow row_p5_15 = page5.createRow((short)14);	
			XSSFRow row_p5_16 = page5.createRow((short)15);	
			XSSFRow row_p5_17 = page5.createRow((short)16);
			XSSFRow row_p5_18 = page5.createRow((short)17);
			XSSFRow row_p5_19 = page5.createRow((short)18);
			XSSFRow row_p5_20 = page5.createRow((short)19);
			XSSFRow row_p5_21 = page5.createRow((short)20);
			XSSFRow row_p5_22 = page5.createRow((short)21);
			XSSFRow row_p5_23 = page5.createRow((short)22);
			XSSFRow row_p5_24 = page5.createRow((short)23);
			XSSFRow row_p5_25 = page5.createRow((short)24);
			XSSFRow row_p5_26 = page5.createRow((short)25);
			XSSFRow row_p5_27 = page5.createRow((short)26);
			XSSFRow row_p5_28 = page5.createRow((short)27);
			XSSFRow row_p5_29 = page5.createRow((short)28);
			XSSFRow row_p5_30 = page5.createRow((short)29);
			XSSFRow row_p5_31 = page5.createRow((short)30);
			XSSFRow row_p5_32 = page5.createRow((short)31);
			XSSFRow row_p5_33 = page5.createRow((short)32);
			XSSFRow row_p5_34 = page5.createRow((short)33);
			XSSFRow row_p5_35 = page5.createRow((short)34);
			XSSFRow row_p5_36 = page5.createRow((short)35);
			XSSFRow row_p5_37 = page5.createRow((short)36);
			XSSFRow row_p5_38 = page5.createRow((short)37);
			XSSFRow row_p5_39 = page5.createRow((short)38);
			XSSFRow row_p5_40 = page5.createRow((short)39);
			XSSFRow row_p5_41 = page5.createRow((short)40);
			XSSFRow row_p5_42 = page5.createRow((short)41);
			XSSFRow row_p5_43 = page5.createRow((short)42);
			XSSFRow row_p5_44 = page5.createRow((short)43);
			XSSFRow row_p5_45 = page5.createRow((short)44);
			XSSFRow row_p5_46 = page5.createRow((short)45);
			XSSFRow row_p5_47 = page5.createRow((short)46);
			XSSFRow row_p5_48 = page5.createRow((short)47);
			XSSFRow row_p5_49 = page5.createRow((short)48);
			XSSFRow row_p5_50 = page5.createRow((short)49);
			XSSFRow row_p5_51 = page5.createRow((short)50);
			XSSFRow row_p5_52 = page5.createRow((short)51);
			XSSFRow row_p5_53 = page5.createRow((short)52);
			XSSFRow row_p5_54 = page5.createRow((short)53);
			
			row_1.setHeightInPoints(23.25f);
			row_2.setHeightInPoints(14.50f);
			row_3.setHeightInPoints(10.50f);
			row_4.setHeightInPoints(13.50f);
			row_5.setHeightInPoints(13.50f);
			row_6.setHeightInPoints(13.50f);
			row_7.setHeightInPoints(14.25f);
			row_8.setHeightInPoints(14.25f);
			row_9.setHeightInPoints(14.25f);
			row_10.setHeightInPoints(14.25f);
			row_11.setHeightInPoints(15.25f);
			row_12.setHeightInPoints(15.25f);
			row_13.setHeightInPoints(18f);
			row_14.setHeightInPoints(21f);
			row_15.setHeightInPoints(21f);
			row_16.setHeightInPoints(21f);
			row_17.setHeightInPoints(11.25f);
			row_18.setHeightInPoints(15.75f);
			row_19.setHeightInPoints(15.75f);
			row_20.setHeightInPoints(12.75f);
			row_21.setHeightInPoints(12.75f);
			row_22.setHeightInPoints(12.75f);
			row_23.setHeightInPoints(12.75f);
			row_24.setHeightInPoints(12.75f);
			row_25.setHeightInPoints(12.75f);
			row_26.setHeightInPoints(12.75f);
			row_27.setHeightInPoints(12.75f);
			row_28.setHeightInPoints(12.75f);
			row_29.setHeightInPoints(12.75f);
			row_30.setHeightInPoints(12.75f);
			row_31.setHeightInPoints(12.75f);
			row_32.setHeightInPoints(12.75f);
			row_33.setHeightInPoints(12.75f);
			row_34.setHeightInPoints(12.75f);
			row_35.setHeightInPoints(12.75f);
			row_36.setHeightInPoints(12.75f);
			row_37.setHeightInPoints(12.75f);
			row_38.setHeightInPoints(12.75f);
			row_39.setHeightInPoints(12.75f);
			row_40.setHeightInPoints(12.75f);
			row_41.setHeightInPoints(12.75f);
			row_42.setHeightInPoints(12.75f);
			row_43.setHeightInPoints(12.75f);
			row_44.setHeightInPoints(14.25f);
			row_45.setHeightInPoints(14.25f);
			row_46.setHeightInPoints(14.25f);
			row_47.setHeightInPoints(14.25f);
			row_48.setHeightInPoints(14.25f);
			row_49.setHeightInPoints(14.25f);
			row_50.setHeightInPoints(14.25f);
			row_51.setHeightInPoints(14.25f);
			row_52.setHeightInPoints(14.25f);
			row_53.setHeightInPoints(12.75f);
			row_54.setHeightInPoints(12.75f);
			
			row_p2_1.setHeightInPoints(23.25f);
			row_p2_2.setHeightInPoints(14.50f);
			row_p2_3.setHeightInPoints(10.50f);
			row_p2_4.setHeightInPoints(13.50f);
			row_p2_5.setHeightInPoints(13.50f);
			row_p2_6.setHeightInPoints(13.50f);
			row_p2_7.setHeightInPoints(14.25f);
			row_p2_8.setHeightInPoints(14.25f);
			row_p2_9.setHeightInPoints(14.25f);
			row_p2_10.setHeightInPoints(14.25f);
			row_p2_11.setHeightInPoints(15.25f);
			row_p2_12.setHeightInPoints(15.25f);
			row_p2_13.setHeightInPoints(18f);
			row_p2_14.setHeightInPoints(21f);
			row_p2_15.setHeightInPoints(21f);
			row_p2_16.setHeightInPoints(21f);
			row_p2_17.setHeightInPoints(11.25f);
			row_p2_18.setHeightInPoints(15.75f);
			row_p2_19.setHeightInPoints(15.75f);
			row_p2_20.setHeightInPoints(12.75f);
			row_p2_21.setHeightInPoints(12.75f);
			row_p2_22.setHeightInPoints(12.75f);
			row_p2_23.setHeightInPoints(12.75f);
			row_p2_24.setHeightInPoints(12.75f);
			row_p2_25.setHeightInPoints(12.75f);
			row_p2_26.setHeightInPoints(12.75f);
			row_p2_27.setHeightInPoints(12.75f);
			row_p2_28.setHeightInPoints(12.75f);
			row_p2_29.setHeightInPoints(12.75f);
			row_p2_30.setHeightInPoints(12.75f);
			row_p2_31.setHeightInPoints(12.75f);
			row_p2_32.setHeightInPoints(12.75f);
			row_p2_33.setHeightInPoints(12.75f);
			row_p2_34.setHeightInPoints(12.75f);
			row_p2_35.setHeightInPoints(12.75f);
			row_p2_36.setHeightInPoints(12.75f);
			row_p2_37.setHeightInPoints(12.75f);
			row_p2_38.setHeightInPoints(12.75f);
			row_p2_39.setHeightInPoints(12.75f);
			row_p2_40.setHeightInPoints(12.75f);
			row_p2_41.setHeightInPoints(12.75f);
			row_p2_42.setHeightInPoints(12.75f);
			row_p2_43.setHeightInPoints(12.75f);
			row_p2_44.setHeightInPoints(14.25f);
			row_p2_45.setHeightInPoints(14.25f);
			row_p2_46.setHeightInPoints(14.25f);
			row_p2_47.setHeightInPoints(14.25f);
			row_p2_48.setHeightInPoints(14.25f);
			row_p2_49.setHeightInPoints(14.25f);
			row_p2_50.setHeightInPoints(14.25f);
			row_p2_51.setHeightInPoints(14.25f);
			row_p2_52.setHeightInPoints(14.25f);
			row_p2_53.setHeightInPoints(12.75f);
			row_p2_54.setHeightInPoints(12.75f);
			
			row_p3_1.setHeightInPoints(23.25f);
			row_p3_2.setHeightInPoints(14.50f);
			row_p3_3.setHeightInPoints(10.50f);
			row_p3_4.setHeightInPoints(13.50f);
			row_p3_5.setHeightInPoints(13.50f);
			row_p3_6.setHeightInPoints(13.50f);
			row_p3_7.setHeightInPoints(14.25f);
			row_p3_8.setHeightInPoints(14.25f);
			row_p3_9.setHeightInPoints(14.25f);
			row_p3_10.setHeightInPoints(14.25f);
			row_p3_11.setHeightInPoints(15.25f);
			row_p3_12.setHeightInPoints(15.25f);
			row_p3_13.setHeightInPoints(18f);
			row_p3_14.setHeightInPoints(21f);
			row_p3_15.setHeightInPoints(21f);
			row_p3_16.setHeightInPoints(21f);
			row_p3_17.setHeightInPoints(11.25f);
			row_p3_18.setHeightInPoints(15.75f);
			row_p3_19.setHeightInPoints(15.75f);
			row_p3_20.setHeightInPoints(12.75f);
			row_p3_21.setHeightInPoints(12.75f);
			row_p3_22.setHeightInPoints(12.75f);
			row_p3_23.setHeightInPoints(12.75f);
			row_p3_24.setHeightInPoints(12.75f);
			row_p3_25.setHeightInPoints(12.75f);
			row_p3_26.setHeightInPoints(12.75f);
			row_p3_27.setHeightInPoints(12.75f);
			row_p3_28.setHeightInPoints(12.75f);
			row_p3_29.setHeightInPoints(12.75f);
			row_p3_30.setHeightInPoints(12.75f);
			row_p3_31.setHeightInPoints(12.75f);
			row_p3_32.setHeightInPoints(12.75f);
			row_p3_33.setHeightInPoints(12.75f);
			row_p3_34.setHeightInPoints(12.75f);
			row_p3_35.setHeightInPoints(12.75f);
			row_p3_36.setHeightInPoints(12.75f);
			row_p3_37.setHeightInPoints(12.75f);
			row_p3_38.setHeightInPoints(12.75f);
			row_p3_39.setHeightInPoints(12.75f);
			row_p3_40.setHeightInPoints(12.75f);
			row_p3_41.setHeightInPoints(12.75f);
			row_p3_42.setHeightInPoints(12.75f);
			row_p3_43.setHeightInPoints(12.75f);
			row_p3_44.setHeightInPoints(14.25f);
			row_p3_45.setHeightInPoints(14.25f);
			row_p3_46.setHeightInPoints(14.25f);
			row_p3_47.setHeightInPoints(14.25f);
			row_p3_48.setHeightInPoints(14.25f);
			row_p3_49.setHeightInPoints(14.25f);
			row_p3_50.setHeightInPoints(14.25f);
			row_p3_51.setHeightInPoints(14.25f);
			row_p3_52.setHeightInPoints(14.25f);
			row_p3_53.setHeightInPoints(12.75f);
			row_p3_54.setHeightInPoints(12.75f);
			
			row_p4_1.setHeightInPoints(23.25f);
			row_p4_2.setHeightInPoints(14.50f);
			row_p4_3.setHeightInPoints(10.50f);
			row_p4_4.setHeightInPoints(13.50f);
			row_p4_5.setHeightInPoints(13.50f);
			row_p4_6.setHeightInPoints(13.50f);
			row_p4_7.setHeightInPoints(14.25f);
			row_p4_8.setHeightInPoints(14.25f);
			row_p4_9.setHeightInPoints(14.25f);
			row_p4_10.setHeightInPoints(14.25f);
			row_p4_11.setHeightInPoints(15.25f);
			row_p4_12.setHeightInPoints(15.25f);
			row_p4_13.setHeightInPoints(18f);
			row_p4_14.setHeightInPoints(21f);
			row_p4_15.setHeightInPoints(21f);
			row_p4_16.setHeightInPoints(21f);
			row_p4_17.setHeightInPoints(11.25f);
			row_p4_18.setHeightInPoints(15.75f);
			row_p4_19.setHeightInPoints(15.75f);
			row_p4_20.setHeightInPoints(12.75f);
			row_p4_21.setHeightInPoints(12.75f);
			row_p4_22.setHeightInPoints(12.75f);
			row_p4_23.setHeightInPoints(12.75f);
			row_p4_24.setHeightInPoints(12.75f);
			row_p4_25.setHeightInPoints(12.75f);
			row_p4_26.setHeightInPoints(12.75f);
			row_p4_27.setHeightInPoints(12.75f);
			row_p4_28.setHeightInPoints(12.75f);
			row_p4_29.setHeightInPoints(12.75f);
			row_p4_30.setHeightInPoints(12.75f);
			row_p4_31.setHeightInPoints(12.75f);
			row_p4_32.setHeightInPoints(12.75f);
			row_p4_33.setHeightInPoints(12.75f);
			row_p4_34.setHeightInPoints(12.75f);
			row_p4_35.setHeightInPoints(12.75f);
			row_p4_36.setHeightInPoints(12.75f);
			row_p4_37.setHeightInPoints(12.75f);
			row_p4_38.setHeightInPoints(12.75f);
			row_p4_39.setHeightInPoints(12.75f);
			row_p4_40.setHeightInPoints(12.75f);
			row_p4_41.setHeightInPoints(12.75f);
			row_p4_42.setHeightInPoints(12.75f);
			row_p4_43.setHeightInPoints(12.75f);
			row_p4_44.setHeightInPoints(14.25f);
			row_p4_45.setHeightInPoints(14.25f);
			row_p4_46.setHeightInPoints(14.25f);
			row_p4_47.setHeightInPoints(14.25f);
			row_p4_48.setHeightInPoints(14.25f);
			row_p4_49.setHeightInPoints(14.25f);
			row_p4_50.setHeightInPoints(14.25f);
			row_p4_51.setHeightInPoints(14.25f);
			row_p4_52.setHeightInPoints(14.25f);
			row_p4_53.setHeightInPoints(12.75f);
			row_p4_54.setHeightInPoints(12.75f);
			
			
			row_p5_1.setHeightInPoints(23.25f);
			row_p5_2.setHeightInPoints(14.50f);
			row_p5_3.setHeightInPoints(10.50f);
			row_p5_4.setHeightInPoints(13.50f);
			row_p5_5.setHeightInPoints(13.50f);
			row_p5_6.setHeightInPoints(13.50f);
			row_p5_7.setHeightInPoints(14.25f);
			row_p5_8.setHeightInPoints(14.25f);
			row_p5_9.setHeightInPoints(14.25f);
			row_p5_10.setHeightInPoints(14.25f);
			row_p5_11.setHeightInPoints(15.25f);
			row_p5_12.setHeightInPoints(15.25f);
			row_p5_13.setHeightInPoints(18f);
			row_p5_14.setHeightInPoints(21f);
			row_p5_15.setHeightInPoints(21f);
			row_p5_16.setHeightInPoints(21f);
			row_p5_17.setHeightInPoints(11.25f);
			row_p5_18.setHeightInPoints(15.75f);
			row_p5_19.setHeightInPoints(15.75f);
			row_p5_20.setHeightInPoints(12.75f);
			row_p5_21.setHeightInPoints(12.75f);
			row_p5_22.setHeightInPoints(12.75f);
			row_p5_23.setHeightInPoints(12.75f);
			row_p5_24.setHeightInPoints(12.75f);
			row_p5_25.setHeightInPoints(12.75f);
			row_p5_26.setHeightInPoints(12.75f);
			row_p5_27.setHeightInPoints(12.75f);
			row_p5_28.setHeightInPoints(12.75f);
			row_p5_29.setHeightInPoints(12.75f);
			row_p5_30.setHeightInPoints(12.75f);
			row_p5_31.setHeightInPoints(12.75f);
			row_p5_32.setHeightInPoints(12.75f);
			row_p5_33.setHeightInPoints(12.75f);
			row_p5_34.setHeightInPoints(12.75f);
			row_p5_35.setHeightInPoints(12.75f);
			row_p5_36.setHeightInPoints(12.75f);
			row_p5_37.setHeightInPoints(12.75f);
			row_p5_38.setHeightInPoints(12.75f);
			row_p5_39.setHeightInPoints(12.75f);
			row_p5_40.setHeightInPoints(12.75f);
			row_p5_41.setHeightInPoints(12.75f);
			row_p5_42.setHeightInPoints(12.75f);
			row_p5_43.setHeightInPoints(12.75f);
			row_p5_44.setHeightInPoints(14.25f);
			row_p5_45.setHeightInPoints(14.25f);
			row_p5_46.setHeightInPoints(14.25f);
			row_p5_47.setHeightInPoints(14.25f);
			row_p5_48.setHeightInPoints(14.25f);
			row_p5_49.setHeightInPoints(14.25f);
			row_p5_50.setHeightInPoints(14.25f);
			row_p5_51.setHeightInPoints(14.25f);
			row_p5_52.setHeightInPoints(14.25f);
			row_p5_53.setHeightInPoints(12.75f);
			row_p5_54.setHeightInPoints(12.75f);
			
			XSSFCellStyle style_arial_14 = CreateStyle(workbook,"Arial",14,false);
			XSSFCellStyle style_arial_13 = CreateStyle(workbook,"Arial",13,false);
			XSSFCellStyle style_arial_11 = CreateStyle(workbook,"Arial",11,false);
			XSSFCellStyle style_arial_11_bold = CreateStyle(workbook,"Arial",11,true);
			XSSFCellStyle style_arial_10 = CreateStyle(workbook,"Arial",10,false);
			XSSFCellStyle style_arial_9 = CreateStyle(workbook,"Arial",9,false);	
			XSSFCellStyle style_arial_8 = CreateStyle(workbook,"Arial",8,false);
			
	
			XSSFCellStyle style_border_top_left = CreateCellBorder(workbook,true,false,true,false);
			XSSFCellStyle style_border_top_right = CreateCellBorder(workbook,true,false,false,true);
			XSSFCellStyle style_border_top_left_right = CreateCellBorder(workbook,true,false,true,true);
			XSSFCellStyle style_border_bot_left = CreateCellBorder(workbook,false,true,true,false);
			XSSFCellStyle style_border_bot_right = CreateCellBorder(workbook,false,true,false,true);
			XSSFCellStyle style_border_bot_left_right = CreateCellBorder(workbook,false,true,true,true);
			XSSFCellStyle style_border_top = CreateCellBorder(workbook,true,false,false,false);
			XSSFCellStyle style_border_bot = CreateCellBorder(workbook,false,true,false,false);
			XSSFCellStyle style_border_left = CreateCellBorder(workbook,false,false,true,false);
			XSSFCellStyle style_border_right = CreateCellBorder(workbook,false,false,false,true);
			XSSFCellStyle style_border_left_right = CreateCellBorder(workbook,false,false,true,true);
			XSSFCellStyle style_border_full = CreateCellBorder(workbook,true,true,true,true);
			
			
			XSSFCellStyle com_tl_border_arial_10 =  workbook.createCellStyle();
			  com_tl_border_arial_10.cloneStyleFrom(style_arial_10);
			  com_tl_border_arial_10.setDataFormat(style_border_top_left.getDataFormat());
			
			page1.addMergedRegion(new CellRangeAddress(17,17,0,1));
			page1.addMergedRegion(new CellRangeAddress(17,17,2,10));
			page1.addMergedRegion(new CellRangeAddress(18,18,0,1));
			page1.addMergedRegion(new CellRangeAddress(18,18,2,10));
			page1.addMergedRegion(new CellRangeAddress(43,43,0,9));
			page1.addMergedRegion(new CellRangeAddress(43,43,10,11));
			page1.addMergedRegion(new CellRangeAddress(44,44,0,9));
			page1.addMergedRegion(new CellRangeAddress(44,44,10,11));
			page1.addMergedRegion(new CellRangeAddress(45,45,0,9));
			page1.addMergedRegion(new CellRangeAddress(45,45,10,11));
			page1.addMergedRegion(new CellRangeAddress(46,46,11,12));
			page1.addMergedRegion(new CellRangeAddress(47,47,11,12));
			page1.addMergedRegion(new CellRangeAddress(48,48,11,12));
			page1.addMergedRegion(new CellRangeAddress(49,49,11,12));
			page1.addMergedRegion(new CellRangeAddress(50,50,11,12));
			page1.addMergedRegion(new CellRangeAddress(51,51,11,12));
			
			page2.addMergedRegion(new CellRangeAddress(17,17,0,1));
			page2.addMergedRegion(new CellRangeAddress(17,17,2,10));
			page2.addMergedRegion(new CellRangeAddress(18,18,0,1));
			page2.addMergedRegion(new CellRangeAddress(18,18,2,10));
			page2.addMergedRegion(new CellRangeAddress(43,43,0,9));
			page2.addMergedRegion(new CellRangeAddress(43,43,10,11));
			page2.addMergedRegion(new CellRangeAddress(44,44,0,9));
			page2.addMergedRegion(new CellRangeAddress(44,44,10,11));
			page2.addMergedRegion(new CellRangeAddress(45,45,0,9));
			page2.addMergedRegion(new CellRangeAddress(45,45,10,11));
			page2.addMergedRegion(new CellRangeAddress(46,46,11,12));
			page2.addMergedRegion(new CellRangeAddress(47,47,11,12));
			page2.addMergedRegion(new CellRangeAddress(48,48,11,12));
			page2.addMergedRegion(new CellRangeAddress(49,49,11,12));
			page2.addMergedRegion(new CellRangeAddress(50,50,11,12));
			page2.addMergedRegion(new CellRangeAddress(51,51,11,12));
			
			page3.addMergedRegion(new CellRangeAddress(17,17,0,1));
			page3.addMergedRegion(new CellRangeAddress(17,17,2,10));
			page3.addMergedRegion(new CellRangeAddress(18,18,0,1));
			page3.addMergedRegion(new CellRangeAddress(18,18,2,10));
			page3.addMergedRegion(new CellRangeAddress(43,43,0,9));
			page3.addMergedRegion(new CellRangeAddress(43,43,10,11));
			page3.addMergedRegion(new CellRangeAddress(44,44,0,9));
			page3.addMergedRegion(new CellRangeAddress(44,44,10,11));
			page3.addMergedRegion(new CellRangeAddress(45,45,0,9));
			page3.addMergedRegion(new CellRangeAddress(45,45,10,11));
			page3.addMergedRegion(new CellRangeAddress(46,46,11,12));
			page3.addMergedRegion(new CellRangeAddress(47,47,11,12));
			page3.addMergedRegion(new CellRangeAddress(48,48,11,12));
			page3.addMergedRegion(new CellRangeAddress(49,49,11,12));
			page3.addMergedRegion(new CellRangeAddress(50,50,11,12));
			page3.addMergedRegion(new CellRangeAddress(51,51,11,12));
			
			page4.addMergedRegion(new CellRangeAddress(17,17,0,1));
			page4.addMergedRegion(new CellRangeAddress(17,17,2,10));
			page4.addMergedRegion(new CellRangeAddress(18,18,0,1));
			page4.addMergedRegion(new CellRangeAddress(18,18,2,10));
			page4.addMergedRegion(new CellRangeAddress(43,43,0,9));
			page4.addMergedRegion(new CellRangeAddress(43,43,10,11));
			page4.addMergedRegion(new CellRangeAddress(44,44,0,9));
			page4.addMergedRegion(new CellRangeAddress(44,44,10,11));
			page4.addMergedRegion(new CellRangeAddress(45,45,0,9));
			page4.addMergedRegion(new CellRangeAddress(45,45,10,11));
			page4.addMergedRegion(new CellRangeAddress(46,46,11,12));
			page4.addMergedRegion(new CellRangeAddress(47,47,11,12));
			page4.addMergedRegion(new CellRangeAddress(48,48,11,12));
			page4.addMergedRegion(new CellRangeAddress(49,49,11,12));
			page4.addMergedRegion(new CellRangeAddress(50,50,11,12));
			page4.addMergedRegion(new CellRangeAddress(51,51,11,12));
	
			page5.addMergedRegion(new CellRangeAddress(17,17,0,1));
			page5.addMergedRegion(new CellRangeAddress(17,17,2,10));
			page5.addMergedRegion(new CellRangeAddress(18,18,0,1));
			page5.addMergedRegion(new CellRangeAddress(18,18,2,10));
			page5.addMergedRegion(new CellRangeAddress(43,43,0,9));
			page5.addMergedRegion(new CellRangeAddress(43,43,10,11));
			page5.addMergedRegion(new CellRangeAddress(44,44,0,9));
			page5.addMergedRegion(new CellRangeAddress(44,44,10,11));
			page5.addMergedRegion(new CellRangeAddress(45,45,0,9));
			page5.addMergedRegion(new CellRangeAddress(45,45,10,11));
			page5.addMergedRegion(new CellRangeAddress(46,46,11,12));
			page5.addMergedRegion(new CellRangeAddress(47,47,11,12));
			page5.addMergedRegion(new CellRangeAddress(48,48,11,12));
			page5.addMergedRegion(new CellRangeAddress(49,49,11,12));
			page5.addMergedRegion(new CellRangeAddress(50,50,11,12));
			page5.addMergedRegion(new CellRangeAddress(51,51,11,12));
		
			
			row_1.createCell(0).setCellValue("บริษัท อาทิตย์ เคมีภัณฑ์ จำกัด (สำนักงานใหญ่)");	      
			row_1.getCell(0).setCellStyle(style_arial_14);
			row_1.createCell(9).setCellValue(" ( ต้นฉบับ ) ใบกำกับภาษี  / ใบส่งสินค้า");	      
			row_1.getCell(9).setCellStyle(style_arial_11_bold);
			
			row_p2_1.createCell(0).setCellValue("บริษัท อาทิตย์ เคมีภัณฑ์ จำกัด (สำนักงานใหญ่)");	      
			row_p2_1.getCell(0).setCellStyle(style_arial_14);
			row_p2_1.createCell(9).setCellValue(" ( สำเนา ) ใบกำกับภาษี ) / ใบส่งสินค้า / ใบวางบิล");	      
			row_p2_1.getCell(9).setCellStyle(style_arial_9);
			
			row_p3_1.createCell(0).setCellValue("บริษัท อาทิตย์ เคมีภัณฑ์ จำกัด (สำนักงานใหญ่)");	      
			row_p3_1.getCell(0).setCellStyle(style_arial_14);
			row_p3_1.createCell(9).setCellValue(" ( สำเนา ) ใบกำกับภาษี ) / ใบส่งสินค้า / ใบวางบิล");	      
			row_p3_1.getCell(9).setCellStyle(style_arial_9);
			
			row_p4_1.createCell(0).setCellValue("บริษัท อาทิตย์ เคมีภัณฑ์ จำกัด (สำนักงานใหญ่)");	      
			row_p4_1.getCell(0).setCellStyle(style_arial_14);
			row_p4_1.createCell(9).setCellValue(" ( ต้นฉบับ ) ใบเสร็จรับเงิน");	      
			row_p4_1.getCell(9).setCellStyle(style_arial_11_bold);
			
			row_p5_1.createCell(0).setCellValue("บริษัท อาทิตย์ เคมีภัณฑ์ จำกัด (สำนักงานใหญ่)");	      
			row_p5_1.getCell(0).setCellStyle(style_arial_14);
			row_p5_1.createCell(9).setCellValue(" ( สำเนา ) ใบเสร็จรับเงิน");	      
			row_p5_1.getCell(9).setCellStyle(style_arial_11);

			row_2.createCell(0).setCellValue("ARTHITAYA CHEMICAL CO., LTD.");	      
			row_2.getCell(0).setCellStyle(style_arial_13);	 
			row_2.createCell(9).setCellValue("(เอกสารออกเป็นชุด)");	      
			row_2.getCell(9).setCellStyle(style_arial_10);	
			
			row_p2_2.createCell(0).setCellValue("ARTHITAYA CHEMICAL CO., LTD.");	      
			row_p2_2.getCell(0).setCellStyle(style_arial_13);	 
			row_p2_2.createCell(9).setCellValue("(เอกสารออกเป็นชุด)");	      
			row_p2_2.getCell(9).setCellStyle(style_arial_10);	
			
			row_p3_2.createCell(0).setCellValue("ARTHITAYA CHEMICAL CO., LTD.");	      
			row_p3_2.getCell(0).setCellStyle(style_arial_13);	 
			row_p3_2.createCell(9).setCellValue("(เอกสารออกเป็นชุด)");	      
			row_p3_2.getCell(9).setCellStyle(style_arial_10);	
			
			row_p4_2.createCell(0).setCellValue("ARTHITAYA CHEMICAL CO., LTD.");	      
			row_p4_2.getCell(0).setCellStyle(style_arial_13);	 
			row_p4_2.createCell(9).setCellValue("(เอกสารออกเป็นชุด)");	      
			row_p4_2.getCell(9).setCellStyle(style_arial_10);	
			
			row_p5_2.createCell(0).setCellValue("ARTHITAYA CHEMICAL CO., LTD.");	      
			row_p5_2.getCell(0).setCellStyle(style_arial_13);	 
			row_p5_2.createCell(9).setCellValue("(เอกสารออกเป็นชุด)");	      
			row_p5_2.getCell(9).setCellStyle(style_arial_10);	
			
			row_4.createCell(0).setCellValue("123/3-4-5 หมู่ 5 ถ.สุขุมวิท ต.บ้านฉาง อ.บ้านฉาง จ.ระยอง 21130 ");	      
			row_4.getCell(0).setCellStyle(style_arial_10);	    
			row_4.createCell(11).setCellValue(createRichTextString(workbook,"บัญชีลูกค้าเลขที่","Arial",10));
			row_4.getCell(11).setCellStyle(style_border_top_left);
			row_4.createCell(12);
			row_4.getCell(12).setCellStyle(style_border_top_right);
			

			row_p2_4.createCell(0).setCellValue("123/3-4-5 หมู่ 5 ถ.สุขุมวิท ต.บ้านฉาง อ.บ้านฉาง จ.ระยอง 21130 ");	      
			row_p2_4.getCell(0).setCellStyle(style_arial_10);	    
			row_p2_4.createCell(11).setCellValue(createRichTextString(workbook,"บัญชีลูกค้าเลขที่","Arial",10));
			row_p2_4.getCell(11).setCellStyle(style_border_top_left);
			row_p2_4.createCell(12);
			row_p2_4.getCell(12).setCellStyle(style_border_top_right);
			
			row_p3_4.createCell(0).setCellValue("123/3-4-5 หมู่ 5 ถ.สุขุมวิท ต.บ้านฉาง อ.บ้านฉาง จ.ระยอง 21130 ");	      
			row_p3_4.getCell(0).setCellStyle(style_arial_10);	    
			row_p3_4.createCell(11).setCellValue(createRichTextString(workbook,"บัญชีลูกค้าเลขที่","Arial",10));
			row_p3_4.getCell(11).setCellStyle(style_border_top_left);
			row_p3_4.createCell(12);
			row_p3_4.getCell(12).setCellStyle(style_border_top_right);
			
			row_p4_4.createCell(0).setCellValue("123/3-4-5 หมู่ 5 ถ.สุขุมวิท ต.บ้านฉาง อ.บ้านฉาง จ.ระยอง 21130 ");	      
			row_p4_4.getCell(0).setCellStyle(style_arial_10);	    
			row_p4_4.createCell(11).setCellValue(createRichTextString(workbook,"บัญชีลูกค้าเลขที่","Arial",10));
			row_p4_4.getCell(11).setCellStyle(style_border_top_left);
			row_p4_4.createCell(12);
			row_p4_4.getCell(12).setCellStyle(style_border_top_right);
			
			row_p5_4.createCell(0).setCellValue("123/3-4-5 หมู่ 5 ถ.สุขุมวิท ต.บ้านฉาง อ.บ้านฉาง จ.ระยอง 21130 ");	      
			row_p5_4.getCell(0).setCellStyle(style_arial_10);	    
			row_p5_4.createCell(11).setCellValue(createRichTextString(workbook,"บัญชีลูกค้าเลขที่","Arial",10));
			row_p5_4.getCell(11).setCellStyle(style_border_top_left);
			row_p5_4.createCell(12);
			row_p5_4.getCell(12).setCellStyle(style_border_top_right);
			
		
			row_5.createCell(0).setCellValue("โทร (038) 602 068 , 082-716 0166 , 081-647 3802");	      
			row_5.getCell(0).setCellStyle(style_arial_8);	 
			row_5.createCell(11).setCellValue(createRichTextString(workbook,"ใบสั่งซื้อ","Arial",10));
			row_5.getCell(11).setCellStyle(style_border_top_left);
			row_5.createCell(12).setCellValue(createRichTextString(workbook,ponum,"Arial",10));
			row_5.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_5.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_5.createCell(0).setCellValue("โทร (038) 602 068 , 082-716 0166 , 081-647 3802");	      
			row_p2_5.getCell(0).setCellStyle(style_arial_8);	 
			row_p2_5.createCell(11).setCellValue(createRichTextString(workbook,"ใบสั่งซื้อ","Arial",10));
			row_p2_5.getCell(11).setCellStyle(style_border_top_left);
			row_p2_5.createCell(12).setCellValue(createRichTextString(workbook,ponum,"Arial",10));
			row_p2_5.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_p2_5.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p3_5.createCell(0).setCellValue("โทร (038) 602 068 , 082-716 0166 , 081-647 3802");	      
			row_p3_5.getCell(0).setCellStyle(style_arial_8);	 
			row_p3_5.createCell(11).setCellValue(createRichTextString(workbook,"ใบสั่งซื้อ","Arial",10));
			row_p3_5.getCell(11).setCellStyle(style_border_top_left);
			row_p3_5.createCell(12).setCellValue(createRichTextString(workbook,ponum,"Arial",10));
			row_p3_5.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_p3_5.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p4_5.createCell(0).setCellValue("โทร (038) 602 068 , 082-716 0166 , 081-647 3802");	      
			row_p4_5.getCell(0).setCellStyle(style_arial_8);	 
			row_p4_5.createCell(11).setCellValue(createRichTextString(workbook,"ใบสั่งซื้อ","Arial",10));
			row_p4_5.getCell(11).setCellStyle(style_border_top_left);
			row_p4_5.createCell(12).setCellValue(createRichTextString(workbook,ponum,"Arial",10));
			row_p4_5.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_p4_5.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p5_5.createCell(0).setCellValue("โทร (038) 602 068 , 082-716 0166 , 081-647 3802");	      
			row_p5_5.getCell(0).setCellStyle(style_arial_8);	 
			row_p5_5.createCell(11).setCellValue(createRichTextString(workbook,"ใบสั่งซื้อ","Arial",10));
			row_p5_5.getCell(11).setCellStyle(style_border_top_left);
			row_p5_5.createCell(12).setCellValue(createRichTextString(workbook,ponum,"Arial",10));
			row_p5_5.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_p5_5.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_6.createCell(0).setCellValue("E-Mail address : arthitsho@gmail.com");	      
			row_6.getCell(0).setCellStyle(style_arial_10);	  
			row_6.createCell(11).setCellValue(createRichTextString(workbook,"P/O No.","Arial",10));
			row_6.getCell(11).setCellStyle(style_border_left);
			row_6.createCell(12).setCellValue("");
			row_6.getCell(12).setCellStyle(style_border_right);
			
			row_p2_6.createCell(0).setCellValue("E-Mail address : arthitsho@gmail.com");	      
			row_p2_6.getCell(0).setCellStyle(style_arial_10);	  
			row_p2_6.createCell(11).setCellValue(createRichTextString(workbook,"P/O No.","Arial",10));
			row_p2_6.getCell(11).setCellStyle(style_border_left);
			row_p2_6.createCell(12).setCellValue("");
			row_p2_6.getCell(12).setCellStyle(style_border_right);
			
			row_p3_6.createCell(0).setCellValue("E-Mail address : arthitsho@gmail.com");	      
			row_p3_6.getCell(0).setCellStyle(style_arial_10);	  
			row_p3_6.createCell(11).setCellValue(createRichTextString(workbook,"P/O No.","Arial",10));
			row_p3_6.getCell(11).setCellStyle(style_border_left);
			row_p3_6.createCell(12).setCellValue("");
			row_p3_6.getCell(12).setCellStyle(style_border_right);
			
			row_p4_6.createCell(0).setCellValue("E-Mail address : arthitsho@gmail.com");	      
			row_p4_6.getCell(0).setCellStyle(style_arial_10);	  
			row_p4_6.createCell(11).setCellValue(createRichTextString(workbook,"P/O No.","Arial",10));
			row_p4_6.getCell(11).setCellStyle(style_border_left);
			row_p4_6.createCell(12).setCellValue("");
			row_p4_6.getCell(12).setCellStyle(style_border_right);
			
			row_p5_6.createCell(0).setCellValue("E-Mail address : arthitsho@gmail.com");	      
			row_p5_6.getCell(0).setCellStyle(style_arial_10);	  
			row_p5_6.createCell(11).setCellValue(createRichTextString(workbook,"P/O No.","Arial",10));
			row_p5_6.getCell(11).setCellStyle(style_border_left);
			row_p5_6.createCell(12).setCellValue("");
			row_p5_6.getCell(12).setCellStyle(style_border_right);
			
			row_7.createCell(11).setCellValue(createRichTextString(workbook,"วันที่","Arial",10));
			row_7.getCell(11).setCellStyle(style_border_top_left);
			row_7.createCell(12).setCellValue(createRichTextString(workbook,delivery_date,"Arial",10));
			row_7.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_7.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_7.createCell(11).setCellValue(createRichTextString(workbook,"วันที่","Arial",10));
			row_p2_7.getCell(11).setCellStyle(style_border_top_left);
			row_p2_7.createCell(12).setCellValue(createRichTextString(workbook,delivery_date,"Arial",10));
			row_p2_7.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_p2_7.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p3_7.createCell(11).setCellValue(createRichTextString(workbook,"วันที่","Arial",10));
			row_p3_7.getCell(11).setCellStyle(style_border_top_left);
			row_p3_7.createCell(12).setCellValue(createRichTextString(workbook,delivery_date,"Arial",10));
			row_p3_7.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_p3_7.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p4_7.createCell(11).setCellValue(createRichTextString(workbook,"วันที่","Arial",10));
			row_p4_7.getCell(11).setCellStyle(style_border_top_left);
			row_p4_7.createCell(12).setCellValue(createRichTextString(workbook,delivery_date,"Arial",10));
			row_p4_7.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_p4_7.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p5_7.createCell(11).setCellValue(createRichTextString(workbook,"วันที่","Arial",10));
			row_p5_7.getCell(11).setCellStyle(style_border_top_left);
			row_p5_7.createCell(12).setCellValue(createRichTextString(workbook,delivery_date,"Arial",10));
			row_p5_7.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_p5_7.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_8.createCell(11).setCellValue(createRichTextString(workbook,"Date ","Arial",10));
			row_8.getCell(11).setCellStyle(style_border_left);
			row_8.createCell(12);
			row_8.getCell(12).setCellStyle(style_border_right);
			
			row_p2_8.createCell(11).setCellValue(createRichTextString(workbook,"Date ","Arial",10));
			row_p2_8.getCell(11).setCellStyle(style_border_left);
			row_p2_8.createCell(12);
			row_p2_8.getCell(12).setCellStyle(style_border_right);
			
			row_p3_8.createCell(11).setCellValue(createRichTextString(workbook,"Date ","Arial",10));
			row_p3_8.getCell(11).setCellStyle(style_border_left);
			row_p3_8.createCell(12);
			row_p3_8.getCell(12).setCellStyle(style_border_right);
			
			row_p4_8.createCell(11).setCellValue(createRichTextString(workbook,"Date ","Arial",10));
			row_p4_8.getCell(11).setCellStyle(style_border_left);
			row_p4_8.createCell(12);
			row_p4_8.getCell(12).setCellStyle(style_border_right);
			
			row_p5_8.createCell(11).setCellValue(createRichTextString(workbook,"Date ","Arial",10));
			row_p5_8.getCell(11).setCellStyle(style_border_left);
			row_p5_8.createCell(12);
			row_p5_8.getCell(12).setCellStyle(style_border_right);

			row_9.createCell(0).setCellValue("เลขประจำตัวผู้เสียภาษีอากร 0 2 1 5 5 4 5 0 0 0 3 2 0");	      
			row_9.getCell(0).setCellStyle(style_arial_10);	 
			row_9.createCell(11).setCellValue(createRichTextString(workbook,"การชำระเงิน","Arial",10));
			row_9.getCell(11).setCellStyle(style_border_top_left);
			row_9.createCell(12).setCellValue(createRichTextString(workbook,credit+" วัน","Arial",10));
			row_9.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_9.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_9.createCell(0).setCellValue("เลขประจำตัวผู้เสียภาษีอากร 0 2 1 5 5 4 5 0 0 0 3 2 0");	      
			row_p2_9.getCell(0).setCellStyle(style_arial_10);	 
			row_p2_9.createCell(11).setCellValue(createRichTextString(workbook,"การชำระเงิน","Arial",10));
			row_p2_9.getCell(11).setCellStyle(style_border_top_left);
			row_p2_9.createCell(12).setCellValue(createRichTextString(workbook,credit+" วัน","Arial",10));
			row_p2_9.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_p2_9.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p3_9.createCell(0).setCellValue("เลขประจำตัวผู้เสียภาษีอากร 0 2 1 5 5 4 5 0 0 0 3 2 0");	      
			row_p3_9.getCell(0).setCellStyle(style_arial_10);	 
			row_p3_9.createCell(11).setCellValue(createRichTextString(workbook,"การชำระเงิน","Arial",10));
			row_p3_9.getCell(11).setCellStyle(style_border_top_left);
			row_p3_9.createCell(12).setCellValue(createRichTextString(workbook,credit+" วัน","Arial",10));
			row_p3_9.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_p3_9.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p4_9.createCell(0).setCellValue("เลขประจำตัวผู้เสียภาษีอากร 0 2 1 5 5 4 5 0 0 0 3 2 0");	      
			row_p4_9.getCell(0).setCellStyle(style_arial_10);	 
			row_p4_9.createCell(11).setCellValue(createRichTextString(workbook,"การชำระเงิน","Arial",10));
			row_p4_9.getCell(11).setCellStyle(style_border_top_left);
			row_p4_9.createCell(12).setCellValue(createRichTextString(workbook,credit+" วัน","Arial",10));
			row_p4_9.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_p4_9.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p5_9.createCell(0).setCellValue("เลขประจำตัวผู้เสียภาษีอากร 0 2 1 5 5 4 5 0 0 0 3 2 0");	      
			row_p5_9.getCell(0).setCellStyle(style_arial_10);	 
			row_p5_9.createCell(11).setCellValue(createRichTextString(workbook,"การชำระเงิน","Arial",10));
			row_p5_9.getCell(11).setCellStyle(style_border_top_left);
			row_p5_9.createCell(12).setCellValue(createRichTextString(workbook,credit+" วัน","Arial",10));
			row_p5_9.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_p5_9.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			
			row_10.createCell(11).setCellValue(createRichTextString(workbook,"Term of Payment","Arial",10));
			row_10.getCell(11).setCellStyle(style_border_left);
			row_10.createCell(12);
			row_10.getCell(12).setCellStyle(style_border_right);
			
			row_p2_10.createCell(11).setCellValue(createRichTextString(workbook,"Term of Payment","Arial",10));
			row_p2_10.getCell(11).setCellStyle(style_border_left);
			row_p2_10.createCell(12);
			row_p2_10.getCell(12).setCellStyle(style_border_right);
			
			row_p3_10.createCell(11).setCellValue(createRichTextString(workbook,"Term of Payment","Arial",10));
			row_p3_10.getCell(11).setCellStyle(style_border_left);
			row_p3_10.createCell(12);
			row_p3_10.getCell(12).setCellStyle(style_border_right);
			
			row_p4_10.createCell(11).setCellValue(createRichTextString(workbook,"Term of Payment","Arial",10));
			row_p4_10.getCell(11).setCellStyle(style_border_left);
			row_p4_10.createCell(12);
			row_p4_10.getCell(12).setCellStyle(style_border_right);
			
			row_p5_10.createCell(11).setCellValue(createRichTextString(workbook,"Term of Payment","Arial",10));
			row_p5_10.getCell(11).setCellStyle(style_border_left);
			row_p5_10.createCell(12);
			row_p5_10.getCell(12).setCellStyle(style_border_right);
			
			row_11.createCell(6).setCellValue("เลขที่");	       
			row_11.getCell(6).setCellStyle(style_arial_10);	  
			row_11.createCell(7).setCellValue(createRichTextString(workbook,inv_no,"Arial",14));	      
			row_11.getCell(7).setCellStyle(style_arial_14);	  
			row_11.createCell(11).setCellValue(createRichTextString(workbook,"ครบกำหนด","Arial",10));
			row_11.getCell(11).setCellStyle(style_border_top_left);
			row_11.createCell(12).setCellValue(createRichTextString(workbook,due_date,"Arial",10));
			row_11.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_11.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_11.createCell(6).setCellValue("เลขที่");	       
			row_p2_11.getCell(6).setCellStyle(style_arial_10);	  
			row_p2_11.createCell(7).setCellValue(createRichTextString(workbook,inv_no,"Arial",14));	      
			row_p2_11.getCell(7).setCellStyle(style_arial_14);	  
			row_p2_11.createCell(11).setCellValue(createRichTextString(workbook,"ครบกำหนด","Arial",10));
			row_p2_11.getCell(11).setCellStyle(style_border_top_left);
			row_p2_11.createCell(12).setCellValue(createRichTextString(workbook,due_date,"Arial",10));
			row_p2_11.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_p2_11.getCell(12), workbook, CellStyle.ALIGN_CENTER);			
			
			row_p3_11.createCell(6).setCellValue("เลขที่");	       
			row_p3_11.getCell(6).setCellStyle(style_arial_10);	  
			row_p3_11.createCell(7).setCellValue(createRichTextString(workbook,inv_no,"Arial",14));	      
			row_p3_11.getCell(7).setCellStyle(style_arial_14);	  
			row_p3_11.createCell(11).setCellValue(createRichTextString(workbook,"ครบกำหนด","Arial",10));
			row_p3_11.getCell(11).setCellStyle(style_border_top_left);
			row_p3_11.createCell(12).setCellValue(createRichTextString(workbook,due_date,"Arial",10));
			row_p3_11.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_p3_11.getCell(12), workbook, CellStyle.ALIGN_CENTER);			
			
			row_p4_11.createCell(6).setCellValue("เลขที่");	       
			row_p4_11.getCell(6).setCellStyle(style_arial_10);	  
			row_p4_11.createCell(7).setCellValue(createRichTextString(workbook,inv_no,"Arial",14));	      
			row_p4_11.getCell(7).setCellStyle(style_arial_14);	  
			row_p4_11.createCell(11).setCellValue(createRichTextString(workbook,"ครบกำหนด","Arial",10));
			row_p4_11.getCell(11).setCellStyle(style_border_top_left);
			row_p4_11.createCell(12).setCellValue(createRichTextString(workbook,due_date,"Arial",10));
			row_p4_11.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_p4_11.getCell(12), workbook, CellStyle.ALIGN_CENTER);		
			
			row_p5_11.createCell(6).setCellValue("เลขที่");	       
			row_p5_11.getCell(6).setCellStyle(style_arial_10);	  
			row_p5_11.createCell(7).setCellValue(createRichTextString(workbook,inv_no,"Arial",14));	      
			row_p5_11.getCell(7).setCellStyle(style_arial_14);	  
			row_p5_11.createCell(11).setCellValue(createRichTextString(workbook,"ครบกำหนด","Arial",10));
			row_p5_11.getCell(11).setCellStyle(style_border_top_left);
			row_p5_11.createCell(12).setCellValue(createRichTextString(workbook,due_date,"Arial",10));
			row_p5_11.getCell(12).setCellStyle(style_border_top_right);
			CellUtil.setAlignment(row_p5_11.getCell(12), workbook, CellStyle.ALIGN_CENTER);		
			
			row_12.createCell(11).setCellValue(createRichTextString(workbook,"Due Date","Arial",10));
			row_12.getCell(11).setCellStyle(style_border_bot_left);
			row_12.createCell(12).setCellValue(createRichTextString(workbook,"","Arial",10));
			row_12.getCell(12).setCellStyle(style_border_bot_right);
			
			row_p2_12.createCell(11).setCellValue(createRichTextString(workbook,"Due Date","Arial",10));
			row_p2_12.getCell(11).setCellStyle(style_border_bot_left);
			row_p2_12.createCell(12).setCellValue(createRichTextString(workbook,"","Arial",10));
			row_p2_12.getCell(12).setCellStyle(style_border_bot_right);
			
			row_p3_12.createCell(11).setCellValue(createRichTextString(workbook,"Due Date","Arial",10));
			row_p3_12.getCell(11).setCellStyle(style_border_bot_left);
			row_p3_12.createCell(12).setCellValue(createRichTextString(workbook,"","Arial",10));
			row_p3_12.getCell(12).setCellStyle(style_border_bot_right);
			
			row_p4_12.createCell(11).setCellValue(createRichTextString(workbook,"Due Date","Arial",10));
			row_p4_12.getCell(11).setCellStyle(style_border_bot_left);
			row_p4_12.createCell(12).setCellValue(createRichTextString(workbook,"","Arial",10));
			row_p4_12.getCell(12).setCellStyle(style_border_bot_right);
			
			row_p5_12.createCell(11).setCellValue(createRichTextString(workbook,"Due Date","Arial",10));
			row_p5_12.getCell(11).setCellStyle(style_border_bot_left);
			row_p5_12.createCell(12).setCellValue(createRichTextString(workbook,"","Arial",10));
			row_p5_12.getCell(12).setCellStyle(style_border_bot_right);
			
			
			row_13.createCell(0).setCellValue("ผู้ซื้อ / BUYER");	      
			row_13.getCell(0).setCellStyle(style_arial_8);	 
			row_13.createCell(2).setCellValue(createRichTextString(workbook,customer_name,"Arial",13));
			
			row_p2_13.createCell(0).setCellValue("ผู้ซื้อ / BUYER");	      
			row_p2_13.getCell(0).setCellStyle(style_arial_8);	 
			row_p2_13.createCell(2).setCellValue(createRichTextString(workbook,customer_name,"Arial",13));
			
			row_p3_13.createCell(0).setCellValue("ผู้ซื้อ / BUYER");	      
			row_p3_13.getCell(0).setCellStyle(style_arial_8);	 
			row_p3_13.createCell(2).setCellValue(createRichTextString(workbook,customer_name,"Arial",13));
			
			row_p4_13.createCell(0).setCellValue("ผู้ซื้อ / BUYER");	      
			row_p4_13.getCell(0).setCellStyle(style_arial_8);	 
			row_p4_13.createCell(2).setCellValue(createRichTextString(workbook,customer_name,"Arial",13));
			
			row_p5_13.createCell(0).setCellValue("ผู้ซื้อ / BUYER");	      
			row_p5_13.getCell(0).setCellStyle(style_arial_8);	 
			row_p5_13.createCell(2).setCellValue(createRichTextString(workbook,customer_name,"Arial",13));
			
			
			row_14.createCell(0).setCellValue("ที่อยู่ / ADDRESS");	 
			row_14.getCell(0).setCellStyle(style_arial_8);	
			row_14.createCell(2).setCellValue(createRichTextString(workbook,address_line0,"Arial",13));
			
			row_p2_14.createCell(0).setCellValue("ที่อยู่ / ADDRESS");	 
			row_p2_14.getCell(0).setCellStyle(style_arial_8);	
			row_p2_14.createCell(2).setCellValue(createRichTextString(workbook,address_line0,"Arial",13));
			
			row_p3_14.createCell(0).setCellValue("ที่อยู่ / ADDRESS");	 
			row_p3_14.getCell(0).setCellStyle(style_arial_8);	
			row_p3_14.createCell(2).setCellValue(createRichTextString(workbook,address_line0,"Arial",13));
			
			row_p4_14.createCell(0).setCellValue("ที่อยู่ / ADDRESS");	 
			row_p4_14.getCell(0).setCellStyle(style_arial_8);	
			row_p4_14.createCell(2).setCellValue(createRichTextString(workbook,address_line0,"Arial",13));
			
			row_p5_14.createCell(0).setCellValue("ที่อยู่ / ADDRESS");	 
			row_p5_14.getCell(0).setCellStyle(style_arial_8);	
			row_p5_14.createCell(2).setCellValue(createRichTextString(workbook,address_line0,"Arial",13));
			
			row_15.createCell(2).setCellValue(createRichTextString(workbook,address_line1,"Arial",13));
			row_p2_15.createCell(2).setCellValue(createRichTextString(workbook,address_line1,"Arial",13));
			row_p3_15.createCell(2).setCellValue(createRichTextString(workbook,address_line1,"Arial",13));
			row_p4_15.createCell(2).setCellValue(createRichTextString(workbook,address_line1,"Arial",13));
			row_p5_15.createCell(2).setCellValue(createRichTextString(workbook,address_line1,"Arial",13));
			
			row_16.createCell(2);
			row_16.getCell(2).setCellValue(createRichTextString(workbook,"Tax ID:"+tax_id,"Arial",10));
			
			row_p2_16.createCell(2);
			row_p2_16.getCell(2).setCellValue(createRichTextString(workbook,"Tax ID:"+tax_id,"Arial",10));
			
			row_p3_16.createCell(2);
			row_p3_16.getCell(2).setCellValue(createRichTextString(workbook,"Tax ID:"+tax_id,"Arial",10));
			
			row_p4_16.createCell(2);
			row_p4_16.getCell(2).setCellValue(createRichTextString(workbook,"Tax ID:"+tax_id,"Arial",10));
			
			row_p5_16.createCell(2);
			row_p5_16.getCell(2).setCellValue(createRichTextString(workbook,"Tax ID:"+tax_id,"Arial",10));
			
			row_18.createCell(0);
			row_18.createCell(1);
			row_18.getCell(0).setCellStyle(style_border_top_left);
			row_18.getCell(1).setCellStyle(style_border_top_right);
			row_18.getCell(0).setCellValue(createRichTextString(workbook,"จำนวน","Arial",10));
			
			row_p2_18.createCell(0);
			row_p2_18.createCell(1);
			row_p2_18.getCell(0).setCellStyle(style_border_top_left);
			row_p2_18.getCell(1).setCellStyle(style_border_top_right);
			row_p2_18.getCell(0).setCellValue(createRichTextString(workbook,"จำนวน","Arial",10));
			
			row_p3_18.createCell(0);
			row_p3_18.createCell(1);
			row_p3_18.getCell(0).setCellStyle(style_border_top_left);
			row_p3_18.getCell(1).setCellStyle(style_border_top_right);
			row_p3_18.getCell(0).setCellValue(createRichTextString(workbook,"จำนวน","Arial",10));
			
			row_p4_18.createCell(0);
			row_p4_18.createCell(1);
			row_p4_18.getCell(0).setCellStyle(style_border_top_left);
			row_p4_18.getCell(1).setCellStyle(style_border_top_right);
			row_p4_18.getCell(0).setCellValue(createRichTextString(workbook,"จำนวน","Arial",10));
			
			row_p5_18.createCell(0);
			row_p5_18.createCell(1);
			row_p5_18.getCell(0).setCellStyle(style_border_top_left);
			row_p5_18.getCell(1).setCellStyle(style_border_top_right);
			row_p5_18.getCell(0).setCellValue(createRichTextString(workbook,"จำนวน","Arial",10));

			for(int n = 2 ;n<=10 ;n++ )
			{
				row_18.createCell(n);
				row_18.getCell(n).setCellStyle(style_border_top);
				
				row_p2_18.createCell(n);
				row_p2_18.getCell(n).setCellStyle(style_border_top);
				
				row_p3_18.createCell(n);
				row_p3_18.getCell(n).setCellStyle(style_border_top);
				
				row_p4_18.createCell(n);
				row_p4_18.getCell(n).setCellStyle(style_border_top);
				
				row_p5_18.createCell(n);
				row_p5_18.getCell(n).setCellStyle(style_border_top);
				
			}
			row_18.createCell(11);
			row_18.createCell(12);
			row_18.getCell(11).setCellStyle(style_border_top_left);
			row_18.getCell(12).setCellStyle(style_border_top_left_right);
			row_18.getCell(2).setCellValue("รายการ");
			row_18.getCell(11).setCellValue(createRichTextString(workbook,"หน่วยละ","Arial",10));
			row_18.getCell(12).setCellValue(createRichTextString(workbook,"จำนวนเงิน","Arial",10));
			
			row_p2_18.createCell(11);
			row_p2_18.createCell(12);
			row_p2_18.getCell(11).setCellStyle(style_border_top_left);
			row_p2_18.getCell(12).setCellStyle(style_border_top_left_right);
			row_p2_18.getCell(2).setCellValue("รายการ");
			row_p2_18.getCell(11).setCellValue(createRichTextString(workbook,"หน่วยละ","Arial",10));
			row_p2_18.getCell(12).setCellValue(createRichTextString(workbook,"จำนวนเงิน","Arial",10));
			
			row_p3_18.createCell(11);
			row_p3_18.createCell(12);
			row_p3_18.getCell(11).setCellStyle(style_border_top_left);
			row_p3_18.getCell(12).setCellStyle(style_border_top_left_right);
			row_p3_18.getCell(2).setCellValue("รายการ");
			row_p3_18.getCell(11).setCellValue(createRichTextString(workbook,"หน่วยละ","Arial",10));
			row_p3_18.getCell(12).setCellValue(createRichTextString(workbook,"จำนวนเงิน","Arial",10));
			
			row_p4_18.createCell(11);
			row_p4_18.createCell(12);
			row_p4_18.getCell(11).setCellStyle(style_border_top_left);
			row_p4_18.getCell(12).setCellStyle(style_border_top_left_right);
			row_p4_18.getCell(2).setCellValue("รายการ");
			row_p4_18.getCell(11).setCellValue(createRichTextString(workbook,"หน่วยละ","Arial",10));
			row_p4_18.getCell(12).setCellValue(createRichTextString(workbook,"จำนวนเงิน","Arial",10));
			
			row_p5_18.createCell(11);
			row_p5_18.createCell(12);
			row_p5_18.getCell(11).setCellStyle(style_border_top_left);
			row_p5_18.getCell(12).setCellStyle(style_border_top_left_right);
			row_p5_18.getCell(2).setCellValue("รายการ");
			row_p5_18.getCell(11).setCellValue(createRichTextString(workbook,"หน่วยละ","Arial",10));
			row_p5_18.getCell(12).setCellValue(createRichTextString(workbook,"จำนวนเงิน","Arial",10));
			
			CellUtil.setAlignment(row_18.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_18.getCell(2), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_18.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_18.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			CellUtil.setAlignment(row_p2_18.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p2_18.getCell(2), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p2_18.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p2_18.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			CellUtil.setAlignment(row_p3_18.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p3_18.getCell(2), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p3_18.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p3_18.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			CellUtil.setAlignment(row_p4_18.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p4_18.getCell(2), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p4_18.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p4_18.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			CellUtil.setAlignment(row_p5_18.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p5_18.getCell(2), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p5_18.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p5_18.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_19.createCell(0);
			row_19.createCell(1);
			row_19.createCell(2);
			row_19.getCell(0).setCellStyle(style_border_bot_left);
			row_19.getCell(1).setCellStyle(style_border_bot_right);
			for(int j = 2 ;j<=10 ;j++ )
			{
				row_19.createCell(j);
				row_19.getCell(j).setCellStyle(style_border_bot);
				
			}
			row_19.createCell(11);
			row_19.createCell(12);
			row_19.getCell(11).setCellStyle(style_border_bot_left);
			row_19.getCell(12).setCellStyle(style_border_bot_left_right);
			row_19.getCell(0).setCellValue("QUANTITY");
			row_19.getCell(2).setCellValue("DESCRIPTION");
			row_19.getCell(11).setCellValue(createRichTextString(workbook,"UNIT PRICE","Arial",10));
			row_19.getCell(12).setCellValue(createRichTextString(workbook,"AMOUNT","Arial",10));
	
			CellUtil.setAlignment(row_19.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_19.getCell(2), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_19.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_19.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_19.createCell(0);
			row_p2_19.createCell(1);
			row_p2_19.createCell(2);
			row_p2_19.getCell(0).setCellStyle(style_border_bot_left);
			row_p2_19.getCell(1).setCellStyle(style_border_bot_right);
			for(int j = 2 ;j<=10 ;j++ )
			{
				row_p2_19.createCell(j);
				row_p2_19.getCell(j).setCellStyle(style_border_bot);
				
			}
			row_p2_19.createCell(11);
			row_p2_19.createCell(12);
			row_p2_19.getCell(11).setCellStyle(style_border_bot_left);
			row_p2_19.getCell(12).setCellStyle(style_border_bot_left_right);
			row_p2_19.getCell(0).setCellValue("QUANTITY");
			row_p2_19.getCell(2).setCellValue("DESCRIPTION");
			row_p2_19.getCell(11).setCellValue(createRichTextString(workbook,"UNIT PRICE","Arial",10));
			row_p2_19.getCell(12).setCellValue(createRichTextString(workbook,"AMOUNT","Arial",10));
			CellUtil.setAlignment(row_p2_19.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p2_19.getCell(2), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p2_19.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p2_19.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p3_19.createCell(0);
			row_p3_19.createCell(1);
			row_p3_19.createCell(2);
			row_p3_19.getCell(0).setCellStyle(style_border_bot_left);
			row_p3_19.getCell(1).setCellStyle(style_border_bot_right);
			for(int j = 2 ;j<=10 ;j++ )
			{
				row_p3_19.createCell(j);
				row_p3_19.getCell(j).setCellStyle(style_border_bot);
				
			}
			row_p3_19.createCell(11);
			row_p3_19.createCell(12);
			row_p3_19.getCell(11).setCellStyle(style_border_bot_left);
			row_p3_19.getCell(12).setCellStyle(style_border_bot_left_right);
			row_p3_19.getCell(0).setCellValue("QUANTITY");
			row_p3_19.getCell(2).setCellValue("DESCRIPTION");
			row_p3_19.getCell(11).setCellValue(createRichTextString(workbook,"UNIT PRICE","Arial",10));
			row_p3_19.getCell(12).setCellValue(createRichTextString(workbook,"AMOUNT","Arial",10));
			CellUtil.setAlignment(row_p3_19.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p3_19.getCell(2), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p3_19.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p3_19.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			row_p4_19.createCell(0);
			row_p4_19.createCell(1);
			row_p4_19.createCell(2);
			row_p4_19.getCell(0).setCellStyle(style_border_bot_left);
			row_p4_19.getCell(1).setCellStyle(style_border_bot_right);
			for(int j = 2 ;j<=10 ;j++ )
			{
				row_p4_19.createCell(j);
				row_p4_19.getCell(j).setCellStyle(style_border_bot);
				
			}
			row_p4_19.createCell(11);
			row_p4_19.createCell(12);
			row_p4_19.getCell(11).setCellStyle(style_border_bot_left);
			row_p4_19.getCell(12).setCellStyle(style_border_bot_left_right);
			row_p4_19.getCell(0).setCellValue("QUANTITY");
			row_p4_19.getCell(2).setCellValue("DESCRIPTION");
			row_p4_19.getCell(11).setCellValue(createRichTextString(workbook,"UNIT PRICE","Arial",10));
			row_p4_19.getCell(12).setCellValue(createRichTextString(workbook,"AMOUNT","Arial",10));
			CellUtil.setAlignment(row_p4_19.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p4_19.getCell(2), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p4_19.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p4_19.getCell(12), workbook, CellStyle.ALIGN_CENTER);
	
			row_p5_19.createCell(0);
			row_p5_19.createCell(1);
			row_p5_19.createCell(2);
			row_p5_19.getCell(0).setCellStyle(style_border_bot_left);
			row_p5_19.getCell(1).setCellStyle(style_border_bot_right);
			for(int j = 2 ;j<=10 ;j++ )
			{
				row_p5_19.createCell(j);
				row_p5_19.getCell(j).setCellStyle(style_border_bot);
				
			}
			row_p5_19.createCell(11);
			row_p5_19.createCell(12);
			row_p5_19.getCell(11).setCellStyle(style_border_bot_left);
			row_p5_19.getCell(12).setCellStyle(style_border_bot_left_right);
			row_p5_19.getCell(0).setCellValue("QUANTITY");
			row_p5_19.getCell(2).setCellValue("DESCRIPTION");
			row_p5_19.getCell(11).setCellValue(createRichTextString(workbook,"UNIT PRICE","Arial",10));
			row_p5_19.getCell(12).setCellValue(createRichTextString(workbook,"AMOUNT","Arial",10));
			CellUtil.setAlignment(row_p5_19.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p5_19.getCell(2), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p5_19.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_p5_19.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			
			DrawListCellBorder(workbook,row_20);
			DrawListCellBorder(workbook,row_21);
			DrawListCellBorder(workbook,row_22);
			DrawListCellBorder(workbook,row_23);
			DrawListCellBorder(workbook,row_24);
			DrawListCellBorder(workbook,row_25);
			DrawListCellBorder(workbook,row_26);
			DrawListCellBorder(workbook,row_27);
			DrawListCellBorder(workbook,row_28);
			DrawListCellBorder(workbook,row_29);
			DrawListCellBorder(workbook,row_30);
			DrawListCellBorder(workbook,row_31);
			DrawListCellBorder(workbook,row_32);
			DrawListCellBorder(workbook,row_33);
			DrawListCellBorder(workbook,row_34);
			DrawListCellBorder(workbook,row_35);
			DrawListCellBorder(workbook,row_36);
			DrawListCellBorder(workbook,row_37);
			DrawListCellBorder(workbook,row_38);
			DrawListCellBorder(workbook,row_39);
			DrawListCellBorder(workbook,row_40);
			DrawListCellBorder(workbook,row_41);
			DrawListCellBorder(workbook,row_42);
			DrawListCellBorder(workbook,row_43);
			
			DrawListCellBorder(workbook,row_p2_20);
			DrawListCellBorder(workbook,row_p2_21);
			DrawListCellBorder(workbook,row_p2_22);
			DrawListCellBorder(workbook,row_p2_23);
			DrawListCellBorder(workbook,row_p2_24);
			DrawListCellBorder(workbook,row_p2_25);
			DrawListCellBorder(workbook,row_p2_26);
			DrawListCellBorder(workbook,row_p2_27);
			DrawListCellBorder(workbook,row_p2_28);
			DrawListCellBorder(workbook,row_p2_29);
			DrawListCellBorder(workbook,row_p2_30);
			DrawListCellBorder(workbook,row_p2_31);
			DrawListCellBorder(workbook,row_p2_32);
			DrawListCellBorder(workbook,row_p2_33);
			DrawListCellBorder(workbook,row_p2_34);
			DrawListCellBorder(workbook,row_p2_35);
			DrawListCellBorder(workbook,row_p2_36);
			DrawListCellBorder(workbook,row_p2_37);
			DrawListCellBorder(workbook,row_p2_38);
			DrawListCellBorder(workbook,row_p2_39);
			DrawListCellBorder(workbook,row_p2_40);
			DrawListCellBorder(workbook,row_p2_41);
			DrawListCellBorder(workbook,row_p2_42);
			DrawListCellBorder(workbook,row_p2_43);
			
			DrawListCellBorder(workbook,row_p3_20);
			DrawListCellBorder(workbook,row_p3_21);
			DrawListCellBorder(workbook,row_p3_22);
			DrawListCellBorder(workbook,row_p3_23);
			DrawListCellBorder(workbook,row_p3_24);
			DrawListCellBorder(workbook,row_p3_25);
			DrawListCellBorder(workbook,row_p3_26);
			DrawListCellBorder(workbook,row_p3_27);
			DrawListCellBorder(workbook,row_p3_28);
			DrawListCellBorder(workbook,row_p3_29);
			DrawListCellBorder(workbook,row_p3_30);
			DrawListCellBorder(workbook,row_p3_31);
			DrawListCellBorder(workbook,row_p3_32);
			DrawListCellBorder(workbook,row_p3_33);
			DrawListCellBorder(workbook,row_p3_34);
			DrawListCellBorder(workbook,row_p3_35);
			DrawListCellBorder(workbook,row_p3_36);
			DrawListCellBorder(workbook,row_p3_37);
			DrawListCellBorder(workbook,row_p3_38);
			DrawListCellBorder(workbook,row_p3_39);
			DrawListCellBorder(workbook,row_p3_40);
			DrawListCellBorder(workbook,row_p3_41);
			DrawListCellBorder(workbook,row_p3_42);
			DrawListCellBorder(workbook,row_p3_43);
			
			DrawListCellBorder(workbook,row_p4_20);
			DrawListCellBorder(workbook,row_p4_21);
			DrawListCellBorder(workbook,row_p4_22);
			DrawListCellBorder(workbook,row_p4_23);
			DrawListCellBorder(workbook,row_p4_24);
			DrawListCellBorder(workbook,row_p4_25);
			DrawListCellBorder(workbook,row_p4_26);
			DrawListCellBorder(workbook,row_p4_27);
			DrawListCellBorder(workbook,row_p4_28);
			DrawListCellBorder(workbook,row_p4_29);
			DrawListCellBorder(workbook,row_p4_30);
			DrawListCellBorder(workbook,row_p4_31);
			DrawListCellBorder(workbook,row_p4_32);
			DrawListCellBorder(workbook,row_p4_33);
			DrawListCellBorder(workbook,row_p4_34);
			DrawListCellBorder(workbook,row_p4_35);
			DrawListCellBorder(workbook,row_p4_36);
			DrawListCellBorder(workbook,row_p4_37);
			DrawListCellBorder(workbook,row_p4_38);
			DrawListCellBorder(workbook,row_p4_39);
			DrawListCellBorder(workbook,row_p4_40);
			DrawListCellBorder(workbook,row_p4_41);
			DrawListCellBorder(workbook,row_p4_42);
			DrawListCellBorder(workbook,row_p4_43);
			
			DrawListCellBorder(workbook,row_p5_20);
			DrawListCellBorder(workbook,row_p5_21);
			DrawListCellBorder(workbook,row_p5_22);
			DrawListCellBorder(workbook,row_p5_23);
			DrawListCellBorder(workbook,row_p5_24);
			DrawListCellBorder(workbook,row_p5_25);
			DrawListCellBorder(workbook,row_p5_26);
			DrawListCellBorder(workbook,row_p5_27);
			DrawListCellBorder(workbook,row_p5_28);
			DrawListCellBorder(workbook,row_p5_29);
			DrawListCellBorder(workbook,row_p5_30);
			DrawListCellBorder(workbook,row_p5_31);
			DrawListCellBorder(workbook,row_p5_32);
			DrawListCellBorder(workbook,row_p5_33);
			DrawListCellBorder(workbook,row_p5_34);
			DrawListCellBorder(workbook,row_p5_35);
			DrawListCellBorder(workbook,row_p5_36);
			DrawListCellBorder(workbook,row_p5_37);
			DrawListCellBorder(workbook,row_p5_38);
			DrawListCellBorder(workbook,row_p5_39);
			DrawListCellBorder(workbook,row_p5_40);
			DrawListCellBorder(workbook,row_p5_41);
			DrawListCellBorder(workbook,row_p5_42);
			DrawListCellBorder(workbook,row_p5_43);
	
			int count_detail = 0;
			System.out.println("Starting Write Bill detail to xls file");
			for(int q=20;count_detail<order_detail_list.size();count_detail++,q++)
			{
				
				String temp_price = order_detail_list.get(count_detail).getPrice();
				String temp_sum = order_detail_list.get(count_detail).getSum();
				
				double fix_price = Double.parseDouble(temp_price);
				double fix_sum =  Double.parseDouble(temp_sum);
				
				DecimalFormat formatter = new DecimalFormat("#,###.00");
				
				if(temp_price.indexOf('.')<0)
				{
					temp_price = temp_price + ".00";
					
				}else{
					
				}
				if(temp_sum.indexOf('.')<0)
				{
					temp_sum = temp_sum + ".00";
					
				}else{
					
				}
					   
				page1.getRow(q).createCell(2);
				page1.getRow(q).getCell(0).setCellValue(createRichTextString(workbook,order_detail_list.get(count_detail).getQuantity(),"Arial",10));
				page1.getRow(q).getCell(1).setCellValue(createRichTextString(workbook,order_detail_list.get(count_detail).getProductUnit(),"Arial",10));
				page1.getRow(q).getCell(2).setCellValue(createRichTextString(workbook,order_detail_list.get(count_detail).getAdtDescription(),"Arial",10));
				page1.getRow(q).getCell(11).setCellValue(createRichTextString(workbook,formatter.format(fix_price),"Arial",10));
				page1.getRow(q).getCell(12).setCellValue(createRichTextString(workbook,formatter.format(fix_sum),"Arial",10));
				System.out.println(count_detail+":"+order_detail_list.get(count_detail).getAdtDescription());
				
				
				CellUtil.setAlignment(page1.getRow(q).getCell(0), workbook, CellStyle.ALIGN_CENTER);
				CellUtil.setAlignment(page1.getRow(q).getCell(1), workbook, CellStyle.ALIGN_CENTER);
				CellUtil.setAlignment(page1.getRow(q).getCell(11), workbook, CellStyle.ALIGN_RIGHT);
				CellUtil.setAlignment(page1.getRow(q).getCell(12), workbook, CellStyle.ALIGN_RIGHT);
				
				page2.getRow(q).createCell(2);
				page2.getRow(q).getCell(0).setCellValue(createRichTextString(workbook,order_detail_list.get(count_detail).getQuantity(),"Arial",10));
				page2.getRow(q).getCell(1).setCellValue(createRichTextString(workbook,order_detail_list.get(count_detail).getProductUnit(),"Arial",10));
				page2.getRow(q).getCell(2).setCellValue(createRichTextString(workbook,order_detail_list.get(count_detail).getAdtDescription(),"Arial",10));
				page2.getRow(q).getCell(11).setCellValue(createRichTextString(workbook,formatter.format(fix_price),"Arial",10));
				page2.getRow(q).getCell(12).setCellValue(createRichTextString(workbook,formatter.format(fix_sum),"Arial",10));
				
				CellUtil.setAlignment(page2.getRow(q).getCell(0), workbook, CellStyle.ALIGN_CENTER);
				CellUtil.setAlignment(page2.getRow(q).getCell(1), workbook, CellStyle.ALIGN_CENTER);
				CellUtil.setAlignment(page2.getRow(q).getCell(11), workbook, CellStyle.ALIGN_RIGHT);
				CellUtil.setAlignment(page2.getRow(q).getCell(12), workbook, CellStyle.ALIGN_RIGHT);
				
				page3.getRow(q).createCell(2);
				page3.getRow(q).getCell(0).setCellValue(createRichTextString(workbook,order_detail_list.get(count_detail).getQuantity(),"Arial",10));
				page3.getRow(q).getCell(1).setCellValue(createRichTextString(workbook,order_detail_list.get(count_detail).getProductUnit(),"Arial",10));
				page3.getRow(q).getCell(2).setCellValue(createRichTextString(workbook,order_detail_list.get(count_detail).getAdtDescription(),"Arial",10));
				page3.getRow(q).getCell(11).setCellValue(createRichTextString(workbook,formatter.format(fix_price),"Arial",10));
				page3.getRow(q).getCell(12).setCellValue(createRichTextString(workbook,formatter.format(fix_sum),"Arial",10));
				
				CellUtil.setAlignment(page3.getRow(q).getCell(0), workbook, CellStyle.ALIGN_CENTER);
				CellUtil.setAlignment(page3.getRow(q).getCell(1), workbook, CellStyle.ALIGN_CENTER);
				CellUtil.setAlignment(page3.getRow(q).getCell(11), workbook, CellStyle.ALIGN_RIGHT);
				CellUtil.setAlignment(page3.getRow(q).getCell(12), workbook, CellStyle.ALIGN_RIGHT);
				
				page4.getRow(q).createCell(2);
				page4.getRow(q).getCell(0).setCellValue(createRichTextString(workbook,order_detail_list.get(count_detail).getQuantity(),"Arial",10));
				page4.getRow(q).getCell(1).setCellValue(createRichTextString(workbook,order_detail_list.get(count_detail).getProductUnit(),"Arial",10));
				page4.getRow(q).getCell(2).setCellValue(createRichTextString(workbook,order_detail_list.get(count_detail).getAdtDescription(),"Arial",10));
				page4.getRow(q).getCell(11).setCellValue(createRichTextString(workbook,formatter.format(fix_price),"Arial",10));
				page4.getRow(q).getCell(12).setCellValue(createRichTextString(workbook,formatter.format(fix_sum),"Arial",10));
				
				CellUtil.setAlignment(page4.getRow(q).getCell(0), workbook, CellStyle.ALIGN_CENTER);
				CellUtil.setAlignment(page4.getRow(q).getCell(1), workbook, CellStyle.ALIGN_CENTER);
				CellUtil.setAlignment(page4.getRow(q).getCell(11), workbook, CellStyle.ALIGN_RIGHT);
				CellUtil.setAlignment(page4.getRow(q).getCell(12), workbook, CellStyle.ALIGN_RIGHT);
			
				page5.getRow(q).createCell(2);
				page5.getRow(q).getCell(0).setCellValue(createRichTextString(workbook,order_detail_list.get(count_detail).getQuantity(),"Arial",10));
				page5.getRow(q).getCell(1).setCellValue(createRichTextString(workbook,order_detail_list.get(count_detail).getProductUnit(),"Arial",10));
				page5.getRow(q).getCell(2).setCellValue(createRichTextString(workbook,order_detail_list.get(count_detail).getAdtDescription(),"Arial",10));
				page5.getRow(q).getCell(11).setCellValue(createRichTextString(workbook,formatter.format(fix_price),"Arial",10));
				page5.getRow(q).getCell(12).setCellValue(createRichTextString(workbook,formatter.format(fix_sum),"Arial",10));
				
				CellUtil.setAlignment(page5.getRow(q).getCell(0), workbook, CellStyle.ALIGN_CENTER);
				CellUtil.setAlignment(page5.getRow(q).getCell(1), workbook, CellStyle.ALIGN_CENTER);
				CellUtil.setAlignment(page5.getRow(q).getCell(11), workbook, CellStyle.ALIGN_RIGHT);
				CellUtil.setAlignment(page5.getRow(q).getCell(12), workbook, CellStyle.ALIGN_RIGHT);
				
				
			}
			double total_value_fixed =  Double.parseDouble(total_value);
			DecimalFormat formatter = new DecimalFormat("#,###.00");
			
			row_44.createCell(0);	
			row_44.getCell(0).setCellStyle(style_border_top_left);
			for(int l=1;l<=12;l++)
			{
				row_44.createCell(l);
				if(l!=10){
					row_44.getCell(l).setCellStyle(style_border_top);
				}
			}
			
			row_44.getCell(10).setCellStyle(style_border_top_left);
			row_44.getCell(12).setCellStyle(style_border_full);
			row_44.getCell(0).setCellValue("จำนวนเงินรวมทั้งสิ้น ( ตัวอักษร )");
			row_44.getCell(10).setCellValue(createRichTextString(workbook,"รวมราคาทั้งสิ้น","Arial",10));
			row_44.getCell(12).setCellValue(createRichTextString(workbook,formatter.format(total_value_fixed),"Arial",10));
			CellUtil.setAlignment(row_44.getCell(12), workbook, CellStyle.ALIGN_RIGHT);
			CellUtil.setAlignment(row_44.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			

			row_p2_44.createCell(0);	
			row_p2_44.getCell(0).setCellStyle(style_border_top_left);
			for(int l=1;l<=12;l++)
			{
				row_p2_44.createCell(l);
				if(l!=10){
					row_p2_44.getCell(l).setCellStyle(style_border_top);
				}
			}
			
			row_p2_44.getCell(10).setCellStyle(style_border_top_left);
			row_p2_44.getCell(12).setCellStyle(style_border_full);
			row_p2_44.getCell(0).setCellValue("จำนวนเงินรวมทั้งสิ้น ( ตัวอักษร )");
			row_p2_44.getCell(10).setCellValue(createRichTextString(workbook,"รวมราคาทั้งสิ้น","Arial",10));
			row_p2_44.getCell(12).setCellValue(createRichTextString(workbook,formatter.format(total_value_fixed),"Arial",10));
			CellUtil.setAlignment(row_p2_44.getCell(12), workbook, CellStyle.ALIGN_RIGHT);
			CellUtil.setAlignment(row_p2_44.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			
			row_p3_44.createCell(0);	
			row_p3_44.getCell(0).setCellStyle(style_border_top_left);
			for(int l=1;l<=12;l++)
			{
				row_p3_44.createCell(l);
				if(l!=10){
					row_p3_44.getCell(l).setCellStyle(style_border_top);
				}
			}
			
			row_p3_44.getCell(10).setCellStyle(style_border_top_left);
			row_p3_44.getCell(12).setCellStyle(style_border_full);
			row_p3_44.getCell(0).setCellValue("จำนวนเงินรวมทั้งสิ้น ( ตัวอักษร )");
			row_p3_44.getCell(10).setCellValue(createRichTextString(workbook,"รวมราคาทั้งสิ้น","Arial",10));
			row_p3_44.getCell(12).setCellValue(createRichTextString(workbook,formatter.format(total_value_fixed),"Arial",10));
			CellUtil.setAlignment(row_p3_44.getCell(12), workbook, CellStyle.ALIGN_RIGHT);
			CellUtil.setAlignment(row_p3_44.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			
			row_p4_44.createCell(0);	
			row_p4_44.getCell(0).setCellStyle(style_border_top_left);
			for(int l=1;l<=12;l++)
			{
				row_p4_44.createCell(l);
				if(l!=10){
					row_p4_44.getCell(l).setCellStyle(style_border_top);
				}
			}
			
			row_p4_44.getCell(10).setCellStyle(style_border_top_left);
			row_p4_44.getCell(12).setCellStyle(style_border_full);
			row_p4_44.getCell(0).setCellValue("จำนวนเงินรวมทั้งสิ้น ( ตัวอักษร )");
			row_p4_44.getCell(10).setCellValue(createRichTextString(workbook,"รวมราคาทั้งสิ้น","Arial",10));
			row_p4_44.getCell(12).setCellValue(createRichTextString(workbook,formatter.format(total_value_fixed),"Arial",10));
			CellUtil.setAlignment(row_p4_44.getCell(12), workbook, CellStyle.ALIGN_RIGHT);
			CellUtil.setAlignment(row_p4_44.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			
			row_p5_44.createCell(0);	
			row_p5_44.getCell(0).setCellStyle(style_border_top_left);
			for(int l=1;l<=12;l++)
			{
				row_p5_44.createCell(l);
				if(l!=10){
					row_p5_44.getCell(l).setCellStyle(style_border_top);
				}
			}
			
			row_p5_44.getCell(10).setCellStyle(style_border_top_left);
			row_p5_44.getCell(12).setCellStyle(style_border_full);
			row_p5_44.getCell(0).setCellValue("จำนวนเงินรวมทั้งสิ้น ( ตัวอักษร )");
			row_p5_44.getCell(10).setCellValue(createRichTextString(workbook,"รวมราคาทั้งสิ้น","Arial",10));
			row_p5_44.getCell(12).setCellValue(createRichTextString(workbook,formatter.format(total_value_fixed),"Arial",10));
			CellUtil.setAlignment(row_p5_44.getCell(12), workbook, CellStyle.ALIGN_RIGHT);
			CellUtil.setAlignment(row_p5_44.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			
			double total_vat_fixed = Double.parseDouble(total_vat);
			
			
			row_45.createCell(0);
			row_45.createCell(10);
			row_45.createCell(11);
			row_45.createCell(12);
			row_45.getCell(0).setCellStyle(style_border_left);
			row_45.getCell(10).setCellStyle(style_border_left);
			row_45.getCell(11).setCellStyle(style_border_right);
			row_45.getCell(12).setCellStyle(style_border_full);
			row_45.getCell(12).setCellValue(createRichTextString(workbook,formatter.format(total_vat_fixed),"Arial",10));
			row_45.getCell(10).setCellValue(createRichTextString(workbook,"จำนวนภาษีมูลค่าเพิ่ม","Arial",10));		
			CellUtil.setAlignment(row_45.getCell(12), workbook, CellStyle.ALIGN_RIGHT);
			String str_formula_45 = "BAHTTEXT(M46)";
			row_45.getCell(0).setCellType(Cell.CELL_TYPE_FORMULA);
			row_45.getCell(0).setCellFormula(str_formula_45);
			CellUtil.setAlignment(row_45.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_45.createCell(0);
			row_p2_45.createCell(10);
			row_p2_45.createCell(11);
			row_p2_45.createCell(12);
			row_p2_45.getCell(0).setCellStyle(style_border_left);
			row_p2_45.getCell(10).setCellStyle(style_border_left);
			row_p2_45.getCell(11).setCellStyle(style_border_right);
			row_p2_45.getCell(12).setCellStyle(style_border_full);
			row_p2_45.getCell(12).setCellValue(createRichTextString(workbook,formatter.format(total_vat_fixed),"Arial",10));
			row_p2_45.getCell(10).setCellValue(createRichTextString(workbook,"จำนวนภาษีมูลค่าเพิ่ม","Arial",10));		
			CellUtil.setAlignment(row_p2_45.getCell(12), workbook, CellStyle.ALIGN_RIGHT);
			row_p2_45.getCell(0).setCellType(Cell.CELL_TYPE_FORMULA);
			row_p2_45.getCell(0).setCellFormula(str_formula_45);
			CellUtil.setAlignment(row_p2_45.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			
			row_p3_45.createCell(0);
			row_p3_45.createCell(10);
			row_p3_45.createCell(11);
			row_p3_45.createCell(12);
			row_p3_45.getCell(0).setCellStyle(style_border_left);
			row_p3_45.getCell(10).setCellStyle(style_border_left);
			row_p3_45.getCell(11).setCellStyle(style_border_right);
			row_p3_45.getCell(12).setCellStyle(style_border_full);
			row_p3_45.getCell(12).setCellValue(createRichTextString(workbook,formatter.format(total_vat_fixed),"Arial",10));
			row_p3_45.getCell(10).setCellValue(createRichTextString(workbook,"จำนวนภาษีมูลค่าเพิ่ม","Arial",10));		
			CellUtil.setAlignment(row_p3_45.getCell(12), workbook, CellStyle.ALIGN_RIGHT);
			row_p3_45.getCell(0).setCellType(Cell.CELL_TYPE_FORMULA);
			row_p3_45.getCell(0).setCellFormula(str_formula_45);
			CellUtil.setAlignment(row_p3_45.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			
			row_p4_45.createCell(0);
			row_p4_45.createCell(10);
			row_p4_45.createCell(11);
			row_p4_45.createCell(12);
			row_p4_45.getCell(0).setCellStyle(style_border_left);
			row_p4_45.getCell(10).setCellStyle(style_border_left);
			row_p4_45.getCell(11).setCellStyle(style_border_right);
			row_p4_45.getCell(12).setCellStyle(style_border_full);
			row_p4_45.getCell(12).setCellValue(createRichTextString(workbook,formatter.format(total_vat_fixed),"Arial",10));
			row_p4_45.getCell(10).setCellValue(createRichTextString(workbook,"จำนวนภาษีมูลค่าเพิ่ม","Arial",10));		
			CellUtil.setAlignment(row_p4_45.getCell(12), workbook, CellStyle.ALIGN_RIGHT);
			row_p4_45.getCell(0).setCellType(Cell.CELL_TYPE_FORMULA);
			row_p4_45.getCell(0).setCellFormula(str_formula_45);
			CellUtil.setAlignment(row_p4_45.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			
			row_p5_45.createCell(0);
			row_p5_45.createCell(10);
			row_p5_45.createCell(11);
			row_p5_45.createCell(12);
			row_p5_45.getCell(0).setCellStyle(style_border_left);
			row_p5_45.getCell(10).setCellStyle(style_border_left);
			row_p5_45.getCell(11).setCellStyle(style_border_right);
			row_p5_45.getCell(12).setCellStyle(style_border_full);
			row_p5_45.getCell(12).setCellValue(createRichTextString(workbook,formatter.format(total_vat_fixed),"Arial",10));
			row_p5_45.getCell(10).setCellValue(createRichTextString(workbook,"จำนวนภาษีมูลค่าเพิ่ม","Arial",10));		
			CellUtil.setAlignment(row_p5_45.getCell(12), workbook, CellStyle.ALIGN_RIGHT);
			row_p5_45.getCell(0).setCellType(Cell.CELL_TYPE_FORMULA);
			row_p5_45.getCell(0).setCellFormula(str_formula_45);
			CellUtil.setAlignment(row_p5_45.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			
			
			double total_inc_vat_fixed = Double.parseDouble(total_inc_vat);
				
			
			row_46.createCell(0);
			row_46.createCell(10);
			row_46.createCell(11);
			row_46.createCell(12);
			for(int a=1;a<=9;a++)
			{
				row_46.createCell(a);
				row_46.getCell(a).setCellStyle(style_border_bot);
			}
			row_46.getCell(0).setCellStyle(style_border_bot_left);
			row_46.getCell(10).setCellStyle(style_border_bot_left);
			row_46.getCell(11).setCellStyle(style_border_bot_right);
			row_46.getCell(12).setCellStyle(style_border_full);
			row_46.getCell(10).setCellValue(createRichTextString(workbook,"จำนวนเงินรวมทั้งสิ้น","Arial",10));
			row_46.getCell(12).setCellValue(createRichTextString(workbook,formatter.format(total_inc_vat_fixed),"Arial",10));
			CellUtil.setAlignment(row_46.getCell(12), workbook, CellStyle.ALIGN_RIGHT);
			
			row_p2_46.createCell(0);
			row_p2_46.createCell(10);
			row_p2_46.createCell(11);
			row_p2_46.createCell(12);
			for(int a=1;a<=9;a++)
			{
				row_p2_46.createCell(a);
				row_p2_46.getCell(a).setCellStyle(style_border_bot);
			}
			row_p2_46.getCell(0).setCellStyle(style_border_bot_left);
			row_p2_46.getCell(10).setCellStyle(style_border_bot_left);
			row_p2_46.getCell(11).setCellStyle(style_border_bot_right);
			row_p2_46.getCell(12).setCellStyle(style_border_full);
			row_p2_46.getCell(10).setCellValue(createRichTextString(workbook,"จำนวนเงินรวมทั้งสิ้น","Arial",10));
			row_p2_46.getCell(12).setCellValue(createRichTextString(workbook,formatter.format(total_inc_vat_fixed),"Arial",10));
			CellUtil.setAlignment(row_p2_46.getCell(12), workbook, CellStyle.ALIGN_RIGHT);
			
			row_p3_46.createCell(0);
			row_p3_46.createCell(10);
			row_p3_46.createCell(11);
			row_p3_46.createCell(12);
			for(int a=1;a<=9;a++)
			{
				row_p3_46.createCell(a);
				row_p3_46.getCell(a).setCellStyle(style_border_bot);
			}
			row_p3_46.getCell(0).setCellStyle(style_border_bot_left);
			row_p3_46.getCell(10).setCellStyle(style_border_bot_left);
			row_p3_46.getCell(11).setCellStyle(style_border_bot_right);
			row_p3_46.getCell(12).setCellStyle(style_border_full);
			row_p3_46.getCell(10).setCellValue(createRichTextString(workbook,"จำนวนเงินรวมทั้งสิ้น","Arial",10));
			row_p3_46.getCell(12).setCellValue(createRichTextString(workbook,formatter.format(total_inc_vat_fixed),"Arial",10));
			CellUtil.setAlignment(row_p3_46.getCell(12), workbook, CellStyle.ALIGN_RIGHT);
			
			row_p4_46.createCell(0);
			row_p4_46.createCell(10);
			row_p4_46.createCell(11);
			row_p4_46.createCell(12);
			for(int a=1;a<=9;a++)
			{
				row_p4_46.createCell(a);
				row_p4_46.getCell(a).setCellStyle(style_border_bot);
			}
			row_p4_46.getCell(0).setCellStyle(style_border_bot_left);
			row_p4_46.getCell(10).setCellStyle(style_border_bot_left);
			row_p4_46.getCell(11).setCellStyle(style_border_bot_right);
			row_p4_46.getCell(12).setCellStyle(style_border_full);
			row_p4_46.getCell(10).setCellValue(createRichTextString(workbook,"จำนวนเงินรวมทั้งสิ้น","Arial",10));
			row_p4_46.getCell(12).setCellValue(createRichTextString(workbook,formatter.format(total_inc_vat_fixed),"Arial",10));
			CellUtil.setAlignment(row_p4_46.getCell(12), workbook, CellStyle.ALIGN_RIGHT);
			
			row_p5_46.createCell(0);
			row_p5_46.createCell(10);
			row_p5_46.createCell(11);
			row_p5_46.createCell(12);
			for(int a=1;a<=9;a++)
			{
				row_p5_46.createCell(a);
				row_p5_46.getCell(a).setCellStyle(style_border_bot);
			}
			row_p5_46.getCell(0).setCellStyle(style_border_bot_left);
			row_p5_46.getCell(10).setCellStyle(style_border_bot_left);
			row_p5_46.getCell(11).setCellStyle(style_border_bot_right);
			row_p5_46.getCell(12).setCellStyle(style_border_full);
			row_p5_46.getCell(10).setCellValue(createRichTextString(workbook,"จำนวนเงินรวมทั้งสิ้น","Arial",10));
			row_p5_46.getCell(12).setCellValue(createRichTextString(workbook,formatter.format(total_inc_vat_fixed),"Arial",10));
			CellUtil.setAlignment(row_p5_46.getCell(12), workbook, CellStyle.ALIGN_RIGHT);
			

			row_47.createCell(0);
			row_47.createCell(1);
			row_47.createCell(11);
			row_47.createCell(12);
			row_47.getCell(0).setCellStyle(style_border_left);
			row_47.getCell(11).setCellStyle(style_border_left);
			row_47.getCell(12).setCellStyle(style_border_right);
			row_47.getCell(1).setCellValue("เงินสด");		
			row_47.getCell(11).setCellValue(createRichTextString(workbook,"บริษัท อาทิตย์ เคมีภัณฑ์ จำกัด","Arial",10));
			CellUtil.setAlignment(row_47.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_47.createCell(0);
			row_p2_47.createCell(1);
			row_p2_47.createCell(11);
			row_p2_47.createCell(12);
			row_p2_47.getCell(0).setCellStyle(style_border_left);
			row_p2_47.getCell(11).setCellStyle(style_border_left);
			row_p2_47.getCell(12).setCellStyle(style_border_right);
			row_p2_47.getCell(1).setCellValue("เงินสด");		
			row_p2_47.getCell(11).setCellValue(createRichTextString(workbook,"บริษัท อาทิตย์ เคมีภัณฑ์ จำกัด","Arial",10));
			CellUtil.setAlignment(row_p2_47.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_p3_47.createCell(0);
			row_p3_47.createCell(1);
			row_p3_47.createCell(11);
			row_p3_47.createCell(12);
			row_p3_47.getCell(0).setCellStyle(style_border_left);
			row_p3_47.getCell(11).setCellStyle(style_border_left);
			row_p3_47.getCell(12).setCellStyle(style_border_right);
			row_p3_47.getCell(1).setCellValue("เงินสด");		
			row_p3_47.getCell(11).setCellValue(createRichTextString(workbook,"บริษัท อาทิตย์ เคมีภัณฑ์ จำกัด","Arial",10));
			CellUtil.setAlignment(row_p3_47.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_p4_47.createCell(0);
			row_p4_47.createCell(1);
			row_p4_47.createCell(11);
			row_p4_47.createCell(12);
			row_p4_47.getCell(0).setCellStyle(style_border_left);
			row_p4_47.getCell(11).setCellStyle(style_border_left);
			row_p4_47.getCell(12).setCellStyle(style_border_right);
			row_p4_47.getCell(1).setCellValue("เงินสด");		
			row_p4_47.getCell(11).setCellValue(createRichTextString(workbook,"บริษัท อาทิตย์ เคมีภัณฑ์ จำกัด","Arial",10));
			CellUtil.setAlignment(row_p4_47.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_p5_47.createCell(0);
			row_p5_47.createCell(1);
			row_p5_47.createCell(11);
			row_p5_47.createCell(12);
			row_p5_47.getCell(0).setCellStyle(style_border_left);
			row_p5_47.getCell(11).setCellStyle(style_border_left);
			row_p5_47.getCell(12).setCellStyle(style_border_right);
			row_p5_47.getCell(1).setCellValue("เงินสด");		
			row_p5_47.getCell(11).setCellValue(createRichTextString(workbook,"บริษัท อาทิตย์ เคมีภัณฑ์ จำกัด","Arial",10));
			CellUtil.setAlignment(row_p5_47.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_48.createCell(0);
			row_48.createCell(1);
			row_48.createCell(11);
			row_48.createCell(12);
			row_48.getCell(0).setCellStyle(style_border_left);
			row_48.getCell(11).setCellStyle(style_border_left);
			row_48.getCell(12).setCellStyle(style_border_right);
			row_48.getCell(1).setCellValue("เช็คธนาคาร / สาขา");
			row_48.getCell(11).setCellValue(createRichTextString(workbook,"ARTHITAYA CHEMICAL CO., LTD.","Arial",9));
			CellUtil.setAlignment(row_48.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_48.createCell(0);
			row_p2_48.createCell(1);
			row_p2_48.createCell(11);
			row_p2_48.createCell(12);
			row_p2_48.getCell(0).setCellStyle(style_border_left);
			row_p2_48.getCell(11).setCellStyle(style_border_left);
			row_p2_48.getCell(12).setCellStyle(style_border_right);
			row_p2_48.getCell(1).setCellValue("เช็คธนาคาร / สาขา");
			row_p2_48.getCell(11).setCellValue(createRichTextString(workbook,"ARTHITAYA CHEMICAL CO., LTD.","Arial",9));
			CellUtil.setAlignment(row_p2_48.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_p3_48.createCell(0);
			row_p3_48.createCell(1);
			row_p3_48.createCell(11);
			row_p3_48.createCell(12);
			row_p3_48.getCell(0).setCellStyle(style_border_left);
			row_p3_48.getCell(11).setCellStyle(style_border_left);
			row_p3_48.getCell(12).setCellStyle(style_border_right);
			row_p3_48.getCell(1).setCellValue("เช็คธนาคาร / สาขา");
			row_p3_48.getCell(11).setCellValue(createRichTextString(workbook,"ARTHITAYA CHEMICAL CO., LTD.","Arial",9));
			CellUtil.setAlignment(row_p3_48.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_p4_48.createCell(0);
			row_p4_48.createCell(1);
			row_p4_48.createCell(11);
			row_p4_48.createCell(12);
			row_p4_48.getCell(0).setCellStyle(style_border_left);
			row_p4_48.getCell(11).setCellStyle(style_border_left);
			row_p4_48.getCell(12).setCellStyle(style_border_right);
			row_p4_48.getCell(1).setCellValue("เช็คธนาคาร / สาขา");
			row_p4_48.getCell(11).setCellValue(createRichTextString(workbook,"ARTHITAYA CHEMICAL CO., LTD.","Arial",9));
			CellUtil.setAlignment(row_p4_48.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_p5_48.createCell(0);
			row_p5_48.createCell(1);
			row_p5_48.createCell(11);
			row_p5_48.createCell(12);
			row_p5_48.getCell(0).setCellStyle(style_border_left);
			row_p5_48.getCell(11).setCellStyle(style_border_left);
			row_p5_48.getCell(12).setCellStyle(style_border_right);
			row_p5_48.getCell(1).setCellValue("เช็คธนาคาร / สาขา");
			row_p5_48.getCell(11).setCellValue(createRichTextString(workbook,"ARTHITAYA CHEMICAL CO., LTD.","Arial",9));
			CellUtil.setAlignment(row_p5_48.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			
			row_49.createCell(0);
			row_49.createCell(4);
			row_49.createCell(8);
			row_49.createCell(11);
			row_49.createCell(12);
			row_49.getCell(0).setCellStyle(style_border_left);
			row_49.getCell(11).setCellStyle(style_border_left);
			row_49.getCell(12).setCellStyle(style_border_right);
			row_49.getCell(0).setCellValue("เลขที่เช็ค");
			row_49.getCell(4).setCellValue("ผู้รับสินค้า");
			row_49.getCell(8).setCellValue("วันที่ ");
			
			row_p2_49.createCell(0);
			row_p2_49.createCell(4);
			row_p2_49.createCell(8);
			row_p2_49.createCell(11);
			row_p2_49.createCell(12);
			row_p2_49.getCell(0).setCellStyle(style_border_left);
			row_p2_49.getCell(11).setCellStyle(style_border_left);
			row_p2_49.getCell(12).setCellStyle(style_border_right);
			row_p2_49.getCell(0).setCellValue("เลขที่เช็ค");
			row_p2_49.getCell(4).setCellValue("ผู้รับสินค้า");
			row_p2_49.getCell(8).setCellValue("วันที่ ");
			
			row_p3_49.createCell(0);
			row_p3_49.createCell(4);
			row_p3_49.createCell(8);
			row_p3_49.createCell(11);
			row_p3_49.createCell(12);
			row_p3_49.getCell(0).setCellStyle(style_border_left);
			row_p3_49.getCell(11).setCellStyle(style_border_left);
			row_p3_49.getCell(12).setCellStyle(style_border_right);
			row_p3_49.getCell(0).setCellValue("เลขที่เช็ค");
			row_p3_49.getCell(4).setCellValue("ผู้รับสินค้า");
			row_p3_49.getCell(8).setCellValue("วันที่ ");
			
			row_p4_49.createCell(0);
			row_p4_49.createCell(4);
			row_p4_49.createCell(8);
			row_p4_49.createCell(11);
			row_p4_49.createCell(12);
			row_p4_49.getCell(0).setCellStyle(style_border_left);
			row_p4_49.getCell(11).setCellStyle(style_border_left);
			row_p4_49.getCell(12).setCellStyle(style_border_right);
			row_p4_49.getCell(0).setCellValue("เลขที่เช็ค");
			row_p4_49.getCell(4).setCellValue("ผู้รับสินค้า");
			row_p4_49.getCell(8).setCellValue("วันที่ ");
			
			row_p5_49.createCell(0);
			row_p5_49.createCell(4);
			row_p5_49.createCell(8);
			row_p5_49.createCell(11);
			row_p5_49.createCell(12);
			row_p5_49.getCell(0).setCellStyle(style_border_left);
			row_p5_49.getCell(11).setCellStyle(style_border_left);
			row_p5_49.getCell(12).setCellStyle(style_border_right);
			row_p5_49.getCell(0).setCellValue("เลขที่เช็ค");
			row_p5_49.getCell(4).setCellValue("ผู้รับสินค้า");
			row_p5_49.getCell(8).setCellValue("วันที่ ");
			
			row_50.createCell(0);
			row_50.createCell(11);
			row_50.createCell(12);
			row_50.getCell(0).setCellStyle(style_border_left);
			row_50.getCell(11).setCellStyle(style_border_left);
			row_50.getCell(12).setCellStyle(style_border_right);
			row_50.getCell(0).setCellValue(createRichTextString(workbook,"วันที่","Arial",10));
			
			row_p2_50.createCell(0);
			row_p2_50.createCell(11);
			row_p2_50.createCell(12);
			row_p2_50.getCell(0).setCellStyle(style_border_left);
			row_p2_50.getCell(11).setCellStyle(style_border_left);
			row_p2_50.getCell(12).setCellStyle(style_border_right);
			row_p2_50.getCell(0).setCellValue(createRichTextString(workbook,"วันที่","Arial",10));
			
			row_p3_50.createCell(0);
			row_p3_50.createCell(11);
			row_p3_50.createCell(12);
			row_p3_50.getCell(0).setCellStyle(style_border_left);
			row_p3_50.getCell(11).setCellStyle(style_border_left);
			row_p3_50.getCell(12).setCellStyle(style_border_right);
			row_p3_50.getCell(0).setCellValue(createRichTextString(workbook,"วันที่","Arial",10));
			
			row_p4_50.createCell(0);
			row_p4_50.createCell(11);
			row_p4_50.createCell(12);
			row_p4_50.getCell(0).setCellStyle(style_border_left);
			row_p4_50.getCell(11).setCellStyle(style_border_left);
			row_p4_50.getCell(12).setCellStyle(style_border_right);
			row_p4_50.getCell(0).setCellValue(createRichTextString(workbook,"วันที่","Arial",10));
			
			row_p5_50.createCell(0);
			row_p5_50.createCell(11);
			row_p5_50.createCell(12);
			row_p5_50.getCell(0).setCellStyle(style_border_left);
			row_p5_50.getCell(11).setCellStyle(style_border_left);
			row_p5_50.getCell(12).setCellStyle(style_border_right);
			row_p5_50.getCell(0).setCellValue(createRichTextString(workbook,"วันที่","Arial",10));
			
			

			row_51.createCell(0);
			row_51.createCell(4);
			row_51.createCell(8);
			row_51.createCell(11);
			row_51.createCell(12);
			row_51.getCell(0).setCellStyle(style_border_left);
			row_51.getCell(11).setCellStyle(style_border_left);
			row_51.getCell(12).setCellStyle(style_border_right);
			row_51.getCell(0).setCellValue(createRichTextString(workbook,"ผู้รับเงิน","Arial",10));
			row_51.getCell(4).setCellValue("ผู้ส่งสินค้า");
			row_51.getCell(8).setCellValue("วันที่ ");
			row_51.getCell(11).setCellValue(createRichTextString(workbook,"ผู้มีอำนาจลงนาม","Arial",10));
			CellUtil.setAlignment(row_51.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_51.createCell(0);
			row_p2_51.createCell(4);
			row_p2_51.createCell(8);
			row_p2_51.createCell(11);
			row_p2_51.createCell(12);
			row_p2_51.getCell(0).setCellStyle(style_border_left);
			row_p2_51.getCell(11).setCellStyle(style_border_left);
			row_p2_51.getCell(12).setCellStyle(style_border_right);
			row_p2_51.getCell(0).setCellValue(createRichTextString(workbook,"ผู้รับเงิน","Arial",10));
			row_p2_51.getCell(4).setCellValue("ผู้ส่งสินค้า");
			row_p2_51.getCell(8).setCellValue("วันที่ ");
			row_p2_51.getCell(11).setCellValue(createRichTextString(workbook,"ผู้มีอำนาจลงนาม","Arial",10));
			CellUtil.setAlignment(row_p2_51.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_p3_51.createCell(0);
			row_p3_51.createCell(4);
			row_p3_51.createCell(8);
			row_p3_51.createCell(11);
			row_p3_51.createCell(12);
			row_p3_51.getCell(0).setCellStyle(style_border_left);
			row_p3_51.getCell(11).setCellStyle(style_border_left);
			row_p3_51.getCell(12).setCellStyle(style_border_right);
			row_p3_51.getCell(0).setCellValue(createRichTextString(workbook,"ผู้รับเงิน","Arial",10));
			row_p3_51.getCell(4).setCellValue("ผู้ส่งสินค้า");
			row_p3_51.getCell(8).setCellValue("วันที่ ");
			row_p3_51.getCell(11).setCellValue(createRichTextString(workbook,"ผู้มีอำนาจลงนาม","Arial",10));
			CellUtil.setAlignment(row_p3_51.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_p4_51.createCell(0);
			row_p4_51.createCell(4);
			row_p4_51.createCell(8);
			row_p4_51.createCell(11);
			row_p4_51.createCell(12);
			row_p4_51.getCell(0).setCellStyle(style_border_left);
			row_p4_51.getCell(11).setCellStyle(style_border_left);
			row_p4_51.getCell(12).setCellStyle(style_border_right);
			row_p4_51.getCell(0).setCellValue(createRichTextString(workbook,"ผู้รับเงิน","Arial",10));
			row_p4_51.getCell(4).setCellValue("ผู้ส่งสินค้า");
			row_p4_51.getCell(8).setCellValue("วันที่ ");
			row_p4_51.getCell(11).setCellValue(createRichTextString(workbook,"ผู้มีอำนาจลงนาม","Arial",10));
			CellUtil.setAlignment(row_p4_51.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_p5_51.createCell(0);
			row_p5_51.createCell(4);
			row_p5_51.createCell(8);
			row_p5_51.createCell(11);
			row_p5_51.createCell(12);
			row_p5_51.getCell(0).setCellStyle(style_border_left);
			row_p5_51.getCell(11).setCellStyle(style_border_left);
			row_p5_51.getCell(12).setCellStyle(style_border_right);
			row_p5_51.getCell(0).setCellValue(createRichTextString(workbook,"ผู้รับเงิน","Arial",10));
			row_p5_51.getCell(4).setCellValue("ผู้ส่งสินค้า");
			row_p5_51.getCell(8).setCellValue("วันที่ ");
			row_p5_51.getCell(11).setCellValue(createRichTextString(workbook,"ผู้มีอำนาจลงนาม","Arial",10));
			CellUtil.setAlignment(row_p5_51.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_52.createCell(0);
			row_52.createCell(11);
			row_52.createCell(12);
			for(int b=1;b<=10;b++)
			{
				row_52.createCell(b);
				row_52.getCell(b).setCellStyle(style_border_bot);
				
			}
			row_52.getCell(0).setCellStyle(style_border_bot_left);
			row_52.getCell(11).setCellStyle(style_border_bot_left);
			row_52.getCell(12).setCellStyle(style_border_bot_right);
			row_52.getCell(11).setCellValue(createRichTextString(workbook,"Authorized Signature","Arial",10));
			CellUtil.setAlignment(row_52.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_p2_52.createCell(0);
			row_p2_52.createCell(11);
			row_p2_52.createCell(12);
			for(int b=1;b<=10;b++)
			{
				row_p2_52.createCell(b);
				row_p2_52.getCell(b).setCellStyle(style_border_bot);
				
			}
			row_p2_52.getCell(0).setCellStyle(style_border_bot_left);
			row_p2_52.getCell(11).setCellStyle(style_border_bot_left);
			row_p2_52.getCell(12).setCellStyle(style_border_bot_right);
			row_p2_52.getCell(11).setCellValue(createRichTextString(workbook,"Authorized Signature","Arial",10));
			CellUtil.setAlignment(row_p2_52.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_p3_52.createCell(0);
			row_p3_52.createCell(11);
			row_p3_52.createCell(12);
			for(int b=1;b<=10;b++)
			{
				row_p3_52.createCell(b);
				row_p3_52.getCell(b).setCellStyle(style_border_bot);
				
			}
			row_p3_52.getCell(0).setCellStyle(style_border_bot_left);
			row_p3_52.getCell(11).setCellStyle(style_border_bot_left);
			row_p3_52.getCell(12).setCellStyle(style_border_bot_right);
			row_p3_52.getCell(11).setCellValue(createRichTextString(workbook,"Authorized Signature","Arial",10));
			CellUtil.setAlignment(row_p3_52.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_p4_52.createCell(0);
			row_p4_52.createCell(11);
			row_p4_52.createCell(12);
			for(int b=1;b<=10;b++)
			{
				row_p4_52.createCell(b);
				row_p4_52.getCell(b).setCellStyle(style_border_bot);
				
			}
			row_p4_52.getCell(0).setCellStyle(style_border_bot_left);
			row_p4_52.getCell(11).setCellStyle(style_border_bot_left);
			row_p4_52.getCell(12).setCellStyle(style_border_bot_right);
			row_p4_52.getCell(11).setCellValue(createRichTextString(workbook,"Authorized Signature","Arial",10));
			CellUtil.setAlignment(row_p4_52.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_p5_52.createCell(0);
			row_p5_52.createCell(11);
			row_p5_52.createCell(12);
			for(int b=1;b<=10;b++)
			{
				row_p5_52.createCell(b);
				row_p5_52.getCell(b).setCellStyle(style_border_bot);
				
			}
			row_p5_52.getCell(0).setCellStyle(style_border_bot_left);
			row_p5_52.getCell(11).setCellStyle(style_border_bot_left);
			row_p5_52.getCell(12).setCellStyle(style_border_bot_right);
			row_p5_52.getCell(11).setCellValue(createRichTextString(workbook,"Authorized Signature","Arial",10));
			CellUtil.setAlignment(row_p5_52.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			
			row_53.createCell(0);
			row_53.getCell(0).setCellValue(createRichTextString(workbook,"ใบเสร็จรับเงินฉบับนี้จะสมบูรณ์ต่อเมื่อ ธนาคารได้เรียกเก็บเงินตามเช็คเข้าบัญชีของบริษัทฯเรียบร้อยแล้ว","Arial",10));
			
			row_p2_53.createCell(0);
			row_p2_53.getCell(0).setCellValue(createRichTextString(workbook,"ใบเสร็จรับเงินฉบับนี้จะสมบูรณ์ต่อเมื่อ ธนาคารได้เรียกเก็บเงินตามเช็คเข้าบัญชีของบริษัทฯเรียบร้อยแล้ว","Arial",10));
			
			row_p3_53.createCell(0);
			row_p3_53.getCell(0).setCellValue(createRichTextString(workbook,"ใบเสร็จรับเงินฉบับนี้จะสมบูรณ์ต่อเมื่อ ธนาคารได้เรียกเก็บเงินตามเช็คเข้าบัญชีของบริษัทฯเรียบร้อยแล้ว","Arial",10));
			
			row_p4_53.createCell(0);
			row_p4_53.getCell(0).setCellValue(createRichTextString(workbook,"ใบเสร็จรับเงินฉบับนี้จะสมบูรณ์ต่อเมื่อ ธนาคารได้เรียกเก็บเงินตามเช็คเข้าบัญชีของบริษัทฯเรียบร้อยแล้ว","Arial",10));
			
			row_p5_53.createCell(0);
			row_p5_53.getCell(0).setCellValue(createRichTextString(workbook,"ใบเสร็จรับเงินฉบับนี้จะสมบูรณ์ต่อเมื่อ ธนาคารได้เรียกเก็บเงินตามเช็คเข้าบัญชีของบริษัทฯเรียบร้อยแล้ว","Arial",10));
			
			row_54.createCell(0);
			row_54.getCell(0).setCellValue(createRichTextString(workbook,"If payment is made by cheque, this receipt will be valid, Until the cheque is honoured by the bank","Arial",10));
			
			row_p2_54.createCell(0);
			row_p2_54.getCell(0).setCellValue(createRichTextString(workbook,"If payment is made by cheque, this receipt will be valid, Until the cheque is honoured by the bank","Arial",10));
			
			row_p3_54.createCell(0);
			row_p3_54.getCell(0).setCellValue(createRichTextString(workbook,"If payment is made by cheque, this receipt will be valid, Until the cheque is honoured by the bank","Arial",10));
			
			row_p4_54.createCell(0);
			row_p4_54.getCell(0).setCellValue(createRichTextString(workbook,"If payment is made by cheque, this receipt will be valid, Until the cheque is honoured by the bank","Arial",10));
			
			row_p5_54.createCell(0);
			row_p5_54.getCell(0).setCellValue(createRichTextString(workbook,"If payment is made by cheque, this receipt will be valid, Until the cheque is honoured by the bank","Arial",10));
			
			workbook.write(new FileOutputStream(fileName));
			
			
			String file_path = fileName;
			String new_status ="";
			
			if(status.equals("order_created"))
			{
				String update_new_status =  " UPDATE `order_main` SET `status`='inv_generated'"
										   +" WHERE order_main.order_id ='"+order_id+"'";
				connect.createStatement().executeUpdate(update_new_status);
				
			}else{
				
				
				
			}	
		    System.out.println("Word document created to : " + application.getRealPath("/report/"));
			
		    
			String sql_update_file_path = " UPDATE `order_main` SET `inv_file_path`= ? "
										 +" , `inv_file_name`= ? "
									     +" , `inv_generated_datetime`= NOW() "
										 +" WHERE order_main.order_id = ? ";
			
			PreparedStatement preparedStatement = null;
			preparedStatement = connect.prepareStatement(sql_update_file_path);
		
		    
			System.out.println("sql_update_file_path:"+sql_update_file_path);
		    
			String pattern = Pattern.quote(System.getProperty("file.separator"));
			String[] parts = file_path.split(pattern);
			int temp = parts.length;
			String inv_file_name = parts[temp-1];
			
			String output = inv_file_name+"&"+file_path;
			
			preparedStatement.setString(1, file_path);
			preparedStatement.setString(2, inv_file_name);
			preparedStatement.setString(3, order_id);
			
			preparedStatement.executeUpdate();
			
		    
			out.print(output);
			
	} catch (Exception e) {
		e.printStackTrace();
		connect.close();
		out.print("fail");
	}
	///////////////////////////////////////////////////////End Writing Excel Process/////////////////////////////////////////////////////////
	 
	connect.close();
%>
