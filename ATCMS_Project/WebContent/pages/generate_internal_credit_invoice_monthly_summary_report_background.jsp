<%@page import="org.apache.poi.xwpf.usermodel.TextAlignment"%>
<%@page import="org.apache.poi.xwpf.usermodel.Borders"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "com.atcms.*" %>
<%@ page import = "java.util.List" %>
<%@ page import = "java.util.ArrayList"%>
<%@ page import = "java.util.Properties" %>
<%@ page import = "javax.servlet.ServletContext" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.util.Date" %>
<%@ page import="java.math.BigInteger" %>
<%@ page import="java.io.FileOutputStream" %>
<%@ page import="org.apache.poi.xssf.usermodel.*" %>
<%@ page import="org.apache.poi.hssf.usermodel.HSSFCellStyle" %>
<%@ page import="org.apache.poi.hssf.util.*" %>
<%@ page import="org.apache.poi.ss.util.CellUtil" %>
<%@ page import="org.apache.poi.ss.usermodel.*" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>
<%@ page import = "java.text.DecimalFormat" %>




<%!
		public static final short EXCEL_COLUMN_WIDTH_FACTOR = 256; 
	    public static final short EXCEL_ROW_HEIGHT_FACTOR = 20; 
	    public static final int UNIT_OFFSET_LENGTH = 7; 
	    public static final int[] UNIT_OFFSET_MAP = new int[] { 0, 36, 73, 109, 146, 182, 219 };
	    
	    public static RichTextString createRichTextString (XSSFWorkbook workbook_in,String text,String font_name ,int font_size)
	    {
	    	 RichTextString richString = new XSSFRichTextString(text);
	    	 XSSFFont font = workbook_in.createFont();
	    	 font.setFontName(font_name);
			 font.setFontHeightInPoints((short)font_size);
			 richString.applyFont(font);
			 return richString;
	    }
	    
	    
		public static XSSFCellStyle CreateStyle(XSSFWorkbook workbook_in
							 				   ,String font_name_in 
							                   , int font_size_in
							                   , boolean isBold)
		{
				XSSFFont font = workbook_in.createFont();
				font.setFontName(font_name_in);
				font.setFontHeightInPoints((short)font_size_in);
				font.setBold(isBold);
				XSSFCellStyle style = workbook_in.createCellStyle();
				style.setFont(font);
				return style ;
		}
				
		public static XSSFCellStyle CreateCellBorder(XSSFWorkbook workbook_in 
										, boolean top 
										, boolean bot 
										, boolean left 
										, boolean right)
		{
				
				XSSFCellStyle style = workbook_in.createCellStyle();
				if(top){  style.setBorderTop(HSSFCellStyle.BORDER_THIN); }
				if(bot){ style.setBorderBottom(HSSFCellStyle.BORDER_THIN); }
				if(left){ style.setBorderLeft(HSSFCellStyle.BORDER_THIN); }
				if(right){ style.setBorderRight(HSSFCellStyle.BORDER_THIN); }
				
				return style ;
		}
		public static void DrawListCellBorder (XSSFWorkbook workbook , XSSFRow row_in){
			
			XSSFCellStyle style_border_top_left = CreateCellBorder(workbook,true,false,true,false);
			XSSFCellStyle style_border_top_right = CreateCellBorder(workbook,true,false,false,true);
			XSSFCellStyle style_border_top_left_right = CreateCellBorder(workbook,true,false,true,true);
			XSSFCellStyle style_border_bot_left = CreateCellBorder(workbook,false,true,true,false);
			XSSFCellStyle style_border_bot_right = CreateCellBorder(workbook,false,true,false,true);
			XSSFCellStyle style_border_bot_left_right = CreateCellBorder(workbook,false,true,true,true);
			XSSFCellStyle style_border_top = CreateCellBorder(workbook,true,false,false,false);
			XSSFCellStyle style_border_bot = CreateCellBorder(workbook,false,true,false,false);
			XSSFCellStyle style_border_left = CreateCellBorder(workbook,false,false,true,false);
			XSSFCellStyle style_border_right = CreateCellBorder(workbook,false,false,false,true);
			XSSFCellStyle style_border_left_right = CreateCellBorder(workbook,false,false,true,true);
			XSSFCellStyle style_border_full = CreateCellBorder(workbook,true,true,true,true);
			
			row_in.createCell(0);
			row_in.createCell(1);
			row_in.createCell(11);
			row_in.createCell(12);
			row_in.getCell(0).setCellStyle(style_border_left_right);
			row_in.getCell(1).setCellStyle(style_border_left_right);
			row_in.getCell(11).setCellStyle(style_border_left_right);
			row_in.getCell(12).setCellStyle(style_border_left_right);

		}
	    public static int widthUnits2Pixel(short widthUnits) {
	    	
	        int pixels = (widthUnits / EXCEL_COLUMN_WIDTH_FACTOR) * UNIT_OFFSET_LENGTH; 
	        int offsetWidthUnits = widthUnits % EXCEL_COLUMN_WIDTH_FACTOR; 
	        pixels += Math.floor((float) offsetWidthUnits / ((float) EXCEL_COLUMN_WIDTH_FACTOR / UNIT_OFFSET_LENGTH));   
	        return pixels; 
	    }
	    public static short pixel2WidthUnits(int pxs) {
	        short widthUnits = (short) (EXCEL_COLUMN_WIDTH_FACTOR * (pxs / UNIT_OFFSET_LENGTH)); 
	        widthUnits += UNIT_OFFSET_MAP[(pxs % UNIT_OFFSET_LENGTH)];  
	        return widthUnits; 
	    } 
	    


%>

<% 

	System.out.println("Start generate_external_credit_invoice_monthly_summary_report_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
	
	/////////////////////////Start Get Data Process /////////////////////////////
	
	String month = request.getParameter("month");
	String year = request.getParameter("year");
	String form_of_report = request.getParameter("form_of_report");
	
	
	String order_id ="";
	String type ="";
	String ponum ="";
	String customer_name="";
	String customer_address_full="";  
	String total_value="";
	String total_vat="";
	String total_inc_vat = "";
	String inv_no="";
	String due_date="";
	String delivery_date="";
	String credit="";
	String tax_id = "";
	String address_line0 = "";
	String address_line1 = "";
	String status = "";
	String inv_no_temp ="";
	
	
	List<OrderDetail> order_detail_list = new ArrayList<OrderDetail>();

	

	////////////////////////////////////////////////////////////////////////////
	
	String order_detail_query = " SELECT * "+
												"  FROM order_detail "+
												" JOIN order_main "+
												" ON  order_detail.order_id = order_main.order_id "+
												" JOIN company "+
												" ON  order_main.customer_id = company.company_id "	+											
											    " WHERE  MONTH(order_main.delivery_date) = '"+month+"' "+
												" AND YEAR(order_main.delivery_date) ='"+year+"' "+
											    " ORDER BY order_main.inv_no ASC , order_detail.index ASC ";

	ResultSet rs_order_detail = connect.createStatement().executeQuery(order_detail_query);
	System.out.println("order_detail_query:"+order_detail_query);
	int i=0;
	while(rs_order_detail.next())
    {
			OrderDetail temp_order_detail = new OrderDetail();
			temp_order_detail.setAdtDescription(rs_order_detail.getString("adt_description"));
			temp_order_detail.setOrderId(rs_order_detail.getString("order_id"));
			temp_order_detail.setInvoiceNo(rs_order_detail.getString("inv_no"));
			if(rs_order_detail.getString("name_th").equals("-"))
			{
				temp_order_detail.setCustomerName(rs_order_detail.getString("name_en"));
			}else{
				temp_order_detail.setCustomerName(rs_order_detail.getString("name_th"));
			}
			temp_order_detail.setOrderMainTotalIncVat(rs_order_detail.getString("total_inc_vat"));
			
			temp_order_detail.setProductUnit(rs_order_detail.getString("unit"));
			temp_order_detail.setDeliveryDate(rs_order_detail.getString("delivery_date"));
			temp_order_detail.setPrice(rs_order_detail.getString("price"));
			temp_order_detail.setQuantity(rs_order_detail.getString("quantity"));
			temp_order_detail.setSum(rs_order_detail.getString("sum"));
			
		
			String temp_profit_percentage = rs_order_detail.getString("profit_percentage");
		
			if(!(("").equals(temp_profit_percentage))&&(!("NaN").equals(temp_profit_percentage))&&(temp_profit_percentage!=null))
			{
				temp_order_detail.setProfitPercentage(rs_order_detail.getString("profit_percentage"));
				
			}else{
				
				temp_order_detail.setProfitPercentage("-");
				
			}
			
			String temp_sum_cost = rs_order_detail.getString("sum_cost");
			
			if(!(("").equals(temp_sum_cost))&&(!("NaN").equals(temp_sum_cost))&&(temp_sum_cost!=null))
			{
				temp_order_detail.setSumCost(rs_order_detail.getString("sum_cost"));
				
			}else{
				
				temp_order_detail.setSumCost("-");
				
			}
			
			String temp_sum_profit = rs_order_detail.getString("sum_profit");
			
			if(!(("").equals(temp_sum_profit))&&(!("NaN").equals(temp_sum_profit))&&(temp_sum_profit!=null))
			{
				temp_order_detail.setSumProfit(rs_order_detail.getString("sum_profit"));
				
			}else{
				
				temp_order_detail.setSumProfit("-");
				
			}
			String temp_total_cost = rs_order_detail.getString("total_cost");
			
			if(!(("").equals(temp_total_cost))&&(!("NaN").equals(temp_total_cost))&&(temp_total_cost!=null))
			{
				temp_order_detail.setOrderMainTotalCost(rs_order_detail.getString("total_cost"));
				
			}else{
				
				temp_order_detail.setOrderMainTotalCost("-");
				
			}
			
			String temp_total_profit = rs_order_detail.getString("total_profit");
			
			if(!(("").equals(temp_total_profit))&&(!("NaN").equals(temp_total_profit))&&(temp_total_profit!=null))
			{
				temp_order_detail.setOrderMainTotalProfit(rs_order_detail.getString("total_profit"));
				
			}else{
				
				temp_order_detail.setOrderMainTotalProfit("-");
				
			}
			
			String temp_cost_per_unit = rs_order_detail.getString("cost_per_unit");
			
			if(!(("").equals(temp_cost_per_unit))&&(!("NaN").equals(temp_cost_per_unit))&&(temp_cost_per_unit!=null))
			{
				temp_order_detail.setCostPerUnit(rs_order_detail.getString("cost_per_unit"));
				
			}else{
				
				temp_order_detail.setCostPerUnit("-");
				
			}
			
			String temp_profit_per_unit = rs_order_detail.getString("profit_per_unit");
			
			if(!(("").equals(temp_profit_per_unit))&&(!("NaN").equals(temp_profit_per_unit))&&(temp_profit_per_unit!=null))
			{
				temp_order_detail.setProfitPerUnitI(rs_order_detail.getString("profit_per_unit"));
				
			}else{
				
				temp_order_detail.setProfitPerUnitI("-");
				
			}
			
			String temp_avg_profit_percentage = rs_order_detail.getString("avg_profit_percentage");
			
			if(!(("").equals(temp_avg_profit_percentage))&&(!("NaN").equals(temp_avg_profit_percentage))&&(temp_avg_profit_percentage!=null))
			{
				temp_order_detail.setOrderMainAvgProfitPercentage(rs_order_detail.getString("avg_profit_percentage"));
				
			}else{
				
				temp_order_detail.setOrderMainAvgProfitPercentage("-");
				
			}



			temp_order_detail.setOrderMainTotalIncVat(rs_order_detail.getString("total_inc_vat"));
			temp_order_detail.setOrderMainTotalValue(rs_order_detail.getString("total_value"));
			temp_order_detail.setOrderMainTotalVat(rs_order_detail.getString("total_vat"));

	
			
			order_detail_list.add(temp_order_detail);
			
		//	System.out.println(i+":"+temp_order_detail.getAdtDescription());
			i++;
    }
	
	System.out.println("order_detail_list.size:"+order_detail_list.size());
	
	//////////////////////////End Get Data Process /////////////////////////////
	
	
	/////////////////////////Start Writing Excel Process//////////////////////////
	
	

	try {		
		
			inv_no_temp = inv_no.replace("/","_");
			Date curr_date = new Date();
	//		String curr_year = curr_date.getYear()+2443+"";
	//		String curr_month = curr_date.getMonth()+1+"";
			String year_temp = (Integer.parseInt(year)+543)+"";
			
			    if(month.length()==1)
			    {
				    month = "0" + month;
			    }
			
			 System.out.println(application.getRealPath("/report/"));
			    
			File file_month_year = new File(application.getRealPath("/report/"+month+"_"+year_temp));
		    if (!file_month_year.exists()) {
	            if (file_month_year.mkdir()) {
	                System.out.println("file_month_year Created!");
	            } else {
	                System.out.println("Failed to create file file_month_year");
	            }
	        }
			File file_msr_inv = new File(application.getRealPath("/report/"+month+"_"+year_temp+"/monthly_summary_report/"));
			    if (!file_msr_inv.exists()) {
		            if (file_msr_inv.mkdir()) {
		                System.out.println("Folder Summary Monthly Report   Created!");
		            } else {
		                System.out.println("Failed to create file credit_inv");
		            }
		        }
			    

	
			
			   
			String fileName = application.getRealPath("/report/"+month+"_"+year_temp+"/monthly_summary_report/"+"credit_report_"+form_of_report+".xlsx");
	
	
			XSSFWorkbook workbook = new XSSFWorkbook(); 

		    XSSFSheet page1 = workbook.createSheet("Page1");
		    
			
		    				  
		
		    	workbook.getSheetAt(0).setColumnWidth(0,pixel2WidthUnits((short)(120)));//A
		    	workbook.getSheetAt(0).setColumnWidth(1,pixel2WidthUnits((short)(90)));//B
		    	workbook.getSheetAt(0).setColumnWidth(2,pixel2WidthUnits((short)(260)));//C
		    	workbook.getSheetAt(0).setColumnWidth(3,pixel2WidthUnits((short)(300)));//D
		    	workbook.getSheetAt(0).setColumnWidth(4,pixel2WidthUnits((short)(100)));//E
		    	workbook.getSheetAt(0).setColumnWidth(5,pixel2WidthUnits((short)(70)));//F
		    	workbook.getSheetAt(0).setColumnWidth(6,pixel2WidthUnits((short)(80)));//G
		    	workbook.getSheetAt(0).setColumnWidth(7,pixel2WidthUnits((short)(80)));//H
		    	workbook.getSheetAt(0).setColumnWidth(8,pixel2WidthUnits((short)(80)));//I
		    	workbook.getSheetAt(0).setColumnWidth(9,pixel2WidthUnits((short)(80)));//J
		    	workbook.getSheetAt(0).setColumnWidth(10,pixel2WidthUnits((short)(80)));//K
		    	workbook.getSheetAt(0).setColumnWidth(11,pixel2WidthUnits((short)(80)));//L
		    	workbook.getSheetAt(0).setColumnWidth(12,pixel2WidthUnits((short)(80)));//M
		    	workbook.getSheetAt(0).setColumnWidth(13,pixel2WidthUnits((short)(100)));//N
		    	workbook.getSheetAt(0).setColumnWidth(14,pixel2WidthUnits((short)(100)));//O
		    	workbook.getSheetAt(0).setColumnWidth(15,pixel2WidthUnits((short)(120)));//P
		    	workbook.getSheetAt(0).setColumnWidth(16,pixel2WidthUnits((short)(80)));//Q
		    	workbook.getSheetAt(0).setColumnWidth(17,pixel2WidthUnits((short)(100)));//R
		    	workbook.getSheetAt(0).setColumnWidth(18,pixel2WidthUnits((short)(200)));//S

		   
		    /*
	
		    	workbook.getSheetAt(y).setMargin(Sheet.TopMargin, 0.472);
		    	workbook.getSheetAt(y).setMargin(Sheet.HeaderMargin, 0.315);
		    	workbook.getSheetAt(y).setMargin(Sheet.LeftMargin, 0.472);
		    	workbook.getSheetAt(y).setMargin(Sheet.RightMargin, 0.078);
		    	workbook.getSheetAt(y).setMargin(Sheet.BottomMargin, 0.748);
		    	workbook.getSheetAt(y).setMargin(Sheet.FooterMargin, 0.04);
		    */
		    
	
	
			XSSFCellStyle style_arial_14 = CreateStyle(workbook,"Arial",14,false);
			XSSFCellStyle style_arial_13 = CreateStyle(workbook,"Arial",13,false);
			XSSFCellStyle style_arial_11 = CreateStyle(workbook,"Arial",11,false);
			XSSFCellStyle style_arial_11_bold = CreateStyle(workbook,"Arial",11,true);
			XSSFCellStyle style_arial_10 = CreateStyle(workbook,"Arial",10,false);
			XSSFCellStyle style_arial_9 = CreateStyle(workbook,"Arial",9,false);	
			XSSFCellStyle style_arial_8 = CreateStyle(workbook,"Arial",8,false);
			
			
			XSSFRow row_1 = page1.createRow((short)0);
			row_1.createCell(0).setCellValue("Delivery Date");
			row_1.createCell(1).setCellValue("Invoice No.");
			row_1.createCell(2).setCellValue("Customer Name");
			row_1.createCell(3).setCellValue("Product Name");
			row_1.createCell(4).setCellValue("Cost/Unit");
			row_1.createCell(5).setCellValue("Price/Unit");
			row_1.createCell(6).setCellValue("Profit/Unit");
			row_1.createCell(7).setCellValue("Profit(%)");
			row_1.createCell(8).setCellValue("Quantity");
			row_1.createCell(9).setCellValue("Unit");
			row_1.createCell(10).setCellValue("Sum Cost");
			row_1.createCell(11).setCellValue("Sum");
			row_1.createCell(12).setCellValue("Sum Profit");
			row_1.createCell(13).setCellValue("Total Sum Cost");
			row_1.createCell(14).setCellValue("Total Sum");
			row_1.createCell(15).setCellValue("Total Sum Profit");
			row_1.createCell(16).setCellValue("Avg Profit %");
			row_1.createCell(17).setCellValue("Total Vat (7%)");
			row_1.createCell(18).setCellValue("Total Inc Vat");

			
			
			CellUtil.setAlignment(row_1.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(1), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(2), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(3), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(4), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(5), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(6), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(7), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(8), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(9), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(10), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(11), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(12), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(13), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(14), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(15), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(16), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(17), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(18), workbook, CellStyle.ALIGN_CENTER);

			
	
			String flag_order_id="";
			
			int row_num = 1;
			for(int c =0 ; c<order_detail_list.size();c++)
			{
				XSSFRow temp_row = page1.createRow((short)row_num+1);
			   System.out.println("flag_order_id:"+flag_order_id+","+order_detail_list.get(c).getOrderId());	
			
				if(flag_order_id.equals(order_detail_list.get(c).getOrderId()))
				{
			        System.out.println("case A");
					 
					temp_row.createCell(0).setCellValue("-");
					temp_row.createCell(1).setCellValue("-");
					temp_row.createCell(2).setCellValue("-");
					temp_row.createCell(3).setCellValue(order_detail_list.get(c).getAdtDescription());
					
					if((order_detail_list.get(c).getCostPerUnit().equals("-")))
					{
						temp_row.createCell(4).setCellValue("-");
						System.out.println("set cell 4 to -");
					}else{
						temp_row.createCell(4).setCellValue(Double.parseDouble(order_detail_list.get(c).getCostPerUnit()));
						System.out.println("set cell 4 to x");
					}
					
					if((order_detail_list.get(c).getPrice().equals("-")))
					{
						temp_row.createCell(5).setCellValue("-");
						System.out.println("set cell 5 to -");
					}else{
						temp_row.createCell(5).setCellValue(Double.parseDouble(order_detail_list.get(c).getPrice()));
						System.out.println("set cell 5 to x");
					}
					
					if((order_detail_list.get(c).getProfitPerUnit().equals("-")))
					
					{
						temp_row.createCell(6).setCellValue("-");
						System.out.println("set cell 6 to -");
					}else{
						
						temp_row.createCell(6).setCellValue(Double.parseDouble(order_detail_list.get(c).getProfitPerUnit()));
						System.out.println("set cell 6 to x");
					}


					if((order_detail_list.get(c).getProfitPercentage().equals("-")))
					{
						
						temp_row.createCell(7).setCellValue("-");
						System.out.println("set cell 7 to -");
					}else{
				
						temp_row.createCell(7).setCellValue(Double.parseDouble(order_detail_list.get(c).getProfitPercentage())+" %");
						System.out.println("set cell 7 to x");
					}



					temp_row.createCell(8).setCellValue(Double.parseDouble(order_detail_list.get(c).getQuantity()));
					temp_row.createCell(9).setCellValue(order_detail_list.get(c).getProductUnit());
		
					
					if((order_detail_list.get(c).getSumCost().equals("-")))
					{
						temp_row.createCell(10).setCellValue("-");
						System.out.println("set cell 10 to -");
					}else{
						
						temp_row.createCell(10).setCellValue(Double.parseDouble(order_detail_list.get(c).getSumCost()));
						System.out.println("set cell 10 to x");
					}

					if((order_detail_list.get(c).getSum().equals("-")))
					{
						temp_row.createCell(11).setCellValue("-");
						System.out.println("set cell 11 to -");
					}else{
						
						temp_row.createCell(11).setCellValue(Double.parseDouble(order_detail_list.get(c).getSum()));
						System.out.println("set cell 11 to x");
					}

					if((order_detail_list.get(c).getSumProfit().equals("-")))
					{
						temp_row.createCell(12).setCellValue("-");
						System.out.println("set cell 12 to -");
					}else{
						
						temp_row.createCell(12).setCellValue(Double.parseDouble(order_detail_list.get(c).getSumProfit()));
						System.out.println("set cell 12 to x");
					}

					

				
					temp_row.createCell(13).setCellValue("-"); // total sum cost
					temp_row.createCell(14).setCellValue("-"); // total sum 
					temp_row.createCell(15).setCellValue("-"); // total sum profit
					temp_row.createCell(16).setCellValue("-"); // avg profit percentage
					temp_row.createCell(17).setCellValue("-"); // avg profit percentage
					temp_row.createCell(18).setCellValue("-"); // avg profit percentage
					
					


					CellUtil.setAlignment(temp_row.getCell(0), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(1), workbook, CellStyle.ALIGN_CENTER);

					CellUtil.setAlignment(temp_row.getCell(4), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(5), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(6), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(7), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(8), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(9), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(10), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(11), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(12), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(13), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(14), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(15), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(16), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(17), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(18), workbook, CellStyle.ALIGN_CENTER);
					
					

					
				}else{
					
					System.out.println("case B");

					flag_order_id = order_detail_list.get(c).getOrderId();

					temp_row.createCell(0).setCellValue(order_detail_list.get(c).getDeliveryDate());

					temp_row.createCell(1).setCellValue(order_detail_list.get(c).getInvoiceNo());

					temp_row.createCell(2).setCellValue(order_detail_list.get(c).getCustomerName());

					temp_row.createCell(3).setCellValue(order_detail_list.get(c).getAdtDescription());
				
					if((order_detail_list.get(c).getCostPerUnit().equals("-")))
					{
						temp_row.createCell(4).setCellValue("-");
						System.out.println("set cell 4 to -");
					}else{
						temp_row.createCell(4).setCellValue(Double.parseDouble(order_detail_list.get(c).getCostPerUnit()));
						System.out.println("set cell 4 to x");
					}
					
					if((order_detail_list.get(c).getPrice().equals("-")))
					{
						temp_row.createCell(5).setCellValue("-");
						System.out.println("set cell 5 to -");
					}else{
						temp_row.createCell(5).setCellValue(Double.parseDouble(order_detail_list.get(c).getPrice()));
						System.out.println("set cell 5 to x");
					}
					
					if((order_detail_list.get(c).getProfitPerUnit().equals("-")))
					
					{
						temp_row.createCell(6).setCellValue("-");
						System.out.println("set cell 6 to -");
					}else{
						
						temp_row.createCell(6).setCellValue(Double.parseDouble(order_detail_list.get(c).getProfitPerUnit()));
						System.out.println("set cell 6 to x");
					}


					if((order_detail_list.get(c).getProfitPercentage().equals("-")))
					{
						
						temp_row.createCell(7).setCellValue("-");
						System.out.println("set cell 7 to -");
					}else{
				
						temp_row.createCell(7).setCellValue(Double.parseDouble(order_detail_list.get(c).getProfitPercentage())+" %");
						System.out.println("set cell 7 to x");
					}

					temp_row.createCell(8).setCellValue(Double.parseDouble(order_detail_list.get(c).getQuantity()));

					temp_row.createCell(9).setCellValue(order_detail_list.get(c).getProductUnit());
		
					

					
					if((order_detail_list.get(c).getSumCost().equals("-")))
					{
						temp_row.createCell(10).setCellValue("-");
						System.out.println("set cell 10 to -");
					}else{
						
						temp_row.createCell(10).setCellValue(Double.parseDouble(order_detail_list.get(c).getSumCost()));
						System.out.println("set cell 10 to x");
					}

					if((order_detail_list.get(c).getSum().equals("-")))
					{
						temp_row.createCell(11).setCellValue("-");
						System.out.println("set cell 11 to -");
					}else{
						
						temp_row.createCell(11).setCellValue(Double.parseDouble(order_detail_list.get(c).getSum()));
						System.out.println("set cell 11 to x");
					}

					if((order_detail_list.get(c).getSumProfit().equals("-")))
					{
						temp_row.createCell(12).setCellValue("-");
						System.out.println("set cell 12 to -");
					}else{
						
						temp_row.createCell(12).setCellValue(Double.parseDouble(order_detail_list.get(c).getSumProfit()));
						System.out.println("set cell 12 to x");
					}

					
					
					if((order_detail_list.get(c).getOrderMainTotalCost().equals("-")))
					{
						temp_row.createCell(13).setCellValue("-");
						System.out.println("set cell 13 to -");
					}else{
						
						temp_row.createCell(13).setCellValue(Double.parseDouble(order_detail_list.get(c).getOrderMainTotalCost()));
						System.out.println("set cell 13 to x");
					}

					if((order_detail_list.get(c).getOrderMainTotalValue().equals("-")))
					{
						temp_row.createCell(14).setCellValue("-");
						System.out.println("set cell 14 to -");
					}else{
						
						temp_row.createCell(14).setCellValue(Double.parseDouble(order_detail_list.get(c).getOrderMainTotalValue()));
						System.out.println("set cell 14 to x");
					}

	
					if((order_detail_list.get(c).getOrderMainTotalProfit().equals("-")))
					{
						temp_row.createCell(15).setCellValue("-");
						System.out.println("set cell 15 to -");
					}else{
						
						temp_row.createCell(15).setCellValue(Double.parseDouble(order_detail_list.get(c).getOrderMainTotalProfit()));
						System.out.println("set cell 15 to x");
					}
					/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					if((order_detail_list.get(c).getOrderMainAvgProfitPercentage().equals("-")))
					{
						temp_row.createCell(16).setCellValue("-");
						System.out.println("set cell 16 to -");
					}else{
						
						temp_row.createCell(16).setCellValue(Double.parseDouble(order_detail_list.get(c).getOrderMainAvgProfitPercentage())+" %");
						System.out.println("set cell 16 to :"+Double.parseDouble(order_detail_list.get(c).getOrderMainAvgProfitPercentage()));
					}
					///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
					if((order_detail_list.get(c).getOrderMainTotalVat().equals("-")))
					{
						temp_row.createCell(17).setCellValue("-");
						System.out.println("set cell 17 to -");
					}else{
						
						temp_row.createCell(17).setCellValue(Double.parseDouble(order_detail_list.get(c).getOrderMainTotalVat()));
						System.out.println("set cell 17 to x");
					}

				
					if((order_detail_list.get(c).getOrderMainTotalIncVat().equals("-")))
					{
						temp_row.createCell(18).setCellValue("-");
						System.out.println("set cell 18 to -");
					}else{
						
						temp_row.createCell(18).setCellValue(Double.parseDouble(order_detail_list.get(c).getOrderMainTotalIncVat()));
						System.out.println("set cell 18 to x");
					}


					System.out.println("Check");
				
					CellUtil.setAlignment(temp_row.getCell(0), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(1), workbook, CellStyle.ALIGN_CENTER);

					CellUtil.setAlignment(temp_row.getCell(4), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(5), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(6), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(7), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(8), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(9), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(10), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(11), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(12), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(13), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(14), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(15), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(16), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(17), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(18), workbook, CellStyle.ALIGN_CENTER);
					
				

				}
			
				
				row_num++;
			}
		
			
			

			
			
			
			
			
			int count_detail = 0;
			System.out.println("Starting Write Bill detail to xls file");
	
     
			
			workbook.write(new FileOutputStream(fileName));
			
			
			String file_path = fileName;
		
			String pattern = Pattern.quote(System.getProperty("file.separator"));
			String[] parts = file_path.split(pattern);
			int temp = parts.length;
			String inv_file_name = parts[temp-1];
			
			String output = inv_file_name+"&"+file_path;
			
	
			
		    
			out.print(output);
			
	} catch (Exception e) {
		e.printStackTrace();
		connect.close();
		out.print("fail");
	}
	
	///////////////////////////////////////////////////////End Writing Excel Process/////////////////////////////////////////////////////////
	 
	connect.close();
%>
