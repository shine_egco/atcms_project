<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="../bower_components/sidebar/sidebar_style_3.css" rel="stylesheet" type="text/css">
   
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

 <script>
 		
	
 
 
</script>
    
    

</head>

<body>

    <div id="wrapper">
	<nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>
                <div class="sidebar-header">
                    <h3>Bored Sidebar</h3>
                </div>
                
    
        		<div id="includedSidebar"></div>
        		
        	</nav>



        <!-- Navigation -->
       

        <div id="content" class="form-control">
        
        	   <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-justify"></i>
                                <span></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Page</a></li>
                       
                            </ul>
                        </div>
                    </div>
                </nav>
                
             <div class="row">
	                <div class="col-lg-12">
	                    <h1 class="page-header">Other Products</h1>
	                </div>
                <!-- /.col-lg-12 -->
             </div>
			
             <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		                     <div class="modal-dialog">
		                         <div class="modal-content">
		                             <div class="modal-header">
		                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                                 <h4 class="modal-title" id="myModalLabel">Modal title</h4>
		                             </div>
		                             <div class="modal-body">
		                               	<input id="product_id" name="product_id" class="form-control" disabled >
		                               	<br>
		                               	<input id="name_th" name = "name_th" class="form-control" value=""   >
		                               	<br>
		                               	<input id="name_en" name = "name_en" class="form-control" value=""   >
		                               	<br>
		                                <input id="unit_th" name = "unit_th" class="form-control" value=""   >
		                               	<br>
		                               	<input id="unit_en" name = "unit_en" class="form-control" value=""   >
		                               	<br>
		                               
		                               
		                               
		                             </div>
		                             <div class="modal-footer">
		                                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                                 <button type="button" id="submit_change_pro_btn" onclick="submit_change_product()" name="submit_change_product" class="btn btn-primary">Save changes</button>
		                             </div>
		                         </div>
		                         <!-- /.modal-content -->
		                     </div>
		                     <!-- /.modal-dialog -->
		     </div>
		  	 
                                        	
			
             <div class="row">
                <div class="col-lg-12">
                
                	<div class="container">
					    <div class="row">    
					        <div class="col-xs-8 col-xs-offset-2">
							    <div class="input-group">
					                <div class="input-group-btn search-panel">
					                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					                    	<span id="search_concept">Filter by</span> <span class="caret"></span>
					                    </button>
					                    <ul class="dropdown-menu" role="menu">
					                      <li><a href="">Contains</a></li>
					                  
					                    </ul>
					                </div>
					             
					                <input type="text" id="s_param" class="form-control" placeholder="Search term...">
					                <span class="input-group-btn">
					                    <button class="btn btn-default" id="but_send" onclick="fetch_product_to_table()" type="button"><span class="glyphicon glyphicon-search" ></span></button>
					                </span>
					            </div>
					        </div>
						</div>
					</div>
                	<br>
                	
                    <div class="panel panel-default">
                      <div class="panel-heading">
                           	Product List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        	<div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                      		<th>Product ID</th>   
                                            <th>Name TH</th>
                                            <th>Name EN</th>
                                            <th>Category</th>
                                            <th style="width:1%"></th>
                                          
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        
                           
                                       
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        
        

        
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
            <!-- jQuery Custom Scroller CDN -->
     <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
    	

    	$("#includedSidebar").load("sidebar.jsp"); 
    	
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').fadeOut();
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        
        
    	
        $('#dataTables-example').DataTable({
                responsive: true
        });
		
       // fetch_product_to_table();
       
    });
    
    function addEventListener(){
    	
    	var input = document.getElementById("s_param");

    	// Execute a function when the user releases a key on the keyboard
    	input.addEventListener("keyup", function(event) {
    	  // Number 13 is the "Enter" key on the keyboard
    	  if (event.keyCode === 13) {
    	    // Cancel the default action, if needed
    	   // event.preventDefault();
    	    // Trigger the button element with a click
    	    document.getElementById("but_send").click();
    	  }
    	});
    	
    }
    

	 function fetch_product_to_table(){
		
		  var xmlhttp;
		  
		  var s_param = document.getElementById("s_param").value;
		  
		  alert(s_param);
		  
		  var  table = $('#dataTables-example').DataTable();
	       table.clear();
			
		  
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
				//	alert("I'm Back");//////////////////
					
			
					var jsonObj = JSON.parse(xmlhttp.responseText);
				
					//alert("I'm Back");
					
					if(jsonObj.length == 0) {
					//	massage();
						alert("Error Occer Can't get all customer list");
					}
					else{
	
						for(i in jsonObj) {
							$('#dataTables-example').DataTable().row.add([
                              '<tr><td>'+jsonObj[i].productID+'</td>'
	                           ,'<td>'+jsonObj[i].nameTH+'</td>'
	                           ,'<td>'+jsonObj[i].nameEN+'</td>'
	                           ,'<td>'+jsonObj[i].category+'</td>'
	                           ,'<td><button id = "'+jsonObj[i].productID+'" type="button" class="btn btn-info btn-circle btn-md" onclick = "show_product_detail(this.id)" data-target="#myModal">'
	                           +'<i class="glyphicon glyphicon-search"></i></button> </td></tr>'	                               
	                         ]).draw();
						}
			
					}
					
					
					//System.out.println("temp_x:"+temp_x);	
				
				
				}// end if check state
			}// end function
			
	
			xmlhttp.open("POST", "get_product_by_keyword_background.jsp?keyword="+s_param, true);
			xmlhttp.send();
	
		
	}
	 
	function show_product_detail(id){
		
		//alert(id);
		
		sessionStorage.setItem("other_product_id_for_get_detail", id); 
		
		window.open("report_other_product_detail.jsp");

	}	
	
    
    </script>

</body>

</html>
