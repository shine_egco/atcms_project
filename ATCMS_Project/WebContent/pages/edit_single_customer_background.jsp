<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>


<% 

	System.out.println("Start edit_single_company_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}


     /////////////////Data Parameter////////////////
     String code_name = "";
     String company_id = request.getParameter("company_id");
     String name_th = request.getParameter("name_th");
     String name_en = request.getParameter("name_en") ; 
     String address = "" ;
     String address_th = request.getParameter("address_th");
     String address_en = request.getParameter("address_en");
     String tax_id = request.getParameter("tax_id") ; 
     String credit_str = request.getParameter("credit");
     String email = request.getParameter("email");
     int credit =0 ;
     
     System.out.println("company_id:"+company_id);
     System.out.println("name_th:"+name_th);
     System.out.println("name_en:"+name_en);
     System.out.println("address_th:"+address_th);
     System.out.println("address_en:"+address_en);
     System.out.println("tax_id:"+tax_id);
     System.out.println("credit:"+credit_str);
     /////////////////////////////////////////////////

      String type = "customer";
      if(credit_str.equals(""))
      {
    	 credit=0;
      }else{
    	  credit = Integer.parseInt(request.getParameter("credit"));
      }
      try{
    	 
    	 String checking_sql = "SELECT * FROM `company` WHERE company_id='"+company_id+"'";
    	 ResultSet rs_check_company_id =  connect.createStatement().executeQuery(checking_sql);
    	 rs_check_company_id.last();
    		if(rs_check_company_id.getRow() == 1) {
    			
    			 System.out.println("Start update to database");
    	    	 String sql_update = "UPDATE `company` SET "+
    	    	 							"`name_th`='"+name_th+"'"+
    	    	 			                ",`name_en`='"+name_en+"'"+
    	    	 							",`email`='"+email+"'"+
    	    	 			                ",`credit`='"+credit+"'"+
    	    	 							",`tax_id`='"+tax_id+"'"+
    	    	 			                ",`address_en`='"+address_en+"'"+
    	    	 							",`address_th`='"+address_th+"'"+
    	    	 			   " WHERE BINARY company_id='"+company_id+"'";
    	    	 
    	    	 System.out.println("sql_update:"+sql_update);
    	    	 connect.createStatement().executeUpdate(sql_update);
    			
    			
    			
    		}
    		else{
    			//not found company_id 
    			//error occer
    			throw new Exception();
    		}
    	  
    	  
    	  /*
    	  
    	
    	 			                
    	  
    	  
    	  */
    	  
    	  /*
	      System.out.println("Start Import to Database ");
	      String sql = " INSERT INTO `company`("+
	      								    "`name_th` "+
	      									", `name_en` "+
	      								    ", `address` "+
	      									", `type` "+
	      								    ", `email` "+
	      									", `credit`"+
	      								    ", `tax_id`"+
	      									", `address_en`"+
	      								    ", `address_th`"+
	      								    ", `code_name` "+
	      								    ", `company_id`)"+
	      			 " VALUES ("+
	      		              "'"+name_th+"'" +
	      					  ",'"+name_en +"'"+
	      					  ",'"+address+"'"+
	      					  ",'"+type+"'"+
	      					  ","+null +
	      					  ","+credit + 
	      					  ",'"+tax_id+"'"+
	      					  ",'"+address_en+"'"+
	      					  ",'"+address_th+"'"+
	      					  ",'"+code_name+"'"+
	      					  ",'"+company_id+"')";
	      
	
	      		
			System.out.println("sql:"+sql);		
			PreparedStatement pstmt = connect.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);  
			pstmt.executeUpdate();  
			ResultSet keys = pstmt.getGeneratedKeys();    
			keys.next();  
			int key = keys.getInt(1);
			System.out.println("Adding Success with company_id:"+key);
			*/
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  connect.close();
    	  out.print("fail");
      }
     
    System.out.println("SUCCESS");
    out.print("success");
	connect.close();
%>
