<%@page import="org.apache.poi.xwpf.usermodel.TextAlignment"%>
<%@page import="org.apache.poi.xwpf.usermodel.Borders"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "com.atcms.*" %>
<%@ page import = "java.util.List" %>
<%@ page import = "java.util.ArrayList"%>
<%@ page import = "java.util.Properties" %>
<%@ page import = "javax.servlet.ServletContext" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.util.Date" %>
<%@ page import="java.math.BigInteger" %>
<%@ page import="java.io.FileOutputStream" %>
<%@ page import="org.apache.poi.xssf.usermodel.*" %>
<%@ page import="org.apache.poi.hssf.usermodel.HSSFCellStyle" %>
<%@ page import="org.apache.poi.hssf.util.*" %>
<%@ page import="org.apache.poi.ss.util.CellUtil" %>
<%@ page import="org.apache.poi.ss.usermodel.*" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>
<%@ page import = "java.text.DecimalFormat" %>




<%!
		public static final short EXCEL_COLUMN_WIDTH_FACTOR = 256; 
	    public static final short EXCEL_ROW_HEIGHT_FACTOR = 20; 
	    public static final int UNIT_OFFSET_LENGTH = 7; 
	    public static final int[] UNIT_OFFSET_MAP = new int[] { 0, 36, 73, 109, 146, 182, 219 };
	    
	    public static RichTextString createRichTextString (XSSFWorkbook workbook_in,String text,String font_name ,int font_size)
	    {
	    	 RichTextString richString = new XSSFRichTextString(text);
	    	 XSSFFont font = workbook_in.createFont();
	    	 font.setFontName(font_name);
			 font.setFontHeightInPoints((short)font_size);
			 richString.applyFont(font);
			 return richString;
	    }
	    
	    
		public static XSSFCellStyle CreateStyle(XSSFWorkbook workbook_in
							 				   ,String font_name_in 
							                   , int font_size_in
							                   , boolean isBold)
		{
				XSSFFont font = workbook_in.createFont();
				font.setFontName(font_name_in);
				font.setFontHeightInPoints((short)font_size_in);
				font.setBold(isBold);
				XSSFCellStyle style = workbook_in.createCellStyle();
				style.setFont(font);
				return style ;
		}
				
		public static XSSFCellStyle CreateCellBorder(XSSFWorkbook workbook_in 
										, boolean top 
										, boolean bot 
										, boolean left 
										, boolean right)
		{
				
				XSSFCellStyle style = workbook_in.createCellStyle();
				if(top){  style.setBorderTop(HSSFCellStyle.BORDER_THIN); }
				if(bot){ style.setBorderBottom(HSSFCellStyle.BORDER_THIN); }
				if(left){ style.setBorderLeft(HSSFCellStyle.BORDER_THIN); }
				if(right){ style.setBorderRight(HSSFCellStyle.BORDER_THIN); }
				
				return style ;
		}
		public static void DrawListCellBorder (XSSFWorkbook workbook , XSSFRow row_in){
			
			XSSFCellStyle style_border_top_left = CreateCellBorder(workbook,true,false,true,false);
			XSSFCellStyle style_border_top_right = CreateCellBorder(workbook,true,false,false,true);
			XSSFCellStyle style_border_top_left_right = CreateCellBorder(workbook,true,false,true,true);
			XSSFCellStyle style_border_bot_left = CreateCellBorder(workbook,false,true,true,false);
			XSSFCellStyle style_border_bot_right = CreateCellBorder(workbook,false,true,false,true);
			XSSFCellStyle style_border_bot_left_right = CreateCellBorder(workbook,false,true,true,true);
			XSSFCellStyle style_border_top = CreateCellBorder(workbook,true,false,false,false);
			XSSFCellStyle style_border_bot = CreateCellBorder(workbook,false,true,false,false);
			XSSFCellStyle style_border_left = CreateCellBorder(workbook,false,false,true,false);
			XSSFCellStyle style_border_right = CreateCellBorder(workbook,false,false,false,true);
			XSSFCellStyle style_border_left_right = CreateCellBorder(workbook,false,false,true,true);
			XSSFCellStyle style_border_full = CreateCellBorder(workbook,true,true,true,true);
			
			row_in.createCell(0);
			row_in.createCell(1);
			row_in.createCell(11);
			row_in.createCell(12);
			row_in.getCell(0).setCellStyle(style_border_left_right);
			row_in.getCell(1).setCellStyle(style_border_left_right);
			row_in.getCell(11).setCellStyle(style_border_left_right);
			row_in.getCell(12).setCellStyle(style_border_left_right);

		}
	    public static int widthUnits2Pixel(short widthUnits) {
	    	
	        int pixels = (widthUnits / EXCEL_COLUMN_WIDTH_FACTOR) * UNIT_OFFSET_LENGTH; 
	        int offsetWidthUnits = widthUnits % EXCEL_COLUMN_WIDTH_FACTOR; 
	        pixels += Math.floor((float) offsetWidthUnits / ((float) EXCEL_COLUMN_WIDTH_FACTOR / UNIT_OFFSET_LENGTH));   
	        return pixels; 
	    }
	    public static short pixel2WidthUnits(int pxs) {
	        short widthUnits = (short) (EXCEL_COLUMN_WIDTH_FACTOR * (pxs / UNIT_OFFSET_LENGTH)); 
	        widthUnits += UNIT_OFFSET_MAP[(pxs % UNIT_OFFSET_LENGTH)];  
	        return widthUnits; 
	    } 
	    


%>

<% 

	System.out.println("Start generate_external_cash_invoice_monthly_summary_report_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
	
	/////////////////////////Start Get Data Process /////////////////////////////
	
	String month = request.getParameter("month");
	String year = request.getParameter("year");
	String form_of_report = request.getParameter("form_of_report");
	
	
	String order_id ="";
	String type ="";
	String ponum ="";
	String customer_name="";
	String customer_address_full="";  
	String total_value="";
	String total_vat="";
	String total_inc_vat = "";
	String inv_no="";
	String due_date="";
	String delivery_date="";
	String credit="";
	String tax_id = "";
	String address_line0 = "";
	String address_line1 = "";
	String status = "";
	String inv_no_temp ="";
	
	
	List<CashBillDetail> cb_detail_list = new ArrayList<CashBillDetail>();

	

	////////////////////////////////////////////////////////////////////////////
	
	String cb_detail_query = " SELECT * "+
												"  FROM cash_bill_detail "+
												" JOIN cash_bill_main "+
												" ON  cash_bill_detail.cash_bill_id = cash_bill_main.cash_bill_id "+
												" JOIN company "+
												" ON  cash_bill_main.customer_id = company.company_id "	+											
											    " WHERE  MONTH(cash_bill_main.bill_date) = '"+month+"' "+
												" AND YEAR(cash_bill_main.bill_date) ='"+year+"' "+
											    " ORDER BY cash_bill_main.inv_no ASC , cash_bill_detail.index ASC ";

	ResultSet rs_cb_detail = connect.createStatement().executeQuery(cb_detail_query);
	System.out.println("cash_bill_detail_query:"+cb_detail_query);
	int i=0;
	while(rs_cb_detail.next())
    {
			CashBillDetail temp_cb_detail = new CashBillDetail();
			temp_cb_detail.setAdtDescription(rs_cb_detail.getString("adt_description"));
			temp_cb_detail.setCashBillId(rs_cb_detail.getString("cash_bill_id"));
			temp_cb_detail.setInvoiceNo(rs_cb_detail.getString("inv_no"));
			if(rs_cb_detail.getString("name_th").equals("-"))
			{
				temp_cb_detail.setCustomerName(rs_cb_detail.getString("name_en"));
			}else{
				temp_cb_detail.setCustomerName(rs_cb_detail.getString("name_th"));
			}
			temp_cb_detail.setCashBillIMainTotalIncVat(rs_cb_detail.getString("total_inc_vat"));
			
			temp_cb_detail.setProductUnit(rs_cb_detail.getString("unit"));
			temp_cb_detail.setBillDate(rs_cb_detail.getString("bill_date"));
			temp_cb_detail.setPrice(rs_cb_detail.getString("price"));
			temp_cb_detail.setQuantity(rs_cb_detail.getString("quantity"));
			temp_cb_detail.setSum(rs_cb_detail.getString("sum"));
			temp_cb_detail.setCashBillMainTotalValue(rs_cb_detail.getString("total_value"));
			temp_cb_detail.setCashBillMainTotalVat(rs_cb_detail.getString("total_vat"));
			temp_cb_detail.setCashBillIMainTotalIncVat(rs_cb_detail.getString("total_inc_vat"));
			
			cb_detail_list.add(temp_cb_detail);
			
		//	System.out.println(i+":"+temp_order_detail.getAdtDescription());
			i++;
    }
	
	
	//System.out.println("order_detail_list.size:"+order_detail_list.size());
	
	//////////////////////////End Get Data Process /////////////////////////////
	
	
	/////////////////////////Start Writing Excel Process//////////////////////////
	
	

	try {		
		
			inv_no_temp = inv_no.replace("/","_");
			Date curr_date = new Date();
	//		String curr_year = curr_date.getYear()+2443+"";
	//		String curr_month = curr_date.getMonth()+1+"";
			String year_temp = (Integer.parseInt(year)+543)+"";
			
			    if(month.length()==1)
			    {
				    month = "0" + month;
			    }
			
			 System.out.println(application.getRealPath("/report/"));
			    
			File file_month_year = new File(application.getRealPath("/report/"+month+"_"+year_temp));
		    if (!file_month_year.exists()) {
	            if (file_month_year.mkdir()) {
	                System.out.println("file_month_year Created!");
	            } else {
	                System.out.println("Failed to create file file_month_year");
	            }
	        }
			File file_msr_inv = new File(application.getRealPath("/report/"+month+"_"+year_temp+"/monthly_summary_report/"));
			    if (!file_msr_inv.exists()) {
		            if (file_msr_inv.mkdir()) {
		                System.out.println("Folder Summary Monthly Report   Created!");
		            } else {
		                System.out.println("Failed to create file credit_inv");
		            }
		        }
			    

	
			
			   
			String fileName = application.getRealPath("/report/"+month+"_"+year_temp+"/monthly_summary_report/"+"cash_report_"+form_of_report+".xlsx");
	
	
			XSSFWorkbook workbook = new XSSFWorkbook(); 

		    XSSFSheet page1 = workbook.createSheet("Page1");
		    
			
		    				  
		
		    	workbook.getSheetAt(0).setColumnWidth(0,pixel2WidthUnits((short)(120)));//A
		    	workbook.getSheetAt(0).setColumnWidth(1,pixel2WidthUnits((short)(90)));//B
		    	workbook.getSheetAt(0).setColumnWidth(2,pixel2WidthUnits((short)(260)));//C
		    	workbook.getSheetAt(0).setColumnWidth(3,pixel2WidthUnits((short)(300)));//D
		    	workbook.getSheetAt(0).setColumnWidth(4,pixel2WidthUnits((short)(100)));//E
		    	workbook.getSheetAt(0).setColumnWidth(5,pixel2WidthUnits((short)(70)));//F
		    	workbook.getSheetAt(0).setColumnWidth(6,pixel2WidthUnits((short)(80)));//G
		    	workbook.getSheetAt(0).setColumnWidth(7,pixel2WidthUnits((short)(80)));//H
		    	workbook.getSheetAt(0).setColumnWidth(8,pixel2WidthUnits((short)(80)));//I
		    	workbook.getSheetAt(0).setColumnWidth(9,pixel2WidthUnits((short)(200)));//J
		    	workbook.getSheetAt(0).setColumnWidth(10,pixel2WidthUnits((short)(200)));//K
		    	workbook.getSheetAt(0).setColumnWidth(11,pixel2WidthUnits((short)(200)));//L


		   
		    /*
	
		    	workbook.getSheetAt(y).setMargin(Sheet.TopMargin, 0.472);
		    	workbook.getSheetAt(y).setMargin(Sheet.HeaderMargin, 0.315);
		    	workbook.getSheetAt(y).setMargin(Sheet.LeftMargin, 0.472);
		    	workbook.getSheetAt(y).setMargin(Sheet.RightMargin, 0.078);
		    	workbook.getSheetAt(y).setMargin(Sheet.BottomMargin, 0.748);
		    	workbook.getSheetAt(y).setMargin(Sheet.FooterMargin, 0.04);
		    */
		    
	
	
			XSSFCellStyle style_arial_14 = CreateStyle(workbook,"Arial",14,false);
			XSSFCellStyle style_arial_13 = CreateStyle(workbook,"Arial",13,false);
			XSSFCellStyle style_arial_11 = CreateStyle(workbook,"Arial",11,false);
			XSSFCellStyle style_arial_11_bold = CreateStyle(workbook,"Arial",11,true);
			XSSFCellStyle style_arial_10 = CreateStyle(workbook,"Arial",10,false);
			XSSFCellStyle style_arial_9 = CreateStyle(workbook,"Arial",9,false);	
			XSSFCellStyle style_arial_8 = CreateStyle(workbook,"Arial",8,false);
			
			
			XSSFRow row_1 = page1.createRow((short)0);
			row_1.createCell(0).setCellValue("Bill Date");
			row_1.createCell(1).setCellValue("Invoice No.");
			row_1.createCell(2).setCellValue("Customer Name");
			row_1.createCell(3).setCellValue("Product Name");
			row_1.createCell(4).setCellValue("Quantity");
			row_1.createCell(5).setCellValue("Unit");
			row_1.createCell(6).setCellValue("Unit Price");
			row_1.createCell(7).setCellValue("Sum");
			row_1.createCell(8).setCellValue("Total Value");
			row_1.createCell(9).setCellValue("Total VAT");
			row_1.createCell(10).setCellValue("Total Include VAT");


			
			
			CellUtil.setAlignment(row_1.getCell(0), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(1), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(2), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(3), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(4), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(5), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(6), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(7), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(8), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(9), workbook, CellStyle.ALIGN_CENTER);
			CellUtil.setAlignment(row_1.getCell(10), workbook, CellStyle.ALIGN_CENTER);
	
			String flag_order_id="";
			
			int row_num = 1;
			for(int c =0 ; c<cb_detail_list.size();c++)
			{
				XSSFRow temp_row = page1.createRow((short)row_num+1);
				
				if(flag_order_id.equals(cb_detail_list.get(c).getCashBillId()))
				{
					/*
				  System.out.println("case A");
					 
					temp_row.createCell(0).setCellValue("-");
					temp_row.createCell(1).setCellValue("-");
					temp_row.createCell(2).setCellValue("-");
					temp_row.createCell(3).setCellValue(order_detail_list.get(c).getAdtDescription());
					temp_row.createCell(4).setCellValue(Double.parseDouble(order_detail_list.get(c).getQuantity()));
					temp_row.createCell(5).setCellValue(order_detail_list.get(c).getProductUnit());
					temp_row.createCell(6).setCellValue(Double.parseDouble(order_detail_list.get(c).getPrice()));
					temp_row.createCell(7).setCellValue(Double.parseDouble(order_detail_list.get(c).getSum()));
					
					
					
					String total_grand = ((Double.parseDouble(order_detail_list.get(c).getSum())*0.07)*10 / 10)+"";
					System.out.println("total_grand:"+total_grand);
					String final_total_grand ="";
					
					if(total_grand.indexOf(".")>0){
						System.out.println("case A1");
						
						String [] parts = total_grand.split(Pattern.quote("."));

					 	final_total_grand = parts[0]+"."+(parts[1].substring(0,1));
				

					}else{
						System.out.println("case A2");
						final_total_grand = total_grand;
					}
					
					temp_row.createCell(8).setCellValue(Double.parseDouble(final_total_grand));
					temp_row.createCell(9).setCellValue("-");
					System.out.println("final_total_grand:"+final_total_grand);
					
					CellUtil.setAlignment(temp_row.getCell(0), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(1), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(2), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(4), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(5), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(6), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(7), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(8), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(9), workbook, CellStyle.ALIGN_CENTER);
					
				*/
					
				}else{
				//	System.out.println("case B");
					
					flag_order_id = cb_detail_list.get(c).getCashBillId();
					temp_row.createCell(0).setCellValue(cb_detail_list.get(c).getBillDate());
					temp_row.createCell(1).setCellValue(cb_detail_list.get(c).getInvNo());
					temp_row.createCell(2).setCellValue(cb_detail_list.get(c).getCustomerName());
					temp_row.createCell(3).setCellValue(cb_detail_list.get(c).getAdtDescription());
					temp_row.createCell(4).setCellValue(Double.parseDouble(cb_detail_list.get(c).getQuantity()));
					temp_row.createCell(5).setCellValue(cb_detail_list.get(c).getProductUnit());
					temp_row.createCell(6).setCellValue(Double.parseDouble(cb_detail_list.get(c).getPrice()));
					temp_row.createCell(7).setCellValue(Double.parseDouble(cb_detail_list.get(c).getSum()));
					
					temp_row.createCell(8).setCellValue(Double.parseDouble(cb_detail_list.get(c).getCashBillMainTotalValue()));
					temp_row.createCell(9).setCellValue(Double.parseDouble(cb_detail_list.get(c).getCashBillMainTotalVat()));
					temp_row.createCell(10).setCellValue(Double.parseDouble(cb_detail_list.get(c).getCashBillMainTotalIncVat()));
					
					
					String total_grand = ((Double.parseDouble(cb_detail_list.get(c).getSum())*0.07)*10 / 10)+"";
			//		System.out.println("total_grand:"+total_grand);
					String final_total_grand ="";
					
					if(total_grand.indexOf(".")>0){
						System.out.println("case B1");
						
						String [] parts = total_grand.split(Pattern.quote("."));

			
					 	final_total_grand = parts[0]+"."+(parts[1].substring(0,1));
				

					}else{
						System.out.println("case B2");
						final_total_grand = total_grand;
					}
					
		//			System.out.println("final_total_grand:"+final_total_grand);
			//		temp_row.createCell(8).setCellValue(Double.parseDouble(final_total_grand));
			//		temp_row.createCell(9).setCellValue(Double.parseDouble(order_detail_list.get(c).getOrderMainTotalIncVat()));
					
					
					System.out.println("Check");
					CellUtil.setAlignment(temp_row.getCell(4), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(5), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(6), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(7), workbook, CellStyle.ALIGN_CENTER);
					CellUtil.setAlignment(temp_row.getCell(8), workbook, CellStyle.ALIGN_CENTER);
		   		    CellUtil.setAlignment(temp_row.getCell(9), workbook, CellStyle.ALIGN_CENTER);
		   		    CellUtil.setAlignment(temp_row.getCell(10), workbook, CellStyle.ALIGN_CENTER);
		   		    
		   		 row_num++;

				}
			
				
				
			}
		
			
			

			
			
			
			
			
			int count_detail = 0;
			System.out.println("Starting Write Bill detail to xls file");
	
     
			
			workbook.write(new FileOutputStream(fileName));
			
			
			String file_path = fileName;
		
			String pattern = Pattern.quote(System.getProperty("file.separator"));
			String[] parts = file_path.split(pattern);
			int temp = parts.length;
			String inv_file_name = parts[temp-1];
			
			String output = inv_file_name+"&"+file_path;
			
	
			
		    
			out.print(output);
			
	} catch (Exception e) {
		e.printStackTrace();
		connect.close();
		out.print("fail");
	}
	
	///////////////////////////////////////////////////////End Writing Excel Process/////////////////////////////////////////////////////////
	 
	connect.close();
%>
