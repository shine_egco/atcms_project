<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
     <link href="../dist/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
     <link href="../dist/css/morris.css" rel="stylesheet" type="text/css">

     <link href="../bower_components/sidebar/sidebar_style_3.css" rel="stylesheet" type="text/css"> 

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

 <script>

	function getMonthString(temp){
			
		var month = "";
				switch (temp) {
				
			    case "1":
			    	month = "January";
			        break;
			    case "2":
			    	month = "February";
			        break;
			    case "3":
			    	month = "March";
			        break;
			    case "4":
			    	month = "April";
			        break;
			    case "5":
			    	month = "May";
			        break;
			    case "6":
			    	month = "June";
			    	break;
			    case "7":
			    	month = "July";
			    	break;
			    case "8":
			    	month = "August";
			    	break;
			    case "9":
			    	month = "September";
			    	break;
			    case "10":
			    	month = "October";
			    	break;
			    case "11":
			    	month = "November";
			    	break;
			    case "12":
			    	month = "December";
			    	break;
			}
				
			return month;
		
	}
	
	
	function fetch_monthly_summary_report_detail(){
		
		 var report_id = sessionStorage.getItem("report_id_for_get_detail");
		 
	
			var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
					
					var jsonObj = JSON.parse(xmlhttp.responseText);
					
					if(xmlhttp.responseText=="fail")
					{
						alert("Update Error");
					}else{
						
					//alert("Success");
						var month = document.getElementById("report_month");
						var year = document.getElementById("report_year");
						
						document.getElementById("report_header");
						
						
								month.value = jsonObj[0].month;
								year.value =  jsonObj[0].year;
								
								
							//	alert(month.value+","+year.value);
								document.getElementById("report_header").innerHTML = "Summary Report : "+getMonthString(month.value)+" "+year.value;
								
								fetch_credit_inv_to_table();
							//	alert(month.value+" , " +year.value);
						
						//location.reload();
					}
					
				}// end if check state
			}// end function
			
		
			xmlhttp.open("POST", "get_monthly_summary_report_by_report_id_background.jsp?report_id="+report_id, true);
			xmlhttp.send();

		
		
	}
	function fetch_credit_inv_to_table(){
		
		var xmlhttp;
		
		var month = document.getElementById("report_month");
		var year = document.getElementById("report_year");
		
		  var  table = $('#dataTables-credit-inv').DataTable();
	       table.clear();
		
		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange = function() {
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
				
				var jsonObj = JSON.parse(xmlhttp.responseText);
				
				if(jsonObj.length == 0)
				{
					//alert("0 Credit Inv Return");	
					
					calculate_credit_summary();
					fetch_cash_inv_to_table();
					
				}else{
				//	alert("Inv Num return:"+jsonObj.length);
				//dataTables-credit-inv
				
					for(i in jsonObj) {
						//alert(jsonObj[i].deliveryDate);
							
								$('#dataTables-credit-inv').DataTable().row.add([
		                           '<tr><td name='+jsonObj[i].invNo+'><center>'+jsonObj[i].invNo+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].deliveryDate+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].customerName+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].totalValue+'</center></td>'
		                        //   ,'<td><center>'+jsonObj[i].totalCost+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].totalProfit+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].status+'</center></td>'
		                           ,'<td><center><button id = "'+jsonObj[i].orderId+'" type="button" class="btn btn-info btn-circle btn-md" onclick = "redirect_order_detail(this.id)">'
		                           +'<i class="glyphicon glyphicon-search"></i></button></center></td></tr>'	       
		                         ]).draw();
							}
				
					calculate_credit_summary();
					fetch_cash_inv_to_table();
				}
				
			}// end if check state
		}// end function
		
	
		xmlhttp.open("POST", "get_credit_inv_by_month_and_year_background.jsp?year="+year.value+"&month="+month.value, true);
		xmlhttp.send();

	//	get_credit_inv_by_month_and_year_background.jsp
		
	}
	
	 function numberWithCommas(x) {
		 
		    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		    
		}
	
	function calculate_credit_summary(){
		
		  
		var table = $('#dataTables-credit-inv').DataTable();
		 
		var data = table.rows().data();
		 
		//alert( 'The table has '+data.length+' records' );
		 
		 var  total_num_credit_inv = document.getElementById("total_num_credit_inv");
		 var  total_value_credit_inv = document.getElementById("total_value_credit_inv");
		 var  total_cost_credit_inv = document.getElementById("total_cost_credit_inv");
		 var  total_profit_credit_inv = document.getElementById("total_profit_credit_inv");
		 /////////////////////////////////////////////////////////////////////////////////////////
		  var  current_value_credit_inv = document.getElementById("current_value_credit_inv");
		 var  current_cost_credit_inv = document.getElementById("current_cost_credit_inv");
		 var  current_profit_credit_inv = document.getElementById("current_profit_credit_inv");
		 
		 var  pending_value_credit_inv = document.getElementById("pending_value_credit_inv");
		 var  pending_cost_credit_inv = document.getElementById("pending_cost_credit_inv");
		 var  pending_profit_credit_inv = document.getElementById("pending_profit_credit_inv");


		 
		 
		 var  completed_num_credit_inv = document.getElementById("completed_num_credit_inv");
		 var  progress_credit_inv = document.getElementById("progress_credit_inv");
		 	
		        total_num_credit_inv.innerHTML = data.length + "" ;
		        
		        
		       		var completed_credit_inv = 0; 
		       		var total_value_credit_inv_dis = 0;
		       		var total_cost_credit_inv_dis = 0;
		       		var total_profit_credit_inv_dis = 0;
		       		 
		       		var current_value_credit_inv_dis = 0;
		       		var current_cost_credit_inv_dis = 0;
		       		var current_profit_credit_inv_dis = 0;
		       		
		       		var pending_value_credit_inv_dis = 0;
		       		var pending_cost_credit_inv_dis = 0;
		       		var pending_profit_credit_inv_dis = 0;
		       		 
		       		 
		       		
		            var rows = $("#dataTables-credit-inv").dataTable().fnGetNodes();
		            
		            for(var i=0;i<rows.length;i++)
		            {
		              //  cells.push($(rows[i]).find("td:eq(0)").text()); 
		            	//if(( $(rows[i]).find("td:eq(5)").text())=="completed")
		            	if((( $(rows[i]).find("td:eq(4)").text())!="null")&&(( $(rows[i]).find("td:eq(4)").text())!="NaN"))
		            	{
		            		console.log($(rows[i]).find("td:eq(0)").text()+","+$(rows[i]).find("td:eq(2)").text()+","+$(rows[i]).find("td:eq(3)").text()+","+$(rows[i]).find("td:eq(4)").text());
		            		completed_credit_inv ++;
		            		
		            		current_cost_credit_inv_dis = current_cost_credit_inv_dis +parseFloat($(rows[i]).find("td:eq(3)").text() , 10) ;
		            		current_profit_credit_inv_dis = current_profit_credit_inv_dis +parseFloat($(rows[i]).find("td:eq(4)").text() , 10) ;
		            		
		            		current_value_credit_inv_dis =  current_value_credit_inv_dis +parseFloat($(rows[i]).find("td:eq(3)").text() , 10) ;
		            	
		            	}
		            		
		            	total_value_credit_inv_dis = total_value_credit_inv_dis +parseFloat($(rows[i]).find("td:eq(2)").text() , 10) ;
		            	
		            	
		            	
		            }
	
		            completed_num_credit_inv.innerHTML = completed_credit_inv+"";
		            
		            total_value_credit_inv.innerHTML = numberWithCommas(total_value_credit_inv_dis.toFixed(2)+"");
		            
		            current_value_credit_inv.innerHTML =  numberWithCommas(current_value_credit_inv_dis.toFixed(2)+"");
		            current_cost_credit_inv.innerHTML = numberWithCommas(current_cost_credit_inv_dis.toFixed(2)+"");
		            current_profit_credit_inv.innerHTML = numberWithCommas(current_profit_credit_inv_dis.toFixed(2)+"");
		            pending_value_credit_inv.innerHTML =   numberWithCommas((total_value_credit_inv_dis - current_value_credit_inv_dis).toFixed(2) +"" );
		        
		            ///////////////////////////////////////////////////////////////////////////////////////
		            
		            var temp_credit_progess = 0;
		            
		            temp_credit_progess =  ((parseFloat(completed_credit_inv , 10) / (parseFloat(total_num_credit_inv.innerHTML  , 10))*100));
		            
		            progress_credit_inv.innerHTML = temp_credit_progess.toFixed(2) +" %" ; 
		            /////////////////////////////////////////////////////////////////////////////////////////////
		            
		            
		      
	
	}
	
	function calculate_cash_summary(){
		
		  
		var table = $('#dataTables-cash-inv').DataTable();
		 
		var data = table.rows().data();
		 
		//alert( 'The table has '+data.length+' records' );
		 
		 var  total_num_cash_inv = document.getElementById("total_num_cash_inv");
		 var  total_value_cash_inv = document.getElementById("total_value_cash_inv");
		 var  total_cost_cash_inv = document.getElementById("total_cost_cash_inv");
		 var  total_profit_cash_inv = document.getElementById("total_profit_cash_inv");
		 
		 
		 /////////////////////////////////////////////////////////////////////////////////////////
		  var  current_value_credit_inv = document.getElementById("current_value_cash_inv");
		 var  current_cost_credit_inv = document.getElementById("current_cost_cash_inv");
		 var  current_profit_credit_inv = document.getElementById("current_profit_cash_inv");
		 
		 var  pending_value_credit_inv = document.getElementById("pending_value_cash_inv");
		 var  pending_cost_credit_inv = document.getElementById("pending_cost_cash_inv");
		 var  pending_profit_credit_inv = document.getElementById("pending_profit_cash_inv");


		 
		 
		 var  completed_num_cash_inv = document.getElementById("completed_num_cash_inv");
		 var  progress_cash_inv = document.getElementById("progress_cash_inv");
		 	
					 total_num_cash_inv.innerHTML = data.length + "" ;
		        
		        
		       		var completed_cash_inv = 0; 
		       		var total_value_cash_inv_dis = 0;
		       		var total_cost_cash_inv_dis = 0;
		       		var total_profit_cash_inv_dis = 0;
		       		 
		       		var current_value_cash_inv_dis = 0;
		       		var current_cost_cash_inv_dis = 0;
		       		var current_profit_cash_inv_dis = 0;
		       		
		       		var pending_value_cash_inv_dis = 0;
		       		var pending_cost_cash_inv_dis = 0;
		       		var pending_profit_cash_inv_dis = 0;
		       		 
		       		 
		       		
		            var rows = $("#dataTables-cash-inv").dataTable().fnGetNodes();
		            
		            for(var i=0;i<rows.length;i++)
		            {
		              //  cells.push($(rows[i]).find("td:eq(0)").text()); 
		            	if((( $(rows[i]).find("td:eq(4)").text())!="null")&&(( $(rows[i]).find("td:eq(4)").text())!="NaN"))
		            	{
		            		completed_cash_inv ++;
		            		
		            		current_cost_cash_inv_dis = current_cost_cash_inv_dis +parseFloat($(rows[i]).find("td:eq(3)").text() , 10) ;
		            		current_profit_cash_inv_dis = current_profit_cash_inv_dis +parseFloat($(rows[i]).find("td:eq(4)").text() , 10) ;
		            		
		            		current_value_cash_inv_dis =  current_value_cash_inv_dis +parseFloat($(rows[i]).find("td:eq(3)").text() , 10) ;
		            	
		            	}
		            		
		            	total_value_cash_inv_dis = total_value_cash_inv_dis +parseFloat($(rows[i]).find("td:eq(2)").text() , 10) ;
		            	
		            	
		            	
		            }
	
		            completed_num_cash_inv.innerHTML = completed_cash_inv+"";
		            
		            total_value_cash_inv.innerHTML = numberWithCommas(total_value_cash_inv_dis.toFixed(2)+"");
		            
		            current_value_cash_inv.innerHTML =  numberWithCommas(current_value_cash_inv_dis.toFixed(2)+"");
		            current_cost_cash_inv.innerHTML = numberWithCommas(current_cost_cash_inv_dis.toFixed(2)+"");
		            current_profit_cash_inv.innerHTML = numberWithCommas(current_profit_cash_inv_dis.toFixed(2)+"");
		            pending_value_cash_inv.innerHTML =   numberWithCommas((total_value_cash_inv_dis - current_value_cash_inv_dis).toFixed(2) +"" );
		        
		            ///////////////////////////////////////////////////////////////////////////////////////
		            
		            var temp_cash_progess = 0;
		            
		            temp_cash_progess =  ((parseFloat(completed_cash_inv , 10) / (parseFloat(total_num_cash_inv.innerHTML  , 10))*100));
		            
		            progress_cash_inv.innerHTML = temp_cash_progess.toFixed(2) +" %" ; 
		            /////////////////////////////////////////////////////////////////////////////////////////////
		            
		            
		      
	
	}
	
	
	function fetch_cash_inv_to_table(){
		
		var xmlhttp;
		
		var month = document.getElementById("report_month");
		var year = document.getElementById("report_year");
		
		  var  table = $('#dataTables-cash-inv').DataTable();
	       table.clear();
		
		
		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange = function() {
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
				
				var jsonObj = JSON.parse(xmlhttp.responseText);
				
				if(jsonObj.length == 0)
				{
					//alert("0 Credit Inv Return");	
					
					calculate_cash_summary();
					fetch_short_bill_to_table();
					
				}else{
				//	alert("Inv Num return:"+jsonObj.length);
				//dataTables-credit-inv
				
					for(i in jsonObj) {
							
								$('#dataTables-cash-inv').DataTable().row.add([
		                           '<tr><td><center>'+jsonObj[i].invNo+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].billDate+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].customerName+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].totalValue+'</center></td>'
		                         //  ,'<td><center>'+jsonObj[i].totalCost+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].totalProfit+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].status+'</center></td>'
		                           ,'<td><center><button id = "'+jsonObj[i].cashBillId+'" type="button" class="btn btn-info btn-circle btn-md" onclick = "redirect_cash_bill_detail(this.id)" >'
		                           +'<i class="glyphicon glyphicon-search"></i></button></center></td></tr>'	       
		                         ]).draw();
							}
					
					calculate_cash_summary();
					fetch_short_bill_to_table();
					//location.reload();
				}
				
			}// end if check state
		}// end function
		
	
		xmlhttp.open("POST", "get_cash_bill_main_by_month_and_year_background.jsp?year="+year.value+"&month="+month.value, true);
		xmlhttp.send();
		
	}
	
	function fetch_short_bill_to_table(){
		
			var xmlhttp;
		
		var month = document.getElementById("report_month");
		var year = document.getElementById("report_year");
		
		  var  table = $('#dataTables-short-bill').DataTable();
	       table.clear();
		
		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange = function() {
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
				
				var jsonObj = JSON.parse(xmlhttp.responseText);
				
				if(jsonObj.length == 0)
				{
				//	alert("0 Daily Novat Return");	
				
					fetch_purchase_main_to_table();
					
				}else{
					
			//	alert("Inv Num return:"+jsonObj.length);
				//dataTables-credit-inv
				
				// waiting for test case
				
					for(i in jsonObj) {
							
								$('#dataTables-short-bill').DataTable().row.add([
		                           '<tr><td><center>'+jsonObj[i].invNo+'</center></td>'		        
		                           ,'<td><center>'+jsonObj[i].billDate+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].customerName+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].tag+'</center></td>'                  
		                           ,'<td><center>'+jsonObj[i].totalValue+'</center></td>'             
		                           ,'<td><center>'+jsonObj[i].totalProfit+'</center></td>'             
		                           ,'<td><center><button id = "'+jsonObj[i].shortBillId+'" type="button" class="btn btn-info btn-circle btn-md" onclick = "redirect_short_bill_detail(this.id)" >'
		                           +'<i class="glyphicon glyphicon-search"></i></button></center></td></tr>'	             
		                         ]).draw();
							}
				    
					calculate_daily_novat_summary();

					//location.reload();
					fetch_purchase_main_to_table();
				}
				

			}// end if check state
		}// end function
		
	
		xmlhttp.open("POST", "get_short_bill_main_by_month_and_year_background.jsp?year="+year.value+"&month="+month.value, true);
		xmlhttp.send();
		
	}
	
	function calculate_daily_novat_summary(){
		
		var table = $('#dataTables-short-bill').DataTable();
		var data = table.rows().data();
		
		
		var total_num_daily_novat = document.getElementById("total_num_daily_novat");
		var total_value_daily_novat = document.getElementById("total_value_daily_novat");
		var total_cost_daily_novat = document.getElementById("total_cost_daily_novat");
		var total_profit_daily_novat = document.getElementById("total_profit_daily_novat");
		
		
		var total_value = 0;
		var total_cost = 0;
		var total_profit = 0;
		
		
			  total_num_daily_novat.innerHTML = data.length + "" ;
			  
		 var rows = $("#dataTables-short-bill").dataTable().fnGetNodes();	  
		 
		 for(var i=0;i<rows.length;i++)
         {
			 
			// console.log($(rows[i]).find("td:eq(1)").text()+","+$(rows[i]).find("td:eq(3)").text()+","+$(rows[i]).find("td:eq(4)").text()+","+$(rows[i]).find("td:eq(5)").text());
			 total_value = total_value + parseFloat($(rows[i]).find("td:eq(3)").text() , 10) ;
			 total_cost = total_cost + parseFloat($(rows[i]).find("td:eq(4)").text() , 10) ;
			 total_profit = total_profit + parseFloat($(rows[i]).find("td:eq(5)").text() , 10) ;
	        
         }
		 total_value_daily_novat.innerHTML = numberWithCommas(total_value.toFixed(2)+"");
		 total_cost_daily_novat.innerHTML = numberWithCommas(total_cost.toFixed(2)+"");
		 total_profit_daily_novat.innerHTML = numberWithCommas(total_profit.toFixed(2)+"");
		 
	}
	
	
	function calculate_purchase_summary() {
		
		
		var table = $('#dataTables-purchase-inv').DataTable();
		
		var data = table.rows().data();
		
		
		 
		 var  total_num_purchase_inv = document.getElementById("total_num_purchase_inv");
		 var  num_direct_purchase_inv = document.getElementById("num_direct_purchase_inv");
		 var  num_indirect_purchase_inv = document.getElementById("num_indirect_purchase_inv");
		 
		 
		 
		 var  total_value_puchase_inv = document.getElementById("total_value_puchase_inv");
		 var  total_value_direct_puchase_inv = document.getElementById("total_value_direct_puchase_inv");
		 var  total_value_indirect_puchase_inv = document.getElementById("total_value_indirect_puchase_inv");
		 
		
		
				 total_num_purchase_inv.innerHTML = data.length + "" ;
				 
				 var num_direct_bill  = 0 ; 
				 var num_indirect_bill = 0 ;
				 var total_value_puchase_inv_dis = 0;
				 var total_value_direct_puchase_inv_dis = 0 ;
				 var total_value_indirect_puchase_inv_dis = 0; 
				 
			     var rows = $("#dataTables-purchase-inv").dataTable().fnGetNodes();
			     
			  //  alert("35");
		   for(var i=0;i<rows.length;i++)
            {
			  // alert("34");
			 
              //  cells.push($(rows[i]).find("td:eq(0)").text()); 
	            	if(( $(rows[i]).find("td:eq(3)").text())=="direct")
	            	{
	            			
	            		num_direct_bill ++ ;
	            		total_value_direct_puchase_inv_dis = total_value_direct_puchase_inv_dis + parseFloat($(rows[i]).find("td:eq(4)").text() , 10) ;

	            	
	            	}else{

	            		num_indirect_bill++;
	            		total_value_indirect_puchase_inv_dis = total_value_indirect_puchase_inv_dis +  parseFloat($(rows[i]).find("td:eq(4)").text() , 10) ;
	            	}
	            	
	            	total_value_puchase_inv_dis = total_value_puchase_inv_dis +  parseFloat($(rows[i]).find("td:eq(4)").text() , 10) ;
            }
	        
		   
					   num_direct_purchase_inv.innerHTML = num_direct_bill+"";
					   num_indirect_purchase_inv.innerHTML = num_indirect_bill + "";
					   
					   total_value_puchase_inv.innerHTML = numberWithCommas(total_value_puchase_inv_dis.toFixed(2)+"");
					   total_value_direct_puchase_inv.innerHTML = numberWithCommas(total_value_direct_puchase_inv_dis.toFixed(2)+"");
					   total_value_indirect_puchase_inv.innerHTML = numberWithCommas(total_value_indirect_puchase_inv_dis.toFixed(2)+"");

		
	}
	
	function fetch_purchase_main_to_table(){
		
		//alert("Active2");
		
		var xmlhttp;
		
		var month = document.getElementById("report_month");
		var year = document.getElementById("report_year");
		
	    var  table = $('#dataTables-purchase-inv').DataTable();
		  
	      table.clear();
		
		
		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange = function() {
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
				
				var jsonObj = JSON.parse(xmlhttp.responseText);
				
				if(jsonObj.length == 0)
				{
					//alert("0 purchase_main Return");	
					calculate_purchase_summary();
					
				}else{
					//alert("back from purchase_main_background");
	
					for(i in jsonObj) {
								
							
								$('#dataTables-purchase-inv').DataTable().row.add([
		                           '<tr><td><center>'+jsonObj[i].invoiceDate+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].vendorName+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].invNo+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].purchaseStatus+'</center></td>'
		                           ,'<td><center>'+jsonObj[i].totalValue+'</center></td>'
		                           ,'<td><center><button id = "'+jsonObj[i].purchaseId+'" type="button" class="btn btn-info btn-circle btn-md" onclick = "redirect_purchase_bill_detail(this.id)" >'
		                           +'<i class="glyphicon glyphicon-search"></i></button></center></td></tr>'	       
		                         ]).draw();
						
						
							}
					
					calculate_purchase_summary();
					alert("active32");
					calculate_total_grand_table();
					
					//location.reload();
				}
				

			}// end if check state
		}// end function
		
	
		xmlhttp.open("POST", "get_purchase_main_by_month_and_year_background.jsp?year="+year.value+"&month="+month.value, true);
		xmlhttp.send();
		
		
	}
	
	
	function calculate_total_grand_table(){
		
		alert("active");
		
		var  current_value_credit_inv = document.getElementById("current_value_credit_inv").innerHTML;
		var  current_profit_credit_inv = document.getElementById("current_profit_credit_inv").innerHTML;
		
		var  current_value_cash_inv = document.getElementById("current_value_cash_inv").innerHTML;
		var  current_profit_cash_inv = document.getElementById("current_profit_cash_inv").innerHTML;
			 
		//alert("current_value_credit_inv:"+current_value_credit_inv+"  current_value_cash_inv:" + current_value_cash_inv);
	
		var  total_value_direct_puchase_inv = document.getElementById("total_value_direct_puchase_inv").innerHTML;
		var  total_value_indirect_puchase_inv = document.getElementById("total_value_indirect_puchase_inv").innerHTML;
		 
		var total_profit_daily_novat = document.getElementById("total_profit_daily_novat").innerHTML;
		
		
		var sell_amount = document.getElementById("sell_amount");
		var purchase_amount = document.getElementById("purchase_amount");
		var total_grand_profit = document.getElementById("total_grand_profit");
		var calculated_vat = document.getElementById("calculated_vat");
		////////////////////////////////////////////////////////////////////////////////////
		
		//current_value_credit_inv = "1,000,000.00";
	//	alert("current_value_credit_inv:"+current_value_credit_inv);
	//	alert("current_value_cash_inv:"+current_value_cash_inv);
		
		
		
		var array_current_value_credit_inv = (current_value_credit_inv).split(",");
		var array_current_value_cash_inv = (current_value_cash_inv).split(",");
		
   
		
		
		
	//	alert("array_current_value_credit_inv[0]:"+array_current_value_credit_inv[0]);
	//	alert("array_current_value_cash_inv[0]:"+array_current_value_cash_inv[0]);
		
		var sell_amount_credit = "";
		var sell_amount_cash = "";
		
		
		var total_direct_purchase = "";
		var total_indirect_purchase = "";
		
		
		for(var y=0;y<array_current_value_credit_inv.length;y++)
		{
			sell_amount_credit = sell_amount_credit + array_current_value_credit_inv[y];
			
		}
		
		for(var z=0;z<array_current_value_cash_inv.length;z++)
		{
			sell_amount_cash = sell_amount_cash + array_current_value_cash_inv[z];
			
		}
		

		
		
		
			
		
		sell_amount.innerHTML =   numberWithCommas(( parseFloat((sell_amount_credit) ,10) 
							 	                   + parseFloat((sell_amount_cash) ,10)).toFixed(2))+"";
		
		
		
		
		///////////////////////////////////////////////////////////////////////////////////////
		var array_total_value_direct_puchase_inv = total_value_direct_puchase_inv.split(",");
		var array_total_value_indirect_puchase_inv = total_value_indirect_puchase_inv.split(",");
		
		
		for(var za=0;za<array_total_value_direct_puchase_inv.length;za++)
		{
			total_direct_purchase = total_direct_purchase + array_total_value_direct_puchase_inv[za];
		
		}
		
		for(var zb=0;zb<array_total_value_indirect_puchase_inv.length;zb++)
		{
			total_indirect_purchase = total_indirect_purchase + array_total_value_indirect_puchase_inv[zb];
		
		}
		

		
		purchase_amount.innerHTML = numberWithCommas(( parseFloat((array_total_value_direct_puchase_inv[0]+array_total_value_direct_puchase_inv[1]) ,10) 
                									 + parseFloat((array_total_value_indirect_puchase_inv[0]+array_total_value_indirect_puchase_inv[1]) ,10)).toFixed(2))+"";
		/////////////////////////////////////////////////////////////////////////////////////////
		
		var array_current_profit_credit_inv = (current_profit_credit_inv).split(",");
		var array_current_profit_cash_inv = (current_profit_cash_inv).split(",");
		var array_total_profit_daily_novat  = (total_profit_daily_novat).split(",");
		
		
		//alert(parseFloat((array_total_profit_daily_novat[0]+array_total_profit_daily_novat[1]) ,10));
			
		total_grand_profit.innerHTML = numberWithCommas(( parseFloat((array_current_profit_credit_inv[0]+array_current_profit_credit_inv[1]) ,10) 
                										+ parseFloat((array_current_profit_cash_inv[0]+array_current_profit_cash_inv[1]) ,10)).toFixed(2))+"";
		
				
		
		calculated_vat.innerHTML = numberWithCommas(
								  ((( parseFloat((sell_amount_credit) ,10) 
               					    + parseFloat((sell_amount_cash) ,10))
               					    -
               					    ( parseFloat((total_direct_purchase) ,10) 
       								+ parseFloat((total_indirect_purchase) ,10)))*0.07).toFixed(2))+"";
               					    
                
		
		
	//	sell_amount.innerHTML = parseFloat(current_value_credit_inv.innerHTML) + parseFloat(current_value_cash_inv.innerHTML);
	//	purchase_amount.innerHTML =  parseFloat(current_profit_credit_inv.innerHTML) + parseFloat(current_profit_cash_inv.innerHTML);
		
		
		
		 
		
	}
	
	function redirect_short_bill_detail(short_bill_id)
	{
		//alert(order_id);
		sessionStorage.setItem("short_bill_id_for_get_detail", short_bill_id); 
		$('#createReportModel').modal('hide');
		
		window.open("report_short_bill_detail.jsp");
		
		//$('#createReportModel').modal('hide');
	}
	
	function redirect_purchase_bill_detail(order_id){
		
		//alert(order_id);
		sessionStorage.setItem("purchase_id_for_get_detail", order_id); 
		
		$('#createReportModel').modal('hide');
		window.open("report_purchase_bill_detail.jsp");
	//	$('#createReportModel').modal('hide');
	}
	
	function redirect_order_detail(order_id){
		
		//alert(order_id);
		sessionStorage.setItem("order_id_for_get_detail", order_id); 
		
		window.open("report_credit_inv_detail.jsp");
		$('#createReportModel').modal('hide');
	}
	function redirect_cash_bill_detail(cash_bill_id){
		
	//	alert(cash_bill_id);
		sessionStorage.setItem("cash_bill_id_for_get_detail", cash_bill_id); 
		$('#createReportModel').modal('hide');
		window.open("report_cash_bill_detail.jsp");
	//	$('#createReportModel').modal('hide');
	}
	
	
	
	function show_modal_create_summary_credit_report(){
	
		$('#createReportModel').modal('show');
		
		//alert("OWOOWOO");
		var type_of_generate = document.getElementById("type_of_generate");
		type_of_generate.value = "credit_invoice";
		
		var month = document.getElementById("report_month");
		var year = document.getElementById("report_year");
		
		var label_of_generate = document.getElementById("label_of_generate");
		label_of_generate.innerHTML = "Month : "+month.value+"     Year : "+year.value;
		
//		$('#createReportModel').modal('hide');
	
	}
	
	function show_modal_create_summary_cash_report(){
		
		$('#createReportModel').modal('show');
		
		
		var type_of_generate = document.getElementById("type_of_generate");
		type_of_generate.value = "cash_invoice";
		
		var month = document.getElementById("report_month");
		var year = document.getElementById("report_year");
		
		var label_of_generate = document.getElementById("label_of_generate");
		label_of_generate.innerHTML = "Month : "+month.value+"     Year : "+year.value;
		
	//	$('#createReportModel').modal('hide');
	
	}
	
		function show_modal_create_summary_purchase_report(){
		
		$('#createReportModel').modal('show');
		
		
		var type_of_generate = document.getElementById("type_of_generate");
		type_of_generate.value = "purchase_invoice";
		
		var month = document.getElementById("report_month");
		var year = document.getElementById("report_year");
		
		var label_of_generate = document.getElementById("label_of_generate");
		label_of_generate.innerHTML = "Month : "+month.value+"     Year : "+year.value;
		
//		$('#createReportModel').modal('hide');
	
		
	}
	
		
		function show_modal_create_summary_sb_report(){
			
			$('#createReportModel').modal('show');
			//alert();
			
			var type_of_generate = document.getElementById("type_of_generate");
			type_of_generate.value = "short_bill";
			
			var month = document.getElementById("report_month");
			var year = document.getElementById("report_year");
			
			var label_of_generate = document.getElementById("label_of_generate");
			label_of_generate.innerHTML = "Month : "+month.value+"     Year : "+year.value;
			
//			$('#createReportModel').modal('hide');
		
			
		}
	function generate_report(){
		
		//alert("gem");
		var type_of_generate = document.getElementById("type_of_generate").value;
		var month = document.getElementById("report_month").value;
		var year = document.getElementById("report_year").value;
		var form_of_report = document.getElementById("select_form_of_report").value;
		
		var parameter = "?type_of_generate="+type_of_generate+
						"&form_of_report="+form_of_report+
						"&month="+month+
						"&year="+year;
		
		var link ;
		
		if(type_of_generate=="cash_invoice")
		{
			if(form_of_report=="internal_use")
			{
				link = "generate_internal_cash_invoice_monthly_summary_report_background.jsp"
			}else{
				link = "generate_external_cash_invoice_monthly_summary_report_background.jsp"
			}
			
			
		}else if (type_of_generate=="credit_invoice"){
			
			if(form_of_report=="internal_use")
			{
				link = "generate_internal_credit_invoice_monthly_summary_report_background.jsp"
			}else{
				link = "generate_external_credit_invoice_monthly_summary_report_background.jsp"
			}
			
			
		}else if (type_of_generate=="purchase_invoice"){
			
			if(form_of_report=="internal_use")
			{
				link = "generate_internal_purchase_invoice_monthly_summary_report_background.jsp"
			}else{
				link = "generate_external_purchase_invoice_monthly_summary_report_background.jsp"
			}
			
		}else if(type_of_generate=="short_bill"){
			
			if(form_of_report=="internal_use")
			{
				alert("prepare to going");
				link = "generate_internal_short_bill_monthly_summary_report_background.jsp"
			}else{
				//link = "generate_external_purchase_invoice_monthly_summary_report_background.jsp"
			}
			
			
			
		}

		var xmlhttp;

		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange = function() {
			
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
				
				if(xmlhttp.responseText=="fail")
				{
					alert("Error Occer to Generate inv , pls contact Admin");
				}else{
					//alert("gen success");
					
					alert(xmlhttp.responseText);
					var values = xmlhttp.responseText.split("&");
					var file_name = values[0];
					var file_path = values[1];
					
					document.getElementById("inv_file_name").value = file_name;
					document.getElementById("inv_file_path").value = file_path;
					document.getElementById("form_file_path_gen").submit();
					
					setTimeout( function() { $('#myModal').modal('hide'); }, 800);
					
				}
				
	
				

			}// end if check state
		}// end function
		
	
		xmlhttp.open("POST",link+parameter, true);
		xmlhttp.send();
		
		
	}
	function generate_monthy_product_report(){
		
		var month = document.getElementById("report_month").value;
		var year = document.getElementById("report_year").value;
		
		var xmlhttp;

		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange = function() {
			
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
				
				if(xmlhttp.responseText=="fail")
				{
					alert("Error Occer to Generate inv , pls contact Admin");
				}else{
					//	alert(xmlhttp.responseText);
					//form_download
					
						var inv_file_name = document.getElementById('inv_file_name');
		 				var inv_file_path = document.getElementById('inv_file_path');
						var values = xmlhttp.responseText.split("&");
						var file_name = values[0];
						var file_path = values[1];
						
						alert("Your file "+file_name+" are ready.");
					
						inv_file_name.value = file_name;
						inv_file_path.value = file_path;
					
					document.forms["form_download"].submit();
	

				}
				
	
				

			}// end if check state
		}// end function
		
	
		xmlhttp.open("POST","generate_product_monthly_summary_report_background.jsp"+"?month="+month+"&year="+year, true);
		xmlhttp.send();
		
	//	alert();
	}
	
	
	function redirect_purchase_payment_method_detail(){
		
		 var report_id = sessionStorage.getItem("report_id_for_get_detail");
		
	
		window.open("report_monthly_purchase_payment_method_detail.jsp");
	
		
	}
	
</script>
    
    

</head>

<body>

    <div id="wrapper">
    		<nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>
                <div class="sidebar-header">
                    <h3>Bored Sidebar</h3>
                </div>
                
    
        		<div id="includedSidebar"></div>
        		
        	</nav>



        <!-- Navigation -->
       

        <div id="content" class="form-control">
        
        	   <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-justify"></i>
                                <span></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Page</a></li>
                       
                            </ul>
                        </div>
                    </div>
                </nav>
        
             <div class="row">
	                <div class="col-lg-4">
	                  				  <h2 id="report_header" class="page-header"> </h2>
	                  				  <input id="report_month" type="hidden">
	                  				  <input id="report_year" type="hidden">
	                  				  <!--  
               				  		<form  method="get" action="../DownloadServlet" id="form_download">
												    <div class="input-group">
														      <input id="inv_file_name" name="inv_file_name" type="hidden" class="form-control" readonly>
														      <input id="inv_file_path" name="inv_file_path" type="hidden">
												    </div>
								   </form>
	                
 										-->	
	                </div>
	                    <div class="col-lg-4">
	                    </div>
	                 <div class="col-lg-2">
	                 				<br>
	                 				<table>
	                 					<tr>
	                 						<td>
	                 								 <button type="button" class=" btn btn-info" onclick="redirect_purchase_payment_method_detail()">Monthly Purchase Payment Method Report</button>
	                 						</td>
	                 						<td>
	                 							
													 <button type="button" class=" btn btn-success"  onclick="generate_monthy_product_report()" >Overall Report</button>

	                 						</td>
	                 					</tr>
	                 				</table>
	                 				
	                 				 
	                  				
	                </div>
                <!-- /.col-lg-12 -->
             </div>
			<div class="row" id="row_n">
            		<div class="col-lg-6">
             	    		  <div class="panel panel-default">
                      			
                       		   <div class="panel-body">
                        			 <div class="dataTable_wrapper">
                        			 			<table class="table table-bordered table-hover table-sortable " id="table_credit_summary" style="border:none;">
			             	    					 <tr class=" text-left" style="border:none;">															
										    					<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Sell Amount : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="sell_amount" class="text-left" style="font-size: 17px;">XX</label>
																</td>
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Purchase Amount : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="purchase_amount" class="text-left" style="font-size: 17px;">XX</label>
																</td>
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Calculated VAT : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="calculated_vat" class="text-left" style="font-size: 17px;">XX %</label>
																</td>
																
															</tr>
															 <tr class=" text-left" style="border:none;">															
										    					<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Total Grand Profit  : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="total_grand_profit" class="text-left" style="font-size: 17px;">XX</label>
																</td>
								

															</tr>

			             	    				</table>
			                          </div>
			                         </div>
			                   </div>
             	    		       
	                </div>
	                
	                
	                 

            </div>
                                        	
			
             <div class="row" id = "row_0">
             	    <div class="col-lg-6">
             	    		  <div class="panel panel-default">
                      				  <div class="panel-heading">
                         						  Credit Invoice
                       					 </div>
                       		   <div class="panel-body">
                        			 <div class="dataTable_wrapper">
			             	    			<table class="table table-striped table-bordered table-hover" id="dataTables-credit-inv">
			                                    <thead>
			                                        <tr>	
			                                        
			                                       		 <th style="width: 70px;" >Inv No.</th> 
			                                       		  <th style="width: 70px;" >Inv Date</th> 
			                                             <th style="width: 180px;" >Customer Name</th>
			                                             <th style="width: 120px;">Total Value</th>
			                                      
			                                             <th style="width: 120px;">Total Profit</th>
			                                            <th>Status</th>
			                                            
			                                            <th></th>
			                                            
			                                        </tr>
			                                    </thead>
			                                    <tbody>
			                                    
			                                    </tbody>
			                                </table>
			                            </div>
			                         </div>
			                   </div>
             	    		       
	                </div>
	                <div class="col-lg-6">
             	    		  <div class="panel panel-default">
                      				  <div class="panel-heading">
                         						  Cash Invoice
                       					 </div>
                       		   <div class="panel-body">
                        			 <div class="dataTable_wrapper">
			             	    			<table class="table table-striped table-bordered table-hover" id="dataTables-cash-inv">
			                                    <thead>
			                                        <tr>	
			                                        
			                                       		 <th style="width: 70px;" >Inv No.</th> 
			                                       		 <th style="width: 70px;" >Inv Date</th> 
			                                             <th style="width: 180px;" >Customer Name</th>
			                                             <th style="width: 120px;">Total Value</th>
			                                       
			                                             <th style="width: 120px;">Total Profit</th>
			                                            <th>Status</th>
			                                            <th></th>
			                                            
			                                        </tr>
			                                    </thead>
			                                    <tbody>
			                                    
			                                    </tbody>
			                                </table>
			                            </div>
			                         </div>
			                   </div>
             	    		       
	                </div>

            </div>
            <div class="row" id="row_1">
            		<div class="col-lg-6">
             	    		  <div class="panel panel-default">
                      			
                       		   <div class="panel-body">
                        			 <div class="dataTable_wrapper">
                        			 			<table class="table table-bordered table-hover table-sortable " id="table_credit_summary" style="border:none;">
			             	    					 <tr class=" text-left" style="border:none;">															
										    					<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Total Invoice : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="total_num_credit_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Completed Invoice : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="completed_num_credit_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Progress : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="progress_credit_inv" class="text-left" style="font-size: 17px;">XX %</label>
																</td>
																
															</tr>
															 <tr class=" text-left" style="border:none;">															
										    					<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Total Value : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="total_value_credit_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Total Cost : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="total_cost_credit_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
		
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Total Profit : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="total_profit_credit_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>

															</tr>
															
															<tr class=" text-left" style="border:none;">															
										    					<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Current Value : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="current_value_credit_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Current Cost : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="current_cost_credit_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
		
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Current Profit : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="current_profit_credit_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>

															</tr>
																<tr class=" text-left" style="border:none;">															
										    					<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Pending Value : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="pending_value_credit_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Pending Cost : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="pending_cost_credit_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
		
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Pending Profit : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="pending_profit_credit_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>

															</tr>
															
															
			             	    				
			             	    				</table>
			                          </div>
			                         </div>
			                   </div>
             	    		       
	                </div>
	                 <div class="col-lg-6">
             	    		  <div class="panel panel-default">
                      			
                       		   <div class="panel-body">
                        			 <div class="dataTable_wrapper">
                        			 
                        			 		<table class="table table-bordered table-hover table-sortable " id="table_cash_summary" style="border:none;">
			             	    					 <tr class=" text-left" style="border:none;">															
										    					<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Total Invoice : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="total_num_cash_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Completed Invoice : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="completed_num_cash_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Progress : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="progress_cash_inv" class="text-left" style="font-size: 17px;">XX %</label>
																</td>
																
															</tr>
															 <tr class=" text-left" style="border:none;">															
										    					<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Total Value : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="total_value_cash_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Total Cost : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="total_cost_cash_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
		
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Total Profit : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="total_profit_cash_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>

															</tr>
															
															<tr class=" text-left" style="border:none;">															
										    					<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Current Value : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="current_value_cash_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Current Cost : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="current_cost_cash_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
		
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Current Profit : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="current_profit_cash_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>

															</tr>
																<tr class=" text-left" style="border:none;">															
										    					<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Pending Value : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="pending_value_cash_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Pending Cost : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="pending_cost_cash_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
		
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Pending Profit : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="pending_profit_cash_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>

															</tr>
															
															
			             	    				
			             	    				</table>
			             	    		
			                            </div>
			                         </div>
			                   </div>
             	    		       
	                </div>

            </div>
            <div class="row" id="row_2">
				                <div class="col-lg-6">
				                		  <div class="panel panel-default">
		                      				 	  <div class="panel-heading">
		                         						 Short Bill
		                       					 </div>
                       		 			   <div class="panel-body">
			             	    			<table class="table table-striped table-bordered table-hover" id="dataTables-short-bill">
			                                    <thead>
			                                        <tr>	
			                                        	  <th>Inv No</th>
			                                       		 <th>Bill Date</th> 
			                                       		 <th>Cus Name</th>
			                                       		  <th>Tag</th>
			                                             <th>Total Value</th>
		                                                  <th>Total Profit</th>
			                              
			                                             <th></th>
			                                            
			                                        </tr>
			                                    </thead>
			                                    <tbody>
			                                    
			                                    </tbody>
			                                </table>
			             	    		       
				                		</div>
				                </div>
				           </div>
				           <div class="col-lg-6">
             	    		  <div class="panel panel-default">
                      				  <div class="panel-heading">
                         						 Purchase Bill
                       					 </div>
                       		   <div class="panel-body">
                        			 <div class="dataTable_wrapper">
			             	    			<table class="table table-striped table-bordered table-hover" id="dataTables-purchase-inv">
			                                    <thead>
			                                        <tr>	
			                                        
			                                       		 <th style="width: 70px;" >Inv Date</th> 
			                                             <th>Vendor Name</th>
														 <th>Inv No.</th>
														 <th>Status</th>
			                                             <th>Total Value</th>

			                                            <th style="width: 20px;"></th>
			                                            
			                                        </tr>
			                                    </thead>
			                                    <tbody>
			                                    
			                                    </tbody>
			                                </table>
			                            </div>
			                         </div>
			                   </div>
             	    		       
	                </div>
             </div>
              <div class="row" id="row_3">
            		<div class="col-lg-6">
             	    		  <div class="panel panel-default">
                      			
                       		   <div class="panel-body">
                        			 <div class="dataTable_wrapper">
                        			 			<table class="table table-bordered table-hover table-sortable " id="table_daily_novat_summary" style="border:none;">
			             	    					 <tr class=" text-left" style="border:none;">															
										    					<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Total Bill : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="total_num_daily_novat" class="text-left" style="font-size: 17px;">XX</label>
																</td>
															</tr>
															 <tr class=" text-left" style="border:none;">															
										    					<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Total Value : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="total_value_daily_novat" class="text-left" style="font-size: 17px;">XX</label>
																</td>
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Total Cost : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="total_cost_daily_novat" class="text-left" style="font-size: 17px;">XX</label>
																</td>
		
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Total Profit : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="total_profit_daily_novat" class="text-left" style="font-size: 17px;">XX</label>
																</td>

															</tr>

			             	    				</table>
			                          </div>
			                         </div>
			                   </div>
             	    		       
	                </div>
        			<div class="col-lg-6">
             	    		  <div class="panel panel-default">
                      			
                       		   <div class="panel-body">
                        			 <div class="dataTable_wrapper">
                        			 			<table class="table table-bordered table-hover table-sortable " id="table_purchase_summary" style="border:none;">
			             	    					 <tr class=" text-left" style="border:none;">															
										    					<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Total Invoice : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="total_num_purchase_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Direct Invoice : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="num_direct_purchase_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Indirect Invoice : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="num_indirect_purchase_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
														
																
															</tr>
															 <tr class=" text-left" style="border:none;">															
										    					<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Total Value : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="total_value_puchase_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
																
																<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Total Direct Inv  : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="total_value_direct_puchase_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>
																
																		<td class=" col-lg-2 text-left" style="border:none;">
														      			 <label class="text-left" style="font-size: 17px;">Total Indirect Inv  : </label>
														      	</td>
																<td class=" col-lg-1 text-left" style="border:none;">
																			 <label id="total_value_indirect_puchase_inv" class="text-left" style="font-size: 17px;">XX</label>
																</td>

															</tr>
															
						
															
															
			             	    				
			             	    				</table>
			                          </div>
			                         </div>
			                   </div>
             	    		       
	                </div>
        

        
        </div>
        <!-- /#page-wrapper -->
           
        <div class="modal fade" id="createReportModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style=" position: fixed; top: 10%; left: 10%; right: 10%; bottom: 15% ">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Generate Report</h4>
                                        </div>
                                        <div class="modal-body">
                                        	 <input id="type_of_generate" type="text" class="form-control" placeholder="Type of Generate" readonly>
                                        	  <br>
                                        	  
                                        	  <div class="form-group">
												  <label for="sel1">Form of Report :</label>
												  <select class="form-control" id="select_form_of_report">
												        <option value = "external_use">External Use</option>
												        <option value = "internal_use">Internal Use</option>
												  </select>
												</div>
												                                        	  
                                        	  <br>
                                        	  <label id="label_of_generate" class="form-control text-center"></label>
                                             

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" name="submit_create_customer"  id="submit_create_customer" class="btn btn-primary" onclick="generate_report()">Generate</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
      </div>
      
      	<form id="form_file_path_gen" method="get" action="../DownloadServlet">
		 <input id="inv_file_name" name="inv_file_name" type="hidden">		
		 <input id="inv_file_path" name="inv_file_path" type="hidden">	
		 	
		
	</form>	  
	 
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
                  
                         <div class="modal-dialog">
                          	 <div class="col-sm-6 col-sm-offset-3 text-center">
									 <div class="container">
													<div class="row">
														
												            <div id="loading">
												                <ul class="bokeh">
												                    <li></li>
												                    <li></li>
												                    <li></li>
												                </ul>
													            </div>
													</div>
									</div>
									
								</div>
 
                          </div>
                     
                          <!-- /.modal-content -->
             
                      <!-- /.modal-dialog -->
      </div>

    </div>
    </div>
    
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
   
	
    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script src="../dist/js/dataTables.tableTools.js"></script>
     <script src="../dist/js/raphael-min.js"></script>
     <script src="../dist/js/morris.min.js"></script>
     
     
     <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <!--     
    
     <script src="../dist/js/chart.js"></script>
 -->
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
    	


    	
    	$("#includedSidebar").load("sidebar.jsp"); 
    	
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').fadeOut();
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        
        
        
        $('#dataTables-credit-inv').DataTable({
            responsive: true,
            "aLengthMenu": [[25, 50, 75, -1], [1, 3, 5]],
            "iDisplayLength": 10 ,
            lengthChange: false 
            ,
            "sDom": 'T<"clear">lfrtip' ,
            "oTableTools": {
                    "aButtons": [
	                                   {
	                                       "sExtends":    "text",
	                                       "fnClick": function ( nButton, oConfig, oFlash ) {
	                                          	show_modal_create_summary_credit_report();
	                                       },
	                                       "sButtonText": "<i class='glyphicon glyphicon-list-alt'></i>",
	                                        "sExtraData": [ 
	                                                            { "name":"operation", "value":"downloadcsv" }       
	                                                      ]
	                                    
	                                   }
                   			    ]
                }

   	    });
     //   dataTables-purchase-inv
        
        $('#dataTables-cash-inv').DataTable({
            responsive: true,
            "aLengthMenu": [[25, 50, 75, -1], [1, 3, 5]],
            "iDisplayLength": 10 ,
            lengthChange: false 
            ,
            "sDom": 'T<"clear">lfrtip' ,
            "oTableTools": {
                    "aButtons": [
	                                   {
	                                       "sExtends":    "text",
	                                       "fnClick": function ( nButton, oConfig, oFlash ) {
	                                          	show_modal_create_summary_cash_report();
	                                       },
	                                       "sButtonText": "<i class='glyphicon glyphicon-list-alt'></i>",
	                                        "sExtraData": [ 
	                                                            { "name":"operation", "value":"downloadcsv" }       
	                                                      ]
	                                    
	                                   }
                   			    ]
                }

   	    });
     
        $('#dataTables-short-bill').DataTable({
            responsive: true,
            "aLengthMenu": [[25, 50, 75, -1], [1, 3, 5]],
            "iDisplayLength": 10 ,
            lengthChange: false 
            ,
            "sDom": 'T<"clear">lfrtip' ,
            "oTableTools": {
                    "aButtons": [
	                                   {
	                                       "sExtends":    "text",
	                                       "fnClick": function ( nButton, oConfig, oFlash ) {
	                                          	show_modal_create_summary_sb_report();
	                                       },
	                                       "sButtonText": "<i class='glyphicon glyphicon-list-alt'></i>",
	                                        "sExtraData": [ 
	                                                            { "name":"operation", "value":"downloadcsv" }       
	                                                      ]
	                                    
	                                   }
                   			    ]
                }

   	    });
     
        $('#dataTables-purchase-inv').DataTable({
            responsive: true,
            "aLengthMenu": [[25, 50, 75, -1], [1, 3, 5]],
            "iDisplayLength": 10 ,
            lengthChange: false 
            ,
            "sDom": 'T<"clear">lfrtip' ,
            "oTableTools": {
                    "aButtons": [
	                                   {
	                                       "sExtends":    "text",
	                                       "fnClick": function ( nButton, oConfig, oFlash ) {
	                                          	show_modal_create_summary_purchase_report();
	                                       },
	                                       "sButtonText": "<i class='glyphicon glyphicon-list-alt'></i>",
	                                        "sExtraData": [ 
	                                                            { "name":"operation", "value":"downloadcsv" }       
	                                                      ]
	                                    
	                                   }
                   			    ]
                }

   	    });
    	fetch_monthly_summary_report_detail();

    });
    </script>

</body>

</html>
