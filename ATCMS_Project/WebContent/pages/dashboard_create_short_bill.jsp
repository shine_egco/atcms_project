<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../dist/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
  <link href="../bower_components/sidebar/sidebar_style_3.css" rel="stylesheet" type="text/css"> 

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- 
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	 -->
	 <style>
				body.modal-open #wrap{
				    -webkit-filter: blur(7px);
				    -moz-filter: blur(15px);
				    -o-filter: blur(15px);
				    -ms-filter: blur(15px);
				    filter: blur(15px);
				}
				  
				.modal-backdrop {background: #f7f7f7;}
				
				.close {
				    font-size: 50px;
				    display:block;
				}
				
				.modal {
				    position: fixed;
				    top: 40%;
				    left: 10%;
				    right: 10%;
				    bottom: 15%;
				}			
	 </style>
	 <style type="text/css">
		 #loading {
			    background: #f4f4f2 url("img/page-bg.png") repeat scroll 0 0;
			    height: 100%;
			    left: 0;
			    margin: auto;
			    position: fixed;
			    top: 0;
			    width: 100%;
			}
			.bokeh {
			    border: 0.01em solid rgba(150, 150, 150, 0.1);
			    border-radius: 50%;
			    font-size: 100px;
			    height: 1em;
			    list-style: outside none none;
			    margin: 0 auto;
			    position: relative;
			    top: 35%;
			    width: 1em;
			    z-index: 2147483647;
			}
			.bokeh li {
			    border-radius: 50%;
			    height: 0.2em;
			    position: absolute;
			    width: 0.2em;
			}
			.bokeh li:nth-child(1) {
			    animation: 1.13s linear 0s normal none infinite running rota, 3.67s ease-in-out 0s alternate none infinite running opa;
			    background: #00c176 none repeat scroll 0 0;
			    left: 50%;
			    margin: 0 0 0 -0.1em;
			    top: 0;
			    transform-origin: 50% 250% 0;
			}
			.bokeh li:nth-child(2) {
			    animation: 1.86s linear 0s normal none infinite running rota, 4.29s ease-in-out 0s alternate none infinite running opa;
			    background: #ff003c none repeat scroll 0 0;
			    margin: -0.1em 0 0;
			    right: 0;
			    top: 50%;
			    transform-origin: -150% 50% 0;
			}
			.bokeh li:nth-child(3) {
			    animation: 1.45s linear 0s normal none infinite running rota, 5.12s ease-in-out 0s alternate none infinite running opa;
			    background: #fabe28 none repeat scroll 0 0;
			    bottom: 0;
			    left: 50%;
			    margin: 0 0 0 -0.1em;
			    transform-origin: 50% -150% 0;
			}
			.bokeh li:nth-child(4) {
			    animation: 1.72s linear 0s normal none infinite running rota, 5.25s ease-in-out 0s alternate none infinite running opa;
			    background: #88c100 none repeat scroll 0 0;
			    margin: -0.1em 0 0;
			    top: 50%;
			    transform-origin: 250% 50% 0;
			}
			@keyframes opa {
			12% {
			    opacity: 0.8;
			}
			19.5% {
			    opacity: 0.88;
			}
			37.2% {
			    opacity: 0.64;
			}
			40.5% {
			    opacity: 0.52;
			}
			52.7% {
			    opacity: 0.69;
			}
			60.2% {
			    opacity: 0.6;
			}
			66.6% {
			    opacity: 0.52;
			}
			70% {
			    opacity: 0.63;
			}
			79.9% {
			    opacity: 0.6;
			}
			84.2% {
			    opacity: 0.75;
			}
			91% {
			    opacity: 0.87;
			}
			}
			
			@keyframes rota {
			100% {
			    transform: rotate(360deg);
			}
			}
			
			
    </style>
	 
	<script type="text/javascript" src="../js/jquery-2.1.1.min.js"></script>

	
		
	<script>
			
	
function get_short_inv_no(){
		
		var current_date = new Date();
        var c_year =  current_date.getFullYear();
        var int_year = parseInt(c_year,10) + parseInt(543,10) - parseInt(2500,10);
		
		var temp_inv_no =  document.getElementById("temp_inv_no");
		var temp_inv_no_value =  document.getElementById("temp_inv_no").value;
		
		 var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
				//	alert("I'm Back from get_credit_inv_no_background");//////////////////
					
					//alert(xmlhttp.responseText);
					
				//	temp_inv_no_value = "55/5555";
					document.getElementById("temp_inv_no").value = xmlhttp.responseText;
					
					
				
				    }
				else{
					
					
					//alert("Cannot get latest Inv No.");
				}// end if check state
			}// end function
			

			xmlhttp.open("POST", "get_short_inv_no_background.jsp?year="+int_year, true);
			xmlhttp.send();

		
	}
		
		function set_product_to_table(keyword){
			
			  var  table = $('#dataTables-example-product').DataTable();
			       table.clear();

			  var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
					//	alert("I'm Back");//////////////////
						
				
						var jsonObj = JSON.parse(xmlhttp.responseText);
					
						//alert("I'm Back");
						
						if(jsonObj.length == 0) {
						//	massage();
							//alert("Error Occer Can't get all customer list");
							
						}
						else{

							for(i in jsonObj) {
								$('#dataTables-example-product').DataTable().row.add([
	                              '<tr><td>'+jsonObj[i].nameTH+'</td>'
	                              ,'<td>'+jsonObj[i].nameEN+'</td>'
	                       //       ,'<td>'+jsonObj[i].category+'</td>'
	                              ,'<td><button id = "'+jsonObj[i].productID+'" type="button" class="btn gray-light btn-circle btn-md" onclick = "show_modal_edit_product_name(this.id)">'
	                              +'<i class="glyphicon glyphicon-wrench"></i></button> </td>'
	                              ,'<td><button id = "'+jsonObj[i].productID+'" type="button" class="btn btn-info btn-circle btn-md" onclick = "grap_product(this.id)">'
	                              +'<i class="glyphicon glyphicon-chevron-right"></i></button> </td></tr>'

	                              
	                                                          
	                            ]).draw();
							}
				
						}
						
						
						//System.out.println("temp_x:"+temp_x);	
					
					
					}// end if check state
				}// end function
				

				xmlhttp.open("POST", "get_product_by_keyword_background.jsp?keyword="+keyword, true);
				xmlhttp.send();

			
		}
		function init_group_option(){
			
			  var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
						
						var jsonObj = JSON.parse(xmlhttp.responseText);
						if(jsonObj.length == 0) {
							//	massage();
								alert("Error Can't get group_code_desc");
						}
						else{
						
						//	var sel_group_code = document.getElementById("sel_group_code");
							for(i in jsonObj) {
								
								$("#sel_group_code").append($('<option>', { 
								        value: jsonObj[i].code,
								        text : jsonObj[i].shortDescription 
								    }));
								
							}
							
							$("#sel_group_code").append($('<option>', { 
						        value: "ATC_PRD",
						        text : "ATC_PRD" 
						    }));
							
						
						}
						
					
					
					
					}// end if check state
				}// end function
				

				xmlhttp.open("POST", "get_group_code_description_background.jsp", true);
				xmlhttp.send();
			
			
			 
		}
	
	
		

		
		function create_short_bill(but_id){
			
   		    $('#myModal').modal('show');
		   
			//alert("call fetch_cus_data :"+company_id);
			var checking ="";
			var short_bill_main_id ; 
			///////////////////////////////////////////
			var cus_name = document.getElementById('cus_name');
			var cus_name_value = cus_name.value ; 
			///////////////////////////////////////////
			var cus_tel = document.getElementById('cus_tel');
			var cus_tel_value = cus_tel.value ; 

			////////////////////////////////////////////////////
			
			var select_tag = document.getElementById('select_tag');
			var select_tag_value = select_tag.options[select_tag.selectedIndex].value;
			////////////////////////////////////////////////////
			
			var total_vat = document.getElementById("total_vat");
			var total_vat_value = total_vat.value;
			////////////////////////////////////////////////////
			var total_value = document.getElementById("total_value");
			var total_value_value = total_value.value;
			////////////////////////////////////////////////////
			var total_inc_vat = document.getElementById("total_inc_vat");
			var total_inc_vat_value = total_inc_vat.value;
			///////////////////////////////////////////////////
			var bill_date = document.getElementById("bill_date");
			var bill_date_value = bill_date.value;
			///////////////////////////////////////////////////
			var note = 	 document.getElementById("note");
			var note_value = note.value ;
				///////////////////////////////////////////////////
		
			
			
			var temp_inv_no =  document.getElementById("temp_inv_no");
			var temp_inv_no_value =  document.getElementById("temp_inv_no").value;
				
			var row_count = $('#tab_logic > tbody  > tr').length;
			//alert("row_count:"+row_count);
			
			
		
			if((row_count>1)&&(temp_inv_no_value!="")&&(bill_date_value!="")){
				
				var pd_array = new Array();
	
					$('#tab_logic > tbody  > tr').each(function() {
						if(this.id=="addr0")
						{
							
						}else{
							
							var id ;
							if(this.id.length==5)
							{
								id = this.id.slice(-1);
							}else{
								id = this.id.slice(-2);
							}
							//alert("id:"+id);
							
							pd_array.push(id);		
						}
						
					});
				
					
					for(var i=0 ; i<pd_array.length ; i++){
						
						//alert(pd_array[i]);
					}
					
					var index =  parseInt(pd_array.length, 10);
					
					
					
					var parameter_short_bill_main = "customer_name="+cus_name_value+
												 "&customer_tel="+cus_tel_value+
												"&total_vat="+total_vat_value+
												"&total_value="+total_value_value+
												"&total_inc_vat="+total_inc_vat_value+
												"&bill_date="+bill_date_value+
												"&temp_inv_no_value="+temp_inv_no_value+
												"&tag="+select_tag_value+
												"&note="+note_value+
												"&index="+index;
					
					var parameter_short_bill_detail ="";
				//	alert("index:"+index);
					for(var j=0;j<index ;j++)
					{
				
						var pd_id = document.getElementById("raw_id"+pd_array[j]);
						var pd_name = document.getElementById("name"+pd_array[j]);
						var pd_price = document.getElementById("price"+pd_array[j]);
						var pd_quantity = document.getElementById("quantity"+pd_array[j]);
						var pd_sum = document.getElementById("sum"+pd_array[j]);
						var pd_unit = document.getElementById("unit"+pd_array[j]);
						//alert("pd_id:"+pd_id.value);
						/*
						if((pd_addition.value==null)||(pd_addition.value==""))
						{
							pd_addition_value="-";
						}else{
							pd_addition_value = pd_addition.value;
						}
						*/
						 parameter_short_bill_detail =	parameter_short_bill_detail +
								 						"&pd_id"+j+"="+pd_id.value+ 
														"&pd_name"+j+"="+pd_name.value+
				//										"&pd_addition"+j+"="+pd_addition_value+
														"&pd_price"+j+"="+pd_price.value+
														"&pd_quantity"+j+"="+pd_quantity.value+
														"&pd_sum"+j+"="+pd_sum.value+
														"&pd_unit"+j+"="+pd_unit.value;
										
					}

					var xmlhttp;
					
					if(window.XMLHttpRequest) {
							// code for IE7+, Firefox, Chrome, Opera, Safari
							xmlhttp = new XMLHttpRequest();
					}
					else {
							// code for IE6, IE5
							xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					
					xmlhttp.onreadystatechange = function() {     
					
						if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
								var result = xmlhttp.responseText;
								if(result=="error")
								{
									alert("Error Occer can't create Main Order Ticket")
								}
								else{

									cash_bill_main_id = result;

								
									setTimeout( function() { $('#myModal').modal('hide'); }, 1200);
									window.location = "dashboard_main.jsp";
									
								}
						
							}		
					}
					
				//	alert(parameter_short_bill_detail);
					xmlhttp.open("POST", "create_short_bill_full_background.jsp?"+parameter_short_bill_main+parameter_short_bill_detail, true);
					xmlhttp.send();
					
					
					
			}else{
				$('#myModal').modal('hide');
				alert("Some Parameter Missing");
				//  $("#myModal").modal('hide');
			  
			}
				
	
			
		}
		function grap_product(product_id)
		{
			
			
			if(product_id != "") {
						
						/* AJAX */
						var xmlhttp;
						
						if(window.XMLHttpRequest) {
							// code for IE7+, Firefox, Chrome, Opera, Safari
							xmlhttp = new XMLHttpRequest();
						}
						else {
							// code for IE6, IE5
							xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
						}
						
						xmlhttp.onreadystatechange = function() {
							if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
								var jsonObj = JSON.parse(xmlhttp.responseText);
								
								if(jsonObj.length == 0) {
			
									alert("no  Data");
								}
								else{
								//	alert("get Data");
									
									  var newid = 0;
								        $.each($("#tab_logic tr"), function() {
								            if (parseInt($(this).data("id")) > newid) {
								                newid = parseInt($(this).data("id"));
								            }
								        });
								        newid++;
								        
								        var tr = $("<tr></tr>", {
								            id: "addr"+newid,
								            "data-id": newid
								        });
								        
								     
								        // loop through each td and create new elements with name of newid
								            $.each($("#tab_logic tbody tr:nth(0) td"), function() {
								                var cur_td = $(this);
								                
								                var children = cur_td.children();
								                
								                // add new td and element if it has a name
								                if ($(this).data("name") != undefined) {
								                    var td = $("<td></td>", {
								                        "data-name": $(cur_td).data("name")
								                    });
								                    
								                    var c = $(cur_td).find($(children[0]).prop('tagName')).clone().val("");
								                    c.attr("name", $(cur_td).data("name") + newid);			
								                    c.attr("id", $(cur_td).data("name") + newid);		
								                    c.appendTo($(td));
								                    td.appendTo($(tr));
								                  
								                
								                 
							                 
	
								                    
								                } else {
								                    var td = $("<td></td>", {
								                        'text': $('#tab_logic tr').length
								                    }).appendTo($(tr));
								                }
								            });
								        	
								            

								            
								            // add the new row
								            $(tr).appendTo($('#tab_logic'));
								        
								            $(tr).find("td button.row-remove").on("click", function() {
								                 $(this).closest("tr").remove();
								                 calculate_total();
								            });
								            
								            var  pd_name = document.getElementById("name"+newid);
								            var  pd_unit = document.getElementById("unit"+newid);
								           // var  pd_addition = document.getElementById("addition"+newid);
								            var  pd_price = document.getElementById("price"+newid); 
								            var  pd_quantity = document.getElementById("quantity"+newid); 
								            var  pd_sum = document.getElementById("sum"+newid); 
								            var  pd_id = document.getElementById("raw_id"+newid); 
								            var  pd_lan_flag = document.getElementById("lan_flag"+newid); 

									             pd_name.value =  jsonObj[0].displayName;
								                 pd_unit.value = jsonObj[0].displayUnit;
								            	 pd_price.value = parseFloat(jsonObj[0].pricePool).toFixed(2);
								            	 pd_lan_flag.value = jsonObj[0].productLanFlag;
							            		 pd_quantity.value = 1;
							            		 pd_sum.value = pd_price.value ;
							            		 pd_id.value = jsonObj[0].productID;

												 calculate_total();
								}
							}
							
						}
						
						xmlhttp.open("POST", "fetch_product_data_background.jsp?product_id="+product_id, true);
						xmlhttp.send();
			}
			 // Dynamic Rows Code
	        
	        // Get max row id and set new id

			
		}
		function SetShowCurrentData(){
    		
            var current_date = new Date();
            var c_year =  current_date.getFullYear();
            var int_year = parseInt(c_year,10) + parseInt(543,10) - parseInt(2500,10);
            
      		 var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
										
							var jsonObj = JSON.parse(xmlhttp.responseText);
							
							if(jsonObj.length == 0) {
		
								alert("no  Data");
							}
							else{
									//var text = jsonObj[i].nameTH+" "+jsonObj[i].nameEN;
									var init_credit_inv_no = String(jsonObj[0].initCreditInvNo);
									var add = 4 - init_credit_inv_no.length;
									
									for(var i=0;i<add;i++)
									{
										init_credit_inv_no = "0"+ init_credit_inv_no;
									}
									
									
								
								   // display inv_no
								   // document.getElementById("box_inv_credit_no").innerHTML = "("+int_year+"/"+init_credit_inv_no+")";
								
							}

		

							
					}// end if check state
				}// end function
				

				xmlhttp.open("POST", "get_current_both_inv_no_background.jsp", true);
				xmlhttp.send();

    	}
		
	
		
		
		function calculate_sum(id_in)
		{
			

			var id ;
			if((id_in.length==6)||(id_in.length==9))
			{
				//alert("<10");
				id = id_in.slice(-1);
				
			}else{
				//alert("over 10");
				id = id_in.slice(-2);
			}

			var sum_price = 0;
			var pd_price = document.getElementById("price"+id);
			var pd_quantity = document.getElementById("quantity"+id);
			
				sum_price = pd_price.value * pd_quantity.value ; 
				
			var pd_sum = document.getElementById("sum"+id);
				pd_sum.value = parseFloat(sum_price).toFixed(2); 
			
			
				calculate_total();
				 
		}
		function calculate_total(){
			
			//alert("calculate_total");
			
			var tbody = document.getElementById("pd_tbody");
			var total_value = document.getElementById("total_value");
			var total_vat = document.getElementById("total_vat");
			
			var total_inc_vat = document.getElementById("total_inc_vat");
			var total_novat = 0 ;
				total_vat.value = 0 ;
				total_inc_vat.value = 0 ;
				total_value.value = 0;
				
			$('#tab_logic > tbody  > tr').each(function() {
				
					//alert(this.id);
					if(this.id=="addr0")
					{
		
					}else{
					//	alert(this.id);
						var id = this.id.substring(4);
							//alert(id);
						var sum = document.getElementById("sum"+id);
						
					
						
						 total_novat = parseFloat(total_novat, 10) + parseFloat(sum.value, 10 ) ; 
						 
						//alert("total_novat:"+total_novat+" sum.value:"+sum.value);
						
					}
				
			 });
			
			//need many scenario for testing 
			
			console.log("total_novat(1095):"+total_novat);
			var total_vat_string = String(((total_novat * 0.07) * 10 / 10).toFixed(2));
			
			console.log("total_vat_string(1097):"+total_vat_string);
			
			//var total_inc_vat_string = String(((total_novat)+(total_novat * 0.07) * 10 / 10).toFixed(2));
			var total_inc_vat_string = String(((total_novat * 1.07) * 10 / 10).toFixed(2));
			
			console.log("total_inc_vat_string(1102):"+total_inc_vat_string);
			
			var final_total_vat;
			var final_total_inc_vat;
			
			if(total_vat_string.indexOf(".")>0)
			{
				var total_vat_string_parts = total_vat_string.split('.');
				final_total_vat = total_vat_string_parts[0] +"."+ total_vat_string_parts[1].substring(0,2);
			}else{
				final_total_vat = total_vat_string;
			}
		
			
			if(total_inc_vat_string.indexOf(".")>0)
			{
				var total_inc_vat_string_parts = total_inc_vat_string.split('.');
				final_total_inc_vat = total_inc_vat_string_parts[0] +"."+ total_inc_vat_string_parts[1].substring(0,2);
			}
			else{
				final_total_inc_vat = total_inc_vat_string;
			}
		
			
		//	total_value.value = ( parseFloat(final_total_inc_vat) - parseFloat(final_total_vat) ); Fix bug 
			total_value.value = parseFloat(total_novat);
		
			console.log("total_value(1122):"+total_value.value);
			
			total_vat.value =   parseFloat(final_total_vat);
			total_inc_vat.value =	parseFloat(final_total_inc_vat);
			
			
			
		}
		
		function toggle_product_language(button){
    		
    		var lan_flag = button.value ; 

		//	alert(button.id);
    		var prefix = button.id.substring(8);
    		var pd_id = document.getElementById("raw_id"+prefix); 
    		//alert(pd_id);
    		
    		
    		var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
				  //  $('#show_inv_history').modal('show');
					var jsonObj = JSON.parse(xmlhttp.responseText);

					if(jsonObj.length == 0) {
					//	massage();
						alert("Product not found !!");
					}
					else{
						//alert("get product detail");
						var pd_name = document.getElementById("name"+prefix);
						var pd_unit = document.getElementById("unit"+prefix);
						if(lan_flag=="EN")
						{
							pd_name.value = jsonObj[0].nameTH;
							pd_unit.value = jsonObj[0].unitTH;
							button.value = "TH";
						}else{
							pd_name.value = jsonObj[0].nameEN;
							pd_unit.value = jsonObj[0].unitEN;
							button.value = "EN";
						}
			
					}

				}
				
			}
			xmlhttp.open("POST", "get_product_detail_by_product_id_background.jsp?product_id="+pd_id.value, true);
			xmlhttp.send();

    		
    	}
		
		

		function submit_edited_product(){
    		
			var name_th = document.getElementById("edited_pro_name_th");
			var unit_th = document.getElementById("edited_pro_unit_th");
			var name_en = document.getElementById("edited_pro_name_en");
			var unit_en = document.getElementById("edited_pro_unit_en");
			
			var product_id = document.getElementById("edited_product_id");
			
			
			var parameter = "product_id="+product_id.value+
							"&name_th="+name_th.value+
							"&unit_th="+unit_th.value+
							"&name_en="+name_en.value+
							"&unit_en="+unit_en.value;
			
			//alert(parameter);
			
			var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
				  //  $('#show_inv_history').modal('show');
					//var jsonObj = JSON.parse(xmlhttp.responseText);
					
					alert(xmlhttp.responseText);
					
					$('#editProModel').modal('hide');

				}
				
			}
			xmlhttp.open("POST", "update_product_name_background.jsp?"+parameter, true);
			xmlhttp.send();

    	}
    	function fetch_old_product_name_to_edit_modal(id){
    		
			var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {			
					
					var jsonObj = JSON.parse(xmlhttp.responseText);
					if(jsonObj.length == 0) {
						//	massage();
							alert("Product not found !!");
						}
						else{
							
							var name_th = document.getElementById("edited_pro_name_th");
							var unit_th = document.getElementById("edited_pro_unit_th");
							var name_en = document.getElementById("edited_pro_name_en");
							var unit_en = document.getElementById("edited_pro_unit_en");
							var edited_product_id = document.getElementById("edited_product_id");
							
								name_th.value = jsonObj.nameTH;
								name_en.value = jsonObj.nameEN;
								unit_th.value = jsonObj.unitTH;
								unit_en.value = jsonObj.unitEN;
								edited_product_id.value = jsonObj.productID;
							
						}
		

				}
				
			}
			xmlhttp.open("POST", "fetch_product_name_by_product_id_background.jsp?product_id="+id, true);
			xmlhttp.send();
    		
    		
    	}
	
    	
		
    	function display_c(){
        	
    		var refresh=1000; // Refresh rate in milli seconds
    		mytime=setTimeout('display_ct()',refresh)
    	
    	}

    	function display_ct() {
    	
    		var strcount;
    		var x = new Date();
    		
    		document.getElementById('live_time').innerHTML = x.toString("dd MMMM yyyy ,HH:mm:ss ");
    		tt=display_c();
    	}

		
	
	</script>
	


</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
    		<nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>
                <div class="sidebar-header">
                    <h3>Bored Sidebar</h3>
                </div>
                
    
        		<div id="includedSidebar"></div>
        		
        	</nav>



        <!-- Navigation -->
       

        <div id="content" class="form-control">
        
        	   <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-justify"></i>
                                <span></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Page</a></li>
                       
                            </ul>
                        </div>
                    </div>
                </nav>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Create Short Bill <small id=box_inv_credit_no></small> </h1>
                    <div class="col-md-2">
                  	  <input type="text" id="temp_inv_no" class="form-control" placeholder="XX/XXXX">
                  	  <br>
                    </div>
         

                
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            
            <div class="row">
           
                <div class="col-lg-6">
                       <div class="panel panel-default">
	                        <div class="panel-heading">
	                           	Product List
	                        </div>
	                        <!-- /.panel-heading -->
	                        <div class="panel-body">
	                        	<div class="dataTable_wrapper">
	                        	   <div class=row>
	                        		<div class="col-lg-4">
	                        		</div>
	                        		<div class="col-lg-6">
	                        		<input type="text"  class="form-control"  id="input_search_prd" placeholder="Search Product Here...">
	                        		</div>
	                        		<div class="col-lg-2">
	                        		<button class='form-control fa glyphicon-plus'  onclick="show_modal_new_product()"></button>
	                        		</div>
	                        	   </div>	
	                                <table class="table table-striped table-bordered table-hover" id="dataTables-example-product">
	                                    <thead>
	                                        <tr>
	                                      <!--   	<th>ID</th>   -->
	                                            <th>Name TH</th>
	                                            <th>Name EN</th>
	                                                         <!-- 
                                            <th>Category</th>
                                             -->
	                                            <th style="width:5%"></th>
	                                            <th style="width:5%"></th>
	                                          
	                                        
	                                        </tr>
	                                    </thead>
	                                    <tbody>
	                                        
	                                        
	                           
	                                       
	                                        
	                                    </tbody>
	                                </table>
	                            </div>
	                        </div>
                    </div>
                    
                    </div>
            
                    <div class="col-lg-6">
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                            New Invoice
	                        </div>
	                        
	                        <!-- /.panel-heading -->
	                        <div class="panel-body">
	                        		
	  
	                           		 <br>
	                      
	                      			
	                      			 
	                        					    <label>Customer Name (ชื่อลูกค้า)</label>                   						
	                        		
	                        					     <input id="cus_name" name = "cus_name" class="form-control" value="" >
	     
																	                        					 	
	                        	   	
	                        						<label> เบอร์โทรศัพท์ : </label>	                        						
	                        				
	                        					 	<input id="cus_tel" name ="cus_tel" class="form-control">
	     
																	                        					 	
	                        	   
	                        	   								<label> Tag  : </label>	 
	                        	 
				                        	   			<div class="form-group">
																
																  <select class="form-control" id="select_tag">
																    <option value="Front">Front</option>
																    <option value="Lazada">Lazada</option>
																    <option value="Shopee">Shopee</option>
											
																  </select>
														</div>     
	                        	   
	                        	   						 <div class="form-group">
														    <label>Note</label>
														    </div>
											
														    <textarea class="form-control" id="note" name="note" rows="4"></textarea>
														<br>
	                        	   		
	                        						<label>Bill Date : (Use B.C. Format idiot!! )  </label>	                        						
	                      
	                        					 	<input type="date" id="bill_date" name ="bill_date" class="form-control" >  					                        					
	                        				
	                        					
	                        	   			
	                        	   				 
	                        
	                    
	                        		<br><br>
	                            	
													<table class="table table-bordered table-hover table-sortable" id="tab_logic" onchange="calculate_total()">
														<thead>
															<tr>
																<th class="text-center">
																	Product Name
																</th>
																<th class="text-center" style="width: 90px;">
																	Unit
																</th>
																<!-- 
																<th class="text-center">
																	Addition 
																</th>
																 -->
																<th class="text-center" style="width: 110px;">
																	Price
																</th>
										    					<th class="text-center" style="width: 90px;">
																	Quantity
																</th>
																<th class="text-center" style="width: 130px;">
																	Sum
																</th>
																
										        				<th class="text-center" style="border-top: 1px solid #ffffff; border-right: 1px solid #ffffff;">
																</th>
																
															</tr>
														</thead>
														<tbody id = "pd_tbody">
														
										    				<tr id='addr0' data-id="0" class="hidden">
																<td data-name="name">
																    <input type="text" name='pd_name' id = "pd_name" class="form-control"/>
																</td>
																<td data-name="unit">
																    <input type="text" name='pd_unit' id = "pd_unit" class="form-control" style="text-align:center;" />
																</td>
																<!-- 
																<td data-name="addition">
																    <input type="text" name='pd_addition' id="pd_addition" placeholder='Color , Perfume Code , etc' class="form-control"/>
																</td>
																 -->
																<td data-name="price">
																    <input type ="text" name = 'pd_price' id="pd_price" class="form-control text-right" oninput="calculate_sum(this.id)" >
																</td>
										    					<td data-name="quantity">
																    <input type ="number" name = 'pd_quantity' id="pd_quantity" class="form-control text-right" oninput="calculate_sum(this.id)" >
																</td>
																<td data-name="sum">
																	 <input readonly type="text" name='pd_sum' id = "pd_sum" class="form-control text-right" onchange="calculate_total()"/>
																	 
																</td>
																
																<td data-name="lan_flag">
																	<input type="button" id="but_lan_flag" class="btn btn-default btn-md pull-right" onclick="toggle_product_language(this)" value="XX"> <br>
																	
																	<input type="hidden" id="pd_lan_flag" >
																	 
																</td>
																
																<td data-name="raw_id" class="hidden">
																	<input type="hidden" name="pd_id" id="pd_id">
																</td>
																 
																
										                        <td data-name="del">
										                            <button name="del0" class='btn btn-danger glyphicon glyphicon-remove row-remove' ></button>
										                        </td>
															</tr>
														</tbody>
													</table>	
													
													<table class="table table-bordered table-hover table-sortable " id="table_summary" style="border:none;">
													
																<tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Total Value : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input  type="text" name="total_value" id="total_value" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>

														 <tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Vat (7%) : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input  type="text" name="total_vat" id="total_vat" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>
												
															<tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Total Inc VAT : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input  type="text" name="total_inc_vat" id="total_inc_vat" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>
															
															<tr>
																<td class=" col-lg-6 text-right" style="border:none;">
														      			 
														      	</td>
														      		
																 <td class=" col-lg-6 text-right" style="border:none;">
																	<div class="btn-group">
																		  <button type="button" id="gen_inv" class="btn btn-primary btn-md"  onclick="create_short_bill(this.id)">Save</button>
																		 
												
																
																		
																		    
													
																	</div>
																</td>
															</tr>
													
													</table>
													                            
	                        </div>
	                        <!-- .panel-body -->
	                    </div>
                    <!-- /.panel -->
                </div>

                <!-- /.col-lg-6 -->
            </div>
            


            <!-- /.row -->
        </div>

 
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
                  
                         <div class="modal-dialog">
                          	 <div class="col-sm-6 col-sm-offset-3 text-center">
									 <div class="container">
													<div class="row">
														
												            <div id="loading">
												                <ul class="bokeh">
												                    <li></li>
												                    <li></li>
												                    <li></li>
												                </ul>
													            </div>
													</div>
									</div>
									
								</div>
 
                          </div>
                     
                          <!-- /.modal-content -->
             
                      <!-- /.modal-dialog -->
      </div>
      
      <div class="modal fade" id="createProModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style=" position: fixed; top: 10%; left: 10%; right: 10%; bottom: 15% ">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Create New Product</h4>
                                        </div>
                                        <div class="modal-body">
                                       		<div class="row">
                                       			.	<div class="col-md-6">
                                       					<input name="new_pro_name_th" id="new_pro_name_th" type="text" class="form-control" placeholder="ชื่อภาษาไทย">
                                       				</div>
                                       				<div class="col-md-4">
                                       					<input name="new_pro_unit_th" id="new_pro_unit_th" type="text" class="form-control" placeholder="หน่วยภาษาไทย">
                                       				</div>
                                       				
                                       		</div>
                                       		<div class="row">
                                       				<br>
                                       				<div class="col-md-6">
                                       					<input name="new_pro_name_en" id="new_pro_name_en" type="text" class="form-control" placeholder="Name Eng">
                                       				</div>
                                       				<div class="col-md-4">
                                       					<input name="new_pro_unit_en" id="new_pro_unit_en" type="text" class="form-control" placeholder="Unit Eng">
                                       				</div>
                                       				<br>
                                       		
                                       		</div>
                                       		<div class=row>
                                       			<br>
                                       			<div class="col-md-4">
	                                       			<select id="sel_category" name="sel_category" class="form-control" onchange="change_category(this)">
	                                       				 <option value="OTH_PRD">Other Product</option>
	 													 <option value="ATC_PRD">ATC Product</option>
	                                       			</select>
	                                       		</div>
	                                       		<div class="col-md-5">
	                                       			<select id="sel_group_code" name="sel_group_code" class="form-control">
	                                       				  
	                                       			</select>
	                                       		</div>
                                       		</div>
                                       		<div class="row">                                      			
                                       			 <div class="col-md-4">
                                       			  <br>
                                       			 <input type="number" name="new_pro_price" id="new_pro_price" class="form-control" placeholder="Price (no VAT)">
                                       			</div>
                                       		</div>
                                              
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" name="submit_create_product"  id="submit_create_product" class="btn btn-primary" onclick="create_single_product()">Create Product</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
      </div>
      <div class="modal fade" id="editProModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style=" position: fixed; top: 10%; left: 10%; right: 10%; bottom: 15% ">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Edit Product Name</h4>
                                        </div>
                                        <div class="modal-body">
                                       		<div class="row">
                                       			.	<div class="col-md-6">
                                       					<input name="edited_pro_name_th" id="edited_pro_name_th" type="text" class="form-control" placeholder="ชื่อภาษาไทย">
                                       				</div>
                                       				<div class="col-md-4">
                                       					<input name="edited_pro_unit_th" id="edited_pro_unit_th" type="text" class="form-control" placeholder="หน่วยภาษาไทย">
                                       				</div>
                                       				
                                       		</div>
                                       		<div class="row">
                                       				<br>
                                       				<div class="col-md-6">
                                       					<input name="edited_pro_name_en" id="edited_pro_name_en" type="text" class="form-control" placeholder="Name Eng">
                                       				</div>
                                       				<div class="col-md-4">
                                       					<input name="edited_pro_unit_en" id="edited_pro_unit_en" type="text" class="form-control" placeholder="Unit Eng">
                                       					<input type="hidden" name="edited_product_id" id="edited_product_id">
                                       				</div>
                                       				<br>
                                       		
                                       		</div>                                              
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" name="submit_edit_product_but"  id="submit_edit_product_but" class="btn btn-primary" onclick="submit_edited_product()">Submit Change</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
      </div>
      
    </div> 
    
    <!-- /#wrapper -->

    <!-- jQuery  -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
     <script src="../dist/js/dataTables.tableTools.js"></script>
      <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
            <script src="../js/date.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    	
    	function create_single_product(){
    		
    		$('#createProModel').modal('hide');
    		var parameter = "";    		
    		var new_pro_name_th = document.getElementById("new_pro_name_th").value;
    		var new_pro_unit_th = document.getElementById("new_pro_unit_th").value;
    		var new_pro_name_en = document.getElementById("new_pro_name_en").value;
    		var new_pro_unit_en = document.getElementById("new_pro_unit_en").value;
    		
    		if(new_pro_name_th=="")
    		{
    			new_pro_name_th = "-";
    		}
    		if(new_pro_unit_th=="")
    		{
    			new_pro_unit_th = "-";
    		}
    		
    		if(new_pro_name_en=="")
    		{
    			new_pro_name_en = "-";
    		}
    		if(new_pro_unit_en=="")
    		{
    			new_pro_unit_en = "-";
    		}
    			
    			
    		var init_price = document.getElementById("new_pro_price").value;
    		
    		var sel_category = document.getElementById("sel_category");
    		var sel_group_code = document.getElementById("sel_group_code");
    		
    		var new_pro_category = sel_category.options[sel_category.selectedIndex].value;
    		var new_pro_group_code = sel_group_code.options[sel_group_code.selectedIndex].value;
    		/*
    		alert("data:"+"\n"+new_pro_name_th+"\n"
    			 + new_pro_unit_th+"\n"
    			 + new_pro_name_en+"\n"
    			 + new_pro_unit_en+"\n"
    			 + new_pro_category+"\n"
    			 + new_pro_group_code+"\n"
    			 + init_price);
    		*/
    		 $('#myModal').modal('show');
 			setTimeout( function() { }, 2000);
    		
			var checking ="Warning:";
    		
    		if((new_pro_unit_th=="")&&(new_pro_name_en=""))
    		{
    			checking = checking + "Product Name TH and EN are NULL ";
    		}
    		
    		if(init_price=="")
    		{
    			checking = checking + "Init price  is NULL";
    		}
 			if(checking=="Warning:")
 			{
 				//alert("Ready");
 				parameter = "new_pro_name_th="+new_pro_name_th
				   +"&new_pro_unit_th="+new_pro_unit_th
				   +"&new_pro_name_en="+new_pro_name_en
				   +"&new_pro_unit_en="+new_pro_unit_en
				   +"&new_pro_category="+new_pro_category
				   +"&new_pro_group_code="+new_pro_group_code
				   +"&init_price="+init_price;
		
    			/* AJAX */
    			var xmlhttp_2;
    			
    			if(window.XMLHttpRequest) {
    				// code for IE7+, Firefox, Chrome, Opera, Safari
    				xmlhttp_2 = new XMLHttpRequest();
    			}
    			else {
    				// code for IE6, IE5
    				xmlhttp_2 = new ActiveXObject("Microsoft.XMLHTTP");
    			}
    			
    			xmlhttp_2.onreadystatechange = function() {  				
    				if(xmlhttp_2.readyState == 4 && xmlhttp_2.status == 200) {						
    				
						var result = xmlhttp_2.responseText;
    					
   						if(result=="fail")
   						{
   							alert("Error Occer contact Admin");
   						}
   						else {
   							$('#myModal').modal('hide');
   							//alert("I'm Back from create_single_product_background SUCCESS");
   							set_product_to_table();
   						}

    				}
    				
    			}
    			alert(parameter);
    			
    			xmlhttp_2.open("POST", "create_single_product_background.jsp?"+parameter, true);
    			xmlhttp_2.send();
 				
 				
 				
 			}else{
 				
 				$('#myModal').modal('hide');
 				alert(checking);
 			}
    		
    		
    		
    	}
    	
    
    	
    	
	    function show_modal_new_product(){
	    	$('#createProModel').modal('show');
	    	
	    }
    	function show_modal_edit_product_name(id){
    		
    		fetch_old_product_name_to_edit_modal(id);
    		$('#editProModel').modal('show');
    		
    	}
	    
    </script>
    
    <script>
    $(document).ready(function() {
  
    	
    	

    	$("#includedSidebar").load("sidebar.jsp"); 
    	
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').fadeOut();
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        
        
    	init_group_option();
    	SetShowCurrentData();
    	 get_short_inv_no();
    	var table_cus = $('#dataTables-example-customer').DataTable({
    	      responsive: true,
              "aLengthMenu": [[25, 50, 75, -1], [1, 3, 5]],
              "iDisplayLength": 3 ,
              lengthChange: false 
              ,
              "sDom": 'T<"clear">lfrtip' ,
              "oTableTools": {
                      "aButtons": [
	                                   {
	                                       "sExtends":    "text",
	                                       "fnClick": function ( nButton, oConfig, oFlash ) {
	                                          	show_modal_new_customer();
	                                       },
	                                       "sButtonText": "<i class='fa glyphicon-plus'></i>",
	                                        "sExtraData": [ 
	                                                            { "name":"operation", "value":"downloadcsv" }       
	                                                      ]
	                                    
	                                   }
                     			 ]
                  }
             
        });
        
     	 $("#input_search_prd").on('keyup', function (e) {
      		  var value = document.getElementById("input_search_prd").value;
      	     if (e.keyCode == 13) {
      		
      			set_product_to_table(value);
      	     }
     		});
      	  
         	 $('#dataTables-example-product').DataTable({
                responsive: true,
                "aLengthMenu": [[25, 50, 75, -1], [1, 3, 5]],
                "bFilter": false,
                "iDisplayLength": 5 ,
                lengthChange: false 

      	    });
        
    //	set_customer_to_table();
     //	set_product_to_table();
        display_ct();
     
        // Sortable Code
        var fixHelperModified = function(e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
        
            $helper.children().each(function(index) {
                $(this).width($originals.eq(index).width())
            });
            
            return $helper;
        };
                                           
        $(".table-sortable tbody").sortable({
            helper: fixHelperModified      
        }).disableSelection();

        $(".table-sortable thead").disableSelection();

		
    });
    </script>

</body>

</html>
