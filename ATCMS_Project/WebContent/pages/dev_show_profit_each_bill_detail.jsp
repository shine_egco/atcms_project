<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

 <script>
	 
	 
	 function show_confirm_delete(id){
		
 		$('#confirm-delete-order').modal('show');
 		//alert(id);
		  document.getElementById("delete_bill_id").value = id;

		 
	 }
	
	 function formatMoney(number, places, symbol, thousand, decimal) {
			number = number || 0;
			places = !isNaN(places = Math.abs(places)) ? places : 0;
			symbol = symbol !== undefined ? symbol : "";
			thousand = thousand || ",";
			decimal = decimal || ".";
			var negative = number < 0 ? "-" : "",
			    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
			    j = (j = i.length) > 3 ? j % 3 : 0;
			return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
	}

	function fetch_uncomplete_credit_inv_to_table(){
		
		var xmlhttp;
		
		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange = function() {
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
				//alert("I'm Back");//////////////////
				
		
				var jsonObj = JSON.parse(xmlhttp.responseText);
			
				//	alert("I'm Back");
				
				if(jsonObj.length == 0) {
			
					alert("0 Row Return");
				}
				else{
					
				//	alert(jsonObj.length);
					for(i in jsonObj) {
						
						$('#credit-dataTables-example').DataTable().row.add([
                            '<tr><td>'+jsonObj[i].invNo+'</td>'
                            ,'<td>'+jsonObj[i].customerName+'</td>'
                            ,'<td><center>'+jsonObj[i].totalValue+'</center></td>'
                            ,'<td><center>'+jsonObj[i].totalCost+'</center></td>'
                            ,'<td><center>'+jsonObj[i].totalProfit+'</center></td>'
                            ,'<button id="'+jsonObj[i].orderId+'" onclick="redirect_order_detail(this.id)" class="btn btn-info btn-circle btn-md"><i class="glyphicon glyphicon-search"></i></button> </td></tr>'	                               
                          ]).draw();
						}
					
				  
					//orderId
				}

			}// end if check state
		}// end function
		

		xmlhttp.open("POST", "get_uncalculated_profit_credit_inv_background.jsp", true);
		xmlhttp.send();
		
		
		
	}
	function fetch_uncomplete_cash_inv_to_table(){
		
		
		var xmlhttp;
		
		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange = function() {
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
				//alert("I'm Back");//////////////////
				
		
				var jsonObj = JSON.parse(xmlhttp.responseText);
			
				//	alert("I'm Back");
				
				if(jsonObj.length == 0) {
			
					alert("0 Row Return");
				}
				else{
					
				//	alert(jsonObj.length);
					for(i in jsonObj) {
						
						$('#cash-dataTables-example').DataTable().row.add([
                                  '<tr><td>'+jsonObj[i].invNo+'</td>'
                                  ,'<td>'+jsonObj[i].customerName+'</td>'
                                  ,'<td><center>'+jsonObj[i].totalValue+'</center></td>'
                                  ,'<td><center>'+jsonObj[i].totalCost+'</center></td>'
                                  ,'<td><center>'+jsonObj[i].totalProfit+'</center></td>'
                                  ,'<button id="'+jsonObj[i].cashBillId+'" onclick="redirect_cash_bill_detail(this.id)" class="btn btn-info btn-circle btn-md"><i class="glyphicon glyphicon-search"></i></button> </td></tr>'	                               
                                ]).draw();
      						}

					//orderId
				}

			}// end if check state
		}// end function
		

		xmlhttp.open("POST", "get_uncalculated_profit_cash_inv_background.jsp", true);
		xmlhttp.send();
		
		
		
	}
	function redirect_order_detail(order_id){
		
		//alert(order_id);
		sessionStorage.setItem("order_id_for_get_detail", order_id); 
		
		window.open("report_credit_inv_detail.jsp");
	}
	function redirect_cash_bill_detail(cash_bill_id){
		
	//	alert(cash_bill_id);
		sessionStorage.setItem("cash_bill_id_for_get_detail", cash_bill_id); 
		
		window.open("report_cash_bill_detail.jsp");
	}
	
	
</script>
<style>
	.text-right {
 		 text-align:right;
	}

</style>
    

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
      		   <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.jsp">ATCMS Admin</a>
            </div>
            <!-- /.navbar-header -->

   
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="dashboard_main.jsp"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-group fa-fw"></i>Customer&Vendor<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="customer_sub.jsp">Customers</a>
                                </li>
                                <li>
                                    <a href="vendor_sub.jsp">Vendors</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                         <li>
                            <a href="#"><i class="fa fa-leaf"></i>  Product<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="atc_product_sub.jsp">ATC Product</a>
                                </li>
                                <li>
                                    <a href="other_product_sub.jsp">Other Product</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o"></i>  Bill Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="direct_bill_sub.jsp">Direct Bill</a>
                                </li>
                                <li>
                                    <a href="indirect_bill_sub.jsp">Indirect Bill</a>
                                </li>
                                <li>
                                    <a href="credit_inv_sub.jsp">Credit Invoice</a>
                                </li>
                                <li>
                                    <a href="cash_inv_sub.jsp">Cash Invoice</a>
                                </li>
                                <li>
                                    <a href="credit_note_sub.jsp">Credit Note (ใบลดหนี้)</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                        	<a href="#"><i class="glyphicon glyphicon-fire"></i> Production (การผลิต)<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">                         
                               <li>
                                    <a href="production_material_main.jsp">Material</a>
                                </li>
                                <li>
                                    <a href="production_work_order_main.jsp">Work Order</a>
                                </li>
                                <li>
                                    <a href="production_formula_main.jsp">Formula</a>
                                </li>
                                 <li>
                                    <a href="production_product_relate_formula.jsp">Product Relate Formula</a>
                                </li>                
                            </ul>
                        </li>
                        <li>
                        	<a href="#"><i class="glyphicon glyphicon-stats"></i> Reporting (รายงาน)<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="monthly_summary_report_main.jsp">Monthly Summary Report</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                        	<a href="#"><i class="glyphicon glyphicon-road"></i> (DEVEL)0pinG Z()Ne <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="dev_show_profit_each_bill_detail.jsp">Each  Bill Value Detail</a>
                                </li>
                                <li>
                                    <a href="dev_balancing_work_order.jsp">Balancing Work Order</a>
                                </li>
                
                            </ul>
                        </li>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        
        
        <div id="page-wrapper">
             <div class="row">
	                <div class="col-lg-12">
	                    <h1 class="page-header">Dev Zone</h1>
	                </div>
                <!-- /.col-lg-12 -->
             </div>

  
             <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Credit Invoice
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        	 <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="credit-dataTables-example">
                                    <thead>
                                        <tr>
                                        	<th style="width: 60px;" >Inv No.</th>
                                            <th>Customer Name</th>
                                            <th style="width: 140px;">Total Value</th>
                                            <th style="width: 140px;">Total Cost</th>
                                            <th style="width: 140px;">Total Profit</th>
                                            <th style="width:1%"></th>
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        
                                      
              
                                        
                                    </tbody>
                                </table>
                            </div>
                        	
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Cash Invoice
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        	 <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="cash-dataTables-example">
                                    <thead>
                                        <tr>
         									<th style="width: 60px;" >Inv No.</th>
                                            <th>Customer Name</th>
                                            <th style="width: 140px;">Total Value</th>
                                            <th style="width: 140px;">Total Cost</th>
                                            <th style="width: 140px;">Total Profit</th>
                                            <th style="width:1%"></th>
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        
                                      
                                        
                                    </tbody>
                                </table>
                            </div>
                        	
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        
         <div class="modal fade" id="confirm-delete-order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			    <div class="modal-dialog">
			        <div class="modal-content">
			        	<form>
					          <div class="modal-body">
								    <textarea id="reason_delete" class="form-control" rows="2" placeholder="Reason for deleting this order"></textarea>
								    
								    <input type="hidden" id="delete_bill_id">
							  </div>
							  <div class="modal-footer">
								    <button type="button" data-dismiss="modal" class="btn btn-danger" id="but_delete_order" onclick="DeleteBill(this.form)">Delete</button>
								    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
							  </div>
						 </form>
			        </div>
			    </div>
		</div>             

        
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    
    
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
        fetch_uncomplete_credit_inv_to_table();
        fetch_uncomplete_cash_inv_to_table();

    });
    </script>
	<script>
    // tooltip demo
	    $('.tooltip-demo').tooltip({
	        selector: "[data-toggle=tooltip]",
	        container: "body"
	    })
	
	    // popover demo
	    $("[data-toggle=popover]")
	        .popover()
    </script>
</body>

</html>
