<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>



<% 

	System.out.println("Start get_order_detail_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

	List<Order> order_list = new ArrayList<Order>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	
	String order_id = request.getParameter("order_id");
	System.out.println("order_id:"+order_id);

      try{
    	  
    	  String sql_query = " SELECT order_main.delivery_date "+
    	  					         " ,order_main.due_date "+
    	  							 " ,order_main.inv_generated_date "+
    	  					         " ,order_main.inv_no "+
    	  							 " ,order_main.create_date "+
    	  					         " ,order_main.order_id "+
    	  							 " ,order_main.status "+
    	  					         " ,order_main.ponum "+
    	  							 " ,order_main.total_inc_vat "+
    	  					         " ,order_main.total_value "+
    	  							 " ,order_main.total_vat "+
    	  					         " ,order_main.type "+
    	  							 " ,company.name_th "+
    	  					         " ,company.name_en "+
    	  							 " ,company.address_th "+
    	  					         " ,company.address_en "+
    	  							 " ,order_main.inv_file_path "+
							" FROM order_main"+
		    			    " JOIN company "+
							" ON  order_main.customer_id = company.company_id "+
							" WHERE BINARY order_main.order_id='"+order_id+"'";
    	  System.out.println("sql_query:"+sql_query);
    	  ResultSet rs_ord = connect.createStatement().executeQuery(sql_query);
          
    	  while(rs_ord.next())
          {
              Order ord = new Order();
              		ord.setCompletedDate("completed_date");
              		ord.setCreateDate("create_date");
              		ord.setCustomerId("customer_id");
              		
              		if(("-").equals(rs_ord.getString("name_th")))
              		{
              			ord.setCustomerName(rs_ord.getString("name_en"));
              			ord.setCustomerAddress(rs_ord.getString("address_en"));
              			
              		}else{
              			ord.setCustomerName(rs_ord.getString("name_th"));
              			ord.setCustomerAddress(rs_ord.getString("address_th"));
              		}
              		
              		ord.setDeliveryDate(rs_ord.getString("delivery_date"));
              		ord.setDueDate(rs_ord.getString("due_date"));
              		ord.setIncGeneratedDate(rs_ord.getString("inv_generated_date"));
              		ord.setInvNo(rs_ord.getString("inv_no"));
              		ord.setCreateDate(rs_ord.getString("create_date"));
              		ord.setOrderId(rs_ord.getString("order_id"));
              		ord.setOrderStatus(rs_ord.getString("status"));
              		ord.setPoNum(rs_ord.getString("ponum"));
					ord.setTotalIncVat(rs_ord.getString("total_inc_vat"));
					ord.setTotalValue(rs_ord.getString("total_value"));
					ord.setTotalVat(rs_ord.getString("total_vat"));
					ord.setType(rs_ord.getString("type"));
					if(rs_ord.getString("inv_file_path")!=null){	
						ord.setInvFilePath(rs_ord.getString("inv_file_path"));
						String pattern = Pattern.quote(System.getProperty("file.separator"));
						String[] parts = rs_ord.getString("inv_file_path").split(pattern);
						int temp = parts.length;
						String inv_file_name = parts[temp-1];
							   
						ord.setInvFileName(inv_file_name);
					}else{
						ord.setInvFilePath("-");
					}
				//	System.out.println("Q:path:"+rs_ord.getString("inv_file_path"));
				//	System.out.println("Q:name:"+inv_file_name);
					
					
              order_list.add(ord);
              
          }
             
         	try {
				Json = mapper.writeValueAsString(order_list); 
				System.out.println("Json:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	out.print(Json);
	   
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  connect.close();
    	  out.print("fail");
      }
     
  
	connect.close();
%>
