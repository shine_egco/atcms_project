<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "com.atcms.*" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "java.util.Properties" %>
<%@ page import = "javax.servlet.ServletContext" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.util.Date" %>



<% 

	System.out.println("Start create_formula_full_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
	///////////////////file_path parameter/////////////
	String form_name_en = request.getParameter("form_name_en");
	String form_name_th = request.getParameter("form_name_th");
	String form_code = request.getParameter("form_code");
	String description = request.getParameter("description");
	String capacity_unit = request.getParameter("capacity_unit");
	String capacity_value = request.getParameter("capacity_value");
	String packing_unit = request.getParameter("packing_unit");
	String total_cost = request.getParameter("total_cost");
	String remark = request.getParameter("remark");

	String net_capacity_value = "";
	
	System.out.println("form_name_en:"+form_name_en);
	System.out.println("form_name_th:"+form_name_th);
	System.out.println("form_code:"+form_code);
	System.out.println("description:"+description);
	System.out.println("capacity_unit:"+capacity_unit);
	System.out.println("capacity_value:"+capacity_value);
	System.out.println("packing_unit:"+packing_unit);
	System.out.println("total_cost:"+total_cost);
	System.out.println("remark:"+remark);
	
	
	switch(capacity_value)
	{
			case "3.8":
				net_capacity_value = "3.5" ; 
				break;
				
			case "20":
				net_capacity_value = "17.5";
				break;
				
			case  "30":
				net_capacity_value = "28";
				break;
				
			default :
				net_capacity_value = "";
	
	}
	
	
	 ///////////////////////////////////////////////////
//	List<Product> product_list = new ArrayList<Product>();

	try {		
		
		String formula_id ="FORM"+ KeyGen.generateFormulaID();
		
		//get current credit_inv_no
		String sql_query = " INSERT INTO `formula_main`(`formula_id` "+
																									 ", `formula_code` "+
																									 ", `name_th` "+
																									 ", `name_en` "+
																									 ", `description` "+
																									 ", `remark` "+
																									 ", `total_cost` "+
																									 ", `capacity` "+
																									 ", `net_capacity` "+
																									 ", `capacity_unit` "+
																									 ", `packaging_unit` )"+
													  " VALUES ( '"+formula_id+" ' "+
															  		   " ,'"+form_code+" ' "+
															  		   " ,'"+form_name_th+" ' "+
															  	       " ,'"+form_name_en+" ' "+
															  	       " ,'"+description+" ' "+
															  	        " ,'"+remark+" ' "+
															  	       " ,'"+total_cost+" ' "+
															  	       " ,'"+capacity_value+" ' "+
															  	       " ,'"+net_capacity_value+" ' "+
															  	       " ,'"+capacity_unit+" ' "+
															  	       " ,'"+packing_unit+" ') ";
		
		System.out.println("sql_query:"+sql_query);
		connect.createStatement().executeUpdate(sql_query);
		
		out.print(formula_id);
		
	} catch (Exception e) {
		
		e.printStackTrace();
		connect.close();
		out.print("error");
	}
	 
	connect.close();
	
%>
