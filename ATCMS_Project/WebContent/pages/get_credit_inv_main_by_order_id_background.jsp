<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>
<%@ page import = "java.text.DateFormat" %>



<% 

	System.out.println("Start get_credit_inv_detail_by_order_id_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

		
	//List<Order> order_list = new ArrayList<Order>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	//String purchase_id = request.getParameter("purchase_id");
	//System.out.println("purchase_id:"+purchase_id);
	 String order_id = request.getParameter("order_id");

	 System.out.println("order_id:"+order_id);



      try{
    	  
    
    		System.out.println("Keyword is Text"); 
       		String	sql_query =  " SELECT * "+
    						"  FROM order_main "+
    						"  JOIN company "+
    						"  ON  order_main.customer_id = company.company_id "+
    						"  WHERE BINARY order_main.order_id ='"+order_id+"'";
  
   			 
   			 

    
    	  System.out.println("sql_query:"+sql_query);
    	  
    	  
    	  ResultSet rs_ord = connect.createStatement().executeQuery(sql_query);
          
    	  
    	  Order ord = new Order();
    	  
    	  while(rs_ord.next())
          {
             
              		ord.setInvNo(rs_ord.getString("inv_no"));
         	  if(rs_ord.getString("name_th").equals("-"))
              {
         			ord.setCustomerName(rs_ord.getString("name_en"));
         			ord.setCustomerAddress(rs_ord.getString("address_en"));
         	  }else{
         		    ord.setCustomerName(rs_ord.getString("name_th"));
         		    ord.setCustomerAddress(rs_ord.getString("address_th"));
         	  }
              		ord.setCustomerId(rs_ord.getString("company_id"));
         	  		ord.setTotalIncVat(rs_ord.getString("total_inc_vat"));
              		ord.setCompletedDate(rs_ord.getString("completed_date"));
              		ord.setCreateDate(rs_ord.getString("create_date"));      
              		ord.setDeliveryDate(rs_ord.getString("delivery_date"));
         	  		ord.setDueDate(rs_ord.getString("due_date"));
       				ord.setIncGeneratedDate(rs_ord.getString("inv_generated_datetime"));
       				ord.setInvFileName(rs_ord.getString("inv_file_path"));
       				ord.setInvNo(rs_ord.getString("inv_no"));
       				ord.setOrderDate(rs_ord.getString("order_date"));
       				ord.setOrderId(rs_ord.getString("order_id"));
       				ord.setOrderStatus(rs_ord.getString("status"));
       				ord.setPoNum(rs_ord.getString("ponum"));
       				ord.setTotalIncVat(rs_ord.getString("total_inc_vat"));
       				ord.setTotalValue(rs_ord.getString("total_value"));
       				ord.setTotalVat(rs_ord.getString("total_vat"));
       				ord.setType(rs_ord.getString("type"));
       				ord.setComputeCredit(rs_ord.getString("compute_credit"));
       				ord.setTaxId(rs_ord.getString("tax_id"));
       				ord.setInvFilePath(rs_ord.getString("inv_file_path"));
       				ord.setInvFileName(rs_ord.getString("inv_file_name"));
       				ord.setNote(rs_ord.getString("note"));
       				ord.setPaymentReference(rs_ord.getString("payment_reference"));
       				ord.setTotalCost(rs_ord.getString("total_cost"));
       				ord.setTotalProfit(rs_ord.getString("total_profit"));
       				
       				
         //     credit_inv_list.add(ord);
            } 
              
          
             
         	try {
				Json = mapper.writeValueAsString(ord); 
				System.out.println("Json:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	
         	
         	out.print(Json);
	   
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  out.print("fail");
      }
     
  
	connect.close();
%>
