<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>
<%@ page import = "java.text.DateFormat" %>



<% 

	System.out.println("Start get_product_relate_customer_history_background");
			//set Database Connection
			String hostProps = "";
			String usernameProps  = "";
			String passwordProps  = "";
			String databaseProps = "";
			
			try {
				//get current path
				ServletContext servletContext = request.getSession().getServletContext();
				
				InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
				Properties props = new Properties();
				
				props.load(input);
			 
				hostProps  = props.getProperty("host"); 
				usernameProps  = props.getProperty("username");
				passwordProps  = props.getProperty("password");
				databaseProps = props.getProperty("database");
				
				System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
						"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
				
			} catch (Exception e) { 
				out.println(e);  
			}
			
			// connect database
			Connection connect = null;		
			try {
				Class.forName("com.mysql.jdbc.Driver");
			
				connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
						"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
			
				if(connect != null){
					System.out.println("Database Connect Sucesses."); 
				} else {
					System.out.println("Database Connect Failed.");	
				}
			
			} catch (Exception e) {
				out.println(e.getMessage());
				e.printStackTrace();
			}

		
	List<ProductSellHistory> psh_list = new ArrayList<ProductSellHistory>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	//String purchase_id = request.getParameter("purchase_id");
	//System.out.println("purchase_id:"+purchase_id);
	 String customer_id = request.getParameter("customer_id");

	 System.out.println("customer_id:"+customer_id);



      try{
    	  
    
    	
       		String	sql_query =  "             SELECT order_detail.product_id  "+
							     "              , order_detail.adt_description  "+
							     "              , order_detail.price "+
							     "              , product.name_th "+
							     "              , product.name_en "+
							     "              , product.unit_en "+
							     "               , product.unit_th "+
							     "               , order_detail.unit "+
							     "                , order_main.inv_no "+
							     "               , order_detail.quantity "+
							     "               , order_detail.cost_per_unit "+
								 "               , order_main.delivery_date AS bill_date "+
							     "         FROM order_detail  "+
							     "         JOIN order_main "+
							     "         ON order_detail.order_id = order_main.order_id "+
							     "         JOIN product "+
							     "         ON order_detail.product_id = product.product_id  "+
							     "         WHERE order_main.customer_id = '"+customer_id +"' "+
							     "         GROUP BY   order_detail.product_id  "+
							     "              , order_detail.adt_description  "+
							     "              , order_detail.price "+
							     "		        , product.name_th "+
							     "              , product.name_en "+
							     "              , product.unit_en "+
							     "              , product.unit_th "+
							     "              , order_detail.unit "+
							     "              , order_main.inv_no "+
							    "               , order_detail.cost_per_unit "+
							     "              , order_detail.quantity "+
							     "              , bill_date "+
							     "         UNION "+
							    		 
							     "         SELECT cash_bill_detail.product_id  "+
							     "              , cash_bill_detail.adt_description  "+
							     "              , cash_bill_detail.price "+
							     "              , product.name_th "+
							     "              , product.name_en "+
							     "              , product.unit_en "+
							     "              , product.unit_th "+
							     "              , cash_bill_detail.unit "+
							     "              , cash_bill_main.inv_no "+
							     "              , cash_bill_detail.quantity "+
							    "               , cash_bill_detail.cost_per_unit "+
							     "              , cash_bill_main.bill_date as bill_date "+
							     "         FROM cash_bill_detail "+
							     "         JOIN cash_bill_main "+
							     "         ON cash_bill_main.cash_bill_id = cash_bill_detail.cash_bill_id  "+
							     "         JOIN product "+
							     "         ON cash_bill_detail.product_id = product.product_id  "+
							     "         WHERE cash_bill_main.customer_id = '"+customer_id+"' "+
							     "         GROUP BY  cash_bill_detail.product_id  "+
							     "              , cash_bill_detail.adt_description "+
							     "              , cash_bill_detail.price "+
							     "              , product.name_th "+
							     "              , product.name_en "+
							     "              , product.unit_en "+
							     "              , product.unit_th "+
							     "              , cash_bill_detail.unit "+
							     "              , cash_bill_main.inv_no "+
							     "              , bill_date "+
							    "               , cash_bill_detail.cost_per_unit "+
							     "              , cash_bill_detail.quantity ; ";


    
    	  System.out.println("sql_query:"+sql_query);
    	  
    	  
    	  ResultSet rs_psh = connect.createStatement().executeQuery(sql_query);
          
    	  
    	 
    	  
    	  while(rs_psh.next())
          {
    		  ProductSellHistory psh = new ProductSellHistory();
    		  
    					  psh.setInvoiceNo(rs_psh.getString("inv_no"));
						  psh.setAdtDescription(rs_psh.getString("adt_description"));
						  psh.setProductId(rs_psh.getString("product_id"));
						  psh.setPrice(rs_psh.getString("price"));
						  psh.setNameEN(rs_psh.getString("name_en"));
						  psh.setNameTH(rs_psh.getString("name_th"));
						  psh.setUnitEN(rs_psh.getString("unit_en"));
						  psh.setUnitTH(rs_psh.getString("unit_th"));
						  psh.setUseUnit(rs_psh.getString("unit"));
						  psh.setQuantity(rs_psh.getString("quantity"));
       				      psh.setInvDate(rs_psh.getString("bill_date"));
       				      psh.setCostPerUnit(rs_psh.getString("cost_per_unit"));
    					  psh_list.add(psh);
            } 
              
          
             
         	try {
				Json = mapper.writeValueAsString(psh_list); 
				System.out.println("Json:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	
         	
         	out.print(Json);
	   
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  out.print("fail");
      }
     
  
	connect.close();
%>
