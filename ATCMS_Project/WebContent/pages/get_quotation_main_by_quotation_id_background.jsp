<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>
<%@ page import = "java.text.DateFormat" %>



<% 

	System.out.println("Start get_quotation_main_by_quotation_id_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

		
	//List<Order> order_list = new ArrayList<Order>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	//String purchase_id = request.getParameter("purchase_id");
	//System.out.println("purchase_id:"+purchase_id);
	 String quotation_id = request.getParameter("quotation_id");

	 System.out.println("quotation_id:"+quotation_id);



      try{
    	   
    	
    	
       		String	sql_query =  " SELECT * "+
    						"  FROM quotation_main "+
       						" JOIN company "+
    						" ON  quotation_main.customer_id = company.company_id "+
    						"  WHERE BINARY quotation_main.quotation_id ='"+quotation_id+"'";
  
   			 
   		   	 
		
    
    	  System.out.println("sql_query:"+sql_query);
    	  
    	  
    	  ResultSet rs_qo = connect.createStatement().executeQuery(sql_query);
          
    	  
    	  Quotation qo = new Quotation();
    	  
    	  while(rs_qo.next())
          {
    		  
    		  
    		    qo.setQuotationId(rs_qo.getString("quotation_id"));
    		    qo.setCreateDate(rs_qo.getString("create_date"));
    		    qo.setQuotationDate(rs_qo.getString("quotation_date"));
    		    qo.setStatus(rs_qo.getString("status"));
    		    qo.setTotalIncVat(rs_qo.getString("total_inc_vat"));
    		    qo.setTotalValue(rs_qo.getString("total_value"));
    		  	qo.setTotalVat(rs_qo.getString("total_vat"));
    		  	qo.setCustomerID(rs_qo.getString("customer_id"));
    		  	qo.setCredit(rs_qo.getString("credit"));
    		  	qo.setExpireDate(rs_qo.getString("expire_date"));
             	
             	if((!("-").equals(rs_qo.getString("name_en")))&&(!("").equals(rs_qo.getString("name_en"))))
             	{
             
            
             		qo.setCustomerName(rs_qo.getString("name_en"));
             		qo.setCustomerAddress(rs_qo.getString("address_en"));
         
             		
             	}else{
             		qo.setCustomerName(rs_qo.getString("name_th"));
             		qo.setCustomerAddress(rs_qo.getString("address_th"));
             	}
             

       				
         	//     credit_inv_list.add(ord);
            } 
              
            
               
         	try {
				Json = mapper.writeValueAsString(qo); 
				System.out.println("Json:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	
         	
         	out.print(Json);
	   
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  connect.close();
    	  out.print("fail");
      }
     
  
	connect.close();
%>
