<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>



<% 

	System.out.println("Start get_daily_novat_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

//	List<DailyNovatDetail> daily_novat_list = new ArrayList<DailyNovatDetail>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	
	String dn_id = request.getParameter("daily_novat_id");

	
	
	   try{
	    	  
	    	  String sql_query = " SELECT * "+
	    						"  FROM daily_novat_main "+
	    						"  WHERE BINARY daily_novat_main.daily_novat_id ='"+dn_id+"'" ;

	    	  
	    	  System.out.println("sql_query:"+sql_query);
	    	  ResultSet rs = connect.createStatement().executeQuery(sql_query);
	          
	    	  
	    	  DailyNovat daily_novat_temp = new DailyNovat();
	    	  
	    	  while(rs.next())
	          {
	 
	    		  
	    		 		 daily_novat_temp.setBillDate(rs.getString("bill_date"));
	    		 		 daily_novat_temp.setContactRef(rs.getString("contact_ref"));
	    		 		 daily_novat_temp.setNameRef(rs.getString("name_ref"));
	    		 		 daily_novat_temp.setIndex(rs.getString("index"));
	    		 		 daily_novat_temp.setAvgProfitPercentage(rs.getString("avg_profit_percentage"));
	    		 		 
	    		 		// daily_novat_temp.setAdtDescription(rs.getString("adt_description"));
	    		 		 daily_novat_temp.setDailyNovatId(rs.getString("daily_novat_id"));
	    		 //		 daily_novat_temp.setPrice(rs.getString("price"));
	    		 	//	 daily_novat_temp.setSum(rs.getString("sum"));
	    		 		 daily_novat_temp.setTotalValue(rs.getString("total_value"));
	    		 		 daily_novat_temp.setTotalCost(rs.getString("total_cost"));
	    		 		 daily_novat_temp.setTotalProfit(rs.getString("total_profit"));
	    		 		
	    		// 		 daily_novat_temp.setUnit(rs.getString("unit"));	    
	    	//	 		daily_novat_temp.setQuantity(rs.getString("quantity"));
	    		// 		 daily_novat_list.add(daily_novat_temp);
	              
	           } 

	         	try {
					Json = mapper.writeValueAsString(daily_novat_temp); 
					System.out.println("Json:"+Json);
				} catch (JsonGenerationException ex) {
					ex.printStackTrace();
				} catch (JsonMappingException ex) {
					ex.printStackTrace();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
	         	out.print(Json);
		   
		     
	      }
	      catch(Exception x){
	    	  System.out.println(x);
	    	  connect.close();
	    	  out.print("fail");
	      }

	   
	   
	connect.close();
%>
