<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "com.atcms.*" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "java.util.Properties" %>
<%@ page import = "javax.servlet.ServletContext" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>


<% 

	System.out.println("Start fetch_company_data_by_company_id_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
	///////////////////file_path parameter/////////////
	String company_id = request.getParameter("company_id");
	System.out.println("company_id:"+company_id);
	 ///////////////////////////////////////////////////
	List<Customer> customer_list = new ArrayList<Customer>();
	String Json="";
	ObjectMapper mapper = new ObjectMapper();
	try {		
		
		String sql_query = " SELECT * "+
				   " FROM `company` "+
				   " WHERE BINARY company_id = '"+company_id+"'";
	
		ResultSet rs =  connect.createStatement().executeQuery(sql_query);
		System.out.println(sql_query);	
		
			
			while(rs.next()) {
				 Customer cus = new Customer();
				
				 cus.setCompanyId(rs.getString("company_id"));
		 		 cus.setCodeName(rs.getString("code_name"));
		 		 cus.setAddressTH(rs.getString("address_th"));
		 		 cus.setAddressEN(rs.getString("address_en"));
		 		 cus.setAddressEN_FirstLine(rs.getString("address_en_line1"));
		 		 cus.setAddressEN_SecondLine(rs.getString("address_en_line2"));
		 		 cus.setAddressTH_FirstLine(rs.getString("address_th_line1"));
		 		 cus.setAddressTH_SecondLine(rs.getString("address_th_line2"));
		 		 
		 		 cus.setNameTH(rs.getString("name_th"));
		 		 cus.setNameEN(rs.getString("name_en"));
		 		 cus.setCodeName(rs.getString("code_name"));
		 		 cus.setTaxID(rs.getString("tax_id"));
		 		 cus.setCompanyId(rs.getString("company_id"));
		 		 cus.setCredit(rs.getString("credit"));
		 		 cus.setEmail(rs.getString("email"));
		 		 
		 		 
				
				
				customer_list.add(cus);
				
				if(customer_list.size()==10) break;
			}
			

		
	 	System.out.println("customer_list size :"+customer_list.size());
		try {
			Json = mapper.writeValueAsString(customer_list); 
			
		} catch (JsonGenerationException ex) {
			ex.printStackTrace();
		} catch (JsonMappingException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		System.out.println("Json:"+Json);
		
		out.print(Json);
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	 
	connect.close();
%>
