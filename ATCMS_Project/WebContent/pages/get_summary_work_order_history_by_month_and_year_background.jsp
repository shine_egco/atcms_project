<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>
<%@ page import = "java.text.DateFormat" %>



<% 

	System.out.println("Start get_work_order_history_by_month_and_year_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

		
	List<Report> report_list = new ArrayList<Report>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	//String purchase_id = request.getParameter("purchase_id");

	String 	month = request.getParameter("month");
	String  year = request.getParameter("year");
	



      try{
    	  
    

       		String	sql_query =  " SELECT  RE.product_id , SUM(RE.quantity) AS SUM_QUANTITY , SUM(RE.sum_cost) AS SUM_COST  "+
       														", RE.formula_id , RE.capacity , RE.capacity_unit "+
       	            										", RE.packaging_unit "+
       	            										", RE.pro_name_th , RE.pro_name_en "+
       	            										", RE.form_name_th , RE.form_name_en "+
       									   "  FROM (  "+
											       		"	SELECT workorder_detail.product_id  "+
											       	    "				   , workorder_detail.quantity "+
											       	    "  				   , workorder_detail.sum_cost  "+
											       	    " 				   , workorder_detail.formula_id "+
											       	    "  				   , formula_main.capacity "+
											       	    "  				   , formula_main.capacity_unit "+
											       	    "				   , formula_main.packaging_unit "+
											       	    "				   , product.name_th  AS pro_name_th"+
											       	     "				   , product.name_en  AS pro_name_en "+
											       	     "				   , formula_main.name_th AS form_name_th "+
											       	     "				   , formula_main.name_en AS form_name_en "+
											       		"	FROM workorder_detail "+
											       		"	JOIN product "+
											       		"	ON  workorder_detail.product_id = product.product_id "+
											       		"	JOIN formula_main  "+
											       	    "	ON  workorder_detail.formula_id  = formula_main.formula_id "+
											       		"  JOIN workorder_main "+
											       	    "  ON  workorder_detail.workorder_id = workorder_main.workorder_id "+
											       		"   WHERE MONTH(workorder_main.manu_date ) = '"+month+"' "+
											       	    "   AND YEAR(workorder_main.manu_date) =  '"+year+"' "+
       											 "   	)  AS RE "+
     										"  	GROUP BY RE.product_id ";
  
   			 
   			 

    
    	  System.out.println("sql_query:"+sql_query);
    	  
    	  
    	  ResultSet rs = connect.createStatement().executeQuery(sql_query);
          

    	  while(rs.next())
          {
    		  
        	  
        		    Report rp = new Report();
        	  
       				rp.setFormulaID(rs.getString("formula_id"));
       				rp.setProductID(rs.getString("product_id"));
       				rp.setTotalQuantity(rs.getString("SUM_QUANTITY"));
       				rp.setTotalCost(rs.getString("SUM_COST"));
       				rp.setProNameTH(rs.getString("pro_name_th"));
       				rp.setProNameEN(rs.getString("pro_name_en"));
       				rp.setFullUnit(rs.getString("capacity")+" "+rs.getString("capacity_unit")+"/"+rs.getString("packaging_unit"));
       				
       				report_list.add(rp);	
       				
           } 
              
          
             
         	try {
				Json = mapper.writeValueAsString(report_list); 
				System.out.println("Json:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	
         	
         	out.print(Json);
	   
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  out.print("fail");
      }
     
  
	connect.close();
%>
