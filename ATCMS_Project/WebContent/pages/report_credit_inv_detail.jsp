<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../bower_components/sidebar/sidebar_style_3.css" rel="stylesheet" type="text/css"> 
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

 <script>
	
	 function formatMoney(number, places, symbol, thousand, decimal) {
			number = number || 0;
			places = !isNaN(places = Math.abs(places)) ? places : 0;
			symbol = symbol !== undefined ? symbol : "";
			thousand = thousand || ",";
			decimal = decimal || ".";
			var negative = number < 0 ? "-" : "",
			    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
			    j = (j = i.length) > 3 ? j % 3 : 0;
			return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
	}
	
	 
	 function DeleteOrderForEditing()
 	{
		 
		var order_id = sessionStorage.getItem("order_id_for_get_detail");
		var reason ="-";
 
	    		
	    		var xmlhttp;
					
					if(window.XMLHttpRequest) {
						// code for IE7+, Firefox, Chrome, Opera, Safari
						xmlhttp = new XMLHttpRequest();
					}
					else {
						// code for IE6, IE5
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					
					xmlhttp.onreadystatechange = function() {
						if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
								alert(xmlhttp.responseText);
								if(xmlhttp.responseText=="success")
								{
									console.log("success deleted:"+order_id);
									//location.reload();
									
								}else{
									
								}
	
						}// end if check state
					}// end function
					
	
					xmlhttp.open("POST", "delete_single_order_background.jsp?order_id="+order_id+"&reason="+reason, true);
					xmlhttp.send();
 		

 		
 	}
	 function grap_product()
		{
			var pro_search_input = document.getElementById("pd_name-0");
			
			var input_value = (pro_search_input.value).split(",");
			console.log("input_value[0]"+input_value[0]);
			var product_id = input_value[0];

			//alert("product_id:"+product_id);
				
			if(product_id != "") {
						
						/* AJAX */
						var xmlhttp;
						
						if(window.XMLHttpRequest) {
							// code for IE7+, Firefox, Chrome, Opera, Safari
							xmlhttp = new XMLHttpRequest();
						}
						else {
							// code for IE6, IE5
							xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
						}
						
						xmlhttp.onreadystatechange = function() {
							if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
								var jsonObj = JSON.parse(xmlhttp.responseText);
								
								if(jsonObj.length == 0) {
			
									alert("no  Data");
								}
								else{
								//	alert("get Data");
									
									  var newid = 0;
								        $.each($("#tab_logic tr"), function() {
								            if (parseInt($(this).data("id")) > newid) {
								                newid = parseInt($(this).data("id"));
								            }
								        });
								        newid++;
								        
								        var tr = $("<tr></tr>", {
								            id: "addr"+newid,
								            "data-id": newid
								        });
								        
								     
								        // loop through each td and create new elements with name of newid
								            $.each($("#tab_logic tbody tr:nth(0) td"), function() {
								                var cur_td = $(this);
								                
								                var children = cur_td.children();
								                
								                // add new td and element if it has a name
								                if ($(this).data("name") != undefined) {
								                    var td = $("<td></td>", {
								                        "data-name": $(cur_td).data("name")
								                    });
								                    
								                    var c = $(cur_td).find($(children[0]).prop('tagName')).clone().val("");
								                    c.attr("name", $(cur_td).data("name") + newid);			
								                    c.attr("id", $(cur_td).data("name") + newid);		
								                    c.appendTo($(td));
								                    td.appendTo($(tr));
								                  

								                } else {
								                    var td = $("<td></td>", {
								                        'text': $('#tab_logic tr').length
								                    }).appendTo($(tr));
								                }
								            });
								        	
								            

								            
								            // add the new row
								            $(tr).appendTo($('#tab_logic'));
								        
								            $(tr).find("td button.row-remove").on("click", function() {
								                 $(this).closest("tr").remove();
								                 calculate_total();
								            });
								            
								            var  pd_name = document.getElementById("name_x"+newid);
								            var  pd_unit = document.getElementById("unit_x"+newid);
								           // var  pd_addition = document.getElementById("addition"+newid);
								            var  pd_price = document.getElementById("price_x"+newid); 
								            var  pd_quantity = document.getElementById("quantity_x"+newid); 
								            var  pd_sum = document.getElementById("sum_x"+newid); 
								            var  pd_id = document.getElementById("raw_id_x"+newid); 
								            var  pd_lan_flag = document.getElementById("lan_flag_x"+newid); 
					                    //  	var	 but_lan_flag = document.getElementById("but_lan_flag"+newid); 
				
								            	pd_name.value =  jsonObj[0].displayName;
								            	pd_unit.value = jsonObj[0].displayUnit;
								            	pd_price.value = parseFloat(jsonObj[0].pricePool).toFixed(2);
								            	pd_lan_flag.value = jsonObj[0].productLanFlag;
								     //       	but_lan_flag.value = jsonObj[0].productLanFlag;
								        
							            		pd_quantity.value = 1;
							            		pd_sum.value = pd_price.value ;
							            		pd_id.value = jsonObj[0].productID;
					                           
					                           
												calculate_total();
												pro_search_input.value="";
								}
							}
							
						}
						
						xmlhttp.open("POST", "fetch_product_data_background.jsp?product_id="+product_id, true);
						xmlhttp.send();
			}
			 // Dynamic Rows Code
	        
	        // Get max row id and set new id

			
		}
	
	 
	 function  fetch_order_main(){
		 
		 var order_id = sessionStorage.getItem("order_id_for_get_detail");
         
	      var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					
					var jsonObj = JSON.parse(xmlhttp.responseText);
					
					if(jsonObj.length == 0) {

						//alert("no  Data");
					}
					else{
					//	alert("get Data");
						 
					//	alert(xmlhttp.responseText);
						var inv_name = document.getElementById("inv_name");
						var inv_address = document.getElementById("inv_address");
						var inv_tax_id = document.getElementById("inv_tax_id"); 
						var compute_credit = document.getElementById("compute_credit"); 
						var delivery_date  = document.getElementById("delivery_date"); 
						var due_date = document.getElementById("due_date"); 
						var inv_no = document.getElementById("inv_no"); 
						var customer_id =  document.getElementById("company_id_hidden"); 
					//	var create_date = document.getElementById("create_date"); 
						var complete_date = document.getElementById("complete_date"); 
						var po_no = document.getElementById("po_no"); 
						var order_view_id = document.getElementById("order_view_id"); 
						var inv_file_name = document.getElementById('inv_file_name');
						var inv_file_path = document.getElementById('inv_file_path');
						var status_but = document.getElementById('status_but');
						var modal_status_but = document.getElementById('modal_status_but');
						var modal_note = document.getElementById('modal_note');
						var payment_ref = document.getElementById('payment_ref');
						
						
						var total_vat = document.getElementById('total_vat');
						var total_sum = document.getElementById('total_sum');
						var total_inc_vat = document.getElementById('total_inc_vat');
						var total_sum_cost =  document.getElementById('total_sum_cost');
						var total_sum_profit =  document.getElementById('total_sum_profit');
						
						//////////////////////// use for Editing Mode //////////////////////////
						var total_vat_x = document.getElementById('total_vat_x');
						var total_value_x = document.getElementById('total_value_x');
						var total_inc_vat_x = document.getElementById('total_inc_vat_x');
						
						
						//	status_but.innerHTML = jsonObj.orderStatus+"<span class='caret'></span>";
							
							if(jsonObj.orderStatus=="order_created")
							{
								status_but.innerHTML = "Order Created"+"<span class='caret'></span>";
								modal_status_but.innerHTML = "Order Created"+"<span class='caret'></span>";
								modal_status_but.name = "order_created";
								
							}else if(jsonObj.orderStatus=="pending"){
								status_but.innerHTML = "Pending"+"<span class='caret'></span>";
								modal_status_but.innerHTML = "Pending"+"<span class='caret'></span>";
								modal_status_but.name = "pending";
						
							}else if(jsonObj.orderStatus=="during_delivery"){
								status_but.innerHTML = "During Delivery"+"<span class='caret'></span>";
								modal_status_but.innerHTML = "During Delivery"+"<span class='caret'></span>";
								modal_status_but.name = "during_delivery";
						
							}else if(jsonObj.orderStatus=="delivered"){
								status_but.innerHTML = "Delivered"+"<span class='caret'></span>";
								modal_status_but.innerHTML = "Delivered"+"<span class='caret'></span>";
								modal_status_but.name = "delivered";
						
							}else if(jsonObj.orderStatus=="billing"){
								status_but.innerHTML = "Billing"+"<span class='caret'></span>";
								modal_status_but.innerHTML = "Billing"+"<span class='caret'></span>";
								modal_status_but.name = "billing";
						
							}else if(jsonObj.orderStatus=="completed"){
								status_but.innerHTML = "Completed"+"<span class='caret'></span>";
								modal_status_but.innerHTML = "Completed"+"<span class='caret'></span>";
								modal_status_but.name = "completed";
						
							}
							
							
							
							inv_name.value = jsonObj.customerName;
							inv_address.value = jsonObj.customerAddress; 					
							inv_tax_id.value = jsonObj.taxId; 
							compute_credit.value =  jsonObj.computeCredit;
							delivery_date.value =  jsonObj.deliveryDate;
							due_date.value =  jsonObj.dueDate;
						//	create_date.value = jsonObj.createDate;
							complete_date.value = jsonObj.completedDate;
							customer_id.value = jsonObj.customerId;
							po_no.value = jsonObj.poNum;
							order_view_id.value = jsonObj.orderId;
							inv_file_name.value = jsonObj.invFileName;
							inv_file_path.value = jsonObj.invFilePath;
							modal_note.value = jsonObj.note;
							payment_ref.value = jsonObj.paymentReference;
							
							total_vat.value = jsonObj.totalVat;
							total_sum.value = jsonObj.totalValue;
							total_inc_vat.value = jsonObj.totalIncVat;
							
							//////////////////for Editing mode ////////////////////
							total_vat_x.value = jsonObj.totalVat;
							total_value_x.value = jsonObj.totalValue;
							total_inc_vat_x.value = jsonObj.totalIncVat;
							
							total_sum_cost.value = jsonObj.totalCost;
							total_sum_profit.value = jsonObj.totalProfit;		
							
							inv_no.value = jsonObj.invNo;
							
							
						
						
					}

				}// end if check state
			}// end function
			

			xmlhttp.open("POST", "get_credit_inv_main_by_order_id_background.jsp?order_id="+order_id, true);
			xmlhttp.send();
	    	
		 
	 } 
	 
	 function  fetch_order_detail(){
		 
		 var order_id = sessionStorage.getItem("order_id_for_get_detail");
         
	      var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					
					var jsonObj = JSON.parse(xmlhttp.responseText);
					
					if(jsonObj.length == 0) {

						//alert("no  Data");
					}
					else{
						//alert("get Order Detail Data");
							
							for(i in jsonObj) {

								///////////////////////////////////////////////////////
								  var newid = 0;
							        $.each($("#order-detail-dataTables-example tr"), function() {
							            if (parseInt($(this).data("id")) > newid) {
							                newid = parseInt($(this).data("id"));
							            }
							        });
							        newid++;
							        
							        var tr = $("<tr></tr>", {
							            id: "addr"+newid,
							            "data-id": newid
							        });
							     ///////////////////////////////////////////////////////   
							        
								  var newid_x = 0;
							        $.each($("#tab_logic tr"), function() {
							            if (parseInt($(this).data("id")) > newid_x) {
							            	newid_x = parseInt($(this).data("id"));
							            }
							        });
							        newid_x++;
							        
							        var tr_x = $("<tr></tr>", {
							            id: "addr"+newid_x,
							            "data-id": newid_x
							        });
							        /////////////////////////////////////////////////////
							        
							     
							        // loop through each td and create new elements with name of newid
							            $.each($("#order-detail-dataTables-example tbody tr:nth(0) td"), function() {
							                var cur_td = $(this);
							                
							                var children = cur_td.children();
							                
							                // add new td and element if it has a name
							                if ($(this).data("name") != undefined) {
							                    var td = $("<td></td>", {
							                        "data-name": $(cur_td).data("name")
							                    });
							                    
							                    var c = $(cur_td).find($(children[0]).prop('tagName')).clone().val("");
							                    c.attr("name", $(cur_td).data("name") + newid);			
							                    c.attr("id", $(cur_td).data("name") + newid);		
							                    c.appendTo($(td));
							                    td.appendTo($(tr));
							                  

							                } else {
							                    var td = $("<td></td>", {
							                        'text': $('#order-detail-dataTables-example tr').length
							                    }).appendTo($(tr));
							                }
							            });
							        ///////////////////////////////////////////////////////////////////////////	
							           // loop through each td and create new elements with name of newid
							            $.each($("#tab_logic tbody tr:nth(0) td"), function() {
							                var cur_td_x = $(this);
							                
							                var children_x = cur_td_x.children();
							                
							                // add new td and element if it has a name
							                if ($(this).data("name") != undefined) {
							                    var td_x = $("<td></td>", {
							                        "data-name": $(cur_td_x).data("name")
							                    });
							                    
							                    var c_x = $(cur_td_x).find($(children_x[0]).prop('tagName')).clone().val("");
							                    c_x.attr("name", $(cur_td_x).data("name") + newid_x);			
							                    c_x.attr("id", $(cur_td_x).data("name") + newid_x);		
							                    c_x.appendTo($(td_x));
							                    td_x.appendTo($(tr_x));
							                  

							                } else {
							                    var td_x = $("<td></td>", {
							                        'text': $('#tab_logic tr').length
							                    }).appendTo($(tr_x));
							                }
							            });
							        	
							        /////////////////////////////////////////////////////////////////////////
							        
							            // add the new row
							            $(tr).appendTo($('#order-detail-dataTables-example'));
							            
							            var  pd_name = document.getElementById("name"+newid);
							            var  pd_cost_per_unit = document.getElementById("cost_per_unit"+newid);
							            var  pd_price = document.getElementById("price"+newid);
							            var  pd_profit_per_unit = document.getElementById("profit_per_unit"+newid);
							            var  pd_quantity = document.getElementById("quantity"+newid);
							            var  pd_unit = document.getElementById("unit"+newid);
							            var  pd_sum_cost = document.getElementById("sum_cost"+newid);
							            var  pd_sum = document.getElementById("sum"+newid);
							            var  pd_sum_profit = document.getElementById("sum_profit"+newid);
							            var  pd_product_id =  document.getElementById("product_id"+newid);
							            
							            
							   			//	alert(pd_name);
							           		 pd_name.value = jsonObj[i].adtDescription;
							           		 pd_cost_per_unit.value = jsonObj[i].costPerUnit;
							           		 pd_price.value = jsonObj[i].price;
							           		 pd_profit_per_unit.value = jsonObj[i].profitPerUnit;
							           	  	 pd_quantity.value = jsonObj[i].quantity;
							           	     pd_unit.value = jsonObj[i].productUnit;
							           	     pd_sum_cost.value = jsonObj[i].sumCost;
							           	     pd_sum.value = jsonObj[i].sum;
							           	 	 pd_sum_profit.value =  jsonObj[i].sumProfit;
							           	 	 pd_product_id.value = (parseInt(i)+1)+"_"+jsonObj[i].productId;
							           	 	 pd_product_id.name = jsonObj[i].index;
							        ////////////////////////////////////////////////////////////
							
							
							       // add the new row
							            $(tr_x).appendTo($('#tab_logic'));
							        
							            $(tr_x).find("td button.row-remove").on("click", function() {
							                 $(this).closest("tr").remove();
							                 calculate_total();
							            });
							            
							            var  pd_name_x = document.getElementById("name_x"+newid_x);
							            var  pd_unit_x = document.getElementById("unit_x"+newid_x);
							           // var  pd_addition = document.getElementById("addition"+newid);
							            var  pd_price_x = document.getElementById("price_x"+newid_x); 
							            var  pd_quantity_x = document.getElementById("quantity_x"+newid_x); 
							            var  pd_sum_x = document.getElementById("sum_x"+newid_x); 
							            var  pd_id_x = document.getElementById("raw_id_x"+newid_x); 
							            var  pd_lan_flag_x = document.getElementById("lan_flag_x"+newid_x); 
				                    //  	var	 but_lan_flag = document.getElementById("but_lan_flag"+newid); 
									//	var  pd_product_id_x =  document.getElementById("raw_id_x"+newid);
							        	pd_name_x.value =  jsonObj[i].adtDescription;
						            	pd_unit_x.value = jsonObj[i].productUnit;
						            	pd_price_x.value = parseFloat(jsonObj[i].price).toFixed(2);
						            	pd_lan_flag_x.value = "-";
						     //       	but_lan_flag.value = jsonObj[0].productLanFlag;
						     
						     		    pd_id_x.value = jsonObj[i].productId;
						     		    pd_id_x.name = jsonObj[i].index;
					            		pd_quantity_x.value = jsonObj[i].quantity;
					            		pd_sum_x.value = jsonObj[i].sum;
					            		
							
							}
							
						  	calculate_profit_percentage();
						  	calculate_summation_table();

					}

				}// end if check state
			}// end function
			

			xmlhttp.open("POST", "get_credit_inv_detail_by_order_id_background.jsp?order_id="+order_id, true);
			xmlhttp.send();
	 
	 }
	 
	 function calculate_cost(temp_id){
		 	 
		 var temp_id_parts = temp_id.split('_');
		 var prefix = temp_id_parts[0];
		 var product_id = temp_id_parts[1];
		 
		//alert(product_id.substring(0,3));
				if(product_id.substring(0,3)=="OTH")
				{
								   var xmlhttp;
									
									if(window.XMLHttpRequest) {
										// code for IE7+, Firefox, Chrome, Opera, Safari
										xmlhttp = new XMLHttpRequest();
									}
									else {
										// code for IE6, IE5
										xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
									}
									
									xmlhttp.onreadystatechange = function() {
										
										if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
											
											var jsonObj = JSON.parse(xmlhttp.responseText);
											
											if(jsonObj.length == 0) {
								
												alert("no  Data");
											}
											else{
												
												var result = "";
												var sum = 0;
												var divider = 0;
												var avg_cost = 0;
												var avg_cost_str = "";
												var final_avg_cost ="";
												for(i in jsonObj) {
													
												    result = result + "Date : "+jsonObj[i].invoiceDate+"\n"+
													     	          "Price "+jsonObj[i].price +" x "+ jsonObj[i].quantity +" = "+jsonObj[i].sum+"\n\n";
												    
												    sum = sum + parseFloat(jsonObj[i].sum);
												    divider = divider + parseFloat(jsonObj[i].quantity);
												}
												
												avg_cost = sum / divider ;
												
												avg_cost_str = avg_cost+"";
												
												if(avg_cost_str.indexOf(".")>0)
												{
													var avg_cost_str_parts = avg_cost_str.split('.');
													final_avg_cost = avg_cost_str_parts[0] +"."+ avg_cost_str_parts[1].substring(0,2);
												}else{
													
													final_avg_cost = avg_cost_str;
												}
												//alert("final_avg_cost:"+final_avg_cost);
												
												var cost_per_unit = document.getElementById('cost_per_unit'+prefix);
													cost_per_unit.value = final_avg_cost;
													
													var today = new Date();
													var dd = today.getDate();
													var mm = today.getMonth()+1; //January is 0!
								
													var yyyy = today.getFullYear();
													if(dd<10){
													    dd='0'+dd
													} 
													if(mm<10){
													    mm='0'+mm
													} 
													var today = yyyy+'-'+mm+'-'+dd;
													
													cost_per_unit.name = today;
												
												result = result + "Sum : "+sum +" , "+"Quantity : "+divider+"\n"+
														 "Avg Cost : "+final_avg_cost + " Baht";
												
												
												calculate_each_row("xxxxxxxxxxxxx"+prefix);
											  	calculate_profit_percentage();
												
												alert(result);
								
											}
								
										}// end if check state
									}// end function
									
								
									xmlhttp.open("POST", "calculate_oth_product_cost_by_product_id_background.jsp?product_id="+product_id, true);
									xmlhttp.send();
				}
				else{
					
					 var xmlhttp;
						
						if(window.XMLHttpRequest) {
							// code for IE7+, Firefox, Chrome, Opera, Safari
							xmlhttp = new XMLHttpRequest();
						}
						else {
							// code for IE6, IE5
							xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
						}
						
						xmlhttp.onreadystatechange = function() {
							
							if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
								
								alert(xmlhttp.responseText);
								
								//var jsonObj = JSON.parse(xmlhttp.responseText);
								
	
					
							}// end if check state
						}// end function
						
					
						xmlhttp.open("POST", "calculate_atc_product_cost_by_product_id_background.jsp?product_id="+product_id, true);
						xmlhttp.send();

				}
	 
	 }
	 
	 function calculate_each_row (id){
		  
		 var row_num = "";
		 
		 if((id.length ==14) || (id.length == 15)) {
			 
			 row_num =	 id.substring(13);
			 
		}else if((id.length == 6) || (id.length == 7) ) {
			
			row_num =  id.substring(5);
			 
		}else if((id.length == 9) || (id.length ==10) ){
			
			row_num =  id.substring(8);
			 
		}
	
		//alert(row_num);
		 	
		
		 
		 var  cost_per_unit = document.getElementById("cost_per_unit"+row_num);
		 var  price = document.getElementById("price"+row_num);
		 var  profit_per_unit = document.getElementById("profit_per_unit"+row_num);
		 var  quantity = document.getElementById("quantity"+row_num);
		 var  sum_cost = document.getElementById("sum_cost"+row_num);
		 var  sum = document.getElementById("sum"+row_num);
		 var  sum_profit = document.getElementById("sum_profit"+row_num);
		 		 		 
		 var profit_per_unit_value = parseFloat(price.value , 10) - parseFloat(cost_per_unit.value , 10);
		 	 
			    profit_per_unit.value =  parseFloat(profit_per_unit_value , 10).toFixed(2) ;
		
		var sum_cost_value =  parseFloat(cost_per_unit.value , 10) * parseFloat(quantity.value , 10) ; 
		
			   sum_cost.value = parseFloat(sum_cost_value , 10).toFixed(2);
			   
		var sum_profit_value =  parseFloat(profit_per_unit_value , 10)  *   parseFloat(quantity.value , 10) ; 
			
			  sum_profit.value = parseFloat(sum_profit_value , 10).toFixed(2);
			  
			  
			  calculate_profit_percentage();
			  calculate_summation_table();

		
	 }
	 
	 function calculate_summation_table(){
		 
	//	alert("Calulate Summation");
			 
		 var num_row =   $('#order-detail-dataTables-example tr').length -2 ;
		 
	//	 alert(num_row);
		 
		 var total_sum_value  = 0;
		 var total_sum_cost_value = 0;
		 var total_sum_profit_value = 0;
		 
		 
		 for (var  i=1; i<=num_row; i++)
		{
			 
			 var  sum_cost = document.getElementById("sum_cost"+i);
			 var  sum = document.getElementById("sum"+i);
			 var  sum_profit = document.getElementById("sum_profit"+i);
			 	
			// 	alert(sum_cost.value+" , "+sum.value+","+sum_profit.value);
			 
			    	 total_sum_cost_value = parseFloat(total_sum_cost_value , 10)  + parseFloat(sum_cost.value , 10 );
			    	 total_sum_value = parseFloat(total_sum_value , 10) +  parseFloat(sum.value , 10);
			    	 total_sum_profit_value = parseFloat(total_sum_profit_value , 10) + parseFloat(sum_profit.value , 10);
			 
		}
		 
		 var  total_sum_cost = document.getElementById("total_sum_cost");
		 var  total_sum = document.getElementById("total_sum");
		 var  total_sum_profit = document.getElementById("total_sum_profit");
		 var  total_vat = document.getElementById("total_vat");
		 var  total_inc_vat = document.getElementById("total_inc_vat");
		 
			//	 total_sum_cost.value = (total_sum_cost_value);
			//	 total_sum.value = (total_sum_value);
			//	 total_sum_profit.value = (total_sum_profit_value);
				 
		var total_vat_string = String((total_sum_value * 0.07) * 10 / 10);
		var total_sum_string = String((total_sum_value * 1.07) * 10 / 10);
		var total_sum_cost_string = String(total_sum_cost_value);
		
		var final_total_vat;
		var final_total_sum;		 
		var final_total_sum_cost;
 
		if(total_vat_string.indexOf(".")>0)
		{
			var total_vat_string_parts = total_vat_string.split('.');
			final_total_vat = total_vat_string_parts[0] +"."+ total_vat_string_parts[1].substring(0,2);
		}else{
			final_total_vat = total_vat_string;
		}
	
		
		if(total_sum_string.indexOf(".")>0)
		{
			var total_sum_string_parts = total_sum_string.split('.');
			final_total_sum = total_sum_string_parts[0] +"."+ total_sum_string_parts[1].substring(0,2);
		}
		else{
			final_total_sum = total_sum_string;
		}
		
		if(total_sum_cost_string.indexOf(".")>0)
		{
			var total_sum_cost_string_parts = total_sum_cost_string.split('.');
			final_total_sum_cost = total_sum_cost_string_parts[0] +"."+ total_sum_cost_string_parts[1].substring(0,2);
		}
		else{
			final_total_sum_cost = total_sum_cost_string;
		}
		
		
		total_vat.value =  parseFloat(final_total_vat);
		total_inc_vat.value = parseFloat(final_total_sum);
		total_sum_cost.value =  parseFloat(final_total_sum_cost);
		total_sum_profit.value = (parseFloat(total_sum.value) - parseFloat(final_total_sum_cost)).toFixed(2) +"";
		
		
		
		
		/////////////////////////////////////////////// Calculate Profit Percentage /////////////////////////////
		
		var  avg_profit_percent = document.getElementById("avg_profit_percent");
		
		var temp_answer = 0;
		
		 temp_answer =  (parseFloat(total_sum.value  , 10) - parseFloat(total_sum_cost.value  , 10));
		 temp_answer = (temp_answer / parseFloat(total_sum.value  , 10) ) * 100 ;
 
		 
		 avg_profit_percent.value = temp_answer.toFixed(2) +" %";
		 
		 
	 }
	 
	 
	 function numberWithCommas(x) {
		 
		    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		    
		}
	 
	 
	 
	 
	 function calculate_profit_percentage(){
		 
		 console.log("Start");
		 
		 var temp_row =   $('#order-detail-dataTables-example tr').length -2 ;
		 
		 var temp_answer = 0 ;
		 
		 console.log("temp_answer:"+temp_answer);
		 console.log("temp_row:"+temp_row);
		 
		 for (var  temp=1 ;  temp<=temp_row ;  temp++)
		{
			 console.log("start loop :"+i);
				 
				 var  cost_per_unit = document.getElementById("cost_per_unit"+temp);
				 var  price = document.getElementById("price"+temp);
				 var profit_per_unit = document.getElementById("profit_per_unit"+temp);
				 var  percent_profit = document.getElementById("percent_profit"+temp);
				 	
						 temp_answer =  (parseFloat(price.value  , 10) - parseFloat(cost_per_unit.value  , 10));
						 temp_answer = (temp_answer / parseFloat(price.value  , 10) ) * 100 ;
				 
						 
						 percent_profit.value = temp_answer.toFixed(2) +" %";
						 
						 temp_answer = 0;
				
			}

		 
	 }
	 
	 function GenerateInvoice(){
 		
		  var order_id = document.getElementById('order_view_id').value;
		  var inv_file_name = document.getElementById('inv_file_name');
		  var inv_file_path = document.getElementById('inv_file_path');
			
		 // alert("gen inv ");
		//  $('ul.setup-panel li:eq(1)').removeClass('disabled');
		//  $('ul.setup-panel li a[href="#step-2"]').trigger('click');
		//  button.remove();
		  
		  var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
					if(xmlhttp.responseText=="fail")
					{
						alert("Error Occer to Generate inv , pls contact Admin");
					}else{
						
						//alert(xmlhttp.responseText);
						var values = xmlhttp.responseText.split("&");
						var file_name = values[0];
						var file_path = values[1];
						
						
						alert("Your file "+file_name+" are ready.");
					
						inv_file_name.value = file_name;
						inv_file_path.value = file_path;
					}
					
					
						
				}// end if check state
			}// end function
			
			alert("going to send POST");
			xmlhttp.open("POST", "generate_credit_inv_background.jsp?order_id="+order_id, true);
			xmlhttp.send();

		  
	}
	function show_modal_change_status(temp){

		$('#status_detail_modal').modal('show');
	
	}
	
	function change_status(temp)
	{
	
		var modal_status_but = document.getElementById('modal_status_but');	
			modal_status_but.innerHTML = temp.innerHTML+"<span class='caret'></span>";
			modal_status_but.name = temp.id;
		
	}
	function submit_change_status(){
		
		var modal_note = document.getElementById('modal_note');
		var complete_date = document.getElementById('complete_date');
		var payment_ref = document.getElementById('payment_ref');
		var status_but = document.getElementById('status_but');
		var order_id = document.getElementById('order_view_id');		
		var	modal_status_but = document.getElementById('modal_status_but');	
		var parameter ; 

	//	alert("status:"+modal_status_but.name+" , "+"modal_note:"+modal_note.value+" , complete_date:"+complete_date.value+" , payment_ref:"+payment_ref.value);
			parameter = "order_id="+order_id.value+
						"&complete_date="+complete_date.value+
						"&status="+modal_status_but.name+
						"&note="+modal_note.value+
						"&payment_ref="+payment_ref.value;
	
		var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
					if(xmlhttp.responseText=="fail")
					{
						alert("Change Status error");
					}else{
						
						alert("Success");
						location.reload();
					}
					
				}// end if check state
			}// end function
			
		
			xmlhttp.open("POST", "change_order_status_detail_background.jsp?"+parameter, true);
			xmlhttp.send();

	}
	function save_order_updated(){
		
		var pd_array = new Array();
		var parameter_order_main = "";
		var parameter_order_detail ="";
		
		

		$('#order-detail-dataTables-example > tbody  > tr').each(function() {

			if(this.id=="addr0")
			{
				
			}else{
				
				var id ;
				if(this.id.length==5)
				{
					id = this.id.slice(-1);
				}else{
					id = this.id.slice(-2);
				}
				//alert("id:"+id);
				
				pd_array.push(id);		
			}

			
		});
		
		var index =  parseInt(pd_array.length, 10);
		
		
		
		
		var order_id = document.getElementById('order_view_id');
		var po_no  = document.getElementById('po_no');
		var  total_sum_cost = document.getElementById("total_sum_cost");
		var  total_sum = document.getElementById("total_sum");
		var  total_sum_profit = document.getElementById("total_sum_profit");
		var  total_vat = document.getElementById("total_vat");
		var  total_inc_vat = document.getElementById("total_inc_vat");
		var avg_profit_percent = document.getElementById("avg_profit_percent");
		
		var avg_profit_array = (avg_profit_percent.value).split(' ');
		var temp_avg_profit_percent = avg_profit_array[0];
		
		var order_id_value = order_id.value ; 
		
		
		parameter_order_main = "order_id="+order_id_value+
							   "&num_index="+index+
							   "&po_no="+po_no.value+
							   "&total_sum_cost="+total_sum_cost.value+
							   "&total_sum="+total_sum.value+
							   "&total_sum_profit="+total_sum_profit.value+
							   "&total_vat="+total_vat.value+
							   "&total_inc_vat="+total_inc_vat.value+
							   "&avg_profit_percent="+temp_avg_profit_percent;
		console.log(parameter_order_main);
		
	//	alert(parameter_order_main);
		
		for(var j=0;j<index ;j++)
		{
				var pd_product_id = document.getElementById('product_id'+pd_array[j]);
				var pd_product_id_value = pd_product_id.value ; 
				var index_value = pd_product_id.name;
				
						 var temp_id_parts = pd_product_id_value.split('_');
						 var final_product_id = temp_id_parts[1];

				var pd_name = document.getElementById('name'+pd_array[j]);
				var pd_name_value = pd_name.value ; 
				
				var pd_cost_per_unit = document.getElementById('cost_per_unit'+pd_array[j]);
				var pd_cost_per_unit_value = pd_cost_per_unit.value ; 
				
					  var pd_calculated_cost_date_value  = pd_cost_per_unit.name ;
				
				var pd_price = document.getElementById('price'+pd_array[j]);
				var pd_price_value = pd_price.value ; 
				
				var pd_profit_per_unit = document.getElementById('profit_per_unit'+pd_array[j]);
				var pd_profit_per_unit_value = pd_profit_per_unit.value ; 
				
				var pd_quantity = document.getElementById('quantity'+pd_array[j]);
				var pd_quantity_value = pd_quantity.value ; 
				
				var pd_unit = document.getElementById('unit'+pd_array[j]);
				var pd_unit_value = pd_unit.value ; 
				
				var pd_sum_cost = document.getElementById('sum_cost'+pd_array[j]);
				var pd_sum_cost_value = pd_sum_cost.value ; 
				
				var pd_sum = document.getElementById('sum'+pd_array[j]);
				var pd_sum_value = pd_sum.value ; 
				
				var pd_sum_profit = document.getElementById('sum_profit'+pd_array[j]);
				var pd_sum_profit_value = pd_sum_profit.value ; 
				
				var pd_percent_profit = document.getElementById('percent_profit'+pd_array[j]);
				var pd_percent_profit_value = pd_percent_profit.value ; 
				if(pd_percent_profit_value=='NaN')
				{
					pd_percent_profit_value = '0';
				}
				
				
				
				var pd_percent_profit_array = pd_percent_profit_value.split(' ');
				var temp_pd_percent_profit = pd_percent_profit_array[0];
				

				parameter_order_detail = parameter_order_detail +
										 "&pd_id"+j+"="+final_product_id+
										 "&pd_name"+j+"="+pd_name_value+
										 "&pd_cost_per_unit"+j+"="+pd_cost_per_unit_value+
										 "&pd_price"+j+"="+pd_price_value+
										 "&pd_profit_per_unit"+j+"="+pd_profit_per_unit_value+
										 "&pd_percent_profit"+j+"="+temp_pd_percent_profit+
										 "&pd_quantity"+j+"="+pd_quantity_value+
										 "&pd_unit"+j+"="+pd_unit_value+
										 "&pd_sum_cost"+j+"="+pd_sum_cost_value+
										 "&pd_sum"+j+"="+pd_sum_value+
										 "&pd_sum_profit"+j+"="+pd_sum_profit_value+
										 "&index"+j+"="+index_value+
										 "&pd_calculated_cost_date"+j+"="+pd_calculated_cost_date_value;
				 
										 
		}
		
			alert(parameter_order_detail);
		
		var xmlhttp;
		
		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange = function() {
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
				if(xmlhttp.responseText=="fail")
				{
					alert("Update Error");
				}else{
					
					alert("Success");
					
					
					//location.reload();
				}
				
			}// end if check state
		}// end function
		
	
		xmlhttp.open("POST", "update_full_order_by_order_id_background.jsp?"+parameter_order_main+parameter_order_detail, true);
		xmlhttp.send();

		
	}
	function calculate_due_date(deliv_date){
	
		var devlivery_date = new Date (deliv_date);
		var new_date = new Date();
			new_date.setDate(devlivery_date.getDate());
			new_date.setMonth(devlivery_date.getMonth());
			new_date.setYear(devlivery_date.getYear()+1900);
				
		var credit = document.getElementById("compute_credit");

		if(credit.value!=""){
			
			var credit_int = parseInt(credit.value, 10);
			
			//alert("credit_int:"+credit_int);
			//alert(devlivery_date.getDate() + credit_int);
			new_date.setDate(devlivery_date.getDate() + credit_int);
			//alert(new_date.getMonth()+1);
			//alert("new_date:"+new_date);
			var due_date = document.getElementById("due_date");
				//due_date.value = new_date;
				// format for set value is yyyy-mm-dd
			var due_date_year =  parseInt(new_date.getYear()+1900, 10);
			var due_date_month = parseInt(new_date.getMonth()+1, 10); 
			var due_date_date = parseInt(new_date.getDate(), 10);
			
			var year = due_date_year.toString();
			var month = due_date_month.toString();
			var date = due_date_date.toString();
			if(month.length==1)
			{
				month = "0" +month;
			}
			else{
				
			}
			if(date.length==1)
			{
				date = "0" +date;
			}
			else{
				
			}
			
			
		
			
			due_date.value = ""+year+"-"+month+"-"+date;
			//alert(""+year+"-"+month+"-"+date);
		}else{
			alert("credit is null");
		}
			
	}
	
	function search_pro_name(temp){
		
		 var input_box = temp;
		 var keyword = input_box.value;
		
		 //alert("input_box:"+input_box.id);
		 var temp2 = (input_box.id).split("-");  // get row from input value
		 
		 var row = temp2[1];
		 

		 if(keyword != "") {
				
			
				var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
						var jsonObj = JSON.parse(xmlhttp.responseText);
						
						if(jsonObj.length == 0) {
	
						//	alert("no  Data");
						}
						else{
							//alert("Have Data");

							
							$('#pro_lists-'+row).empty();
							
							
							for(i in jsonObj) {
							var text = jsonObj[i].productID+","+jsonObj[i].nameTH+","+jsonObj[i].nameEN;
							//	var text = jsonObj[i].nameTH+","+jsonObj[i].nameEN;
										
						//	text = text.replace("""","");	
							var id = jsonObj[i].productID ; 
								//alert(jsonObj[i].nameTH+" "+jsonObj[i].nameEN);
								var option = '<option id="'+id+'"  value="'+text+'" >' ;
								
								$('#pro_lists-'+row).append(option);
							}
							
						}
					}
					
				}
				
				xmlhttp.open("POST", "get_product_by_keyword_background.jsp?keyword="+keyword, true);
				xmlhttp.send();
			}
		

}

function fetch_pro_data(temp){
	  
		alert("fetch_pro_data");
		
		var input_value = (temp.value).split(",");
		var product_id = input_value[0];
		var temp_get_row = (temp.id).split("-");
		var row = temp_get_row[1];

}



function calculate_sum(id_in)
{
	
	console.log("id_in:"+id_in);
	var id ;
	if((id_in.length==8)||(id_in.length==11))
	{
		//alert("<10");
		id = id_in.slice(-1);
		
	}else{
		//alert("over 10");
		id = id_in.slice(-2);
	}
	console.log("id:"+id);
	var sum_price = 0;
	var pd_price = document.getElementById("price_x"+id);
	var pd_quantity = document.getElementById("quantity_x"+id);
	
		sum_price = pd_price.value * pd_quantity.value ; 
		
	var pd_sum = document.getElementById("sum_x"+id);
		pd_sum.value = parseFloat(sum_price).toFixed(2); 
	
	
		calculate_total();
		 
}

function create_order(){
	
	console.log("/////Start create_order////////");
	var checking ="";
	var order_main_id ;
	
	var company_id = document.getElementById('company_id_hidden');
	var company_id_value = company_id.value;	
	////////////////////////////////////////////////////
	var customer_address = document.getElementById('inv_address');
	var customer_address_value = customer_address.value ; 
	////////////////////////////////////////////////////
	var customer_tax_id =  document.getElementById('inv_tax_id');
	var customer_tax_id_value = customer_tax_id.value;
	////////////////////////////////////////////////////
	var po_no = document.getElementById('po_no');
	var po_no_value = po_no.value ;
	////////////////////////////////////////////////////
	var credit = document.getElementById('compute_credit');
	var credit_value = credit.value ;
	////////////////////////////////////////////////////
	var dalivery_date = document.getElementById('delivery_date');
	var dalivery_date_value = dalivery_date.value ;
	////////////////////////////////////////////////////
	var due_date = document.getElementById('due_date');
	var due_date_value = due_date.value ;
	///////////////////////////////////////////////////
	var total_vat = document.getElementById("total_vat_x");
	var total_vat_value = total_vat.value;
	///////////////////////////////////////////////////
	var total_value = document.getElementById("total_value_x");
	var total_value_value = total_value.value;
	////////////////////////////////////////////////////
	var total_inc_vat = document.getElementById("total_inc_vat_x");
	var total_inc_vat_value = total_inc_vat.value;
	///////////////////////////////////////////////////
	
	var temp_inv_no =  document.getElementById("inv_no");
	var temp_inv_no_value =  document.getElementById("inv_no").value;
			
	var row_count = $('#tab_logic > tbody  > tr').length;
		//alert("row_count:"+row_count);
	if((row_count>1)&&(dalivery_date_value!="")&&(temp_inv_no_value!="")){
		
		var pd_array = new Array();
		
			//tab_logic is table id
			
			$('#tab_logic > tbody  > tr').each(function() {
				if(this.id=="addr0")
				{
					
				}else{
					
					var id ;
					if(this.id.length==5)
					{
						id = this.id.slice(-1);
					}else{
						id = this.id.slice(-2);
					}
					//alert("id:"+id);
					
					pd_array.push(id);		
				}
				
			});
		
			
			for(var i=0 ; i<pd_array.length ; i++){
				
				//alert(pd_array[i]);
			}
			
			var index =  parseInt(pd_array.length, 10);
			
			
			
			var parameter_order_main = "company_id="+company_id_value+
										"&po_no="+po_no_value+
										"&dalivery_date="+dalivery_date_value+
										"&due_date="+due_date_value+
										"&total_value="+total_value_value+
										"&total_vat="+total_vat_value+
										"&total_inc_vat="+total_inc_vat_value+
										"&temp_inv_no_value="+temp_inv_no_value+
										"&compute_credit="+credit_value+
										"&index="+index;
			
			var parameter_order_detail ="";
			console.log("1429:index :"+index);
			for(var j=0;j<index ;j++)
			{
				//alert("j:"+j);
				var pd_id = document.getElementById("raw_id_x"+pd_array[j]);
				var pd_name = document.getElementById("name_x"+pd_array[j]);
				//var pd_addition = document.getElementById("addition"+pd_array[j]);
				//var	pd_addition_value ;
				var pd_price = document.getElementById("price_x"+pd_array[j]);
				var pd_quantity = document.getElementById("quantity_x"+pd_array[j]);
				var pd_sum = document.getElementById("sum_x"+pd_array[j]);
				var pd_unit = document.getElementById("unit_x"+pd_array[j]);
				//alert("pd_id:"+pd_id.value);
				/*
				if((pd_addition.value==null)||(pd_addition.value==""))
				{
					pd_addition_value="-";
				}else{
					pd_addition_value = pd_addition.value;
				}
				*/
				 parameter_order_detail =	parameter_order_detail +
					 						"&pd_id"+j+"="+pd_id.value+ 
											"&pd_name"+j+"="+pd_name.value+
	//										"&pd_addition"+j+"="+pd_addition_value+
											"&pd_price"+j+"="+pd_price.value+
											"&pd_quantity"+j+"="+pd_quantity.value+
											"&pd_sum"+j+"="+pd_sum.value+
											"&pd_unit"+j+"="+pd_unit.value;
								
				console.log("1463:parameter_order_detail:"+parameter_order_detail);
			}

			var xmlhttp;
			
			if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
			}
			else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {     
			
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
						var result = xmlhttp.responseText;
						if(result=="error")
						{
							alert("Error Occer can't create Main Order Ticket")
						}
						else{

							order_main_id = result;
							//alert("success to create order_main :"+order_main_id);
							console.log("success create order main:"+order_main_id);
							
							sessionStorage.setItem("order_id_for_get_detail", order_main_id); 
							window.location = "report_credit_inv_detail.jsp";
						}
				
					}		
			}
			
			xmlhttp.open("POST", "create_order_full_background.jsp?"+parameter_order_main+parameter_order_detail, true);
			xmlhttp.send();
			
			
			
	}else{
		//$('#myModal').modal('hide');
		alert("Some Parameter Missing");
		//  $("#myModal").modal('hide');
	  
	}
		
	
	
}

function submit_editing(){
	
	DeleteOrderForEditing();
	create_order();
	
	
}

function toggle_product_language(button){
	
	var lan_flag = button.value ; 

//	alert(button.id);
	var prefix = button.id.substring(8);
	var pd_id = document.getElementById("raw_id"+prefix); 
	//alert(pd_id);
	
	
	var xmlhttp;
	
	if(window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}
	else {
		// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.onreadystatechange = function() {
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
		  //  $('#show_inv_history').modal('show');
			var jsonObj = JSON.parse(xmlhttp.responseText);

			if(jsonObj.length == 0) {
			//	massage();
				alert("Product not found !!");
			}
			else{
				//alert("get product detail");
				var pd_name = document.getElementById("name"+prefix);
				var pd_unit = document.getElementById("unit"+prefix);
				if(lan_flag=="EN")
				{
					pd_name.value = jsonObj[0].nameTH;
					pd_unit.value = jsonObj[0].unitTH;
					button.value = "TH";
				}else{
					pd_name.value = jsonObj[0].nameEN;
					pd_unit.value = jsonObj[0].unitEN;
					button.value = "EN";
				}
	
			}

		}
		
	}
	xmlhttp.open("POST", "get_product_detail_by_product_id_background.jsp?product_id="+pd_id.value, true);
	xmlhttp.send();

	
}
function calculate_total(){
	
//	alert("calculate_total");
	
	var tbody = document.getElementById("pd_tbody");
	var total_value = document.getElementById("total_value_x");
	var total_vat = document.getElementById("total_vat_x");
	
	var total_inc_vat = document.getElementById("total_inc_vat_x");
	var total_novat = 0 ;
		total_vat.value = 0 ;
		total_inc_vat.value = 0 ;
		total_value.value = 0;
		
	$('#tab_logic > tbody  > tr').each(function() {
		
	//		alert(this.id);
			if(this.id=="addr0")
			{

			}else{
			//	alert(this.id);
				var id = this.id.substring(4);
					//alert(id);
				var sum = document.getElementById("sum_x"+id);
				
			
				
				 total_novat = parseFloat(total_novat, 10) + parseFloat(sum.value, 10 ) ; 
				 
				//alert("total_novat:"+total_novat+" sum.value:"+sum.value);
				
			}
		
	 });
	
	//need many scenario for testing 

	console.log("total_novat(1095):"+total_novat);
	var total_vat_string = String(((total_novat * 0.07) * 10 / 10).toFixed(2));
	
	console.log("total_vat_string(1097):"+total_vat_string);
	
//	var total_inc_vat_string = String(((total_novat)+(total_novat * 0.07) * 10 / 10).toFixed(2));
	var total_inc_vat_string = String(((total_novat * 1.07) * 10 / 10).toFixed(2));
	console.log("total_inc_vat_string(1102):"+total_inc_vat_string);
	
	var final_total_vat;
	var final_total_inc_vat;
	
	if(total_vat_string.indexOf(".")>0)
	{
		var total_vat_string_parts = total_vat_string.split('.');
		final_total_vat = total_vat_string_parts[0] +"."+ total_vat_string_parts[1].substring(0,2);
	}else{
		final_total_vat = total_vat_string;
	}

	
	if(total_inc_vat_string.indexOf(".")>0)
	{
		var total_inc_vat_string_parts = total_inc_vat_string.split('.');
		final_total_inc_vat = total_inc_vat_string_parts[0] +"."+ total_inc_vat_string_parts[1].substring(0,2);
	}
	else{
		final_total_inc_vat = total_inc_vat_string;
	}

	
//	total_value.value = ( parseFloat(final_total_inc_vat) - parseFloat(final_total_vat) ); Fix bug 
	total_value.value = parseFloat(total_novat);

	console.log("total_value(1122):"+total_value.value);
	
	total_vat.value =   parseFloat(final_total_vat);
	total_inc_vat.value =	parseFloat(final_total_inc_vat);
	
	
}
	function launch_edit_mode(){
		
		var default_panel =  document.getElementById('default_panel');
		var inv_no = document.getElementById('inv_no');
		var po_no = document.getElementById('po_no');
		var compute_credit = document.getElementById('compute_credit');
		var delivery_date =  document.getElementById('delivery_date');
		var due_date =  document.getElementById('due_date');
		
		document.getElementById("edit_panel").removeAttribute("hidden");
		
		inv_no.readOnly = false;
		po_no.readOnly = false;
		compute_credit.readOnly = false;
		delivery_date.readOnly = false;
		due_date.readOnly = false;
		default_panel.remove();
		
	}
	
</script>
<style>
	.text-right {
 		 text-align:right;
	}
	hr {
	  -moz-border-bottom-colors: none;
	  -moz-border-image: none;
	  -moz-border-left-colors: none;
	  -moz-border-right-colors: none;
	  -moz-border-top-colors: none;
	  border-color: #DCDCDC;
	  border-style: solid none;
	  border-width: 1px 0;
	  margin: 18px 0;
	}
	
	datalist {
			  display: none;
			}

</style>
    

</head>

<body>

    <div id="wrapper">

    		<nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>
                <div class="sidebar-header">
                    <h3>Bored Sidebar</h3>
                </div>
                
    
        		<div id="includedSidebar"></div>
        		
        	</nav>



        <!-- Navigation -->
       

        <div id="content" class="form-control">
        
        	   <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-justify"></i>
                                <span></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Page</a></li>
                       
                            </ul>
                        </div>
                    </div>
                </nav>
             <div class="row">
             <br>
	           <div class="col-lg-3">
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                          Credit Invoice
	                        </div>
	                   
	                       		 
	                        <!-- /.panel-heading -->
	                        <div class="panel-body">
	                        	
	                        	 <button class="btn btn-warning form-control"  onclick="launch_edit_mode()">Launch Edit Mode</button>
	                        	 <br>
	                        	 <br>
	                        		                       	
	                        	<div class="dropdown">
								  <button class="btn btn-primary dropdown-toggle btn-block" id="status_but" onclick="show_modal_change_status(this)" type="button" data-toggle="dropdown"><span class="caret"></span></button>
								</div>
								<br>
								   	<label>Invoice No.</label>
								<input name="inv_no" id="inv_no" type="text" class="form-control" placeholder="INV No" readonly>
	                        	<br>
	                        	<input name="order_view_id" id="order_view_id" type="text" class="form-control" placeholder="Order ID" readonly>
                                <br>	
	                        	<label>Customer Name (ชื่อลูกค้า)</label>
	                           		 <input id="inv_name" name = "inv_name" class="form-control" value="" disabled  >
	                           		 <input id ="company_id_hidden" name="company_id_hidden" type="hidden" value="" >
	                           		 <br>
	                        	<label>Address (ที่อยู่)</label>
	                        		 <textarea id="inv_address" name ="inv_address" class="form-control" rows="3" disabled></textarea>
	                        	 	 <br>
	                        	<label>Tax ID (เลขประจำตัวผู้เสียภาษี)</label>
	                        		<input class="form-control" id="inv_tax_id" name = "inv_tax_id" value="" disabled>
	                        		<br>
	                      
	                      			<table>
	                      			 		<tr class="pagination-centered">
	                      			 		
	                      			 			<td style="padding-left:0.5em;">
	                        						<label>Computed Credit   </label>	                        						
	                        					</td>
	                        					<td style="padding-left:0.5em;">
	                        					 	<input type="number" id="compute_credit" name ="compute_credit" class="form-control" readonly>  					                        					
	                        						<br>
	                        					</td>	

	                        	   			</tr>
	                        	   			
	                        	   			<tr>
	                        	   				<td>
	                        	   					
	                        	   				</td>
	                        	   			</tr>
                   	   			
	                        	   			<tr class="pagination-centered">
	                      			 			<td style="padding-left:0.5em;">
	                        						<label>Delivery Date   </label>	                        						
	                        					</td>
	                        					<td style="padding-left:0.5em;">
	                        					 	<input type="date" id="delivery_date" name ="delivery_date" class="form-control"  oninput="calculate_due_date(this.value)" readonly>  					                        					
	                        						<br>
	                        					</td>	

	                        	   			</tr>
	                        	   			<tr class="pagination-centered">
	                        	   				 <td style="padding-left:0.5em;">
	                        						<label>Due Date   </label>	                        						
	                        					</td>
	                        					<td style="padding-left:0.5em;">
	                        					 	<input type="date" id="due_date" name ="due_date" class="form-control" readonly>  
	                        					 	<br>					                        					
	                        					</td>	                  	   				
	                        	   			</tr>
	                        	   			<!-- 
	                        	   			<tr class="pagination-centered">
	                        	   				<td style="padding-left:2em;">
	                        						<label>Create Date   </label>	                        						
	                        					</td>
	                        					<td style="padding-left:1em;">
	                        					 	<input type="date" id="create_date" name ="create_date" class="form-control" readonly>  	
	                        					 	<br>				                        					
	                        					</td>	
	                        					
	                        	   			</tr>
	                        	   			 -->
	                        	   			<tr>
		                        				<td style="padding-left:0.5em;">
		                        						<label>Purchase Order No.(เลขที่ใบสั่งซื้อ)   </label>	                        						
		                        				</td>	      
		                        				<td style="padding-left:0.5em;">
		                        				 	<input id="po_no" name ="po_no" class="form-control" readonly>
		                        				 	<br>
		                        				</td>                  				
	                        			   </tr>

	                        		</table>
	                        		<hr>
	                        		<br>

	                        		<div class="row">
	                        			<div class="col-md-2 col-md-offset-3">
		                        			<button id="generate_credit_inv" class="btn btn-primary btn-md" onclick="GenerateInvoice()">Generate Invoice</button>
		                        			<br>
		                        			<br>
		                        		</div>
		                        	</div>
		                        	<div class="row">
		                        		<div class="col-md-6" style="float: none;margin: 0 auto;">
	                        			
                        							<form  method="get" action="../DownloadServlet" >
																 
																		      <input id="inv_file_name" name="inv_file_name" type="text" class="form-control" readonly>
																		      <input id="inv_file_path" name="inv_file_path" type="hidden">
																		     <br>
																		      <input type="submit" class="btn btn-primary form-control" value="Download">
																		     
																	    
																  
												   </form>
										</div>
	                        		</div>
	                        		
	                        		<div>
	                        			<hr>
	                        			<button type="button" onclick="show_confirm_delete(this.id)"  class="form-control btn btn-danger">Delete Invoice</button>
	                        		</div>
	                        		
	                     
													
													  
												
											
	                        		
	                        		
	                        		<br>
	                            	
												
													                            
	                        </div>
	                        <!-- .panel-body -->
	                    </div>
                    <!-- /.panel -->
                </div>
                 
                 
             <div class="col-lg-9">
                  
                  <div class="panel panel-default"id="edit_panel">
                  
                  		 <div class="row">
	                          		<div class="col-lg-7">
	                          				<br>
	                          				  <input list="pro_lists-0" type="text" name='pd_name-0' id = "pd_name-0" class="form-control"
																    		onkeyup="search_pro_name(this)"
																    		onchange="" />
											  <datalist id="pro_lists-0"></datalist>
	                          				
	                          				
	                          		
	                          		</div>
	                          		<div class="col-lg-2">
	                          			<br>
	                          			 <button type="button" class="btn btn-primary pull-left" onclick="grap_product()"><b>+</b> Add new row</button>
	                          		</div>
	                    
	                          		
	                       </div>
	                       <br>
	                       <div class="row">
	                          		 <div class="col-lg-12">
													<table class="table table-bordered table-hover table-sortable" id="tab_logic" onchange="calculate_total()">
														<thead>
															<tr>
																<th class="text-center">
																	Product Name
																</th>
																<th class="text-center" style="width: 90px;">
																	Unit
																</th>
																<!-- 
																<th class="text-center">
																	Addition 
																</th>
																 -->
																<th class="text-center" style="width: 110px;">
																	Price
																</th>
										    					<th class="text-center" style="width: 90px;">
																	Quantity
																</th>
																<th class="text-center" style="width: 130px;">
																	Sum
																</th>
																
										        	
																
																<th class="text-center" style="width: 90px;">
																	
																</th>
																<th class="text-center" style="width: 90px;">
																</th>
																
															</tr>
														</thead>
														<tbody id = "pd_tbody">
														
										    				<tr id='addr0' data-id="0" class="hidden">
																<td data-name="name_x">
																    <input type="text" name='pd_name_x' id = "pd_name_x" class="form-control"/>
																</td>
																<td data-name="unit_x">
																    <input type="text" name='pd_unit_x' id = "pd_unit_x" class="form-control" style="text-align:center;" />
																</td>
																
																<!-- 
																<td data-name="addition">
																    <input type="text" name='pd_addition' id="pd_addition" placeholder='Color , Perfume Code , etc' class="form-control"/>
																</td>
																 -->
																 
																<td data-name="price_x">
																    <input type ="text" name = 'pd_price_x' id="pd_price_x" class="form-control text-right" oninput="calculate_sum(this.id)" >
																</td>
										    					<td data-name="quantity_x">
																    <input type ="number" name = 'pd_quantity_x' id="pd_quantity_x" class="form-control text-right" oninput="calculate_sum(this.id)" >
																</td>
																<td data-name="sum_x">
																	 <input readonly type="text" name='pd_sum_x' id = "pd_sum_x" class="form-control text-right" onchange="calculate_total()"/>
																	 
																</td>
																
																<td data-name="lan_flag_x">
																	<input type="button" id="but_lan_flag_x" class="btn btn-default btn-md pull-right" onclick="toggle_product_language(this)" value="XX"> <br>
																	
																	<input type="hidden" id="pd_lan_flag" >
																	 
																</td>
																
																
																<td data-name="raw_id_x" class="hidden">
																	<input type="hidden" name="pd_id_x" id="pd_id_x">
																</td>
																 
																
										                        <td data-name="del_x">
										                            <button name="del0" class='btn btn-danger glyphicon glyphicon-remove row-remove' ></button>
										                        </td>
										                        
															</tr>
														</tbody>
													</table>	
													
													<table class="table table-bordered table-hover table-sortable " id="table_summary" style="border:none;">
													
																<tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Total Value : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input  type="text" name="total_value_x" id="total_value_x" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>

														 <tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Vat (7%) : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input  type="text" name="total_vat_x" id="total_vat_x" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>
												
															<tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Total Inc VAT : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input  type="text" name="total_inc_vat_x" id="total_inc_vat_x" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>
															
															<tr>
																<td class=" col-lg-6 text-right" style="border:none;">
														      			 
														      	</td>
														      		
																 <td class=" col-lg-6 text-right" style="border:none;">
															
																		  <button type="button" id="gen_inv" class="btn btn-primary btn-md"  onclick="submit_editing(this.id)">Save Changes</button>
																		 
																	
																
																  </ul>
																	</div>
																</td>
															</tr>
													
													</table>
										</div>                       
	                        </div>
	                       
         
	                       
                  </div>
                  <div id="default_panel">
                  
                  			 <div class="panel-body">
                  			 
                  			 	 <div class="alert alert-success fade in alert-dismissable" id="alert_saved_time" style="display: none">
	                                <button id="alert_saved_time_but" type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                                Save Change on : 14.10.2016 ,8.37 AM
                            	</div>
                  			 		
                  			 </div>
                  			
                  
	           
	                        <table class="table table-striped table-bordered table-hover" id="order-detail-dataTables-example">
									<thead>
										<tr>
											<th class="text-center" style="width: 200px;">
												Product Name
											</th>
											<th class="text-center" style="width: 90px;">
												Cost/Unit
											</th>
											<th class="text-center" style="width: 90px;">
												Price/Unit
											</th>
											<th class="text-center" style="width: 90px;">
												Profit/Unit
											</th>
											<th class="text-center" style="width: 90px;">
												Profit(%)
											</th>
											
					    					<th class="text-center" style="width: 50px;">
												Quantity
											</th>
											<th class="text-center" style="width: 90px;">
												Unit
											</th>
											<th class="text-center" style="width: 100px;">
												Sum Cost
											</th>
											<th class="text-center" style="width: 100px;">
												Sum
											</th>
										    <th class="text-center" style="width: 100px;">
												Sum Profit
											</th>
											
					        				<th class="text-center" style="border-top: 1px solid #ffffff; border-right: 1px solid #ffffff; width: 20px;">
											</th>
											
										</tr>
									</thead>
									<tbody id = "pd_tbody">
									
					    				<tr id='addr0' data-id="0" class="hidden">
											<td data-name="name" style=" width : 200px;">
											    <input type="text" name='pd_name' id = "pd_name" class="form-control" />
											</td>
											<td data-name="cost_per_unit" style="text-align:center; width : 100px;" >
											    <input type="text"   id = "pd_cost_per_unit" class="form-control" style="text-align:right;" onchange="calculate_each_row(this.id)"    />
											</td>
											<td data-name="price" style=" width : 100px;">
											    <input type ="text" name = 'pd_price' id="pd_price" class="form-control text-right"   onchange="calculate_each_row(this.id)"  />
											</td>
											<td data-name="profit_per_unit" style="text-align:center; width : 80px; ">
											    <input type="text" name='pd_profit_per_unit' id = "pd_profit_per_unit" class="form-control" style="text-align:right;"    readonly  />											    
											</td>		
											<td data-name="percent_profit" style="text-align:center; width : 50px; ">
											    <input type="text" name='pd_percent_profit' id = "pd_percent_profit" class="form-control" style="text-align:center;"    readonly  />											    
											</td>											
					    					<td data-name="quantity" style="text-align:center; width : 100px;" >
											    <input type ="number" name = 'pd_quantity' id="pd_quantity" class="form-control text-right" onchange="calculate_each_row(this.id)"  />
											</td>
											<td data-name="unit" style="text-align:center; width : 50px;" >
												 <input  type="text" name='pd_unit' id = "pd_unit" class="form-control text-center" />
												 
											</td>
											<td data-name="sum_cost" style="width : 100px;">
												 <input  type="text" name='pd_sum_cost' id = "pd_sum_cost" class="form-control text-right"  readonly />
												 
											</td>
											<td data-name="sum" style="width : 100px;">
												 <input  type="text" name='pd_sum' id = "pd_sum" class="form-control text-right"  readonly />
												 
											</td>
											<td data-name="sum_profit" style=" width : 100px;">
												 <input  type="text" name='pd_sum_profit' id = "pd_sum_profit" class="form-control text-right"   readonly />
												 
											</td>
											<td data-name="product_id" style=" width : 100px;">
											    <button onclick="calculate_cost(this.value)"  class="btn btn-warning btn-circle btn-md" >
											   		 <i class="glyphicon glyphicon-transfer"></i>
											    </button>
												<input type="hidden" name="index" id="pd_product_id">
												
											</td>
											 
										
					             
										</tr>
									</tbody>
								</table>	
								
								<table class="table table-bordered table-hover table-sortable " id="table_summary" style="border:none;">
								
								
														 <tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Total Sum Cost : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input readonly type="text" name="total_sum_cost" id="total_sum_cost" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>
														  <tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Total Sum  : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input readonly type="text" name="total_sum" id="total_sum" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>
															 <tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Avg Profit Percentage  : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input readonly type="text" name="avg_profit_percent" id="avg_profit_percent" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="%">
																 	 </div>
																</td>
															</tr>
															
															 <tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Total Sum Profit  : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input readonly type="text" name="total_sum_profit" id="total_sum_profit" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>

														 <tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Vat (7%) : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input readonly type="text" name="total_vat" id="total_vat" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>
												
															<tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Total Inc Vat : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input readonly type="text" name="total_inc_vat" id="total_inc_vat" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>
															
									
											</table>
								
								<div class="col-lg-2">
									
								</div>
								<div class="col-lg-2">  
									
								</div>
								<div class="col-lg-2">
									
								</div>
								<div class="col-lg-2">
									
								</div>
								<div class="col-lg-2">
									
								</div>
								<div class="col-lg-2">
									<br>
									<button type="button" class="form-control btn btn-primary" onclick="save_order_updated()">Save</button>
								</div>
	                        
	                        
	                       
	                        <!-- .panel-body -->
	               
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
           </div>
      

        
        </div>
    <div class="modal fade" id="status_detail_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style=" position: fixed; top: 10%; left: 10%; right: 10%; bottom: 15% ">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Change Status</h4>
                                        </div>
                                        <div class="modal-body">
                                       		<div class="row">
                                       				 <div class="panel-body">
                                       				 	    <div class="dropdown">
																  <button class="btn btn-primary dropdown-toggle btn-block" id="modal_status_but" name="" type="button" data-toggle="dropdown"><span class="caret"></span></button>
																   <ul id="modal_status_ul" class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1" style="left: 50% !important;
																																			    right: auto !important;
																																			    text-align: center !important;
																																			    transform: translate(-50%, 0) !important;">
																        <li><a href="#" onclick="change_status(this)" id="pending">Pending</a></li>
																        <li><a href="#" onclick="change_status(this)" id="during_delivery">During Delivery</a></li>
																        <li><a href="#" onclick="change_status(this)" id="delivered">Delivered</a></li>
																        <li><a href="#" onclick="change_status(this)" id="billing">Billing</a></li>
																        <li><a href="#" onclick="change_status(this)" id="completed">Completed</a></li>
																        
																  </ul>
															</div>
															<br>
														   <div class="form-group">
																  <label for="comment">Note :</label>
																  <textarea class="form-control" rows="3" id="modal_note"></textarea>
													  	  </div>
													  	  <div>
													  		  <div>
																		  	
				                        						  <label>Complete Date :</label>	                        						
				                        					      <input type="date" id="complete_date" name ="complete_date" class="form-control" >  
				                        					      <br>		
				                        					      <label>Payment Reference :</label>	                        						
				                        					      <input type="text" id="payment_ref" name ="payment_ref" class="form-control" >  
				                        					      <br>						                        					
											  				  </div>
					                                       					
                                       					</div>
                                       		</div>
                                       		                       
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" name="submit_change_status_but"  id="submit_change_status_but" class="btn btn-primary" onclick="submit_change_status()">Submit Change</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
     </div>
        <!-- /#page-wrapper -->

    </div>
     <div class="modal fade" id="confirm-delete-order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			    <div class="modal-dialog">
			        <div class="modal-content">
			        	<form>
					          <div class="modal-body">
								    <textarea id="reason_delete" class="form-control" rows="2" placeholder="Reason for deleting this order"></textarea>
								    
								    <input type="hidden" id="delete_order_id">
							  </div>
							  <div class="modal-footer">
								    <button type="button" data-dismiss="modal" class="btn btn-danger" id="but_delete_order" onclick="DeleteOrder()">Delete</button>
								    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
							  </div>
						 </form>
			        </div>
			    </div>
		  </div>            
		  </div>     
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
  <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    
    
    <script>
	function show_confirm_delete(){
		$('#confirm-delete-order').modal('show');
		
		//  document.getElementById("delete_order_id").value = id;

	}
	function DeleteOrder()
	{
		//alert("DeleteOrder");
		var order_id = document.getElementById("order_view_id").value;
		var reason = document.getElementById("reason_delete").value;
		var inv_prefix = order_id.charAt(0)+order_id.charAt(1);
		//alert("inv_prefix"+inv_prefix);
		if(inv_prefix=="OR")
		{
			//alert("2398");
			
			//var cash_bill_id = order_id;
			var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
						alert(xmlhttp.responseText);
						if(xmlhttp.responseText=="success")
						{
						window.open("dashboard_main.jsp");
							
						}else{
							
						}

				}// end if check state
			}// end function
			
			//alert("2426");
			xmlhttp.open("POST", "delete_single_order_background.jsp?order_id="+order_id+"&reason="+reason, true);
			xmlhttp.send();
			
			
			
		}else{
			
			
    		
    		
		}

		
	}
	
	function redirect_to_dashboard_main(){
		
		var xmlhttp;
		
		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange = function() {
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText=="success")
					{
						window.open("dashboard_main.jsp");
						
					}else{
						
					}

			}// end if check state
		}// end function
		

		xmlhttp.open("POST", "delete_single_order_background.jsp?order_id="+order_id+"&reason="+reason, true);
		xmlhttp.send();
		
	}
	
    $(document).ready(function() {
	
    	$("#includedSidebar").load("sidebar.jsp"); 
    	
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').fadeOut();
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        document.getElementById('edit_panel').setAttribute("hidden","");
        
        
    	fetch_order_main();
    	fetch_order_detail();
    	

    	
    });
    </script>

</body>

</html>
