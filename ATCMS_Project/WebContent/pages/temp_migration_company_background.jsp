<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>


<% 

	System.out.println("Start temp_migration_company_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
	///////////////////file_path parameter/////////////
	String file_pathx = request.getParameter("file_path");
	System.out.println("File_path background:"+file_pathx);
	 ///////////////////////////////////////////////////
	 
	 ///////////////////init parameter ///////////////
	 int i = 0;
     int num_row = 0;
     int num_col  = 0;
     int count = 0;
     List<String> fail_list = new ArrayList<String>();
     String[] header_array = new  String[]{"code_name","name_th","name_en","address",
    		 							   "address_th","address_th_line1","address_th_line2",
    		 							   "address_en","address_en_line1","address_en_line2",
    		 							   "tax_id","type"};

	
     System.out.println("Header_array size : "+ header_array.length);
     ////////////////////////////////////////////////
     /////////////////Data Parameter////////////////
     String code_name = "";
     String name_th = "" ;
     String name_en = "" ; 
     String address = "" ;
     String address_th = "";
     String address_th_line1 = "";
     String address_th_line2 = "";
     String address_en = "";
     String address_en_line1 = "";
     String address_en_line2 = "";
     String tax_id = "" ; 
     String type ="";
     ///////////////////////////////////////////////
     
     
   
     
     
     try{
    	 
   		FileInputStream file = new FileInputStream(new File(file_pathx));
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		 
         //Get first/desired sheet from the workbook
         XSSFSheet sheet = workbook.getSheetAt(0);
         //Iterate through each rows one by one
         Iterator<Row> rowIterator = sheet.iterator();
         
         Iterator<Row> headerIt_row_check = sheet.iterator();
         Row row_check = headerIt_row_check.next();
         Iterator<Cell> headerIt_cell_check = row_check.cellIterator();
         while (rowIterator.hasNext())
         {
        	 
        	 if(count==0)
             {
         		while(headerIt_cell_check.hasNext()){
         			Cell header_cell = headerIt_cell_check.next();
         			   switch (header_cell.getCellType())
                        {
                            case Cell.CELL_TYPE_NUMERIC:
                                break;
                            case Cell.CELL_TYPE_STRING:
                               // System.out.print(header_cell.getStringCellValue()+ " ");
                               	 System.out.println(header_array[i]+":"+header_cell.getStringCellValue());
                                	 if(header_array[i].equals(header_cell.getStringCellValue()))
                                	 {
                                		 System.out.println("Pass:" + i);
                                	 }
                                	 else{
                           
                                		throw new Exception();           		
                                	 }
                                	 
                                break;
                        }// end switch
         				i++;
         		}// end headerIt_cell_check.hasNext()
         		System.out.println("");
         		count = count +1;
         		rowIterator.next();
         		System.out.println("////////// END Header////////////");        		        		
         	 }// end if(count==0)
        	 
         	try{
         		System.out.println("Row:"+num_row);
         		Row row = rowIterator.next();
         		Iterator<Cell> cellIterator = row.cellIterator();
         	    while (cellIterator.hasNext())
                {
                    Cell cell = cellIterator.next();
                    switch(num_col){
	                	case 0 :
	                		code_name = cell.getStringCellValue();
	      					code_name.replaceAll("\\s+","");
	      					System.out.println("code_name:"+code_name);
	                		
	           			break;
	           			
	           			case 1 :
	           				name_th = cell.getStringCellValue();
	      					name_th.replaceAll("\\s+","");
	      					System.out.println("name_th:"+name_th);	
	           			break;
	           			
	           			case 2 :
	           				name_en = cell.getStringCellValue();
	      					name_en.replaceAll("\\s+","");
	      					System.out.println("name_en:"+name_en);
	           			break;
	                    
	           			case 3 :
	           				address = cell.getStringCellValue();
	      					address.replaceAll("\\s+","");
	      					System.out.println("address:"+address);
	      					
	           			break;
                    	
	           			case 4 :
	           				address_th = cell.getStringCellValue();
	      					address_th.replaceAll("\\s+","");
	      					System.out.println("address_th:"+address_th);
	      					
	           			break;
	           			
	           			case 5 :
	           				address_th_line1 = cell.getStringCellValue();
	           				address_th_line1.replaceAll("\\s+","");
	      					System.out.println("address_th_line1:"+address_th_line1);
	      					
	           			break;
	           			
	           			case 6 :
	           				address_th_line2 = cell.getStringCellValue();
	           				address_th_line2.replaceAll("\\s+","");
	      					System.out.println("address_th_line2:"+address_th_line2);
	      					
	           			break;
	           			
	           			
	           			case 7 :
	           				address_en = cell.getStringCellValue();
	      					address_en.replaceAll("\\s+","");
	      					System.out.println("address_en:"+address_en);
	      					
	           			break;
	           			
	           			case 8 :
	           				address_en_line1 = cell.getStringCellValue();
	           				address_en_line1.replaceAll("\\s+","");
	      					System.out.println("address_en_line1:"+address_en_line1);
	      					
	           			break;
	           			
	           			case 9 :
	           				address_en_line2 = cell.getStringCellValue();
	           				address_en_line2.replaceAll("\\s+","");
	      					System.out.println("address_en_line2:"+address_en_line2);
	      					
	           			break;
	           			
	           			case 10 :
	           				tax_id = cell.getStringCellValue();
	      					tax_id.replaceAll("\\s+","");
	      					System.out.println("tax_id:"+tax_id);
	           			break;
	           			
	           			case 11 :
	           				type = cell.getStringCellValue();
	           				type.replaceAll("\\s+","");
	      					System.out.println("type:"+type);
	           			break;
	           			
                    }// end switch case 
                    num_col++;
         	    	
                }// end while(cellIterator.hasNext())
         	    num_col=0;
                num_row++;
                ///////////////Process Add to Database each row /////////////////
                String company_id = KeyGen.genTimeStamp();
               
                int credit = 30;
                
                
                System.out.println("Start Import to Database ");
                String sql = " INSERT INTO `company`("+
                								    "`name_th` "+
                									", `name_en` "+
                								    ", `address` "+
                									", `type` "+
                								    ", `email` "+
                									", `credit`"+
                								    ", `tax_id`"+
                									", `address_en`"+
                									", `address_en_line1`"+   
                									", `address_en_line2`"+   
                								    ", `address_th`"+
                								    ", `address_th_line1`"+   
                        							", `address_th_line2`"+   
                								    ", `code_name` "+
                								    ", `company_id`)"+
                			 " VALUES ("+
                		              "'"+name_th+"'" +
                					  ",'"+name_en +"'"+
                					  ",'"+address+"'"+
                					  ",'"+type+"'"+
                					  ","+null +
                					  ","+credit + 
                					  ",'"+tax_id+"'"+
                					  ",'"+address_en+"'"+
                					  ",'"+address_en_line1+"'"+
                					  ",'"+address_en_line2+"'"+
                					  ",'"+address_th+"'"+
                					  ",'"+address_th_line1+"'"+
                        			  ",'"+address_th_line2+"'"+
                					  ",'"+code_name+"'"+
                					  ",'"+company_id+"')";
                

                		
				System.out.println("sql:"+sql);		
				PreparedStatement pstmt = connect.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);  
				pstmt.executeUpdate();  
				ResultSet keys = pstmt.getGeneratedKeys();    
				keys.next();  
				int key = keys.getInt(1);
				System.out.println("Adding Success with company_id:"+key);
				
                /////////////////////////////////////////////////////////////////
               
         	}catch(Exception getData){
         		System.out.println("Error cant't read Excel:"+getData);
                
        		num_col = 0;
        		fail_list.add(num_row+1+"");
        		num_row++;
         		
         	}
            
         }// end while (rowIterator.hasNext())
     }//end try fileinputstream
		catch(Exception x){
			System.out.println(x);
			  connect.close();
			out.print("fail");
			
     }
    out.println("success");
	connect.close();
%>
