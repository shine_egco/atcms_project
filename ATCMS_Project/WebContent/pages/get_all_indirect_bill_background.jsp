<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>



<% 

	System.out.println("Start get_all_direct_bill_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

	List<Purchase> direct_bill_list = new ArrayList<Purchase>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	
	String purchase_id = request.getParameter("purchase_id");
	System.out.println("purchase_id:"+purchase_id);

      try{
    	  
    	  String sql_query = " SELECT purchase_main.delivery_date "+
    	  					         " ,purchase_main.due_date "+
    	  					         " ,purchase_main.inv_no "+
    	  							 " ,purchase_main.create_date "+
    	  					         " ,purchase_main.purchase_id "+
    	  							 " ,purchase_main.status "+
    	  							 " ,purchase_main.total_inc_vat "+
    	  					         " ,purchase_main.total_value "+
    	  							 " ,purchase_main.total_vat "+
    	  					         " ,purchase_main.type "+
    	  							 " ,company.name_th "+
    	  					         " ,company.name_en "+
    	  							 " ,company.address_th "+
    	  					         " ,company.address_en "+
							" FROM purchase_main"+
		    			    " JOIN company "+
							" ON  purchase_main.vendor_id = company.company_id "+
							" WHERE purchase_main.type_of_purchase='indirect'";
    	  System.out.println("sql_query:"+sql_query);
    	  ResultSet rs_pur = connect.createStatement().executeQuery(sql_query);
          
    	  while(rs_pur.next())
          {
              Purchase pur = new Purchase();
           	   		 pur.setCompletedDate("completed_date");
             		 pur.setCreateDate("create_date");
             		 pur.setVendorId("vendor_id");
              		
              		if(("-").equals(rs_pur.getString("name_th")))
              		{
              			pur.setVendorName(rs_pur.getString("name_en"));
              			pur.setVendorAddress(rs_pur.getString("address_en"));
              			
              		}else{
              			pur.setVendorName(rs_pur.getString("name_th"));
              			pur.setVendorAddress(rs_pur.getString("address_th"));
              		}
              		
              		pur.setDeliveryDate(rs_pur.getString("delivery_date"));
              		pur.setDueDate(rs_pur.getString("due_date"));
              		pur.setInvNo(rs_pur.getString("inv_no"));
              		pur.setCreateDate(rs_pur.getString("create_date"));
              		pur.setPurchaseId(rs_pur.getString("purchase_id"));
              		pur.setPurchaseStatus(rs_pur.getString("status"));
              		pur.setTotalIncVat(rs_pur.getString("total_inc_vat"));
              		pur.setTotalValue(rs_pur.getString("total_value"));
              		pur.setTotalVat(rs_pur.getString("total_vat"));
              		pur.setType(rs_pur.getString("type"));
	
				//	System.out.println("Q:path:"+rs_ord.getString("inv_file_path"));
				//	System.out.println("Q:name:"+inv_file_name);
					
					
              direct_bill_list.add(pur);
              
          }
             
         	try {
				Json = mapper.writeValueAsString(direct_bill_list); 
				System.out.println("Json:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	out.print(Json);
	   
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  connect.close();
    	  out.print("fail");
      }
     
  
	connect.close();
%>
