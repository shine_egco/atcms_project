<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    
    
    
   
  

    <title> ATCMS Prototype</title>

    <!-- 
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <!--  Bootstrap Core CSS -->
   <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
   

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
	
    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../bower_components/sidebar/sidebar_style_3.css" rel="stylesheet" type="text/css">
   

 <!--  
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
 	 -->


      <% 
    	System.out.println("Start dashboard_main BM ");
  		//set Database Connection
  		String hostProps = "";
  		String usernameProps  = "";
  		String passwordProps  = "";
  		String databaseProps = "";
  		
  		try {
  			ServletContext servletContext = request.getSession().getServletContext();
  			
  			InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
  			Properties props = new Properties();
  			
  			props.load(input);

  			hostProps  = props.getProperty("host");
  			usernameProps  = props.getProperty("username");
  			passwordProps  = props.getProperty("password");
  			databaseProps = props.getProperty("database");
  		} catch (Exception e) { 
  			out.println(e);  
  		}
  	
  		// connect database
  		Statement statement = null;
		Connection connect = null;	
		
		List<Order> order_list = new ArrayList<Order>();
		List<CashBill> cash_bill_list = new ArrayList<CashBill>();
		List<ShortBill> short_bill_list = new ArrayList<ShortBill>();
		List<Quotation> quotation_list = new ArrayList<Quotation>();
		
  		try {
				Class.forName("com.mysql.jdbc.Driver");
				
				Date current_date = new Date();
				
				String current_year = current_date.getYear()+1900+"";
				String current_month = current_date.getMonth()+1+"";
	
				connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
						"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");//////"&characterEncoding=tis620" Encoding Thai);
	
				if(connect != null){
				   System.out.println("Database Connect Sucesses.");
				   
				   String sql_query_credit_bill = " SELECT order_main.order_id , order_main.status "+
				   					  " ,company.company_id ,company.name_th , company.name_en "  +
				   					  " ,order_main.inv_no "+
					         " FROM order_main "+
							 " LEFT JOIN company "+
							 " ON order_main.customer_id = company.company_id "+
							 " WHERE company.type = 'customer'"+
							 " AND YEAR(order_main.delivery_date)='"+current_year+"'"+
							 " ORDER BY order_main.inv_no DESC "+
							 " LIMIT 50 ";
				   
				   System.out.println("sql_query_credit_bill:"+sql_query_credit_bill);
			 	   ResultSet rs_credit_bill = connect.createStatement().executeQuery(sql_query_credit_bill);
			 	   
			 	   
			 	   	
				 	  while(rs_credit_bill.next())
					  {
					 		if(!("00/0000").equals(rs_credit_bill.getString("inv_no")))  
							{
					 			 Order ord = new Order();
									ord.setCustomerId(rs_credit_bill.getString("company_id"));
									ord.setOrderId(rs_credit_bill.getString("order_id"));
									ord.setOrderStatus(rs_credit_bill.getString("status"));
									if(("-").equals(rs_credit_bill.getString("name_th")))
									{
										ord.setCustomerName(rs_credit_bill.getString("name_en"));
									}else{
										ord.setCustomerName(rs_credit_bill.getString("name_th"));									
									}
									ord.setInvNo(rs_credit_bill.getString("inv_no"));
					 		   
					 		   order_list.add(ord);
							}
					 		  
					  }
				 	  /////////////////////////////////////////////////////////////////////////////////////////////////
				 	 String sql_query_cash_bill = " SELECT cash_bill_main.cash_bill_id , cash_bill_main.status "+
		   					  " ,company.company_id ,company.name_th , company.name_en "  +
		   					  " ,cash_bill_main.inv_no "+
			         " FROM cash_bill_main "+
					 " LEFT JOIN company "+
					 " ON cash_bill_main.customer_id = company.company_id "+
					 " WHERE company.type = 'customer'"+
					 " AND YEAR(cash_bill_main.bill_date)='"+current_year+"'"+
					 " ORDER BY cash_bill_main.inv_no DESC "+
					 " LIMIT 50 ";
				 	  
				 	 System.out.println("sql_query_cash_bill:"+sql_query_cash_bill);
				 	 ResultSet rs_cash_bill = connect.createStatement().executeQuery(sql_query_cash_bill);
				  	
				 	  while(rs_cash_bill.next())
					  {
					 		if(!("c00/0000").equals(rs_cash_bill.getString("inv_no")))  
							{
					 			CashBill cash = new CashBill();
					 			cash.setCustomerId(rs_cash_bill.getString("company_id"));
					 			cash.setCashBillId(rs_cash_bill.getString("cash_bill_id"));
					 			cash.setStatus(rs_cash_bill.getString("status"));
									if(("-").equals(rs_cash_bill.getString("name_th"))||(("").equals(rs_cash_bill.getString("name_th"))))
									{
										cash.setCustomerName(rs_cash_bill.getString("name_en"));
									}else{
										cash.setCustomerName(rs_cash_bill.getString("name_th"));									
									}
									cash.setInvNo(rs_cash_bill.getString("inv_no"));
					 		   
									cash_bill_list.add(cash);
							}
					 		  
					  }
				 	  
				 	  
				 	  
				 	  ///////////////////////////////////////////////////////////////////////////////////////////////
				 	  
				 	  
				 	  
				 	   String sql_query_short_bill = " SELECT * "+
			         " FROM short_bill_main "+
					 " ORDER BY short_bill_main.inv_no DESC "+
					 " LIMIT 50 ";
				 	  
				 	 System.out.println("sql_query_short_bill:"+sql_query_short_bill);
				 	 ResultSet rs_short_bill = connect.createStatement().executeQuery(sql_query_short_bill);
				  	
				 	  while(rs_short_bill.next())
					  {
				 		 System.out.println("Active 1 ");
					 		if(!("SB000000").equals(rs_short_bill.getString("inv_no")))  
							{
					 			 System.out.println("Active 2 ");
					 			ShortBill shortbill = new ShortBill();
					 		//	System.out.println("Active 3 ");
					 			shortbill.setshortBillId(rs_short_bill.getString("short_bill_id"));
					 		//	System.out.println("Active  4");
					 			shortbill.setCustomerName(rs_short_bill.getString("customer_name"));
					 		//	System.out.println("Active 5 ");
					 			shortbill.setCustomerTel(rs_short_bill.getString("customer_tel"));
					 		//	System.out.println("Active 6 ");
								shortbill.setInvNo(rs_short_bill.getString("inv_no"));
							//	System.out.println("Active 7 ");
								short_bill_list.add(shortbill);
							//	 System.out.println("Active 8 ");
							}
					 		  
					  }
				 	  
				 	 /////////////////////////////////////////////////////////////////////////////////////////////////
				 	 
					 	 String sql_query_quotation = " SELECT *"+
				         " FROM quotation_main "+
						 " LEFT JOIN company "+
						 " ON quotation_main.customer_id = company.company_id "+
						 " WHERE  YEAR(quotation_main.quotation_date)='"+current_year+"'"+
						 " ORDER BY quotation_main.quotation_no DESC "+
						 " LIMIT 50 ";
					 	  
					 	 System.out.println("sql_query_quotation:"+sql_query_quotation);
					 	 ResultSet rs_quotation = connect.createStatement().executeQuery(sql_query_quotation);
					  	
					 	  while(rs_quotation.next())
						  {
					 		 Quotation quo_unit = new Quotation();
					 		 
					 		 if((rs_quotation.getString("name_th").equals("-"))||(rs_quotation.getString("name_th").equals("")))
					 		 {
					 			 quo_unit.setCustomerName(rs_quotation.getString("name_en"));
					 		 }else{
					 			quo_unit.setCustomerName(rs_quotation.getString("name_th"));
					 		 }
					 			 		 
					 		 
					 		 quo_unit.setQuotationNo(rs_quotation.getString("quotation_no"));
					 		 quo_unit.setQuotationDate(rs_quotation.getString("quotation_date"));
					 		 quo_unit.setTotalValue(rs_quotation.getString("total_value"));
					 		quo_unit.setQuotationId(rs_quotation.getString("quotation_id"));
					 		
					 		 quotation_list.add(quo_unit);
						 		  
						  }
				 	  
				 	  
				  
	
				  } else {
					  
			       System.out.println("Database Connect Failed.");	
			       
			      }

		} catch (Exception e) {
			out.println(e.getMessage());
			e.printStackTrace();
		}
    	
    %>
    <script>
    	function show_order_detail(order_id){
    		
				sessionStorage.setItem("order_id_for_get_detail", order_id); 
				
				window.open("report_credit_inv_detail.jsp");

    	}
    	
    	
    	function show_cash_bill_detail(cash_bill_id){
		
    		sessionStorage.setItem("cash_bill_id_for_get_detail", cash_bill_id); 
			
			window.open("report_cash_bill_detail.jsp");

   		} 	function show_short_bill_detail(short_bill_id){
		
    		sessionStorage.setItem("short_bill_id_for_get_detail", short_bill_id); 
			
			window.open("report_short_bill_detail.jsp");

   		}
    	

    	function show_confirm_delete(id){
    		$('#confirm-delete-order').modal('show');
    		  document.getElementById("delete_order_id").value = id;

    	}
    	function DeleteOrder(form_id)
    	{
    		var frm_element = form_id;
    		var order_id = frm_element.elements["delete_order_id"].value;
    		var reason = document.getElementById("reason_delete").value;
    		var inv_prefix = order_id[0]+order_id[1];
    		if(inv_prefix=="CB")
    		{
    			var cash_bill_id = order_id;
    			var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
							alert(xmlhttp.responseText);
							if(xmlhttp.responseText=="success")
							{
								location.reload();
								
							}else{
								
							}

					}// end if check state
				}// end function
				

				xmlhttp.open("POST", "delete_single_cash_bill_background.jsp?cash_bill_id="+cash_bill_id+"&reason="+reason, true);
				xmlhttp.send();
    			
    			
    			
    		}else{
	    		
	    		var xmlhttp;
					
					if(window.XMLHttpRequest) {
						// code for IE7+, Firefox, Chrome, Opera, Safari
						xmlhttp = new XMLHttpRequest();
					}
					else {
						// code for IE6, IE5
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					
					xmlhttp.onreadystatechange = function() {
						if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
								alert(xmlhttp.responseText);
								if(xmlhttp.responseText=="success")
								{
									location.reload();
									
								}else{
									
								}
	
						}// end if check state
					}// end function
					
	
					xmlhttp.open("POST", "delete_single_order_background.jsp?order_id="+order_id+"&reason="+reason, true);
					xmlhttp.send();
    		}

    		
    	}
    	
    
 
    </script>
   


</head>

<body>

    <div class="wrapper">
    
    		<nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>
                <div class="sidebar-header">
                    <h3>Bored Sidebar</h3>
                </div>
                
    
        		<div id="includedSidebar"></div>
        		
        	</nav>



        <!-- Navigation -->
       

        <div id="content" class="form-control">
        
        	   <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-justify"></i>
                                <span></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Page</a></li>
                       
                            </ul>
                        </div>
                    </div>
                </nav>
                
                

            <!-- /.row -->
            <div class="row">
               
				<div class="col-lg-6 col-md-6">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-files-o fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                     <div class="huge" id=cash_total_inc_vat_current_month>Temp Delivery Note</div>
                                     <div id="cash_sub_detail">Pending for xxxxxx Doc </div>
                                </div>
                            </div>
                        </div>
                        <a href="dashboard_create_cash_bill.jsp">
                            <div class="panel-footer">
                                <span class="pull-left">--</span>
                                <span class="pull-right"> Create <i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-credit-card fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                     <div class="huge" id=cash_total_inc_vat_current_month>-----</div>
                                     <div id="cash_sub_detail"> ------</div>
                                </div>
                            </div>
                        </div>
                        <a href="dashboard_create_cash_bill.jsp">
                            <div class="panel-footer">
                                <span class="pull-left">--</span>
                                <span class="pull-right">View <i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
               
            </div>
            
            
            
        	<div class="row">	
                <div class="col-lg-6">
                	 <div class="panel panel-default">
                	    <div class="panel-heading">
                           	Temp Delivery Note List
                        </div>
                       <div class="panel-body">
                        
                   				<!-- Left Conner  -->
                   				
                   			<div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example-order">
                                    <thead>
                                        <tr>
                                            <th>Invoice No.</th>
                                            <th>Customer Name</th>
                                            <th>Status</th>
                                            
                                            <th></th>
                                          	<th></th>
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        
                                      
                                           <%
                                        for(int i=0 ; i<order_list.size();i++)
                                        {
                                        %>
                                        <tr>	
                                        	<td><%= order_list.get(i).getInvNo()%></td>
                                        	 <td><%= order_list.get(i).getCustomerName()%></td>
                                        
                                        	<td> <div class="text-center">  <%= order_list.get(i).getOrderStatus()%> </div></td>
                                        	<td> 
                                        		
	                                        	<div class="text-center">  	
	                                        	<button id = "<%=order_list.get(i).getOrderId() %>" type="button" class="btn btn-info btn-circle btn-md" onclick = "show_order_detail(this.id)">
	                                        			<i class="glyphicon glyphicon-search"></i>            	
	                                        						
	                            					</button> 
	                            					
	                            				</div>
                                        		
                                        	</td>
                                        	<td> 
                                        		
	                                        	<div class="text-center">  	
	                                        	<button id = "<%=order_list.get(i).getOrderId() %>" type="button" class="btn btn-danger btn-circle btn-md" onclick="show_confirm_delete(this.id)">
	                                        			<i class="glyphicon glyphicon-trash"></i>            	
	                                        						
	                            					</button> 
	                            					
	                            				</div>
                                        		
                                        	</td>
                                        	
                                        	
                                        </tr>
                                     	<% 
                                        }
                                        
                                        %>
                                       
                                    
                                     
                                        
                                    </tbody>
                                </table>
                            </div>
                                 
                                  
                           </div>
                       </div> 
                </div>
                <div class="col-lg-6">
                	 <div class="panel panel-default">
                	    <div class="panel-heading">
                           	Cash Invoice List
                        </div>
                       <div class="panel-body">
                        
                   				<!-- Left Conner  -->
                   				
                   			<div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example-cash-bill">
                                    <thead>
                                        <tr>
                                            <th>Invoice No.</th>
                                            <th>Customer Name</th>
                                            <th>Status</th>
                                            
                                            <th></th>
                                          	<th></th>
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        
                                      
                                     
                                       
                                    
                                     
                                        
                                    </tbody>
                                </table>
                            </div>
                                 
                                  
                           </div>
                       </div> 
                </div>

                
              
            </div>
            
            
           
             
        
        
        </div>
	       
         
     
          <div class="modal fade" id="confirm-delete-order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			    <div class="modal-dialog">
			        <div class="modal-content">
			        	<form>
					          <div class="modal-body">
								    <textarea id="reason_delete" class="form-control" rows="2" placeholder="Reason for deleting this order"></textarea>
								    
								    <input type="hidden" id="delete_order_id">
							  </div>
							  <div class="modal-footer">
								    <button type="button" data-dismiss="modal" class="btn btn-danger" id="but_delete_order" onclick="DeleteOrder(this.form)">Delete</button>
								    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
							  </div>
						 </form>
			        </div>
			    </div>
		  </div>            
		  
    
      </div>
        <!-- /#page-wrapper -->

    
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script src="../js/date.js"></script>
    
    
            <!-- jQuery Custom Scroller CDN -->
     <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>


    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
    	
    	
    	$("#includedSidebar").load("sidebar.jsp"); 
    	
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').fadeOut();
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        
        
        
        
        $('#dataTables-example-order').DataTable({
                responsive: true
        });
        $('#dataTables-example-cash-bill').DataTable({
           		responsive: true
  	    });
        $('#dataTables-example-abb-inv').DataTable({
       		responsive: true
	    });
        $('#dataTables-example-short-inv').DataTable({
       		responsive: true
	    });
       
        

        
        var current_date = new Date();
        var c_year =  current_date.getFullYear();
        var int_year = parseInt(c_year,10) + parseInt(543,10) - parseInt(2500,10);
        
        var part_one_credit_inv_no = document.getElementById('part_one_credit_inv_no');
        part_one_credit_inv_no.value = int_year+"/";
        	
        //SetShowCurrentData();
        setDailyNovatToTable();
        display_ct();
        display_total_value_bill_this_month();
        
        var navListItems = $('ul.setup-panel li a'),
            allWells = $('.setup-content');

        allWells.hide();

        navListItems.click(function(e)
        {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this).closest('li');
            
            if (!$item.hasClass('disabled')) {
                navListItems.closest('li').removeClass('active');
                $item.addClass('active');
                allWells.hide();
                $target.show();
            }
        });
        
        $('ul.setup-panel li.active a').trigger('click');
        
  
        
    });
    //////////////
    
    </script>

</body>

</html>
