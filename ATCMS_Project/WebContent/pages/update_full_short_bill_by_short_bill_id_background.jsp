<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "com.atcms.*" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "java.util.Properties" %>
<%@ page import = "javax.servlet.ServletContext" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.util.Date" %>



<% 

	System.out.println("Start update_full_short_bill_by_short_bill_id_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		/*
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		*/
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
	///////////////////file_path parameter/////////////
	
	String short_bill_id = request.getParameter("short_bill_id");
	int num_index = Integer.parseInt(request.getParameter("num_index"));

	String total_sum_cost  = request.getParameter("total_sum_cost");
	String total_sum  = request.getParameter("total_sum");
	String total_sum_profit  = request.getParameter("total_sum_profit");
	String total_vat  = request.getParameter("total_vat");
	String total_inc_vat  = request.getParameter("total_inc_vat");
	

	
	
	System.out.println("short_bill_id:"+short_bill_id);
	System.out.println("num_index:"+num_index);

	
	System.out.println("total_sum_cost:"+total_sum_cost);
	System.out.println("total_sum:"+total_sum);
	System.out.println("total_sum_profit:"+total_sum_profit);
	System.out.println("total_vat:"+total_vat);
	System.out.println("total_inc_vat:"+total_inc_vat);
	

//	List<Product> product_list = new ArrayList<Product>();

	ObjectMapper mapper = new ObjectMapper();
	try {		
					
					for(int i=0; i<num_index;i++)
					{
						
						String index = request.getParameter("index"+i);
						String pd_id = request.getParameter("pd_id"+i);
						String adt_description =  request.getParameter("pd_name"+i);
						String cost_per_unit = request.getParameter("pd_cost_per_unit"+i);
						String price = request.getParameter("pd_price"+i);
						String profit_per_unit = request.getParameter("pd_profit_per_unit"+i);
						String quantity =   request.getParameter("pd_quantity"+i);
						String unit =   request.getParameter("pd_unit"+i);
						String sum_cost =  request.getParameter("pd_sum_cost"+i);
						String sum = request.getParameter("pd_sum"+i);
						String sum_profit =  request.getParameter("pd_sum_profit"+i);
						String calculated_cost_date = request.getParameter("pd_calculated_cost_date"+i);
						
						char first_char = calculated_cost_date.charAt(0);
	
						    if(first_char=='2')
						    {
						    	calculated_cost_date = "'"+ calculated_cost_date +"'";
						    }else{
						    	calculated_cost_date = null;
						    }
						
						
						    System.out.println("Round:"+i+" , index :"+index);
						    System.out.println("pd_id:"+pd_id);
						    System.out.println("adt_description:"+adt_description);
						    System.out.println("cost_per_unit:"+cost_per_unit);
						    System.out.println("price:"+price);
						    System.out.println("quantity:"+quantity);
						    System.out.println("unit:"+unit);
						    System.out.println("sum_cost:"+sum_cost);
						    System.out.println("sum:"+sum);
						    System.out.println("sum_profit:"+sum_profit);
						    System.out.println("calculated_cost_date:"+calculated_cost_date);
						 
						    System.out.println();
						    
						    String sql_short_bill_detail = " UPDATE short_bill_detail  "+
						    									     "	SET short_bill_detail.adt_description = '"+adt_description+"' "+
						    												" , short_bill_detail.cost_per_unit = '"+cost_per_unit +"' "+
						    									     		" , short_bill_detail.price = '"+price + "' "+
						    												" , short_bill_detail.profit_per_unit = '"+profit_per_unit+"' "+
						    												" , short_bill_detail.quantity = '"+quantity +"' "+
						    									     		" , short_bill_detail.sum_cost = '"+sum_cost+ "' "+
						    												" , short_bill_detail.sum = '"+sum+ "' "+
						    									     		" , short_bill_detail.sum_profit = '"+sum_profit +"' "+
						    												" , short_bill_detail.calculate_cost_date = "+calculated_cost_date+" "+
						    										  " WHERE BINARY short_bill_detail.product_id ='"+pd_id+"' "+
						    									      " AND short_bill_detail.index = '"+index+"' ";
						
						    System.out.println("sql_short_bill_detail : "+sql_short_bill_detail);
						    
						    connect.createStatement().executeUpdate(sql_short_bill_detail);

					}
					
					String sql_short_bill_main = " UPDATE short_bill_main  "+	
									" SET short_bill_main.total_cost = '"+total_sum_cost +"' "+
								    " , short_bill_main.total_value = '"+total_sum+"' "+
									" , short_bill_main.total_profit = '"+total_sum_profit+"' "+
								    " , short_bill_main.total_vat = '"+total_vat+"' "+
									" , short_bill_main.total_inc_vat = '"+total_inc_vat+"' "+
							" WHERE BINARY short_bill_main.short_bill_id = '"+short_bill_id+"' ";
					System.out.println("sql_short_bill_main : "+sql_short_bill_main);
					
					connect.createStatement().executeUpdate(sql_short_bill_main);
			

	
		
	} catch (Exception e) {
		
		e.printStackTrace();
		connect.close();
		out.print("error");
	}
	 
	connect.close();
%>
