<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

 <script>
	
	 function formatMoney(number, places, symbol, thousand, decimal) {
			number = number || 0;
			places = !isNaN(places = Math.abs(places)) ? places : 0;
			symbol = symbol !== undefined ? symbol : "";
			thousand = thousand || ",";
			decimal = decimal || ".";
			var negative = number < 0 ? "-" : "",
			    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
			    j = (j = i.length) > 3 ? j % 3 : 0;
			return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
	}
	
	
	 
	 function fetch_quotation_main(){
		 
		 var quotation_id = sessionStorage.getItem("quotation_id_for_get_detail");
		// alert(quotation_id);
         
	      var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					
					var jsonObj = JSON.parse(xmlhttp.responseText);
					
					if(jsonObj.length == 0) {

						//alert("no  Data");
					}
					else{
					//	alert("get Data");
						 
					//	alert(xmlhttp.responseText);
						var cus_name = document.getElementById("cus_name");
						var cus_address = document.getElementById("cus_address");
						var cus_tax_id = document.getElementById("cus_tax_id"); 
						var quotation_date  = document.getElementById("quotation_date"); 
						var expire_date = document.getElementById("expire_date"); 
						var quotation_no = document.getElementById("quotation_no"); 
						var quotation_view_id = document.getElementById("quotation_view_id"); 
						var quot_file_name = document.getElementById('quot_file_name');
						var quot_file_path = document.getElementById('quot_file_path');
						var status_but = document.getElementById('status_but');

						
						
						var total_vat = document.getElementById('total_vat');
						var total_sum = document.getElementById('total_sum');
						var total_inc_vat = document.getElementById('total_inc_vat');
						var total_sum_cost =  document.getElementById('total_sum_cost');
						var total_sum_profit =  document.getElementById('total_sum_profit');
						
	
							
							
							cus_name.value = jsonObj.customerName;
							cus_address.value = jsonObj.customerAddress; 					
							cus_tax_id.value = jsonObj.taxId; 
							
							compute_credit.value =  jsonObj.credit;
							quotation_date.value =  jsonObj.quotationDate;
							expire_date.value =  jsonObj.expireDate;

							quotation_view_id.value = jsonObj.orderId;
							quot_file_name.value = jsonObj.invFileName;
							quot_file_path.value = jsonObj.invFilePath;

							total_vat.value = jsonObj.totalVat;
							total_sum.value = jsonObj.totalValue;
							total_inc_vat.value = jsonObj.totalIncVat;
							
							total_sum_cost.value = jsonObj.totalCost;
							total_sum_profit.value = jsonObj.totalProfit;		
							
							quotation_no.value = jsonObj.invNo;
							
							
						
						
					}

				}// end if check state
			}// end function
			

			xmlhttp.open("POST", "get_quotation_main_by_quotation_id_background.jsp?quotation_id="+quotation_id, true);
			xmlhttp.send();
	    	
		 
	 } 
	 
	 function  fetch_quotation_detail(){
		 
		 var quotation_id = sessionStorage.getItem("quotation_id_for_get_detail");
         
	      var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					
					var jsonObj = JSON.parse(xmlhttp.responseText);
					
					if(jsonObj.length == 0) {

						//alert("no  Data");
					}
					else{

						//alert("get  Quotation Detail Data");
							
							for(i in jsonObj) {

							
								  var newid = 0;
							        $.each($("#quotation-detail-dataTables-example tr"), function() {
							            if (parseInt($(this).data("id")) > newid) {
							                newid = parseInt($(this).data("id"));
							            }
							        });
							        newid++;
							        
							        var tr = $("<tr></tr>", {
							            id: "addr"+newid,
							            "data-id": newid
							        });
							        
							     
							        // loop through each td and create new elements with name of newid
							            $.each($("#quotation-detail-dataTables-example tbody tr:nth(0) td"), function() {
							                var cur_td = $(this);
							                
							                var children = cur_td.children();
							                
							                // add new td and element if it has a name
							                if ($(this).data("name") != undefined) {
							                    var td = $("<td></td>", {
							                        "data-name": $(cur_td).data("name")
							                    });
							                    
							                    var c = $(cur_td).find($(children[0]).prop('tagName')).clone().val("");
							                    c.attr("name", $(cur_td).data("name") + newid);			
							                    c.attr("id", $(cur_td).data("name") + newid);		
							                    c.appendTo($(td));
							                    td.appendTo($(tr));
							                  

							                } else {
							                    var td = $("<td></td>", {
							                        'text': $('#quotation-detail-dataTables-example tr').length
							                    }).appendTo($(tr));
							                }
							            });
							        	
							            
							            // add the new row
							            $(tr).appendTo($('#quotation-detail-dataTables-example'));
							            
							            var  pd_name = document.getElementById("name"+newid);
							            var  pd_cost_per_unit = document.getElementById("cost_per_unit"+newid);
							            var  pd_price = document.getElementById("price"+newid);
							            var  pd_profit_per_unit = document.getElementById("profit_per_unit"+newid);
							            var  pd_quantity = document.getElementById("quantity"+newid);
							            var  pd_unit = document.getElementById("unit"+newid);
							            var  pd_sum_cost = document.getElementById("sum_cost"+newid);
							            var  pd_sum = document.getElementById("sum"+newid);
							            var  pd_sum_profit = document.getElementById("sum_profit"+newid);
							            var  pd_product_id =  document.getElementById("product_id"+newid);
							            
							            
							   			//	alert(pd_name);
							           		 pd_name.value = jsonObj[i].adtDescription;
							           		 pd_cost_per_unit.value = jsonObj[i].costPerUnit;
							           		 pd_price.value = jsonObj[i].price;
							           		 pd_profit_per_unit.value = jsonObj[i].profitPerUnit;
							           	  	 pd_quantity.value = jsonObj[i].quantity;
							           	     pd_unit.value = jsonObj[i].productUnit;
							           	     pd_sum_cost.value = jsonObj[i].sumCost;
							           	     pd_sum.value = jsonObj[i].sum;
							           	 	 pd_sum_profit.value =  jsonObj[i].sumProfit;
							           	 	 pd_product_id.value = (parseInt(i)+1)+"_"+jsonObj[i].productId;
							           	 	 pd_product_id.name = jsonObj[i].index;
							        	
							}

						  	calculate_profit_percentage();
						  	calculate_summation_table();
						
						
						

					}

				}// end if check state
			}// end function
			

			xmlhttp.open("POST", "get_quotation_detail_by_quotation_id_background.jsp?quotation_id="+quotation_id, true);
			xmlhttp.send();
	 
	 }
	 
	 function calculate_cost(temp_id){
		 	 
		 var temp_id_parts = temp_id.split('_');
		 var prefix = temp_id_parts[0];
		 var product_id = temp_id_parts[1];
		 
		//alert(product_id.substring(0,3));
				if(product_id.substring(0,3)=="OTH")
				{
								   var xmlhttp;
									
									if(window.XMLHttpRequest) {
										// code for IE7+, Firefox, Chrome, Opera, Safari
										xmlhttp = new XMLHttpRequest();
									}
									else {
										// code for IE6, IE5
										xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
									}
									
									xmlhttp.onreadystatechange = function() {
										
										if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
											
											var jsonObj = JSON.parse(xmlhttp.responseText);
											
											if(jsonObj.length == 0) {
								
												alert("no  Data");
											}
											else{
												
												var result = "";
												var sum = 0;
												var divider = 0;
												var avg_cost = 0;
												var avg_cost_str = "";
												var final_avg_cost ="";
												for(i in jsonObj) {
													
												    result = result + "Date : "+jsonObj[i].invoiceDate+"\n"+
													     	          "Price "+jsonObj[i].price +" x "+ jsonObj[i].quantity +" = "+jsonObj[i].sum+"\n\n";
												    
												    sum = sum + parseFloat(jsonObj[i].sum);
												    divider = divider + parseFloat(jsonObj[i].quantity);
												}
												
												avg_cost = sum / divider ;
												
												avg_cost_str = avg_cost+"";
												
												if(avg_cost_str.indexOf(".")>0)
												{
													var avg_cost_str_parts = avg_cost_str.split('.');
													final_avg_cost = avg_cost_str_parts[0] +"."+ avg_cost_str_parts[1].substring(0,2);
												}else{
													
													final_avg_cost = avg_cost_str;
												}
												//alert("final_avg_cost:"+final_avg_cost);
												
												var cost_per_unit = document.getElementById('cost_per_unit'+prefix);
													cost_per_unit.value = final_avg_cost;
													
													var today = new Date();
													var dd = today.getDate();
													var mm = today.getMonth()+1; //January is 0!
								
													var yyyy = today.getFullYear();
													if(dd<10){
													    dd='0'+dd
													} 
													if(mm<10){
													    mm='0'+mm
													} 
													var today = yyyy+'-'+mm+'-'+dd;
													
													cost_per_unit.name = today;
												
												result = result + "Sum : "+sum +" , "+"Quantity : "+divider+"\n"+
														 "Avg Cost : "+final_avg_cost + " Baht";
												
												
												calculate_each_row("xxxxxxxxxxxxx"+prefix);
											  	calculate_profit_percentage();
												
												alert(result);
								
											}
								
										}// end if check state
									}// end function
									
								
									xmlhttp.open("POST", "calculate_oth_product_cost_by_product_id_background.jsp?product_id="+product_id, true);
									xmlhttp.send();
				}
				else{
					
					 var xmlhttp;
						
						if(window.XMLHttpRequest) {
							// code for IE7+, Firefox, Chrome, Opera, Safari
							xmlhttp = new XMLHttpRequest();
						}
						else {
							// code for IE6, IE5
							xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
						}
						
						xmlhttp.onreadystatechange = function() {
							
							if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
								
								alert(xmlhttp.responseText);
								
								//var jsonObj = JSON.parse(xmlhttp.responseText);
								
	
					
							}// end if check state
						}// end function
						
					
						xmlhttp.open("POST", "calculate_atc_product_cost_by_product_id_background.jsp?product_id="+product_id, true);
						xmlhttp.send();

				}
	 
	 }
	 
	 function calculate_each_row (id){
		  
		 var row_num = "";
		 
		 if((id.length ==14) || (id.length == 15)) {
			 
			 row_num =	 id.substring(13);
			 
		}else if((id.length == 6) || (id.length == 7) ) {
			
			row_num =  id.substring(5);
			 
		}else if((id.length == 9) || (id.length ==10) ){
			
			row_num =  id.substring(8);
			 
		}
	
		//alert(row_num);
		 	
		
		 
		 var  cost_per_unit = document.getElementById("cost_per_unit"+row_num);
		 var  price = document.getElementById("price"+row_num);
		 var  profit_per_unit = document.getElementById("profit_per_unit"+row_num);
		 var  quantity = document.getElementById("quantity"+row_num);
		 var  sum_cost = document.getElementById("sum_cost"+row_num);
		 var  sum = document.getElementById("sum"+row_num);
		 var  sum_profit = document.getElementById("sum_profit"+row_num);
		 		 		 
		 var profit_per_unit_value = parseFloat(price.value , 10) - parseFloat(cost_per_unit.value , 10);
		 	 
			    profit_per_unit.value =  parseFloat(profit_per_unit_value , 10).toFixed(2) ;
		
		var sum_cost_value =  parseFloat(cost_per_unit.value , 10) * parseFloat(quantity.value , 10) ; 
		
			   sum_cost.value = parseFloat(sum_cost_value , 10).toFixed(2);
			   
		var sum_profit_value =  parseFloat(profit_per_unit_value , 10)  *   parseFloat(quantity.value , 10) ; 
			
			  sum_profit.value = parseFloat(sum_profit_value , 10).toFixed(2);
			  
			  
			  calculate_profit_percentage();
			  calculate_summation_table();

		
	 }
	 
	 function calculate_summation_table(){
		 
	//	alert("Calulate Summation");
			 
		 var num_row =   $('#quotation-detail-dataTables-example tr').length -2 ;
		 
	//	 alert(num_row);
		 
		 var total_sum_value  = 0;
		 var total_sum_cost_value = 0;
		 var total_sum_profit_value = 0;
		 
		 
		 for (var  i=1; i<=num_row; i++)
		{
			 
			 var  sum_cost = document.getElementById("sum_cost"+i);
			 var  sum = document.getElementById("sum"+i);
			 var  sum_profit = document.getElementById("sum_profit"+i);
			 	
			// 	alert(sum_cost.value+" , "+sum.value+","+sum_profit.value);
			 
			    	 total_sum_cost_value = parseFloat(total_sum_cost_value , 10)  + parseFloat(sum_cost.value , 10 );
			    	 total_sum_value = parseFloat(total_sum_value , 10) +  parseFloat(sum.value , 10);
			    	 total_sum_profit_value = parseFloat(total_sum_profit_value , 10) + parseFloat(sum_profit.value , 10);
			 
		}
		 
		 var  total_sum_cost = document.getElementById("total_sum_cost");
		 var  total_sum = document.getElementById("total_sum");
		 var  total_sum_profit = document.getElementById("total_sum_profit");
		 var  total_vat = document.getElementById("total_vat");
		 var  total_inc_vat = document.getElementById("total_inc_vat");
		 
			//	 total_sum_cost.value = (total_sum_cost_value);
			//	 total_sum.value = (total_sum_value);
			//	 total_sum_profit.value = (total_sum_profit_value);
				 
		var total_vat_string = String((total_sum_value * 0.07) * 10 / 10);
		var total_sum_string = String((total_sum_value * 1.07) * 10 / 10);
		var total_sum_cost_string = String(total_sum_cost_value);
		
		var final_total_vat;
		var final_total_sum;		 
		var final_total_sum_cost;
 
		if(total_vat_string.indexOf(".")>0)
		{
			var total_vat_string_parts = total_vat_string.split('.');
			final_total_vat = total_vat_string_parts[0] +"."+ total_vat_string_parts[1].substring(0,2);
		}else{
			final_total_vat = total_vat_string;
		}
	
		
		if(total_sum_string.indexOf(".")>0)
		{
			var total_sum_string_parts = total_sum_string.split('.');
			final_total_sum = total_sum_string_parts[0] +"."+ total_sum_string_parts[1].substring(0,2);
		}
		else{
			final_total_sum = total_sum_string;
		}
		
		if(total_sum_cost_string.indexOf(".")>0)
		{
			var total_sum_cost_string_parts = total_sum_cost_string.split('.');
			final_total_sum_cost = total_sum_cost_string_parts[0] +"."+ total_sum_cost_string_parts[1].substring(0,2);
		}
		else{
			final_total_sum_cost = total_sum_cost_string;
		}
		
		
		total_vat.value =  parseFloat(final_total_vat);
		total_inc_vat.value = parseFloat(final_total_sum);
		total_sum_cost.value =  parseFloat(final_total_sum_cost);
		total_sum_profit.value = (parseFloat(total_sum.value) - parseFloat(final_total_sum_cost)).toFixed(2) +"";
		
		
		
		
		/////////////////////////////////////////////// Calculate Profit Percentage /////////////////////////////
		
		var  avg_profit_percent = document.getElementById("avg_profit_percent");
		
		var temp_answer = 0;
		
		 temp_answer =  (parseFloat(total_sum.value  , 10) - parseFloat(total_sum_cost.value  , 10));
		 temp_answer = (temp_answer / parseFloat(total_sum.value  , 10) ) * 100 ;
 
		 
		 avg_profit_percent.value = temp_answer.toFixed(2) +" %";
		 
		 
	 }
	 
	 
	 function numberWithCommas(x) {
		 
		    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		    
		}
	 
	 
	 
	 
	 function calculate_profit_percentage(){
		 
		 console.log("Start");
		 
		 var temp_row =   $('#quotation-detail-dataTables-example tr').length -2 ;
		 
		 var temp_answer = 0 ;
		 
		 console.log("temp_answer:"+temp_answer);
		 console.log("temp_row:"+temp_row);
		 
		 for (var  temp=1 ;  temp<=temp_row ;  temp++)
		{
			 console.log("start loop :"+i);
				 
				 var  cost_per_unit = document.getElementById("cost_per_unit"+temp);
				 var  price = document.getElementById("price"+temp);
				 var profit_per_unit = document.getElementById("profit_per_unit"+temp);
				 var  percent_profit = document.getElementById("percent_profit"+temp);
				 	
						 temp_answer =  (parseFloat(price.value  , 10) - parseFloat(cost_per_unit.value  , 10));
						 temp_answer = (temp_answer / parseFloat(price.value  , 10) ) * 100 ;
				 
						 
						 percent_profit.value = temp_answer.toFixed(2) +" %";
						 
						 temp_answer = 0;
				
			}

		 
	 }
	 
	 function GenerateInvoice(){
 		
		  var order_id = document.getElementById('quotation_view_id').value;
		  var quot_file_name = document.getElementById('quot_file_name');
		  var quot_file_path = document.getElementById('quot_file_path');
			
		 // alert("gen inv ");
		//  $('ul.setup-panel li:eq(1)').removeClass('disabled');
		//  $('ul.setup-panel li a[href="#step-2"]').trigger('click');
		//  button.remove();
		  
		  var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
					if(xmlhttp.responseText=="fail")
					{
						alert("Error Occer to Generate inv , pls contact Admin");
					}else{
						
						//alert(xmlhttp.responseText);
						var values = xmlhttp.responseText.split("&");
						var file_name = values[0];
						var file_path = values[1];
						
						
						alert("Your file "+file_name+" are ready.");
					
						quot_file_name.value = file_name;
						quot_file_path.value = file_path;
					}
					
					
						
				}// end if check state
			}// end function
			
			alert("going to send POST");
			xmlhttp.open("POST", "generate_credit_inv_background.jsp?order_id="+order_id, true);
			xmlhttp.send();

		  
	}
	function show_modal_change_status(temp){

		$('#status_detail_modal').modal('show');
	
	}
	
	function change_status(temp)
	{
	
		var modal_status_but = document.getElementById('modal_status_but');	
			modal_status_but.innerHTML = temp.innerHTML+"<span class='caret'></span>";
			modal_status_but.name = temp.id;
		
	}
	function submit_change_status(){
		
		var modal_note = document.getElementById('modal_note');
		var complete_date = document.getElementById('complete_date');
		var payment_ref = document.getElementById('payment_ref');
		var status_but = document.getElementById('status_but');
		var order_id = document.getElementById('quotation_view_id');		
		var	modal_status_but = document.getElementById('modal_status_but');	
		var parameter ; 

	//	alert("status:"+modal_status_but.name+" , "+"modal_note:"+modal_note.value+" , complete_date:"+complete_date.value+" , payment_ref:"+payment_ref.value);
			parameter = "order_id="+order_id.value+
						"&complete_date="+complete_date.value+
						"&status="+modal_status_but.name+
						"&note="+modal_note.value+
						"&payment_ref="+payment_ref.value;
	
		var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
					if(xmlhttp.responseText=="fail")
					{
						alert("Change Status error");
					}else{
						
						alert("Success");
						location.reload();
					}
					
				}// end if check state
			}// end function
			
		
			xmlhttp.open("POST", "change_order_status_detail_background.jsp?"+parameter, true);
			xmlhttp.send();

	}
	function save_order_updated(){
		
		var pd_array = new Array();
		var parameter_order_main = "";
		var parameter_order_detail ="";
		
		

		$('#quotation-detail-dataTables-example > tbody  > tr').each(function() {

			if(this.id=="addr0")
			{
				
			}else{
				
				var id ;
				if(this.id.length==5)
				{
					id = this.id.slice(-1);
				}else{
					id = this.id.slice(-2);
				}
				//alert("id:"+id);
				
				pd_array.push(id);		
			}

			
		});
		
		var index =  parseInt(pd_array.length, 10);
		
		
		
		
		var order_id = document.getElementById('quotation_view_id');
		var po_no  = document.getElementById('po_no');
		var  total_sum_cost = document.getElementById("total_sum_cost");
		var  total_sum = document.getElementById("total_sum");
		var  total_sum_profit = document.getElementById("total_sum_profit");
		var  total_vat = document.getElementById("total_vat");
		var  total_inc_vat = document.getElementById("total_inc_vat");
		var avg_profit_percent = document.getElementById("avg_profit_percent");
		
		var avg_profit_array = (avg_profit_percent.value).split(' ');
		var temp_avg_profit_percent = avg_profit_array[0];
		
		var order_id_value = order_id.value ; 
		
		
		parameter_order_main = "order_id="+order_id_value+
							   "&num_index="+index+
							   "&po_no="+po_no.value+
							   "&total_sum_cost="+total_sum_cost.value+
							   "&total_sum="+total_sum.value+
							   "&total_sum_profit="+total_sum_profit.value+
							   "&total_vat="+total_vat.value+
							   "&total_inc_vat="+total_inc_vat.value+
							   "&avg_profit_percent="+temp_avg_profit_percent;
		console.log(parameter_order_main);
		
	//	alert(parameter_order_main);
		
		for(var j=0;j<index ;j++)
		{
				var pd_product_id = document.getElementById('product_id'+pd_array[j]);
				var pd_product_id_value = pd_product_id.value ; 
				var index_value = pd_product_id.name;
				
						 var temp_id_parts = pd_product_id_value.split('_');
						 var final_product_id = temp_id_parts[1];

				var pd_name = document.getElementById('name'+pd_array[j]);
				var pd_name_value = pd_name.value ; 
				
				var pd_cost_per_unit = document.getElementById('cost_per_unit'+pd_array[j]);
				var pd_cost_per_unit_value = pd_cost_per_unit.value ; 
				
					  var pd_calculated_cost_date_value  = pd_cost_per_unit.name ;
				
				var pd_price = document.getElementById('price'+pd_array[j]);
				var pd_price_value = pd_price.value ; 
				
				var pd_profit_per_unit = document.getElementById('profit_per_unit'+pd_array[j]);
				var pd_profit_per_unit_value = pd_profit_per_unit.value ; 
				
				var pd_quantity = document.getElementById('quantity'+pd_array[j]);
				var pd_quantity_value = pd_quantity.value ; 
				
				var pd_unit = document.getElementById('unit'+pd_array[j]);
				var pd_unit_value = pd_unit.value ; 
				
				var pd_sum_cost = document.getElementById('sum_cost'+pd_array[j]);
				var pd_sum_cost_value = pd_sum_cost.value ; 
				
				var pd_sum = document.getElementById('sum'+pd_array[j]);
				var pd_sum_value = pd_sum.value ; 
				
				var pd_sum_profit = document.getElementById('sum_profit'+pd_array[j]);
				var pd_sum_profit_value = pd_sum_profit.value ; 
				
				var pd_percent_profit = document.getElementById('percent_profit'+pd_array[j]);
				var pd_percent_profit_value = pd_percent_profit.value ; 
				if(pd_percent_profit_value=='NaN')
				{
					pd_percent_profit_value = '0';
				}
				
				
				
				var pd_percent_profit_array = pd_percent_profit_value.split(' ');
				var temp_pd_percent_profit = pd_percent_profit_array[0];
				

				parameter_order_detail = parameter_order_detail +
										 "&pd_id"+j+"="+final_product_id+
										 "&pd_name"+j+"="+pd_name_value+
										 "&pd_cost_per_unit"+j+"="+pd_cost_per_unit_value+
										 "&pd_price"+j+"="+pd_price_value+
										 "&pd_profit_per_unit"+j+"="+pd_profit_per_unit_value+
										 "&pd_percent_profit"+j+"="+temp_pd_percent_profit+
										 "&pd_quantity"+j+"="+pd_quantity_value+
										 "&pd_unit"+j+"="+pd_unit_value+
										 "&pd_sum_cost"+j+"="+pd_sum_cost_value+
										 "&pd_sum"+j+"="+pd_sum_value+
										 "&pd_sum_profit"+j+"="+pd_sum_profit_value+
										 "&index"+j+"="+index_value+
										 "&pd_calculated_cost_date"+j+"="+pd_calculated_cost_date_value;
				 
										 
		}
		
			alert(parameter_order_detail);
		
		var xmlhttp;
		
		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange = function() {
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
				if(xmlhttp.responseText=="fail")
				{
					alert("Update Error");
				}else{
					
					alert("Success");
					
					
					//location.reload();
				}
				
			}// end if check state
		}// end function
		
	
		xmlhttp.open("POST", "update_full_order_by_order_id_background.jsp?"+parameter_order_main+parameter_order_detail, true);
		xmlhttp.send();

		
	}
	
	
</script>
<style>
	.text-right {
 		 text-align:right;
	}
	hr {
	  -moz-border-bottom-colors: none;
	  -moz-border-image: none;
	  -moz-border-left-colors: none;
	  -moz-border-right-colors: none;
	  -moz-border-top-colors: none;
	  border-color: #DCDCDC;
	  border-style: solid none;
	  border-width: 1px 0;
	  margin: 18px 0;
	}

</style>
    

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
      		   <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.jsp">ATCMS Admin</a>
            </div>
            <!-- /.navbar-header -->

   
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="dashboard_main.jsp"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-group fa-fw"></i>Customer&Vendor<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="customer_sub.jsp">Customers</a>
                                </li>
                                <li>
                                    <a href="vendor_sub.jsp">Vendors</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                         <li>
                            <a href="#"><i class="fa fa-leaf"></i>  Product<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="atc_product_sub.jsp">ATC Product</a>
                                </li>
                                <li>
                                    <a href="other_product_sub.jsp">Other Product</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o"></i>  Bill Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="direct_bill_sub.jsp">Direct Bill</a>
                                </li>
                                <li>
                                    <a href="indirect_bill_sub.jsp">Indirect Bill</a>
                                </li>
                                <li>
                                    <a href="credit_inv_sub.jsp">Credit Invoice</a>
                                </li>
                                <li>
                                    <a href="cash_inv_sub.jsp">Cash Invoice</a>
                                </li>
                                <li>
                                    <a href="credit_note_sub.jsp">Credit Note (ใบลดหนี้)</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                        	<a href="#"><i class="glyphicon glyphicon-fire"></i> Production (การผลิต)<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">                         
                               <li>
                                    <a href="production_material_main.jsp">Material</a>
                                </li>
                                <li>
                                    <a href="production_work_order_main.jsp">Work Order</a>
                                </li>
                                <li>
                                    <a href="production_formula_main.jsp">Formula</a>
                                </li>
                                 <li>
                                    <a href="production_product_relate_formula.jsp">Product Relate Formula</a>
                                </li>                
                            </ul>
                        </li>
                        <li>
                        	<a href="#"><i class="glyphicon glyphicon-stats"></i> Reporting (รายงาน)<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="monthly_summary_report_main.jsp">Monthly Summary Report</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                        	<a href="#"><i class="glyphicon glyphicon-road"></i> (DEVEL)0pinG Z()Ne <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="dev_show_profit_each_bill_detail.jsp">Each  Bill Value Detail</a>
                                </li>
                                <li>
                                    <a href="dev_balancing_work_order.jsp">Balancing Work Order</a>
                                </li>
                
                            </ul>
                        </li>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
             <div class="row">
             <br>
	           <div class="col-lg-4">
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                          Credit Invoice
	                        </div>
	                        
	                        <!-- /.panel-heading -->
	                        <div class="panel-body">
	                        		                       	
	                        	<div class="dropdown">
								  <button class="btn btn-primary dropdown-toggle btn-block" id="status_but" onclick="show_modal_change_status(this)" type="button" data-toggle="dropdown"><span class="caret"></span></button>
								</div>
								<br>
								   	<label>Quotation No.</label>
								<input name="quotation_no" id="quotation_no" type="text" class="form-control" placeholder="INV No" readonly>
	                        	<br>
	                        	<input name="quotation_view_id" id="quotation_view_id" type="text" class="form-control" placeholder="Quotation ID" readonly>
                                <br>	
	                        	<label>Customer Name (ชื่อลูกค้า)</label>
	                           		 <input id="cus_name" name = "cus_name" class="form-control" value="" disabled  >
	                           		 <input id ="company_id_hidden" name="company_id_hidden" type="hidden" value="" >
	                           		 <br>
	                        	<label>Address (ที่อยู่)</label>
	                        		 <textarea id="cus_address" name ="cus_address" class="form-control" rows="3" disabled></textarea>
	                        	 	 <br>
	                        	<label>Tax ID (เลขประจำตัวผู้เสียภาษี)</label>
	                        		<input class="form-control" id="cus_tax_id" name = "cus_tax_id" value="" disabled>
	                        		<br>
	                      
	                      			<table>

	                        	   			<tr class="pagination-centered">
	                      			 		
	                      			 			<td style="padding-left:2em;">
	                        						<label>Computed Credit   </label>	                        						
	                        					</td>
	                        					<td style="padding-left:1em;">
	                        					 	<input type="number" id="compute_credit" name ="compute_credit" class="form-control" readonly>  					                        					
	                        						<br>
	                        					</td>	

	                        	   			</tr>
                   	   			
	                        	   			<tr class="pagination-centered">
	                      			 			<td style="padding-left:2em;">
	                        						<label>Quotation Date : </label>	                        						
	                        					</td>
	                        					<td style="padding-left:1em;">
	                        					 	<input type="date" id="quotation_date" name ="quotation_date" class="form-control" readonly>  					                        					
	                        						<br>
	                        					</td>	

	                        	   			</tr>
	                        	   			<tr class="pagination-centered">
	                        	   				 <td style="padding-left:2em;">
	                        						<label>Expire Date   </label>	                        						
	                        					</td>
	                        					<td style="padding-left:1em;">
	                        					 	<input type="date" id="expire_date" name ="expire_date" class="form-control" readonly>  
	                        					 	<br>					                        					
	                        					</td>	                  	   				
	                        	   			</tr>
	      

	                        		</table>
	                        		<hr>
	                        		<br>

	                        		<div class="row">
	                        			<div class="col-md-2 col-md-offset-4">
		                        			<button id="generate_quotation" class="btn btn-primary btn-md" onclick="GenerateQuotation()">Generate Quotation</button>
		                        			<br>
		                        			<br>
		                        		</div>
		                        	</div>
		                        	<div class="row">
		                        		<div class="col-md-6" style="float: none;margin: 0 auto;">
	                        			
                        							<form  method="get" action="../DownloadServlet" >
																    <div class="input-group">
																		      <input id="quot_file_name" name="quot_file_name" type="text" class="form-control" readonly>
																		      <input id="quot_file_path" name="quot_file_path" type="hidden">
																		      <span class="input-group-btn">
																		        <input type="submit" class="btn btn-primary" value="Download">
																		      </span>
																	    
																    </div><!-- /input-group -->
												   </form>
										</div>
	                        		</div>
	                        		
	                        		<div>
	                        			<hr>
	                        			<button type="button" onclick="show_confirm_delete(this.id)"  class="form-control btn btn-danger">Delete Invoice</button>
	                        		</div>
	                        	
	                     
													
													  
												
											
	                        		
	                        		
	                        		<br>
	                            	
												
													                            
	                        </div>
	                        <!-- .panel-body -->
	                    </div>
                    <!-- /.panel -->
                </div>
                
                
                  <div class="col-lg-8">
                  
                  			 <div class="panel-body">
                  			 
                  			 	 <div class="alert alert-success fade in alert-dismissable" id="alert_saved_time" style="display: none">
	                                <button id="alert_saved_time_but" type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                                Save Change on : 14.10.2016 ,8.37 AM
                            	</div>
                  			 		
                  			 </div>
                  			
                  
	           
	                        <table class="table table-striped table-bordered table-hover" id="quotation-detail-dataTables-example">
									<thead>
										<tr>
											<th class="text-center" style="width: 200px;">
												Product Name
											</th>
											<th class="text-center" style="width: 90px;">
												Cost/Unit
											</th>
											<th class="text-center" style="width: 90px;">
												Price/Unit
											</th>
											<th class="text-center" style="width: 90px;">
												Profit/Unit
											</th>
											<th class="text-center" style="width: 90px;">
												Profit(%)
											</th>
											
					    					<th class="text-center" style="width: 50px;">
												Quantity
											</th>
											<th class="text-center" style="width: 90px;">
												Unit
											</th>
											<th class="text-center" style="width: 100px;">
												Sum Cost
											</th>
											<th class="text-center" style="width: 100px;">
												Sum
											</th>
										    <th class="text-center" style="width: 100px;">
												Sum Profit
											</th>
											
					        				<th class="text-center" style="border-top: 1px solid #ffffff; border-right: 1px solid #ffffff; width: 20px;">
											</th>
											
										</tr>
									</thead>
									<tbody id = "pd_tbody">
									
					    				<tr id='addr0' data-id="0" class="hidden">
											<td data-name="name" style=" width : 200px;">
											    <input type="text" name='pd_name' id = "pd_name" class="form-control" />
											</td>
											<td data-name="cost_per_unit" style="text-align:center; width : 100px;" >
											    <input type="text"   id = "pd_cost_per_unit" class="form-control" style="text-align:right;" onchange="calculate_each_row(this.id)"    />
											</td>
											<td data-name="price" style=" width : 100px;">
											    <input type ="text" name = 'pd_price' id="pd_price" class="form-control text-right"   onchange="calculate_each_row(this.id)"  />
											</td>
											<td data-name="profit_per_unit" style="text-align:center; width : 80px; ">
											    <input type="text" name='pd_profit_per_unit' id = "pd_profit_per_unit" class="form-control" style="text-align:right;"    readonly  />											    
											</td>		
											<td data-name="percent_profit" style="text-align:center; width : 50px; ">
											    <input type="text" name='pd_percent_profit' id = "pd_percent_profit" class="form-control" style="text-align:center;"    readonly  />											    
											</td>											
					    					<td data-name="quantity" style="text-align:center; width : 100px;" >
											    <input type ="number" name = 'pd_quantity' id="pd_quantity" class="form-control text-right" onchange="calculate_each_row(this.id)"  />
											</td>
											<td data-name="unit" style="text-align:center; width : 50px;" >
												 <input  type="text" name='pd_unit' id = "pd_unit" class="form-control text-center" />
												 
											</td>
											<td data-name="sum_cost" style="width : 100px;">
												 <input  type="text" name='pd_sum_cost' id = "pd_sum_cost" class="form-control text-right"  readonly />
												 
											</td>
											<td data-name="sum" style="width : 100px;">
												 <input  type="text" name='pd_sum' id = "pd_sum" class="form-control text-right"  readonly />
												 
											</td>
											<td data-name="sum_profit" style=" width : 100px;">
												 <input  type="text" name='pd_sum_profit' id = "pd_sum_profit" class="form-control text-right"   readonly />
												 
											</td>
											<td data-name="product_id" style=" width : 100px;">
											    <button onclick="calculate_cost(this.value)"  class="btn btn-warning btn-circle btn-md" >
											   		 <i class="glyphicon glyphicon-transfer"></i>
											    </button>
												<input type="hidden" name="index" id="pd_product_id">
												
											</td>
											 
										
					             
										</tr>
									</tbody>
								</table>	
								
								<table class="table table-bordered table-hover table-sortable " id="table_summary" style="border:none;">
								
								
														 <tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Total Sum Cost : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input readonly type="text" name="total_sum_cost" id="total_sum_cost" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>
														  <tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Total Sum  : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input readonly type="text" name="total_sum" id="total_sum" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>
															 <tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Avg Profit Percentage  : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input readonly type="text" name="avg_profit_percent" id="avg_profit_percent" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="%">
																 	 </div>
																</td>
															</tr>
															
															 <tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Total Sum Profit  : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input readonly type="text" name="total_sum_profit" id="total_sum_profit" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>

														 <tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Vat (7%) : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input readonly type="text" name="total_vat" id="total_vat" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>
												
															<tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Total Inc Vat : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input readonly type="text" name="total_inc_vat" id="total_inc_vat" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>
															
									
											</table>
								
								<div class="col-lg-2">
									
								</div>
								<div class="col-lg-2">  
									
								</div>
								<div class="col-lg-2">
									
								</div>
								<div class="col-lg-2">
									
								</div>
								<div class="col-lg-2">
									
								</div>
								<div class="col-lg-2">
									<br>
									<button type="button" class="form-control btn btn-primary" onclick="save_order_updated()">Save</button>
								</div>
	                        
	                        
	                       
	                        <!-- .panel-body -->
	               
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
             </div>
      

        
        </div>
    <div class="modal fade" id="status_detail_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style=" position: fixed; top: 10%; left: 10%; right: 10%; bottom: 15% ">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Change Status</h4>
                                        </div>
                                        <div class="modal-body">
                                       		<div class="row">
                                       				 <div class="panel-body">
                                       				 	    <div class="dropdown">
																  <button class="btn btn-primary dropdown-toggle btn-block" id="modal_status_but" name="" type="button" data-toggle="dropdown"><span class="caret"></span></button>
																   <ul id="modal_status_ul" class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1" style="left: 50% !important;
																																			    right: auto !important;
																																			    text-align: center !important;
																																			    transform: translate(-50%, 0) !important;">
																        <li><a href="#" onclick="change_status(this)" id="pending">Pending</a></li>
																        <li><a href="#" onclick="change_status(this)" id="during_delivery">During Delivery</a></li>
																        <li><a href="#" onclick="change_status(this)" id="delivered">Delivered</a></li>
																        <li><a href="#" onclick="change_status(this)" id="billing">Billing</a></li>
																        <li><a href="#" onclick="change_status(this)" id="completed">Completed</a></li>
																        
																  </ul>
															</div>
															<br>
														   <div class="form-group">
																  <label for="comment">Note :</label>
																  <textarea class="form-control" rows="3" id="modal_note"></textarea>
													  	  </div>
													  	  <div>
													  		  <div>
																		  	
				                        						  <label>Complete Date :</label>	                        						
				                        					      <input type="date" id="complete_date" name ="complete_date" class="form-control" >  
				                        					      <br>		
				                        					      <label>Payment Reference :</label>	                        						
				                        					      <input type="text" id="payment_ref" name ="payment_ref" class="form-control" >  
				                        					      <br>						                        					
											  				  </div>
					                                       					
                                       					</div>
                                       		</div>
                                       		                       
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" name="submit_change_status_but"  id="submit_change_status_but" class="btn btn-primary" onclick="submit_change_status()">Submit Change</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
     </div>
        <!-- /#page-wrapper -->

    </div>
     <div class="modal fade" id="confirm-delete-order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			    <div class="modal-dialog">
			        <div class="modal-content">
			        	<form>
					          <div class="modal-body">
								    <textarea id="reason_delete" class="form-control" rows="2" placeholder="Reason for deleting this order"></textarea>
								    
								    <input type="hidden" id="delete_order_id">
							  </div>
							  <div class="modal-footer">
								    <button type="button" data-dismiss="modal" class="btn btn-danger" id="but_delete_order" onclick="DeleteOrder()">Delete</button>
								    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
							  </div>
						 </form>
			        </div>
			    </div>
		  </div>            
		  
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    
    
    <script>
	function show_confirm_delete(){
		$('#confirm-delete-order').modal('show');
		
		//  document.getElementById("delete_order_id").value = id;

	}
	function DeleteOrder()
	{
		
		var quotation_id = document.getElementById("quotation_view_id").value;
		var reason = document.getElementById("reason_delete").value;
		var inv_prefix = quotation_id.charAt(0)+quotation_id.charAt(1);
		if(inv_prefix=="CB")
		{
			var cash_bill_id = order_id;
			var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
						alert(xmlhttp.responseText);
						if(xmlhttp.responseText=="success")
						{
						window.open("credit_inv_sub.jsp");
							
						}else{
							
						}

				}// end if check state
			}// end function
			

			xmlhttp.open("POST", "delete_single_cash_bill_background.jsp?cash_bill_id="+cash_bill_id+"&reason="+reason, true);
			xmlhttp.send();
			
			
			
		}else{
    		
    		var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
							alert(xmlhttp.responseText);
							if(xmlhttp.responseText=="success")
							{
								window.open("credit_inv_sub.jsp");
								
							}else{
								
							}

					}// end if check state
				}// end function
				

				xmlhttp.open("POST", "delete_single_order_background.jsp?order_id="+order_id+"&reason="+reason, true);
				xmlhttp.send();
		}

		
	}
	
	
	
    $(document).ready(function() {
	
    	
    	fetch_quotation_main();
    	fetch_quotation_detail();
    	

    	
    });
    </script>

</body>

</html>
