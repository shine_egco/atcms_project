<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="../bower_components/sidebar/sidebar_style_3.css" rel="stylesheet" type="text/css"> 

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

 <script>
	 function set_credit_note_to_table(){
		/*
		  var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
					//alert("I'm Back");//////////////////
					
			
					var jsonObj = JSON.parse(xmlhttp.responseText);
				
					//	alert("I'm Back");
					
					if(jsonObj.length == 0) {
				
						alert("Error Occer Can't get all customer list");
					}
					else{
					  
						for(i in jsonObj) {
							$('#dataTables-example-product').DataTable().row.add([
	                           '<tr><td>'+jsonObj[i].invNo+'</td>'
	                           ,'<td>'+jsonObj[i].vendorName+'</td>'
	                           ,'<td class="text-right">'+jsonObj[i].totalIncVat+'</td>'
	                           ,'<td><button id = "'+jsonObj[i].purchaseID+'" type="button" class="btn gray-light btn-circle btn-md" onclick = "edit_data(this.id)" data-toggle="modal" data-target="#myModal">'
	                           +'<i class="glyphicon glyphicon-wrench"></i></button> </td></tr>'	                               
	                         ]).draw();
						}
						
					}
					
					
				
				
				
				}// end if check state
			}// end function
			
	
			xmlhttp.open("POST", "get_all_direct_bill_background.jsp", true);
			xmlhttp.send();
	*/
	}
	 function formatMoney(number, places, symbol, thousand, decimal) {
			number = number || 0;
			places = !isNaN(places = Math.abs(places)) ? places : 0;
			symbol = symbol !== undefined ? symbol : "";
			thousand = thousand || ",";
			decimal = decimal || ".";
			var negative = number < 0 ? "-" : "",
			    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
			    j = (j = i.length) > 3 ? j % 3 : 0;
			return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
	}

	function init_monthly_total_direct_bill()
	{
		/*
		var xmlhttp;
		
		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange = function() {
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
				
				
				var temp = xmlhttp.responseText;
			
				//alert(formatMoney(100000));
				
				document.getElementById("total_monthly_value").innerHTML = formatMoney(xmlhttp.responseText)+" THB";
				var d = new Date();
				var month = new Array();
				month[0] = "January";
				month[1] = "February";
				month[2] = "March";
				month[3] = "April";
				month[4] = "May";
				month[5] = "June";
				month[6] = "July";
				month[7] = "August";
				month[8] = "September";
				month[9] = "October";
				month[10] = "November";
				month[11] = "December";
				var n = month[d.getMonth()];
				
				document.getElementById("current_month").innerHTML = n + " "+d.getFullYear(); 
				
			
			}// end if check state
		}// end function
		

		xmlhttp.open("POST","get_monthly_total_direct_bill_background.jsp", true);
		xmlhttp.send();
		*/
	}
	function edit_data(product_id)
	{
	
			
	}
	function submit_change_product()
	{

	}
	function change_search_by_date(){
		
		  document.getElementById('keyword').type = 'date';
	//	alert("active");
	}
	function change_search_by_keyword(){
		
		  document.getElementById('keyword').type = 'text';
	//	alert("active");
	}
	
	function search_credit_inv(){
		
		 var  table = $('#dataTables-example-inv').DataTable();
	       table.clear();
		
		
		var type = document.getElementById('keyword').type ;
		var keyword =  document.getElementById('keyword').value;
		
		var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
					//alert("I'm Back");//////////////////
					
			
					var jsonObj = JSON.parse(xmlhttp.responseText);
				
					//	alert("I'm Back");
					
					if(jsonObj.length == 0) {
				
						alert("Keyword not found.");
					}
					else{
						alert(jsonObj.length);
						for(i in jsonObj) {
							$('#dataTables-example-inv').DataTable().row.add([
                                '<tr><td>'+jsonObj[i].invNo+'</td>'
                                ,'<td>'+jsonObj[i].customerName+'</td>'
                                ,'<td>'+jsonObj[i].totalIncVat+'</td>'
                                ,'<td>'+jsonObj[i].status+'</td>'
                                ,'<button id="'+jsonObj[i].orderId+'" onclick="show_order_detail(this.id)" class="btn btn-info btn-circle btn-md"><i class="glyphicon glyphicon-search"></i></button> </td></tr>'	                               
                              ]).draw();
							}
						
					  
						//orderId
					}

				}// end if check state
			}// end function
			
	
			xmlhttp.open("POST", "get_credit_inv_by_keyword_background.jsp?type="+type+"&keyword="+keyword, true);
			xmlhttp.send();
		
		
		
		//alert(type);
	}
	
	
	function show_order_detail(order_id)
	{
		sessionStorage.setItem("order_id_for_get_detail", order_id); 
		
		window.open("report_credit_inv_detail.jsp");
	}
	
	function show_modal_create_canceled_inv(){
		
		$('#createCanceledInv').modal('show');
	}
	
	function create_canceled_inv(){
		
		
		var select_inv_type = document.getElementById('select_inv_type').value ;
		var cancelled_inv_date  = document.getElementById('cancelled_inv_date').value ;
		var cancelled_inv_no = document.getElementById('cancelled_inv_no').value ;
		
		var parameter = "";
		
			   parameter = "inv_type="+select_inv_type+"&inv_date="+cancelled_inv_date+"&inv_no="+cancelled_inv_no;
		
		var link = "";
		
		if(select_inv_type=="credit"){
			
			link = "create_cancelled_credit_inv_background.jsp?"+parameter; 
			
		}else{
			
			link = "create_cancelled_cash_inv_background.jsp?"+parameter;; 
			
		}
		
		alert(link);
		
		 
		 
		var xmlhttp;
		
		if(window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.onreadystatechange = function() {
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
				//alert("I'm Back");//////////////////
				
		
				var jsonObj = JSON.parse(xmlhttp.responseText);
			
				//	alert("I'm Back");
				
				if(jsonObj.length == 0) {
			
					alert("Keyword not found.");
				}
				else{
				  
					//orderId
				}

			}// end if check state
		}// end function
		

		xmlhttp.open("POST", link, true);
		xmlhttp.send();
	
	
	}
	
</script>
<style>
	.text-right {
 		 text-align:right;
	}

</style>
    

</head>

<body>

    <div id="wrapper">

    		<nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>
                <div class="sidebar-header">
                    <h3>Bored Sidebar</h3>
                </div>
                
    
        		<div id="includedSidebar"></div>
        		
        	</nav>



        <!-- Navigation -->
       

        <div id="content" class="form-control">
        
        	   <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-justify"></i>
                                <span></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Page</a></li>
                       
                            </ul>
                        </div>
                    </div>
                </nav>
             <div class="row">
	                <div class="col-lg-12">
	                                    
	                    <h1 class="page-header">Credit Invoice</h1>
	           			<div align="right">
	           				<button type="button" class="btn btn-default btn-lg" onclick="show_modal_create_canceled_inv()">
								  <span class="glyphicon glyphicon-duplicate" aria-hidden="true"></span> Create Canceled Inv
								</button>
	           			</div>
	                </div>
	                
	                
                <!-- /.col-lg-12 -->
             </div>
             <div class="row">
             	<div class="container">
					    <div class="row">    
					        <div class="col-xs-8 col-xs-offset-2">
							    <div class="input-group">
					                <div class="input-group-btn search-panel">
					                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					                    	<span id="search_concept">Filter by</span> <span class="caret"></span>
					                    </button>
					                    <ul class="dropdown-menu" role="menu">
					                      <li><a href="#" onclick="change_search_by_keyword()">Keyword</a></li>
					                      <li><a href="#" onclick="change_search_by_keyword()">Status</a></li>
					                      <li><a href="#" onclick="change_search_by_date()">Date</a></li>	
					                    </ul>
					                </div>
					                <input type="hidden" name="search_param" value="all" id="search_param">         
					                <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Search term...">
					                <span class="input-group-btn">
					                    <button class="btn btn-default" onclick="search_credit_inv()" type="button"><span class="glyphicon glyphicon-search"></span></button>
					                </span>
					            </div>
					        </div>
						</div>
					</div>

           	  </div>
           	  
           	  <br>
  
             <div class="row">
                <div class="col-xs-8 col-xs-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           	Credit Note 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        	<div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example-inv">
                                    <thead>
                                        <tr>
                                      <!--   	<th>ID</th>   -->
                                            <th>Invoice No.</th>
                                            <th>Vendor Name</th>
                                            <th>Grand Total</th>
                                            <th>Status</th>
                                            <th style="width:1%"></th>
                                          
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        
                           
                                       
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        
        

        
        </div>
        
        <div class="modal fade" id="createCanceledInv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style=" position: fixed; top: 10%; left: 10%; right: 10%; bottom: 15% ">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Create Canceled Inv</h4>
                                        </div>
                                        <div class="modal-body">
                                       		<div class="row">
                                       			.	<div class="col-md-4">
                                       					 <label class="text" style="font-size: 17px;">INV No : </label>
                                       				</div>
                                       				<div class="col-md-6">
                                       					<input name="cancelled_inv_no" id="cancelled_inv_no" type="text" class="form-control" placeholder="XXXX">
                                       				</div>
                                       				
                                       		</div>
                                       		<div class="row">
                                       				<br>
                                       				<div class="col-md-4">
                                       					 <label class="text" style="font-size: 17px;">INV Date : </label>
                                       				</div>
                                       				<div class="col-md-4">
                                       					<input name="cancelled_inv_date" id="cancelled_inv_date" type="date" class="form-control" placeholder="">
                                       				</div>
                                       				<br>
                                       		
                                       		</div>
                                       		<div class="row">
                                       				<br>
                                       				<div class="col-md-4">
                                       					 <label class="text" style="font-size: 17px;">Type : </label>
                                       				</div>
                                       				<div class="col-md-4">
                                       						  <select class="form-control" id="select_inv_type" >
																	    <option value="credit">Credit Inv</option>
																	    <option value="cash">Cash Inv</option>											    
												 			 </select>
                                       				</div>
                                       				<br>
                                       		
                                       		</div>
        
        
                                              
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" name="submit_create_canceled"  id="submit_create_canceled" class="btn btn-primary" onclick="create_canceled_inv()">Create Invoice</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
      </div>
        
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
   <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    
    
    <script>
    $(document).ready(function() {
    	
     	
    	$("#includedSidebar").load("sidebar.jsp"); 
    	
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').fadeOut();
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        
        
    	
    	
        $('#dataTables-example').DataTable({
                responsive: true
        });
        $('.search-panel .dropdown-menu').find('a').click(function(e) {
    		e.preventDefault();
    		var param = $(this).attr("href").replace("#","");
    		var concept = $(this).text();
    		$('.search-panel span#search_concept').text(concept);
    		$('.input-group #search_param').val(param);
    	});
		
        set_credit_note_to_table();
       // 	init_monthly_total_direct_bill();
    });
    </script>
	<script>
    // tooltip demo
	    $('.tooltip-demo').tooltip({
	        selector: "[data-toggle=tooltip]",
	        container: "body"
	    })
	
	    // popover demo
	    $("[data-toggle=popover]")
	        .popover()
    </script>
</body>

</html>
