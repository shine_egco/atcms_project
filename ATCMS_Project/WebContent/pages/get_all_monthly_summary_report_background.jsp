<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>



<% 

	System.out.println("Start get_all_monthly_summary_report_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

	List<MonthlySummaryReport> report_list = new ArrayList<MonthlySummaryReport>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();

      try{
    	  
    	  String sql_query = " SELECT * "+
							" FROM monthly_summary_report"+
							" WHERE 1"+
							" ORDER BY monthly_summary_report.month  ASC";
    	  
    	  System.out.println("sql_query:"+sql_query);
    	  ResultSet rs_msr = connect.createStatement().executeQuery(sql_query);
          
    	  while(rs_msr.next())
          {
    		  MonthlySummaryReport msr = new MonthlySummaryReport();
							
   						msr.setIndex(rs_msr.getString("index"));
   						msr.setReportID(rs_msr.getString("report_id"));
   						msr.setMonth(rs_msr.getString("month"));
   						msr.setYear(rs_msr.getString("year"));
   						msr.setDateOfValidation(rs_msr.getString("date_of_validation"));
 
   
    		 			msr.setAllCreditInvTotalSum(rs_msr.getString("all_credit_inv_total_sum"));
    		 			msr.setAllCreditInvTotalProfit(rs_msr.getString("all_credit_inv_total_profit"));
    		 			msr.setCurrentCreditInvTotalSum(rs_msr.getString("current_credit_inv_total_sum"));
    		 			msr.setCurrentCreditInvTotalProfit(rs_msr.getString("current_credit_inv_total_profit"));
    		 			
    		 			msr.setCashInvTotalSum(rs_msr.getString("cash_inv_total_sum"));
    		 			msr.setCashInvTotalProfit(rs_msr.getString("cash_inv_total_profit"));
    		 			
    		 			msr.setDailyNovatTotalSum(rs_msr.getString("daily_novat_total_sum"));
    		 			msr.setDailyNovatTotalProfit(rs_msr.getString("daily_novat_total_profit"));
    		 			
    		 			msr.setDirectBillTotal(rs_msr.getString("direct_bill_total"));
    		 			msr.setIndirectBillTotal(rs_msr.getString("indirect_bill_total"));
    		 			
    		 			msr.setCreditNoteTotalSum(rs_msr.getString("credit_note_total_sum"));
    		 			msr.setCreditNoteTotalProfit(rs_msr.getString("credit_note_total_profit"));
    		 			msr.setStatus(rs_msr.getString("status"));	
    		 		
					
    		  report_list.add(msr);
              
          }
             
         	try {
				Json = mapper.writeValueAsString(report_list); 
				System.out.println("Json:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	  connect.close();
         	out.print(Json);
	   
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  connect.close();
    	  out.print("fail");
      }
     
  
	connect.close();
%>
