<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

 <script>
	 
	 
	 function show_confirm_delete(id){
		
 		$('#confirm-delete-order').modal('show');
 		//alert(id);
		  document.getElementById("delete_bill_id").value = id;

		 
	 }
	
	 function formatMoney(number, places, symbol, thousand, decimal) {
			number = number || 0;
			places = !isNaN(places = Math.abs(places)) ? places : 0;
			symbol = symbol !== undefined ? symbol : "";
			thousand = thousand || ",";
			decimal = decimal || ".";
			var negative = number < 0 ? "-" : "",
			    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
			    j = (j = i.length) > 3 ? j % 3 : 0;
			return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
	}

	 
	 function onchange_capacity_unit_ul(temp){
	
			var click_li_value = temp.innerHTML;
			var capacity_but =   document.getElementById("capacity_but");
				  capacity_but.innerHTML = click_li_value+"<span class='caret'></span>";
				  capacity_but.name = click_li_value.toLowerCase();
				 // alert(capacity_but.name);
				  
		  
	 }
	 
	 function onchange_packaging_unit_ul(temp)
	 {
			var click_li_value = temp.innerHTML;
			var packaging_but =   document.getElementById("packaging_but");
			   	packaging_but.innerHTML = click_li_value+"<span class='caret'></span>";
			   	packaging_but.name = click_li_value.toLowerCase();
		 
	 }
	 
	 function get_atc_product_related_formula_to_table(){
		 
		 
		  var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
					
			
					
					var jsonObj = JSON.parse(xmlhttp.responseText);
				
			
					if(jsonObj.length == 0) {
					//	massage();
					//	alert("0 Result");
					}
					else{
					
						
						for(i in jsonObj) {
							$('#datatables_product_related_formula').DataTable().row.add([
	                           '<tr><td>'+jsonObj[i].nameTH+'</td>'
	                           ,'<td>'+jsonObj[i].nameEN+'</td>'
	                           ,'<td><center>'+jsonObj[i].capacity+' '+jsonObj[i].capacityUnit+'/'+jsonObj[i].packageUnit+'</center></td>'
	                           ,'<td><button id = "'+jsonObj[i].productID+'" name =  "'+jsonObj[i].formulaID+'"   type="button" class="btn btn-info btn-circle btn-md" onclick = "grap_product(this)">'
                                +'<i class="glyphicon glyphicon-chevron-right"></i></button></td></tr>'
	                                           
	                         ]).draw();
						}
						
						
					}
					
					
					//System.out.println("temp_x:"+temp_x);	
				
				
				}// end if check state
			}// end function
			
	
			xmlhttp.open("POST", "get_product_related_formula_background.jsp", true);
			xmlhttp.send();
			
		 
	 }
	 
		function grap_product(button)
		{
			var product_id = button.id;
			var formula_id = button.name;
			
			if(product_id != "") {
						
						/* AJAX */
						var xmlhttp;
						
						if(window.XMLHttpRequest) {
							// code for IE7+, Firefox, Chrome, Opera, Safari
							xmlhttp = new XMLHttpRequest();
						}
						else {
							// code for IE6, IE5
							xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
						}
						
						xmlhttp.onreadystatechange = function() {
							if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
								var jsonObj = JSON.parse(xmlhttp.responseText);
								
								if(jsonObj.length == 0) {
			
									alert("no  Data");
								}
								else{
								//	alert("get Data");
									
									  var newid = 0;
								        $.each($("#tab_logic tr"), function() {
								            if (parseInt($(this).data("id")) > newid) {
								                newid = parseInt($(this).data("id"));
								            }
								        });
								        newid++;
								        
								        var tr = $("<tr></tr>", {
								            id: "addr"+newid,
								            "data-id": newid
								        });
								        
								     
								        // loop through each td and create new elements with name of newid
								            $.each($("#tab_logic tbody tr:nth(0) td"), function() {
								                var cur_td = $(this);
								                
								                var children = cur_td.children();
								                
								                // add new td and element if it has a name
								                if ($(this).data("name") != undefined) {
								                    var td = $("<td></td>", {
								                        "data-name": $(cur_td).data("name")
								                    });
								                    
								                    var c = $(cur_td).find($(children[0]).prop('tagName')).clone().val("");
								                    c.attr("name", $(cur_td).data("name") + newid);			
								                    c.attr("id", $(cur_td).data("name") + newid);		
								                    c.appendTo($(td));
								                    td.appendTo($(tr));

								                    
								                } else {
								                    var td = $("<td></td>", {
								                        'text': $('#tab_logic tr').length
								                    }).appendTo($(tr));
								                }
								            });
								        	
								            

								            
								            // add the new row
								            $(tr).appendTo($('#tab_logic'));
								        
								            $(tr).find("td button.row-remove").on("click", function() {
								                 $(this).closest("tr").remove();
								                 calculate_total();
								            });
								            
								            var  pd_name = document.getElementById("name"+newid);
								            var  pd_unit = document.getElementById("unit"+newid);
								           // var  pd_addition = document.getElementById("addition"+newid);
								            var  pd_cost = document.getElementById("cost"+newid); 
								            var  pd_quantity = document.getElementById("quantity"+newid); 
								            var  pd_sum = document.getElementById("sum"+newid); 
								            var  pd_id = document.getElementById("raw_id"+newid); 
								         //   var  pd_lan_flag = document.getElementById("lan_flag"+newid); 

									             pd_name.value =  jsonObj[0].displayName;
								                 pd_unit.value = jsonObj[0].displayUnit;
								      
								            	 pd_cost.value = parseFloat(jsonObj[0].productCost).toFixed(2);
								         //   	 pd_lan_flag.value = jsonObj[0].productLanFlag;
							            		 pd_quantity.value = 1;
							            		 pd_sum.value = pd_cost.value ;
							            		 pd_id.value = jsonObj[0].productID;
							            		 pd_id.name = jsonObj[0].formulaID;

												 calculate_total();
								}
							}
							
						}
						
						xmlhttp.open("POST", "fetch_atc_product_full_data_by_product_id_background.jsp?product_id="+product_id, true);
						xmlhttp.send();
			}
			 // Dynamic Rows Code
	        
	        // Get max row id and set new id

			
		}
	 
	 
	 function generateBatchNumber(){
		 
		 
		 var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
									
							if(xmlhttp.responseText=="fail")
							{
								
							}else{
								
								var jsonObj = JSON.parse(xmlhttp.responseText);
								
								var batch_number =   document.getElementById("batch_number");
									   batch_number.value = jsonObj.temp_code;
						
									 //  alert(xmlhttp.responseText);
								
								
								
							}
				
				}// end if check state
			}// end function
			
	
			xmlhttp.open("POST", "generate_batch_number_background.jsp", true);
			xmlhttp.send();
		 
		 
	 }
	 
		function calculate_sum(id_in)
		{
			

			var id ;
			if((id_in.length==6)||(id_in.length==9))
			{
				//alert("<10");
				id = id_in.slice(-1);
				
			}else{
				//alert("over 10");
				id = id_in.slice(-2);
			}

			var sum_price = 0;
			var pd_cost = document.getElementById("cost"+id);
			var pd_quantity = document.getElementById("quantity"+id);
			
				sum_cost = pd_cost.value * pd_quantity.value ; 
				
			var pd_sum = document.getElementById("sum"+id);
				   pd_sum.value = parseFloat(sum_cost).toFixed(2); 
			
			
				calculate_total();
				 
		}
		
function calculate_total(){
			
			//alert("calculate_total");
			
			var tbody = document.getElementById("pd_tbody");
	//		var total_vat = document.getElementById("total_vat");
			var total_sum_cost = document.getElementById("total_sum_cost");
			
			var total_novat = 0 ;

			$('#tab_logic > tbody  > tr').each(function() {
				
					//alert(this.id);
					if(this.id=="addr0")
					{
		
					}else{
					//	alert(this.id);
						var id = this.id.substring(4);
							//alert(id);
						var sum = document.getElementById("sum"+id);
						
					
						
						 total_novat = parseFloat(total_novat, 10) + parseFloat(sum.value, 10 ) ; 
						 
						//alert("total_novat:"+total_novat+" sum.value:"+sum.value);
						
					}
				
			 });
			
			//need many scenario for testing 
	
	
			var final_total_sum;
			var total_novat_str = total_novat + "";
			
		
			if(total_novat_str.indexOf(".")>0)
			{
				var total_sum_string_parts = total_novat_str.split('.');
				final_total_sum = total_sum_string_parts[0] +"."+ total_sum_string_parts[1].substring(0,2);
			}
			else{
				final_total_sum = total_sum_string;
			}
		
			

			total_sum_cost.value =   parseFloat(final_total_sum);

			
			
			
		}
		
		function create_work_order(){
			
			var checking ="";
		   var work_order_main_id ="";
			///////////////////////////////////////////
			var manufac_date = document.getElementById('manufac_date');
			var manufac_date_value = manufac_date.value ; 
			////////////////////////////////////////////////////
			var batch_number = document.getElementById('batch_number');
			var batch_number_value = batch_number.value;	
			////////////////////////////////////////////////////
			
			var total_sum_cost = document.getElementById('total_sum_cost');
			var total_sum_cost_value = total_sum_cost.value;
				
			var row_count = $('#tab_logic > tbody  > tr').length;
			//alert("row_count:"+row_count);

			if((row_count>1)&&(manufac_date_value!="")){
				
				var pd_array = new Array();
	
					$('#tab_logic > tbody  > tr').each(function() {
						if(this.id=="addr0")
						{
							
						}else{
							
							var id ;
							if(this.id.length==5)
							{
								id = this.id.slice(-1);
							}else{
								id = this.id.slice(-2);
							}
							//alert("id:"+id);
							
							pd_array.push(id);		
						}
						
					});
				
					
					for(var i=0 ; i<pd_array.length ; i++){
						
						//alert(pd_array[i]);
					}
					
					var index =  parseInt(pd_array.length, 10);

					
					var parameter_work_order_main = "manufac_date="+manufac_date_value+
																	  		 "&batch_number="+batch_number_value+
																	  		 "&total_sum_cost="+total_sum_cost_value+
																	  		 "&index="+index;
					
					var parameter_work_order_detail ="";
				//	alert("index:"+index);
					for(var j=0;j<index ;j++)
					{
				
						var pd_id = document.getElementById("raw_id"+pd_array[j]);
						var pd_name = document.getElementById("name"+pd_array[j]);
						var pd_cost = document.getElementById("cost"+pd_array[j]);
						var pd_quantity = document.getElementById("quantity"+pd_array[j]);
						var pd_sum = document.getElementById("sum"+pd_array[j]);
						var pd_unit = document.getElementById("unit"+pd_array[j]);

						parameter_work_order_detail =	parameter_work_order_detail +
								 						"&pd_id"+j+"="+pd_id.value+ 
								 						"&pd_formula_id"+j+"="+pd_id.name+ 
														"&pd_name"+j+"="+pd_name.value+
														"&pd_cost"+j+"="+pd_cost.value+
														"&pd_quantity"+j+"="+pd_quantity.value+
														"&pd_sum"+j+"="+pd_sum.value+
														"&pd_unit"+j+"="+pd_unit.value;
										
					}

					var xmlhttp;
					
					if(window.XMLHttpRequest) {
							// code for IE7+, Firefox, Chrome, Opera, Safari
							xmlhttp = new XMLHttpRequest();
					}
					else {
							// code for IE6, IE5
							xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					
					xmlhttp.onreadystatechange = function() {     
					
						if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
								var result = xmlhttp.responseText;
								
								if(result=="error")
								{
									alert("Error Occer can't create Main Order Ticket")
								}
								else{

									work_order_main_id = result;
									console.log(result);
									alert("result:"+result);
								
									window.location = "production_work_order_main.jsp";
									
								}
						
							}		
					}
					
				//	alert(parameter_cash_bill_detail);
				    console.log(parameter_work_order_main+parameter_work_order_detail);
					xmlhttp.open("POST", "create_work_order_full_background.jsp?"+parameter_work_order_main+parameter_work_order_detail, true);
					xmlhttp.send();
					
					
					
			}else{
				$('#myModal').modal('hide');
				alert("Some Parameter Missing");
				//  $("#myModal").modal('hide');
			  
			}

			
		}
	 
	 
</script>
<style>
	.text-right {
 		 text-align:right;
	}
	

</style>
    

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
       		   <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.jsp">ATCMS Admin</a>
            </div>
            <!-- /.navbar-header -->

   
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="dashboard_main.jsp"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-group fa-fw"></i>Customer&Vendor<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="customer_sub.jsp">Customers</a>
                                </li>
                                <li>
                                    <a href="vendor_sub.jsp">Vendors</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                         <li>
                            <a href="#"><i class="fa fa-leaf"></i>  Product<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="atc_product_sub.jsp">ATC Product</a>
                                </li>
                                <li>
                                    <a href="other_product_sub.jsp">Other Product</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o"></i>  Bill Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="direct_bill_sub.jsp">Direct Bill</a>
                                </li>
                                <li>
                                    <a href="indirect_bill_sub.jsp">Indirect Bill</a>
                                </li>
                                <li>
                                    <a href="credit_inv_sub.jsp">Credit Invoice</a>
                                </li>
                                <li>
                                    <a href="cash_inv_sub.jsp">Cash Invoice</a>
                                </li>
                                <li>
                                    <a href="credit_note_sub.jsp">Credit Note (ใบลดหนี้)</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                        	<a href="#"><i class="glyphicon glyphicon-fire"></i> Production (การผลิต)<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">                         
                               <li>
                                    <a href="production_material_main.jsp">Material</a>
                                </li>
                                <li>
                                    <a href="production_work_order_main.jsp">Work Order</a>
                                </li>
                                <li>
                                    <a href="production_formula_main.jsp">Formula</a>
                                </li>
                                 <li>
                                    <a href="production_product_relate_formula.jsp">Product Relate Formula</a>
                                </li>                
                            </ul>
                        </li>
                        <li>
                        	<a href="#"><i class="glyphicon glyphicon-stats"></i> Reporting (รายงาน)<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="monthly_summary_report_main.jsp">Monthly Summary Report</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                        	<a href="#"><i class="glyphicon glyphicon-road"></i> (DEVEL)0pinG Z()Ne <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="dev_show_profit_each_bill_detail.jsp">Each  Bill Value Detail</a>
                                </li>
                                <li>
                                    <a href="dev_balancing_work_order.jsp">Balancing Work Order</a>
                                </li>
                
                            </ul>
                        </li>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
             <div class="row">
	                <div class="col-lg-12">
	                    <h1 class="page-header"></h1>
	                </div>
                <!-- /.col-lg-12 -->
             </div>
                    
         
         	 <div class="row">
         	 		  <div class="col-lg-6">
		                    <div class="panel panel-default">
		                        <div class="panel-heading">
		                            ATC Product List  (Formula Related)
		                        </div>
		                        <div class="panel-body">
		                        <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="datatables_product_related_formula">
                                    <thead>
                                        <tr>
                                            <th>Name TH</th>
                                            <th>Name EN</th>
                                       		<th>Unit</th>
                                       		 <th style="width:5%"></th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
		                        
		                        
		                        
		                        </div>
		                   </div>
	                  </div>
	                   
	                  <div class="col-lg-6" >
		                    <div class="panel panel-default">
		                        <div class="panel-heading">
		                            New Work Order
		                        </div>
		                        <div class="panel-body">
		                        	<label>Manufacturing  Date : </label>
	                           		 	<input type="date" id="manufac_date" name ="manufac_date" class="form-control" >  					           
	                           		 <br>
	                           		 <label>Batch Number : </label>
	                           		 <input type="text" id="batch_number" name = "batch_number" class="form-control" readonly >
	                           		 <br>

	                      			 <hr>
	                      	
												  <table class="table table-bordered table-hover table-sortable" id="tab_logic" onchange="calculate_total()">
														<thead>
															<tr>
																<th class="text-center">
																	Product Name
																</th>
																<th class="text-center" style="width: 90px;">
																	Unit
																</th>
																<!-- 
																<th class="text-center">
																	Addition 
																</th>
																 -->
																<th class="text-center" style="width: 110px;">
																	Cost
																</th>
										    					<th class="text-center" style="width: 90px;">
																	Quantity
																</th>
																<th class="text-center" style="width: 130px;">
																	Sum
																</th>
																
										        				<th class="text-center" style="border-top: 1px solid #ffffff; border-right: 1px solid #ffffff;">
																</th>
																
															</tr>
														</thead>
														<tbody id = "pd_tbody">
														
										    				<tr id='addr0' data-id="0" class="hidden">
																<td data-name="name">
																    <input type="text" name='pd_name' id = "pd_name" class="form-control"/>
																</td>
																<td data-name="unit">
																    <input type="text" name='pd_unit' id = "pd_unit" class="form-control" style="text-align:center;" />
																</td>
																<!-- 
																<td data-name="addition">
																    <input type="text" name='pd_addition' id="pd_addition" placeholder='Color , Perfume Code , etc' class="form-control"/>
																</td>
																 -->
																<td data-name="cost">
																    <input type ="text" name = 'pd_cost' id="pd_cost" class="form-control text-right" oninput="calculate_sum(this.id)" >
																</td>
										    					<td data-name="quantity">
																    <input type ="number" name = 'pd_quantity' id="pd_quantity" class="form-control text-right" oninput="calculate_sum(this.id)" >
																</td>
																<td data-name="sum">
																	 <input readonly type="text" name='pd_sum' id = "pd_sum" class="form-control text-right" onchange="calculate_total()"/>
																	 
																</td>
																
																<td data-name="raw_id" class="hidden">
																	<input type="hidden" name="pd_id" id="pd_id">
																</td>
																 
																
										                        <td data-name="del">
										                            <button name="del0" class='btn btn-danger glyphicon glyphicon-remove row-remove' ></button>
										                        </td>
										                        
															</tr>
														</tbody>
													</table>	
													
													<table class="table table-bordered table-hover table-sortable " id="table_summary" style="border:none;">

														
												
															<tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Total : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input readonly type="text" name="total_sum_cost" id="total_sum_cost" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>
															
				
													</table>
									<!-- /.row -->
	       
		                        
		                        		                    
		          				<div class="row" >
		          					
										<br>
										<div class="pull-right"  style="padding-right:2em ;">
												<button type="button" class="form-control btn btn-primary" style="width:200px;" onclick="create_work_order()">Save</button>
										</div>
								</div>
								
	                       	    </div>
	                       	    
	                       	    
		                    </div>
		                  
         	 		  </div>
         	 
         	 </div>
        

        
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    
    
    <script>
    $(document).ready(function() {
    	
        $('#dataTables-example').DataTable({
                responsive: true
        });
        
        get_atc_product_related_formula_to_table();
        generateBatchNumber();

		

    });
    </script>
	<script>
    // tooltip demo
	    $('.tooltip-demo').tooltip({
	        selector: "[data-toggle=tooltip]",
	        container: "body"
	    })
	
	    // popover demo
	    $("[data-toggle=popover]")
	        .popover()
    </script>
</body>

</html>
