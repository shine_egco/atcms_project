<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.Locale" %>



<% 

	System.out.println("Start get_purchase_detail_by_product_id_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

	List<PurchaseDetail> purchase_list = new ArrayList<PurchaseDetail>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	
	String product_id = request.getParameter("product_id");
	
      try{
    	  
    	  String sql_query  = " SELECT purchase_main.inv_no "+
    			  				"	 , purchase_detail.purchase_id "+
    			  				"	 , purchase_detail.sum "+
    			  				"	 , purchase_detail.price "+
    			  				"	 , purchase_detail.quantity "+
    			  				"	 , company.name_th "+
    			  				"	 , company.name_en "+
    			  				"	 , purchase_main.invoice_date "+
    			  				" 	 , product.unit_en "+
    			  				"    , product.unit_th "+
    			 			" FROM purchase_detail "+
    			 			" JOIN purchase_main "+
    						" ON  purchase_main.purchase_id = purchase_detail.purchase_id "+
    			 			" JOIN company "+
    						" ON  purchase_main.vendor_id = company.company_id "+
    			 			" JOIN product "+
    						" ON  product.product_id = purchase_detail.product_id "+
    						" WHERE BINARY purchase_detail.product_id = '"+product_id+"'";
    	  
    	  ResultSet rs_pur = connect.createStatement().executeQuery(sql_query);
          
    	  System.out.println("sql_query:"+sql_query);
    	  
    	  while(rs_pur.next())
          {
    		  PurchaseDetail pur = new PurchaseDetail();
    		  
    		  pur.setInvNo(rs_pur.getString("inv_no"));
    		  pur.setPurchaseId(rs_pur.getString("purchase_id"));
    		  
    		  if(("-").equals(rs_pur.getString("name_en")))
    		  {
    			  pur.setVendorName(rs_pur.getString("name_th"));

    			  
    		  }else{
    			  pur.setVendorName(rs_pur.getString("name_en"));
    			
    		  }
    		  
    		  if(("-").equals(rs_pur.getString("unit_en")))
    		  {
    			 
    			  pur.setProductUnit(rs_pur.getString("unit_th"));
    			
    			  
    		  }else{
    	
    			  pur.setProductUnit(rs_pur.getString("unit_en"));
    		  }
    		  
    		
    		  
    		
    		  pur.setPrice(rs_pur.getString("price"));
    		  pur.setQuantity(rs_pur.getString("quantity"));
    		  pur.setSum(rs_pur.getString("sum"));
    		  
    		  
    		  String inv_date_old_str = rs_pur.getString("invoice_date");
    		//  Date date = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH).parse(inv_date_old_str);
    		 
    		  
    		//  String inv_date_new_str =  new SimpleDateFormat("dd MMMM yyyy",Locale.ENGLISH).format(date);
    		  pur.setInvoiceDate(inv_date_old_str);
    		  
    		  
    		  purchase_list.add(pur);
              
          }
             
         	try {
				Json = mapper.writeValueAsString(purchase_list); 
				System.out.println("Json_pur:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	out.print(Json);
	   
	 
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  connect.close();
    	  out.print("fail");
      }
     
    System.out.println("SUCCESS");
	connect.close();
%>
