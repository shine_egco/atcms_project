<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.util.concurrent.TimeUnit" %>
<%@ page import = "java.sql.PreparedStatement" %>


<% 

	System.out.println("Start dev_regenerate_code_company_id_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
  

      try{
    	   KeyGen key = new KeyGen();
    	
    	//   System.out.println("key_str:"+key_str);
    	   
    	   
    	   String sql_company =     " SELECT * "+
    	   											" FROM company "+
    			   									" WHERE 1";
    	   ResultSet rs_main = connect.createStatement().executeQuery(sql_company);
    	   
    	   int count = 0;
    	   
    	   while(rs_main.next())
           {
    		   
    		   String old_company_id = rs_main.getString("company_id");
    		  System.out.println(count+":"+old_company_id);
    		   
    		   String new_company_id = key.generateCompanyID();
    		   TimeUnit.MILLISECONDS.sleep(100);
    		   System.out.println("new-> :"+new_company_id);
    		  	 
			   String regen_cash_bill_main = " UPDATE cash_bill_main "+
							" SET cash_bill_main.customer_id = '"+new_company_id+"' "+
							" WHERE BINARY cash_bill_main.customer_id = '"+old_company_id+"' ";
			   
			     System.out.println("----regen_cash_bill_main:"+regen_cash_bill_main);
    		   
    			//Active
			   connect.createStatement().executeUpdate(regen_cash_bill_main);
    			   
    			   String regen_credit_note_main = " UPDATE credit_note_main "+
								" SET credit_note_main.customer_id = '"+new_company_id+"' "+
								" WHERE BINARY credit_note_main.customer_id = '"+old_company_id+"' ";
    			   
    			   System.out.println("----regen_credit_note_main:"+regen_credit_note_main);
    			   // Active
    			  connect.createStatement().executeUpdate(regen_credit_note_main);
    			   
    			   String regen_order_main = "  UPDATE order_main "+
							" SET order_main.customer_id = '"+new_company_id+"' "+
							" WHERE BINARY order_main.customer_id = '"+old_company_id+"' ";
    			   
    			   System.out.println("----regen_order_main:"+regen_order_main);
    			   // Active
    			   connect.createStatement().executeUpdate(regen_order_main); 

    			  String regen_purchase_main = "  UPDATE purchase_main "+
							" SET purchase_main.vendor_id = '"+new_company_id+"' "+
							" WHERE BINARY purchase_main.vendor_id = '"+old_company_id+"' ";
    			  
    			   System.out.println("----regen_purchase_main:"+regen_purchase_main);
    			   // Active
    			   connect.createStatement().executeUpdate(regen_purchase_main); 
    			   
    			     String regen_company = "  UPDATE company "+
							" SET company.company_id = '"+new_company_id+"' "+
							" WHERE BINARY company.company_id = '"+old_company_id+"' ";
    			  
    			   System.out.println("----regen_company:"+regen_company);
    			   // Active
    			   connect.createStatement().executeUpdate(regen_company); 





    		   count++;
    		   
           }
    	  
    	
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  connect.close();
    	  out.print("fail");
      }
     
    System.out.println("REGENERATE ORDER_ID SUCCESS");
    out.print("success");
	connect.close();
%>
