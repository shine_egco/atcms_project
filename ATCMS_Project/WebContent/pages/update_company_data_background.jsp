<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "com.atcms.*" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "java.util.Properties" %>
<%@ page import = "javax.servlet.ServletContext" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>


<% 

	System.out.println("Start update_company_data_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}
	///////////////////file_path parameter/////////////
	String company_id = request.getParameter("company_id");
	String name_th = request.getParameter("name_th");
	String name_en = request.getParameter("name_en");
	String tax_id = request.getParameter("tax_id");
	String address_th_line_1 = request.getParameter("address_th_line_1");
	String address_th_line_2 = request.getParameter("address_th_line_2");
	String address_en_line_1 = request.getParameter("address_en_line_1");
	String address_en_line_2 = request.getParameter("address_en_line_2");
	String credit = request.getParameter("credit");
	
	

	try {		
		
		
		String sql_query =" UPDATE company "+
						  " SET company.name_th='"+name_th+"' "+
						  	 " ,company.name_en='"+name_en+"' "+
							 " ,company.tax_id='"+tax_id+"' "+
						  	 " ,company.address_th_line1='"+address_th_line_1+"' "+
							 " ,company.address_th_line2='"+address_th_line_2+"' "+
						  	 " ,company.address_en_line1='"+address_en_line_1+"' "+
							 " ,company.address_en_line2='"+address_en_line_2+"' "+
						  	 " ,company.credit='"+ credit+"' "+
						  " WHERE BINARY company.company_id='"+company_id+"' " ;

		System.out.println("sql_query_update:"+sql_query);
		
	  connect.createStatement().executeUpdate(sql_query);
	
			

	out.println("SUCCESS");
		
	} catch (Exception e) {
		e.printStackTrace();
		  connect.close();
		out.println("FAIL");
	}
	 
	connect.close();
%>
