<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>
<%@ page import = "java.text.DateFormat" %>



<% 

	System.out.println("Start get_cash_bill_detail_by_cash_bill_id_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		/*
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		*/
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

		
	List<CashBillDetail> cb_detail_list = new ArrayList<CashBillDetail>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	//String purchase_id = request.getParameter("purchase_id");
	//System.out.println("purchase_id:"+purchase_id);
	 String cash_bill_id = request.getParameter("cash_bill_id");

	 System.out.println("cash_bill_id:"+cash_bill_id);



      try{
    	  
    
    	
       		String	sql_query = " SELECT * "+
       							" FROM cash_bill_detail "+
	       			  	        " JOIN product "+
    				   			" ON  cash_bill_detail.product_id=product.product_id "+
    						    " WHERE BINARY cash_bill_detail.cash_bill_id ='"+cash_bill_id+"'";
  
   			 
   			 

    
    	  System.out.println("sql_query:"+sql_query);
    	  
    	  
    	  ResultSet rs_cb = connect.createStatement().executeQuery(sql_query);
          
    	  
    	  
    	  
    	  while(rs_cb.next())
          {
    		  CashBillDetail cb_detail = new CashBillDetail();
             
    		  cb_detail.setAdtDescription(rs_cb.getString("adt_description"));
    		  cb_detail.setPrice(rs_cb.getString("price"));
    		  cb_detail.setQuantity(rs_cb.getString("quantity"));
    		  cb_detail.setSum(rs_cb.getString("sum"));
         		
           	  if(rs_cb.getString("name_th").equals("-"))
                {
           		cb_detail.setProductUnit(rs_cb.getString("unit_en"));
	           	}else{
	           	
	           		cb_detail.setProductUnit(rs_cb.getString("unit_th"));
	           	}
         		
         		
           	cb_detail.setCostPerUnit(rs_cb.getString("cost_per_unit"));
           	cb_detail.setSumCost(rs_cb.getString("sum_cost"));
           	cb_detail.setProfitPerUnitI(rs_cb.getString("profit_per_unit"));
           	cb_detail.setSumProfit(rs_cb.getString("sum_profit"));
           	cb_detail.setProductId(rs_cb.getString("product_id"));
           	cb_detail.setIndex(rs_cb.getString("index"));
           
           	  

           	cb_detail_list.add(cb_detail);
            } 
              
          
             
         	try {
				Json = mapper.writeValueAsString(cb_detail_list); 
				System.out.println("Json:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	
         	
         	out.print(Json);
	   
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  connect.close();
    	  out.print("fail");
      }
     
  
	connect.close();
%>
