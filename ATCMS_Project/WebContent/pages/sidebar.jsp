
             
                <ul class="list-unstyled components">
                    <li>
                    	<a href="dashboard_main.jsp"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                     <a href="#CV_SubMenu" data-toggle="collapse" aria-expanded="false">
                     	<i class="fa fa-group fa-fw"></i> Customer&Vendor
                     </a>
                        <ul class="collapse list-unstyled" id="CV_SubMenu">
                            <li><a href="customer_sub.jsp">Customers</a></li>
                            <li><a href="vendor_sub.jsp">Vendors</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#ProductSubmenu" data-toggle="collapse" aria-expanded="false">
                        	<i class="fa fa-leaf"></i> Product
                        </a>
                        <ul class="collapse list-unstyled" id="ProductSubmenu">
                            <li><a href="product_explorer_sub.jsp">Product Explorer</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#BillSubmenu" data-toggle="collapse" aria-expanded="false">
                        	<i class="fa fa-files-o"></i> Bill Management
                        </a>
                        <ul class="collapse list-unstyled" id="BillSubmenu">
                        	 <li><a href="dashboard_bm.jsp">DashBoard Billing</a></li>
                            <li><a href="direct_bill_sub.jsp">Direct Bill</a></li>
                            <li><a href="indirect_bill_sub.jsp">Indirect Bill</a></li>
                             <li><a href="credit_inv_sub.jsp">Credit Invoice</a></li>
                            <li><a href="cash_inv_sub.jsp">Cash Invoice</a></li>
                        <!--    <li><a href="abb_inv_sub.jsp">Abbreviated Invoice</a></li>
                                <li><a href="credit_note_sub.jsp">Credit Note</a></li>    -->
                        </ul>
                    </li>
                     <li>
                        <a href="#ProductionSubmenu" data-toggle="collapse" aria-expanded="false">
                        	<i class="glyphicon glyphicon-fire"></i> Production
                        </a>
                        <ul class="collapse list-unstyled" id="ProductionSubmenu">
                             <li><a href="production_material_main.jsp">Material</a></li>
                             <li><a href="production_work_order_main.jsp">Work Order</a></li>
							 <li><a href="production_formula_main.jsp">Formula</a></li>
							 <li><a href="production_product_relate_formula.jsp">Product Relate Formula</a></li>                
                        </ul>
                    </li>
                     <li>
                        <a href="#ReportSubmenu" data-toggle="collapse" aria-expanded="false">
                        	<i class="glyphicon glyphicon-stats"></i> Reporting
                        </a>
                        <ul class="collapse list-unstyled" id="ReportSubmenu">
                             <li><a href="monthly_summary_report_main.jsp">Monthly Summary Report</a></li>
                           
                        </ul>
                    </li>
                    <li>
                        <a href="#DevSubmenu" data-toggle="collapse" aria-expanded="false">
                        	<i class="glyphicon glyphicon-road"></i> (DEVEL)0pinG Z()Ne
                        </a>
                        
                        <ul class="collapse list-unstyled" id="DevSubmenu">
                             <li><a href="dev_show_profit_each_bill_detail.jsp">Each  Bill Value Detail</a></li>
                             <li><a href="dev_balancing_work_order.jsp">Balancing Work Order</a></li>
                             <li><a href="dev_change_id_format.jsp">Dev Change ID Format</a></li>
                             <li><a href="dev_testing_new_search.jsp">Testing New Search</a></li>
                        </ul>
                    </li>
                 
                </ul>

                <ul class="list-unstyled CTAs">
                    <li><a href="#" class="download">Download source</a></li>
                    <li><a href="#" class="article">Back to article</a></li>
                </ul>
      