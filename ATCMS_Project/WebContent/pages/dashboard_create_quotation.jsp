<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    

    <title> ATCMS Prototype</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
 
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../dist/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">

   <link href="../bower_components/sidebar/sidebar_style_3.css" rel="stylesheet" type="text/css">
   
 
   <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- 
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	 -->
	 <style>
				body.modal-open #wrap{
				    -webkit-filter: blur(7px);
				    -moz-filter: blur(15px);
				    -o-filter: blur(15px);
				    -ms-filter: blur(15px);
				    filter: blur(15px);
				}
				  
				.modal-backdrop {background: #f7f7f7;}
				
				.close {
				    font-size: 50px;
				    display:block;
				}
				
				.modal {
				    position: fixed;
				    top: 40%;
				    left: 10%;
				    right: 10%;
				    bottom: 15%;
				}			
	 </style>
	 <style type="text/css">
		 #loading {
			    background: #f4f4f2 url("img/page-bg.png") repeat scroll 0 0;
			    height: 100%;
			    left: 0;
			    margin: auto;
			    position: fixed;
			    top: 0;
			    width: 100%;
			}
			.bokeh {
			    border: 0.01em solid rgba(150, 150, 150, 0.1);
			    border-radius: 50%;
			    font-size: 100px;
			    height: 1em;
			    list-style: outside none none;
			    margin: 0 auto;
			    position: relative;
			    top: 35%;
			    width: 1em;
			    z-index: 2147483647;
			}
			.bokeh li {
			    border-radius: 50%;
			    height: 0.2em;
			    position: absolute;
			    width: 0.2em;
			}
			.bokeh li:nth-child(1) {
			    animation: 1.13s linear 0s normal none infinite running rota, 3.67s ease-in-out 0s alternate none infinite running opa;
			    background: #00c176 none repeat scroll 0 0;
			    left: 50%;
			    margin: 0 0 0 -0.1em;
			    top: 0;
			    transform-origin: 50% 250% 0;
			}
			.bokeh li:nth-child(2) {
			    animation: 1.86s linear 0s normal none infinite running rota, 4.29s ease-in-out 0s alternate none infinite running opa;
			    background: #ff003c none repeat scroll 0 0;
			    margin: -0.1em 0 0;
			    right: 0;
			    top: 50%;
			    transform-origin: -150% 50% 0;
			}
			.bokeh li:nth-child(3) {
			    animation: 1.45s linear 0s normal none infinite running rota, 5.12s ease-in-out 0s alternate none infinite running opa;
			    background: #fabe28 none repeat scroll 0 0;
			    bottom: 0;
			    left: 50%;
			    margin: 0 0 0 -0.1em;
			    transform-origin: 50% -150% 0;
			}
			.bokeh li:nth-child(4) {
			    animation: 1.72s linear 0s normal none infinite running rota, 5.25s ease-in-out 0s alternate none infinite running opa;
			    background: #88c100 none repeat scroll 0 0;
			    margin: -0.1em 0 0;
			    top: 50%;
			    transform-origin: 250% 50% 0;
			}
			@keyframes opa {
			12% {
			    opacity: 0.8;
			}
			19.5% {
			    opacity: 0.88;
			}
			37.2% {
			    opacity: 0.64;
			}
			40.5% {
			    opacity: 0.52;
			}
			52.7% {
			    opacity: 0.69;
			}
			60.2% {
			    opacity: 0.6;
			}
			66.6% {
			    opacity: 0.52;
			}
			70% {
			    opacity: 0.63;
			}
			79.9% {
			    opacity: 0.6;
			}
			84.2% {
			    opacity: 0.75;
			}
			91% {
			    opacity: 0.87;
			}
			}
			
			@keyframes rota {
			100% {
			    transform: rotate(360deg);
			}
			}
			
			datalist {
			  display: none;
			}
						
    </style>
	 
	<script type="text/javascript" src="../js/jquery-2.1.1.min.js"></script>
	<script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js"></script>
	
		
	<script>
	
	function grap_product()
	{
		var pro_search_input = document.getElementById("pd_name-0");
		
		var input_value = (pro_search_input.value).split(",");
		var product_id = input_value[0];

		
	
			
		//alert("product_id:"+product_id);
			
		if(product_id != "") {
					
					/* AJAX */
					var xmlhttp;
					
					if(window.XMLHttpRequest) {
						// code for IE7+, Firefox, Chrome, Opera, Safari
						xmlhttp = new XMLHttpRequest();
					}
					else {
						// code for IE6, IE5
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					
					xmlhttp.onreadystatechange = function() {
						if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
							var jsonObj = JSON.parse(xmlhttp.responseText);
							
							if(jsonObj.length == 0) {
		
								alert("no  Data");
							}
							else{
							//	alert("get Data");
								
								  var newid = 0;
							        $.each($("#tab_logic tr"), function() {
							            if (parseInt($(this).data("id")) > newid) {
							                newid = parseInt($(this).data("id"));
							            }
							        });
							        newid++;
							        
							        var tr = $("<tr></tr>", {
							            id: "addr"+newid,
							            "data-id": newid
							        });
							        
							     
							        // loop through each td and create new elements with name of newid
							            $.each($("#tab_logic tbody tr:nth(0) td"), function() {
							                var cur_td = $(this);
							                
							                var children = cur_td.children();
							                
							                // add new td and element if it has a name
							                if ($(this).data("name") != undefined) {
							                    var td = $("<td></td>", {
							                        "data-name": $(cur_td).data("name")
							                    });
							                    
							                    var c = $(cur_td).find($(children[0]).prop('tagName')).clone().val("");
							                    c.attr("name", $(cur_td).data("name") + newid);			
							                    c.attr("id", $(cur_td).data("name") + newid);		
							                    c.appendTo($(td));
							                    td.appendTo($(tr));
							                  

							                } else {
							                    var td = $("<td></td>", {
							                        'text': $('#tab_logic tr').length
							                    }).appendTo($(tr));
							                }
							            });
							        	
							            

							            
							            // add the new row
							            $(tr).appendTo($('#tab_logic'));
							        
							            $(tr).find("td button.row-remove").on("click", function() {
							                 $(this).closest("tr").remove();
							                 calculate_total();
							            });
							            
							            var  pd_name = document.getElementById("name"+newid);
							            var  pd_unit = document.getElementById("unit"+newid);
							           // var  pd_addition = document.getElementById("addition"+newid);
							            var  pd_price = document.getElementById("price"+newid); 
							            var  pd_quantity = document.getElementById("quantity"+newid); 
							            var  pd_sum = document.getElementById("sum"+newid); 
							            var  pd_id = document.getElementById("raw_id"+newid); 
							            var  pd_lan_flag = document.getElementById("lan_flag"+newid); 
				                    //  	var	 but_lan_flag = document.getElementById("but_lan_flag"+newid); 
			
							            	pd_name.value =  jsonObj[0].displayName;
							            	pd_unit.value = jsonObj[0].displayUnit;
							            	pd_price.value = parseFloat(jsonObj[0].pricePool).toFixed(2);
							            	pd_lan_flag.value = jsonObj[0].productLanFlag;
							     //       	but_lan_flag.value = jsonObj[0].productLanFlag;
							        
						            		pd_quantity.value = 1;
						            		pd_sum.value = pd_price.value ;
						            		pd_id.value = jsonObj[0].productID;
				                           
				                           
											calculate_total();
											pro_search_input.value="";
							}
						}
						
					}
					
					xmlhttp.open("POST", "fetch_product_data_background.jsp?product_id="+product_id, true);
					xmlhttp.send();
		}
		 // Dynamic Rows Code
        
        // Get max row id and set new id

		
	}
	
		
		function set_product_to_table(keyword){
			
			  var  table = $('#dataTables-example-product').DataTable();
			       table.clear();

			  var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
					//	alert("I'm Back");//////////////////
						
				
						var jsonObj = JSON.parse(xmlhttp.responseText);
					
						//alert("I'm Back");
						
						if(jsonObj.length == 0) {
						//	massage();
							//alert("Error Occer Can't get all customer list");
							
						}
						else{

							for(i in jsonObj) {
								$('#dataTables-example-product').DataTable().row.add([
	                              '<tr><td>'+jsonObj[i].nameTH+'</td>'
	                              ,'<td>'+jsonObj[i].nameEN+'</td>'
	                       //       ,'<td>'+jsonObj[i].category+'</td>'
	                              ,'<td><button id = "'+jsonObj[i].productID+'" type="button" class="btn gray-light btn-circle btn-md" onclick = "show_modal_edit_product_name(this.id)">'
	                              +'<i class="glyphicon glyphicon-wrench"></i></button> </td>'
	                              ,'<td><button id = "'+jsonObj[i].productID+'" type="button" class="btn btn-info btn-circle btn-md" onclick = "grap_product(this.id)">'
	                              +'<i class="glyphicon glyphicon-chevron-right"></i></button> </td></tr>'

	                              
	                                                          
	                            ]).draw();
							}
				
						}
						
						
						//System.out.println("temp_x:"+temp_x);	
					
					
					}// end if check state
				}// end function
				

				xmlhttp.open("POST", "get_product_by_keyword_background.jsp?keyword="+keyword, true);
				xmlhttp.send();

			
		}
		function init_group_option(){
			
			  var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
						
						var jsonObj = JSON.parse(xmlhttp.responseText);
						if(jsonObj.length == 0) {
							//	massage();
								alert("Error Can't get group_code_desc");
						}
						else{
						
						//	var sel_group_code = document.getElementById("sel_group_code");
							for(i in jsonObj) {
								
								$("#sel_group_code").append($('<option>', { 
								        value: jsonObj[i].code,
								        text : jsonObj[i].shortDescription 
								    }));
								
							}
							
							$("#sel_group_code").append($('<option>', { 
						        value: "ATC_PRD",
						        text : "ATC_PRD" 
						    }));
							
						
						}
						
					
					
					
					}// end if check state
				}// end function
				

				xmlhttp.open("POST", "get_group_code_description_background.jsp", true);
				xmlhttp.send();
			
			
			 
		}
		function search_cus_name() {	
			
			var cus_name_select = document.getElementById("cus_name");
			var keyword = cus_name_select.value;
	
		 // not using  
			if($('#namelist').find('option').length > 0) {
				
				$('#namelist').find('option').remove();
			
			}
			if(keyword != "") {
				
				/* AJAX */
				var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
						var jsonObj = JSON.parse(xmlhttp.responseText);
						
						if(jsonObj.length == 0) {
	
						//	alert("no  Data");
						}
						else{
							//alert("Have Data");
							
							$('#cus_lists').empty();
							
							
							for(i in jsonObj) {
								var text = jsonObj[i].companyId+","+jsonObj[i].nameTH+","+jsonObj[i].nameEN;
								var id = jsonObj[i].companyId ; 
								//alert(jsonObj[i].nameTH+" "+jsonObj[i].nameEN);
								var option = '<option id="'+id+'"  value="'+text+'" >' ;
								
								$('#cus_lists').append(option);
							}
							
						}
					}
					
				}
				
				xmlhttp.open("POST", "search_cus_name_background.jsp?keyword="+keyword, true);
				xmlhttp.send();
			}
		
		}

		function fetch_company_data(){
		//	alert();
			var cus_name = document.getElementById("cus_name");
			var cus_name_value = cus_name.value;
			var full_text = cus_name_value.split(",");
			
			var company_id = full_text[0];
			if(company_id != "") {
				
				/* AJAX */
				var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
						var jsonObj = JSON.parse(xmlhttp.responseText);
						
						if(jsonObj.length == 0) {
	
						//	alert("no  Data");
						}
						else{
					
							var inv_address = document.getElementById("inv_address");
							var inv_tax_id = document.getElementById("inv_tax_id"); 
							var company_id = document.getElementById("company_id_hidden");
							var credit = document.getElementById("credit");
							
								if((jsonObj[0].nameTH)=="-")
								{
									cus_name.value = jsonObj[0].nameEN;
									inv_address.value = jsonObj[0].addressEN; 		
								}else
								{
									cus_name.value = jsonObj[0].nameTH;
									inv_address.value = jsonObj[0].addressTH; 		
								}
								  
							
								
								
								inv_tax_id.value = jsonObj[0].taxID; 
								company_id.value = jsonObj[0].companyId;
								credit.value = jsonObj[0].credit;
					
							
						}
					}
					
				}
				
				xmlhttp.open("POST", "fetch_company_data_background.jsp?company_id="+company_id, true);
				xmlhttp.send();
			}
		}
		
		

		

		
		function create_quotation(but_id){
			
   		    $('#myModal').modal('show');
		   
			//alert("call fetch_cus_data :"+company_id);
			var checking ="";
			var quotation_main_id = document.getElementById('temp_quotation_id');
			var quotation_main_id_value = quotation_main_id.value;
			///////////////////////////////////////////
			var contact_name = document.getElementById('contact_name');
			var contact_name_value = contact_name.value ; 
			///////////////////////////////////////////
			var contact_tel = document.getElementById('contact_tel');
			var contact_tel_value = contact_tel.value ; 
			///////////////////////////////////////////
			var contact_email = document.getElementById('contact_email');
			var contact_email_value = contact_email.value ; 
			///////////////////////////////////////////
			var inv_name = document.getElementById('inv_name');
			var inv_name_value = inv_name.value ; 
			////////////////////////////////////////////////////
			var company_id = document.getElementById('company_id_hidden');
			var company_id_value = company_id.value;	
			////////////////////////////////////////////////////
			var customer_address = document.getElementById('inv_address');
			var customer_address_value = customer_address.value ; 
			////////////////////////////////////////////////////
			var customer_tax_id =  document.getElementById('inv_tax_id');
			var customer_tax_id_value = customer_tax_id.value;
			////////////////////////////////////////////////////
			
			var total_vat = document.getElementById("total_vat");
			var total_vat_value = total_vat.value;
			////////////////////////////////////////////////////
			var total_value = document.getElementById("total_value");
			var total_value_value = total_value.value;
			////////////////////////////////////////////////////
			var total_inc_vat = document.getElementById("total_inc_vat");
			var total_inc_vat_value = total_inc_vat.value;
			///////////////////////////////////////////////////
			var quotation_date = document.getElementById("quotation_date");
			var quotation_date_value = quotation_date.value;
			///////////////////////////////////////////////////
			var expire_date = document.getElementById("expire_date");
			var expire_date_value = expire_date.value;
			///////////////////////////////////////////////////
			
	

				
			var row_count = $('#tab_logic > tbody  > tr').length;
			//alert("row_count:"+row_count);
			
			
		
			
			//	alert(row_count+","+quotation_main_id.value+","+quotation_date.value);
			if((row_count>1)&&(quotation_main_id.value!="")&&(quotation_date.value!="")){	
				var pd_array = new Array();
	
					$('#tab_logic > tbody  > tr').each(function() {
						if(this.id=="addr0")
						{
							
						}else{
							
							var id ;
							if(this.id.length==5)
							{
								id = this.id.slice(-1);
							}else{
								id = this.id.slice(-2);
							}
							//alert("id:"+id);
							
							pd_array.push(id);		
						}
						
					});
				
					
					for(var i=0 ; i<pd_array.length ; i++){
						
						//alert(pd_array[i]);
					}
					
					var index =  parseInt(pd_array.length, 10);
					
					
					
					var parameter_cash_bill_main = "company_id="+company_id_value+
												"&quotation_main_id="+quotation_main_id_value+
												"&contact_name="+contact_name_value+
												"&contact_tel="+contact_tel_value+
												"&contact_email="+contact_email_value+
												"&total_vat="+total_vat_value+
												"&total_value="+total_value_value+
												"&total_inc_vat="+total_inc_vat_value+
												"&quotation_date="+quotation_date_value+
												"&expire_date="+expire_date_value+
												"&index="+index;
					
					var parameter_cash_bill_detail ="";
				//	alert("index:"+index);
					for(var j=0;j<index ;j++)
					{
				
						var pd_id = document.getElementById("raw_id"+pd_array[j]);
						var pd_name = document.getElementById("name"+pd_array[j]);
						var pd_price = document.getElementById("price"+pd_array[j]);
						var pd_quantity = document.getElementById("quantity"+pd_array[j]);
						var pd_sum = document.getElementById("sum"+pd_array[j]);
						var pd_unit = document.getElementById("unit"+pd_array[j]);
						//alert("pd_id:"+pd_id.value);
						/*
						if((pd_addition.value==null)||(pd_addition.value==""))
						{
							pd_addition_value="-";
						}else{
							pd_addition_value = pd_addition.value;
						}
						*/
						 parameter_cash_bill_detail =	parameter_cash_bill_detail +
								 						"&pd_id"+j+"="+pd_id.value+ 
														"&pd_name"+j+"="+pd_name.value+
				//										"&pd_addition"+j+"="+pd_addition_value+
														"&pd_price"+j+"="+pd_price.value+
														"&pd_quantity"+j+"="+pd_quantity.value+
														"&pd_sum"+j+"="+pd_sum.value+
														"&pd_unit"+j+"="+pd_unit.value;
										
					}

					var xmlhttp;
					
					if(window.XMLHttpRequest) {
							// code for IE7+, Firefox, Chrome, Opera, Safari
							xmlhttp = new XMLHttpRequest();
					}
					else {
							// code for IE6, IE5
							xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					
					xmlhttp.onreadystatechange = function() {     
					
						if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
								var result = xmlhttp.responseText;
								if(result=="error")
								{
									alert("Error Occer can't create Main Order Ticket")
								}
								else{


										setTimeout( function() { $('#myModal').modal('hide'); }, 1200);
										window.location = "dashboard_main.jsp";
									
								}
						
							}		
					}
					
				//	alert(parameter_cash_bill_detail);
					xmlhttp.open("POST", "create_quotation_full_background.jsp?"+parameter_cash_bill_main+parameter_cash_bill_detail, true);
					xmlhttp.send();
					
					
					
			}else{
				$('#myModal').modal('hide');
				alert("Some Parameter Missing");
				location.reload();
			  
			}
				
	
			
		}
		
		function SetShowCurrentData(){
    		
            var current_date = new Date();
            var c_year =  current_date.getFullYear();
            var int_year = parseInt(c_year,10) + parseInt(543,10) - parseInt(2500,10);
            
      		 var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
										
							var jsonObj = JSON.parse(xmlhttp.responseText);
							
							if(jsonObj.length == 0) {
		
								alert("no  Data");
							}
							else{
									//var text = jsonObj[i].nameTH+" "+jsonObj[i].nameEN;
									var init_credit_inv_no = String(jsonObj[0].initCreditInvNo);
									var add = 4 - init_credit_inv_no.length;
									
									for(var i=0;i<add;i++)
									{
										init_credit_inv_no = "0"+ init_credit_inv_no;
									}
									
									
								
								   // display inv_no
								   // document.getElementById("box_inv_credit_no").innerHTML = "("+int_year+"/"+init_credit_inv_no+")";
								
							}

		

							
					}// end if check state
				}// end function
				

				xmlhttp.open("POST", "get_current_both_inv_no_background.jsp", true);
				xmlhttp.send();

    	}
		
		function toggle_language(button){
			
			var language = button.firstChild.data ; 
			var company_id = document.getElementById("company_id_hidden").value;
			if(company_id != "") {
				
				/* AJAX */
				var xmlhttp;
				
				if(window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				}
				else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
						var jsonObj = JSON.parse(xmlhttp.responseText);
						
						if(jsonObj.length == 0) {
	
						}
						else{
							
							var inv_name = document.getElementById("inv_name");
							var inv_address = document.getElementById("inv_address");
							var inv_tax_id = document.getElementById("inv_tax_id"); 
							var company_id = document.getElementById("company_id_hidden");
						
							if(language == "TH")
							{
								inv_name.value = jsonObj[0].nameTH;
								inv_address.value = jsonObj[0].addressTH; 					
								inv_tax_id.value = jsonObj[0].taxID; 
								company_id.value = jsonObj[0].companyId;
								
								button.firstChild.data = "EN"; 
							}else{
								
								inv_name.value = jsonObj[0].nameEN;
								inv_address.value = jsonObj[0].addressEN; 					
								inv_tax_id.value = jsonObj[0].taxID; 
								company_id.value = jsonObj[0].companyId;
								button.firstChild.data = "TH"; 
							}
							
							
						}
					}
					
				}
				
				xmlhttp.open("POST", "fetch_company_data_background.jsp?company_id="+company_id, true);
				xmlhttp.send();
			}
			
		
			
		//	alert(company_id);
		}
		
		
		function calculate_sum(id_in)
		{
			

			var id ;
			if((id_in.length==6)||(id_in.length==9))
			{
				//alert("<10");
				id = id_in.slice(-1);
				
			}else{
				//alert("over 10");
				id = id_in.slice(-2);
			}

			var sum_price = 0;
			var pd_price = document.getElementById("price"+id);
			var pd_quantity = document.getElementById("quantity"+id);
			
				sum_price = pd_price.value * pd_quantity.value ; 
				
			var pd_sum = document.getElementById("sum"+id);
				pd_sum.value = parseFloat(sum_price).toFixed(2); 
			
			
				calculate_total();
				 
		}
		function calculate_total(){
			
			//alert("calculate_total");
			
			var tbody = document.getElementById("pd_tbody");
			var total_value = document.getElementById("total_value");
			var total_vat = document.getElementById("total_vat");
			
			var total_inc_vat = document.getElementById("total_inc_vat");
			var total_novat = 0 ;
				total_vat.value = 0 ;
				total_inc_vat.value = 0 ;
				total_value.value = 0;
				
			$('#tab_logic > tbody  > tr').each(function() {
				
					//alert(this.id);
					if(this.id=="addr0")
					{
		
					}else{
					//	alert(this.id);
						var id = this.id.substring(4);
							//alert(id);
						var sum = document.getElementById("sum"+id);
						
					
						
						 total_novat = parseFloat(total_novat, 10) + parseFloat(sum.value, 10 ) ; 
						 
						//alert("total_novat:"+total_novat+" sum.value:"+sum.value);
						
					}
				
			 });
			
			//need many scenario for testing 
			
			var total_vat_string = String((total_novat * 0.07) * 10 / 10);
			var total_inc_vat_string = String((total_novat * 1.07) * 10 / 10);
			
			var final_total_vat;
			var final_total_inc_vat;
			
			if(total_vat_string.indexOf(".")>0)
			{
				var total_vat_string_parts = total_vat_string.split('.');
				final_total_vat = total_vat_string_parts[0] +"."+ total_vat_string_parts[1].substring(0,2);
			}else{
				final_total_vat = total_vat_string;
			}
		
			
			if(total_inc_vat_string.indexOf(".")>0)
			{
				var total_inc_vat_string_parts = total_inc_vat_string.split('.');
				final_total_inc_vat = total_inc_vat_string_parts[0] +"."+ total_inc_vat_string_parts[1].substring(0,2);
			}
			else{
				final_total_inc_vat = total_inc_vat_string;
			}
		
			
			total_value.value = (parseFloat(final_total_inc_vat) - parseFloat(final_total_vat));
			total_vat.value =   parseFloat(final_total_vat);
			total_inc_vat.value =	parseFloat(final_total_inc_vat);
			
			
			
		}
		
		function toggle_product_language(button){
    		
    		var lan_flag = button.value ; 

		//	alert(button.id);
    		var prefix = button.id.substring(8);
    		var pd_id = document.getElementById("raw_id"+prefix); 
    		//alert(pd_id);
    		
    		
    		var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
				  //  $('#show_inv_history').modal('show');
					var jsonObj = JSON.parse(xmlhttp.responseText);

					if(jsonObj.length == 0) {
					//	massage();
						alert("Product not found !!");
					}
					else{
						//alert("get product detail");
						var pd_name = document.getElementById("name"+prefix);
						var pd_unit = document.getElementById("unit"+prefix);
						if(lan_flag=="EN")
						{
							pd_name.value = jsonObj[0].nameTH;
							pd_unit.value = jsonObj[0].unitTH;
							button.value = "TH";
						}else{
							pd_name.value = jsonObj[0].nameEN;
							pd_unit.value = jsonObj[0].unitEN;
							button.value = "EN";
						}
			
					}

				}
				
			}
			xmlhttp.open("POST", "get_product_detail_by_product_id_background.jsp?product_id="+pd_id.value, true);
			xmlhttp.send();

    		
    	}
		
		

function submit_edited_product(){
    		
			var name_th = document.getElementById("edited_pro_name_th");
			var unit_th = document.getElementById("edited_pro_unit_th");
			var name_en = document.getElementById("edited_pro_name_en");
			var unit_en = document.getElementById("edited_pro_unit_en");
			
			var product_id = document.getElementById("edited_product_id");
			
			
			var parameter = "product_id="+product_id.value+
							"&name_th="+name_th.value+
							"&unit_th="+unit_th.value+
							"&name_en="+name_en.value+
							"&unit_en="+unit_en.value;
			
			//alert(parameter);
			
			var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
				  //  $('#show_inv_history').modal('show');
					//var jsonObj = JSON.parse(xmlhttp.responseText);
					
					alert(xmlhttp.responseText);
					
					$('#editProModel').modal('hide');

				}
				
			}
			xmlhttp.open("POST", "update_product_name_background.jsp?"+parameter, true);
			xmlhttp.send();

    	}
    	function fetch_old_product_name_to_edit_modal(id){
    		
			var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {			
					
					var jsonObj = JSON.parse(xmlhttp.responseText);
					if(jsonObj.length == 0) {
						//	massage();
							alert("Product not found !!");
						}
						else{
							
							var name_th = document.getElementById("edited_pro_name_th");
							var unit_th = document.getElementById("edited_pro_unit_th");
							var name_en = document.getElementById("edited_pro_name_en");
							var unit_en = document.getElementById("edited_pro_unit_en");
							var edited_product_id = document.getElementById("edited_product_id");
							
								name_th.value = jsonObj.nameTH;
								name_en.value = jsonObj.nameEN;
								unit_th.value = jsonObj.unitTH;
								unit_en.value = jsonObj.unitEN;
								edited_product_id.value = jsonObj.productID;
							
						}
		

				}
				
			}
			xmlhttp.open("POST", "fetch_product_name_by_product_id_background.jsp?product_id="+id, true);
			xmlhttp.send();
    		
    		
    	}
	
		function calculate_expire_date(temp_date){
			
		
		     // wrong logic
			var quotation_date = new Date (temp_date);
			var new_date = new Date();
				new_date.setDate(quotation_date.getDate());
				new_date.setMonth(quotation_date.getMonth());
				new_date.setYear(quotation_date.getYear()+1900);
					
	
				
				var credit_int = parseInt("14", 10);
				

				new_date.setDate(quotation_date.getDate() + credit_int);
				//alert(new_date.getMonth()+1);
				//alert("new_date:"+new_date);
				var expire_date = document.getElementById("expire_date");
					//due_date.value = new_date;
					// format for set value is yyyy-mm-dd
				var expire_date_year =  parseInt(new_date.getYear()+1900, 10);
				var expire_date_month = parseInt(new_date.getMonth()+1, 10); 
				var expire_date_date = parseInt(new_date.getDate(), 10);
				
				var year = expire_date_year.toString();
				var month = expire_date_month.toString();
				var date = expire_date_date.toString();
				if(month.length==1)
				{
					month = "0" +month;
				}
				else{
					
				}
				if(date.length==1)
				{
					date = "0" +date;
				}
				else{
					
				}
				
				
			
				
				expire_date.value = ""+year+"-"+month+"-"+date;
				//alert(""+year+"-"+month+"-"+date);
	
				
		}
    	
		
    	function display_c(){
        	
    		var refresh=1000; // Refresh rate in milli seconds
    		mytime=setTimeout('display_ct()',refresh)
    	
    	}

    	function display_ct() {
    	
    		var strcount;
    		var x = new Date();
    		
    		document.getElementById('live_time').innerHTML = x.toString("dd MMMM yyyy ,HH:mm:ss ");
    		tt=display_c();
    	}

		function generate_quotation_no(){
			
			
			var xmlhttp;
			
			if(window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {			
					
					var code  = "Initial";
					
					code= xmlhttp.responseText;
					
					document.getElementById("temp_quotation_id").value = code;
		

				}
				
			}
			xmlhttp.open("POST", "generate_quotation_no_background.jsp", true);
			xmlhttp.send();
    		
			
		
			
		}
		
		function search_pro_name(temp){
		
				 var input_box = temp;
				 var keyword = input_box.value;
				
				 //alert("input_box:"+input_box.id);
				 var temp2 = (input_box.id).split("-");  // get row from input value
				 
				 var row = temp2[1];
				 
		
				 if(keyword != "") {
						
					
						var xmlhttp;
						
						if(window.XMLHttpRequest) {
							// code for IE7+, Firefox, Chrome, Opera, Safari
							xmlhttp = new XMLHttpRequest();
						}
						else {
							// code for IE6, IE5
							xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
						}
						
						xmlhttp.onreadystatechange = function() {
							if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {						
								var jsonObj = JSON.parse(xmlhttp.responseText);
								
								if(jsonObj.length == 0) {
			
								//	alert("no  Data");
								}
								else{
									console.log("Have Data with:"+jsonObj.length);

									
									$('#pro_lists-'+row).empty();
									
									
									for(i in jsonObj) {
									//	var text = jsonObj[i].productID+","+jsonObj[i].nameTH+","+jsonObj[i].nameEN;
										var text = jsonObj[i].nameTH+","+jsonObj[i].nameEN;
										
											text = text.replace(/[&\/\\#,+$~%.'":*?<>{}]/g,"");
										var id = jsonObj[i].productID ; 
										//alert(jsonObj[i].nameTH+" "+jsonObj[i].nameEN);
										var option = '<option id="'+id+'"  value="'+i+":"+text+'" >' ;
										
										$('#pro_lists-'+row).append(option);
										
										console.log(i+":"+text);
									}
									
								}
							}
							
						}
						
						xmlhttp.open("POST", "get_product_by_keyword_background.jsp?keyword="+keyword, true);
						xmlhttp.send();
					}
				

		}
		
		function fetch_pro_data(temp){
			  
				alert("fetch_pro_data");
				
				var input_value = (temp.value).split(",");
				var product_id = input_value[0];
				var temp_get_row = (temp.id).split("-");
				var row = temp_get_row[1];

		}
		
		function add_new_row(){
			
			
			
		}
	
	</script>
	


</head>

<body>

    <div id="wrapper">

    		<nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>
                <div class="sidebar-header">
                    <h3>Bored Sidebar</h3>
                </div>
                
    
        		<div id="includedSidebar"></div>
        		
        	</nav>



        <!-- Navigation -->
       

        <div id="content" class="form-control">
        
        	   <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-justify"></i>
                                <span></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Page</a></li>
                       
                            </ul>
                        </div>
                    </div>
                </nav>
                
            <div class="row">
                <div class="col-lg-12">
                 
                    <div class="col-md-2">
                  	  <input type="text" id="temp_quotation_id" class="form-control" placeholder="NQ201XXXXXXX">
                  	  <br>
                    </div>
                    <div class="col-md-2">
                      <button type="button" class="btn btn-primary" onclick="generate_quotation_no()">Generate Quotation No.</button>
                     </div>

                
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
   
            <div class="row">
           
            
                    <div class="col-lg-12">
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
		                         <h4 class="panel-title">
							          <a data-toggle="collapse" href="#customer_panel" aria-expanded="false"> Customer Panel &nbsp;&nbsp; &nbsp; - </a>
							     </h4>
	                           
	                        </div>
	                        
	                        <!-- /.panel-heading -->
	                         <div id="customer_panel" class="collapse list-unstyled in">
			                        <div class="panel-body">
			                        		<button type="button" class="btn btn-default pull-right" onclick="toggle_language(this)">EN</button> <br>
			                        		<br>
			                        	<div class="col-lg-4">
					                        	     <label>Customer Name (ชื่อลูกค้า)</label>
					                           		
					                           		
					                           		
					                           		 <input list="cus_lists" class="form-control" id="cus_name" name="cus_name" 
					                           		 		value="" placeholder="Customer Name" onkeyup="search_cus_name()" onchange="fetch_company_data()"  >
						                              <datalist  id="cus_lists" >    
															
												      </datalist>
																		                        				
					                           	
					                           		 <input id ="company_id_hidden" name="company_id_hidden" type="hidden" value="" >
					                           		  <br>
					                           		 <input class="form-control" id="inv_tax_id" name = "inv_tax_id" value="" placeholder="Tax ID">
					                           		 <br>
					                           		 
					                           	 	<label>Quotation Date : (Use B.C. Format)  </label>
			                          		  		<input type="date" id="quotation_date" name ="quotation_date" class="form-control" oninput="calculate_expire_date(this.value)">  
			                          		  		<br>
			                           	</div>
			                           	<div class="col-lg-4">
			                        	<label>Address (ที่อยู่)</label>
			                        		 <textarea id="inv_address" name ="inv_address" class="form-control" rows="4" ></textarea>
			                        	 	 <br>
			                        	 	 	<label>Expire Date : </label>	       
			                       		  		<input type="date" id="expire_date" name ="expire_date" class="form-control" >  	
			                        	</div>
			                        
			                      					 <div class="col-lg-4">
			                     
			                        		                 						
			                        			
			                        					 	<input id="contact_name" name ="contact_name" class="form-control" placeholder="Contact Name">
			                        						<br>
			                        			
			                        					 	<input id="contact_tel" name ="contact_tel" class="form-control" placeholder="Contact Tel">
			                       							<br>
			                        			
			                        					 	<input type="email" id="contact_email" name ="contact_email" class="form-control" placeholder="Contact Email">
			                        					 	<br>
			                        					 	<input type="number" id="credit" name ="credit" class="form-control" placeholder="Credit">
			                        					 	
			                        					</div>
			             
			                        		<br><br>
			                          </div>
	          				 </div>
	                          <div class="row">
	                          		<div class="col-lg-6">
	                          				<br>
	                          				  <input list="pro_lists-0" type="text" name='pd_name-0' id = "pd_name-0" class="form-control"
																    		onkeyup="search_pro_name(this)"
																    		onchange="" />
											  <datalist id="pro_lists-0"></datalist>
	                          				
	                          				
	                          		
	                          		</div>
	                          		<div class="col-lg-2">
	                          			<br>
	                          			 <button type="button" class="btn btn-primary pull-left" onclick="grap_product()"><b>+</b> Add new row</button>
	                          		</div>
	                          		
	                          		<div class="col-lg-10">
	                          			
	                          			
	                          			
	                          		</div>
	                          		
	                          </div>
	                          <br>
	                          <div class="row">
	                          		 <div class="col-lg-12">
													<table class="table table-bordered table-hover table-sortable" id="tab_logic" onchange="calculate_total()">
														<thead>
															<tr>
																<th class="text-center">
																	Product Name
																</th>
																<th class="text-center" style="width: 90px;">
																	Unit
																</th>
																<!-- 
																<th class="text-center">
																	Addition 
																</th>
																 -->
																<th class="text-center" style="width: 110px;">
																	Price
																</th>
										    					<th class="text-center" style="width: 90px;">
																	Quantity
																</th>
																<th class="text-center" style="width: 130px;">
																	Sum
																</th>
																
										        	
																
																<th class="text-center" style="width: 90px;">
																	
																</th>
																<th class="text-center" style="width: 90px;">
																</th>
																
															</tr>
														</thead>
														<tbody id = "pd_tbody">
														
										    				<tr id='addr0' data-id="0" class="hidden">
																<td data-name="name">
																    <input type="text" name='pd_name' id = "pd_name" class="form-control"/>
																</td>
																<td data-name="unit">
																    <input type="text" name='pd_unit' id = "pd_unit" class="form-control" style="text-align:center;" />
																</td>
																
																<!-- 
																<td data-name="addition">
																    <input type="text" name='pd_addition' id="pd_addition" placeholder='Color , Perfume Code , etc' class="form-control"/>
																</td>
																 -->
																 
																<td data-name="price">
																    <input type ="text" name = 'pd_price' id="pd_price" class="form-control text-right" oninput="calculate_sum(this.id)" >
																</td>
										    					<td data-name="quantity">
																    <input type ="number" name = 'pd_quantity' id="pd_quantity" class="form-control text-right" oninput="calculate_sum(this.id)" >
																</td>
																<td data-name="sum">
																	 <input readonly type="text" name='pd_sum' id = "pd_sum" class="form-control text-right" onchange="calculate_total()"/>
																	 
																</td>
																
																<td data-name="lan_flag">
																	<input type="button" id="but_lan_flag" class="btn btn-default btn-md pull-right" onclick="toggle_product_language(this)" value="XX"> <br>
																	
																	<input type="hidden" id="pd_lan_flag" >
																	 
																</td>
																
																
																<td data-name="raw_id" class="hidden">
																	<input type="hidden" name="pd_id" id="pd_id">
																</td>
																 
																
										                        <td data-name="del">
										                            <button name="del0" class='btn btn-danger glyphicon glyphicon-remove row-remove' ></button>
										                        </td>
										                        
															</tr>
														</tbody>
													</table>	
													
													<table class="table table-bordered table-hover table-sortable " id="table_summary" style="border:none;">
													
																<tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Total Value : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input  type="text" name="total_value" id="total_value" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>

														 <tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Vat (7%) : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input  type="text" name="total_vat" id="total_vat" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>
												
															<tr class=" text-right" style="border:none;">															
										    					<td class=" col-lg-8 text-right" style="border:none;">
														      			 <label class="text-right" style="font-size: 17px;">Total Inc VAT : </label>
														      	</td>
																<td class=" col-lg-6 text-right" style="border:none;">
																
																	  <div class="pull-right">
								
																			<input  type="text" name="total_inc_vat" id="total_inc_vat" class="form-control text-right "
																		  		   style="border: 0; font-size: 17px; " placeholder="THB">
																 	 </div>
																</td>
															</tr>
															
															<tr>
																<td class=" col-lg-6 text-right" style="border:none;">
														      			 
														      	</td>
														      		
																 <td class=" col-lg-6 text-right" style="border:none;">
																	<div class="btn-group">
																		  <button type="button" id="gen_inv" class="btn btn-primary btn-md"  onclick="create_quotation(this.id)">Submit Quotation</button>
																		  <button type="button" class="btn btn-primary dropdown-toggle btn-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																		    <span class="caret"></span>
																		    <span class="sr-only">Toggle Dropdown</span>
																		  </button>
																		  <ul class="dropdown-menu">
																		    <li><a id="nogen_inv" onclick="create_quotation(this.id)" href="#"> Save</a></li>
																		    
																
																  </ul>
																	</div>
																</td>
															</tr>
													
													</table>
										</div>                       
	                        </div>
	                        <!-- .panel-body -->
	                    </div>
                    <!-- /.panel -->
                </div>

                <!-- /.col-lg-6 -->
            </div>
            


            <!-- /.row -->
        </div>

	<form id="form_file_path_gen" method="get" action="../DownloadServlet">
		 <input id="inv_file_name" name="inv_file_name" type="hidden">		
		 <input id="inv_file_path" name="inv_file_path" type="hidden">		
		
	</form>	     
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
                  
                         <div class="modal-dialog">
                          	 <div class="col-sm-6 col-sm-offset-3 text-center">
									 <div class="container">
													<div class="row">
														
												            <div id="loading">
												                <ul class="bokeh">
												                    <li></li>
												                    <li></li>
												                    <li></li>
												                </ul>
													            </div>
													</div>
									</div>
									
								</div>
 
                          </div>
                     
                          <!-- /.modal-content -->
             
                      <!-- /.modal-dialog -->
      </div>
      <div class="modal fade" id="createCusModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style=" position: fixed; top: 10%; left: 10%; right: 10%; bottom: 15% ">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Create New Customer</h4>
                                        </div>
                                             <div class="modal-body">
                                        	 <input name="new_cus_tax_id" id="new_cus_tax_id" type="text" class="form-control" placeholder="เลขประจำตัวผู้เสียภาษี">
                                        	  <br>
                                             <input name="new_cus_name_th" id="new_cus_name_th" type="text" class="form-control" placeholder="ชื่อบริษัท(ภาษาไทย)">
                                              <br>
                                             <input name="new_cus_name_en" id="new_cus_name_en" type="text" class="form-control" placeholder="ชื่อบริษัท(ภาษาอังกฤษ)">
                                              <br>
                                             <textarea name="new_cus_add_th_line1" id="new_cus_add_th_line1" class="form-control" rows="2" placeholder="ที่อยู่(ภาษาไทย) line1" ></textarea>
                                              <br>
                                              <textarea name="new_cus_add_th_line2" id="new_cus_add_th_line2" class="form-control" rows="2" placeholder="ที่อยู่(ภาษาไทย) line2" ></textarea>
                                              <br>
                                              <textarea name="new_cus_add_en_line1" id="new_cus_add_en_line1" class="form-control" rows="2" placeholder="ที่อยู่(ภาษาอังกฤษ) line1" ></textarea>
                                              <br>
                                               <textarea name="new_cus_add_en_line2" id="new_cus_add_en_line2" class="form-control" rows="2" placeholder="ที่อยู่(ภาษาอังกฤษ) line2" ></textarea>
                                              <br>
                                              <input type="number" name="new_cus_credit" id="new_cus_credit" class="form-control" placeholder="Credit (Day)" >
                                              
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" name="submit_create_customer"  id="submit_create_customer" class="btn btn-primary" onclick="create_single_customer()">Create Customer</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
      </div>
      <div class="modal fade" id="createProModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style=" position: fixed; top: 10%; left: 10%; right: 10%; bottom: 15% ">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Create New Product</h4>
                                        </div>
                                        <div class="modal-body">
                                       		<div class="row">
                                       			.	<div class="col-md-6">
                                       					<input name="new_pro_name_th" id="new_pro_name_th" type="text" class="form-control" placeholder="ชื่อภาษาไทย">
                                       				</div>
                                       				<div class="col-md-4">
                                       					<input name="new_pro_unit_th" id="new_pro_unit_th" type="text" class="form-control" placeholder="หน่วยภาษาไทย">
                                       				</div>
                                       				
                                       		</div>
                                       		<div class="row">
                                       				<br>
                                       				<div class="col-md-6">
                                       					<input name="new_pro_name_en" id="new_pro_name_en" type="text" class="form-control" placeholder="Name Eng">
                                       				</div>
                                       				<div class="col-md-4">
                                       					<input name="new_pro_unit_en" id="new_pro_unit_en" type="text" class="form-control" placeholder="Unit Eng">
                                       				</div>
                                       				<br>
                                       		
                                       		</div>
                                       		<div class=row>
                                       			<br>
                                       			<div class="col-md-4">
	                                       			<select id="sel_category" name="sel_category" class="form-control" onchange="change_category(this)">
	                                       				 <option value="OTH_PRD">Other Product</option>
	 													 <option value="ATC_PRD">ATC Product</option>
	                                       			</select>
	                                       		</div>
	                                       		<div class="col-md-5">
	                                       			<select id="sel_group_code" name="sel_group_code" class="form-control">
	                                       				  
	                                       			</select>
	                                       		</div>
                                       		</div>
                                       		<div class="row">                                      			
                                       			 <div class="col-md-4">
                                       			  <br>
                                       			 <input type="number" name="new_pro_price" id="new_pro_price" class="form-control" placeholder="Price (no VAT)">
                                       			</div>
                                       		</div>
                                              
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" name="submit_create_product"  id="submit_create_product" class="btn btn-primary" onclick="create_single_product()">Create Product</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
      </div>
      <div class="modal fade" id="editProModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style=" position: fixed; top: 10%; left: 10%; right: 10%; bottom: 15% ">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Edit Product Name</h4>
                                        </div>
                                        <div class="modal-body">
                                       		<div class="row">
                                       			.	<div class="col-md-6">
                                       					<input name="edited_pro_name_th" id="edited_pro_name_th" type="text" class="form-control" placeholder="ชื่อภาษาไทย">
                                       				</div>
                                       				<div class="col-md-4">
                                       					<input name="edited_pro_unit_th" id="edited_pro_unit_th" type="text" class="form-control" placeholder="หน่วยภาษาไทย">
                                       				</div>
                                       				
                                       		</div>
                                       		<div class="row">
                                       				<br>
                                       				<div class="col-md-6">
                                       					<input name="edited_pro_name_en" id="edited_pro_name_en" type="text" class="form-control" placeholder="Name Eng">
                                       				</div>
                                       				<div class="col-md-4">
                                       					<input name="edited_pro_unit_en" id="edited_pro_unit_en" type="text" class="form-control" placeholder="Unit Eng">
                                       					<input type="hidden" name="edited_product_id" id="edited_product_id">
                                       				</div>
                                       				<br>
                                       		
                                       		</div>                                              
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" name="submit_edit_product_but"  id="submit_edit_product_but" class="btn btn-primary" onclick="submit_edited_product()">Submit Change</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
      </div>
      
    </div> 
    
    <!-- /#wrapper -->

    <!-- jQuery  -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
     <script src="../dist/js/dataTables.tableTools.js"></script>
    

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script src="../js/date.js"></script>
     <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
  		function fetch_customer_to_input_box(){
  			
  			alert();
  		}  
    
    
    	function create_single_customer(){
    		
    		$('#createCusModel').modal('hide');
			var parameter = "";    		
			var new_cus_tax_id = document.getElementById("new_cus_tax_id").value;
    		var new_cus_name_th = document.getElementById("new_cus_name_th").value;
    		var new_cus_name_en = document.getElementById("new_cus_name_en").value;
    		var new_cus_add_th_line1 = document.getElementById("new_cus_add_th_line1").value;
    		var new_cus_add_th_line2 = document.getElementById("new_cus_add_th_line2").value;
    		var new_cus_add_en_line1 = document.getElementById("new_cus_add_en_line1").value;  		
    		var new_cus_add_en_line2 = document.getElementById("new_cus_add_en_line2").value;  		
    		var new_cus_credit = document.getElementById("new_cus_credit").value;
    		
    		
    		 $('#myModal').modal('show');
    			setTimeout( function() { }, 2000);
    		 
    		
    		var checking ="Warning:";
    		
    		if((new_cus_name_th=="")&&(new_cus_add_en=""))
    		{
    			checking = checking + "Customer Name TH and EN are NULL ";
    		}
    		
    		if(new_cus_tax_id=="")
    		{
    			checking = checking + "Tax ID is NULL";
    		}
 			if(checking=="Warning:")
 			{
 				//alert("Ready");
 				parameter = "name_th="+new_cus_name_th
				   +"&name_en="+new_cus_name_en
				   +"&address_th_line1="+new_cus_add_th_line1
				   +"&address_th_line2="+new_cus_add_th_line2
				   +"&address_en_line1="+new_cus_add_en_line1
				   +"&address_en_line2="+new_cus_add_en_line2
				   +"&tax_id="+new_cus_tax_id
				   +"&credit="+new_cus_credit
				   +"&type="+"customer";
		
    			/* AJAX */
    			var xmlhttp_2;
    			
    			if(window.XMLHttpRequest) {
    				// code for IE7+, Firefox, Chrome, Opera, Safari
    				xmlhttp_2 = new XMLHttpRequest();
    			}
    			else {
    				// code for IE6, IE5
    				xmlhttp_2 = new ActiveXObject("Microsoft.XMLHTTP");
    			}
    			
    			xmlhttp_2.onreadystatechange = function() {  				
    				if(xmlhttp_2.readyState == 4 && xmlhttp_2.status == 200) {						
    				
    					
    					var result = xmlhttp_2.responseText;
    					
   						if(result=="fail")
   						{
   							alert("Error Occer contact Admin");
   						}
   						else {
   							$('#myModal').modal('hide');
   							alert("Success to Create Customer");
   							set_customer_to_table();
   							
   						}
    					
    				}
    				
    			}
    			xmlhttp_2.open("POST", "create_single_company_background.jsp?"+parameter, true);
    			xmlhttp_2.send();
 				
 				
 				
 			}else{
 				
 				$('#myModal').modal('hide');
 				alert(checking);
 			}
    		
    	}
    	function create_single_product(){
    		
    		$('#createProModel').modal('hide');
    		var parameter = "";    		
    		var new_pro_name_th = document.getElementById("new_pro_name_th").value;
    		var new_pro_unit_th = document.getElementById("new_pro_unit_th").value;
    		var new_pro_name_en = document.getElementById("new_pro_name_en").value;
    		var new_pro_unit_en = document.getElementById("new_pro_unit_en").value;
    		
    		if(new_pro_name_th=="")
    		{
    			new_pro_name_th = "-";
    		}
    		if(new_pro_unit_th=="")
    		{
    			new_pro_unit_th = "-";
    		}
    		
    		if(new_pro_name_en=="")
    		{
    			new_pro_name_en = "-";
    		}
    		if(new_pro_unit_en=="")
    		{
    			new_pro_unit_en = "-";
    		}
    			
    			
    		var init_price = document.getElementById("new_pro_price").value;
    		
    		var sel_category = document.getElementById("sel_category");
    		var sel_group_code = document.getElementById("sel_group_code");
    		
    		var new_pro_category = sel_category.options[sel_category.selectedIndex].value;
    		var new_pro_group_code = sel_group_code.options[sel_group_code.selectedIndex].value;
    		/*
    		alert("data:"+"\n"+new_pro_name_th+"\n"
    			 + new_pro_unit_th+"\n"
    			 + new_pro_name_en+"\n"
    			 + new_pro_unit_en+"\n"
    			 + new_pro_category+"\n"
    			 + new_pro_group_code+"\n"
    			 + init_price);
    		*/
    		 $('#myModal').modal('show');
 			setTimeout( function() { }, 2000);
    		
			var checking ="Warning:";
    		
    		if((new_pro_unit_th=="")&&(new_pro_name_en=""))
    		{
    			checking = checking + "Product Name TH and EN are NULL ";
    		}
    		
    		if(init_price=="")
    		{
    			checking = checking + "Init price  is NULL";
    		}
 			if(checking=="Warning:")
 			{
 				//alert("Ready");
 				parameter = "new_pro_name_th="+new_pro_name_th
				   +"&new_pro_unit_th="+new_pro_unit_th
				   +"&new_pro_name_en="+new_pro_name_en
				   +"&new_pro_unit_en="+new_pro_unit_en
				   +"&new_pro_category="+new_pro_category
				   +"&new_pro_group_code="+new_pro_group_code
				   +"&init_price="+init_price;
		
    			/* AJAX */
    			var xmlhttp_2;
    			
    			if(window.XMLHttpRequest) {
    				// code for IE7+, Firefox, Chrome, Opera, Safari
    				xmlhttp_2 = new XMLHttpRequest();
    			}
    			else {
    				// code for IE6, IE5
    				xmlhttp_2 = new ActiveXObject("Microsoft.XMLHTTP");
    			}
    			
    			xmlhttp_2.onreadystatechange = function() {  				
    				if(xmlhttp_2.readyState == 4 && xmlhttp_2.status == 200) {						
    				
						var result = xmlhttp_2.responseText;
    					
   						if(result=="fail")
   						{
   							alert("Error Occer contact Admin");
   						}
   						else {
   							$('#myModal').modal('hide');
   							//alert("I'm Back from create_single_product_background SUCCESS");
   							set_product_to_table();
   						}

    				}
    				
    			}
    			alert(parameter);
    			
    			xmlhttp_2.open("POST", "create_single_product_background.jsp?"+parameter, true);
    			xmlhttp_2.send();
 				
 				
 				
 			}else{
 				
 				$('#myModal').modal('hide');
 				alert(checking);
 			}
    		
    		
    		
    	}
    	
    
    	function show_modal_new_customer(){
    	//	alert("show_modal_new_customer");
    		$('#createCusModel').modal('show');
    		
    	}
    	
    	
	    function show_modal_new_product(){
	    	$('#createProModel').modal('show');
	    	
	    }
    	function show_modal_edit_product_name(id){
    		
    		fetch_old_product_name_to_edit_modal(id);
    		$('#editProModel').modal('show');
    		
    	}

    	function matchCustom(params, data) {
    	    // If there are no search terms, return all of the data
    	    if ($.trim(params.term) === '') {
    	      return data;
    	    }

    	    // Do not display the item if there is no 'text' property
    	    if (typeof data.text === 'undefined') {
    	      return null;
    	    }

    	    // `params.term` should be the term that is used for searching
    	    // `data.text` is the text that is displayed for the data object
    	    if (data.text.indexOf(params.term) > -1) {
    	      var modifiedData = $.extend({}, data, true);
    	      modifiedData.text += ' (matched)';

    	      // You can return modified objects from here
    	      // This includes matching the `children` how you want in nested data sets
    	      return modifiedData;
    	    }

    	    // Return `null` if the term should not be displayed
    	    return null;
    	}
    
    	function log (name, evt) {
 				alert(name+":"+evt);
    		}

    	

 
    </script>
    
    
    
    <script>
    $(document).ready(function() {
    
  	
    	
    	$("#includedSidebar").load("sidebar.jsp"); 
    	
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').fadeOut();
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        
        ////////////////////////
        
 
        
        ///////////////////////////////////////////////////////////
    //	init_group_option();
    	SetShowCurrentData();
    	var table_cus = $('#dataTables-example-customer').DataTable({
    	      responsive: true,
              "aLengthMenu": [[25, 50, 75, -1], [1, 3, 5]],
              "iDisplayLength": 3 ,
              lengthChange: false 
              ,
              "sDom": 'T<"clear">lfrtip' ,
              "oTableTools": {
                      "aButtons": [
	                                   {
	                                       "sExtends":    "text",
	                                       "fnClick": function ( nButton, oConfig, oFlash ) {
	                                          	show_modal_new_customer();
	                                       },
	                                       "sButtonText": "<i class='fa glyphicon-plus'></i>",
	                                        "sExtraData": [ 
	                                                            { "name":"operation", "value":"downloadcsv" }       
	                                                      ]
	                                    
	                                   }
                     			 ]
                  }
             
        });
        
     	 $("#input_search_prd").on('keyup', function (e) {
      		  var value = document.getElementById("input_search_prd").value;
      	     if (e.keyCode == 13) {
      		
      			set_product_to_table(value);
      	     }
     		});
      	  
         	 $('#dataTables-example-product').DataTable({
                responsive: true,
                "aLengthMenu": [[25, 50, 75, -1], [1, 3, 5]],
                "bFilter": false,
                "iDisplayLength": 5 ,
                lengthChange: false 

      	    });
        
    	//set_customer_to_table();
     //	set_product_to_table();
        display_ct();
    	    
        // Sortable Code
        var fixHelperModified = function(e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
        
            $helper.children().each(function(index) {
                $(this).width($originals.eq(index).width())
            });
            
            return $helper;
        };
                                           
        $(".table-sortable tbody").sortable({
            helper: fixHelperModified      
        }).disableSelection();

        $(".table-sortable thead").disableSelection();

		
    });
    </script>

</body>

</html>
