<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>
<%@ page import = "java.text.DateFormat" %>



<% 

	System.out.println("Start get_product_relate_vendor_history_background");
			//set Database Connection
			String hostProps = "";
			String usernameProps  = "";
			String passwordProps  = "";
			String databaseProps = "";
			
			try {
				//get current path
				ServletContext servletContext = request.getSession().getServletContext();
				
				InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
				Properties props = new Properties();
				
				props.load(input);
			 
				hostProps  = props.getProperty("host"); 
				usernameProps  = props.getProperty("username");
				passwordProps  = props.getProperty("password");
				databaseProps = props.getProperty("database");
				
				System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
						"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
				
			} catch (Exception e) { 
				out.println(e);  
			}
			
			// connect database
			Connection connect = null;		
			try {
				Class.forName("com.mysql.jdbc.Driver");
			
				connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
						"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
			
				if(connect != null){
					System.out.println("Database Connect Sucesses."); 
				} else {
					System.out.println("Database Connect Failed.");	
				}
			
			} catch (Exception e) {
				out.println(e.getMessage());
				e.printStackTrace();
			}

		
	List<ProductPurchaseHistory> pph_list = new ArrayList<ProductPurchaseHistory>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	//String purchase_id = request.getParameter("purchase_id");
	//System.out.println("purchase_id:"+purchase_id);
	 String vendor_id = request.getParameter("vendor_id");

	 System.out.println("vendor_id:"+vendor_id);



      try{
    	  
    
    	
       		String	sql_query =  " SELECT *  "+
       							 " FROM purchase_detail "+
       							 " JOIN purchase_main "+
       							 " ON purchase_main.purchase_id = purchase_detail.purchase_id "+
       							 " JOIN product "+
       							 " ON purchase_detail.product_id = product.product_id "+
       							 " WHERE purchase_main.vendor_id = '"+vendor_id+"' ";



    
    	  System.out.println("sql_query:"+sql_query);
    	  
    	  
    	  ResultSet rs_pph = connect.createStatement().executeQuery(sql_query);
          
    	  
    	 
    	  
    	  while(rs_pph.next())
          {
    		  ProductPurchaseHistory pph = new ProductPurchaseHistory();
    		  
    		  pph.setInvoiceNo(rs_pph.getString("inv_no"));
    		  pph.setProductId(rs_pph.getString("product_id"));
    		  pph.setPrice(rs_pph.getString("price"));
    		  pph.setNameEN(rs_pph.getString("name_en"));
    		  pph.setNameTH(rs_pph.getString("name_th"));
    		  pph.setUnitEN(rs_pph.getString("unit_en"));
			  pph.setUnitTH(rs_pph.getString("unit_th"));
			  pph.setQuantity(rs_pph.getString("quantity"));
			  pph.setInvDate(rs_pph.getString("invoice_date"));
    		  pph_list.add(pph);
            } 
              
          
             
         	try {
				Json = mapper.writeValueAsString(pph_list); 
				System.out.println("Json:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	
         	
         	out.print(Json);
	   
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  out.print("fail");
      }
     
  
	connect.close();
%>
