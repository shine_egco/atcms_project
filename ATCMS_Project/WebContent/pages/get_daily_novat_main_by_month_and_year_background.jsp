<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>
<%@ page import = "java.text.DateFormat" %>



<% 

	System.out.println("Start get_daily_novat_main_by_month_and_year_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

	List<DailyNovat> dn_bill_list = new ArrayList<DailyNovat>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	
	//String purchase_id = request.getParameter("purchase_id");
	//System.out.println("purchase_id:"+purchase_id);
	 String month = request.getParameter("month");
	 String year = request.getParameter("year");
	 
	 System.out.println("month:"+month);
	 System.out.println("year:"+year);
	 


      try{
    	  
    	  String sql_query = "";
    	  

	   			 
   			sql_query =  " SELECT *"+
						"  FROM daily_novat_main "+
						"  WHERE MONTH(daily_novat_main.bill_date)='"+month+"'" +
						"  AND YEAR(daily_novat_main.bill_date)='"+year+"'"+
						"  ORDER BY daily_novat_main.index  ASC";


    		 
   		 
   		 
    	  
    
    	  System.out.println("sql_query:"+sql_query);
    	  
    	  
    	  ResultSet rs_ord = connect.createStatement().executeQuery(sql_query);
          
    	  
    	  
    	  
    	  while(rs_ord.next())
          {
              DailyNovat dn = new DailyNovat();
              dn.setBillDate(rs_ord.getString("bill_date"));
              dn.setDailyNovatId(rs_ord.getString("daily_novat_id"));
              dn.setIndex(rs_ord.getString("index"));
              dn.setTotalCost(rs_ord.getString("total_cost"));
              dn.setTotalProfit(rs_ord.getString("total_profit"));
              dn.setTotalValue(rs_ord.getString("total_value"));
              dn.setNameRef(rs_ord.getString("name_ref"));
              dn.setContactRef(rs_ord.getString("contact_ref"));
         

       		dn_bill_list.add(dn);
          } 
              
          
             
         	try {
				Json = mapper.writeValueAsString(dn_bill_list); 
				System.out.println("Json:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	
         	
         	out.print(Json);
	   
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  out.print("fail");
      }
     
  
	connect.close();
%>
