<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.Locale" %>



<% 

	System.out.println("Start get_credit_inv_detail_by_product_id_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

	List<OrderDetail> result_list = new ArrayList<OrderDetail>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	
	String product_id = request.getParameter("product_id");
	
      try{
    	  
    	  String sql_query  = " 	   SELECT order_detail.order_id  "+
    				    		       "    , order_detail.price      "+
	    				    		   "    , order_detail.product_id "+
    				    			   "    , order_main.delivery_date AS inv_date "+
	    				    		   "    , order_main.inv_no  "+
    				    			   "    , company.name_th AS com_th "+
    				    			   "    , company.name_en AS com_en "+
    				    			   "    , product.unit_th AS unit_th "+
    				    			   "    , product.unit_en AS unit_en "+
    				    			   "    , order_detail.quantity  "+
    				    			   "    , order_detail.sum "+
    				      		 	 " FROM order_detail "+
    				    	         " JOIN order_main "+
    						         " ON  order_detail.order_id = order_main.order_id "+
    				    	         " JOIN company "+
									 " ON  order_main.customer_id = company.company_id "+
    				    	         " JOIN product "+
									 " ON  product.product_id = order_detail.product_id "+          
		    						 " WHERE BINARY  order_detail.product_id = '"+product_id+"'" +
		    					     " AND order_main.order_id != 'INIT_ORDER_ID' "+
		    						 " ORDER BY order_main.inv_no ";
    	  
    	  System.out.println("sql_query :"+sql_query);
    	  ResultSet rs = connect.createStatement().executeQuery(sql_query);
          
    	
    	  
    	  while(rs.next())
          {
    		  OrderDetail or_de = new OrderDetail();
 			  
    		  or_de.setOrderId(rs.getString("order_id"));
    		  or_de.setPrice(rs.getString("price"));

    		  String inv_date_old_str = rs.getString("inv_date");
    		//  Date date = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH).parse(inv_date_old_str);	  
    		 // String inv_date_new_str =  new SimpleDateFormat("dd MMM yyyy",Locale.ENGLISH).format(date);
    		 
    		  or_de.setInvoiceDate(inv_date_old_str);
    		  or_de.setInvoiceNo(rs.getString("inv_no"));
    		  or_de.setQuantity(rs.getString("quantity"));
    		  or_de.setSum(rs.getString("sum"));
    		  
    		  if(("-").equals(rs.getString("com_en")))
    		  {
    			  or_de.setCustomerName(rs.getString("com_th"));	
    			  
    		  }else{
    			  or_de.setCustomerName(rs.getString("com_en"));	
    		  }
    		  
    		  if(("-").equals(rs.getString("unit_en")))
    		  {
    			  or_de.setProductUnit(rs.getString("unit_th"));	
    			  
    		  }else{
    			  or_de.setProductUnit(rs.getString("unit_en"));	
    		  }
    		  

    		  result_list.add(or_de);
              
          }
             
         	try {
				Json = mapper.writeValueAsString(result_list); 
				System.out.println("Json:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	out.print(Json);
	   
	 
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  connect.close();
    	  out.print("fail");
      }
     
    System.out.println("SUCCESS");
	connect.close();
%>
