<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.util.regex.Pattern" %>
<%@ page import = "java.text.DateFormat" %>



<% 

	System.out.println("Start get_cash_bill_main_by_month_and_year_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}

	List<CashBill> cash_inv_list = new ArrayList<CashBill>();
	String Json = "";
	ObjectMapper mapper = new ObjectMapper();
	
	//String purchase_id = request.getParameter("purchase_id");
	//System.out.println("purchase_id:"+purchase_id);
	 String month = request.getParameter("month");
	 String year = request.getParameter("year");
	 
	 System.out.println("month:"+month);
	 System.out.println("year:"+year);
	 


      try{
    	  
    	  String sql_query = "";
    	  

	   			 
   			sql_query =  " SELECT cash_bill_main.inv_no "+
					            ", company.name_th "+
								", company.name_en "+
							    ", cash_bill_main.total_value "+
						        ", cash_bill_main.total_vat "+
					            ", cash_bill_main.total_inc_vat "+
							    ", cash_bill_main.total_cost "+
							    ", cash_bill_main.total_profit "+
					            ", cash_bill_main.cash_bill_id "+
					            ", cash_bill_main.status "+
							     ", cash_bill_main.bill_date "+
						"  FROM cash_bill_main "+
						"  JOIN company "+
						"  ON  cash_bill_main.customer_id = company.company_id "+
						"  WHERE MONTH(cash_bill_main.bill_date)='"+month+"'" +
						"  AND YEAR(cash_bill_main.bill_date)='"+year+"'"+
						"  ORDER BY cash_bill_main.inv_no  ASC";


    		 
   		 
   		 
    	  
    
    	  System.out.println("sql_query:"+sql_query);
    	  
    	  
    	  ResultSet rs_ord = connect.createStatement().executeQuery(sql_query);
          
    	  
    	  
    	  
    	  while(rs_ord.next())
          {
              CashBill cb = new CashBill();
          			    cb.setInvNo(rs_ord.getString("inv_no"));
         	  if(rs_ord.getString("name_th").equals("-"))
              {
         		 cb.setCustomerName(rs_ord.getString("name_en"));
         	  }else{
         		 cb.setCustomerName(rs_ord.getString("name_th"));
         	  }
         	 cb.setTotalIncVat(rs_ord.getString("total_inc_vat"));
         	 cb.setTotalCost(rs_ord.getString("total_cost"));
         	 cb.setTotalProfit(rs_ord.getString("total_profit"));
         	 cb.setTotalValue(rs_ord.getString("total_value"));
         	 cb.setTotalVat(rs_ord.getString("total_vat"));
         	  
         	cb.setStatus(rs_ord.getString("status"));
       		cb.setCashBillId(rs_ord.getString("cash_bill_id"));
       		cb.setBillDate(rs_ord.getString("bill_date"));
       				
       		cash_inv_list.add(cb);
          } 
              
          
             
         	try {
				Json = mapper.writeValueAsString(cash_inv_list); 
				System.out.println("Json:"+Json);
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			} catch (JsonMappingException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         	
         	
         	out.print(Json);
	   
	     
      }
      catch(Exception x){
    	  System.out.println(x);
    	  out.print("fail");
      }
     
  
	connect.close();
%>
