<%@page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.atcms.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page language="java" import="java.lang.*" %>
<%@ page import = "java.sql.SQLException" %>
<%@ page import = "java.util.List" %>
<%@page import="java.util.ArrayList"%>
<%@ page import = "org.codehaus.jackson.JsonGenerationException" %>
<%@ page import = "org.codehaus.jackson.map.JsonMappingException" %>
<%@ page import = "org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import = "java.io.IOException" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.hssf.usermodel.HSSFDateUtil" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.util.Date" %>	
<%@ page import = "java.util.Properties" %>
<%@ page import = "java.io.InputStream" %>
<%@ page import = "java.sql.PreparedStatement" %>


<% 

	System.out.println("Start create_single_product_background");
	//set Database Connection
	String hostProps = "";
	String usernameProps  = "";
	String passwordProps  = "";
	String databaseProps = "";
	
	try {
		//get current path
		ServletContext servletContext = request.getSession().getServletContext();
		
		InputStream input = servletContext.getResourceAsStream("/properties/connectDB.properties");
		Properties props = new Properties();
		
		props.load(input);
	 
		hostProps  = props.getProperty("host"); 
		usernameProps  = props.getProperty("username");
		passwordProps  = props.getProperty("password");
		databaseProps = props.getProperty("database");
		
		System.out.println("Checking"+"jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
		
	} catch (Exception e) { 
		out.println(e);  
	}
	
	// connect database
	Connection connect = null;		
	try {
		Class.forName("com.mysql.jdbc.Driver");
	
		connect =  DriverManager.getConnection("jdbc:mysql://" + hostProps  + "/" + databaseProps +
				"?user=" + usernameProps  + "&password=" + passwordProps + "&characterEncoding=tis620");
	
		if(connect != null){
			System.out.println("Database Connect Sucesses."); 
		} else {
			System.out.println("Database Connect Failed.");	
		}
	
	} catch (Exception e) {
		out.println(e.getMessage());
		e.printStackTrace();
	}


     /////////////////Data Parameter////////////////
                  
     String new_pro_name_th = request.getParameter("new_pro_name_th");
     String new_pro_unit_th = request.getParameter("new_pro_unit_th"); 
     String new_pro_name_en = request.getParameter("new_pro_name_en");
     String new_pro_unit_en = request.getParameter("new_pro_unit_en"); 
     String new_pro_category = request.getParameter("new_pro_category"); 
     String new_pro_group_code = request.getParameter("new_pro_group_code"); 
     String init_price = request.getParameter("init_price"); 
     
     String product_id ; 
     if(new_pro_category.equals("ATC_PRD"))
     {
    	 product_id = "ATC"+KeyGen.generateProductID();
     }else{
    	 product_id = "OTH"+KeyGen.generateProductID();
     }
     
     System.out.println("new_pro_name_th:"+new_pro_name_th);
     System.out.println("new_pro_unit_th:"+new_pro_unit_th);
     System.out.println("new_pro_name_en:"+new_pro_name_en);
     System.out.println("new_pro_unit_en:"+new_pro_unit_en);
     System.out.println("new_pro_category:"+new_pro_category);
     System.out.println("new_pro_group_code:"+new_pro_group_code);
     System.out.println("init_price:"+init_price);
     /////////////////////////////////////////////////
     

      try{
    	  
    	  /*
    	  System.out.println("Phase 1 ");
    	  System.out.println("Checking Existing Product Name ?");
    	  String sql_check_product_name = " SELECT *"+
    	  								 " FROM product"+
										 " WHERE product.name_th='"+new_pro_name_th+"'"+
    	  								 " OR product.name_en='"+new_pro_name_en+"'";
    	  System.out.println("sql_check_product_name:"+sql_check_product_name);
    	  ResultSet rs_check_product_name =  connect.createStatement().executeQuery(sql_check_product_name);
    	  rs_check_product_name.last();
    	  
    	  if(rs_check_product_name.getRow() == 1) {
 
    		  System.out.println("Existing Product Name:"+new_pro_name_th+","+new_pro_name_en);
    		  throw new Exception();
    		  
    	  }else{
    		  
    		  */
    		  String description = "-";
    		  String name_pool = "";
    		  String pcode_name= "";
    		     
           	String sql_create_prd = " INSERT INTO `product`(`product_id` "+
           									  ", `name_th` "+
           									  ", `name_en` "+
           									  ", `description`"+
           									  ", `name_pool`"+
           									  ", `unit_en`"+
           									  ", `unit_th`"+
           									  ", `pcode_name`"+
           									  ", `category`"+
           									  ", `group_code`)"+
           				" VALUES ('"+product_id+"'"+
           						 ",'"+new_pro_name_th+"'"+
           						 ",'"+new_pro_name_en+"'"+
           						 ",'"+description +"'"+
           						 ",'"+name_pool+"'"+
           					     ",'"+new_pro_unit_en+"'"+
           						 ",'"+new_pro_unit_th+"'"+
           					     ",'"+pcode_name+"'"+
           						 ",'"+new_pro_category+"'"+
           					     ",'"+new_pro_group_code+"')"; 
           	
           	System.out.println("sql_create_prd:"+sql_create_prd);
           	PreparedStatement pstmt = connect.prepareStatement(sql_create_prd, Statement.RETURN_GENERATED_KEYS);  
			pstmt.executeUpdate();  
			ResultSet keys = pstmt.getGeneratedKeys();    
			keys.next();  
			int key = 0;
			key = keys.getInt(1);
			System.out.println("Adding Success with product inddex:"+key);
			if(key!=0)
			{
				String order_id = "INIT_ORDER_ID";
				String order_type = "VAT";
				int quantity = 1;
				
				String sql_order_detail = "INSERT INTO `order_detail`(" +
																	  "  `order_id`"+
																	  ", `product_id`"+
																	  ", `price`"+
																	  ", `type` "+
																	  ", `quantity`"+
																	  ", `sum`)"+
										  " VALUES ('"+order_id+"'"+
												   ",'"+product_id+"'"+
												   ",'"+init_price+"'"+
												   ",'"+order_type+"'"+
												   ", "+quantity+" "+
												   ",'"+init_price+"')";
																	  	
				System.out.println("sql_order_detail:"+sql_order_detail);
				connect.createStatement().executeUpdate(sql_order_detail);	
				
			}else{
				
				System.out.println("Error when initial order date ");
			}
    		  
    	//  } end else
    	  
   
      }
      catch(Exception x){
    	  System.out.println(x);
    	  connect.close();
    	  out.print("fail");
      }
     
    
    out.print("success");
	connect.close();
%>
