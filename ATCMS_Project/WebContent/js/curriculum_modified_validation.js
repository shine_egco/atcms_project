var backEventListener = null;

var unregister = function() {
    if ( backEventListener !== null ) {
        document.removeEventListener( 'tizenhwkey', backEventListener );
        backEventListener = null;
        window.tizen.application.getCurrentApplication().exit();
    }
}

//Initialize function
var init = function () {
	console.log("init() called");
    // register once
    if ( backEventListener !== null ) {
        return;
    }
    
    console.log("init() called");
    
    var backEvent = function(e) {
        if ( e.keyName == "back" ) {
            try {
                if ( $.mobile.urlHistory.activeIndex <= 0 ) {
                    // if first page, terminate app
                    unregister();
                } else {
                    // move previous page
                    $.mobile.urlHistory.activeIndex -= 1;
                    $.mobile.urlHistory.clearForward();
                    window.history.back();
                }
            } catch( ex ) {
                unregister();
            }
        }
    }
    
    // add eventListener for tizenhwkey (Back Button)
    document.addEventListener( 'tizenhwkey', backEvent );
    backEventListener = backEvent;
    
    validate();
};

window.onload = init;
$(document).unload( unregister );

var validate = function(){	
	
		
	var CurriculumNameTH = new LiveValidation('nameCurriculumTH',{validMessage: "Valid!"});
	CurriculumNameTH.add(Validate.Presence);
	
	var CurriculumNameEN = new LiveValidation('nameCurriculumEN',{validMessage: "Valid!"});
	CurriculumNameEN.add(Validate.Presence);
	
	var Year = new LiveValidation('year',{validMessage: "Valid!"});
	Year.add(Validate.Presence);
	
	var BachelorNameTH = new LiveValidation('nameDegreeTH',{validMessage: "Valid!"});
	BachelorNameTH.add(Validate.Presence);
	
	var BachelorNameEN = new LiveValidation('nameDegreeEN',{validMessage: "Valid!"});
	BachelorNameEN.add(Validate.Presence);
	
	/*var DepartmentId = new LiveValidation('DepartmentId',{validMessage: "Valid!"});
	DepartmentId.add(Validate.Presence);
	DepartmentId.add(Validate.Numeriality);*/
	var Modified = new LiveValidation('modified',{validMessage: "Valid!"});
	Modified.add(Validate.Presence);
	
	var Credit = new LiveValidation('credit',{validMessage: "Valid!"});
	Credit.add(Validate.Presence);
	Credit.add(Validate.Numeriality);	
	
	/*var Coorganizer = new LiveValidation('Coorganizer',{validMessage: "Valid!"});
	Coorganizer.add(Validate.Presence);*/
	
	/*var Degree = new LiveValidation('Degree',{validMessage: "Valid!"});
	Degree.add(Validate.Presence);*/
	
	/*var Program = new LiveValidation('Program',{validMessage: "Valid!"});
	Program.add(Validate.Presence);*/
	
	var OHCE = new LiveValidation('OHCE',{validMessage: "Valid!"});
	OHCE.add(Validate.Presence);
	
	var ONEC = new LiveValidation('ONEC',{validMessage: "Valid!"});
	ONEC.add(Validate.Presence);
	
	/*var EngApp = new LiveValidation('EngApp',{validMessage: "Valid!"});
	EngApp.add(Validate.Presence);*/
	

};