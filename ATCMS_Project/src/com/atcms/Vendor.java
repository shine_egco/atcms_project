
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class Vendor {

	private String name_th ;
	private String name_en ;
	private String address ; 
	private String code_name ;
	private String address_th ; 
	private String address_en ; 
	private String tax_id ; 
	private String company_id ;
	private String credit;
	private String email;
	
	public String getNameTH() {return name_th; }
	public String getNameEN() {return name_en; }
	public String getAddress() {return address; }
	public String getAddressTH() {return address_th; }
	public String getAddressEN() { return address_en; }
	public String getTaxID() {return tax_id ; }
	public String getCompanyId() {return company_id; }
	public String getCodeName() {return code_name; }
	public String getCredit(){return credit;}
	public String getEmail(){return email;}
	
	public void setNameTH(String in ) {this.name_th = in;}
	public void setNameEN(String in ) {this.name_en = in;}
	public void setAddress(String in ) {this.address = in;}
	public void setAddressTH (String in) {this.address_th = in;}
	public void setAddressEN (String in) {this.address_en = in;}
	public void setTaxID (String in) {this.tax_id = in;}
	public void setCompanyId (String in) {this.company_id = in;}
	public void setCodeName (String in) {this.code_name = in ;}
	public void setCredit (String in){this.credit = in;}
	public void setEmail (String in){this.email = in;}


}
