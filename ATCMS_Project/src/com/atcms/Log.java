
package com.atcms;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Log {
	
	public static void writeAdminFile(String logText) throws IOException {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",java.util.Locale.US);
		Calendar calendar = Calendar.getInstance();
		String current = sdf.format(calendar.getTime());
		
		String log = current + " " + logText;
		
		//File creation
	
		String strPath = "C:\\admin_log.txt";
		File strFile = new File(strPath);
		boolean fileCreated = strFile.createNewFile();
		if(fileCreated) {			
			try(PrintWriter fout = new PrintWriter(new BufferedWriter(new FileWriter(strFile, true)))) {
			   fout.println(log);
			}catch (IOException e) {
			    //exception handling left as an exercise for the reader
			}
		}
		else {
			//File appending			
			try(PrintWriter fout = new PrintWriter(new BufferedWriter(new FileWriter(strFile, true)))) {
			    fout.println(log);
			}catch (IOException e) {
			    //exception handling left as an exercise for the reader
			}
		}	
		
	}
	
public static void writeUserFile(String logText) throws IOException {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",java.util.Locale.US);
		Calendar calendar = Calendar.getInstance();
		String current = sdf.format(calendar.getTime());
		
		String log = current + " " + logText;
		
		//File creation
		String strPath = "C:\\user_log.txt";
		File strFile = new File(strPath);
		boolean fileCreated = strFile.createNewFile();
		if(fileCreated) {			
			try(PrintWriter fout = new PrintWriter(new BufferedWriter(new FileWriter(strFile, true)))) {
			   fout.println(log);
			}catch (IOException e) {
			    //exception handling left as an exercise for the reader
			}
		}
		else {
			//File appending			
			try(PrintWriter fout = new PrintWriter(new BufferedWriter(new FileWriter(strFile, true)))) {
			    fout.println(log);
			}catch (IOException e) {
			    //exception handling left as an exercise for the reader
			}
		}	
		
	}
	
}
