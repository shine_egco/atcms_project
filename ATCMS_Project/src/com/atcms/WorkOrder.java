
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class WorkOrder {

	
	private String formula_id ;
	private String product_id;
	private String pro_name_th ;
	private String pro_name_en;
	private String form_name_th;
	private String form_name_en;
	private String full_unit;
	private String batch_number;
	
	private String quantity;
	private String sum_cost;
	private String unit_cost;
	

	public String getFormulaID() {return formula_id;}
	public String getProductID(){return product_id;}
	public String getProNameTH (){return pro_name_th;}
	public String getProNameEN(){return pro_name_en;}
	public String getFormNameTH(){return form_name_th;}
	public String getFormNameEN(){return form_name_en;}
	public String getFullUnit(){return full_unit;}
	public String getBatchNumber() {return batch_number;}
	
	public String getQuantity(){return quantity;}
	public String getSumCost(){return sum_cost;}
	public String UnitCost(){return unit_cost;}

	
	public void setFormulaID(String in) { this.formula_id = in;}
	public void setProductID(String in) {this.product_id = in ;}
	public void setProNameTH(String in){this.pro_name_th = in;}
	public void setProNameEN(String in){this.pro_name_en = in;}
	public void setFormNameTH(String in) {this.form_name_th = in;}
	public void setFormNameEN (String in) {this.form_name_en = in;}
	public void setFullUnit(String in) {this.full_unit = in;}
	public void setBatchNumber(String in) {this.batch_number = in;}
	
	public void setQuantity(String in) {this.quantity = in;}
	public void setSumCost(String in) {this.sum_cost = in;}
	public void setUnitCost(String in) {this.unit_cost = in;}

	

}
