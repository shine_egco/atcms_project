
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class InitialParameter {

	private int init_credit_inv_no ;
	private int init_cash_inv_no ;

	public int getInitCreditInvNo() {return init_credit_inv_no; }
	public int getInitCashInvNo() {return init_cash_inv_no; }


	public void setInitCreditInvNo(int in ) {this.init_credit_inv_no = in;}
	public void setInitCashInvNo(int in ) {this.init_cash_inv_no = in;}


}
