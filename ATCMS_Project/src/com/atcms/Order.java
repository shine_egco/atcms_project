

package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class Order {

	private String order_id ;
	private String customer_id ;
	private String customer_name ; 
	private String customer_address;
	private String order_status ;
	private String type ;
	private String order_date;
	private String ponum ; 
	private String completed_date;
	private String delivery_date;
	private String due_date;
	private String create_date;
	private String total_value;
	private String total_vat;
	private String total_inc_vat;
	private String inc_generated_date;
	private String inv_no;
	private String inv_file_path;
	private String inv_file_name;
	private String compute_credit ;
	private String tax_id ;
	private String status;
	private String note;
	private String payment_ref;
	private String total_cost;
	private String total_profit;
	private String avg_profit_percentage;
	

	
	public String getOrderId() {return order_id; }
	public String getCustomerId() {return customer_id; }
	public String getCustomerName() {return customer_name; }
	public String getCustomerAddress() {return customer_address;}
	public String getOrderStatus() {return order_status; }
	public String getType(){ return type;}
	public String getOrderDate() {return order_date;}
	public String getPoNum() {return ponum;}
	public String getCompletedDate(){return completed_date;}
	public String getDeliveryDate(){return delivery_date;}
	public String getDueDate(){return due_date;}
	public String getCreateDate() {return create_date;}
	public String getTotalValue() {return total_value;}
	public String getTotalVat(){return total_vat;}
	public String getTotalIncVat() {return total_inc_vat;}
	public String getIncGeneratedDate() {return inc_generated_date;}
	public String getInvNo() {return inv_no;}
	public String getInvFilePath() {return inv_file_path;}
	public String getInvFileName() {return inv_file_name;}
	public String getComputeCredit() {return compute_credit;}
	public String getTaxId(){return tax_id;}
	public String getStatus(){return status;}
	public String getNote() {return note;}
	public String getPaymentReference(){return payment_ref;}
	public String getTotalCost() {return total_cost;}
	public String getTotalProfit (){return total_profit;}
	public String getAvgProfitPercentage(){return avg_profit_percentage;}
	
	public void setOrderId(String in ) {this.order_id = in;}
	public void setCustomerId(String in ) {this.customer_id = in;}
	public void setCustomerName(String in ) {this.customer_name = in;}
	public void setCustomerAddress(String in) {this.customer_address = in;}
	public void setOrderStatus (String in) {this.order_status = in;}
	public void setType(String in){ this.type=in;}
	public void setOrderDate(String in) {this.order_date=in;}
	public void setPoNum(String in) {this.ponum=in;}
	public void setCompletedDate(String in){this.completed_date=in;}
	public void setDeliveryDate(String in){this.delivery_date=in;}
	public void setDueDate(String in){this.due_date=in;}
	public void setCreateDate(String in) {this.create_date=in;}
	public void setTotalValue(String in) {this.total_value=in;}
	public void setTotalVat(String in){this.total_vat=in;}
	public void setTotalIncVat(String in) {this.total_inc_vat=in;}
	public void setIncGeneratedDate(String in) {this.inc_generated_date=in;}
	public void setInvNo(String in) {this.inv_no=in;}
	public void setInvFilePath(String in){this.inv_file_path=in;}
	public void setInvFileName(String in){this.inv_file_name=in;}
	public void setComputeCredit(String in){this.compute_credit=in;}
	public void setTaxId(String in) {this.tax_id=in;}
	public void setStatus (String in){this.status = in;}
	public void setNote(String in){this.note = in;}
	public void setPaymentReference(String in){this.payment_ref = in;}
	public void setTotalCost(String in) {this.total_cost =in;}
	public void setTotalProfit (String in){this.total_profit = in;}
	public void setAvgProfitPercentage(String in) {this.avg_profit_percentage = in;}

}
