
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class Quotation {

	private String quotation_id ;
	private String customer_id ;
	private String customer_name ; 
	private String customer_address;
	
	private String contact_name;
	private String contact_tel;
	private String contact_email;
	private String quotation_no;
	
	private String status ;
	private String type ;
	private String expire_date;
	private String create_date;
	private String quotation_date;
	private String total_value;
	private String total_vat;
	private String total_inc_vat;
	private String inv_file_path;
	private String inv_file_name;
	private String tax_id;
	private String completed_date;
	private String total_cost;
	private String total_profit;
	private String credit;
	
	
	public String getQuotationId() {return quotation_id; }
	public String getCustomerId() {return customer_id; }
	public String getCustomerName() {return customer_name; }
	public String getCustomerAddress() {return customer_address;}
	
	public String getContactName(){return contact_name;}
	public String getContactTel(){return contact_tel;}
	public String getContactEmail(){return contact_email;}
	public String getQuotationNo(){return quotation_no;}
	public String getCredit() {return credit;}
	
	
	
	public String getStatus() {return status; }
	public String getType(){ return type;}
	public String getExpireDate() {return expire_date;}
	public String getCreateDate() {return create_date;}
	public String getQuotationDate() {return quotation_date;}
	public String getTotalValue() {return total_value;}
	public String getTotalVat(){return total_vat;}
	public String getTotalIncVat() {return total_inc_vat;}
	public String getInvFilePath() {return inv_file_path;}
	public String getInvFileName() {return inv_file_name;}
	public String getTaxId(){return tax_id;}
	public String getCompletedDate(){return completed_date;}
	public String getTotalCost() {return total_cost;}
	public String getTotalProfit (){return total_profit;}
	
	public String getCustomerID(){ return customer_id;}
	
	
	public void setQuotationId(String in ) {this.quotation_id = in;}
	public void setCustomerId(String in ) {this.customer_id = in;}
	public void setCustomerName(String in ) {this.customer_name = in;}
	public void setCustomerAddress(String in) {this.customer_address = in;}
	
	public void setContactName(String in) {this.contact_name = in;}
	public void setContactTel(String in) {this.contact_tel = in;}
	public void setContactEmail(String in) {this.contact_email = in;}
	public void setQuotationNo(String in) {this.quotation_no = in;}
	
	
	public void setStatus (String in) {this.status = in;}
	public void setType(String in){ this.type=in;}
	public void setExpireDate(String in) {this.expire_date=in;}
	public void setCreateDate(String in) {this.create_date=in;}
	public void setQuotationDate(String in) {this.quotation_date=in;}

	public void setTotalValue(String in) {this.total_value=in;}
	public void setTotalVat(String in){this.total_vat=in;}
	public void setTotalIncVat(String in) {this.total_inc_vat=in;}
	public void setInvFilePath(String in){this.inv_file_path=in;}
	public void setInvFileName(String in){this.inv_file_name=in;}
	public void setTaxId(String in){this.tax_id = in;}
	public void setCompletedDate(String in){this.completed_date = in;}
	public void setTotalCost(String in) {this.total_cost =in;}
	public void setTotalProfit (String in){this.total_profit = in;}
	
	public void setCustomerID (String in){this.customer_id = in;}
	public void setCredit (String in) {this.credit = in;}
   
}
