
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class MonthlySummaryReport {

	private String index ;
	private String report_id ;
	private String month ; 
	private String year;
	private String date_of_validation ;
	private String all_credit_inv_total_sum ;
	private String all_credit_inv_total_profit;
	private String current_credit_inv_total_sum ; 
	private String current_credit_inv_total_profit;
	private String cash_inv_total_sum;
	private String cash_inv_total_profit;
	private String daily_novat_total_sum;
	private String daily_novat_total_profit;
	private String direct_bill_total;
	private String indirect_bill_total;
	private String credit_note_total_sum;
	private String credit_note_total_profit;
	private String status;
	

	

	
	public String getIndex() {return index; }
	public String getReportID() {return report_id; }
	public String getMonth() {return month; }
	public String getYear() {return year;}
	public String getDateOfValidation() {return date_of_validation; }
	
	public String getAllCreditInvTotalSum(){ return all_credit_inv_total_sum;}
	public String getAllCreditInvTiotalProfit() {return all_credit_inv_total_profit;}
	public String getCurrentCreditInvTotalSum() {return current_credit_inv_total_sum;}
	public String getCurrentCreditInvTotalProfit() {return current_credit_inv_total_profit;}
	
	public String getCashInvTotalSum() {return cash_inv_total_sum;}
	public String getCashInvTotalProfit(){return cash_inv_total_profit;}
	
	public String getDailyNovatTotalSum() {return daily_novat_total_sum;}
	public String getDailyNovatTotalProfit() {return daily_novat_total_profit;}
	
	public String getDirectBillTotal() {return direct_bill_total;}
	public String getIndirectBillTotal() {return indirect_bill_total;}
	
	public String getCreditNoteTotalSum() {return credit_note_total_sum;}
	public String getCreditNoteTotalProfit(){return credit_note_total_profit;}
	
	public String getStatus(){return status;}

	

	
	
	
	public void setIndex(String in ) {this.index = in;}
	public void setReportID(String in ) {this.report_id = in;}
	public void setMonth(String in ) {this.month = in;}
	public void setYear(String in) {this.year = in;}
	public void setDateOfValidation (String in) {this.date_of_validation = in;}
	
	public void setAllCreditInvTotalSum(String in){ this.all_credit_inv_total_sum=in;}
	public void setAllCreditInvTotalProfit(String in) {this.all_credit_inv_total_profit=in;}
	public void setCurrentCreditInvTotalSum(String in) {this.current_credit_inv_total_sum=in;}
	public void setCurrentCreditInvTotalProfit(String in) {this.current_credit_inv_total_profit=in;}
	
	public void setCashInvTotalSum(String in) {this.cash_inv_total_sum=in;}
	public void setCashInvTotalProfit(String in){this.cash_inv_total_profit=in;}
	
	public void setDailyNovatTotalSum(String in) {this.daily_novat_total_sum=in;}
	public void setDailyNovatTotalProfit(String in) {this.daily_novat_total_profit=in;}
	
	public void setDirectBillTotal(String in) {this.direct_bill_total=in;}
	public void setIndirectBillTotal(String in) {this.indirect_bill_total=in;}
	
	public void setCreditNoteTotalSum(String in){this.credit_note_total_sum=in;}
	public void setCreditNoteTotalProfit(String in){this.credit_note_total_profit=in;}
	
	public void setStatus(String in){this.status = in;}

}
