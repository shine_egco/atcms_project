
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class ProductSalesVolume {

	private String product_id;
	private String formula_id;
	private String name_th ;
	private String name_en ;
	private String full_unit;
	private String total_volume;
	
	private String capacity;
	private String net_capacity;
	private String capacity_unit;
	private String packaging_unit;
	
	

	public String getProductID(){return product_id;}
	public String getFormulaID(){return formula_id;}
	public String getNameTH() {return name_th; }
	public String getNameEN() {return name_en; }
	public String getFullUnit(){return full_unit;}
	public String getTotalVolume (){return total_volume;}
	public String getCapacity(){return capacity;}
	public String getNetCapacity(){return net_capacity;}
	public String getCapacityUnit() {return capacity_unit;}
	public String getPackagingUnit(){return packaging_unit;}

	

	public void setProductID(String in){ this.product_id = in;}
	public void setFormulaID(String in){this.formula_id = in;}
	public void setNameTH(String in ) {this.name_th = in;}
	public void setNameEN(String in ) {this.name_en = in;}
	public void setFullUnit(String in) {this.full_unit = in ;}
	public void setTotalVolume(String in) {this.total_volume = in;}
	public void setCapacity(String in) {this.capacity = in;}
	public void setNetCapacity (String in) {this.net_capacity = in;}
	public void setCapacityUnit(String in) {this.capacity_unit = in;}
	public void setPackagingUnit(String in) {this.packaging_unit = in;}


}
