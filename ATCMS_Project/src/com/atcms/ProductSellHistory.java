
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class ProductSellHistory {

	private String invoice_ref_id ;
	private String customer_id ;
	private String customer_name ; 
	private String order_status ;
	private String price ; 
	private String type ;
	private String adt_description ; 
	private String quantity;
	private String sum;
	private String product_unit;
	private String product_id;
	private String unit_th;
	private String unit_en;
	private String use_unit;
	
	private String name_th ;
	private String name_en ;
	private String inv_date;
	private String cost_per_unit;


	
	private String inv_no;

	private String bill_date;
	

	
	public String getInvoiceRefID() {return invoice_ref_id; }
	public String getCustomerId() {return customer_id; }
	public String getCustomerName() {return customer_name; }
	public String getOrderStatus() {return order_status; }
	public String getPrice() {return price;}
	public String getType() {return type;}
	public String getAdtDescription(){return adt_description;}
	public String getQuantity(){ return quantity;}
	public String getSum(){ return sum;}
	public String getProductId(){return product_id;}
	public String getProductUnit(){ return product_unit;}
	
	public String getUnitTH () {return unit_th;}
	public String getUnitEN () {return unit_en;}
	public String getUseUnit () {return use_unit;}
	public String getNameTH() {return name_th; }
	public String getNameEN() {return name_en; }
	public String getInvDate(){ return inv_date;}
	
	public String getInvNo(){return inv_no;}

	public String getBillDate(){return bill_date;}
	public String getCostPerUnit() {return cost_per_unit;}

	

	
	public void setInvoiceRefID(String in ) {this.invoice_ref_id = in;}
	public void setCustomerId(String in ) {this.customer_id = in;}
	public void setCustomerName(String in ) {this.customer_name = in;}
	public void setOrderStatus (String in) {this.order_status = in;}
	public void setPrice(String in) {this.price=in;}
	public void setType(String in) {this.type=in;}
	public void setAdtDescription(String in){this.adt_description=in;}
	public void setQuantity(String in){this.quantity=in;}
	public void setSum(String in){ this.sum=in;}
	public void setProductId (String in) {this.product_id = in;}
	public void setProductUnit(String in){this.product_unit=in;}
	
	public void setUnitTH (String in) {this.unit_th = in;}
	public void setUnitEN (String in) {this.unit_en = in;}
	public void setUseUnit (String in) {this.use_unit = in;}
	public void setNameTH(String in ) {this.name_th = in;}
	public void setNameEN(String in ) {this.name_en = in;}
    public void setInvDate(String in) {this.inv_date = in;}
	public void setCostPerUnit(String in) {this.cost_per_unit = in;}

	public void setInvoiceNo(String in){this.inv_no = in;}

	public void setBillDate (String in) {this.bill_date = in;}
	

	
}
