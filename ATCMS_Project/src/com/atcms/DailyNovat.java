
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class DailyNovat {

	private String daily_novat_id;
	private String bill_date;
	private String total_value;
	private String index;
	private String note;
	private String total_cost;
	private String total_profit;
	
	private String contact_ref;
	private String name_ref;
	private String avg_profit_percentage ;
	


	public String getTotalValue() {return total_value;}
	public String getDailyNovatId () {return daily_novat_id;}
	public String getBillDate () {return bill_date;}
	public String getIndex() {return index;}
	public String getNote(){return note;}
	public String getTotalCost(){return total_cost;}
	public String getTotalProfit (){return total_profit;}
	public String getContactRef(){return contact_ref;}
	public String getNameRef(){return name_ref;}
	public String getAvgProfitPercentage(){return avg_profit_percentage;}
	
	public void setTotalValue(String in) {this.total_value=in;}
	public void setDailyNovatId(String in) {this.daily_novat_id=in;}
	public void setBillDate(String in ){this.bill_date = in;}
	public void setIndex(String in) {this.index = in;}
	public void setNote (String in) {this.note = in;}
	public void setTotalCost(String in) {this.total_cost = in;}
	public void setTotalProfit (String in) {this.total_profit = in;}
	public void setNameRef(String in) {this.name_ref = in;}
	public void setContactRef(String in) {this.contact_ref = in;}
	public void setAvgProfitPercentage (String in){this.avg_profit_percentage = in;}
	
	 

}
