
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class ProductPurchaseHistory {

	private String invoice_ref_id ;
	private String vendor_id ;
	private String vendor_name ; 
	private String price ; 
	private String type ;
	private String quantity;
	private String sum;
	private String product_unit;
	private String product_id;
	private String unit_th;
	private String unit_en;

	
	private String name_th ;
	private String name_en ;
	private String inv_date;


	
	private String inv_no;

	
	public String getInvoiceRefID() {return invoice_ref_id; }
	public String getVendorId() {return vendor_id; }
	public String getVendorName() {return vendor_name; }
	public String getPrice() {return price;}
	public String getType() {return type;}
	public String getQuantity(){ return quantity;}
	public String getSum(){ return sum;}
	public String getProductId(){return product_id;}
	public String getProductUnit(){ return product_unit;}
	
	public String getUnitTH () {return unit_th;}
	public String getUnitEN () {return unit_en;}
	public String getNameTH() {return name_th; }
	public String getNameEN() {return name_en; }
	public String getInvDate(){ return inv_date;}
	
	public String getInvNo(){return inv_no;}


	

	
	public void setInvoiceRefID(String in ) {this.invoice_ref_id = in;}
	public void setVendorId(String in ) {this.vendor_id = in;}
	public void setVendorName(String in ) {this.vendor_name = in;}
	public void setPrice(String in) {this.price=in;}
	public void setType(String in) {this.type=in;}
	public void setQuantity(String in){this.quantity=in;}
	public void setSum(String in){ this.sum=in;}
	public void setProductId (String in) {this.product_id = in;}
	public void setProductUnit(String in){this.product_unit=in;}
	
	public void setUnitTH (String in) {this.unit_th = in;}
	public void setUnitEN (String in) {this.unit_en = in;}
	public void setNameTH(String in ) {this.name_th = in;}
	public void setNameEN(String in ) {this.name_en = in;}
    public void setInvDate(String in) {this.inv_date = in;}

	public void setInvoiceNo(String in){this.inv_no = in;}

	

	
}
