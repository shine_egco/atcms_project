
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class ShortBill {

	private String short_bill_id ;
	private String customer_name ; 
	private String customer_tel ;
	private String tag;
	private String type ;
	private String bill_date;
	private String create_date;
	private String total_value;
	private String total_vat;
	private String total_inc_vat;
	private String inc_generated_date;
	private String inv_file_path;
	private String inv_file_name;
	private String tax_id;
	private String note;
	private String payment_ref;
	private String total_cost;
	private String total_profit;
	private String inv_no;
	

	
	public String getshortBillId() {return short_bill_id; }
	public String getCustomerName() {return customer_name; }
	public String getCustomerTel() {return customer_tel;}
	public String getType(){ return type;}
	public String getBillDate() {return bill_date;}
	public String getCreateDate() {return create_date;}
	public String getTotalValue() {return total_value;}
	public String getTotalVat(){return total_vat;}
	public String getTotalIncVat() {return total_inc_vat;}
	public String getIncGeneratedDate() {return inc_generated_date;}
	public String getInvFilePath() {return inv_file_path;}
	public String getInvFileName() {return inv_file_name;}
	public String getTaxId(){return tax_id;}
	public String getNote(){return note;}
	public String getPaymentReference() {return payment_ref;}
	public String getTotalCost() {return total_cost;}
	public String getTotalProfit (){return total_profit;}
	public String getInvNo() {return inv_no;}
	public String getTag() {return tag;}
	
	
	public void setshortBillId(String in ) {this.short_bill_id = in;}
	public void setCustomerName(String in ) {this.customer_name = in;}
	public void setCustomerTel(String in) {this.customer_tel = in;}
	public void setType(String in){ this.type=in;}
	public void setBillDate(String in) {this.bill_date=in;}
	public void setCreateDate(String in) {this.create_date=in;}
	public void setTotalValue(String in) {this.total_value=in;}
	public void setTotalVat(String in){this.total_vat=in;}
	public void setTotalIncVat(String in) {this.total_inc_vat=in;}
	public void setIncGeneratedDate(String in) {this.inc_generated_date=in;}
	public void setInvFilePath(String in){this.inv_file_path=in;}
	public void setInvFileName(String in){this.inv_file_name=in;}
	public void setTaxId(String in){this.tax_id = in;}
	public void setNote(String in){this.note = in;}
	public void setPaymentReference(String in){this.payment_ref = in;}
	public void setTotalCost(String in) {this.total_cost =in;}
	public void setTotalProfit (String in){this.total_profit = in;}
	public void setInvNo (String in) {this.inv_no = in;}
	public void setTag (String in) {this.tag = in;}

}
