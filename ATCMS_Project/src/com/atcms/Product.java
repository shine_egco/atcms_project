
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class Product {
	private String product_id ; 
	private String name_th ;
	private String name_en ;
	private String name_pool ; 
	private String code_name ;
	private String unit_th ; 
	private String unit_en ; 
	private String category ; 
	private String group_code ;
	private String price_pool ; 
	private String display_name ; 
	private String display_unit ;
	private String product_lan_flag ;
	private String formula_id;
	private String formula_code;
	private String form_name_th;
	private String form_name_en;
	private String capacity;
	private String net_capacity;
	private String capacity_unit;
	private String package_unit;
	private String remark;
	private String product_cost;
	private String material_id;
	private String std_price;
	private String updated_date;
	private String is_scu;
	private String scu_product_id;
	
	
	
	
	public String getProductID() {return product_id; }
	public String getNameTH() {return name_th; }
	public String getNameEN() {return name_en; }
	public String getCodeName() {return code_name; }
	public String getNamePool() {return name_pool; }
	public String getUnitTH() {return unit_th;}
	public String getUnitEN() {return unit_en;}
	public String getCategory() {return category ;}
	public String getGroupCode() {return group_code;}
	public String getPricePool() {return price_pool;}
	public String getDisplayName () {return display_name;}
	public String getDisplayUnit (){return display_unit;}
	public String getProductLanFlag() {return product_lan_flag;}
	public String getMaterial_ID() {return material_id;}
	public String GetIsSCU() {return is_scu;}
	public String getSCUProductID() {return scu_product_id;}
	
	public String getFormulaID() {return formula_id;}
	public String getFormulaCode(){return formula_code;}
	public String getFomulaNameTH(){return form_name_th;}
	public String getFomulaNameEN(){return form_name_en;}
	public String getCapacity(){return capacity;}
	public String getNetCapacity() {return net_capacity;}
	public String getCapacityUnit(){return capacity_unit;}
	public String getPackageUnit(){return package_unit;}
	public String getRemark() {return remark;}
	public String getProductCost(){return product_cost;}
	public String getStdPrice() {return std_price;}
	public String getUpdatedDate() {return updated_date;}

	public void setProductID (String in) {this.product_id = in;}
	public void setNameTH(String in ) {this.name_th = in;}
	public void setNameEN(String in ) {this.name_en = in;}
	public void setCodeName (String in) {this.code_name = in; }
	public void setNamePool (String in) {this.name_pool = in;}
	public void setUnitTH ( String in ) {this.unit_th = in; }
	public void setUnitEN (String in) {this.unit_en = in ; }
	public void setCategory (String in) {this.category = in;} 
	public void setGroupCode (String in) {this.group_code = in ;}
	public void setPricePool (String in) {this.price_pool = in ;}
	public void setDisplayName (String in){this.display_name = in;}
	public void setDisplayUnit (String in) {this.display_unit = in;}
	public void setProductLanFlag (String in) {this.product_lan_flag = in;}
	
	public void setFormulaID(String in) {this.formula_id = in; }
	public void setFormulaCode (String in) {this.formula_code = in; }
	public void setFormulaNameTH(String in) {this.form_name_th = in;}
	public void setFormulaNameEN(String in) {this.form_name_en = in;}
	public void setCapacity(String in ) {this.capacity = in;}
	public void setNetCapacity(String in) {this.net_capacity = in;}
	public void setCapacityUnit(String in) {this.capacity_unit = in;}
	public void setPackageUnit(String in) {this.package_unit = in; }
	public void setRemark (String in) {this.remark = in;}
	public void setProductCost(String in) {this.product_cost = in; }
	public void setMaterial_ID(String in) {this.material_id = in;}
	public void setStdPrice(String in) {this.std_price = in;}
	public void setUpdatedDate(String in) {this.updated_date = in;}
	public void setIsSCU(String in) {this.is_scu = in;}
	public void setSCUProductId(String in) {this.scu_product_id = in;}
	
}
