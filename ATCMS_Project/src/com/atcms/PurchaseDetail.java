
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class PurchaseDetail {

	private String purchase_id ;
	private String vendor_id ;
	private String vendor_name ; 
	private String vendor_address;
	private String status ;
	private String type ;
	private String order_date;
	private String completed_date;
	private String delivery_date;
	private String due_date;
	private String create_date;
	private String total_value;
	private String total_vat;
	private String total_inc_vat;
	private String inv_no;
	private String inv_file_path;
	private String inv_file_name;
	private String price;
	private String quantity;
	private String sum;
	private String invoice_date;
	private String product_unit;
	private String product_id;
	private String product_name;
	private String index;
	private String purchase_main_total_value;
	private String purchase_main_total_vat;
	private String purchase_main_total_inc_vat;
	
	
	
	
	public String getPurchaseId() {return purchase_id; }
	public String getVendorId() {return vendor_id; }
	public String getVendorName() {return vendor_name; }
	public String getVendorAddress() {return vendor_address;}
	public String getPurchaseStatus() {return status; }
	public String getType(){ return type;}
	public String getOrderDate() {return order_date;}
	public String getCompletedDate(){return completed_date;}
	public String getDeliveryDate(){return delivery_date;}
	public String getDueDate(){return due_date;}
	public String getCreateDate() {return create_date;}
	public String getTotalValue() {return total_value;}
	public String getTotalVat(){return total_vat;}
	public String getTotalIncVat() {return total_inc_vat;}
	public String getInvNo() {return inv_no;}
	public String getInvFilePath() {return inv_file_path;}
	public String getInvFileName() {return inv_file_name;}
	public String getPrice() {return price;}
	public String getQuantity(){return quantity;}
	public String getSum(){return sum;}
	public String getInvoiceDate(){return invoice_date;}
	public String getProductUnit(){return product_unit;}
	public String getProductID(){return product_id;}
	public String getProductName(){return product_name;}
	public String  getIndex() {return index;}
	public String getPurchaseMainTotalValue(){return purchase_main_total_value;}
	public String getPurchaseMainTotalVat(){return purchase_main_total_vat;}
	public String getPurchaseMainTotalIncVat() {return purchase_main_total_inc_vat;}
	
	
	
	public void setPurchaseId(String in ) {this.purchase_id = in;}
	public void setVendorId(String in ) {this.vendor_id = in;}
	public void setVendorName(String in ) {this.vendor_name = in;}
	public void setVendorAddress(String in) {this.vendor_address = in;}
	public void setPurchaseStatus (String in) {this.status = in;}
	public void setType(String in){ this.type=in;}
	public void setOrderDate(String in) {this.order_date=in;}
	public void setCompletedDate(String in){this.completed_date=in;}
	public void setDeliveryDate(String in){this.delivery_date=in;}
	public void setDueDate(String in){this.due_date=in;}
	public void setCreateDate(String in) {this.create_date=in;}
	public void setTotalValue(String in) {this.total_value=in;}
	public void setTotalVat(String in){this.total_vat=in;}
	public void setTotalIncVat(String in) {this.total_inc_vat=in;}
	public void setInvNo(String in) {this.inv_no=in;}
	public void setInvFilePath(String in){this.inv_file_path=in;}
	public void setInvFileName(String in){this.inv_file_name=in;}
	public void setPrice(String in){this.price = in;}
	public void setQuantity(String in){this.quantity = in;}
	public void setSum(String in){this.sum = in;}
	public void setInvoiceDate(String in){this.invoice_date = in;}
	public void setProductUnit(String in){this.product_unit = in;}
	public void setProductID(String in) {this.product_id = in;}
	public void setProductName(String in) {this.product_name= in;}
	public void setIndex(String in) {this.index = in; }
	public void setPurchaseMainTotalValue(String in) {this.purchase_main_total_value = in; }
	public void setPurchaseMainTotalVat(String in) {this.purchase_main_total_vat = in; }
	public void setPurchaseMainTotalIncVat(String in) {this.purchase_main_total_inc_vat = in;}

}
