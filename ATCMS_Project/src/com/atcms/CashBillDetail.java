
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class CashBillDetail {

	private String cash_bill_id ;
	private String customer_id ;
	private String customer_name ; 
	private String order_status ;
	private String price ; 
	private String type ;
	private String adt_description ; 
	private String quantity;
	private String sum;
	private String product_unit;
	private String product_id;
	private String product_name ; 
	private String cost_per_unit;
	private String sum_cost;
	private String profit_per_unit;
	private String sum_profit;
	private String index;

	
	private String inv_no;
	private String cash_bill_main_total_inc_vat;
	
	private String cash_bill_main_total_value;
	private String cash_bill_main_total_vat;
	private String cash_bill_main_total_cost;
	private String cash_bill_main_total_profit;
	private String cash_bill_main_avg_profit_percentage;
	private String bill_date;
	
	private String profit_percentage;
	private String inv_date;
	
	
	public String getCashBillId() {return cash_bill_id; }
	public String getCustomerId() {return customer_id; }
	public String getCustomerName() {return customer_name; }
	public String getOrderStatus() {return order_status; }
	public String getPrice() {return price;}
	public String getType() {return type;}
	public String getAdtDescription(){return adt_description;}
	public String getQuantity(){ return quantity;}
	public String getSum(){ return sum;}
	public String getProductId(){return product_id;}
	public String getProductUnit(){ return product_unit;}
	public String getProductName(){ return product_name;}
	public String getCostPerUnit(){return cost_per_unit;}
	public String getSumCost(){return sum_cost;}
	public String getProfitPerUnit () {return profit_per_unit;}
	public String getSumProfit() {return sum_profit;}
	public String getIndex(){return index;}
	public String getInvNo(){return inv_no;}
	public String getCashBillMainTotalIncVat() {return cash_bill_main_total_inc_vat;}
	public String getBillDate(){return bill_date;}
	
	public String getCashBillMainTotalValue(){return cash_bill_main_total_value;}
	public String getCashBillMainTotalVat(){return cash_bill_main_total_vat;}
	public String getCashBillMainTotalCost(){return cash_bill_main_total_cost;}
	public String getCashBillMainTotalProfit(){return cash_bill_main_total_profit;}
	public String getCashBillMainAvgProfirPercentage(){return cash_bill_main_avg_profit_percentage;}	
	
	public String getProfitPercentage(){return profit_percentage;}
	public String getInvoiceDate() {return inv_date;}
	

	
	public void setCashBillId(String in ) {this.cash_bill_id = in;}
	public void setCustomerId(String in ) {this.customer_id = in;}
	public void setCustomerName(String in ) {this.customer_name = in;}
	public void setOrderStatus (String in) {this.order_status = in;}
	public void setPrice(String in) {this.price=in;}
	public void setType(String in) {this.type=in;}
	public void setAdtDescription(String in){this.adt_description=in;}
	public void setQuantity(String in){this.quantity=in;}
	public void setSum(String in){ this.sum=in;}
	public void setProductId (String in) {this.product_id = in;}
	public void setProductUnit(String in){this.product_unit=in;}
	public void setProductName(String in){this.product_name=in;}
	
	public void setCostPerUnit(String in) {this.cost_per_unit = in;}
	public void setSumCost(String in) {this.sum_cost = in;}
	public void setProfitPerUnitI(String in) {this.profit_per_unit = in;}
	public void setSumProfit(String in) {this.sum_profit = in;}
	public void setIndex(String in){this.index = in;}
	public void setInvoiceNo(String in){this.inv_no = in;}
	public void setCashBillIMainTotalIncVat(String in) {this.cash_bill_main_total_inc_vat = in;}
	public void setBillDate (String in) {this.bill_date = in;}
	
	public void setCashBillMainTotalValue(String in) {this.cash_bill_main_total_value = in;}
	public void setCashBillMainTotalVat(String in) {this.cash_bill_main_total_vat = in;}
	public void setCashBillMainTotalCost(String in){this.cash_bill_main_total_cost = in;}
	public void setCashBillMainTotalProfit(String in) {this.cash_bill_main_total_profit = in;}
	public void setCashBillMainAvgProfitPercentage(String in) {this.cash_bill_main_avg_profit_percentage = in;}
	
	public void setProfitPercentage(String in) {this.profit_percentage = in;}
	public void setInvoiceDate(String in) {this.inv_date = in;}
}
