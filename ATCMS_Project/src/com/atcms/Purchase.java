
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class Purchase {

	private String purchase_id ;
	private String vendor_id ;
	private String vendor_name ; 
	private String vendor_address;
	private String status ;
	private String type ;
	private String order_date;
	private String completed_date;
	private String delivery_date;
	private String due_date;
	private String create_date;
	private String total_value;
	private String total_vat;
	private String total_inc_vat;
	private String inv_no;
	private String inv_file_path;
	private String inv_file_name;
	private String invoice_date;
	private String vendor_tax_id;
	private String payment_method;
	private String payment_ref;
	private String note;

	
	public String getPurchaseId() {return purchase_id; }
	public String getVendorId() {return vendor_id; }
	public String getVendorName() {return vendor_name; }
	public String getVendorAddress() {return vendor_address;}
	public String getPurchaseStatus() {return status; }
	public String getType(){ return type;}
	public String getOrderDate() {return order_date;}
	public String getCompletedDate(){return completed_date;}
	public String getDeliveryDate(){return delivery_date;}
	public String getDueDate(){return due_date;}
	public String getCreateDate() {return create_date;}
	public String getTotalValue() {return total_value;}
	public String getTotalVat(){return total_vat;}
	public String getTotalIncVat() {return total_inc_vat;}
	public String getInvNo() {return inv_no;}
	public String getInvFilePath() {return inv_file_path;}
	public String getInvFileName() {return inv_file_name;}
	public String getInvoiceDate() {return invoice_date;}
	public String getVendorTaxID() {return vendor_tax_id;}
	public String getPaymentMethod() {return payment_method;}
	public String getPaymentRef() {return payment_ref;}
	public String getNote() {return note;}
	
	
	public void setPurchaseId(String in ) {this.purchase_id = in;}
	public void setVendorId(String in ) {this.vendor_id = in;}
	public void setVendorName(String in ) {this.vendor_name = in;}
	public void setVendorAddress(String in) {this.vendor_address = in;}
	public void setPurchaseStatus (String in) {this.status = in;}
	public void setType(String in){ this.type=in;}
	public void setOrderDate(String in) {this.order_date=in;}
	public void setCompletedDate(String in){this.completed_date=in;}
	public void setDeliveryDate(String in){this.delivery_date=in;}
	public void setDueDate(String in){this.due_date=in;}
	public void setCreateDate(String in) {this.create_date=in;}
	public void setTotalValue(String in) {this.total_value=in;}
	public void setTotalVat(String in){this.total_vat=in;}
	public void setTotalIncVat(String in) {this.total_inc_vat=in;}
	public void setInvNo(String in) {this.inv_no=in;}
	public void setInvFilePath(String in){this.inv_file_path=in;}
	public void setInvFileName(String in){this.inv_file_name=in;}
	public void setInvoiceDate(String in){this.invoice_date = in ;}
	public void setVendorTaxID(String in){ this.vendor_tax_id = in;}
	public void setPaymentMethod(String in) {this.payment_method = in;}
	public void setPatmentRef(String in) {this.payment_ref = in;}
	public void setNote(String in) {this.note = in;}
}
