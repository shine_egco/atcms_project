
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class CreditAndCashDetail {

	private String sell_id ;
	private String customer_id ;
	private String customer_name ; 
	private String price ; 
	private String type ;
	private String quantity;
	private String sum;
	private String product_unit;
	private String product_name ; 
	private String inv_date ;
	private String inv_no;
	

	
	public String getSellId() {return sell_id;}
	public String getCustomerId() {return customer_id; }
	public String getCustomerName() {return customer_name; }
	public String getPrice() {return price;}
	public String getType() {return type;}
	public String getQuantity(){ return quantity;}
	public String getSum(){ return sum;}
	public String getProductUnit(){ return product_unit;}
	public String getProductName(){ return product_name;}
	public String getInvDate(){return inv_date;}
	public String getInvNo() {return inv_no;}
	

	
	public void setSellId(String in) {this.sell_id = in;}
	public void setCustomerId(String in ) {this.customer_id = in;}
	public void setCustomerName(String in ) {this.customer_name = in;}
	public void setPrice(String in) {this.price=in;}
	public void setType(String in) {this.type=in;}
	public void setQuantity(String in){this.quantity=in;}
	public void setSum(String in){ this.sum=in;}
	public void setProductUnit(String in){this.product_unit=in;}
	public void setProductName(String in){this.product_name=in;}
	public void setInvDate(String in){this.inv_date =in;}
	public void setInvNo (String in){ this.inv_no = in;}

}
