package com.atcms;

import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class KeyGen {

	private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";;
	private static final String ALPHABET_LONG = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	private static final long BASE = 36;
	private static final String DIGIT = "0123456789";

	public String temp_code;

	public static String genTimeStamp() {
		Date date = new Date();
		String temp_year = date.getYear() + 1900 + "";
		String temp_month = date.getMonth() + 1 + "";
		String temp_date = date.getDate() + "";
		String temp_hour = date.getHours() + "";
		String temp_minute = date.getMinutes() + "";
		String temp_second = date.getSeconds() + "";

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		String key = temp_year + temp_month + temp_date + temp_hour
				+ temp_minute + temp_second + System.currentTimeMillis();

		return key;
	}

	public static String encode(long num) {

		// for generate batch number
		StringBuilder sb = new StringBuilder();

		while (num > 0) {
			sb.append(ALPHABET.charAt((int) (num % BASE)));
			num /= BASE;
		}

		return sb.reverse().toString();
	}

	public static String encode_long(long num) {

		// for replace planetext timestamp

		StringBuilder sb = new StringBuilder();

		while (num > 0) {
			sb.append(ALPHABET_LONG.charAt((int) (num % BASE)));
			num /= BASE;
		}

		return sb.reverse().toString();
	}

	public static String getTimeStamp() {

		Date date = new Date();
		String id = encode_long(date.getTime());

		return id;

	}

	public static String generateOrderID() {
		Date date = new Date();
		date.setYear(1000);
		String id = encode_long(date.getTime());

		// System.out.println(date);

		return id;

	}

	public static String generateCashBillID() {
		Date date = new Date();
		date.setYear(1100);
		String id = encode_long(date.getTime());

		// System.out.println(date);

		return id;

	}
	public static String generateAbbInvID() {
		Date date = new Date();
		date.setYear(2100);
		String id = encode_long(date.getTime());

		// System.out.println(date);

		return id;

	}

	public static String generateDailyNovatID() {
		Date date = new Date();
		date.setYear(1200);
		String id = encode_long(date.getTime());

		// System.out.println(date);

		return id;

	}

	public static String generateCreditNoteID() {
		Date date = new Date();
		date.setYear(1300);
		String id = encode_long(date.getTime());

		return id;

	}

	public static String generatePurchaseID() {
		Date date = new Date();
		date.setYear(1400);
		String id = encode_long(date.getTime());

		return id;

	}

	public static String generateCompanyID() {
		Date date = new Date();
		date.setYear(1500);
		String id = encode_long(date.getTime());
		return id;

	}

	public static String generateFormulaID() {
		Date date = new Date();
		date.setYear(1600);
		String id = encode_long(date.getTime());
		return id;

	}

	public static String generateProductID() {
		Date date = new Date();
		date.setYear(1700);
		String id = encode_long(date.getTime());
		return id;

	}

	public static String generateWorkOrderID() {
		Date date = new Date();
		date.setYear(1800);
		String id = encode_long(date.getTime());
		return id;

	}

	public static String generateMonthlySummaryReportID() {
		Date date = new Date();
		date.setYear(1900);
		String id = encode_long(date.getTime());
		return id;

	}

	public static String generateBatchNumber() {

		Date date = new Date();
		date.setYear(2000);
		String id = encode(date.getTime());

		return id;
	}
	public static String generateShortBillID() {
		Date date = new Date();
		date.setYear(2100);
		String id = encode_long(date.getTime());
		return id;

	}
	
	

	public void setTempCode(String in) {
		this.temp_code = in;

	}

	public static String generateQuatationNo(){
		
		Date date = new Date();
		
		String temp_year = (date.getYear()-100 )+ "";
		String temp_month = date.getMonth() + 1 + "";
		String temp_date = date.getDate() + "";
		String temp_hour = date.getHours() + "";
		String temp_minute = date.getMinutes() + "";
		String temp_second =( date.getSeconds()%6)+ "";
		
		String part_2 = temp_hour+temp_minute+temp_second;
		

		String prefix = "NQ";
		
		String code = prefix+temp_year +temp_month+temp_date;
			   code = code + part_2;
		
		return code;
	}

}
