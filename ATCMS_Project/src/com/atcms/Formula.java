
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class Formula {

	private String formula_id ;
	private String formula_code ;
	private String name_th ; 
	private String name_en;
	private String description ;
	private String total_cost ;
	private String capacity;
	private String capacity_unit ; 
	private String packaging_unit;
	private String remark ;

	
	public String getFormulaID() {return formula_id; }
	public String getFormulaCode() {return formula_code; }
	public String getNameTH() {return name_th; }
	public String getNameEN() {return name_en;}
	public String getDescription() {return description; }
	public String getTotalCost() {return total_cost;}
	public String getCapacity(){ return capacity;}
	public String getCapacityUnit() {return capacity_unit;}
	public String getPackagingUnit() {return packaging_unit;}
	public String getRemark(){return remark;}


	public void setFormulaID(String in ) {this.formula_id = in;}
	public void setFormulaCode(String in ) {this.formula_code = in;}
	public void setNameTH(String in ) {this.name_th = in;}
	public void setNameEN(String in) {this.name_en = in;}
	public void setDescription (String in) {this.description = in;}
	public void setTotalCost(String in) {this.total_cost =in;}
	public void setCapacity(String in){ this.capacity=in;}
	public void setCapacityUnit(String in) {this.capacity_unit=in;}
	public void setPackagingUnit(String in) {this.packaging_unit=in;}
	public void setRemark(String in) {this.remark = in;}


}
