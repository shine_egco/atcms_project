
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class DailyNovatDetail {

	private String daily_novat_id;
	private String adt_description;
	private String price; 
	private String quantity;
	private String sum;
	private String unit;
	private String bill_date;
	private String total_value;
	private String product_name;
	private String product_id;
	
	private String cost_per_unit;
	private String sum_cost;
	private String profit_per_unit;
	private String sum_profit;
	private String index;
	private String profit_percentage;
	private String dn_main_total_value;
	private String dn_main_avg_profit_percentage;
	private String dn_main_total_cost;
	private String dn_main_total_profit;
	
	private String name_ref;
	private String contact_ref;
	
	


	public String getTotalValue() {return total_value;}
	public String getDailyNovatId () {return daily_novat_id;}
	public String getAdtDescription() {return adt_description;}
	public String getPrice() {return price;}
	public String getQuantity() {return quantity;}
	public String getSum () {return sum;}
	public String getUnit(){return unit;}
	public String getBillDate () {return bill_date;}
	public String getProductName () {return product_name;}
	public String getCostPerUnit(){return cost_per_unit;}
	public String getSumCost(){return sum_cost;}
	public String getProfitPerUnit () {return profit_per_unit;}
	public String getSumProfit() {return sum_profit;}
	public String getIndex(){return index;}
	
	public String getProfitPercentage (){return profit_percentage;}
	public String getDnMainTotalValue(){return dn_main_total_value;}
	public String getDnMainAvgProfitPercentage(){return dn_main_avg_profit_percentage;}
	public String getDnMainTotalCost(){return dn_main_total_cost;}
	public String getDnMainTotalProfit(){return dn_main_total_profit;}
   
	public String getNameRef(){return name_ref;}
	public String getContactRef(){return  contact_ref;}	
	public String getProductId() {return product_id;}
	
	
	public void setTotalValue(String in) {this.total_value=in;}
	public void setDailyNovatId(String in) {this.daily_novat_id=in;}
	public void setAdtDescription(String in) {this.adt_description=in;}
	public void setPrice(String in) {this.price=in;}
	public void setSum(String in) { this.sum =in;}
	public void setUnit(String in) {this.unit = in;}
	public void setBillDate(String in ){this.bill_date = in;}
	public void setQuantity (String in) {this.quantity = in;}
	public void setProductName(String in) {this.product_name = in;}	
	public void setCostPerUnit(String in) {this.cost_per_unit = in;}
	public void setSumCost(String in) {this.sum_cost = in;}
	public void setProfitPerUnitI(String in) {this.profit_per_unit = in;}
	public void setSumProfit(String in) {this.sum_profit = in;}
	public void setIndex(String in){this.index = in;}
	public void setProfitPercentage(String in){this.profit_percentage = in;}
	public void setDnMainTotalValue(String in) {this.dn_main_total_value = in;}
	public void setDnMainAvgProfitPercentage(String in){this.dn_main_avg_profit_percentage = in ;}
	public void setDnMainTotalCost(String in) {this.dn_main_total_cost = in;}
	 public void setDnmainTotalProfit(String in){this.dn_main_total_profit = in;}
	 public void setNameRef(String in) {this.name_ref = in;}
	 public void setContactRef(String in) {this.contact_ref = in;}
	 public void setProductId(String in) {this.product_id = in;}

}
