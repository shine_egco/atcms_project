
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class Description {

	private String code ;
	private String type ;
	private String short_description ; 
	private String long_description ;

	
	public String getCode() {return code; }
	public String getType() {return type; }
	public String getShortDescription() {return short_description; }
	public String getLongDescription() {return long_description; }

	
	public void setCode(String in ) {this.code = in;}
	public void setType(String in ) {this.type = in;}
	public void setShortDescription(String in ) {this.short_description = in;}
	public void setLongDescription (String in) {this.long_description = in;}



}
