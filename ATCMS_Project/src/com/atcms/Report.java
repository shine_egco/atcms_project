
package com.atcms;
import java.util.Date;
import java.util.Calendar;
import java.sql.Timestamp;

public class Report {

	private String credit_total_inc_vat; // 
	private String cash_bill_total_inc_vat ;
	private String daily_vonat_total_value ; 

	
	public String getCreditTotalIncVat() {return credit_total_inc_vat; }
	public String getCashBillTotalIncVat() {return cash_bill_total_inc_vat; }
	public String getDailyNovatTotalValue() {return daily_vonat_total_value; }

	public void setCreditTotalIncVat(String in ) {this.credit_total_inc_vat = in;}
	public void setCashBillTotalIncVat(String in ) {this.cash_bill_total_inc_vat = in;}
	public void setDailyNovatTotalValue(String in ) {this.daily_vonat_total_value = in;}
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////////
	
	private String formula_id ;
	private String product_id;
	private String pro_name_th ;
	private String pro_name_en;
	private String form_name_th;
	private String form_name_en;
	
	private String total_quantity;
	private String total_cost;
	private String full_unit;
	
	public String getFormulaID() {return formula_id;}
	public String getProductID(){return product_id;}
	public String getProNameTH (){return pro_name_th;}
	public String getProNameEN(){return pro_name_en;}
	public String getFormNameTH(){return form_name_th;}
	public String getFormNameEN(){return form_name_en;}
	public String getTotalQuantity (){return total_quantity;}
	public String getTotalCost(){return total_cost;}
	public String getFullUnit(){return full_unit;}

	
	public void setFormulaID(String in) { this.formula_id = in;}
	public void setProductID(String in) {this.product_id = in ;}
	public void setProNameTH(String in){this.pro_name_th = in;}
	public void setProNameEN(String in){this.pro_name_en = in;}
	public void setFormNameTH(String in) {this.form_name_th = in;}
	public void setFormNameEN (String in) {this.form_name_en = in;}
	public void setTotalQuantity (String in) {this.total_quantity = in;}
	public void setTotalCost(String in){this.total_cost = in ;}
	public void setFullUnit(String in) {this.full_unit = in;}
	

}
